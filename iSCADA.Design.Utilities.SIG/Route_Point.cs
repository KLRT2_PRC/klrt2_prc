﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iSCADA.Design.Utilities.SIG
{
    public class Route_Point : SIGSymbol
    {
        protected override void OnPainting(PaintEventArgs e)
        {


            GraphicsPath _gPath = new GraphicsPath();

            if (this.SymbolOrientation == SymbolOrientation.Horizontal)
            {
                switchsize = this.Width / 3;
                this.Height = Toint(switchsize * 2.5);
                SetHorizontalGpath(switchsize, _gPath);

            }
            else
            {
                switchsize = this.Height / 3;
                this.Width = Toint(switchsize * 2.5);
                SetVerticleGpath(switchsize, _gPath);
            }


            SetRotate(e);
            StartPaint(e, _gPath);

        }





        private void DrawText(PaintEventArgs e)
        {
            if (!string.IsNullOrEmpty(this._innerText))
            {
                Point p;
                Font f = new Font(this.Font.FontFamily, (float)(switchsize * 1.5), this.Font.Style);
                StringFormat sf = new StringFormat(); ;
                if (this.SymbolOrientation == SymbolOrientation.Horizontal)
                {
                    p = new Point(Toint(switchsize / 2), Toint((p1.Y + p2.Y) / 2f));

                    sf.FormatFlags = StringFormatFlags.DirectionVertical;
                    sf.LineAlignment = StringAlignment.Center;
                    sf.Alignment = StringAlignment.Center;

                    e.Graphics.TranslateTransform(p.X, p.Y);
                    e.Graphics.RotateTransform(180);
                    e.Graphics.TranslateTransform(-p.X, -p.Y);

                }
                else
                {
                    p = new Point(Toint((p1.X + p2.X) / 2f), Toint(switchsize ));
                    sf.LineAlignment = StringAlignment.Center;
                    sf.Alignment = StringAlignment.Center;

                    if (this.Reversion)
                    {
                        e.Graphics.TranslateTransform(p.X, p.Y);
                        e.Graphics.RotateTransform(180);
                        e.Graphics.TranslateTransform(-p.X, -p.Y);
                    }
                }

                DrawString(e.Graphics, this._innerText, f, p, sf);
            }
        }

        private void SetHorizontalGpath(float switchsize, GraphicsPath _gPath)
        {

            p1 = new Point(Toint(switchsize * 0.1f), Toint(switchsize * 0.25f));
            p2 = new Point(p1.X, Toint(switchsize * 2.25f));
            p3 = new Point(Toint(switchsize * 2), p2.Y);
            p4 = new Point(Toint(switchsize * 2.9f), Toint(switchsize * 1.25));
            p5 = new Point(p3.X, p1.Y);

            _gPath.AddLine(p1, p2);
            _gPath.AddLine(p2, p3);
            _gPath.AddLine(p3, p4);
            _gPath.AddLine(p4, p5);
            _gPath.AddLine(p5, p1);




        }

        private void SetVerticleGpath(float switchsize, GraphicsPath _gPath)
        {
            p1 = new Point(Toint(switchsize * 0.25f), Toint(switchsize * 0.1f));
            p2 = new Point(Toint(switchsize * 2.25f), p1.Y);
            p3 = new Point(p2.X, Toint(switchsize * 2));
            p4 = new Point(Toint(switchsize * 1.25), Toint(switchsize * 2.9f));
            p5 = new Point(p1.X, p3.Y);


            _gPath.AddLine(p1, p2);
            _gPath.AddLine(p2, p3);
            _gPath.AddLine(p3, p4);
            _gPath.AddLine(p4, p5);
            _gPath.AddLine(p5, p1);
            _gPath.CloseFigure();
        }

        private void SetRotate(PaintEventArgs e)
        {
            int rotate = 0;

            if (this.SymbolOrientation == SymbolOrientation.Horizontal && this.Reversion)

                rotate = 180;
            else if (this.SymbolOrientation == SymbolOrientation.Verticle && this.Reversion)
                rotate = 180;
            else
                return;

            e.Graphics.TranslateTransform(Width / 2, Height / 2);
            e.Graphics.RotateTransform(rotate);
            e.Graphics.TranslateTransform(Width / -2, Height / -2);
        }

        private void StartPaint(PaintEventArgs e, GraphicsPath _gPath)
        {


            if (this.Fill)
                FillPath(e.Graphics, _gPath);
            else
                DrawPath(e.Graphics, _gPath);

            DrawText(e);
        }

        #region Field

        //基本單位
        float switchsize;

        //五個點
        private Point p1, p2, p3, p4, p5;

        //文字的方框大小
        private Rectangle textRect;

        //可以寫字
        private string _innerText;

        #endregion

        #region Property

        public string InnerText
        {
            get { return this._innerText; }
            set
            {
                this._innerText = value;
                this.Refresh();
            }
        }

        #endregion
    }
}
