﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iSCADA.Design.Utilities.SIG
{
    public abstract class SIGSymbol : UserControl, ISerializable
    {


        public event EventHandler<EventArgs> OnToolStripItemClick;


        private bool _fillGradiant = false;
        private SymbolState _state = SymbolState.Off;
        private SymbolStyle _style = SymbolStyle.Default;

        private System.Drawing.Pen _pen = new Pen(Brushes.Black);
        private DashStyle _dashstyle = DashStyle.Solid;
        private LineCap _startcap = LineCap.NoAnchor;
        private LineCap _endcap = LineCap.NoAnchor;
        private int _extenderWidth = 3;
        private System.Drawing.Brush _brush = System.Drawing.Brushes.Black;
        private bool _reversion = false;
        private SymbolOrientation _orientation;


        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            throw new NotImplementedException();
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // ElectricalSymbol
            // 
            this.BackColor = System.Drawing.Color.Transparent;
            this.SetStyle(System.Windows.Forms.ControlStyles.SupportsTransparentBackColor, true);
            this.Name = "ElectricalSymbol";
            //this._pen.DashStyle =(System.Drawing.Drawing2D.DashStyle) this._dashstyle;
            this.ResumeLayout(false);
        }

        #region DrawShape

        public void DrawLine(Graphics e, Point startPoint, Point endPoint)
        {
            e.DrawLine(this.Pen, startPoint, endPoint);
        }

        public void DrawPath(Graphics e, GraphicsPath path)
        {
            e.DrawPath(this.Pen, path);
        }

        public void DrawArc(Graphics e, float x1, float y1, float radius)
        {
            DrawArc(e, x1, y1, radius, radius, 0, 360);
        }


        public void DrawArc(Graphics e, float x1, float y1, float radiusWidth, float radiusHeight, float startAngle, float sweepAngle)
        {
            e.DrawArc(this.Pen, x1, y1, radiusWidth, radiusHeight, startAngle, sweepAngle);
        }


        /// <summary>
        /// 透過圓畫正三角形
        /// </summary>
        /// <param name="e"></param>
        /// <param name="circleCenterPoint">圓心</param>
        /// <param name="Radius">圓半徑</param>
        /// <param name="orientation">水平或垂直</param>
        public void DrawRegularTriangle(Graphics e, Point circleCenterPoint, int Radius, int angle)
        {
            Point t1 = new Point()
            {
                X = circleCenterPoint.X,
                Y = circleCenterPoint.Y - Radius
            };


            double THeight = ((float)3 / 2) * Radius;
            double SideLength = Math.Sqrt(3) * Radius;

            int tempX1 = (int)t1.X;
            int tempX2 = (int)(t1.Y + THeight);

            Point t2 = new Point()
            {
                X = (int)(tempX1 + SideLength / 2),
                Y = tempX2
            };

            Point t3 = new Point()
            {
                X = (int)(tempX1 - SideLength / 2),
                Y = tempX2
            };

            e.TranslateTransform(circleCenterPoint.X, circleCenterPoint.Y);
            e.RotateTransform(angle);
            e.TranslateTransform(-(circleCenterPoint.X), -(circleCenterPoint.Y));



            e.DrawLine(this.Pen, t1, t2);
            e.DrawLine(this.Pen, t2, t3);
            e.DrawLine(this.Pen, t1, t3);
        }


        public void DrawRectangles(Graphics e, Rectangle[] rect)
        {
            e.DrawRectangles(this.Pen, rect);
        }

        public void DrawString(Graphics e, string value, Font font, Point p, StringFormat sf)
        {
            e.DrawString(value, font, this.Brush, p, sf);
        }
        #endregion

        #region FillShape

        public void FillRectangles(Graphics e, Rectangle[] rect)
        {
            e.FillRectangles(this.Brush, rect);
        }

        public void FillTriangle(Graphics e, Point p1, Point p2, Point p3)
        {
            GraphicsPath _gPath = new GraphicsPath();

            _gPath.AddLine(p1, p2);
            _gPath.CloseFigure();
            _gPath.AddLine(p1, p3);
            _gPath.CloseFigure();
            _gPath.AddLine(p2, p3);
            _gPath.CloseFigure();

            e.FillPath(this.Brush, _gPath);
        }

        public void FillPath(Graphics e, GraphicsPath gPath)
        {
            GraphicsPath _gPath = new GraphicsPath();

            e.FillPath(this.Brush, gPath);
        }

        #endregion

        #region protect method
        protected int Toint(decimal value)
        {
            return ((int)value);
        }

        protected int Toint(float value)
        {
            return ((int)value);
        }

        protected int Toint(double value)
        {
            return ((int)value);
        }

        #endregion

        #region vitual method

        protected virtual void ToolStripItem_Click(object sender, EventArgs e)
        {
            // _RaiseEvent( sender, e);
            if (OnToolStripItemClick != null)
            {
                OnToolStripItemClick.Invoke(sender, e);
            }
        }

        #endregion

        #region override method
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            OnPainting(e);
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            this.Refresh();
        }

        #endregion

        #region abstract method

        protected abstract void OnPainting(PaintEventArgs e);

        #endregion

        #region Property

        //[Category("Electrical Symbols"), Description("Get the value of drawing brush"), DefaultValueAttribute(true)]
        [Category("Electrical Symbols"), Description("Get the value of drawing brush")]
        public virtual System.Drawing.Brush Brush
        {
            get { return this._brush; }
            set
            {
                this._brush = value;
                this.Refresh();
            }
        }

        //[Category("Electrical Symbols"), Description("Get the value of drawing pen"), DefaultValueAttribute(true)]
        [Category("Electrical Symbols"), Description("Get the value of drawing pen")]
        //public virtual System.Drawing.Pen Pen
        public new System.Drawing.Pen Pen
        {
            get { return this._pen; }
            set
            {
                this._pen = value;
                this._pen.Width = this.ExtenderWidth;

                this.Refresh();
            }
        }

        public bool Reversion
        {
            get { return this._reversion; }
            set
            {
                this._reversion = value;
                this.Refresh();
            }
        }
        ////[Category("Electrical Symbols"), Description("Get or set the DashStyle"), DefaultValueAttribute(DashStyle.Solid)]
        [Category("Electrical Symbols"), Description("Get or set the DashStyle")]
        public DashStyle dashstyle
        {
            get { return this._dashstyle; }
            set
            {
                this._dashstyle = value;
                switch (value)
                {
                    case DashStyle.Dash:
                        this._pen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash;
                        break;
                    case DashStyle.DashDot:
                        this._pen.DashStyle = System.Drawing.Drawing2D.DashStyle.DashDot;
                        break;
                    case DashStyle.DashDotDot:
                        this._pen.DashStyle = System.Drawing.Drawing2D.DashStyle.DashDotDot;
                        break;
                    case DashStyle.Dot:
                        this._pen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dot;
                        break;
                    case DashStyle.Solid:
                        this._pen.DashStyle = System.Drawing.Drawing2D.DashStyle.Solid;
                        break;
                }
                this.Refresh();
            }
        }

        [Category("Electrical Symbols"), Description("Get or set the StartCap"), DefaultValueAttribute(LineCap.NoAnchor)]
        public LineCap StartCap
        {
            get { return this._startcap; }
            set
            {
                this._startcap = value;
                switch (value)
                {
                    case LineCap.ArrowAnchor:
                        this._pen.StartCap = System.Drawing.Drawing2D.LineCap.ArrowAnchor;
                        break;
                    case LineCap.DiamondAnchor:
                        this._pen.StartCap = System.Drawing.Drawing2D.LineCap.DiamondAnchor;
                        break;
                    case LineCap.Flat:
                        this._pen.StartCap = System.Drawing.Drawing2D.LineCap.Flat;
                        break;
                    case LineCap.Round:
                        this._pen.StartCap = System.Drawing.Drawing2D.LineCap.Round;
                        break;
                    case LineCap.RoundAnchor:
                        this._pen.StartCap = System.Drawing.Drawing2D.LineCap.RoundAnchor;
                        break;
                    case LineCap.Square:
                        this._pen.StartCap = System.Drawing.Drawing2D.LineCap.Square;
                        break;
                    case LineCap.SquareAnchor:
                        this._pen.StartCap = System.Drawing.Drawing2D.LineCap.SquareAnchor;
                        break;
                    case LineCap.Triangle:
                        this._pen.StartCap = System.Drawing.Drawing2D.LineCap.Triangle;
                        break;
                    default:
                        this._pen.StartCap = System.Drawing.Drawing2D.LineCap.NoAnchor;
                        break;
                }
                this.Refresh();
            }
        }

        [Category("Electrical Symbols"), Description("Get or set the EndCap"), DefaultValueAttribute(LineCap.NoAnchor)]
        public LineCap EndCap
        {
            get { return this._endcap; }
            set
            {
                this._endcap = value;
                switch (value)
                {
                    case LineCap.ArrowAnchor:
                        this._pen.EndCap = System.Drawing.Drawing2D.LineCap.ArrowAnchor;
                        break;
                    case LineCap.DiamondAnchor:
                        this._pen.EndCap = System.Drawing.Drawing2D.LineCap.DiamondAnchor;
                        break;
                    case LineCap.Flat:
                        this._pen.EndCap = System.Drawing.Drawing2D.LineCap.Flat;
                        break;
                    case LineCap.Round:
                        this._pen.EndCap = System.Drawing.Drawing2D.LineCap.Round;
                        break;
                    case LineCap.RoundAnchor:
                        this._pen.EndCap = System.Drawing.Drawing2D.LineCap.RoundAnchor;
                        break;
                    case LineCap.Square:
                        this._pen.EndCap = System.Drawing.Drawing2D.LineCap.Square;
                        break;
                    case LineCap.SquareAnchor:
                        this._pen.EndCap = System.Drawing.Drawing2D.LineCap.SquareAnchor;
                        break;
                    case LineCap.Triangle:
                        this._pen.EndCap = System.Drawing.Drawing2D.LineCap.Triangle;
                        break;
                    default:
                        this._pen.EndCap = System.Drawing.Drawing2D.LineCap.NoAnchor;
                        break;
                }
                this.Refresh();
            }
        }


        [Category("Electrical Symbols"), Description("Get or set the Width of the Line")]
        public int ExtenderWidth
        {
            get { return this._extenderWidth; }
            set
            {
                if (value <= 0) return;
                this._extenderWidth = value;
                this._pen.Width = value;
                this.Refresh();
            }
        }

        [Category("Electrical Symbols"), Description("Get or set the Width of the Line"), DefaultValueAttribute(false)]
        public virtual bool Fill
        {
            get { return this._fillGradiant; }
            set
            {
                this._fillGradiant = value;
                Refresh();
            }
        }

        [Category("Electrical Symbols"), Description("Get or set the style of the symbol"), DefaultValueAttribute(true)]
        public virtual SymbolStyle Style
        {
            get { return this._style; }
            set
            {
                this._style = value;
                this.Refresh();
            }
        }


        [Category("Electrical Symbols"), Description("Get or set the State"), DefaultValueAttribute(SymbolState.Off)]
        public virtual SymbolState State
        {
            get { return this._state; }
            set
            {
                this._state = value;
                switch (this._state)
                {
                    case SymbolState.Disconnected:
                        this._brush = System.Drawing.Brushes.Blue;
                        this._pen = System.Drawing.Pens.Blue;
                        break;
                    case SymbolState.On:
                    case SymbolState.Default:
                        this._brush = System.Drawing.Brushes.Lime;
                        this._pen = System.Drawing.Pens.Lime;
                        break;

                    case SymbolState.Off:
                        this._brush = System.Drawing.Brushes.Black;
                        this._pen = System.Drawing.Pens.Black;
                        break;
                    case SymbolState.Error:
                        this._brush = System.Drawing.Brushes.Red;
                        this._pen = System.Drawing.Pens.Red;
                        break;
                    case SymbolState.Level1_Alarm:
                        this._brush = System.Drawing.Brushes.Gold;
                        this._pen = System.Drawing.Pens.Gold;
                        break;
                    case SymbolState.Level2_Alarm:
                        this._brush = System.Drawing.Brushes.Orange;
                        this._pen = System.Drawing.Pens.Orange;
                        break;
                }
                //this._brush = this._state == SymbolState.On ? System.Drawing.Brushes.Red : System.Drawing.Brushes.Lime;
                //this._pen = this._state == SymbolState.On ? System.Drawing.Pens.Red : System.Drawing.Pens.Lime;
                this.Refresh();
            }
        }



        [Category("Electrical Symbols"), Description("Get or set the Group ID"), DefaultValueAttribute(1)]
        public virtual System.String GroupID
        {
            get; set;
        }

        public SymbolOrientation SymbolOrientation
        {
            get { return this._orientation; }
            set
            {
                this._orientation = value;
                Refresh();
            }
        }

        #endregion




    }

    #region Enums
    public enum SymbolStyle
    {
        Default = 0,
        Advance = 1
    }

    public enum SymbolDirection
    {
        Left = 0,
        Top = 1,
        Right = 2,
        Bottom = 3
    }

    public enum SymbolOrientation
    {
        Horizontal,
        Verticle
    }

    public enum ConnectHands
    {
        None = 0,
        LefOrToptHand = 1,
        RightOrBottomHand = 2,
        Both = 3
    }

    public enum SymbolState
    {
        Disconnected,//未連線/Gray
        On,//正常/上腺/Lime
        Off,//關機/下線/Black
        Error,//故障/Red
        Level1_Alarm,//警報1/
        Level2_Alarm,//警報2/
        Default //預設第二種ON模式
    }

    public enum DashStyle
    {
        Dash,
        DashDot,
        DashDotDot,
        Dot,
        Solid
    }
    public enum LineCap
    {
        ArrowAnchor,
        DiamondAnchor,
        Flat,
        NoAnchor,
        Round,
        RoundAnchor,
        Square,
        SquareAnchor,
        Triangle
    }


    #endregion Enums
}
