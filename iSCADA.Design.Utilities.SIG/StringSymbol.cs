﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iSCADA.Design.Utilities.SIG
{
    public class StringSymbol : SIGSymbol
    {
        protected override void OnPainting(PaintEventArgs e)
        {
            if (this.SymbolOrientation == SymbolOrientation.Horizontal)
            {
                switchsize = this.Width / 3;
            
            }
            else
            {
                switchsize = this.Height / 3;
            }


            StartPaint(e);
        }

        private void StartPaint(PaintEventArgs e)
        {
            if (!string.IsNullOrEmpty(this._innerText))
            {
                Point p = new Point(this.Width / 2, this.Height / 2);
                Font f = new Font(this.Font.FontFamily, switchsize * 1.5f, this.Font.Style);
                StringFormat sf = new StringFormat();
                sf.LineAlignment = StringAlignment.Center;
                sf.Alignment = StringAlignment.Center;

                if (this.SymbolOrientation == SymbolOrientation.Verticle)
                {
                    if (this.Reversion)
                    {
                        e.Graphics.TranslateTransform(p.X, p.Y);
                        e.Graphics.RotateTransform(180);
                        e.Graphics.TranslateTransform(-p.X, -p.Y);
                    }

                }
                else
                {
                    sf.FormatFlags = StringFormatFlags.DirectionVertical;
                    if (this.Reversion)
                    {
                        e.Graphics.TranslateTransform(p.X, p.Y);
                        e.Graphics.RotateTransform(180);
                        e.Graphics.TranslateTransform(-p.X, -p.Y);
                    }
                }

                DrawString(e.Graphics, this._innerText, f, p, sf);
            }
        }

        #region Field

        private float switchsize;
        private string _innerText;
        #endregion

        #region Property

        public string InnerText { get { return this._innerText; } set { this._innerText = value; this.Refresh(); } }
        #endregion
    }
}
