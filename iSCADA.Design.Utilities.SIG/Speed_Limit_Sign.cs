﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iSCADA.Design.Utilities.SIG
{
    public class Speed_Limit_Sign : SIGSymbol
    {
        protected override void OnPainting(PaintEventArgs e)
        {
            System.Drawing.Drawing2D.GraphicsPath _gpath = new System.Drawing.Drawing2D.GraphicsPath();

            float switchsize = 0;

            if (this.SymbolOrientation == SymbolOrientation.Verticle)
            {
                switchsize = this.Height / 4;
                SetVertical(switchsize);
            }
            else
            {
                switchsize = this.Width / 4;
                SetHorizontal(switchsize);
            }
            SetReversion(e);
            StartDraw(e, _gpath);
        }

   

        private void SetHorizontal(float switchsize)
        {
            this.Height = (int)switchsize * 2;

            P1 = new Point((int)(switchsize * 3.75f), (int)(switchsize * 0.75f));
            P2 = new Point(P1.X, (int)(switchsize * 1.25f));
            P3 = new Point(P1.X, (P1.Y + P2.Y) / 2);
            P4 = new Point((int)(switchsize * 1.75f), P3.Y);
            P6 = new Point((int)(switchsize * 0.1f), P4.Y);
            P5 = new Point((P4.X + P6.X) / 2, (P4.Y - (Math.Abs(P4.X - P6.X) /2 )));
            P7 = new Point(P5.X, (P4.Y + (Math.Abs(P4.X - P6.X) / 2)));

        }

        private void SetVertical(float switchsize)
        {
            this.Width = (int)switchsize * 2;
            P1 = new Point((int)(switchsize * 0.75f),(int)(switchsize * 3.75f) );
            P2 = new Point((int)(switchsize * 1.25f),P1.Y);
            P3 = new Point(((P1.X + P2.X) / 2), P1.Y);
            P4 = new Point(P3.X, (int)(switchsize * 1.75f));
            P6 = new Point(P4.X, (int)(switchsize * 0.1f));
            P5 = new Point((P4.X- (Math.Abs(P4.Y - P6.Y) / 2)), (P4.Y + P6.Y) / 2);
            P7 = new Point((P4.X + (Math.Abs(P4.Y - P6.Y)) / 2),P5.Y);

        }
        private void SetReversion(PaintEventArgs e)
        {
            if (this.Reversion)
            {
                e.Graphics.TranslateTransform(this.Width / 2, this.Height / 2);
                e.Graphics.RotateTransform(180);
                e.Graphics.TranslateTransform(this.Width / -2, this.Height / -2);
                e.Graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            }
        }
        private void StartDraw(PaintEventArgs e, GraphicsPath _gpath)
        {
            DrawLine(e.Graphics, P1, P2);
            DrawLine(e.Graphics, P3, P4);
            DrawLine(e.Graphics, P4, P5);
            DrawLine(e.Graphics, P5, P6);
            DrawLine(e.Graphics, P6, P7);
            DrawLine(e.Graphics, P7, P4);
        }


        #region Field
        // 共有六個線，七個點
        Point P1, P2, P3, P4, P5, P6, P7;

        #endregion
    }
}
