﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iSCADA.Design.Utilities.SIG
{
    public class Tram_Traffic_Light : SIGSymbol
    {

        protected override void OnPainting(PaintEventArgs e)
        {
            System.Drawing.Drawing2D.GraphicsPath _gpath = new System.Drawing.Drawing2D.GraphicsPath();

            float switchsize = 0;

            if (this.SymbolOrientation == SymbolOrientation.Verticle)
            {
                switchsize = this.Height / 4;
                SetVertical(switchsize);
            }
            else
            {
                switchsize = this.Width / 4;
                SetHorizontal(switchsize);
            }

            SetReversion(e);
            StartDraw(e, _gpath);
        }

        private void SetHorizontal(float switchsize)
        {
            this.Height = (int)switchsize * 2;

            L1X1 = (switchsize * 3.75f);
            L1Y1 = (switchsize * 0.75f);
            L1X2 = L1X1;
            L1Y2 = (switchsize * 1.25f);

            L2X1 = (switchsize * 1.75f);
            L2Y1 = (switchsize * 1f);
            L2X2 = L2Y2 = (switchsize * 3.75f);
            L2Y2 = L2Y1;

            L3X1 = (switchsize * 0.5f);
            L3Y1 = L2Y1 / 2;
            L3X2 = (switchsize * 1.25f);
            L3Y2 = L3Y1;

            L4X1 = (switchsize * 0.5f);
            L4Y1 = L2Y1 * 1.5f;
            L4X2 = (switchsize * 1.25f);
            L4Y2 = L4Y1;

            L5X1 = (float)(switchsize * (1.75/ 2f)) ;
            L5Y1 = L3Y1;
            L5X2 = L5X1;
            L5Y2 = L4Y1;


            Arc1X1 = L2X1 - switchsize;
            Arc1Y1 = L2Y1 / 2;
            Arc1Width = switchsize;
            Arc1Height = switchsize;
            Arc1StartAngle = 270;
            Arc1SweepAngle = 180;

            Arc2X1 = 0;
            Arc2Y1 = L2Y1 / 2;
            Arc2Width = switchsize;
            Arc2Height = switchsize;
            Arc2StartAngle = 90;
            Arc2SweepAngle = 180;

            C1Diameter = switchsize * 0.75f;
            C1X1 = (L5X1 - C1Diameter) * 0.5f;
            C1Y1 = Arc2Y1 * 1.25f;


            C2Diameter = C1Diameter;
            C2X1 = L5X1 + ((L5X1 - C1Diameter) * 0.5f) ;
            C2Y1 = Arc2Y1 * 1.25f;
       

            Rec1.X = (int)C1X1;
            Rec1.Y = (int)(C1Y1 * 1.5f);
            Rec1.Width = (int)C1Diameter;
            Rec1.Height = (int)(C1Y1 * 0.25f);


            Rec2.X = (int)C1X1;
            Rec2.Y = (int)(C1Y1 + C1Diameter / 2.5);
            Rec2.Width = (int)C1Diameter;
            Rec2.Height = (int)(C1X1 * 0.25f);

            Rec2.X = (int)(C1X1 + C1Diameter / 2.5);
            Rec2.Y = (int)C1Y1;
            Rec2.Width = (int)(C1Y1 * 0.25f);
            Rec2.Height = (int)C1Diameter;

            TriangleAngle = 270;
        }

        private void SetVertical(float switchsize)
        {
            this.Width = (int)switchsize * 2;

            L1X1 = (switchsize * 0.75f);
            L1Y1 = (switchsize * 2.75f);
            L1X2 = (switchsize * 1.25f);
            L1Y2 = L1Y1;

            L2X1 = (switchsize * 1f);
            L2Y1 = (switchsize * 1.75f);
            L2X2 = L2X1;
            L2Y2 = (switchsize * 2.75f);

            L3X1 = L2X1 / 2;
            L3Y1 = (switchsize * 0.5f);
            L3X2 = L2X2 / 2;
            L3Y2 = (switchsize * 1.25f);

            L4X1 = L2X1 * 1.5F;
            L4Y1 = (switchsize * 0.5f);
            L4X2 = L2X2 * 1.5F;
            L4Y2 = (switchsize * 1.25f);

            L5X1 = L3X1;
            L5Y1 = (float)(switchsize * (1.75 / 2f)); ;
            L5X2 = L4X1;
            L5Y2 = L5Y1;


            Arc1X1 = L2X1 / 2;
            Arc1Y1 = L2Y1 - switchsize;
            Arc1Width = switchsize;
            Arc1Height = switchsize;
            Arc1StartAngle = 0;
            Arc1SweepAngle = 180;

            Arc2X1 = L2X1 / 2;
            Arc2Y1 = 0;
            Arc2Width = switchsize;
            Arc2Height = switchsize;
            Arc2StartAngle = 180;
            Arc2SweepAngle = 180;

            C1Diameter = switchsize * 0.75f;
            C1X1 = Arc2X1 * 1.25f;
            C1Y1 = (L5Y1 - C1Diameter) * 0.5f;

            C2Diameter = C1Diameter;
            C2X1 = Arc2X1 * 1.25f;
            C2Y1 = L5Y1 + (L5Y1 - C1Diameter) * 0.5f;

            Rec1.X = (int)(C1X1 * 1.5f);
            Rec1.Y = (int)C1Y1;
            Rec1.Width = (int)(C1X1 * 0.25f);
            Rec1.Height = (int)C1Diameter;


            Rec2.X = (int)C1X1;
            Rec2.Y = (int)(C1Y1 + C1Diameter / 2.5);
            Rec2.Width = (int)C1Diameter;
            Rec2.Height = (int)(C1X1 * 0.25f);

            TriangleAngle = 0;
        }

        private void SetReversion(PaintEventArgs e)
        {
            if (this.Reversion)
            {
                e.Graphics.TranslateTransform(this.Width / 2, this.Height / 2);
                e.Graphics.RotateTransform(180);
                e.Graphics.TranslateTransform(this.Width / -2, this.Height / -2);
                e.Graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            }
        }

        private void StartDraw(PaintEventArgs e, System.Drawing.Drawing2D.GraphicsPath _gpath)
        {

            _gpath.AddLine(L1X1, L1Y1, L1X2, L1Y2);
            _gpath.CloseFigure();
            _gpath.AddLine(L2X1, L2Y1, L2X2, L2Y2);
            _gpath.CloseFigure();
            _gpath.AddLine(L3X1, L3Y1, L3X2, L3Y2);
            _gpath.CloseFigure();
            _gpath.AddLine(L4X1, L4Y1, L4X2, L4Y2);
            _gpath.CloseFigure();
            _gpath.AddLine(L5X1, L5Y1, L5X2, L5Y2);
            _gpath.CloseFigure();

            //畫線
            DrawPath(e.Graphics, _gpath);
            //上圓弧
            DrawArc(e.Graphics, Arc1X1, Arc1Y1, Arc1Width, Arc1Height, Arc1StartAngle, Arc1SweepAngle);
            //下圓弧
            DrawArc(e.Graphics, Arc2X1, Arc2Y1, Arc2Width, Arc2Height, Arc2StartAngle, Arc2SweepAngle);

            //中間上圓
            DrawArc(e.Graphics, C1X1, C1Y1, C1Diameter);

            //中間下圓
            DrawArc(e.Graphics, C2X1, C2Y1, C2Diameter);

            //中間上圓十字
            FillRectangles(e.Graphics, new Rectangle[] { Rec1, Rec2 });

            //中間下圓三角
            DrawRegularTriangle(e.Graphics, new Point((int)(C2X1 + C2Diameter / 2), (int)(C2Y1 + C2Diameter / 2)), (int)C2Diameter / 2, TriangleAngle);
        }


        #region Field

        //共有五條線，每條線有兩個點，起點終點
        float L1X1, L1Y1, L1X2, L1Y2;
        float L2X1, L2Y1, L2X2, L2Y2;
        float L3X1, L3Y1, L3X2, L3Y2;
        float L4X1, L4Y1, L4X2, L4Y2;
        float L5X1, L5Y1, L5X2, L5Y2;

        //共有兩圓弧
        float Arc1X1, Arc1Y1, Arc1Width, Arc1Height, Arc1StartAngle, Arc1SweepAngle;
        float Arc2X1, Arc2Y1, Arc2Width, Arc2Height, Arc2StartAngle, Arc2SweepAngle;

        //中間有兩個圓
        float C1X1, C1Y1, C1Diameter;
        float C2X1, C2Y1, C2Diameter;

        //上圓十字架
        Rectangle Rec1;
        Rectangle Rec2;

        //下圓三角形角度
        int TriangleAngle;

        #endregion

  
    }


}
