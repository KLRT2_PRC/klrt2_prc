﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICSC.Database.MSSQL
{
    public class DbOp
    {
        public static int TestConnect(string serverIP, string dbName, string dbUser, string dbPwd, ref string logMsg)
        {
            StringBuilder stringBuilder = new StringBuilder();
            SQLDB sQLDB = new SQLDB(0, serverIP, dbName, dbUser, dbPwd);
            logMsg = string.Empty;
            int num = sQLDB.OpenDB();
            if (num == SQLDB.DB_ACT_TRUE)
            {
                sQLDB.CloseDB();
            }
            else
            {
                LogHandler.AppendLine(stringBuilder, "DB Connect is fail (" + sQLDB.GetLastError() + ")");
            }
            logMsg = stringBuilder.ToString();
            return num;
        }

        public static int ExecuteSP(string serverIP, string dbName, string dbUser, string dbPwd, string sqlCmd, bool detailMsg, ref string logMsg)
        {
            StringBuilder stringBuilder = new StringBuilder();
            SQLDB sQLDB = new SQLDB(0, serverIP, dbName, dbUser, dbPwd);
            logMsg = string.Empty;
            int num = sQLDB.OpenDB();
            if (num == SQLDB.DB_ACT_TRUE)
            {
                num = sQLDB.ExecuteSqlSP(sqlCmd);
                if (num < 0)
                {
                    LogHandler.AppendLine(stringBuilder, sqlCmd + " = " + num.ToString());
                    stringBuilder.AppendLine(sQLDB.GetLastError());
                }
                else if (detailMsg)
                {
                    LogHandler.AppendLine(stringBuilder, sqlCmd + " = " + num.ToString());
                }
            }
            else
            {
                LogHandler.AppendLine(stringBuilder, sqlCmd + " = " + num.ToString());
            }
            sQLDB.CloseDB();
            logMsg = stringBuilder.ToString();
            return num;
        }

        public static int ExecuteSql(string serverIP, string dbName, string dbUser, string dbPwd, ref string sqlCmd, bool detailMsg, ref string logMsg)
        {
            StringBuilder stringBuilder = new StringBuilder();
            SQLDB sQLDB = new SQLDB(0, serverIP, dbName, dbUser, dbPwd);
            logMsg = string.Empty;
            int num = sQLDB.OpenDB();
            if (num == SQLDB.DB_ACT_TRUE)
            {
                num = sQLDB.ExecuteSqlCmd(ref sqlCmd);
                if (num < 0)
                {
                    LogHandler.AppendLine(stringBuilder, sqlCmd + " = " + num.ToString());
                    stringBuilder.AppendLine(sQLDB.GetLastError());
                }
                else if (detailMsg)
                {
                    LogHandler.AppendLine(stringBuilder, sqlCmd + " = " + num.ToString());
                }
            }
            else
            {
                LogHandler.AppendLine(stringBuilder, sqlCmd + " = " + num.ToString());
            }
            sQLDB.CloseDB();
            logMsg = stringBuilder.ToString();
            return num;
        }

        public static int QuerySql(string serverIP, string dbName, string dbUser, string dbPwd, ref string sqlCmd, ref DataTable dt, bool detailMsg, ref string logMsg)
        {
            StringBuilder stringBuilder = new StringBuilder();
            SQLDB sQLDB = new SQLDB(0, serverIP, dbName, dbUser, dbPwd);
            logMsg = string.Empty;
            int num = sQLDB.OpenDB();
            if (num == SQLDB.DB_ACT_TRUE)
            {
                num = sQLDB.QuerySqlResult(ref sqlCmd, ref dt);
                if (num < 0)
                {
                    LogHandler.AppendLine(stringBuilder, sqlCmd + " = " + num.ToString());
                    stringBuilder.AppendLine(sQLDB.GetLastError());
                }
                else if (detailMsg)
                {
                    LogHandler.AppendLine(stringBuilder, sqlCmd + " = " + num.ToString());
                }
            }
            else
            {
                LogHandler.AppendLine(stringBuilder, sqlCmd + " = " + num.ToString());
            }
            sQLDB.CloseDB();
            logMsg = stringBuilder.ToString();
            return num;
        }

        public static int ExecuteSqlList(string serverIP, string dbName, string dbUser, string dbPwd, List<string> sqlList, bool detailMsg, ref string logMsg)
        {
            int num = 0;
            bool flag = true;
            StringBuilder stringBuilder = new StringBuilder();
            SQLDB sQLDB = new SQLDB(0, serverIP, dbName, dbUser, dbPwd);
            logMsg = string.Empty;
            string str = string.Empty;
            int num2 = sQLDB.OpenDB();
            int result;
            if (num2 == SQLDB.DB_ACT_TRUE)
            {
                num2 = sQLDB.DoBegin();
                if (num2 < 0)
                {
                    num = num2;
                    LogHandler.AppendLine(stringBuilder, "DoBegin Error: " + sQLDB.GetLastError());
                }
                else if (detailMsg)
                {
                    LogHandler.AppendLine(stringBuilder, "DoBegin Start");
                }
                for (int i = 0; i < sqlList.Count; i++)
                {
                    str = sqlList[i];
                    num2 = sQLDB.ExecuteSqlCmd(ref str);
                    if (num2 < 0)
                    {
                        num = num2;
                        flag = false;
                        LogHandler.AppendLine(stringBuilder, str + " = " + num2.ToString());
                        stringBuilder.AppendLine(sQLDB.GetLastError());
                    }
                    else if (!flag || detailMsg)
                    {
                        LogHandler.AppendLine(stringBuilder, str + " = " + num2.ToString());
                    }
                }
                if (flag)
                {
                    num2 = sQLDB.DoCommit();
                    if (num2 < 0)
                    {
                        num = num2;
                        LogHandler.AppendLine(stringBuilder, "DoCommit Error: " + sQLDB.GetLastError());
                    }
                    else if (detailMsg)
                    {
                        LogHandler.AppendLine(stringBuilder, "DoCommit Ok");
                    }
                }
                else
                {
                    num2 = sQLDB.DoRollBack();
                    if (num2 < 0)
                    {
                        num = num2;
                        LogHandler.AppendLine(stringBuilder, "DoRollBack Error: " + sQLDB.GetLastError());
                        sQLDB.CloseDB();
                        logMsg = stringBuilder.ToString();
                        result = num;
                        return result;
                    }
                    if (detailMsg)
                    {
                        LogHandler.AppendLine(stringBuilder, "DoRollBack Ok");
                        sQLDB.CloseDB();
                        logMsg = stringBuilder.ToString();
                        result = -99;
                        return result;
                    }
                }
            }
            else
            {
                LogHandler.AppendLine(stringBuilder, sQLDB.GetLastError());
            }
            sQLDB.CloseDB();
            logMsg = stringBuilder.ToString();
            result = num;
            return result;
        }

        public static int ExecuteSqlListWithoutRollback(string serverIP, string dbName, string dbUser, string dbPwd, List<string> sqlList, bool detailMsg, ref string logMsg)
        {
            int result = 0;
            StringBuilder stringBuilder = new StringBuilder();
            SQLDB sQLDB = new SQLDB(0, serverIP, dbName, dbUser, dbPwd);
            logMsg = string.Empty;
            string str = string.Empty;
            int num = sQLDB.OpenDB();
            if (num == SQLDB.DB_ACT_TRUE)
            {
                for (int i = 0; i < sqlList.Count; i++)
                {
                    str = sqlList[i];
                    num = sQLDB.ExecuteSqlCmd(ref str);
                    if (num < 0)
                    {
                        result = num;
                        LogHandler.AppendLine(stringBuilder, str + " = " + num.ToString());
                        stringBuilder.AppendLine(sQLDB.GetLastError());
                    }
                    else if (detailMsg)
                    {
                        LogHandler.AppendLine(stringBuilder, str + " = " + num.ToString());
                    }
                }
            }
            else
            {
                LogHandler.AppendLine(stringBuilder, sQLDB.GetLastError());
            }
            sQLDB.CloseDB();
            logMsg = stringBuilder.ToString();
            return result;
        }

        public static int QuerySqlList(string serverIP, string dbName, string dbUser, string dbPwd, List<string> sqlList, ref DataTable[] dts, bool detailMsg, ref string logMsg)
        {
            StringBuilder stringBuilder = new StringBuilder();
            SQLDB sQLDB = new SQLDB(0, serverIP, dbName, dbUser, dbPwd);
            logMsg = string.Empty;
            string str = string.Empty;
            int num = sQLDB.OpenDB();
            if (num == SQLDB.DB_ACT_TRUE)
            {
                for (int i = 0; i < sqlList.Count; i++)
                {
                    str = sqlList[i];
                    num = sQLDB.QuerySqlResult(ref str, ref dts[i]);
                    if (num < 0)
                    {
                        LogHandler.AppendLine(stringBuilder, str + " = " + num.ToString());
                        stringBuilder.AppendLine(sQLDB.GetLastError());
                    }
                    else if (detailMsg)
                    {
                        LogHandler.AppendLine(stringBuilder, str + " = " + num.ToString());
                    }
                }
            }
            else
            {
                LogHandler.AppendLine(stringBuilder, sQLDB.GetLastError());
            }
            sQLDB.CloseDB();
            logMsg = stringBuilder.ToString();
            return num;
        }
    }
}
