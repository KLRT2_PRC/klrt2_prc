﻿using PRC.MyClass;
using PRC.Tools;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PRC
{
    public partial class Form_EventLog_OperatorEvent : Form
    {
        public Form_EventLog_OperatorEvent()
        {
            InitializeComponent();
        }

        public Form_EventLog_OperatorEvent(PermissionObject permission) : this()
        {
            btn_SearchEventLog.Enabled = permission.CanRead;
        }

        private void Form_EventLog_OperatorEvent_Load(object sender, EventArgs e)
        {
          
        }

        private void btn_SearchEventLog_Click(object sender, EventArgs e)
        {
            if (ValidDatesInput())
            {

                EventLog_OperatorQueryDto dto = new EventLog_OperatorQueryDto();
                try
                {
                    string url = string.Format(@"http://localhost:48965/api/EventLog_OperatorLog?&StartTime={0:yyyy-MM-dd}&EndTime={1:yyyy-MM-dd}", dtp_StartDate.Value, dtp_EndDate.Value);
                    List<Sys_OperatorRecord> result = WebApiCaller.Json_GetModeByGet<List<Sys_OperatorRecord>>(url).ContentBody;
                    if (result != null && result.Count > 0)
                    {
                        dataGridView1.DataSource = result;
                        MessageBox.Show("查詢完畢!");
                    }
                    else
                    {
                        dataGridView1.DataSource = new List<Sys_OperatorRecord>();

                        MessageBox.Show("查無資料!");
                    }

                    //   WriteLog.WriteLogByWebApi(this.Name, "Query", EventLog_OperatorNotifyDto.LogType.INFO);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(string.Format("查詢失敗! 失敗訊息:{0}", ex.Message));
                    //   WriteLog.WriteLogByWebApi(this.Name, "Query", EventLog_OperatorNotifyDto.LogType.ERROR);
                }
            }
            else
            {
                MessageBox.Show("起始時間必須小於或等於結束時間!!!");
            }

        }


        private void btn_SearchEventLog_EnabledChanged(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            if (btn.Enabled)
            {
                btn.FlatStyle = FlatStyle.Flat;
                btn.ForeColor = Color.FromArgb(100, 128, 255, 255);
                btn.BackColor = Color.Transparent;
                btn.FlatAppearance.BorderColor = Color.FromArgb(100, 128, 255, 255);

            }
            else
            {
                btn.FlatStyle = FlatStyle.Standard;
            }
        }


        private bool ValidDatesInput()
        {
            return DateTime.Parse(dtp_StartDate.Value.ToShortDateString()) < DateTime.Parse(dtp_EndDate.Value.ToShortDateString());
        }



     
    }

    public class EventLog_OperatorQueryDto
    {
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
    }

    public class Sys_OperatorRecord
    {
        public DateTime EventTime { get; set; }
        public string UserID { get; set; }
        public string ProgID_EN { get; set; }
        public string EventType { get; set; }
        public string EventMessage { get; set; }
    }

}
