﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PRC
{
    static class Program
    {
        /// <summary>
        /// 應用程式的主要進入點。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainFrame());
            //Application.Run(new FormT1());
            //Application.Run(new FormTest());
            // Application.Run(new PRCProcessSet());
            //Application.Run(new EquipProcess());
            //Application.Run(new PRCAnalysis());
            //Application.Run(new PRCAnalysisSet());
        }
    }
}
