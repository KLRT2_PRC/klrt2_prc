﻿namespace PRC
{
    partial class Form_ElecTSS9
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label35 = new System.Windows.Forms.Label();
            this.switch05UC1 = new iSCADA.Design.Utilities.Electrical.Switch05UC();
            this.switch03UC3 = new iSCADA.Design.Utilities.Electrical.Switch03UC();
            this.label34 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.lineUC14 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC13 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.switch05UC3 = new iSCADA.Design.Utilities.Electrical.Switch05UC();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.switch05UC2 = new iSCADA.Design.Utilities.Electrical.Switch05UC();
            this.switch03UC2 = new iSCADA.Design.Utilities.Electrical.Switch03UC();
            this.circleUC4 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC3 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC2 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.airCircuitBreaker4 = new iSCADA.Design.Utilities.Electrical.AirCircuitBreaker();
            this.airCircuitBreaker3 = new iSCADA.Design.Utilities.Electrical.AirCircuitBreaker();
            this.airCircuitBreaker2 = new iSCADA.Design.Utilities.Electrical.AirCircuitBreaker();
            this.lineUC10 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.airCircuitBreaker1 = new iSCADA.Design.Utilities.Electrical.AirCircuitBreaker();
            this.transformer02UC1 = new iSCADA.Design.Utilities.Electrical.Transformer02UC();
            this.circleUC1 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.label17 = new System.Windows.Forms.Label();
            this.lineUC12 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label20 = new System.Windows.Forms.Label();
            this.switch02UC5 = new iSCADA.Design.Utilities.Electrical.Switch02UC();
            this.transformer01UC1 = new iSCADA.Design.Utilities.Electrical.Transformer01UC();
            this.label19 = new System.Windows.Forms.Label();
            this.lineUC11 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label18 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.retangleUC6 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC5 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.switch02UC4 = new iSCADA.Design.Utilities.Electrical.Switch02UC();
            this.switch02UC3 = new iSCADA.Design.Utilities.Electrical.Switch02UC();
            this.switch02UC2 = new iSCADA.Design.Utilities.Electrical.Switch02UC();
            this.lineUC8 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.switch02UC1 = new iSCADA.Design.Utilities.Electrical.Switch02UC();
            this.lineUC7 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC4 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC6 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC5 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.switch04UC2 = new iSCADA.Design.Utilities.Electrical.Switch04UC();
            this.switch04UC1 = new iSCADA.Design.Utilities.Electrical.Switch04UC();
            this.lineUC4 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC3 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC2 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC1 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label2 = new System.Windows.Forms.Label();
            this.retangleUC2 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label1 = new System.Windows.Forms.Label();
            this.retangleUC1 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC3 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC7 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC8 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC9 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC10 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC11 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC12 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label35.Location = new System.Drawing.Point(784, 429);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(41, 34);
            this.label35.TabIndex = 1980;
            this.label35.Text = "24kV\r\n630A";
            // 
            // switch05UC1
            // 
            this.switch05UC1.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.switch05UC1.ExtenderWidth = 2;
            this.switch05UC1.Fill = false;
            this.switch05UC1.GroupID = null;
            this.switch05UC1.Location = new System.Drawing.Point(610, 701);
            this.switch05UC1.Name = "switch05UC1";
            this.switch05UC1.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.switch05UC1.Size = new System.Drawing.Size(23, 61);
            this.switch05UC1.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.switch05UC1.TabIndex = 1957;
            // 
            // switch03UC3
            // 
            this.switch03UC3.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.switch03UC3.ExtenderWidth = 2;
            this.switch03UC3.Fill = false;
            this.switch03UC3.GroupID = null;
            this.switch03UC3.Location = new System.Drawing.Point(626, 615);
            this.switch03UC3.Name = "switch03UC3";
            this.switch03UC3.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.switch03UC3.Size = new System.Drawing.Size(26, 90);
            this.switch03UC3.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.switch03UC3.TabIndex = 1979;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("微軟正黑體", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label34.Location = new System.Drawing.Point(711, 737);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(102, 36);
            this.label34.TabIndex = 1978;
            this.label34.Text = "TSS站內及號誌\r\n通信低壓用電";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("微軟正黑體", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label31.Location = new System.Drawing.Point(421, 78);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(149, 24);
            this.label31.TabIndex = 1976;
            this.label31.Text = "TSS9輕軌設備室";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label30.Location = new System.Drawing.Point(659, 102);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(100, 34);
            this.label30.TabIndex = 1975;
            this.label30.Text = "FROM TPC\r\n3 Ø 3W 22.8kV\r\n";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label29.Location = new System.Drawing.Point(782, 102);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(100, 34);
            this.label29.TabIndex = 1974;
            this.label29.Text = "FROM TPC\r\n3 Ø 3W 22.8kV\r\n";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label28.Location = new System.Drawing.Point(482, 782);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(49, 15);
            this.label28.TabIndex = 1973;
            this.label28.Text = "\"MC41\"";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label27.Location = new System.Drawing.Point(555, 816);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(43, 30);
            this.label27.TabIndex = 1972;
            this.label27.Text = "NT7C\r\n4000A";
            // 
            // lineUC14
            // 
            this.lineUC14.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC14.ExtenderWidth = 2;
            this.lineUC14.Fill = false;
            this.lineUC14.GroupID = null;
            this.lineUC14.Location = new System.Drawing.Point(629, 760);
            this.lineUC14.Name = "lineUC14";
            this.lineUC14.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC14.Size = new System.Drawing.Size(2, 90);
            this.lineUC14.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC14.TabIndex = 1971;
            // 
            // lineUC13
            // 
            this.lineUC13.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC13.ExtenderWidth = 2;
            this.lineUC13.Fill = false;
            this.lineUC13.GroupID = null;
            this.lineUC13.Location = new System.Drawing.Point(531, 760);
            this.lineUC13.Name = "lineUC13";
            this.lineUC13.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC13.Size = new System.Drawing.Size(2, 90);
            this.lineUC13.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC13.TabIndex = 1970;
            // 
            // switch05UC3
            // 
            this.switch05UC3.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.switch05UC3.ExtenderWidth = 2;
            this.switch05UC3.Fill = false;
            this.switch05UC3.GroupID = null;
            this.switch05UC3.Location = new System.Drawing.Point(531, 781);
            this.switch05UC3.Name = "switch05UC3";
            this.switch05UC3.Size = new System.Drawing.Size(100, 37);
            this.switch05UC3.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.switch05UC3.TabIndex = 1968;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label26.Location = new System.Drawing.Point(570, 701);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(43, 30);
            this.label26.TabIndex = 1967;
            this.label26.Text = "NT7B\r\n4000A";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label25.Location = new System.Drawing.Point(581, 648);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(43, 30);
            this.label25.TabIndex = 1966;
            this.label25.Text = "QA72\r\n4000A";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label24.Location = new System.Drawing.Point(470, 701);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(43, 30);
            this.label24.TabIndex = 1965;
            this.label24.Text = "NT7A\r\n4000A";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label23.Location = new System.Drawing.Point(484, 648);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(43, 30);
            this.label23.TabIndex = 1964;
            this.label23.Text = "QA71\r\n4000A";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label22.Location = new System.Drawing.Point(575, 632);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(54, 16);
            this.label22.TabIndex = 1963;
            this.label22.Text = "\"MC22\"";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label21.Location = new System.Drawing.Point(473, 632);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(54, 16);
            this.label21.TabIndex = 1962;
            this.label21.Text = "\"MC21\"";
            // 
            // switch05UC2
            // 
            this.switch05UC2.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.switch05UC2.ExtenderWidth = 2;
            this.switch05UC2.Fill = false;
            this.switch05UC2.GroupID = null;
            this.switch05UC2.Location = new System.Drawing.Point(512, 701);
            this.switch05UC2.Name = "switch05UC2";
            this.switch05UC2.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.switch05UC2.Size = new System.Drawing.Size(23, 61);
            this.switch05UC2.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.switch05UC2.TabIndex = 1959;
            // 
            // switch03UC2
            // 
            this.switch03UC2.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.switch03UC2.ExtenderWidth = 2;
            this.switch03UC2.Fill = false;
            this.switch03UC2.GroupID = null;
            this.switch03UC2.Location = new System.Drawing.Point(528, 615);
            this.switch03UC2.Name = "switch03UC2";
            this.switch03UC2.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.switch03UC2.Size = new System.Drawing.Size(26, 90);
            this.switch03UC2.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.switch03UC2.TabIndex = 1958;
            // 
            // circleUC4
            // 
            this.circleUC4.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC4.ExtenderWidth = 3;
            this.circleUC4.Fill = false;
            this.circleUC4.GroupID = null;
            this.circleUC4.Location = new System.Drawing.Point(818, 627);
            this.circleUC4.Name = "circleUC4";
            this.circleUC4.Size = new System.Drawing.Size(10, 10);
            this.circleUC4.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC4.TabIndex = 1956;
            // 
            // circleUC3
            // 
            this.circleUC3.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC3.ExtenderWidth = 3;
            this.circleUC3.Fill = false;
            this.circleUC3.GroupID = null;
            this.circleUC3.Location = new System.Drawing.Point(698, 627);
            this.circleUC3.Name = "circleUC3";
            this.circleUC3.Size = new System.Drawing.Size(10, 10);
            this.circleUC3.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC3.TabIndex = 1955;
            // 
            // circleUC2
            // 
            this.circleUC2.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC2.ExtenderWidth = 3;
            this.circleUC2.Fill = false;
            this.circleUC2.GroupID = null;
            this.circleUC2.Location = new System.Drawing.Point(756, 627);
            this.circleUC2.Name = "circleUC2";
            this.circleUC2.Size = new System.Drawing.Size(10, 10);
            this.circleUC2.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC2.TabIndex = 1954;
            // 
            // airCircuitBreaker4
            // 
            this.airCircuitBreaker4.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.airCircuitBreaker4.ExtenderWidth = 3;
            this.airCircuitBreaker4.Fill = false;
            this.airCircuitBreaker4.GroupID = null;
            this.airCircuitBreaker4.Location = new System.Drawing.Point(698, 633);
            this.airCircuitBreaker4.Name = "airCircuitBreaker4";
            this.airCircuitBreaker4.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.airCircuitBreaker4.Size = new System.Drawing.Size(17, 91);
            this.airCircuitBreaker4.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.airCircuitBreaker4.TabIndex = 1953;
            // 
            // airCircuitBreaker3
            // 
            this.airCircuitBreaker3.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.airCircuitBreaker3.ExtenderWidth = 3;
            this.airCircuitBreaker3.Fill = false;
            this.airCircuitBreaker3.GroupID = null;
            this.airCircuitBreaker3.Location = new System.Drawing.Point(818, 636);
            this.airCircuitBreaker3.Name = "airCircuitBreaker3";
            this.airCircuitBreaker3.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.airCircuitBreaker3.Size = new System.Drawing.Size(17, 91);
            this.airCircuitBreaker3.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.airCircuitBreaker3.TabIndex = 1952;
            // 
            // airCircuitBreaker2
            // 
            this.airCircuitBreaker2.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.airCircuitBreaker2.ExtenderWidth = 3;
            this.airCircuitBreaker2.Fill = false;
            this.airCircuitBreaker2.GroupID = null;
            this.airCircuitBreaker2.Location = new System.Drawing.Point(756, 633);
            this.airCircuitBreaker2.Name = "airCircuitBreaker2";
            this.airCircuitBreaker2.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.airCircuitBreaker2.Size = new System.Drawing.Size(17, 91);
            this.airCircuitBreaker2.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.airCircuitBreaker2.TabIndex = 1951;
            // 
            // lineUC10
            // 
            this.lineUC10.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC10.ExtenderWidth = 3;
            this.lineUC10.Fill = false;
            this.lineUC10.GroupID = null;
            this.lineUC10.Location = new System.Drawing.Point(686, 630);
            this.lineUC10.Name = "lineUC10";
            this.lineUC10.Size = new System.Drawing.Size(150, 3);
            this.lineUC10.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC10.TabIndex = 1950;
            // 
            // airCircuitBreaker1
            // 
            this.airCircuitBreaker1.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.airCircuitBreaker1.ExtenderWidth = 3;
            this.airCircuitBreaker1.Fill = false;
            this.airCircuitBreaker1.GroupID = null;
            this.airCircuitBreaker1.Location = new System.Drawing.Point(756, 539);
            this.airCircuitBreaker1.Name = "airCircuitBreaker1";
            this.airCircuitBreaker1.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.airCircuitBreaker1.Size = new System.Drawing.Size(17, 91);
            this.airCircuitBreaker1.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.airCircuitBreaker1.TabIndex = 1949;
            // 
            // transformer02UC1
            // 
            this.transformer02UC1.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.transformer02UC1.ExtenderWidth = 3;
            this.transformer02UC1.Fill = false;
            this.transformer02UC1.GroupID = null;
            this.transformer02UC1.Location = new System.Drawing.Point(750, 462);
            this.transformer02UC1.Name = "transformer02UC1";
            this.transformer02UC1.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.transformer02UC1.Size = new System.Drawing.Size(26, 92);
            this.transformer02UC1.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.transformer02UC1.TabIndex = 1948;
            // 
            // circleUC1
            // 
            this.circleUC1.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC1.ExtenderWidth = 3;
            this.circleUC1.Fill = false;
            this.circleUC1.GroupID = null;
            this.circleUC1.Location = new System.Drawing.Point(586, 610);
            this.circleUC1.Name = "circleUC1";
            this.circleUC1.Size = new System.Drawing.Size(8, 8);
            this.circleUC1.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC1.TabIndex = 1947;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label17.Location = new System.Drawing.Point(623, 480);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(77, 80);
            this.label17.TabIndex = 1938;
            this.label17.Text = "RT.\r\n2000kV\r\n22.8kV-\r\n585V / 585V\r\n8%";
            // 
            // lineUC12
            // 
            this.lineUC12.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC12.ExtenderWidth = 3;
            this.lineUC12.Fill = false;
            this.lineUC12.GroupID = null;
            this.lineUC12.Location = new System.Drawing.Point(498, 612);
            this.lineUC12.Name = "lineUC12";
            this.lineUC12.Size = new System.Drawing.Size(150, 3);
            this.lineUC12.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC12.TabIndex = 1945;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label20.Location = new System.Drawing.Point(489, 575);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(58, 17);
            this.label20.TabIndex = 1944;
            this.label20.Text = "\"MCB1\"";
            // 
            // switch02UC5
            // 
            this.switch02UC5.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.switch02UC5.ExtenderWidth = 3;
            this.switch02UC5.Fill = false;
            this.switch02UC5.GroupID = null;
            this.switch02UC5.Location = new System.Drawing.Point(571, 560);
            this.switch02UC5.Name = "switch02UC5";
            this.switch02UC5.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.switch02UC5.Size = new System.Drawing.Size(42, 55);
            this.switch02UC5.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.switch02UC5.TabIndex = 1943;
            // 
            // transformer01UC1
            // 
            this.transformer01UC1.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.transformer01UC1.ExtenderWidth = 3;
            this.transformer01UC1.Fill = false;
            this.transformer01UC1.GroupID = null;
            this.transformer01UC1.Location = new System.Drawing.Point(573, 463);
            this.transformer01UC1.Name = "transformer01UC1";
            this.transformer01UC1.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.transformer01UC1.Size = new System.Drawing.Size(35, 52);
            this.transformer01UC1.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.transformer01UC1.TabIndex = 1942;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label19.Location = new System.Drawing.Point(865, 537);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(60, 17);
            this.label19.TabIndex = 1941;
            this.label19.Text = "控制電源";
            // 
            // lineUC11
            // 
            this.lineUC11.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC11.ExtenderWidth = 3;
            this.lineUC11.Fill = false;
            this.lineUC11.GroupID = null;
            this.lineUC11.Location = new System.Drawing.Point(872, 439);
            this.lineUC11.Name = "lineUC11";
            this.lineUC11.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC11.Size = new System.Drawing.Size(3, 80);
            this.lineUC11.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC11.TabIndex = 1940;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label18.Location = new System.Drawing.Point(783, 480);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(69, 80);
            this.label18.TabIndex = 1939;
            this.label18.Text = "AUX TR.\r\n100kV\r\n22.8kV-\r\n380 / 220V\r\n6%";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label16.Location = new System.Drawing.Point(521, 543);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(59, 17);
            this.label16.TabIndex = 1937;
            this.label16.Text = "\"MG01\"";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label15.Location = new System.Drawing.Point(783, 299);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 34);
            this.label15.TabIndex = 1935;
            this.label15.Text = "24kV\r\n630A";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label14.Location = new System.Drawing.Point(782, 397);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(24, 16);
            this.label14.TabIndex = 1934;
            this.label14.Text = "DS";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label13.Location = new System.Drawing.Point(891, 394);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(24, 16);
            this.label13.TabIndex = 1933;
            this.label13.Text = "DS";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label12.Location = new System.Drawing.Point(722, 348);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(24, 16);
            this.label12.TabIndex = 1932;
            this.label12.Text = "DS";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label11.Location = new System.Drawing.Point(784, 333);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(22, 16);
            this.label11.TabIndex = 1931;
            this.label11.Text = "ES";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label10.Location = new System.Drawing.Point(724, 424);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(22, 16);
            this.label10.TabIndex = 1930;
            this.label10.Text = "ES";
            // 
            // retangleUC6
            // 
            this.retangleUC6.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC6.ExtenderWidth = 3;
            this.retangleUC6.Fill = false;
            this.retangleUC6.GroupID = null;
            this.retangleUC6.Location = new System.Drawing.Point(749, 308);
            this.retangleUC6.Name = "retangleUC6";
            this.retangleUC6.Size = new System.Drawing.Size(28, 20);
            this.retangleUC6.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC6.TabIndex = 1929;
            // 
            // retangleUC5
            // 
            this.retangleUC5.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC5.ExtenderWidth = 3;
            this.retangleUC5.Fill = false;
            this.retangleUC5.GroupID = null;
            this.retangleUC5.Location = new System.Drawing.Point(575, 441);
            this.retangleUC5.Name = "retangleUC5";
            this.retangleUC5.Size = new System.Drawing.Size(28, 20);
            this.retangleUC5.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC5.TabIndex = 1928;
            // 
            // switch02UC4
            // 
            this.switch02UC4.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.switch02UC4.ExtenderWidth = 3;
            this.switch02UC4.Fill = false;
            this.switch02UC4.GroupID = null;
            this.switch02UC4.Location = new System.Drawing.Point(573, 388);
            this.switch02UC4.Name = "switch02UC4";
            this.switch02UC4.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.switch02UC4.Size = new System.Drawing.Size(42, 55);
            this.switch02UC4.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.switch02UC4.TabIndex = 1927;
            // 
            // switch02UC3
            // 
            this.switch02UC3.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.switch02UC3.ExtenderWidth = 3;
            this.switch02UC3.Fill = false;
            this.switch02UC3.GroupID = null;
            this.switch02UC3.Location = new System.Drawing.Point(744, 330);
            this.switch02UC3.Name = "switch02UC3";
            this.switch02UC3.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.switch02UC3.Size = new System.Drawing.Size(42, 55);
            this.switch02UC3.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.switch02UC3.TabIndex = 1926;
            // 
            // switch02UC2
            // 
            this.switch02UC2.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.switch02UC2.ExtenderWidth = 3;
            this.switch02UC2.Fill = false;
            this.switch02UC2.GroupID = null;
            this.switch02UC2.Location = new System.Drawing.Point(744, 388);
            this.switch02UC2.Name = "switch02UC2";
            this.switch02UC2.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.switch02UC2.Size = new System.Drawing.Size(42, 55);
            this.switch02UC2.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.switch02UC2.TabIndex = 1925;
            // 
            // lineUC8
            // 
            this.lineUC8.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC8.ExtenderWidth = 3;
            this.lineUC8.Fill = false;
            this.lineUC8.GroupID = null;
            this.lineUC8.Location = new System.Drawing.Point(558, 385);
            this.lineUC8.Name = "lineUC8";
            this.lineUC8.Size = new System.Drawing.Size(350, 3);
            this.lineUC8.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC8.TabIndex = 1924;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label9.Location = new System.Drawing.Point(538, 299);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(42, 17);
            this.label9.TabIndex = 1923;
            this.label9.Text = "\"GIS\"";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label8.Location = new System.Drawing.Point(613, 429);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 34);
            this.label8.TabIndex = 1922;
            this.label8.Text = "24kV\r\n630A";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label7.Location = new System.Drawing.Point(553, 422);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(22, 16);
            this.label7.TabIndex = 1921;
            this.label7.Text = "ES";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label6.Location = new System.Drawing.Point(613, 397);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(24, 16);
            this.label6.TabIndex = 1920;
            this.label6.Text = "DS";
            // 
            // switch02UC1
            // 
            this.switch02UC1.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.switch02UC1.ExtenderWidth = 3;
            this.switch02UC1.Fill = false;
            this.switch02UC1.GroupID = null;
            this.switch02UC1.Location = new System.Drawing.Point(855, 385);
            this.switch02UC1.Name = "switch02UC1";
            this.switch02UC1.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.switch02UC1.Size = new System.Drawing.Size(42, 55);
            this.switch02UC1.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.switch02UC1.TabIndex = 1919;
            // 
            // lineUC7
            // 
            this.lineUC7.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC7.ExtenderWidth = 3;
            this.lineUC7.Fill = false;
            this.lineUC7.GroupID = null;
            this.lineUC7.Location = new System.Drawing.Point(761, 283);
            this.lineUC7.Name = "lineUC7";
            this.lineUC7.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC7.Size = new System.Drawing.Size(3, 25);
            this.lineUC7.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC7.TabIndex = 1918;
            // 
            // retangleUC4
            // 
            this.retangleUC4.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC4.ExtenderWidth = 3;
            this.retangleUC4.Fill = false;
            this.retangleUC4.GroupID = null;
            this.retangleUC4.Location = new System.Drawing.Point(748, 443);
            this.retangleUC4.Name = "retangleUC4";
            this.retangleUC4.Size = new System.Drawing.Size(28, 20);
            this.retangleUC4.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC4.TabIndex = 1917;
            // 
            // lineUC6
            // 
            this.lineUC6.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC6.ExtenderWidth = 3;
            this.lineUC6.Fill = false;
            this.lineUC6.GroupID = null;
            this.lineUC6.Location = new System.Drawing.Point(831, 264);
            this.lineUC6.Name = "lineUC6";
            this.lineUC6.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC6.Size = new System.Drawing.Size(3, 20);
            this.lineUC6.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC6.TabIndex = 1916;
            // 
            // lineUC5
            // 
            this.lineUC5.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC5.ExtenderWidth = 3;
            this.lineUC5.Fill = false;
            this.lineUC5.GroupID = null;
            this.lineUC5.Location = new System.Drawing.Point(684, 264);
            this.lineUC5.Name = "lineUC5";
            this.lineUC5.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC5.Size = new System.Drawing.Size(3, 20);
            this.lineUC5.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC5.TabIndex = 1915;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label5.Location = new System.Drawing.Point(819, 222);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 34);
            this.label5.TabIndex = 1913;
            this.label5.Text = "24kV\r\n630A";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label4.Location = new System.Drawing.Point(660, 222);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 34);
            this.label4.TabIndex = 1912;
            this.label4.Text = "24kV\r\n630A";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label3.Location = new System.Drawing.Point(538, 222);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 17);
            this.label3.TabIndex = 1911;
            this.label3.Text = "\"HVAS\"";
            // 
            // switch04UC2
            // 
            this.switch04UC2.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.switch04UC2.ExtenderWidth = 2;
            this.switch04UC2.Fill = false;
            this.switch04UC2.GroupID = null;
            this.switch04UC2.Location = new System.Drawing.Point(792, 201);
            this.switch04UC2.Name = "switch04UC2";
            this.switch04UC2.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.switch04UC2.Size = new System.Drawing.Size(21, 55);
            this.switch04UC2.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.switch04UC2.TabIndex = 1910;
            // 
            // switch04UC1
            // 
            this.switch04UC1.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.switch04UC1.ExtenderWidth = 2;
            this.switch04UC1.Fill = false;
            this.switch04UC1.GroupID = null;
            this.switch04UC1.Location = new System.Drawing.Point(698, 203);
            this.switch04UC1.Name = "switch04UC1";
            this.switch04UC1.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.switch04UC1.Size = new System.Drawing.Size(21, 55);
            this.switch04UC1.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.switch04UC1.TabIndex = 1909;
            // 
            // lineUC4
            // 
            this.lineUC4.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC4.ExtenderWidth = 3;
            this.lineUC4.Fill = false;
            this.lineUC4.GroupID = null;
            this.lineUC4.Location = new System.Drawing.Point(716, 151);
            this.lineUC4.Name = "lineUC4";
            this.lineUC4.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC4.Size = new System.Drawing.Size(3, 25);
            this.lineUC4.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC4.TabIndex = 1908;
            // 
            // lineUC3
            // 
            this.lineUC3.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC3.ExtenderWidth = 3;
            this.lineUC3.Fill = false;
            this.lineUC3.GroupID = null;
            this.lineUC3.Location = new System.Drawing.Point(810, 150);
            this.lineUC3.Name = "lineUC3";
            this.lineUC3.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC3.Size = new System.Drawing.Size(3, 25);
            this.lineUC3.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC3.TabIndex = 1907;
            // 
            // lineUC2
            // 
            this.lineUC2.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC2.ExtenderWidth = 3;
            this.lineUC2.Fill = false;
            this.lineUC2.GroupID = null;
            this.lineUC2.Location = new System.Drawing.Point(684, 283);
            this.lineUC2.Name = "lineUC2";
            this.lineUC2.Size = new System.Drawing.Size(150, 3);
            this.lineUC2.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC2.TabIndex = 1906;
            // 
            // lineUC1
            // 
            this.lineUC1.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.DashDot;
            this.lineUC1.ExtenderWidth = 3;
            this.lineUC1.Fill = false;
            this.lineUC1.GroupID = null;
            this.lineUC1.Location = new System.Drawing.Point(684, 261);
            this.lineUC1.Name = "lineUC1";
            this.lineUC1.Size = new System.Drawing.Size(150, 3);
            this.lineUC1.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC1.TabIndex = 1905;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(788, 177);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 21);
            this.label2.TabIndex = 1904;
            this.label2.Text = "MOF";
            // 
            // retangleUC2
            // 
            this.retangleUC2.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.DashDot;
            this.retangleUC2.ExtenderWidth = 3;
            this.retangleUC2.Fill = false;
            this.retangleUC2.GroupID = null;
            this.retangleUC2.Location = new System.Drawing.Point(785, 175);
            this.retangleUC2.Name = "retangleUC2";
            this.retangleUC2.Size = new System.Drawing.Size(53, 28);
            this.retangleUC2.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC2.TabIndex = 1903;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(689, 179);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 21);
            this.label1.TabIndex = 1902;
            this.label1.Text = "MOF";
            // 
            // retangleUC1
            // 
            this.retangleUC1.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.DashDot;
            this.retangleUC1.ExtenderWidth = 3;
            this.retangleUC1.Fill = false;
            this.retangleUC1.GroupID = null;
            this.retangleUC1.Location = new System.Drawing.Point(686, 175);
            this.retangleUC1.Name = "retangleUC1";
            this.retangleUC1.Size = new System.Drawing.Size(53, 28);
            this.retangleUC1.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC1.TabIndex = 1901;
            // 
            // retangleUC3
            // 
            this.retangleUC3.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Dash;
            this.retangleUC3.ExtenderWidth = 3;
            this.retangleUC3.Fill = false;
            this.retangleUC3.GroupID = null;
            this.retangleUC3.Location = new System.Drawing.Point(531, 212);
            this.retangleUC3.Name = "retangleUC3";
            this.retangleUC3.Size = new System.Drawing.Size(408, 80);
            this.retangleUC3.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC3.TabIndex = 1914;
            // 
            // retangleUC7
            // 
            this.retangleUC7.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Dash;
            this.retangleUC7.ExtenderWidth = 3;
            this.retangleUC7.Fill = false;
            this.retangleUC7.GroupID = null;
            this.retangleUC7.Location = new System.Drawing.Point(531, 295);
            this.retangleUC7.Name = "retangleUC7";
            this.retangleUC7.Size = new System.Drawing.Size(408, 180);
            this.retangleUC7.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC7.TabIndex = 1936;
            // 
            // retangleUC8
            // 
            this.retangleUC8.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Dash;
            this.retangleUC8.ExtenderWidth = 3;
            this.retangleUC8.Fill = false;
            this.retangleUC8.GroupID = null;
            this.retangleUC8.Location = new System.Drawing.Point(487, 774);
            this.retangleUC8.Name = "retangleUC8";
            this.retangleUC8.Size = new System.Drawing.Size(180, 70);
            this.retangleUC8.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC8.TabIndex = 1946;
            // 
            // retangleUC9
            // 
            this.retangleUC9.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Dash;
            this.retangleUC9.ExtenderWidth = 3;
            this.retangleUC9.Fill = false;
            this.retangleUC9.GroupID = null;
            this.retangleUC9.Location = new System.Drawing.Point(487, 630);
            this.retangleUC9.Name = "retangleUC9";
            this.retangleUC9.Size = new System.Drawing.Size(83, 125);
            this.retangleUC9.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC9.TabIndex = 1960;
            // 
            // retangleUC10
            // 
            this.retangleUC10.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Dash;
            this.retangleUC10.ExtenderWidth = 3;
            this.retangleUC10.Fill = false;
            this.retangleUC10.GroupID = null;
            this.retangleUC10.Location = new System.Drawing.Point(584, 630);
            this.retangleUC10.Name = "retangleUC10";
            this.retangleUC10.Size = new System.Drawing.Size(83, 125);
            this.retangleUC10.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC10.TabIndex = 1961;
            // 
            // retangleUC11
            // 
            this.retangleUC11.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Dash;
            this.retangleUC11.ExtenderWidth = 3;
            this.retangleUC11.Fill = false;
            this.retangleUC11.GroupID = null;
            this.retangleUC11.Location = new System.Drawing.Point(487, 572);
            this.retangleUC11.Name = "retangleUC11";
            this.retangleUC11.Size = new System.Drawing.Size(180, 55);
            this.retangleUC11.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC11.TabIndex = 1969;
            // 
            // retangleUC12
            // 
            this.retangleUC12.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Dash;
            this.retangleUC12.ExtenderWidth = 3;
            this.retangleUC12.Fill = false;
            this.retangleUC12.GroupID = null;
            this.retangleUC12.Location = new System.Drawing.Point(398, 64);
            this.retangleUC12.Name = "retangleUC12";
            this.retangleUC12.Size = new System.Drawing.Size(584, 799);
            this.retangleUC12.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC12.TabIndex = 1977;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "C15",
            "C16",
            "C17",
            "C18",
            "C19",
            "C20",
            "C21",
            "C22",
            "C23",
            "C24",
            "C25",
            "C26",
            "C27",
            "C28",
            "C29",
            "C30",
            "C31",
            "C32",
            "C33",
            "C34",
            "C35",
            "C36",
            "C37",
            "TSS7",
            "TSS8",
            "TSS9",
            "TSS10",
            "TSS11",
            "TSS12",
            "DTSS1"});
            this.comboBox1.Location = new System.Drawing.Point(150, 120);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 20);
            this.comboBox1.TabIndex = 1981;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // Form_ElecTSS9
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1910, 900);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.switch05UC1);
            this.Controls.Add(this.switch03UC3);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.lineUC14);
            this.Controls.Add(this.lineUC13);
            this.Controls.Add(this.switch05UC3);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.switch05UC2);
            this.Controls.Add(this.switch03UC2);
            this.Controls.Add(this.circleUC4);
            this.Controls.Add(this.circleUC3);
            this.Controls.Add(this.circleUC2);
            this.Controls.Add(this.airCircuitBreaker4);
            this.Controls.Add(this.airCircuitBreaker3);
            this.Controls.Add(this.airCircuitBreaker2);
            this.Controls.Add(this.lineUC10);
            this.Controls.Add(this.airCircuitBreaker1);
            this.Controls.Add(this.transformer02UC1);
            this.Controls.Add(this.circleUC1);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.lineUC12);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.switch02UC5);
            this.Controls.Add(this.transformer01UC1);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.lineUC11);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.retangleUC6);
            this.Controls.Add(this.retangleUC5);
            this.Controls.Add(this.switch02UC4);
            this.Controls.Add(this.switch02UC3);
            this.Controls.Add(this.switch02UC2);
            this.Controls.Add(this.lineUC8);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.switch02UC1);
            this.Controls.Add(this.lineUC7);
            this.Controls.Add(this.retangleUC4);
            this.Controls.Add(this.lineUC6);
            this.Controls.Add(this.lineUC5);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.switch04UC2);
            this.Controls.Add(this.switch04UC1);
            this.Controls.Add(this.lineUC4);
            this.Controls.Add(this.lineUC3);
            this.Controls.Add(this.lineUC2);
            this.Controls.Add(this.lineUC1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.retangleUC2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.retangleUC1);
            this.Controls.Add(this.retangleUC3);
            this.Controls.Add(this.retangleUC7);
            this.Controls.Add(this.retangleUC8);
            this.Controls.Add(this.retangleUC9);
            this.Controls.Add(this.retangleUC10);
            this.Controls.Add(this.retangleUC11);
            this.Controls.Add(this.retangleUC12);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form_ElecTSS9";
            this.Text = "Form_ElecTSS9";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label35;
        private iSCADA.Design.Utilities.Electrical.Switch05UC switch05UC1;
        private iSCADA.Design.Utilities.Electrical.Switch03UC switch03UC3;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC14;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC13;
        private iSCADA.Design.Utilities.Electrical.Switch05UC switch05UC3;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private iSCADA.Design.Utilities.Electrical.Switch05UC switch05UC2;
        private iSCADA.Design.Utilities.Electrical.Switch03UC switch03UC2;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC4;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC3;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC2;
        private iSCADA.Design.Utilities.Electrical.AirCircuitBreaker airCircuitBreaker4;
        private iSCADA.Design.Utilities.Electrical.AirCircuitBreaker airCircuitBreaker3;
        private iSCADA.Design.Utilities.Electrical.AirCircuitBreaker airCircuitBreaker2;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC10;
        private iSCADA.Design.Utilities.Electrical.AirCircuitBreaker airCircuitBreaker1;
        private iSCADA.Design.Utilities.Electrical.Transformer02UC transformer02UC1;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC1;
        private System.Windows.Forms.Label label17;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC12;
        private System.Windows.Forms.Label label20;
        private iSCADA.Design.Utilities.Electrical.Switch02UC switch02UC5;
        private iSCADA.Design.Utilities.Electrical.Transformer01UC transformer01UC1;
        private System.Windows.Forms.Label label19;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC11;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC6;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC5;
        private iSCADA.Design.Utilities.Electrical.Switch02UC switch02UC4;
        private iSCADA.Design.Utilities.Electrical.Switch02UC switch02UC3;
        private iSCADA.Design.Utilities.Electrical.Switch02UC switch02UC2;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private iSCADA.Design.Utilities.Electrical.Switch02UC switch02UC1;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC7;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC4;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC6;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private iSCADA.Design.Utilities.Electrical.Switch04UC switch04UC2;
        private iSCADA.Design.Utilities.Electrical.Switch04UC switch04UC1;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC4;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC3;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC2;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC1;
        private System.Windows.Forms.Label label2;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC2;
        private System.Windows.Forms.Label label1;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC1;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC3;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC7;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC8;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC9;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC10;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC11;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC12;
        private System.Windows.Forms.ComboBox comboBox1;
    }
}