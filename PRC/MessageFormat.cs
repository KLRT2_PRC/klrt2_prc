﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRC
{
   public  class MessageFormat
    {
        public  class MsgContent
        {
            public MsgContent(string objectid, string msg)
            {
                EventTime = DateTime.Now;
                TransID = objectid;
                Content = msg;
            }

            public DateTime EventTime { get; set; }
            public string TransID { get; set; }
            public string Content { get; set; }
        }

        public  class C15
        {
            public C15(ushort[] data)
            {
                SOE = Convert.ToDateTime(data[0].ToString() + "-" + BitConverter.GetBytes(data[1])[1].ToString() + "-" + BitConverter.GetBytes(data[1])[0].ToString() + "  " + BitConverter.GetBytes(data[2])[0].ToString() + ":" + BitConverter.GetBytes(data[3])[1].ToString() + ":" + BitConverter.GetBytes(data[3])[0].ToString() + "." + data[4].ToString() + data[5].ToString());
                byte[] dibyte = new byte[DILength - 12];
                byte[] aibyte = new byte[AILength];
                byte[] dobyte = new byte[DOLength];

                Buffer.BlockCopy(data, 12, dibyte, 0, dibyte.Length);
                Buffer.BlockCopy(data, 24, aibyte, 0, aibyte.Length);
                Buffer.BlockCopy(data, 88, dobyte, 0, dobyte.Length);

                //DI
                BitArray bits = new BitArray(dibyte);
                CPURunning = bits[8];
                CPUError = bits[9];
                CPUCommFault = bits[10];
                INTERLOCKCommFault = bits[11];
                CPUBFlag = bits[12];

                MC64UPSAC220VOn = bits[0];
                MC64TWELEAC220VOn = bits[1];
                MC64AC110V220VMCCBNormal = bits[2];
                MC64DC24VMCCBNormal = bits[3];
                MC64BCMNGSYSTEMOk = bits[4];
                MC64G5CHARGERDCINOk = bits[5];
                MC64G5CHARGERBATTERYOk = bits[6];
                MC64G5CHARGERDCOUTOk = bits[7];
                MC64G6CHARGERDCINOk = bits[24];
                MC64G6CHARGERBATTERYOk = bits[25];
                MC64G6CHARGERDCOUTOk = bits[26];
                MC64EMERGENCYSTOPPBRelease = bits[27];
                MC64ENABLESWITCHOn = bits[28];
                MC64DOOROpen = bits[29];
                MC64G9POWERSUPPLYDCOUTOk = bits[30];
                MC64MC62SAFETYRELAYOn = bits[16];
                MC64MC63SAFETYRELAYOn = bits[17];
                MC64CONTACTOROFFFault = bits[18];
                MC64FANSRUNStart = bits[19];
                MC62MCBFault = bits[40];
                MC62REMOTEMode = bits[41];
                MC62DISCONNECTOROpened = bits[42];
                MC62DISCONNECTORClosed = bits[43];
                MC62K1CONTACTOROpened = bits[44];
                MC62K1CONTACTORClosed = bits[45];
                MC62K3CONTACTOROpened = bits[46];
                MC62K3CONTACTORClosed = bits[47];
                MC62FRAMEFault = bits[32];
                MC62HANDCRANKPlugged = bits[33];
                MC62VOLTAGEAVAILABLEIn = bits[34];

                MC63MCBFault = bits[56];
                MC63REMOTEMode = bits[57];
                MC63DISCONNECTOROpened = bits[58];
                MC63DISCONNECTORClosed = bits[59];
                MC63K2CONTACTOROpened = bits[60];
                MC63K2CONTACTORClosed = bits[61];
                MC63K4CONTACTOROpened = bits[62];
                MC63K4CONTACTORClosed = bits[63];
                MC63FRAMEFault = bits[48];
                MC63HANDCRANKPlugged = bits[49];
                MC63VOLTAGEAVAILABLEIn = bits[50];

                MF80VOLTAGELIMITINGDEVICEBlocked = bits[72];
                MF80VOLTAGELIMITINGDEVICEOpened = bits[73];
                MF80VOLTAGELIMITINGDEVICEClosed = bits[74];
                MF80VOLTAGELIMITINGDEVICEFault = bits[75];
                MF80UminRLBWETIMEOut = bits[76];
                MF80OVERLOADThermal = bits[77];

                MPPAC220VOk = bits[64];
                ESNUPSAIRCONDITIONINGFault = bits[65];
                ESNUPSFault = bits[66];
                ESNUPSAlarm = bits[67];
                COMUPSCOMUPSFault = bits[68];
                COMUPSCOMUPSAlarm = bits[69];
                //AI
                MC62VOLTAGECONDUCTORRail = BitConverter.ToSingle(Swapped(aibyte, 0), 0);
                MC62CURRENTDIODE1 = BitConverter.ToSingle(Swapped(aibyte, 4), 0);
                MC62CURRENTDIODE2 = BitConverter.ToSingle(Swapped(aibyte, 8), 0);
                MC62CURRENTDIODE3 = BitConverter.ToSingle(Swapped(aibyte, 12), 0);
                MC62CURRENTCONTACTORKF1 = BitConverter.ToSingle(Swapped(aibyte, 16), 0);
                MC62CURRENTCONTACTORKF3 = BitConverter.ToSingle(Swapped(aibyte, 20), 0);
                MC62PANELTemperature = BitConverter.ToSingle(Swapped(aibyte, 24), 0);

                MC63VOLTAGECONDUCTORRail = BitConverter.ToSingle(Swapped(aibyte, 28), 0);
                MC63CURRENTDIODE1 = BitConverter.ToSingle(Swapped(aibyte, 32), 0);
                MC63CURRENTDIODE2 = BitConverter.ToSingle(Swapped(aibyte, 36), 0);
                MC63CURRENTDIODE3 = BitConverter.ToSingle(Swapped(aibyte, 40), 0);
                MC63CURRENTCONTACTORKF2 = BitConverter.ToSingle(Swapped(aibyte, 44), 0);
                MC63CURRENTCONTACTORKF4 = BitConverter.ToSingle(Swapped(aibyte, 48), 0);
                MC63PANELTemperature = BitConverter.ToSingle(Swapped(aibyte, 52), 0);

                MF80VOLTAGEMeasuring = BitConverter.ToSingle(Swapped(aibyte, 56), 0);
                MC64PANELTemperature = BitConverter.ToSingle(Swapped(aibyte, 60), 0);
                //DO
                BitArray dobits = new BitArray(dobyte);
                MC62DISCONNECTORONCommand = dobits[8];
                MC62DISCONNECTOROFFCommand = dobits[9];
                MC63DISCONNECTORONCommand = dobits[10];
                MC63DISCONNECTOROFFCommand = dobits[11];
                MF80REMOTECOMMANDFUNCTIONTESTOn = dobits[12];
                MF80REMOTECOMMANDPROTECTIONONDirect = dobits[13];
                LTSLIGHTINGOn = dobits[0];
                CPUDATATRANSFERACCEPTConfirm = dobits[7];
            }

            private byte[] Swapped(byte[] source, int start)
            {
                byte[] dtarray = new byte[4];
                byte[] result = new byte[4];

                Array.Copy(source, start, dtarray, 0, 4);
                Array.Copy(dtarray, 0, result, 2, 2);
                Array.Copy(dtarray, 2, result, 0, 2);
                return result;
            }

            public readonly int Length = 90;
            private readonly int DILength = 22;
            private readonly int AILength = 66;
            private readonly int DOLength = 2;
            public DateTime SOE { get; private set; }
            public readonly string SystemID = "PSS";
            public readonly string Subsystem = "TSS";
            public readonly string Location = "C15";
            //DI
            public bool CPURunning { get; private set; }
            public bool CPUError { get; private set; }
            public bool CPUCommFault { get; private set; }
            public bool INTERLOCKCommFault { get; private set; }
            public bool CPUBFlag { get; private set; }

            public bool MC64UPSAC220VOn { get; private set; }
            public bool MC64TWELEAC220VOn { get; private set; }
            public bool MC64AC110V220VMCCBNormal { get; private set; }
            public bool MC64DC24VMCCBNormal { get; private set; }
            public bool MC64BCMNGSYSTEMOk { get; private set; }
            public bool MC64G5CHARGERDCINOk { get; private set; }
            public bool MC64G5CHARGERBATTERYOk { get; private set; }
            public bool MC64G5CHARGERDCOUTOk { get; private set; }
            public bool MC64G6CHARGERDCINOk { get; private set; }
            public bool MC64G6CHARGERBATTERYOk { get; private set; }
            public bool MC64G6CHARGERDCOUTOk { get; private set; }
            public bool MC64EMERGENCYSTOPPBRelease { get; private set; }
            public bool MC64ENABLESWITCHOn { get; private set; }
            public bool MC64DOOROpen { get; private set; }
            public bool MC64G9POWERSUPPLYDCOUTOk { get; private set; }
            public bool MC64MC62SAFETYRELAYOn { get; private set; }
            public bool MC64MC63SAFETYRELAYOn { get; private set; }
            public bool MC64CONTACTOROFFFault { get; private set; }
            public bool MC64FANSRUNStart { get; private set; }
            public bool MC62MCBFault { get; private set; }
            public bool MC62REMOTEMode { get; private set; }
            public bool MC62DISCONNECTOROpened { get; private set; }
            public bool MC62DISCONNECTORClosed { get; private set; }
            public bool MC62K1CONTACTOROpened { get; private set; }
            public bool MC62K1CONTACTORClosed { get; private set; }
            public bool MC62K3CONTACTOROpened { get; private set; }
            public bool MC62K3CONTACTORClosed { get; private set; }
            public bool MC62FRAMEFault { get; private set; }
            public bool MC62HANDCRANKPlugged { get; private set; }
            public bool MC62VOLTAGEAVAILABLEIn { get; private set; }

            public bool MC63MCBFault { get; private set; }
            public bool MC63REMOTEMode { get; private set; }
            public bool MC63DISCONNECTOROpened { get; private set; }
            public bool MC63DISCONNECTORClosed { get; private set; }
            public bool MC63K2CONTACTOROpened { get; private set; }
            public bool MC63K2CONTACTORClosed { get; private set; }
            public bool MC63K4CONTACTOROpened { get; private set; }
            public bool MC63K4CONTACTORClosed { get; private set; }
            public bool MC63FRAMEFault { get; private set; }
            public bool MC63HANDCRANKPlugged { get; private set; }
            public bool MC63VOLTAGEAVAILABLEIn { get; private set; }

            public bool MF80VOLTAGELIMITINGDEVICEBlocked { get; private set; }
            public bool MF80VOLTAGELIMITINGDEVICEOpened { get; private set; }
            public bool MF80VOLTAGELIMITINGDEVICEClosed { get; private set; }
            public bool MF80VOLTAGELIMITINGDEVICEFault { get; private set; }
            public bool MF80UminRLBWETIMEOut { get; private set; }
            public bool MF80OVERLOADThermal { get; private set; }

            public bool MPPAC220VOk { get; private set; }
            public bool ESNUPSAIRCONDITIONINGFault { get; private set; }
            public bool ESNUPSFault { get; private set; }
            public bool ESNUPSAlarm { get; private set; }
            public bool COMUPSCOMUPSFault { get; private set; }
            public bool COMUPSCOMUPSAlarm { get; private set; }
            //AI
            public float MC62VOLTAGECONDUCTORRail { get; private set; }
            public float MC62CURRENTDIODE1 { get; private set; }
            public float MC62CURRENTDIODE2 { get; private set; }
            public float MC62CURRENTDIODE3 { get; private set; }
            public float MC62CURRENTCONTACTORKF1 { get; private set; }
            public float MC62CURRENTCONTACTORKF3 { get; private set; }
            public float MC62PANELTemperature { get; private set; }

            public float MC63VOLTAGECONDUCTORRail { get; private set; }
            public float MC63CURRENTDIODE1 { get; private set; }
            public float MC63CURRENTDIODE2 { get; private set; }
            public float MC63CURRENTDIODE3 { get; private set; }
            public float MC63CURRENTCONTACTORKF2 { get; private set; }
            public float MC63CURRENTCONTACTORKF4 { get; private set; }
            public float MC63PANELTemperature { get; private set; }

            public float MF80VOLTAGEMeasuring { get; private set; }
            public float MC64PANELTemperature { get; private set; }
            //DO
            public bool MC62DISCONNECTORONCommand { get; private set; }
            public bool MC62DISCONNECTOROFFCommand { get; private set; }
            public bool MC63DISCONNECTORONCommand { get; private set; }
            public bool MC63DISCONNECTOROFFCommand { get; private set; }
            public bool MF80REMOTECOMMANDFUNCTIONTESTOn { get; private set; }
            public bool MF80REMOTECOMMANDPROTECTIONONDirect { get; private set; }
            public bool LTSLIGHTINGOn { get; private set; }
            public bool CPUDATATRANSFERACCEPTConfirm { get; private set; }
        }

        public class TSS7
        {
            public TSS7(ushort[] data)
            {
                //var year = data[0];
                //var month =BitConverter.GetBytes(data[1])[1].ToString();
                //var day = BitConverter.GetBytes(data[1])[0].ToString();                         
                //var hour = BitConverter.GetBytes(data[2])[0].ToString();
                //var min = BitConverter.GetBytes(data[3])[1].ToString();
                //var sec = BitConverter.GetBytes(data[3])[0].ToString();
                //var nasec = data[4].ToString() + "_" + data[5].ToString();
                SOE = Convert.ToDateTime(data[0].ToString() + "-" + BitConverter.GetBytes(data[1])[1].ToString() + "-" + BitConverter.GetBytes(data[1])[0].ToString() + "  " + BitConverter.GetBytes(data[2])[0].ToString() + ":" + BitConverter.GetBytes(data[3])[1].ToString() + ":" + BitConverter.GetBytes(data[3])[0].ToString() + "." + data[4].ToString() + data[5].ToString());
                byte[] dibyte = new byte[DILength - 12];
                byte[] aibyte = new byte[AILength];
                //int[] aibyte = new int[AILength / 4];
                //ushort[] aibyte = new ushort[AILength / 2];
                byte[] dobyte = new byte[DOLength];

                Buffer.BlockCopy(data, 12, dibyte, 0, dibyte.Length);
                Buffer.BlockCopy(data, 50, aibyte, 0, aibyte.Length);
                Buffer.BlockCopy(data, 206, dobyte, 0, dobyte.Length);

                //DI
                BitArray bits = new BitArray(dibyte);
                CPURunning = bits[8];
                CPUError = bits[9];
                CPUCommFault = bits[10];
                RS485Module1Running = bits[11];
                RS485Module1Error = bits[12];
                RS485Module1CommFault = bits[13];
                RS485Module2Running = bits[14];
                RS485Module2Error = bits[15];
                RS485Module2CommFault = bits[0];

                HVASBatteryLow = bits[24];
                HVASAC220VPowerLoss = bits[25];
                AC228KVPower01Normal = bits[26];
                AC228KVPower02Normal = bits[27];
                HVASLBS1Power = bits[28];
                HVASLBS2Power = bits[29];

                GISH01MCCBOFFTRIP = bits[16];
                GISH01PTDSON = bits[17];
                GISH01PTDSOFF = bits[18];
                GISH01ESON = bits[19];
                GISH01ESOFF = bits[20];
                GISH01BusESON = bits[21];
                GISH01BusESOFF = bits[22];
                GISH01BusAircellPressureLev1Alarm = bits[23];
                GISH01BusAircellPressureLev2Alarm = bits[40];
                GISH01PTPressureLev1Alarm = bits[41];
                GISH01PTPressureLev2Alarm = bits[42];
                PTSecSideFuseFault = bits[43];

                GISH02MCCBOFFTRIP = bits[32];
                GISH02GISREMOTEMODE = bits[33];
                GISH02GISLOCALMODE = bits[34];
                GISH02CBON = bits[35];
                GISH02CBOFF = bits[36];
                GISH02DSON = bits[37];
                GISH02DSOFF = bits[38];
                GISH02ESON = bits[39];
                GISH02ESOFF = bits[56];
                GISH02CBPressureLev1Alarm = bits[57];
                GISH02CBPressureLev2Alarm = bits[58];
                GISH02GCBPressureLowAlarm = bits[59];
                GISH02GCBPressureLowLATCHAlarm = bits[60];
                GISH02IEDRELAYFAULTALARM = bits[61];

                GISH02IEDRELAY50 = bits[48];
                GISH02IEDRELAY50N = bits[49];
                GISH02IEDRELAY51 = bits[50];
                GISH02IEDRELAY51N = bits[51];
                GISH02IEDRELAY27 = bits[52];
                GISH02IEDRELAY59 = bits[53];
                GISH02CBTRIP = bits[54];

                GISH03MCCBOFFTRIP = bits[72];
                GISH03GISREMOTEMODE = bits[73];
                GISH03GISLOCALMODE = bits[74];
                GISH03CBON = bits[75];
                GISH03CBOFF = bits[76];
                GISH03DSON = bits[77];
                GISH03DSOFF = bits[78];
                GISH03ESON = bits[79];
                GISH03ESOFF = bits[64];
                GISH03CBPressureLev1Alarm = bits[65];
                GISH03CBPressureLev2Alarm = bits[66];
                GISH03GCBPressureLowAlarm = bits[67];
                GISH03GCBPressureLowLATCHAlarm = bits[68];
                GISH03IEDRELAYFAULTALARM = bits[69];
                GISH03IEDRELAY50 = bits[88];
                GISH03IEDRELAY50N = bits[89];
                GISH03IEDRELAY51 = bits[90];
                GISH03IEDRELAY51N = bits[91];
                GISH03CBTRIP = bits[92];

                GISH04MCCBOFFTRIP = bits[80];
                GISH04GISREMOTEMODE = bits[81];
                GISH04GISLOCALMODE = bits[82];
                GISH04CBON = bits[83];
                GISH04CBOFF = bits[84];
                GISH04DSON = bits[85];
                GISH04DSOFF = bits[86];
                GISH04ESON = bits[87];
                GISH04ESOFF = bits[104];
                GISH04CBPressureLev1Alarm = bits[105];
                GISH04CBPressureLev2Alarm = bits[106];
                GISH04GCBPressureLowAlarm = bits[107];
                GISH04GCBPressureLowLATCHAlarm = bits[108];
                GISH04IEDRELAYFAULTALARM = bits[109];

                GISH04IEDRELAY50 = bits[96];
                GISH04IEDRELAY50N = bits[97];
                GISH04IEDRELAY51 = bits[98];
                GISH04IEDRELAY51N = bits[99];
                GISH04CBTRIP = bits[100];

                TR1TEMPALARM = bits[120];
                TR1TEMPTRIP = bits[121];
                TR2TEMPALARM = bits[122];
                TR2TEMPTRIP = bits[123];
                TR1TR2MCCBOFFTRIP = bits[124];

                BCACTRIP = bits[112];
                BCACPOWERLOW = bits[113];
                BCCHARGERFAULT = bits[114];
                BCDCOUTHIGH = bits[115];
                BCDCOUTLOW = bits[116];
                BCDCTRIP = bits[117];
                BCGROUDFAULT = bits[118];
                BCSIDFAULT = bits[119];

                ADPMCCBOFFTRIP = bits[136];
                DDPMCCBOFFTRIP = bits[137];

                UPSESNAIRCONDITIONFAULT = bits[128];
                UPSESNUPSFAULT = bits[129];
                UPSESNUPSALARM = bits[130];
                UPSCOMUSPFAULT = bits[131];
                UPSCOMUSPALARM = bits[132];

                MC21COMMUNICATIONFAULT = bits[152];
                MC21GROUPALARM = bits[153];
                MC21MCBTRIP = bits[154];
                MC21TIMESYNDISTURBED = bits[155];
                MC21SITRASPROSYSTEMFAULT = bits[156];
                MC21MAINTENANCEALARM = bits[157];
                MC21LOSSFOC = bits[158];
                MC21SHUNTLOSS = bits[59];

                MC21FRAMEFAULTPROTEDTIONACTIVE = bits[144];
                MC21OVERCURRENTTRIPPING = bits[145];
                MC21WARNINIGIMAX = bits[146];
                MC21TRIPIMAX = bits[147];
                MC21DELTAIWARNING = bits[148];
                MC21DELTAITRIP = bits[149];
                MC21DIDTWARNING = bits[150];
                MC21DIDTTRIP = bits[151];

                MC21WARNINGIDMT = bits[168];
                MC21TRIPIDMT = bits[169];
                MC21TRIPCABLEPROTECTION = bits[170];
                MC21TRANSFERTRIPSENT = bits[171];
                MC21TRANSFERTRIPRECEIVED = bits[172];
                MC21LINETESTFAULTCONTSHORTCIRUIT = bits[173];
                MC21DRAWERPOSITIONFAULT = bits[174];
                MC21LINETESTRUNNING = bits[175];

                MC21LOCALMODE = bits[160];
                MC21REMOTEMODE = bits[161];
                MC21CBOPENED = bits[162];
                MC21CBCLOSED = bits[163];
                MC21SWITCHONCBPOSSIBLE = bits[164];
                MC21DRAWERINTESTPOSITION = bits[165];
                MC21DRAWERINOPERATIONPOSITION = bits[166];
                MC21DISCONNECTOROPENED = bits[167];
                MC21DISCONNECTORCLOSED = bits[184];

                MC22COMMUNICATIONFAULT = bits[200];
                MC22GROUPALARM = bits[201];
                MC22MCBTRIP = bits[202];
                MC22TIMESYNDISTURBED = bits[203];
                MC22SITRASPROSYSTEMFAULT = bits[204];
                MC22MAINTENANCEALARM = bits[205];
                MC22LOSSFOC = bits[206];
                MC22SHUNTLOSS = bits[207];

                MC22FRAMEFAULTPROTEDTIONACTIVE = bits[192];
                MC22OVERCURRENTTRIPPING = bits[193];
                MC22WARNINIGIMAX = bits[194];
                MC22TRIPIMAX = bits[195];
                MC22DELTAIWARNING = bits[196];
                MC22DELTAITRIP = bits[197];
                MC22DIDTWARNING = bits[198];
                MC22DIDTTRIP = bits[199];

                MC22WARNINGIDMT = bits[216];
                MC22TRIPIDMT = bits[217];
                MC22TRIPCABLEPROTECTION = bits[218];
                MC22TRANSFERTRIPSENT = bits[219];
                MC22TRANSFERTRIPRECEIVED = bits[220];
                MC22LINETESTFAULTCONTSHORTCIRUIT = bits[221];
                MC22DRAWERPOSITIONFAULT = bits[222];
                MC22LINETESTRUNNING = bits[223];

                MC22LOCALMODE = bits[208];
                MC22REMOTEMODE = bits[209];
                MC22CBOPENED = bits[210];
                MC22CBCLOSED = bits[211];
                MC22SWITCHONCBPOSSIBLE = bits[212];
                MC22DRAWERINTESTPOSITION = bits[213];
                MC22DRAWERINOPERATIONPOSITION = bits[214];
                MC22DISCONNECTOROPENED = bits[215];
                MC22DISCONNECTORCLOSED = bits[232];

                MC41COMMUNICATIONFAULT = bits[248];
                MC41MCBTRIP = bits[249];
                MC41DISCONNECTORTROUBLE = bits[250];
                MC41LOCALMODE = bits[251];
                MC41REMOTEMODE = bits[252];
                MC41DISCONNECTOROPENED = bits[253];
                MC41DISCONNECTORCLOSED = bits[254];

                MC81TIMESYNDISTURBED = bits[264];
                MG01DIODETRIP = bits[265];
                MC81MCBTRIP = bits[266];
                MG01TEMPWARNING = bits[267];
                MG01TEMPTRIP = bits[268];
                MC81FRAMEFAULTPROTECTIONTRIP = bits[269];
                MC81DISCONNECTORTROUBLE = bits[270];
                MC81LOCALMODE = bits[271];

                MC81REMOTEMODE = bits[256];
                MC81DISCONNECTOROPENED = bits[257];
                MC81DISCONNECTORCLOSED = bits[258];

                //Do
                BitArray dobits = new BitArray(dobyte);
                GISH02GISONCOMMAND = dobits[8];
                GISH02GISOFFCOMMAND = dobits[9];
                GISH03GISONCOMMAND = dobits[10];
                GISH03GISOFFCOMMAND = dobits[11];
                GISH04GISONCOMMAND = dobits[12];
                GISH04GISOFFCOMMAND = dobits[13];

                MC21CBONCOMMAND = dobits[0];
                MC21CBOFFCOMMAND = dobits[1];
                MC21DISCONNECTORONCOMMAND = dobits[2];
                MC21DISCONNECTOROFFCOMMAND = dobits[3];
                MC22CBONCOMMAND = dobits[4];
                MC22CBOFFCOMMAND = dobits[5];
                MC22DISCONNECTORONCOMMAND = dobits[6];
                MC22DISCONNECTOROFFCOMMAND = dobits[7];

                MC41LBSONCOMMAND = dobits[24];
                MC41LBSOFFCOMMAND = dobits[25];
                MC81DISCONNECTORONCOMMAND = dobits[28];
                MC81DISCONNECTOROFFCOMMAND = dobits[29];

                //AI
                GISH02RSPHASEVOLTAGE = BitConverter.ToSingle(Swapped(aibyte, 0), 0);
                GISH02STPHASEVOLTAGE = BitConverter.ToSingle(Swapped(aibyte, 4), 0);
                GISH02TRPHASEVOLTAGE = BitConverter.ToSingle(Swapped(aibyte, 8), 0);
                GISH02RPHASECURRENT = BitConverter.ToSingle(Swapped(aibyte, 12), 0);
                GISH02SPHASECURRENT = BitConverter.ToSingle(Swapped(aibyte, 16), 0);
                GISH02TPHASECURRENT = BitConverter.ToSingle(Swapped(aibyte, 20), 0);
                GISH02POWERFACTORPF = BitConverter.ToSingle(Swapped(aibyte, 24), 0);
                GISH02POWERVALUEKW = BitConverter.ToSingle(Swapped(aibyte, 28), 0);
                GISH02POWERKWH = BitConverter.ToSingle(Swapped(aibyte, 32), 0);
                GISH02FREQUENCYHZ = BitConverter.ToSingle(Swapped(aibyte, 36), 0);
                GISH02POWERKVARH = BitConverter.ToSingle(Swapped(aibyte, 40), 0);

                GISH03RSPHASEVOLTAGE = BitConverter.ToSingle(Swapped(aibyte, 44), 0);
                GISH03STPHASEVOLTAGE = BitConverter.ToSingle(Swapped(aibyte, 48), 0);
                GISH03TRPHASEVOLTAGE = BitConverter.ToSingle(Swapped(aibyte, 52), 0);
                GISH03RPHASECURRENT = BitConverter.ToSingle(Swapped(aibyte, 56), 0);
                GISH03SPHASECURRENT = BitConverter.ToSingle(Swapped(aibyte, 60), 0);
                GISH03TPHASECURRENT = BitConverter.ToSingle(Swapped(aibyte, 64), 0);
                GISH03POWERFACTORPF = BitConverter.ToSingle(Swapped(aibyte, 68), 0);
                GISH03POWERVALUEKW = BitConverter.ToSingle(Swapped(aibyte, 72), 0);
                GISH03POWERKWH = BitConverter.ToSingle(Swapped(aibyte, 76), 0);
                GISH03FREQUENCYHZ = BitConverter.ToSingle(Swapped(aibyte, 80), 0);
                GISH03POWERKVARH = BitConverter.ToSingle(Swapped(aibyte, 84), 0);

                GISH04RSPHASEVOLTAGE = BitConverter.ToSingle(Swapped(aibyte, 88), 0);
                GISH04STPHASEVOLTAGE = BitConverter.ToSingle(Swapped(aibyte, 92), 0);
                GISH04TRPHASEVOLTAGE = BitConverter.ToSingle(Swapped(aibyte, 96), 0);
                GISH04RPHASECURRENT = BitConverter.ToSingle(Swapped(aibyte, 100), 0);
                GISH04SPHASECURRENT = BitConverter.ToSingle(Swapped(aibyte, 104), 0);
                GISH04TPHASECURRENT = BitConverter.ToSingle(Swapped(aibyte, 108), 0);
                GISH04POWERFACTORPF = BitConverter.ToSingle(Swapped(aibyte, 112), 0);
                GISH04POWERVALUEKW = BitConverter.ToSingle(Swapped(aibyte, 116), 0);
                GISH04POWERKWH = BitConverter.ToSingle(Swapped(aibyte, 120), 0);
                GISH04FREQUENCYHZ = BitConverter.ToSingle(Swapped(aibyte, 124), 0);
                GISH04POWERKVARH = BitConverter.ToSingle(Swapped(aibyte, 128), 0);

                MC21DCVOLTAGE = BitConverter.ToSingle(Swapped(aibyte, 132), 0);
                MC21DCCURRENT = BitConverter.ToSingle(Swapped(aibyte, 136), 0);
                MC22DCVOLTAGE = BitConverter.ToSingle(Swapped(aibyte, 140), 0);
                MC22DCCURRENT = BitConverter.ToSingle(Swapped(aibyte, 144), 0);
                MC81DCVOLTAGE = BitConverter.ToSingle(Swapped(aibyte, 148), 0);
                MC81DCCURRENT = BitConverter.ToSingle(Swapped(aibyte, 152), 0);

            }

            //private byte[] Swapped(byte[] source)
            //{
            //    byte[] result = new byte[4];
            //    Array.Copy(source, 0, result, 2, 2);
            //    Array.Copy(source, 2, result, 0, 2);
            //    return result;
            //}

            private byte[] Swapped(byte[] source, int start)
            {
                byte[] dtarray = new byte[4];
                byte[] result = new byte[4];

                Array.Copy(source, start, dtarray, 0, 4);
                Array.Copy(dtarray, 0, result, 2, 2);
                Array.Copy(dtarray, 2, result, 0, 2);
                return result;
            }

            public readonly int Length = 212;
            private readonly int DILength = 48;
            private readonly int AILength = 156;
            private readonly int DOLength = 5;
            public DateTime SOE { get; private set; }
            public readonly string SystemID = "PSS";
            public readonly string Subsystem = "TSS";
            public readonly string Location = "TSS7";
            public bool CPURunning { get; private set; }
            public bool CPUError { get; private set; }
            public bool CPUCommFault { get; private set; }
            public bool RS485Module1Running { get; private set; }
            public bool RS485Module1Error { get; private set; }
            public bool RS485Module1CommFault { get; private set; }
            public bool RS485Module2Running { get; private set; }
            public bool RS485Module2Error { get; private set; }
            public bool RS485Module2CommFault { get; private set; }

            public bool HVASBatteryLow { get; private set; }
            public bool HVASAC220VPowerLoss { get; private set; }
            public bool AC228KVPower01Normal { get; private set; } //台電三民變電所22.8KV 電源正常
            public bool AC228KVPower02Normal { get; private set; }//台電內惟變電所22.8KV 電源正常
            public bool HVASLBS1Power { get; private set; }//HVAS LBS 1 三民變電所供電
            public bool HVASLBS2Power { get; private set; }//HVAS LBS 2 內惟變電所供電

            public bool GISH01MCCBOFFTRIP { get; private set; }
            public bool GISH01PTDSON { get; private set; }
            public bool GISH01PTDSOFF { get; private set; }
            public bool GISH01ESON { get; private set; }
            public bool GISH01ESOFF { get; private set; }
            public bool GISH01BusESON { get; private set; }
            public bool GISH01BusESOFF { get; private set; }
            public bool GISH01BusAircellPressureLev1Alarm { get; private set; }
            public bool GISH01BusAircellPressureLev2Alarm { get; private set; }
            public bool GISH01PTPressureLev1Alarm { get; private set; }
            public bool GISH01PTPressureLev2Alarm { get; private set; }
            public bool PTSecSideFuseFault { get; private set; }

            public bool GISH02MCCBOFFTRIP { get; private set; }
            public bool GISH02GISREMOTEMODE { get; private set; }
            public bool GISH02GISLOCALMODE { get; private set; }
            public bool GISH02CBON { get; private set; }
            public bool GISH02CBOFF { get; private set; }
            public bool GISH02DSON { get; private set; }
            public bool GISH02DSOFF { get; private set; }
            public bool GISH02ESON { get; private set; }
            public bool GISH02ESOFF { get; private set; }
            public bool GISH02CBPressureLev1Alarm { get; private set; }
            public bool GISH02CBPressureLev2Alarm { get; private set; }
            public bool GISH02GCBPressureLowAlarm { get; private set; }
            public bool GISH02GCBPressureLowLATCHAlarm { get; private set; }
            public bool GISH02IEDRELAYFAULTALARM { get; private set; }
            public bool GISH02IEDRELAY50 { get; private set; }
            public bool GISH02IEDRELAY50N { get; private set; }
            public bool GISH02IEDRELAY51 { get; private set; }
            public bool GISH02IEDRELAY51N { get; private set; }
            public bool GISH02IEDRELAY27 { get; private set; }
            public bool GISH02IEDRELAY59 { get; private set; }
            public bool GISH02CBTRIP { get; private set; }

            public bool GISH03MCCBOFFTRIP { get; private set; }
            public bool GISH03GISREMOTEMODE { get; private set; }
            public bool GISH03GISLOCALMODE { get; private set; }
            public bool GISH03CBON { get; private set; }
            public bool GISH03CBOFF { get; private set; }
            public bool GISH03DSON { get; private set; }
            public bool GISH03DSOFF { get; private set; }
            public bool GISH03ESON { get; private set; }
            public bool GISH03ESOFF { get; private set; }
            public bool GISH03CBPressureLev1Alarm { get; private set; }
            public bool GISH03CBPressureLev2Alarm { get; private set; }
            public bool GISH03GCBPressureLowAlarm { get; private set; }
            public bool GISH03GCBPressureLowLATCHAlarm { get; private set; }
            public bool GISH03IEDRELAYFAULTALARM { get; private set; }
            public bool GISH03IEDRELAY50 { get; private set; }
            public bool GISH03IEDRELAY50N { get; private set; }
            public bool GISH03IEDRELAY51 { get; private set; }
            public bool GISH03IEDRELAY51N { get; private set; }
            public bool GISH03CBTRIP { get; private set; }

            public bool GISH04MCCBOFFTRIP { get; private set; }
            public bool GISH04GISREMOTEMODE { get; private set; }
            public bool GISH04GISLOCALMODE { get; private set; }
            public bool GISH04CBON { get; private set; }
            public bool GISH04CBOFF { get; private set; }
            public bool GISH04DSON { get; private set; }
            public bool GISH04DSOFF { get; private set; }
            public bool GISH04ESON { get; private set; }
            public bool GISH04ESOFF { get; private set; }
            public bool GISH04CBPressureLev1Alarm { get; private set; }
            public bool GISH04CBPressureLev2Alarm { get; private set; }
            public bool GISH04GCBPressureLowAlarm { get; private set; }
            public bool GISH04GCBPressureLowLATCHAlarm { get; private set; }
            public bool GISH04IEDRELAYFAULTALARM { get; private set; }
            public bool GISH04IEDRELAY50 { get; private set; }
            public bool GISH04IEDRELAY50N { get; private set; }
            public bool GISH04IEDRELAY51 { get; private set; }
            public bool GISH04IEDRELAY51N { get; private set; }
            public bool GISH04CBTRIP { get; private set; }

            public bool TR1TEMPALARM { get; private set; }
            public bool TR1TEMPTRIP { get; private set; }
            public bool TR2TEMPALARM { get; private set; }
            public bool TR2TEMPTRIP { get; private set; }
            public bool TR1TR2MCCBOFFTRIP { get; private set; }

            public bool BCACTRIP { get; private set; }
            public bool BCACPOWERLOW { get; private set; }
            public bool BCCHARGERFAULT { get; private set; }
            public bool BCDCOUTHIGH { get; private set; }
            public bool BCDCOUTLOW { get; private set; }
            public bool BCDCTRIP { get; private set; }
            public bool BCGROUDFAULT { get; private set; }
            public bool BCSIDFAULT { get; private set; }
            public bool ADPMCCBOFFTRIP { get; private set; }
            public bool DDPMCCBOFFTRIP { get; private set; }

            public bool UPSESNAIRCONDITIONFAULT { get; private set; }
            public bool UPSESNUPSFAULT { get; private set; }
            public bool UPSESNUPSALARM { get; private set; }
            public bool UPSCOMUSPFAULT { get; private set; }
            public bool UPSCOMUSPALARM { get; private set; }

            public bool MC21COMMUNICATIONFAULT { get; private set; }
            public bool MC21GROUPALARM { get; private set; }
            public bool MC21MCBTRIP { get; private set; }
            public bool MC21TIMESYNDISTURBED { get; private set; }
            public bool MC21SITRASPROSYSTEMFAULT { get; private set; }
            public bool MC21MAINTENANCEALARM { get; private set; }
            public bool MC21LOSSFOC { get; private set; }
            public bool MC21SHUNTLOSS { get; private set; }
            public bool MC21FRAMEFAULTPROTEDTIONACTIVE { get; private set; }
            public bool MC21OVERCURRENTTRIPPING { get; private set; }
            public bool MC21WARNINIGIMAX { get; private set; }
            public bool MC21TRIPIMAX { get; private set; }
            public bool MC21DELTAIWARNING { get; private set; }
            public bool MC21DELTAITRIP { get; private set; }
            public bool MC21DIDTWARNING { get; private set; }
            public bool MC21DIDTTRIP { get; private set; }
            public bool MC21WARNINGIDMT { get; private set; }
            public bool MC21TRIPIDMT { get; private set; }
            public bool MC21TRIPCABLEPROTECTION { get; private set; }
            public bool MC21TRANSFERTRIPSENT { get; private set; }
            public bool MC21TRANSFERTRIPRECEIVED { get; private set; }
            public bool MC21LINETESTFAULTCONTSHORTCIRUIT { get; private set; }
            public bool MC21DRAWERPOSITIONFAULT { get; private set; }
            public bool MC21LINETESTRUNNING { get; private set; }
            public bool MC21LOCALMODE { get; private set; }
            public bool MC21REMOTEMODE { get; private set; }
            public bool MC21CBOPENED { get; private set; }
            public bool MC21CBCLOSED { get; private set; }
            public bool MC21SWITCHONCBPOSSIBLE { get; private set; }
            public bool MC21DRAWERINTESTPOSITION { get; private set; }
            public bool MC21DRAWERINOPERATIONPOSITION { get; private set; }
            public bool MC21DISCONNECTOROPENED { get; private set; }
            public bool MC21DISCONNECTORCLOSED { get; private set; }

            public bool MC22COMMUNICATIONFAULT { get; private set; }
            public bool MC22GROUPALARM { get; private set; }
            public bool MC22MCBTRIP { get; private set; }
            public bool MC22TIMESYNDISTURBED { get; private set; }
            public bool MC22SITRASPROSYSTEMFAULT { get; private set; }
            public bool MC22MAINTENANCEALARM { get; private set; }
            public bool MC22LOSSFOC { get; private set; }
            public bool MC22SHUNTLOSS { get; private set; }
            public bool MC22FRAMEFAULTPROTEDTIONACTIVE { get; private set; }
            public bool MC22OVERCURRENTTRIPPING { get; private set; }
            public bool MC22WARNINIGIMAX { get; private set; }
            public bool MC22TRIPIMAX { get; private set; }
            public bool MC22DELTAIWARNING { get; private set; }
            public bool MC22DELTAITRIP { get; private set; }
            public bool MC22DIDTWARNING { get; private set; }
            public bool MC22DIDTTRIP { get; private set; }
            public bool MC22WARNINGIDMT { get; private set; }
            public bool MC22TRIPIDMT { get; private set; }
            public bool MC22TRIPCABLEPROTECTION { get; private set; }
            public bool MC22TRANSFERTRIPSENT { get; private set; }
            public bool MC22TRANSFERTRIPRECEIVED { get; private set; }
            public bool MC22LINETESTFAULTCONTSHORTCIRUIT { get; private set; }
            public bool MC22DRAWERPOSITIONFAULT { get; private set; }
            public bool MC22LINETESTRUNNING { get; private set; }
            public bool MC22LOCALMODE { get; private set; }
            public bool MC22REMOTEMODE { get; private set; }
            public bool MC22CBOPENED { get; private set; }
            public bool MC22CBCLOSED { get; private set; }
            public bool MC22SWITCHONCBPOSSIBLE { get; private set; }
            public bool MC22DRAWERINTESTPOSITION { get; private set; }
            public bool MC22DRAWERINOPERATIONPOSITION { get; private set; }
            public bool MC22DISCONNECTOROPENED { get; private set; }
            public bool MC22DISCONNECTORCLOSED { get; private set; }

            public bool MC41COMMUNICATIONFAULT { get; private set; }
            public bool MC41MCBTRIP { get; private set; }
            public bool MC41DISCONNECTORTROUBLE { get; private set; }
            public bool MC41LOCALMODE { get; private set; }
            public bool MC41REMOTEMODE { get; private set; }
            public bool MC41DISCONNECTOROPENED { get; private set; }
            public bool MC41DISCONNECTORCLOSED { get; private set; }

            public bool MC81TIMESYNDISTURBED { get; private set; }
            public bool MG01DIODETRIP { get; private set; }
            public bool MC81MCBTRIP { get; private set; }
            public bool MG01TEMPWARNING { get; private set; }
            public bool MG01TEMPTRIP { get; private set; }
            public bool MC81FRAMEFAULTPROTECTIONTRIP { get; private set; }
            public bool MC81DISCONNECTORTROUBLE { get; private set; }
            public bool MC81LOCALMODE { get; private set; }
            public bool MC81REMOTEMODE { get; private set; }
            public bool MC81DISCONNECTOROPENED { get; private set; }
            public bool MC81DISCONNECTORCLOSED { get; private set; }

            public float GISH02RSPHASEVOLTAGE { get; private set; }
            public float GISH02STPHASEVOLTAGE { get; private set; }
            public float GISH02TRPHASEVOLTAGE { get; private set; }
            public float GISH02RPHASECURRENT { get; private set; }
            public float GISH02SPHASECURRENT { get; private set; }
            public float GISH02TPHASECURRENT { get; private set; }
            public float GISH02POWERFACTORPF { get; private set; }
            public float GISH02POWERVALUEKW { get; private set; }
            public float GISH02POWERKWH { get; private set; }
            public float GISH02FREQUENCYHZ { get; private set; }
            public float GISH02POWERKVARH { get; private set; }

            public float GISH03RSPHASEVOLTAGE { get; private set; }
            public float GISH03STPHASEVOLTAGE { get; private set; }
            public float GISH03TRPHASEVOLTAGE { get; private set; }
            public float GISH03RPHASECURRENT { get; private set; }
            public float GISH03SPHASECURRENT { get; private set; }
            public float GISH03TPHASECURRENT { get; private set; }
            public float GISH03POWERFACTORPF { get; private set; }
            public float GISH03POWERVALUEKW { get; private set; }
            public float GISH03POWERKWH { get; private set; }
            public float GISH03FREQUENCYHZ { get; private set; }
            public float GISH03POWERKVARH { get; private set; }

            public float GISH04RSPHASEVOLTAGE { get; private set; }
            public float GISH04STPHASEVOLTAGE { get; private set; }
            public float GISH04TRPHASEVOLTAGE { get; private set; }
            public float GISH04RPHASECURRENT { get; private set; }
            public float GISH04SPHASECURRENT { get; private set; }
            public float GISH04TPHASECURRENT { get; private set; }
            public float GISH04POWERFACTORPF { get; private set; }
            public float GISH04POWERVALUEKW { get; private set; }
            public float GISH04POWERKWH { get; private set; }
            public float GISH04FREQUENCYHZ { get; private set; }
            public float GISH04POWERKVARH { get; private set; }

            public float MC21DCVOLTAGE { get; private set; }
            public float MC21DCCURRENT { get; private set; }
            public float MC22DCVOLTAGE { get; private set; }
            public float MC22DCCURRENT { get; private set; }
            public float MC81DCVOLTAGE { get; private set; }
            public float MC81DCCURRENT { get; private set; }

            public bool GISH02GISONCOMMAND { get; private set; }
            public bool GISH02GISOFFCOMMAND { get; private set; }
            public bool GISH03GISONCOMMAND { get; private set; }
            public bool GISH03GISOFFCOMMAND { get; private set; }
            public bool GISH04GISONCOMMAND { get; private set; }
            public bool GISH04GISOFFCOMMAND { get; private set; }

            public bool MC21CBONCOMMAND { get; private set; }
            public bool MC21CBOFFCOMMAND { get; private set; }
            public bool MC21DISCONNECTORONCOMMAND { get; private set; }
            public bool MC21DISCONNECTOROFFCOMMAND { get; private set; }
            public bool MC22CBONCOMMAND { get; private set; }
            public bool MC22CBOFFCOMMAND { get; private set; }
            public bool MC22DISCONNECTORONCOMMAND { get; private set; }
            public bool MC22DISCONNECTOROFFCOMMAND { get; private set; }
            public bool MC41LBSONCOMMAND { get; private set; }
            public bool MC41LBSOFFCOMMAND { get; private set; }
            public bool MC81DISCONNECTORONCOMMAND { get; private set; }
            public bool MC81DISCONNECTOROFFCOMMAND { get; private set; }
        }
    }
}

public class ITMSMessageFormat
{
    public class MsgContent
    {
        public DateTime EventTime { get; set; }
        public string Loaction { get; set; }
        public string TransID { get; set; }
        public string Content { get; set; }
    }

    public ITMSMessageFormat(byte[] dataArray)
    {
        try
        {
            string jsonMessage = Encoding.UTF8.GetString(dataArray);
            MsgContent content = JsonConvert.DeserializeObject<MsgContent>(jsonMessage);

            if ("ITMS".Equals(content.TransID))
            {
                IsItmsMessage = true;
                this._location = content.Loaction;
                this._statusList = JsonConvert.DeserializeObject<List<ExtSysVariableStatusesType>>(content.Content);
            }
            else
            {
                IsItmsMessage = false;
            }
        }
        catch (Exception ex)
        {
            IsItmsMessage = false;
        }
    }

    public void NotifyForm()
    {
        if (this._statusList.Count() > 0)
        {
            if (FormMapper.Keys.Contains(this._location))
                FormMapper[this._location].ReceiveNotify(this._statusList);
        }
    }

    public static void RegistItmsForm(string formKey, IReceiveForm form)
    {
        if (FormMapper.Keys.Contains(formKey))
        {
            FormMapper[formKey] = form;
        }
        else
        {
            FormMapper.Add(formKey, form);
        }

    }

    public static void UnRegistAll()
    {
        FormMapper.Clear();
    }

    #region Field

    private List<ExtSysVariableStatusesType> _statusList;
    private string _location;
    public bool IsItmsMessage { get; private set; }

    private static readonly Dictionary<string, IReceiveForm> FormMapper = new Dictionary<string, IReceiveForm>();

    #endregion



    #region Inner Interface、Class

    public interface IReceiveForm
    {
        void ReceiveNotify(List<ExtSysVariableStatusesType> statusList);
    }

    public class Entity
    {
        public string ID { get; set; }


    }

    public class ExtSysVariableStatusesType : Entity
    {

        public ExtSysVariableStatusesObject EqpType { get; set; }
        public ExtSysVariableStatusesObject EqpID { get; set; }
        public ExtSysVariableStatusesObject Group { get; set; }
        public ExtSysVariableStatusesObject EvarName { get; set; }
        public ExtSysVariableStatusesObject EvarValue { get; set; }
        public ExtSysVariableStatusesObject EvarType { get; set; }
        public ExtSysVariableStatusesObject EvarSec { get; set; }
        public ExtSysVariableStatusesObject EvarMs { get; set; }

    }


    public class ExtSysVariableStatusesObject
    {
        public string value { get; set; }
        public long timestamp { get; set; }
        public bool valid { get; set; }
    }
    #endregion
}
