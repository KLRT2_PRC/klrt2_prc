﻿using PRC.MyClass;
using PRC.Tools;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PRC
{
    public partial class Form_EventLog_LoginAndLogoutEvent : Form 
    {
        public Form_EventLog_LoginAndLogoutEvent()
        {
            InitializeComponent();
        }

        public Form_EventLog_LoginAndLogoutEvent(PermissionObject permission) :this()
        {
            btn_SearchEventLogOfLogin.Enabled = permission.CanRead;
        }

        private void Form_EventLog_LoginAndLogoutEvent_Load(object sender, EventArgs e)
        {
            InitCmbobox();
     
        }

        private void InitCmbobox()
        {
            Dictionary<EventLog_LoginDto.DateRangeSearchType, string> source = new Dictionary<EventLog_LoginDto.DateRangeSearchType, string>()
            {
                [EventLog_LoginDto.DateRangeSearchType.LoginIn] = "登入時間",
                [EventLog_LoginDto.DateRangeSearchType.LoginOut] = "結束時間",
                [EventLog_LoginDto.DateRangeSearchType.LoginInAndLoginOut] = "登入時間與結束時間"

            };

            cmb_DateRangeTyoe.DataSource = source.ToList();
            cmb_DateRangeTyoe.DisplayMember = "Value";
            cmb_DateRangeTyoe.ValueMember = "Key";
        }

        private void btn_SearchEventLogOfLogin_Click(object sender, EventArgs e)
        {
            if (ValidDatesInput())
            {
                EventLog_LoginDto dto = new EventLog_LoginDto()
                {
                    UserID = txt_UserID.Text,
                    StartTime = DateTime.Parse(dtp_StartDate.Value.ToShortDateString()),
                    EndTime = DateTime.Parse(dtp_EndDate.Value.ToShortDateString()),
                    DateSerchType = (EventLog_LoginDto.DateRangeSearchType)cmb_DateRangeTyoe.SelectedValue
                };

                try
                {

                    string url = string.Format(@"http://localhost:48965/api/EventLog_Login?UserID={0}&StartTime={1:yyyy-MM-dd}&EndTime={2:yyyy-MM-dd}&DateSerchType={3}", txt_UserID.Text, dtp_StartDate.Value, dtp_EndDate.Value, (EventLog_LoginDto.DateRangeSearchType)cmb_DateRangeTyoe.SelectedValue);

                    List<LoginRecord> result = WebApiCaller.Json_GetModeByGet<List<LoginRecord>>(url).ContentBody;

                    if (result != null && result.Count > 0)
                    {
                        dataGridView1.DataSource = result.OrderByDescending(X => X.LoginInTime).ToArray();
                        MessageBox.Show("查詢完畢!");
                    }
                    else
                    {
                        dataGridView1.DataSource = new List<LoginRecord>();
                        MessageBox.Show("查無資料!");
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(string.Format("查詢失敗! 失敗訊息:{0}", ex.Message));
                }
            }
            else
            {
                MessageBox.Show("起始時間必須小於或等於結束時間!!!");
            }


        }

        private void btn_SearchEventLogOfLogin_EnabledChanged(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            if (btn.Enabled)
            {
                btn.FlatStyle = FlatStyle.Flat;
                btn.ForeColor = Color.FromArgb(100, 128, 255, 255);
                btn.BackColor = Color.Transparent;
                btn.FlatAppearance.BorderColor = Color.FromArgb(100, 128, 255, 255);

            }
            else
            {
                btn.FlatStyle = FlatStyle.Standard;
            }
        }



        private bool ValidDatesInput()
        {
            return DateTime.Parse(dtp_StartDate.Value.ToShortDateString()) < DateTime.Parse(dtp_EndDate.Value.ToShortDateString());
        }


    }



    public class EventLog_LoginDto
    {
        public DateRangeSearchType DateSerchType { get; set; }
        public string UserID { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }


        //日期範圍查詢的類型
        public enum DateRangeSearchType
        {
            LoginIn,
            LoginOut,
            LoginInAndLoginOut
        }
    }



}
