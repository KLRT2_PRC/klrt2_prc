﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PRC.MyClass;
using System.Net.Sockets;
using ICSCNetwork;
using iSCADA.Design.Utilities.Electrical;
using System.Net;
using System.Diagnostics;
using ICSC.SysUtil.MQ;
using Newtonsoft.Json;
using static PRC.MessageFormat;

namespace PRC
{
    public partial class Form_ElecTSS7 : Form
    {
        protected Socket _socket;
        private ICommClient _portClient;
        private ModbusClient _modbusclient;
        System.Threading.Timer _ThreadingTimer = null;
        System.Timers.Timer _TimersTimer = null;
        private bool _ControlFlag = false;
        private bool _Connected = false;
        private int _transactionId;
        //protected readonly UInt16[] _registerData;
        private TSS7 pck;
        /// ////////////////////////////////////////////////////////////////////////
        MessageFormat.TSS7 _tss7;
        MessageFormat.C15 _c15;
        Form_StationC15 sc15 = new Form_StationC15();
        MainFrame _mf = new MainFrame();
        Form_Total1_backup t1 = new Form_Total1_backup();
        Form_Total2 t2 = new Form_Total2();
        static MSMQ _msq;

        public Form_ElecTSS7()
        {
            InitializeComponent();
            _msq = new MSMQ(MSMQ.QueueType.Private, "Q_Event_WSToPSS");
            //_registerData = new UInt16[65600];
        }

        private void Form_ElecTSS7_Load(object sender, EventArgs e)
        {
            MainFrame._udpserver.RecievedData += _udpserver_RecievedData;
        }

        private void _udpserver_RecievedData(object sender, RecieveDataEventArgs e)
        {
            //throw new NotImplementedException();
            UpdateControls();
        }

        private void UpdateControls()
        {
            if (InvokeRequired)
            {
                BeginInvoke(new Action(UpdateControls));
                return;
            }
            else
            {
                UpdateCTStatus();
            }
        }

        private void UpdateCTStatus()
        {
            //switch (_tss7.AC228KVPower01Normal)
            //{
            //    case true:
            //        lineUC3.State = SymbolState.On;
            //        break;
            //    case false:
            //        lineUC3.State = SymbolState.Error;
            //        break;
            //}

            //switch (_tss7.AC228KVPower02Normal)
            //{
            //    case true:
            //        lineUC4.State = SymbolState.On;
            //        break;
            //    case false:
            //        lineUC4.State = SymbolState.Error;
            //        break;
            //}

            switch09UC1.State = MainFrame._mtss7.HVASLBS1Power == true ?  SymbolState.On :  SymbolState.Error;
            switch09UC2.State = MainFrame._mtss7.HVASLBS2Power == true ? SymbolState.On : SymbolState.Error;
            retangleUC6.State=MainFrame._mtss7.GISH02CBON==true ? SymbolState.On : SymbolState.Error;
            retangleUC5.State= MainFrame._mtss7.GISH03CBON == true? SymbolState.On : SymbolState.Error;
            retangleUC4.State= MainFrame._mtss7.GISH04CBON == true ? SymbolState.On : SymbolState.Error;
            transformer01UC1.State = MainFrame._mtss7.TR1TEMPTRIP == true ? SymbolState.On : SymbolState.Error;
            transformer02UC1.State = MainFrame._mtss7.TR2TEMPTRIP == true ? SymbolState.On : SymbolState.Error;
            switch03UC2.State = MainFrame._mtss7.MC21CBOPENED == true ? SymbolState.Error : SymbolState.On;
            switch05UC2.State = MainFrame._mtss7.MC21DISCONNECTOROPENED == true ? SymbolState.Error : SymbolState.On;
            switch03UC3.State = MainFrame._mtss7.MC22CBOPENED == true ? SymbolState.Error : SymbolState.On;
            switch05UC1.State = MainFrame._mtss7.MC22DISCONNECTOROPENED == true ? SymbolState.Error : SymbolState.On;
            switch05UC3.State = MainFrame._mtss7.MC41DISCONNECTOROPENED == true ? SymbolState.Error : SymbolState.On;

            btnH02GISOn.BackColor = MainFrame._mtss7.GISH02GISONCOMMAND == true ? Color.Red : SystemColors.ControlDark;
            btnH02GISOff.BackColor = MainFrame._mtss7.GISH02GISOFFCOMMAND == true ? Color.Red : SystemColors.ControlDark;
            btnH03GISOn.BackColor = MainFrame._mtss7.GISH03GISONCOMMAND == true ? Color.Red : SystemColors.ControlDark;
            btnH03GISOff.BackColor = MainFrame._mtss7.GISH03GISOFFCOMMAND == true ? Color.Red : SystemColors.ControlDark;
            btnH04GISOn.BackColor = MainFrame._mtss7.GISH04GISONCOMMAND == true ? Color.Red : SystemColors.ControlDark;
            btnH04GISOff.BackColor = MainFrame._mtss7.GISH04GISOFFCOMMAND == true ? Color.Red : SystemColors.ControlDark;
            btnMC21CBOn.BackColor = MainFrame._mtss7.MC21CBONCOMMAND == true ? Color.Red : SystemColors.ControlDark;
            btnMC21CBOff.BackColor = MainFrame._mtss7.MC21CBOFFCOMMAND == true ? Color.Red : SystemColors.ControlDark;
            btnMC21DISCONECTOROn.BackColor = MainFrame._mtss7.MC21DISCONNECTORONCOMMAND == true ? Color.Red : SystemColors.ControlDark;
            btnMC21DISCONECTOROff.BackColor = MainFrame._mtss7.MC21DISCONNECTOROFFCOMMAND == true ? Color.Red : SystemColors.ControlDark;
            btnMC22CBOn.BackColor = MainFrame._mtss7.MC22CBONCOMMAND == true ? Color.Red : SystemColors.ControlDark;
            btnMC22CBOff.BackColor = MainFrame._mtss7.MC22CBOFFCOMMAND == true ? Color.Red : SystemColors.ControlDark;
            btnMC22DISCONECTOROn.BackColor = MainFrame._mtss7.MC22DISCONNECTORONCOMMAND == true ? Color.Red : SystemColors.ControlDark;
            btnMC22DISCONECTOROff.BackColor = MainFrame._mtss7.MC22DISCONNECTOROFFCOMMAND == true ? Color.Red : SystemColors.ControlDark;
            btnMC41LBSOn.BackColor = MainFrame._mtss7.MC41LBSONCOMMAND == true ? Color.Red : SystemColors.ControlDark;
            btnMC41LBSOff.BackColor = MainFrame._mtss7.MC41LBSOFFCOMMAND == true ? Color.Red : SystemColors.ControlDark;
            btnMC81DISCONECTOROn.BackColor = MainFrame._mtss7.MC81DISCONNECTORONCOMMAND == true ? Color.Red : SystemColors.ControlDark;
            btnMC81DISCONECTOROff.BackColor = MainFrame._mtss7.MC81DISCONNECTOROFFCOMMAND == true ? Color.Red : SystemColors.ControlDark;
            if (MainFrame._mtss7.GISH02DSON == true && MainFrame._mtss7.GISH02DSOFF == false)
            {
                switch07UC1.State = SymbolState.On;
            }
            else if (MainFrame._mtss7.GISH02DSON == false && MainFrame._mtss7.GISH02DSOFF == true)
            {
                switch07UC1.State = SymbolState.Default;
            }
            else if (MainFrame._mtss7.GISH02DSON == false && MainFrame._mtss7.GISH02DSOFF == false)
            {
                switch07UC1.State = SymbolState.Error;
            }

            if (MainFrame._mtss7.GISH01PTDSON == true && MainFrame._mtss7.GISH01PTDSOFF == false)
            {
                switch07UC4.State = SymbolState.On;
            }
            else if (MainFrame._mtss7.GISH01PTDSON == false && MainFrame._mtss7.GISH01PTDSOFF == true)
            {
                switch07UC4.State = SymbolState.Default;
            }
            else if (MainFrame._mtss7.GISH01PTDSON == false && MainFrame._mtss7.GISH01PTDSOFF == false)
            {
                switch07UC4.State = SymbolState.Error;
            }

            if (MainFrame._mtss7.GISH01BusESON == true && MainFrame._mtss7.GISH01BusESOFF == false)
            {
                switch06UC1.State = SymbolState.On;
            }
            else if (MainFrame._mtss7.GISH01BusESON == false && MainFrame._mtss7.GISH01BusESOFF == true)
            {
                switch06UC1.State = SymbolState.Error;
            }

            if (MainFrame._mtss7.GISH04DSON == true && MainFrame._mtss7.GISH04DSOFF == false)
            {
                switch07UC3.State = SymbolState.On;
            }
            else if (MainFrame._mtss7.GISH04DSON == false && MainFrame._mtss7.GISH04DSOFF == true)
            {
                switch07UC3.State = SymbolState.Default;
            }
            else if (MainFrame._mtss7.GISH04DSON == false && MainFrame._mtss7.GISH04DSOFF == false)
            {
                switch07UC3.State = SymbolState.Error;
            }

            if (MainFrame._mtss7.GISH03DSON == true && MainFrame._mtss7.GISH03DSOFF == false)
            {
                switch07UC2.State = SymbolState.On;
            }
            else if (MainFrame._mtss7.GISH03DSON == false && MainFrame._mtss7.GISH03DSOFF == true)
            {
                switch07UC2.State = SymbolState.Default;
            }
            else if (MainFrame._mtss7.GISH03DSON == false && MainFrame._mtss7.GISH03DSOFF == false)
            {
                switch07UC2.State = SymbolState.Error;
            }

            switch08UC1.State = MainFrame._mtss7.MG01DIODETRIP == true ? SymbolState.Error : SymbolState.On;
            switch05UC4.State = MainFrame._mtss7.MC81DISCONNECTOROPENED == true ? SymbolState.Error : SymbolState.On;

            label67.Text = MainFrame._mtss7.GISH02RSPHASEVOLTAGE.ToString();
            label68.Text = MainFrame._mtss7.GISH02STPHASEVOLTAGE.ToString();
            label70.Text = MainFrame._mtss7.GISH02TRPHASEVOLTAGE.ToString();
            label72.Text = MainFrame._mtss7.GISH02RPHASECURRENT.ToString();
            label80.Text = MainFrame._mtss7.GISH02SPHASECURRENT.ToString();
            label78.Text = MainFrame._mtss7.GISH02TPHASECURRENT.ToString();
            label76.Text = MainFrame._mtss7.GISH02POWERFACTORPF.ToString();
            label74.Text = MainFrame._mtss7.GISH02POWERVALUEKW.ToString();
            label88.Text = MainFrame._mtss7.GISH02POWERKWH.ToString();
            label86.Text = MainFrame._mtss7.GISH02FREQUENCYHZ.ToString();
            label84.Text = MainFrame._mtss7.GISH02POWERKVARH.ToString();
            label82.Text = MainFrame._mtss7.GISH03RSPHASEVOLTAGE.ToString();
            label96.Text = MainFrame._mtss7.GISH03STPHASEVOLTAGE.ToString();
            label94.Text = MainFrame._mtss7.GISH03TRPHASEVOLTAGE.ToString();
            label92.Text = MainFrame._mtss7.GISH03RPHASECURRENT.ToString();
            label124.Text = MainFrame._mtss7.GISH03SPHASECURRENT.ToString();
            label122.Text = MainFrame._mtss7.GISH03TPHASECURRENT.ToString();
            label120.Text = MainFrame._mtss7.GISH03POWERFACTORPF.ToString();
            label118.Text = MainFrame._mtss7.GISH03POWERVALUEKW.ToString();
            label116.Text = MainFrame._mtss7.GISH03POWERKWH.ToString();
            label114.Text = MainFrame._mtss7.GISH03FREQUENCYHZ.ToString();
            label112.Text = MainFrame._mtss7.GISH03POWERKWH.ToString();
            label110.Text = MainFrame._mtss7.GISH04RSPHASEVOLTAGE.ToString();
            label108.Text = MainFrame._mtss7.GISH04STPHASEVOLTAGE.ToString();
            label106.Text = MainFrame._mtss7.GISH04TRPHASEVOLTAGE.ToString();
            label104.Text = MainFrame._mtss7.GISH04RPHASECURRENT.ToString();
            label102.Text = MainFrame._mtss7.GISH04SPHASECURRENT.ToString();
            label100.Text = MainFrame._mtss7.GISH04TPHASECURRENT.ToString();
            label98.Text = MainFrame._mtss7.GISH04POWERFACTORPF.ToString();
            label90.Text = MainFrame._mtss7.GISH04POWERVALUEKW.ToString();
            label154.Text = MainFrame._mtss7.GISH04POWERKWH.ToString();
            label152.Text = MainFrame._mtss7.GISH04FREQUENCYHZ.ToString();
            label150.Text = MainFrame._mtss7.GISH04POWERKWH.ToString();
            label148.Text = MainFrame._mtss7.MC21DCVOLTAGE.ToString();
            label146.Text = MainFrame._mtss7.MC21DCCURRENT.ToString();
            label144.Text = MainFrame._mtss7.MC22DCVOLTAGE.ToString();
            label142.Text = MainFrame._mtss7.MC22DCCURRENT.ToString();
            label140.Text = MainFrame._mtss7.MC81DCVOLTAGE.ToString();
            label138.Text = MainFrame._mtss7.MC81DCCURRENT.ToString();

            label36.Text = MainFrame._mtss7.SOE.ToString("yyyy/MM/dd HH:mm:ss");
            switch (MainFrame._mtss7.HVASBatteryLow)
            {
                case true:
                    circleUC5.State = SymbolState.Error;
                    circleUC5.Fill = true;
                    break;
                case false:
                    circleUC5.State = SymbolState.On;
                    circleUC5.Fill = true;
                    break;
                default:
                    circleUC5.State = SymbolState.Off;
                    circleUC5.Fill = true;
                    break;
            }

            switch (MainFrame._mtss7.HVASAC220VPowerLoss)
            {
                case true:
                    circleUC6.State = SymbolState.Error;
                    circleUC6.Fill = true;
                    break;
                case false:
                    circleUC6.State = SymbolState.On;
                    circleUC6.Fill = true;
                    break;
                default:
                    circleUC6.State = SymbolState.Off;
                    circleUC6.Fill = true;
                    break;
            }

            switch (MainFrame._mtss7.GISH01MCCBOFFTRIP)
            {
                case true:
                    circleUC7.State = SymbolState.Error;
                    circleUC7.Fill = true;
                    break;
                case false:
                    circleUC7.State = SymbolState.On;
                    circleUC7.Fill = true;
                    break;
                default:
                    circleUC7.State = SymbolState.Off;
                    circleUC7.Fill = true;
                    break;
            }
            switch (MainFrame._mtss7.GISH01BusAircellPressureLev1Alarm)
            {
                case true:
                    circleUC8.State = SymbolState.Error;
                    circleUC8.Fill = true;
                    break;
                case false:
                    circleUC8.State = SymbolState.On;
                    circleUC8.Fill = true;
                    break;
                default:
                    circleUC8.State = SymbolState.Off;
                    circleUC8.Fill = true;
                    break;
            }
            switch (MainFrame._mtss7.GISH01BusAircellPressureLev2Alarm)
            {
                case true:
                    circleUC9.State = SymbolState.Error;
                    circleUC9.Fill = true;
                    break;
                case false:
                    circleUC9.State = SymbolState.On;
                    circleUC9.Fill = true;
                    break;
                default:
                    circleUC9.State = SymbolState.Off;
                    circleUC9.Fill = true;
                    break;
            }
            switch (MainFrame._mtss7.GISH01PTPressureLev1Alarm)
            {
                case true:
                    circleUC10.State = SymbolState.Error;
                    circleUC10.Fill = true;
                    break;
                case false:
                    circleUC10.State = SymbolState.On;
                    circleUC10.Fill = true;
                    break;
                default:
                    circleUC10.State = SymbolState.Off;
                    circleUC10.Fill = true;
                    break;
            }
            switch (MainFrame._mtss7.GISH01PTPressureLev2Alarm)
            {
                case true:
                    circleUC11.State = SymbolState.Error;
                    circleUC11.Fill = true;
                    break;
                case false:
                    circleUC11.State = SymbolState.On;
                    circleUC11.Fill = true;
                    break;
                default:
                    circleUC11.State = SymbolState.Off;
                    circleUC11.Fill = true;
                    break;
            }
            switch (MainFrame._mtss7.PTSecSideFuseFault)
            {
                case true:
                    circleUC12.State = SymbolState.Error;
                    circleUC12.Fill = true;
                    break;
                case false:
                    circleUC12.State = SymbolState.On;
                    circleUC12.Fill = true;
                    break;
                default:
                    circleUC12.State = SymbolState.Off;
                    circleUC12.Fill = true;
                    break;
            }
            switch (MainFrame._mtss7.GISH02MCCBOFFTRIP)
            {
                case true:
                    circleUC13.State = SymbolState.Error;
                    circleUC13.Fill = true;
                    break;
                case false:
                    circleUC13.State = SymbolState.On;
                    circleUC13.Fill = true;
                    break;
                default:
                    circleUC13.State = SymbolState.Off;
                    circleUC13.Fill = true;
                    break;
            }
            switch (MainFrame._mtss7.GISH02GISREMOTEMODE)
            {
                case true:
                    circleUC14.State = SymbolState.Error;
                    circleUC14.Fill = true;
                    break;
                case false:
                    circleUC14.State = SymbolState.On;
                    circleUC14.Fill = true;
                    break;
                default:
                    circleUC14.State = SymbolState.Off;
                    circleUC14.Fill = true;
                    break;
            }
            switch (MainFrame._mtss7.GISH02GISLOCALMODE)
            {
                case true:
                    circleUC15.State = SymbolState.Error;
                    circleUC15.Fill = true;
                    break;
                case false:
                    circleUC15.State = SymbolState.On;
                    circleUC15.Fill = true;
                    break;
                default:
                    circleUC15.State = SymbolState.Off;
                    circleUC15.Fill = true;
                    break;
            }
            switch (MainFrame._mtss7.GISH02CBON)
            {
                case true:
                    circleUC16.State = SymbolState.Error;
                    circleUC16.Fill = true;
                    break;
                case false:
                    circleUC16.State = SymbolState.On;
                    circleUC16.Fill = true;
                    break;
                default:
                    circleUC16.State = SymbolState.Off;
                    circleUC16.Fill = true;
                    break;
            }
            switch (MainFrame._mtss7.GISH02CBOFF)
            {
                case true:
                    circleUC17.State = SymbolState.Error;
                    circleUC17.Fill = true;
                    break;
                case false:
                    circleUC17.State = SymbolState.On;
                    circleUC17.Fill = true;
                    break;
                default:
                    circleUC17.State = SymbolState.Off;
                    circleUC17.Fill = true;
                    break;
            }
            switch (MainFrame._mtss7.GISH02CBPressureLev1Alarm)
            {
                case true:
                    circleUC18.State = SymbolState.Error;
                    circleUC18.Fill = true;
                    break;
                case false:
                    circleUC18.State = SymbolState.On;
                    circleUC18.Fill = true;
                    break;
                default:
                    circleUC18.State = SymbolState.Off;
                    circleUC18.Fill = true;
                    break;
            }
            switch (MainFrame._mtss7.GISH02CBPressureLev2Alarm)
            {
                case true:
                    circleUC19.State = SymbolState.Error;
                    circleUC19.Fill = true;
                    break;
                case false:
                    circleUC19.State = SymbolState.On;
                    circleUC19.Fill = true;
                    break;
                default:
                    circleUC19.State = SymbolState.Off;
                    circleUC19.Fill = true;
                    break;
            }
            switch (MainFrame._mtss7.GISH02GCBPressureLowAlarm)
            {
                case true:
                    circleUC20.State = SymbolState.Error;
                    circleUC20.Fill = true;
                    break;
                case false:
                    circleUC20.State = SymbolState.On;
                    circleUC20.Fill = true;
                    break;
                default:
                    circleUC20.State = SymbolState.Off;
                    circleUC20.Fill = true;
                    break;
            }
            switch (MainFrame._mtss7.GISH02GCBPressureLowLATCHAlarm)
            {
                case true:
                    circleUC21.State = SymbolState.Error;
                    circleUC21.Fill = true;
                    break;
                case false:
                    circleUC21.State = SymbolState.On;
                    circleUC21.Fill = true;
                    break;
                default:
                    circleUC21.State = SymbolState.Off;
                    circleUC21.Fill = true;
                    break;
            }
            switch (MainFrame._mtss7.GISH02IEDRELAYFAULTALARM)
            {
                case true:
                    circleUC22.State = SymbolState.Error;
                    circleUC22.Fill = true;
                    break;
                case false:
                    circleUC22.State = SymbolState.On;
                    circleUC22.Fill = true;
                    break;
                default:
                    circleUC22.State = SymbolState.Off;
                    circleUC22.Fill = true;
                    break;
            }
            switch (MainFrame._mtss7.GISH02IEDRELAY50)
            {
                case true:
                    circleUC23.State = SymbolState.Error;
                    circleUC23.Fill = true;
                    break;
                case false:
                    circleUC23.State = SymbolState.On;
                    circleUC23.Fill = true;
                    break;
                default:
                    circleUC23.State = SymbolState.Off;
                    circleUC23.Fill = true;
                    break;
            }
            switch (MainFrame._mtss7.GISH02IEDRELAY50N)
            {
                case true:
                    circleUC24.State = SymbolState.Error;
                    circleUC24.Fill = true;
                    break;
                case false:
                    circleUC24.State = SymbolState.On;
                    circleUC24.Fill = true;
                    break;
                default:
                    circleUC24.State = SymbolState.Off;
                    circleUC24.Fill = true;
                    break;
            }
            switch (MainFrame._mtss7.GISH02IEDRELAY51)
            {
                case true:
                    circleUC25.State = SymbolState.Error;
                    circleUC25.Fill = true;
                    break;
                case false:
                    circleUC25.State = SymbolState.On;
                    circleUC25.Fill = true;
                    break;
                default:
                    circleUC25.State = SymbolState.Off;
                    circleUC25.Fill = true;
                    break;
            }
            switch (MainFrame._mtss7.GISH02IEDRELAY51N)
            {
                case true:
                    circleUC26.State = SymbolState.Error;
                    circleUC26.Fill = true;
                    break;
                case false:
                    circleUC26.State = SymbolState.On;
                    circleUC26.Fill = true;
                    break;
                default:
                    circleUC26.State = SymbolState.Off;
                    circleUC26.Fill = true;
                    break;
            }
            switch (MainFrame._mtss7.GISH02CBTRIP)
            {
                case true:
                    circleUC27.State = SymbolState.Error;
                    circleUC27.Fill = true;
                    break;
                case false:
                    circleUC27.State = SymbolState.On;
                    circleUC27.Fill = true;
                    break;
                default:
                    circleUC27.State = SymbolState.Off;
                    circleUC27.Fill = true;
                    break;
            }
            switch (MainFrame._mtss7.GISH03MCCBOFFTRIP)
            {
                case true:
                    circleUC28.State = SymbolState.Error;
                    circleUC28.Fill = true;
                    break;
                case false:
                    circleUC28.State = SymbolState.On;
                    circleUC28.Fill = true;
                    break;
                default:
                    circleUC28.State = SymbolState.Off;
                    circleUC28.Fill = true;
                    break;
            }
            switch (MainFrame._mtss7.GISH03GISREMOTEMODE)
            {
                case true:
                    circleUC29.State = SymbolState.Error;
                    circleUC29.Fill = true;
                    break;
                case false:
                    circleUC29.State = SymbolState.On;
                    circleUC29.Fill = true;
                    break;
                default:
                    circleUC29.State = SymbolState.Off;
                    circleUC29.Fill = true;
                    break;
            }
            switch (MainFrame._mtss7.GISH03GISLOCALMODE)
            {
                case true:
                    circleUC30.State = SymbolState.Error;
                    circleUC30.Fill = true;
                    break;
                case false:
                    circleUC30.State = SymbolState.On;
                    circleUC30.Fill = true;
                    break;
                default:
                    circleUC30.State = SymbolState.Off;
                    circleUC30.Fill = true;
                    break;
            }
            switch (MainFrame._mtss7.GISH03CBON)
            {
                case true:
                    circleUC31.State = SymbolState.Error;
                    circleUC31.Fill = true;
                    break;
                case false:
                    circleUC31.State = SymbolState.On;
                    circleUC31.Fill = true;
                    break;
                default:
                    circleUC31.State = SymbolState.Off;
                    circleUC31.Fill = true;
                    break;
            }
            switch (MainFrame._mtss7.GISH03CBOFF)
            {
                case true:
                    circleUC32.State = SymbolState.Error;
                    circleUC32.Fill = true;
                    break;
                case false:
                    circleUC32.State = SymbolState.On;
                    circleUC32.Fill = true;
                    break;
                default:
                    circleUC32.State = SymbolState.Off;
                    circleUC32.Fill = true;
                    break;
            }
            switch (MainFrame._mtss7.GISH03CBPressureLev1Alarm)
            {
                case true:
                    circleUC33.State = SymbolState.Error;
                    circleUC33.Fill = true;
                    break;
                case false:
                    circleUC33.State = SymbolState.On;
                    circleUC33.Fill = true;
                    break;
                default:
                    circleUC33.State = SymbolState.Off;
                    circleUC33.Fill = true;
                    break;
            }
            switch (MainFrame._mtss7.GISH03CBPressureLev2Alarm)
            {
                case true:
                    circleUC34.State = SymbolState.Error;
                    circleUC34.Fill = true;
                    break;
                case false:
                    circleUC34.State = SymbolState.On;
                    circleUC34.Fill = true;
                    break;
                default:
                    circleUC34.State = SymbolState.Off;
                    circleUC34.Fill = true;
                    break;
            }
            switch (MainFrame._mtss7.GISH02IEDRELAY27)
            {
                case true:
                    circleUC36.State = SymbolState.Error;
                    circleUC36.Fill = true;
                    break;
                case false:
                    circleUC36.State = SymbolState.On;
                    circleUC36.Fill = true;
                    break;
                default:
                    circleUC36.State = SymbolState.Off;
                    circleUC36.Fill = true;
                    break;
            }
            switch (MainFrame._mtss7.GISH02IEDRELAY59)
            {
                case true:
                    circleUC37.State = SymbolState.Error;
                    circleUC37.Fill = true;
                    break;
                case false:
                    circleUC37.State = SymbolState.On;
                    circleUC37.Fill = true;
                    break;
                default:
                    circleUC37.State = SymbolState.Off;
                    circleUC37.Fill = true;
                    break;
            }

            if (MainFrame._mtss7.GISH02GISLOCALMODE)
            {
                btnH02GISOn.Enabled = false;
                btnH02GISOff.Enabled = false;
            }
            else
            {
                btnH02GISOn.Enabled = true;
                btnH02GISOff.Enabled = true;
            }
            if (MainFrame._mtss7.GISH03GISLOCALMODE)
            {
                btnH03GISOn.Enabled = false;
                btnH03GISOff.Enabled = false;
            }
            else
            {
                btnH03GISOn.Enabled = true;
                btnH03GISOff.Enabled = true;
            }
            if (MainFrame._mtss7.GISH04GISLOCALMODE)
            {
                btnH04GISOn.Enabled = false;
                btnH04GISOff.Enabled = false;
            }
            else
            {
                btnH04GISOn.Enabled = true;
                btnH04GISOff.Enabled = true;
            }
            if (MainFrame._mtss7.MC21LOCALMODE)
            {
                btnMC21CBOn.Enabled = false;
                btnMC21CBOff.Enabled = false;
            }
            else
            {
                btnMC21CBOn.Enabled = true;
                btnMC21CBOff.Enabled = true;
            }
            if (MainFrame._mtss7.MC22LOCALMODE)
            {
                btnMC22CBOn.Enabled = false;
                btnMC22CBOff.Enabled = false;
            }
            else
            {
                btnMC22CBOn.Enabled = true;
                btnMC22CBOff.Enabled = true;
            }
            if (MainFrame._mtss7.MC41LOCALMODE)
            {
                btnMC41LBSOn.Enabled = false;
                btnMC41LBSOff.Enabled = false;
            }
            else
            {
                btnMC41LBSOn.Enabled = true;
                btnMC41LBSOff.Enabled = true;
            }
        }

        private void btn_click(object sender, EventArgs e)
        {
            switch (((Button)sender).Name)
            {
                case "btnH02GISOn":
                    btnH02GISOn.BackColor = Color.Red;
                    btnH02GISOff.BackColor = SystemColors.ControlDark;
                    ExecuteWriteCommand(103, 9, 256);
                    break;
                case "btnH02GISOff":
                    btnH02GISOn.BackColor = SystemColors.ControlDark;
                    btnH02GISOff.BackColor = Color.Red;
                    ExecuteWriteCommand(103, 10, 512);
                    break;
                case "btnH03GISOn":
                    btnH03GISOn.BackColor = Color.Red;
                    btnH03GISOff.BackColor = SystemColors.ControlDark;
                    ExecuteWriteCommand(103, 11, 1024);
                    break;
                case "btnH03GISOff":
                    btnH03GISOn.BackColor = SystemColors.ControlDark;
                    btnH03GISOff.BackColor = Color.Red;
                    ExecuteWriteCommand(103, 12, 2048);
                    break;
                case "btnH04GISOn":
                    btnH04GISOn.BackColor = Color.Red;
                    btnH04GISOff.BackColor = SystemColors.ControlDark;
                    ExecuteWriteCommand(103, 13, 4096);
                    break;
                case "btnH04GISOff":
                    btnH04GISOn.BackColor = SystemColors.ControlDark;
                    btnH04GISOff.BackColor = Color.Red;
                    ExecuteWriteCommand(103, 14, 8192);
                    break;
                case "btnMC21CBOn":
                    btnMC21CBOn.BackColor = Color.Red;
                    btnMC21CBOff.BackColor = SystemColors.ControlDark;
                    ExecuteWriteCommand(103, 1, 1);
                    break;
                case "btnMC21CBOff":
                    btnMC21CBOn.BackColor = SystemColors.ControlDark;
                    btnMC21CBOff.BackColor = Color.Red;
                    ExecuteWriteCommand(103, 2, 2);
                    break;
                case "btnMC21DISCONECTOROn":
                    btnMC21DISCONECTOROn.BackColor = Color.Red;
                    btnMC21DISCONECTOROff.BackColor = SystemColors.ControlDark;
                    ExecuteWriteCommand(103, 3, 4);
                    break;
                case "btnMC21DISCONECTOROff":
                    btnMC21DISCONECTOROn.BackColor = SystemColors.ControlDark;
                    btnMC21DISCONECTOROff.BackColor = Color.Red;
                    ExecuteWriteCommand(103, 4, 8);
                    break;
                case "btnMC22CBOn":
                    btnMC22CBOn.BackColor = Color.Red;
                    btnMC22CBOff.BackColor = SystemColors.ControlDark;
                    ExecuteWriteCommand(103, 5, 16);
                    break;
                case "btnMC22CBOff":
                    btnMC22CBOn.BackColor = SystemColors.ControlDark;
                    btnMC22CBOff.BackColor = Color.Red;
                    ExecuteWriteCommand(103, 6, 32);
                    break;
                case "btnMC22DISCONECTOROn":
                    btnMC22DISCONECTOROn.BackColor = Color.Red;
                    btnMC22DISCONECTOROff.BackColor = SystemColors.ControlDark;
                    ExecuteWriteCommand(103, 7, 64);
                    break;
                case "btnMC22DISCONECTOROff":
                    btnMC22DISCONECTOROn.BackColor = SystemColors.ControlDark;
                    btnMC22DISCONECTOROff.BackColor = Color.Red;
                    ExecuteWriteCommand(103, 8, 128);
                    break;
                case "btnMC41LBSOn":
                    btnMC41LBSOn.BackColor = Color.Red;
                    btnMC41LBSOff.BackColor = SystemColors.ControlDark;
                    ExecuteWriteCommand(104, 9, 256);
                    break;
                case "btnMC41LBSOff":
                    btnMC41LBSOn.BackColor = SystemColors.ControlDark;
                    btnMC41LBSOff.BackColor = Color.Red;
                    ExecuteWriteCommand(104, 10, 512);
                    break;
                case "btnMC81DISCONECTOROn":
                    btnMC81DISCONECTOROn.BackColor = Color.Red;
                    btnMC81DISCONECTOROff.BackColor = SystemColors.ControlDark;
                    ExecuteWriteCommand(104, 13, 4096);
                    break;
                case "btnMC81DISCONECTOROff":
                    btnMC81DISCONECTOROn.BackColor = SystemColors.ControlDark;
                    btnMC81DISCONECTOROff.BackColor = Color.Red;
                    ExecuteWriteCommand(104, 14, 8192);
                    break;
            }
        }

        private void ExecuteWriteCommand(int StartAddress,int num, int value)
        {
            try
            {
                var command = new ModBusCommand(ModBusCommand.FuncWriteSingleRegister)
                {
                    Offset = StartAddress,
                    Count = num,
                    TransId = _transactionId++,
                    Data = new ushort[1]
                };
                command.Data[0] = (ushort)(value);
                MsgContent _msgobj = new MsgContent("PRC", JsonConvert.SerializeObject(command));
                _msq.SendMsg(JsonConvert.SerializeObject(_msgobj));
                //var result = _modbusclient.ExecuteGeneric(_portClient, command);
            }
            catch
            { }
        }

        private void Form_ElecTSS7_FormClosing(object sender, FormClosingEventArgs e)
        {
            //_tcpsvr.Stop();
        }
    }
}
