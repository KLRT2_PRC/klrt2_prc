﻿using iSCADA.Design.Utilities.Electrical;
using iSCADA.Design.Utilities.SIG;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PRC
{
    public partial class Form_SignalC16 : Form, ITMSMessageFormat.IReceiveForm
    {
        public Form_SignalC16()
        {
            InitializeComponent();
        }

        private void Form_SignalC16_Load(object sender, EventArgs e)
        {
            circleUC25.Fill = true;
            circleUC4.Fill = true;
            circleUC22.Fill = true;
            circleUC6.Fill = true;
            circleUC16.Fill = true;
            circleUC10.Fill = true;
            circleUC12.Fill = true;
            circleUC18.Fill = true;
            circleUC1.Fill = true;
        }

        public void ReceiveNotify(List<ITMSMessageFormat.ExtSysVariableStatusesType> statusList)
        {
       
            Action refreshUIAct = () => {
                foreach (Control contorl in this.tabPage1.Controls)
                {
                    if (contorl is ElectricalSymbol)
                    {
                        ElectricalSymbol symbol = contorl as ElectricalSymbol;
                        if (!string.IsNullOrEmpty(symbol.GroupID))
                        {
                            if (GetEntityValue(symbol.GroupID, statusList) != null)
                            {
                                if (int.Parse(GetEntityValue(symbol.GroupID, statusList).EvarValue.value) == 1)
                                {
                                    if (symbol is CircleUC)
                                    {
                                        symbol.State = iSCADA.Design.Utilities.Electrical.SymbolState.On;
                                    }
                                    else
                                    {
                                        symbol.State = iSCADA.Design.Utilities.Electrical.SymbolState.Error;
                                    }
                                  
                                }
                            }
                        }
                    }
                    else if (contorl is SIGSymbol)
                    {
                        SIGSymbol symbol = contorl as SIGSymbol;
                        if (!string.IsNullOrEmpty(symbol.GroupID))
                        {
                            if (GetEntityValue(symbol.GroupID, statusList) != null)
                            {
                                if (int.Parse(GetEntityValue(symbol.GroupID, statusList).EvarValue.value) == 1)
                                {
                                 
                                    symbol.State = iSCADA.Design.Utilities.SIG.SymbolState.Error;
                                }
                            }

                        }

                    }
                }
            };

            if (InvokeRequired)
            {
                this.Invoke(refreshUIAct);
            }
            else
            {
                refreshUIAct();
            }


        }

        private ITMSMessageFormat.ExtSysVariableStatusesType GetEntityValue(string groupID, List<ITMSMessageFormat.ExtSysVariableStatusesType> statusList)
        {
            string gID = groupID.Replace("-", "");
            foreach (ITMSMessageFormat.ExtSysVariableStatusesType entity in statusList)
            {

                if (gID.Equals(string.Format("{0}{1}", entity.Group.value, entity.EqpID.value)))
                {
                    return entity;
                }

            }
            return null;
        }

     
    }
}
