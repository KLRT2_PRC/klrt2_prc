﻿namespace PRC
{
    partial class Form_BMS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel42 = new System.Windows.Forms.Panel();
            this.retangleUC14 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.panel41 = new System.Windows.Forms.Panel();
            this.label46 = new System.Windows.Forms.Label();
            this.panel40 = new System.Windows.Forms.Panel();
            this.panel39 = new System.Windows.Forms.Panel();
            this.retangleUC13 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.panel38 = new System.Windows.Forms.Panel();
            this.label45 = new System.Windows.Forms.Label();
            this.panel37 = new System.Windows.Forms.Panel();
            this.panel36 = new System.Windows.Forms.Panel();
            this.retangleUC12 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.panel35 = new System.Windows.Forms.Panel();
            this.label44 = new System.Windows.Forms.Label();
            this.panel34 = new System.Windows.Forms.Panel();
            this.panel33 = new System.Windows.Forms.Panel();
            this.retangleUC11 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.panel32 = new System.Windows.Forms.Panel();
            this.label43 = new System.Windows.Forms.Label();
            this.panel31 = new System.Windows.Forms.Panel();
            this.panel30 = new System.Windows.Forms.Panel();
            this.retangleUC10 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.panel29 = new System.Windows.Forms.Panel();
            this.label42 = new System.Windows.Forms.Label();
            this.panel28 = new System.Windows.Forms.Panel();
            this.panel27 = new System.Windows.Forms.Panel();
            this.retangleUC9 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.panel26 = new System.Windows.Forms.Panel();
            this.label41 = new System.Windows.Forms.Label();
            this.panel25 = new System.Windows.Forms.Panel();
            this.panel24 = new System.Windows.Forms.Panel();
            this.retangleUC8 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.panel23 = new System.Windows.Forms.Panel();
            this.label40 = new System.Windows.Forms.Label();
            this.panel22 = new System.Windows.Forms.Panel();
            this.panel21 = new System.Windows.Forms.Panel();
            this.retangleUC7 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.panel20 = new System.Windows.Forms.Panel();
            this.label39 = new System.Windows.Forms.Label();
            this.panel19 = new System.Windows.Forms.Panel();
            this.panel18 = new System.Windows.Forms.Panel();
            this.retangleUC6 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.panel17 = new System.Windows.Forms.Panel();
            this.label38 = new System.Windows.Forms.Label();
            this.panel16 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.retangleUC5 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.panel14 = new System.Windows.Forms.Panel();
            this.label37 = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.retangleUC4 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.panel11 = new System.Windows.Forms.Panel();
            this.label36 = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.retangleUC3 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label35 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.retangleUC1 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label34 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label20 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.panel49 = new System.Windows.Forms.Panel();
            this.panel50 = new System.Windows.Forms.Panel();
            this.panel51 = new System.Windows.Forms.Panel();
            this.panel52 = new System.Windows.Forms.Panel();
            this.panel53 = new System.Windows.Forms.Panel();
            this.panel54 = new System.Windows.Forms.Panel();
            this.panel55 = new System.Windows.Forms.Panel();
            this.panel56 = new System.Windows.Forms.Panel();
            this.panel57 = new System.Windows.Forms.Panel();
            this.panel58 = new System.Windows.Forms.Panel();
            this.panel59 = new System.Windows.Forms.Panel();
            this.panel60 = new System.Windows.Forms.Panel();
            this.panel61 = new System.Windows.Forms.Panel();
            this.panel62 = new System.Windows.Forms.Panel();
            this.panel63 = new System.Windows.Forms.Panel();
            this.panel64 = new System.Windows.Forms.Panel();
            this.panel65 = new System.Windows.Forms.Panel();
            this.panel66 = new System.Windows.Forms.Panel();
            this.panel67 = new System.Windows.Forms.Panel();
            this.panel68 = new System.Windows.Forms.Panel();
            this.panel69 = new System.Windows.Forms.Panel();
            this.panel70 = new System.Windows.Forms.Panel();
            this.panel71 = new System.Windows.Forms.Panel();
            this.panel72 = new System.Windows.Forms.Panel();
            this.panel73 = new System.Windows.Forms.Panel();
            this.panel74 = new System.Windows.Forms.Panel();
            this.panel75 = new System.Windows.Forms.Panel();
            this.panel76 = new System.Windows.Forms.Panel();
            this.panel77 = new System.Windows.Forms.Panel();
            this.panel78 = new System.Windows.Forms.Panel();
            this.panel79 = new System.Windows.Forms.Panel();
            this.panel80 = new System.Windows.Forms.Panel();
            this.panel81 = new System.Windows.Forms.Panel();
            this.panel82 = new System.Windows.Forms.Panel();
            this.panel83 = new System.Windows.Forms.Panel();
            this.panel84 = new System.Windows.Forms.Panel();
            this.panel85 = new System.Windows.Forms.Panel();
            this.panel86 = new System.Windows.Forms.Panel();
            this.panel87 = new System.Windows.Forms.Panel();
            this.panel88 = new System.Windows.Forms.Panel();
            this.label31 = new System.Windows.Forms.Label();
            this.panel89 = new System.Windows.Forms.Panel();
            this.label33 = new System.Windows.Forms.Label();
            this.panel90 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.retangleUC26 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC25 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label21 = new System.Windows.Forms.Label();
            this.retangleUC24 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.retangleUC23 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC22 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC21 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC20 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC19 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC18 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC17 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC16 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC2 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label16 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.retangleUC15 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label32 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel42.SuspendLayout();
            this.panel41.SuspendLayout();
            this.panel40.SuspendLayout();
            this.panel39.SuspendLayout();
            this.panel38.SuspendLayout();
            this.panel37.SuspendLayout();
            this.panel36.SuspendLayout();
            this.panel35.SuspendLayout();
            this.panel34.SuspendLayout();
            this.panel33.SuspendLayout();
            this.panel32.SuspendLayout();
            this.panel31.SuspendLayout();
            this.panel30.SuspendLayout();
            this.panel29.SuspendLayout();
            this.panel28.SuspendLayout();
            this.panel27.SuspendLayout();
            this.panel26.SuspendLayout();
            this.panel25.SuspendLayout();
            this.panel24.SuspendLayout();
            this.panel23.SuspendLayout();
            this.panel22.SuspendLayout();
            this.panel21.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel55.SuspendLayout();
            this.panel57.SuspendLayout();
            this.panel58.SuspendLayout();
            this.panel59.SuspendLayout();
            this.panel60.SuspendLayout();
            this.panel61.SuspendLayout();
            this.panel62.SuspendLayout();
            this.panel63.SuspendLayout();
            this.panel64.SuspendLayout();
            this.panel65.SuspendLayout();
            this.panel66.SuspendLayout();
            this.panel67.SuspendLayout();
            this.panel68.SuspendLayout();
            this.panel69.SuspendLayout();
            this.panel70.SuspendLayout();
            this.panel71.SuspendLayout();
            this.panel72.SuspendLayout();
            this.panel73.SuspendLayout();
            this.panel74.SuspendLayout();
            this.panel75.SuspendLayout();
            this.panel76.SuspendLayout();
            this.panel77.SuspendLayout();
            this.panel78.SuspendLayout();
            this.panel79.SuspendLayout();
            this.panel80.SuspendLayout();
            this.panel81.SuspendLayout();
            this.panel82.SuspendLayout();
            this.panel83.SuspendLayout();
            this.panel84.SuspendLayout();
            this.panel85.SuspendLayout();
            this.panel86.SuspendLayout();
            this.panel87.SuspendLayout();
            this.panel88.SuspendLayout();
            this.panel89.SuspendLayout();
            this.panel90.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(133)))), ((int)(((byte)(132)))));
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel42, 2, 13);
            this.tableLayoutPanel1.Controls.Add(this.panel41, 1, 13);
            this.tableLayoutPanel1.Controls.Add(this.panel40, 0, 13);
            this.tableLayoutPanel1.Controls.Add(this.panel39, 2, 12);
            this.tableLayoutPanel1.Controls.Add(this.panel38, 1, 12);
            this.tableLayoutPanel1.Controls.Add(this.panel37, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.panel36, 2, 11);
            this.tableLayoutPanel1.Controls.Add(this.panel35, 1, 11);
            this.tableLayoutPanel1.Controls.Add(this.panel34, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.panel33, 2, 10);
            this.tableLayoutPanel1.Controls.Add(this.panel32, 1, 10);
            this.tableLayoutPanel1.Controls.Add(this.panel31, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.panel30, 2, 9);
            this.tableLayoutPanel1.Controls.Add(this.panel29, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.panel28, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.panel27, 2, 8);
            this.tableLayoutPanel1.Controls.Add(this.panel26, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.panel25, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.panel24, 2, 7);
            this.tableLayoutPanel1.Controls.Add(this.panel23, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.panel22, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.panel21, 2, 6);
            this.tableLayoutPanel1.Controls.Add(this.panel20, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.panel19, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.panel18, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.panel17, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.panel16, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.panel15, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.panel14, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.panel13, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.panel12, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.panel11, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.panel10, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.panel9, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel8, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel7, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel6, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel5, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel4, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel3, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(139, 136);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 14;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(750, 720);
            this.tableLayoutPanel1.TabIndex = 0;
            this.tableLayoutPanel1.CellPaint += new System.Windows.Forms.TableLayoutCellPaintEventHandler(this.tableLayoutPanel1_CellPaint);
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel1.Controls.Add(this.label17);
            this.panel1.Location = new System.Drawing.Point(4, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(217, 44);
            this.panel1.TabIndex = 2;
            // 
            // panel42
            // 
            this.panel42.Controls.Add(this.retangleUC14);
            this.panel42.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel42.Location = new System.Drawing.Point(528, 653);
            this.panel42.Name = "panel42";
            this.panel42.Size = new System.Drawing.Size(219, 64);
            this.panel42.TabIndex = 43;
            // 
            // retangleUC14
            // 
            this.retangleUC14.BackColor = System.Drawing.Color.Lime;
            this.retangleUC14.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC14.ExtenderWidth = 1;
            this.retangleUC14.Fill = false;
            this.retangleUC14.GroupID = null;
            this.retangleUC14.Location = new System.Drawing.Point(87, 6);
            this.retangleUC14.Name = "retangleUC14";
            this.retangleUC14.Size = new System.Drawing.Size(32, 32);
            this.retangleUC14.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC14.TabIndex = 11;
            // 
            // panel41
            // 
            this.panel41.Controls.Add(this.label46);
            this.panel41.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel41.Location = new System.Drawing.Point(228, 653);
            this.panel41.Name = "panel41";
            this.panel41.Size = new System.Drawing.Size(294, 64);
            this.panel41.TabIndex = 42;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label46.Location = new System.Drawing.Point(88, 9);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(96, 26);
            this.label46.TabIndex = 6;
            this.label46.Text = "鼎山街站";
            // 
            // panel40
            // 
            this.panel40.Controls.Add(this.label15);
            this.panel40.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel40.Location = new System.Drawing.Point(3, 653);
            this.panel40.Name = "panel40";
            this.panel40.Size = new System.Drawing.Size(219, 64);
            this.panel40.TabIndex = 41;
            // 
            // panel39
            // 
            this.panel39.Controls.Add(this.retangleUC13);
            this.panel39.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel39.Location = new System.Drawing.Point(528, 603);
            this.panel39.Name = "panel39";
            this.panel39.Size = new System.Drawing.Size(219, 44);
            this.panel39.TabIndex = 40;
            // 
            // retangleUC13
            // 
            this.retangleUC13.BackColor = System.Drawing.Color.Lime;
            this.retangleUC13.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC13.ExtenderWidth = 1;
            this.retangleUC13.Fill = false;
            this.retangleUC13.GroupID = null;
            this.retangleUC13.Location = new System.Drawing.Point(87, 6);
            this.retangleUC13.Name = "retangleUC13";
            this.retangleUC13.Size = new System.Drawing.Size(32, 32);
            this.retangleUC13.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC13.TabIndex = 10;
            // 
            // panel38
            // 
            this.panel38.Controls.Add(this.label45);
            this.panel38.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel38.Location = new System.Drawing.Point(228, 603);
            this.panel38.Name = "panel38";
            this.panel38.Size = new System.Drawing.Size(294, 44);
            this.panel38.TabIndex = 39;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label45.Location = new System.Drawing.Point(88, 9);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(96, 26);
            this.label45.TabIndex = 6;
            this.label45.Text = "灣仔內站";
            // 
            // panel37
            // 
            this.panel37.Controls.Add(this.label14);
            this.panel37.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel37.Location = new System.Drawing.Point(3, 603);
            this.panel37.Name = "panel37";
            this.panel37.Size = new System.Drawing.Size(219, 44);
            this.panel37.TabIndex = 38;
            // 
            // panel36
            // 
            this.panel36.Controls.Add(this.retangleUC12);
            this.panel36.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel36.Location = new System.Drawing.Point(528, 553);
            this.panel36.Name = "panel36";
            this.panel36.Size = new System.Drawing.Size(219, 44);
            this.panel36.TabIndex = 37;
            // 
            // retangleUC12
            // 
            this.retangleUC12.BackColor = System.Drawing.Color.Lime;
            this.retangleUC12.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC12.ExtenderWidth = 1;
            this.retangleUC12.Fill = false;
            this.retangleUC12.GroupID = null;
            this.retangleUC12.Location = new System.Drawing.Point(87, 6);
            this.retangleUC12.Name = "retangleUC12";
            this.retangleUC12.Size = new System.Drawing.Size(32, 32);
            this.retangleUC12.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC12.TabIndex = 9;
            // 
            // panel35
            // 
            this.panel35.Controls.Add(this.label44);
            this.panel35.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel35.Location = new System.Drawing.Point(228, 553);
            this.panel35.Name = "panel35";
            this.panel35.Size = new System.Drawing.Size(294, 44);
            this.panel35.TabIndex = 36;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label44.Location = new System.Drawing.Point(88, 9);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(117, 26);
            this.label44.TabIndex = 6;
            this.label44.Text = "新上國小站";
            // 
            // panel34
            // 
            this.panel34.Controls.Add(this.label13);
            this.panel34.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel34.Location = new System.Drawing.Point(3, 553);
            this.panel34.Name = "panel34";
            this.panel34.Size = new System.Drawing.Size(219, 44);
            this.panel34.TabIndex = 35;
            // 
            // panel33
            // 
            this.panel33.Controls.Add(this.retangleUC11);
            this.panel33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel33.Location = new System.Drawing.Point(528, 503);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(219, 44);
            this.panel33.TabIndex = 34;
            // 
            // retangleUC11
            // 
            this.retangleUC11.BackColor = System.Drawing.Color.Lime;
            this.retangleUC11.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC11.ExtenderWidth = 1;
            this.retangleUC11.Fill = false;
            this.retangleUC11.GroupID = null;
            this.retangleUC11.Location = new System.Drawing.Point(87, 6);
            this.retangleUC11.Name = "retangleUC11";
            this.retangleUC11.Size = new System.Drawing.Size(32, 32);
            this.retangleUC11.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC11.TabIndex = 8;
            // 
            // panel32
            // 
            this.panel32.Controls.Add(this.label43);
            this.panel32.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel32.Location = new System.Drawing.Point(228, 503);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(294, 44);
            this.panel32.TabIndex = 33;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label43.Location = new System.Drawing.Point(88, 9);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(117, 26);
            this.label43.TabIndex = 6;
            this.label43.Text = "愛河之心站";
            // 
            // panel31
            // 
            this.panel31.Controls.Add(this.label12);
            this.panel31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel31.Location = new System.Drawing.Point(3, 503);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(219, 44);
            this.panel31.TabIndex = 32;
            // 
            // panel30
            // 
            this.panel30.Controls.Add(this.retangleUC10);
            this.panel30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel30.Location = new System.Drawing.Point(528, 453);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(219, 44);
            this.panel30.TabIndex = 31;
            // 
            // retangleUC10
            // 
            this.retangleUC10.BackColor = System.Drawing.Color.Lime;
            this.retangleUC10.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC10.ExtenderWidth = 1;
            this.retangleUC10.Fill = false;
            this.retangleUC10.GroupID = null;
            this.retangleUC10.Location = new System.Drawing.Point(87, 6);
            this.retangleUC10.Name = "retangleUC10";
            this.retangleUC10.Size = new System.Drawing.Size(32, 32);
            this.retangleUC10.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC10.TabIndex = 7;
            // 
            // panel29
            // 
            this.panel29.Controls.Add(this.label42);
            this.panel29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel29.Location = new System.Drawing.Point(228, 453);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(294, 44);
            this.panel29.TabIndex = 30;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label42.Location = new System.Drawing.Point(88, 9);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(117, 26);
            this.label42.TabIndex = 6;
            this.label42.Text = "龍華國小站";
            // 
            // panel28
            // 
            this.panel28.Controls.Add(this.label11);
            this.panel28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel28.Location = new System.Drawing.Point(3, 453);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(219, 44);
            this.panel28.TabIndex = 29;
            // 
            // panel27
            // 
            this.panel27.Controls.Add(this.retangleUC9);
            this.panel27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel27.Location = new System.Drawing.Point(528, 403);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(219, 44);
            this.panel27.TabIndex = 28;
            // 
            // retangleUC9
            // 
            this.retangleUC9.BackColor = System.Drawing.Color.Red;
            this.retangleUC9.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC9.ExtenderWidth = 1;
            this.retangleUC9.Fill = false;
            this.retangleUC9.GroupID = null;
            this.retangleUC9.Location = new System.Drawing.Point(87, 6);
            this.retangleUC9.Name = "retangleUC9";
            this.retangleUC9.Size = new System.Drawing.Size(32, 32);
            this.retangleUC9.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC9.TabIndex = 6;
            // 
            // panel26
            // 
            this.panel26.Controls.Add(this.label41);
            this.panel26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel26.Location = new System.Drawing.Point(228, 403);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(294, 44);
            this.panel26.TabIndex = 27;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label41.Location = new System.Drawing.Point(88, 9);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(117, 26);
            this.label41.TabIndex = 6;
            this.label41.Text = "聯合醫院站";
            // 
            // panel25
            // 
            this.panel25.Controls.Add(this.label10);
            this.panel25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel25.Location = new System.Drawing.Point(3, 403);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(219, 44);
            this.panel25.TabIndex = 26;
            // 
            // panel24
            // 
            this.panel24.Controls.Add(this.retangleUC8);
            this.panel24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel24.Location = new System.Drawing.Point(528, 353);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(219, 44);
            this.panel24.TabIndex = 25;
            // 
            // retangleUC8
            // 
            this.retangleUC8.BackColor = System.Drawing.Color.Lime;
            this.retangleUC8.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC8.ExtenderWidth = 1;
            this.retangleUC8.Fill = false;
            this.retangleUC8.GroupID = null;
            this.retangleUC8.Location = new System.Drawing.Point(87, 6);
            this.retangleUC8.Name = "retangleUC8";
            this.retangleUC8.Size = new System.Drawing.Size(32, 32);
            this.retangleUC8.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC8.TabIndex = 5;
            // 
            // panel23
            // 
            this.panel23.Controls.Add(this.label40);
            this.panel23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel23.Location = new System.Drawing.Point(228, 353);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(294, 44);
            this.panel23.TabIndex = 24;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label40.Location = new System.Drawing.Point(88, 9);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(117, 26);
            this.label40.TabIndex = 6;
            this.label40.Text = "美術館東站";
            // 
            // panel22
            // 
            this.panel22.Controls.Add(this.label9);
            this.panel22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel22.Location = new System.Drawing.Point(3, 353);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(219, 44);
            this.panel22.TabIndex = 23;
            // 
            // panel21
            // 
            this.panel21.Controls.Add(this.retangleUC7);
            this.panel21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel21.Location = new System.Drawing.Point(528, 303);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(219, 44);
            this.panel21.TabIndex = 22;
            // 
            // retangleUC7
            // 
            this.retangleUC7.BackColor = System.Drawing.Color.Lime;
            this.retangleUC7.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC7.ExtenderWidth = 1;
            this.retangleUC7.Fill = false;
            this.retangleUC7.GroupID = null;
            this.retangleUC7.Location = new System.Drawing.Point(87, 6);
            this.retangleUC7.Name = "retangleUC7";
            this.retangleUC7.Size = new System.Drawing.Size(32, 32);
            this.retangleUC7.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC7.TabIndex = 4;
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.label39);
            this.panel20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel20.Location = new System.Drawing.Point(228, 303);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(294, 44);
            this.panel20.TabIndex = 21;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label39.Location = new System.Drawing.Point(88, 9);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(138, 26);
            this.label39.TabIndex = 6;
            this.label39.Text = "台鐵美術館站";
            // 
            // panel19
            // 
            this.panel19.Controls.Add(this.label8);
            this.panel19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel19.Location = new System.Drawing.Point(3, 303);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(219, 44);
            this.panel19.TabIndex = 20;
            // 
            // panel18
            // 
            this.panel18.Controls.Add(this.retangleUC6);
            this.panel18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel18.Location = new System.Drawing.Point(528, 253);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(219, 44);
            this.panel18.TabIndex = 19;
            // 
            // retangleUC6
            // 
            this.retangleUC6.BackColor = System.Drawing.Color.Lime;
            this.retangleUC6.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC6.ExtenderWidth = 1;
            this.retangleUC6.Fill = false;
            this.retangleUC6.GroupID = null;
            this.retangleUC6.Location = new System.Drawing.Point(87, 6);
            this.retangleUC6.Name = "retangleUC6";
            this.retangleUC6.Size = new System.Drawing.Size(32, 32);
            this.retangleUC6.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC6.TabIndex = 3;
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.label38);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel17.Location = new System.Drawing.Point(228, 253);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(294, 44);
            this.panel17.TabIndex = 18;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label38.Location = new System.Drawing.Point(88, 9);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(96, 26);
            this.label38.TabIndex = 6;
            this.label38.Text = "馬卡道站";
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.label7);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel16.Location = new System.Drawing.Point(3, 253);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(219, 44);
            this.panel16.TabIndex = 17;
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.retangleUC5);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel15.Location = new System.Drawing.Point(528, 203);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(219, 44);
            this.panel15.TabIndex = 16;
            // 
            // retangleUC5
            // 
            this.retangleUC5.BackColor = System.Drawing.Color.Red;
            this.retangleUC5.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC5.ExtenderWidth = 1;
            this.retangleUC5.Fill = false;
            this.retangleUC5.GroupID = null;
            this.retangleUC5.Location = new System.Drawing.Point(87, 6);
            this.retangleUC5.Name = "retangleUC5";
            this.retangleUC5.Size = new System.Drawing.Size(32, 32);
            this.retangleUC5.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC5.TabIndex = 3;
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.label37);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel14.Location = new System.Drawing.Point(228, 203);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(294, 44);
            this.panel14.TabIndex = 15;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label37.Location = new System.Drawing.Point(88, 9);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(75, 26);
            this.label37.TabIndex = 6;
            this.label37.Text = "鼓山站";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.label6);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel13.Location = new System.Drawing.Point(3, 203);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(219, 44);
            this.panel13.TabIndex = 14;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.retangleUC4);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel12.Location = new System.Drawing.Point(528, 153);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(219, 44);
            this.panel12.TabIndex = 13;
            // 
            // retangleUC4
            // 
            this.retangleUC4.BackColor = System.Drawing.Color.Lime;
            this.retangleUC4.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC4.ExtenderWidth = 1;
            this.retangleUC4.Fill = false;
            this.retangleUC4.GroupID = null;
            this.retangleUC4.Location = new System.Drawing.Point(87, 6);
            this.retangleUC4.Name = "retangleUC4";
            this.retangleUC4.Size = new System.Drawing.Size(32, 32);
            this.retangleUC4.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC4.TabIndex = 4;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.label36);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel11.Location = new System.Drawing.Point(228, 153);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(294, 44);
            this.panel11.TabIndex = 12;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label36.Location = new System.Drawing.Point(88, 9);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(138, 26);
            this.label36.TabIndex = 6;
            this.label36.Text = "鼓山區公所站";
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.label5);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel10.Location = new System.Drawing.Point(3, 153);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(219, 44);
            this.panel10.TabIndex = 11;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.retangleUC3);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel9.Location = new System.Drawing.Point(528, 103);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(219, 44);
            this.panel9.TabIndex = 10;
            // 
            // retangleUC3
            // 
            this.retangleUC3.BackColor = System.Drawing.Color.Lime;
            this.retangleUC3.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC3.ExtenderWidth = 1;
            this.retangleUC3.Fill = false;
            this.retangleUC3.GroupID = null;
            this.retangleUC3.Location = new System.Drawing.Point(87, 6);
            this.retangleUC3.Name = "retangleUC3";
            this.retangleUC3.Size = new System.Drawing.Size(32, 32);
            this.retangleUC3.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC3.TabIndex = 3;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.label35);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(228, 103);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(294, 44);
            this.panel8.TabIndex = 9;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label35.Location = new System.Drawing.Point(88, 9);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(117, 26);
            this.label35.TabIndex = 5;
            this.label35.Text = "文武聖殿站";
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.label4);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(3, 103);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(219, 44);
            this.panel7.TabIndex = 8;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.retangleUC1);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(528, 53);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(219, 44);
            this.panel6.TabIndex = 7;
            // 
            // retangleUC1
            // 
            this.retangleUC1.BackColor = System.Drawing.Color.Lime;
            this.retangleUC1.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC1.ExtenderWidth = 1;
            this.retangleUC1.Fill = false;
            this.retangleUC1.GroupID = null;
            this.retangleUC1.Location = new System.Drawing.Point(87, 6);
            this.retangleUC1.Name = "retangleUC1";
            this.retangleUC1.Size = new System.Drawing.Size(32, 32);
            this.retangleUC1.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC1.TabIndex = 2;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.label34);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(228, 53);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(294, 44);
            this.panel5.TabIndex = 6;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label34.Location = new System.Drawing.Point(88, 9);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(117, 26);
            this.label34.TabIndex = 5;
            this.label34.Text = "壽山公園站";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label3);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(3, 53);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(219, 44);
            this.panel4.TabIndex = 5;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(528, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(219, 44);
            this.panel3.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(82, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 26);
            this.label2.TabIndex = 2;
            this.label2.Text = "狀態";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label20);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(228, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(294, 44);
            this.panel2.TabIndex = 3;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label20.Location = new System.Drawing.Point(119, 9);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(54, 26);
            this.label20.TabIndex = 4;
            this.label20.Text = "站名";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(133)))), ((int)(((byte)(132)))));
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel2.Controls.Add(this.panel49, 2, 13);
            this.tableLayoutPanel2.Controls.Add(this.panel50, 1, 13);
            this.tableLayoutPanel2.Controls.Add(this.panel51, 0, 13);
            this.tableLayoutPanel2.Controls.Add(this.panel52, 2, 12);
            this.tableLayoutPanel2.Controls.Add(this.panel53, 1, 12);
            this.tableLayoutPanel2.Controls.Add(this.panel54, 0, 12);
            this.tableLayoutPanel2.Controls.Add(this.panel55, 2, 11);
            this.tableLayoutPanel2.Controls.Add(this.panel56, 1, 11);
            this.tableLayoutPanel2.Controls.Add(this.panel57, 0, 11);
            this.tableLayoutPanel2.Controls.Add(this.panel58, 2, 10);
            this.tableLayoutPanel2.Controls.Add(this.panel59, 1, 10);
            this.tableLayoutPanel2.Controls.Add(this.panel60, 0, 10);
            this.tableLayoutPanel2.Controls.Add(this.panel61, 2, 9);
            this.tableLayoutPanel2.Controls.Add(this.panel62, 1, 9);
            this.tableLayoutPanel2.Controls.Add(this.panel63, 0, 9);
            this.tableLayoutPanel2.Controls.Add(this.panel64, 2, 8);
            this.tableLayoutPanel2.Controls.Add(this.panel65, 1, 8);
            this.tableLayoutPanel2.Controls.Add(this.panel66, 0, 8);
            this.tableLayoutPanel2.Controls.Add(this.panel67, 2, 7);
            this.tableLayoutPanel2.Controls.Add(this.panel68, 1, 7);
            this.tableLayoutPanel2.Controls.Add(this.panel69, 0, 7);
            this.tableLayoutPanel2.Controls.Add(this.panel70, 2, 6);
            this.tableLayoutPanel2.Controls.Add(this.panel71, 1, 6);
            this.tableLayoutPanel2.Controls.Add(this.panel72, 0, 6);
            this.tableLayoutPanel2.Controls.Add(this.panel73, 2, 5);
            this.tableLayoutPanel2.Controls.Add(this.panel74, 1, 5);
            this.tableLayoutPanel2.Controls.Add(this.panel75, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.panel76, 2, 4);
            this.tableLayoutPanel2.Controls.Add(this.panel77, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.panel78, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.panel79, 2, 3);
            this.tableLayoutPanel2.Controls.Add(this.panel80, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.panel81, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.panel82, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.panel83, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.panel84, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.panel85, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.panel86, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.panel87, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.panel88, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.panel89, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.panel90, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(974, 136);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 14;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(750, 720);
            this.tableLayoutPanel2.TabIndex = 1;
            this.tableLayoutPanel2.CellPaint += new System.Windows.Forms.TableLayoutCellPaintEventHandler(this.tableLayoutPanel2_CellPaint);
            // 
            // panel49
            // 
            this.panel49.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel49.Location = new System.Drawing.Point(528, 653);
            this.panel49.Name = "panel49";
            this.panel49.Size = new System.Drawing.Size(219, 64);
            this.panel49.TabIndex = 43;
            // 
            // panel50
            // 
            this.panel50.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel50.Location = new System.Drawing.Point(228, 653);
            this.panel50.Name = "panel50";
            this.panel50.Size = new System.Drawing.Size(294, 64);
            this.panel50.TabIndex = 42;
            // 
            // panel51
            // 
            this.panel51.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel51.Location = new System.Drawing.Point(3, 653);
            this.panel51.Name = "panel51";
            this.panel51.Size = new System.Drawing.Size(219, 64);
            this.panel51.TabIndex = 41;
            // 
            // panel52
            // 
            this.panel52.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel52.Location = new System.Drawing.Point(528, 603);
            this.panel52.Name = "panel52";
            this.panel52.Size = new System.Drawing.Size(219, 44);
            this.panel52.TabIndex = 40;
            // 
            // panel53
            // 
            this.panel53.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel53.Location = new System.Drawing.Point(228, 603);
            this.panel53.Name = "panel53";
            this.panel53.Size = new System.Drawing.Size(294, 44);
            this.panel53.TabIndex = 39;
            // 
            // panel54
            // 
            this.panel54.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel54.Location = new System.Drawing.Point(3, 603);
            this.panel54.Name = "panel54";
            this.panel54.Size = new System.Drawing.Size(219, 44);
            this.panel54.TabIndex = 38;
            // 
            // panel55
            // 
            this.panel55.Controls.Add(this.retangleUC24);
            this.panel55.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel55.Location = new System.Drawing.Point(528, 553);
            this.panel55.Name = "panel55";
            this.panel55.Size = new System.Drawing.Size(219, 44);
            this.panel55.TabIndex = 37;
            // 
            // panel56
            // 
            this.panel56.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel56.Location = new System.Drawing.Point(228, 553);
            this.panel56.Name = "panel56";
            this.panel56.Size = new System.Drawing.Size(294, 44);
            this.panel56.TabIndex = 36;
            // 
            // panel57
            // 
            this.panel57.Controls.Add(this.label21);
            this.panel57.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel57.Location = new System.Drawing.Point(3, 553);
            this.panel57.Name = "panel57";
            this.panel57.Size = new System.Drawing.Size(219, 44);
            this.panel57.TabIndex = 35;
            // 
            // panel58
            // 
            this.panel58.Controls.Add(this.retangleUC23);
            this.panel58.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel58.Location = new System.Drawing.Point(528, 503);
            this.panel58.Name = "panel58";
            this.panel58.Size = new System.Drawing.Size(219, 44);
            this.panel58.TabIndex = 34;
            // 
            // panel59
            // 
            this.panel59.Controls.Add(this.label56);
            this.panel59.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel59.Location = new System.Drawing.Point(228, 503);
            this.panel59.Name = "panel59";
            this.panel59.Size = new System.Drawing.Size(294, 44);
            this.panel59.TabIndex = 33;
            // 
            // panel60
            // 
            this.panel60.Controls.Add(this.label22);
            this.panel60.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel60.Location = new System.Drawing.Point(3, 503);
            this.panel60.Name = "panel60";
            this.panel60.Size = new System.Drawing.Size(219, 44);
            this.panel60.TabIndex = 32;
            // 
            // panel61
            // 
            this.panel61.Controls.Add(this.retangleUC22);
            this.panel61.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel61.Location = new System.Drawing.Point(528, 453);
            this.panel61.Name = "panel61";
            this.panel61.Size = new System.Drawing.Size(219, 44);
            this.panel61.TabIndex = 31;
            // 
            // panel62
            // 
            this.panel62.Controls.Add(this.label55);
            this.panel62.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel62.Location = new System.Drawing.Point(228, 453);
            this.panel62.Name = "panel62";
            this.panel62.Size = new System.Drawing.Size(294, 44);
            this.panel62.TabIndex = 30;
            // 
            // panel63
            // 
            this.panel63.Controls.Add(this.label23);
            this.panel63.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel63.Location = new System.Drawing.Point(3, 453);
            this.panel63.Name = "panel63";
            this.panel63.Size = new System.Drawing.Size(219, 44);
            this.panel63.TabIndex = 29;
            // 
            // panel64
            // 
            this.panel64.Controls.Add(this.retangleUC21);
            this.panel64.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel64.Location = new System.Drawing.Point(528, 403);
            this.panel64.Name = "panel64";
            this.panel64.Size = new System.Drawing.Size(219, 44);
            this.panel64.TabIndex = 28;
            // 
            // panel65
            // 
            this.panel65.Controls.Add(this.label54);
            this.panel65.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel65.Location = new System.Drawing.Point(228, 403);
            this.panel65.Name = "panel65";
            this.panel65.Size = new System.Drawing.Size(294, 44);
            this.panel65.TabIndex = 27;
            // 
            // panel66
            // 
            this.panel66.Controls.Add(this.label24);
            this.panel66.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel66.Location = new System.Drawing.Point(3, 403);
            this.panel66.Name = "panel66";
            this.panel66.Size = new System.Drawing.Size(219, 44);
            this.panel66.TabIndex = 26;
            // 
            // panel67
            // 
            this.panel67.Controls.Add(this.retangleUC20);
            this.panel67.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel67.Location = new System.Drawing.Point(528, 353);
            this.panel67.Name = "panel67";
            this.panel67.Size = new System.Drawing.Size(219, 44);
            this.panel67.TabIndex = 25;
            // 
            // panel68
            // 
            this.panel68.Controls.Add(this.label53);
            this.panel68.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel68.Location = new System.Drawing.Point(228, 353);
            this.panel68.Name = "panel68";
            this.panel68.Size = new System.Drawing.Size(294, 44);
            this.panel68.TabIndex = 24;
            // 
            // panel69
            // 
            this.panel69.Controls.Add(this.label25);
            this.panel69.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel69.Location = new System.Drawing.Point(3, 353);
            this.panel69.Name = "panel69";
            this.panel69.Size = new System.Drawing.Size(219, 44);
            this.panel69.TabIndex = 23;
            // 
            // panel70
            // 
            this.panel70.Controls.Add(this.retangleUC19);
            this.panel70.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel70.Location = new System.Drawing.Point(528, 303);
            this.panel70.Name = "panel70";
            this.panel70.Size = new System.Drawing.Size(219, 44);
            this.panel70.TabIndex = 22;
            // 
            // panel71
            // 
            this.panel71.Controls.Add(this.label52);
            this.panel71.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel71.Location = new System.Drawing.Point(228, 303);
            this.panel71.Name = "panel71";
            this.panel71.Size = new System.Drawing.Size(294, 44);
            this.panel71.TabIndex = 21;
            // 
            // panel72
            // 
            this.panel72.Controls.Add(this.label26);
            this.panel72.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel72.Location = new System.Drawing.Point(3, 303);
            this.panel72.Name = "panel72";
            this.panel72.Size = new System.Drawing.Size(219, 44);
            this.panel72.TabIndex = 20;
            // 
            // panel73
            // 
            this.panel73.Controls.Add(this.retangleUC18);
            this.panel73.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel73.Location = new System.Drawing.Point(528, 253);
            this.panel73.Name = "panel73";
            this.panel73.Size = new System.Drawing.Size(219, 44);
            this.panel73.TabIndex = 19;
            // 
            // panel74
            // 
            this.panel74.Controls.Add(this.label51);
            this.panel74.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel74.Location = new System.Drawing.Point(228, 253);
            this.panel74.Name = "panel74";
            this.panel74.Size = new System.Drawing.Size(294, 44);
            this.panel74.TabIndex = 18;
            // 
            // panel75
            // 
            this.panel75.Controls.Add(this.label27);
            this.panel75.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel75.Location = new System.Drawing.Point(3, 253);
            this.panel75.Name = "panel75";
            this.panel75.Size = new System.Drawing.Size(219, 44);
            this.panel75.TabIndex = 17;
            // 
            // panel76
            // 
            this.panel76.Controls.Add(this.retangleUC17);
            this.panel76.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel76.Location = new System.Drawing.Point(528, 203);
            this.panel76.Name = "panel76";
            this.panel76.Size = new System.Drawing.Size(219, 44);
            this.panel76.TabIndex = 16;
            // 
            // panel77
            // 
            this.panel77.Controls.Add(this.label50);
            this.panel77.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel77.Location = new System.Drawing.Point(228, 203);
            this.panel77.Name = "panel77";
            this.panel77.Size = new System.Drawing.Size(294, 44);
            this.panel77.TabIndex = 15;
            // 
            // panel78
            // 
            this.panel78.Controls.Add(this.label28);
            this.panel78.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel78.Location = new System.Drawing.Point(3, 203);
            this.panel78.Name = "panel78";
            this.panel78.Size = new System.Drawing.Size(219, 44);
            this.panel78.TabIndex = 14;
            // 
            // panel79
            // 
            this.panel79.Controls.Add(this.retangleUC16);
            this.panel79.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel79.Location = new System.Drawing.Point(528, 153);
            this.panel79.Name = "panel79";
            this.panel79.Size = new System.Drawing.Size(219, 44);
            this.panel79.TabIndex = 13;
            // 
            // panel80
            // 
            this.panel80.Controls.Add(this.label49);
            this.panel80.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel80.Location = new System.Drawing.Point(228, 153);
            this.panel80.Name = "panel80";
            this.panel80.Size = new System.Drawing.Size(294, 44);
            this.panel80.TabIndex = 12;
            // 
            // panel81
            // 
            this.panel81.Controls.Add(this.label29);
            this.panel81.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel81.Location = new System.Drawing.Point(3, 153);
            this.panel81.Name = "panel81";
            this.panel81.Size = new System.Drawing.Size(219, 44);
            this.panel81.TabIndex = 11;
            // 
            // panel82
            // 
            this.panel82.Controls.Add(this.retangleUC2);
            this.panel82.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel82.Location = new System.Drawing.Point(528, 103);
            this.panel82.Name = "panel82";
            this.panel82.Size = new System.Drawing.Size(219, 44);
            this.panel82.TabIndex = 10;
            // 
            // panel83
            // 
            this.panel83.Controls.Add(this.label48);
            this.panel83.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel83.Location = new System.Drawing.Point(228, 103);
            this.panel83.Name = "panel83";
            this.panel83.Size = new System.Drawing.Size(294, 44);
            this.panel83.TabIndex = 9;
            // 
            // panel84
            // 
            this.panel84.Controls.Add(this.label30);
            this.panel84.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel84.Location = new System.Drawing.Point(3, 103);
            this.panel84.Name = "panel84";
            this.panel84.Size = new System.Drawing.Size(219, 44);
            this.panel84.TabIndex = 8;
            // 
            // panel85
            // 
            this.panel85.Controls.Add(this.retangleUC15);
            this.panel85.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel85.Location = new System.Drawing.Point(528, 53);
            this.panel85.Name = "panel85";
            this.panel85.Size = new System.Drawing.Size(219, 44);
            this.panel85.TabIndex = 7;
            // 
            // panel86
            // 
            this.panel86.Controls.Add(this.label47);
            this.panel86.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel86.Location = new System.Drawing.Point(228, 53);
            this.panel86.Name = "panel86";
            this.panel86.Size = new System.Drawing.Size(294, 44);
            this.panel86.TabIndex = 6;
            // 
            // panel87
            // 
            this.panel87.Controls.Add(this.label16);
            this.panel87.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel87.Location = new System.Drawing.Point(3, 53);
            this.panel87.Name = "panel87";
            this.panel87.Size = new System.Drawing.Size(219, 44);
            this.panel87.TabIndex = 5;
            // 
            // panel88
            // 
            this.panel88.Controls.Add(this.label31);
            this.panel88.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel88.Location = new System.Drawing.Point(528, 3);
            this.panel88.Name = "panel88";
            this.panel88.Size = new System.Drawing.Size(219, 44);
            this.panel88.TabIndex = 4;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label31.Location = new System.Drawing.Point(82, 9);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(54, 26);
            this.label31.TabIndex = 2;
            this.label31.Text = "狀態";
            // 
            // panel89
            // 
            this.panel89.Controls.Add(this.label33);
            this.panel89.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel89.Location = new System.Drawing.Point(228, 3);
            this.panel89.Name = "panel89";
            this.panel89.Size = new System.Drawing.Size(294, 44);
            this.panel89.TabIndex = 3;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label33.Location = new System.Drawing.Point(120, 9);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(54, 26);
            this.label33.TabIndex = 4;
            this.label33.Text = "站名";
            // 
            // panel90
            // 
            this.panel90.Controls.Add(this.label32);
            this.panel90.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel90.Location = new System.Drawing.Point(3, 3);
            this.panel90.Name = "panel90";
            this.panel90.Size = new System.Drawing.Size(219, 44);
            this.panel90.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微軟正黑體", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(1334, 87);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 35);
            this.label1.TabIndex = 5;
            this.label1.Text = "=供電";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("微軟正黑體", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label18.Location = new System.Drawing.Point(1548, 87);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(89, 35);
            this.label18.TabIndex = 6;
            this.label18.Text = "=斷電";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("微軟正黑體", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label19.Location = new System.Drawing.Point(724, 14);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(431, 80);
            this.label19.TabIndex = 7;
            this.label19.Text = "高雄輕軌二期設施機電(BMS)\r\n車站及駐車場區AC220V供電";
            // 
            // retangleUC26
            // 
            this.retangleUC26.BackColor = System.Drawing.Color.Red;
            this.retangleUC26.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC26.ExtenderWidth = 1;
            this.retangleUC26.Fill = false;
            this.retangleUC26.GroupID = null;
            this.retangleUC26.Location = new System.Drawing.Point(1296, 87);
            this.retangleUC26.Name = "retangleUC26";
            this.retangleUC26.Size = new System.Drawing.Size(32, 32);
            this.retangleUC26.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC26.TabIndex = 4;
            // 
            // retangleUC25
            // 
            this.retangleUC25.BackColor = System.Drawing.Color.Lime;
            this.retangleUC25.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC25.ExtenderWidth = 1;
            this.retangleUC25.Fill = false;
            this.retangleUC25.GroupID = null;
            this.retangleUC25.Location = new System.Drawing.Point(1510, 87);
            this.retangleUC25.Name = "retangleUC25";
            this.retangleUC25.Size = new System.Drawing.Size(32, 32);
            this.retangleUC25.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC25.TabIndex = 3;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label21.Location = new System.Drawing.Point(72, 9);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(75, 26);
            this.label21.TabIndex = 3;
            this.label21.Text = "駐車場";
            // 
            // retangleUC24
            // 
            this.retangleUC24.BackColor = System.Drawing.Color.Lime;
            this.retangleUC24.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC24.ExtenderWidth = 1;
            this.retangleUC24.Fill = false;
            this.retangleUC24.GroupID = null;
            this.retangleUC24.Location = new System.Drawing.Point(93, 6);
            this.retangleUC24.Name = "retangleUC24";
            this.retangleUC24.Size = new System.Drawing.Size(32, 32);
            this.retangleUC24.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC24.TabIndex = 12;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label22.Location = new System.Drawing.Point(83, 9);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(52, 26);
            this.label22.TabIndex = 3;
            this.label22.Text = "C37";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label23.Location = new System.Drawing.Point(83, 9);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(52, 26);
            this.label23.TabIndex = 3;
            this.label23.Text = "C36";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label24.Location = new System.Drawing.Point(83, 9);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(52, 26);
            this.label24.TabIndex = 3;
            this.label24.Text = "C35";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label25.Location = new System.Drawing.Point(83, 9);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(52, 26);
            this.label25.TabIndex = 3;
            this.label25.Text = "C34";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label26.Location = new System.Drawing.Point(83, 9);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(52, 26);
            this.label26.TabIndex = 3;
            this.label26.Text = "C33";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label27.Location = new System.Drawing.Point(83, 9);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(52, 26);
            this.label27.TabIndex = 3;
            this.label27.Text = "C32";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label28.Location = new System.Drawing.Point(83, 9);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(52, 26);
            this.label28.TabIndex = 3;
            this.label28.Text = "C31";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label29.Location = new System.Drawing.Point(83, 9);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(52, 26);
            this.label29.TabIndex = 3;
            this.label29.Text = "C30";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label30.Location = new System.Drawing.Point(83, 9);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(52, 26);
            this.label30.TabIndex = 3;
            this.label30.Text = "C29";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label56.Location = new System.Drawing.Point(89, 9);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(117, 26);
            this.label56.TabIndex = 7;
            this.label56.Text = "輕軌機場站";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label55.Location = new System.Drawing.Point(89, 9);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(117, 26);
            this.label55.TabIndex = 7;
            this.label55.Text = "凱旋二聖站";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label54.Location = new System.Drawing.Point(89, 9);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(117, 26);
            this.label54.TabIndex = 7;
            this.label54.Text = "凱旋武昌站";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label53.Location = new System.Drawing.Point(89, 9);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(117, 26);
            this.label53.TabIndex = 7;
            this.label53.Text = "五權國小站";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label52.Location = new System.Drawing.Point(99, 9);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(96, 26);
            this.label52.TabIndex = 7;
            this.label52.Text = "衛生局站";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label51.Location = new System.Drawing.Point(89, 9);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(117, 26);
            this.label51.TabIndex = 7;
            this.label51.Text = "凱旋公園站";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label50.Location = new System.Drawing.Point(89, 9);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(117, 26);
            this.label50.TabIndex = 7;
            this.label50.Text = "聖功醫院站";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label49.Location = new System.Drawing.Point(99, 9);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(96, 26);
            this.label49.TabIndex = 7;
            this.label49.Text = "科工館站";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label48.Location = new System.Drawing.Point(89, 9);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(117, 26);
            this.label48.TabIndex = 7;
            this.label48.Text = "數德家商站";
            // 
            // retangleUC23
            // 
            this.retangleUC23.BackColor = System.Drawing.Color.Lime;
            this.retangleUC23.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC23.ExtenderWidth = 1;
            this.retangleUC23.Fill = false;
            this.retangleUC23.GroupID = null;
            this.retangleUC23.Location = new System.Drawing.Point(93, 6);
            this.retangleUC23.Name = "retangleUC23";
            this.retangleUC23.Size = new System.Drawing.Size(32, 32);
            this.retangleUC23.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC23.TabIndex = 11;
            // 
            // retangleUC22
            // 
            this.retangleUC22.BackColor = System.Drawing.Color.Lime;
            this.retangleUC22.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC22.ExtenderWidth = 1;
            this.retangleUC22.Fill = false;
            this.retangleUC22.GroupID = null;
            this.retangleUC22.Location = new System.Drawing.Point(93, 6);
            this.retangleUC22.Name = "retangleUC22";
            this.retangleUC22.Size = new System.Drawing.Size(32, 32);
            this.retangleUC22.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC22.TabIndex = 10;
            // 
            // retangleUC21
            // 
            this.retangleUC21.BackColor = System.Drawing.Color.Lime;
            this.retangleUC21.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC21.ExtenderWidth = 1;
            this.retangleUC21.Fill = false;
            this.retangleUC21.GroupID = null;
            this.retangleUC21.Location = new System.Drawing.Point(93, 6);
            this.retangleUC21.Name = "retangleUC21";
            this.retangleUC21.Size = new System.Drawing.Size(32, 32);
            this.retangleUC21.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC21.TabIndex = 9;
            // 
            // retangleUC20
            // 
            this.retangleUC20.BackColor = System.Drawing.Color.Red;
            this.retangleUC20.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC20.ExtenderWidth = 1;
            this.retangleUC20.Fill = false;
            this.retangleUC20.GroupID = null;
            this.retangleUC20.Location = new System.Drawing.Point(93, 6);
            this.retangleUC20.Name = "retangleUC20";
            this.retangleUC20.Size = new System.Drawing.Size(32, 32);
            this.retangleUC20.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC20.TabIndex = 8;
            // 
            // retangleUC19
            // 
            this.retangleUC19.BackColor = System.Drawing.Color.Lime;
            this.retangleUC19.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC19.ExtenderWidth = 1;
            this.retangleUC19.Fill = false;
            this.retangleUC19.GroupID = null;
            this.retangleUC19.Location = new System.Drawing.Point(93, 6);
            this.retangleUC19.Name = "retangleUC19";
            this.retangleUC19.Size = new System.Drawing.Size(32, 32);
            this.retangleUC19.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC19.TabIndex = 7;
            // 
            // retangleUC18
            // 
            this.retangleUC18.BackColor = System.Drawing.Color.Lime;
            this.retangleUC18.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC18.ExtenderWidth = 1;
            this.retangleUC18.Fill = false;
            this.retangleUC18.GroupID = null;
            this.retangleUC18.Location = new System.Drawing.Point(93, 6);
            this.retangleUC18.Name = "retangleUC18";
            this.retangleUC18.Size = new System.Drawing.Size(32, 32);
            this.retangleUC18.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC18.TabIndex = 6;
            // 
            // retangleUC17
            // 
            this.retangleUC17.BackColor = System.Drawing.Color.Lime;
            this.retangleUC17.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC17.ExtenderWidth = 1;
            this.retangleUC17.Fill = false;
            this.retangleUC17.GroupID = null;
            this.retangleUC17.Location = new System.Drawing.Point(93, 6);
            this.retangleUC17.Name = "retangleUC17";
            this.retangleUC17.Size = new System.Drawing.Size(32, 32);
            this.retangleUC17.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC17.TabIndex = 5;
            // 
            // retangleUC16
            // 
            this.retangleUC16.BackColor = System.Drawing.Color.Red;
            this.retangleUC16.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC16.ExtenderWidth = 1;
            this.retangleUC16.Fill = false;
            this.retangleUC16.GroupID = null;
            this.retangleUC16.Location = new System.Drawing.Point(93, 6);
            this.retangleUC16.Name = "retangleUC16";
            this.retangleUC16.Size = new System.Drawing.Size(32, 32);
            this.retangleUC16.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC16.TabIndex = 4;
            // 
            // retangleUC2
            // 
            this.retangleUC2.BackColor = System.Drawing.Color.Lime;
            this.retangleUC2.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC2.ExtenderWidth = 1;
            this.retangleUC2.Fill = false;
            this.retangleUC2.GroupID = null;
            this.retangleUC2.Location = new System.Drawing.Point(93, 6);
            this.retangleUC2.Name = "retangleUC2";
            this.retangleUC2.Size = new System.Drawing.Size(32, 32);
            this.retangleUC2.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC2.TabIndex = 4;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label16.Location = new System.Drawing.Point(83, 9);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(52, 26);
            this.label16.TabIndex = 5;
            this.label16.Text = "C28";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label47.Location = new System.Drawing.Point(89, 9);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(117, 26);
            this.label47.TabIndex = 7;
            this.label47.Text = "高雄高工站";
            // 
            // retangleUC15
            // 
            this.retangleUC15.BackColor = System.Drawing.Color.Lime;
            this.retangleUC15.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC15.ExtenderWidth = 1;
            this.retangleUC15.Fill = false;
            this.retangleUC15.GroupID = null;
            this.retangleUC15.Location = new System.Drawing.Point(93, 6);
            this.retangleUC15.Name = "retangleUC15";
            this.retangleUC15.Size = new System.Drawing.Size(32, 32);
            this.retangleUC15.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC15.TabIndex = 13;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label32.Location = new System.Drawing.Point(82, 9);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(54, 26);
            this.label32.TabIndex = 2;
            this.label32.Text = "車站";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label15.Location = new System.Drawing.Point(83, 9);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(52, 26);
            this.label15.TabIndex = 5;
            this.label15.Text = "C27";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label14.Location = new System.Drawing.Point(83, 9);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(52, 26);
            this.label14.TabIndex = 5;
            this.label14.Text = "C26";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label13.Location = new System.Drawing.Point(83, 9);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(52, 26);
            this.label13.TabIndex = 5;
            this.label13.Text = "C25";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label12.Location = new System.Drawing.Point(83, 9);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(52, 26);
            this.label12.TabIndex = 3;
            this.label12.Text = "C24";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label11.Location = new System.Drawing.Point(83, 9);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(52, 26);
            this.label11.TabIndex = 3;
            this.label11.Text = "C23";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label10.Location = new System.Drawing.Point(83, 9);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(52, 26);
            this.label10.TabIndex = 3;
            this.label10.Text = "C22";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label9.Location = new System.Drawing.Point(83, 9);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(52, 26);
            this.label9.TabIndex = 3;
            this.label9.Text = "C21";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label8.Location = new System.Drawing.Point(83, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 26);
            this.label8.TabIndex = 3;
            this.label8.Text = "C20";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label7.Location = new System.Drawing.Point(83, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 26);
            this.label7.TabIndex = 3;
            this.label7.Text = "C19";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label6.Location = new System.Drawing.Point(83, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 26);
            this.label6.TabIndex = 3;
            this.label6.Text = "C18";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label5.Location = new System.Drawing.Point(83, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 26);
            this.label5.TabIndex = 3;
            this.label5.Text = "C17";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label4.Location = new System.Drawing.Point(83, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 26);
            this.label4.TabIndex = 3;
            this.label4.Text = "C16";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label3.Location = new System.Drawing.Point(83, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 26);
            this.label3.TabIndex = 3;
            this.label3.Text = "C15";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label17.Location = new System.Drawing.Point(81, 9);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(54, 26);
            this.label17.TabIndex = 4;
            this.label17.Text = "車站";
            // 
            // FormBMS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkGray;
            this.ClientSize = new System.Drawing.Size(1904, 900);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.retangleUC26);
            this.Controls.Add(this.retangleUC25);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormBMS";
            this.Text = "FormBMS";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel42.ResumeLayout(false);
            this.panel41.ResumeLayout(false);
            this.panel41.PerformLayout();
            this.panel40.ResumeLayout(false);
            this.panel40.PerformLayout();
            this.panel39.ResumeLayout(false);
            this.panel38.ResumeLayout(false);
            this.panel38.PerformLayout();
            this.panel37.ResumeLayout(false);
            this.panel37.PerformLayout();
            this.panel36.ResumeLayout(false);
            this.panel35.ResumeLayout(false);
            this.panel35.PerformLayout();
            this.panel34.ResumeLayout(false);
            this.panel34.PerformLayout();
            this.panel33.ResumeLayout(false);
            this.panel32.ResumeLayout(false);
            this.panel32.PerformLayout();
            this.panel31.ResumeLayout(false);
            this.panel31.PerformLayout();
            this.panel30.ResumeLayout(false);
            this.panel29.ResumeLayout(false);
            this.panel29.PerformLayout();
            this.panel28.ResumeLayout(false);
            this.panel28.PerformLayout();
            this.panel27.ResumeLayout(false);
            this.panel26.ResumeLayout(false);
            this.panel26.PerformLayout();
            this.panel25.ResumeLayout(false);
            this.panel25.PerformLayout();
            this.panel24.ResumeLayout(false);
            this.panel23.ResumeLayout(false);
            this.panel23.PerformLayout();
            this.panel22.ResumeLayout(false);
            this.panel22.PerformLayout();
            this.panel21.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.panel20.PerformLayout();
            this.panel19.ResumeLayout(false);
            this.panel19.PerformLayout();
            this.panel18.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            this.panel15.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panel55.ResumeLayout(false);
            this.panel57.ResumeLayout(false);
            this.panel57.PerformLayout();
            this.panel58.ResumeLayout(false);
            this.panel59.ResumeLayout(false);
            this.panel59.PerformLayout();
            this.panel60.ResumeLayout(false);
            this.panel60.PerformLayout();
            this.panel61.ResumeLayout(false);
            this.panel62.ResumeLayout(false);
            this.panel62.PerformLayout();
            this.panel63.ResumeLayout(false);
            this.panel63.PerformLayout();
            this.panel64.ResumeLayout(false);
            this.panel65.ResumeLayout(false);
            this.panel65.PerformLayout();
            this.panel66.ResumeLayout(false);
            this.panel66.PerformLayout();
            this.panel67.ResumeLayout(false);
            this.panel68.ResumeLayout(false);
            this.panel68.PerformLayout();
            this.panel69.ResumeLayout(false);
            this.panel69.PerformLayout();
            this.panel70.ResumeLayout(false);
            this.panel71.ResumeLayout(false);
            this.panel71.PerformLayout();
            this.panel72.ResumeLayout(false);
            this.panel72.PerformLayout();
            this.panel73.ResumeLayout(false);
            this.panel74.ResumeLayout(false);
            this.panel74.PerformLayout();
            this.panel75.ResumeLayout(false);
            this.panel75.PerformLayout();
            this.panel76.ResumeLayout(false);
            this.panel77.ResumeLayout(false);
            this.panel77.PerformLayout();
            this.panel78.ResumeLayout(false);
            this.panel78.PerformLayout();
            this.panel79.ResumeLayout(false);
            this.panel80.ResumeLayout(false);
            this.panel80.PerformLayout();
            this.panel81.ResumeLayout(false);
            this.panel81.PerformLayout();
            this.panel82.ResumeLayout(false);
            this.panel83.ResumeLayout(false);
            this.panel83.PerformLayout();
            this.panel84.ResumeLayout(false);
            this.panel84.PerformLayout();
            this.panel85.ResumeLayout(false);
            this.panel86.ResumeLayout(false);
            this.panel86.PerformLayout();
            this.panel87.ResumeLayout(false);
            this.panel87.PerformLayout();
            this.panel88.ResumeLayout(false);
            this.panel88.PerformLayout();
            this.panel89.ResumeLayout(false);
            this.panel89.PerformLayout();
            this.panel90.ResumeLayout(false);
            this.panel90.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel42;
        private System.Windows.Forms.Panel panel41;
        private System.Windows.Forms.Panel panel40;
        private System.Windows.Forms.Panel panel39;
        private System.Windows.Forms.Panel panel38;
        private System.Windows.Forms.Panel panel37;
        private System.Windows.Forms.Panel panel36;
        private System.Windows.Forms.Panel panel35;
        private System.Windows.Forms.Panel panel34;
        private System.Windows.Forms.Panel panel33;
        private System.Windows.Forms.Panel panel32;
        private System.Windows.Forms.Panel panel31;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel49;
        private System.Windows.Forms.Panel panel50;
        private System.Windows.Forms.Panel panel51;
        private System.Windows.Forms.Panel panel52;
        private System.Windows.Forms.Panel panel53;
        private System.Windows.Forms.Panel panel54;
        private System.Windows.Forms.Panel panel55;
        private System.Windows.Forms.Panel panel56;
        private System.Windows.Forms.Panel panel57;
        private System.Windows.Forms.Panel panel58;
        private System.Windows.Forms.Panel panel59;
        private System.Windows.Forms.Panel panel60;
        private System.Windows.Forms.Panel panel61;
        private System.Windows.Forms.Panel panel62;
        private System.Windows.Forms.Panel panel63;
        private System.Windows.Forms.Panel panel64;
        private System.Windows.Forms.Panel panel65;
        private System.Windows.Forms.Panel panel66;
        private System.Windows.Forms.Panel panel67;
        private System.Windows.Forms.Panel panel68;
        private System.Windows.Forms.Panel panel69;
        private System.Windows.Forms.Panel panel70;
        private System.Windows.Forms.Panel panel71;
        private System.Windows.Forms.Panel panel72;
        private System.Windows.Forms.Panel panel73;
        private System.Windows.Forms.Panel panel74;
        private System.Windows.Forms.Panel panel75;
        private System.Windows.Forms.Panel panel76;
        private System.Windows.Forms.Panel panel77;
        private System.Windows.Forms.Panel panel78;
        private System.Windows.Forms.Panel panel79;
        private System.Windows.Forms.Panel panel80;
        private System.Windows.Forms.Panel panel81;
        private System.Windows.Forms.Panel panel82;
        private System.Windows.Forms.Panel panel83;
        private System.Windows.Forms.Panel panel84;
        private System.Windows.Forms.Panel panel85;
        private System.Windows.Forms.Panel panel86;
        private System.Windows.Forms.Panel panel87;
        private System.Windows.Forms.Panel panel88;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Panel panel89;
        private System.Windows.Forms.Panel panel90;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC1;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC14;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC13;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC12;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC11;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC10;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC9;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC8;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC7;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC6;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC5;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC4;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC3;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC25;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC26;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC24;
        private System.Windows.Forms.Label label21;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC23;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label22;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC22;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label23;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC21;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label24;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC20;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label25;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC19;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label26;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC18;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label27;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC17;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label28;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC16;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label29;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC2;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label30;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC15;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label32;
    }
}