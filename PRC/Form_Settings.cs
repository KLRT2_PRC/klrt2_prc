﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PRC.MyClass;
using PRC.Tools;

namespace PRC
{
    public partial class Form_Settings : Form, IPermissionForm
    {
        private Form_UserMgt _userMgt;
        private Form_GroupMgt _groupMgt;
        private Form_DeptMgt _dept;
        private Form_GroupProg _groupProg;
        public Form_Settings()
        {
            InitializeComponent();
        }

        private void Form_Settings_Load(object sender, EventArgs e)
        {

        }

        public void setUserMgtDept()
        {
            _userMgt.setDept();
        }

        public void setProgGroup()
        {
        }
        public void ResetSettings()
        {
            panelMain.Controls.Clear();

            PermissionManager.InitForm(this, new PERMISSION_FORM_ID[] { PERMISSION_FORM_ID.UserMgt, PERMISSION_FORM_ID.GroupProg,
                                                                        PERMISSION_FORM_ID.Group, PERMISSION_FORM_ID.DeptMgt });
            InitChildForm();

        }
        private void InitChildForm()
        {
            _userMgt = new Form_UserMgt(UserMgtPermission);
            DoInitChild(_userMgt);

            _groupMgt = new Form_GroupMgt(GroupPermission);
            DoInitChild(_groupMgt);

            _dept = new Form_DeptMgt(DeptMgtPermission);
            DoInitChild(_dept);

            _groupProg = new Form_GroupProg(GroupProgPermission);
            DoInitChild(_groupProg);

        }

        private void DoInitChild(Form form)
        {
            form.TopLevel = false;
            form.Dock = DockStyle.Fill;
            panelMain.Controls.Add(form);
        }

        public void ShowInitForm(object sender, PERMISSION_FORM_ID formCode)
        {
            switch (formCode)
            {
                case  PERMISSION_FORM_ID.UserMgt:
                    _groupMgt.Hide();
                    _dept.Hide();
                    _groupProg.Hide();
                    _userMgt.Show();
                    break;
                case  PERMISSION_FORM_ID.Group:
                    _userMgt.Hide();
                    _dept.Hide();
                    _groupProg.Hide();
                    _groupMgt.Show();
                    _groupMgt.Focus();
                    break;
             
                case  PERMISSION_FORM_ID.DeptMgt:
                    _groupMgt.Hide();
                    _userMgt.Hide();
                    _groupProg.Hide();
                    _dept.Show();
                    break;
                case  PERMISSION_FORM_ID.GroupProg:
                    _groupMgt.Hide();
                    _userMgt.Hide();
                    _dept.Hide();
                    _groupProg.Show();
                    break;
            }

        }

        public void btnUserMgt_Click(object sender, EventArgs e)
        {

            ShowInitForm(sender,  PERMISSION_FORM_ID.UserMgt);
            setUserMgtDept();
        }

        public void btnGroupMgt_Click(object sender, EventArgs e)
        {
            ShowInitForm(sender, PERMISSION_FORM_ID.Group);
        }

        public void btnDeptMgt_Click(object sender, EventArgs e)
        {
            ShowInitForm(sender,  PERMISSION_FORM_ID.DeptMgt);
        }

        public void btnGroupProg_Click(object sender, EventArgs e)
        {
            ShowInitForm(sender,  PERMISSION_FORM_ID.GroupProg);
            setProgGroup();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            ShowInitForm(sender,  PERMISSION_FORM_ID.Group);

        }

        private void button_EnabledChanged(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            if (btn.Enabled)
            {
                btn.FlatStyle = FlatStyle.Flat;
                btn.ForeColor = Color.Lime;
                btn.BackColor = Color.Transparent;
                btn.FlatAppearance.BorderColor = Color.Lime;

            }
            else
            {
                btn.FlatStyle = FlatStyle.Standard;
            }
        }


        #region Init with Permission

        public void InitByPermission(List<PermissionObject> permissionObjects)
        {
            foreach (PermissionObject obj in permissionObjects)
            {
                switch (obj.PermissionCode)
                {
                    case  PERMISSION_FORM_ID.UserMgt:
                        UserMgtPermission = obj;
                        break;
                    case  PERMISSION_FORM_ID.GroupProg:
                        GroupProgPermission = obj;
                        break;
                    case  PERMISSION_FORM_ID.Group:
                        GroupPermission = obj;
                        break;
                    case  PERMISSION_FORM_ID.DeptMgt:
                        DeptMgtPermission = obj;
                        break;
                }
            }
        }

        #endregion

        #region Property

        public PermissionObject UserMgtPermission { get; private set; }
        public PermissionObject GroupProgPermission { get; private set; }
        public PermissionObject GroupPermission { get; private set; }
        public PermissionObject DeptMgtPermission { get; private set; }

        #endregion


    }
}
