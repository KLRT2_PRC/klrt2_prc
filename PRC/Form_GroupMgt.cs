﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PRC.MyClass;
using ICSC.Database.MSSQL;
using System.Net.Http;
using System.Net.Http.Headers;
using PRC.Tools;

namespace PRC
{
    public partial class Form_GroupMgt : Form
    {
        public Form_GroupMgt()
        {
            InitializeComponent();
            ComboBoxItem item = new ComboBoxItem();
            item.Text = "停用";
            item.Value = 0;
            cmbGroupPrivilege.Items.Add(item);
            item = new ComboBoxItem();
            item.Text = "啟用";
            item.Value = 1;
            cmbGroupPrivilege.Items.Add(item);
            //item = new CheckBoxItem();
            //item.Text = "刪除留存";
            //item.Value = 2;
            //cmbUserPrivilege.Items.Add(item);
            cmbGroupPrivilege.SelectedIndex = 1;
        }

        public Form_GroupMgt(PermissionObject permission):this()
        {
            btnGroupI.Enabled = permission.CanCreate;

            btnGroupU.Enabled = permission.CanUpdate;
            btnGroupMemberI.Enabled = permission.CanUpdate;
            button1.Enabled = permission.CanUpdate;
            button2.Enabled = permission.CanUpdate;

            btnGroupD.Enabled = permission.CanDelete;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < listGroup.SelectedItems.Count; i++)
            {
                var item = listGroup.SelectedItems[i];
                ListViewItem itemG = new ListViewItem();
                itemG.Text = item.SubItems[1].Text;
                itemG.SubItems.Add(item.SubItems[2].Text);
                itemG.SubItems.Add(item.SubItems[3].Text);
                listAllMember.Items.Add(itemG);

            }
            for (int i = listGroup.SelectedItems.Count - 1; i >= 0; i--)
            {

                //listGroup.SelectedItems[i].Remove();
                listGroup.Items.Remove(listGroup.SelectedItems[i]);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < listAllMember.SelectedItems.Count; i++)
            {
                var item = listAllMember.SelectedItems[i];
                ListViewItem itemG = new ListViewItem();
                itemG.Text = cmbGroup.Text;
                itemG.SubItems.Add(item.Text);
                itemG.SubItems.Add(item.SubItems[1].Text);
                itemG.SubItems.Add(item.SubItems[2].Text);
                listGroup.Items.Add(itemG);
                //listAllMember.SelectedItems[i].Remove();
            }
            for (int i = listAllMember.SelectedItems.Count - 1; i >= 0; i--)
            {

                //listAllMember.SelectedItems[i].Remove();

                listAllMember.Items.Remove(listAllMember.SelectedItems[i]);
            }


        }

        private void Form_SystemMgt_Load(object sender, EventArgs e)
        {
        }

        private void Form_GroupMgt_Enter(object sender, EventArgs e)
        {
            gridGroup.AutoGenerateColumns = false;
            //gridGroup.DataSource = getData();
            getGroupData();
        }
        private DataTable getGroupMemberData()
        {
            StringBuilder logSB = new StringBuilder();
            StringBuilder eventSB = new StringBuilder();
            string tempMsg = string.Empty;
            StringBuilder sqlSB = new StringBuilder();
            string sqlCmd = string.Empty;
            DataTable dt = new DataTable();
            int sts = 0;
            try
            {

                sqlSB.Append(" select gu.*,u.UserName,g.GroupName_Cht,d.DeptID from Sys_GroupToUser gu join Sys_User u ");
                sqlSB.Append(" on gu.UserID = u.UserID join Sys_Group g on gu.GroupID = g.GroupID join Sys_DeptID d on u.DeptID = d.SerialNo");
                sqlSB.Append(" where g.GroupID = '" + cmbGroup.SelectedValue.ToString() + "'");
                sqlCmd = sqlSB.ToString();

                sts = DbOp.QuerySql(SHM.DBIp, SHM.DBName, SHM.DBUser, SHM.DBPasswd, ref sqlCmd, ref dt, SHM.LOG_LEVEL_DBDetail, ref tempMsg);

                logSB.AppendLine(tempMsg);



            }
            catch (Exception ex)
            {
                eventSB.AppendLine("Exception: " + ex.Message);
            }
            finally
            {
                if (eventSB.Length > 0)
                    SHM.QueueEvent.Enqueue(eventSB.ToString());
                if (logSB.Length > 0)
                    SHM.QueueDatabase.Enqueue(logSB.ToString());
            }


            return dt;
        }

        private DataTable getAlllMemberData()
        {
            StringBuilder logSB = new StringBuilder();
            StringBuilder eventSB = new StringBuilder();
            string tempMsg = string.Empty;
            StringBuilder sqlSB = new StringBuilder();
            string sqlCmd = string.Empty;
            DataTable dt = new DataTable();
            int sts = 0;
            try
            {

                sqlSB.Append(" select d.DeptID,u.UserID,u.UserName from Sys_User u join Sys_DeptID d on u.DeptID = d.SerialNo ");
                sqlSB.Append(" where not exists (");
                sqlSB.Append(" select gu.UserID from Sys_GroupToUser gu where u.UserID = gu.UserID and gu.GroupID = '" + cmbGroup.SelectedValue.ToString() + "'");
                sqlSB.Append(" ) and UserPrivilege = '1' order by u.DeptID");
                sqlCmd = sqlSB.ToString();

                sts = DbOp.QuerySql(SHM.DBIp, SHM.DBName, SHM.DBUser, SHM.DBPasswd, ref sqlCmd, ref dt, SHM.LOG_LEVEL_DBDetail, ref tempMsg);

                logSB.AppendLine(tempMsg);



            }
            catch (Exception ex)
            {
                eventSB.AppendLine("Exception: " + ex.Message);
            }
            finally
            {
                if (eventSB.Length > 0)
                    SHM.QueueEvent.Enqueue(eventSB.ToString());
                if (logSB.Length > 0)
                    SHM.QueueDatabase.Enqueue(logSB.ToString());
            }


            return dt;
        }

        private async void getGroupData(string isOpen)
        {
            string baseAddr = "http://localhost:48965/";
            List<Group> groupData = new List<Group>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseAddr);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                string uriStr = "api/Group?isOpen=" + isOpen;
                HttpResponseMessage response = await client.GetAsync(uriStr);
                if (response.IsSuccessStatusCode)
                {
                    groupData = await response.Content.ReadAsAsync<List<Group>>();
                }
            }
            //gridGroup.DataSource = groupData;
            cmbGroup.DisplayMember = "GroupName_Cht";
            cmbGroup.ValueMember = "GroupID";
            cmbGroup.DataSource = groupData;
            cmbGroup.SelectedIndex = 0;
        }


        private async void getGroupData()
        {
            string baseAddr = "http://localhost:48965/";
            List<Group> groupData = new List<Group>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseAddr);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                string uriStr = "api/Group?isOpen=" + 1;
                HttpResponseMessage response = await client.GetAsync(uriStr);
                if (response.IsSuccessStatusCode)
                {
                    groupData = await response.Content.ReadAsAsync<List<Group>>();
                }
            }
            gridGroup.DataSource = groupData;
            cmbGroup.DisplayMember = "GroupName_Cht";
            cmbGroup.ValueMember = "GroupID";
            cmbGroup.DataSource = groupData;
            cmbGroup.SelectedIndex = 0;
        }

        private DataTable getData()
        {
            StringBuilder logSB = new StringBuilder();
            StringBuilder eventSB = new StringBuilder();
            string tempMsg = string.Empty;
            StringBuilder sqlSB = new StringBuilder();
            string sqlCmd = string.Empty;
            DataTable dt = new DataTable();
            int sts = 0;
            try
            {

                sqlSB.Append("SELECT * FROM Sys_Group where GroupPrivilege <> 2");

                sqlCmd = sqlSB.ToString();

                sts = DbOp.QuerySql(SHM.DBIp, SHM.DBName, SHM.DBUser, SHM.DBPasswd, ref sqlCmd, ref dt, SHM.LOG_LEVEL_DBDetail, ref tempMsg);

                logSB.AppendLine(tempMsg);



            }
            catch (Exception ex)
            {
                eventSB.AppendLine("Exception: " + ex.Message);
            }
            finally
            {
                if (eventSB.Length > 0)
                    SHM.QueueEvent.Enqueue(eventSB.ToString());
                if (logSB.Length > 0)
                    SHM.QueueDatabase.Enqueue(logSB.ToString());
            }
            return dt;
        }

        private void btnGroupI_Click(object sender, EventArgs e)
        {
            #region "舊"
            //StringBuilder logSB = new StringBuilder();
            //StringBuilder eventSB = new StringBuilder();
            //string tempMsg = string.Empty;
            //StringBuilder sqlSB = new StringBuilder();
            //string sqlCmd = string.Empty;

            //int sts = 0;
            //try
            //{
            //    ComboBoxItem item = (ComboBoxItem)cmbGroupPrivilege.SelectedItem;
            //    sqlSB.Append("INSERT INTO Sys_Group (GroupID, GroupName_En, GroupName_Cht , GroupPrivilege, UpdateUserID, InsertUserID, InsertTime, UpdateTime, Description ) ");
            //    sqlSB.Append(" VALUES (");
            //    sqlSB.Append("'" + txbGroupID.Text + "',");
            //    sqlSB.Append("'" + txbGroupEn.Text + "',");
            //    sqlSB.Append("'" + txbGroupCht.Text + "',");

            //    sqlSB.Append("'" + item.Value.ToString() + "',");
            //    sqlSB.Append("'" + SHM.Staff + "',");
            //    sqlSB.Append("'" + SHM.Staff + "',");
            //    sqlSB.Append("'" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "',");
            //    sqlSB.Append("'" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "',");
            //    sqlSB.Append("'" + txbDesc.Text + "') ;");

            //    sqlCmd = sqlSB.ToString();

            //    sts = DbOp.ExecuteSql(SHM.DBIp, SHM.DBName, SHM.DBUser, SHM.DBPasswd, ref sqlCmd, SHM.LOG_LEVEL_DBDetail, ref tempMsg);

            //    if (sts > 0)
            //    {
            //        MessageBox.Show("新增成功");
            //        gridGroup.DataSource = getData();
            //    }

            //    logSB.AppendLine(tempMsg);



            //}
            //catch (Exception ex)
            //{
            //    eventSB.AppendLine("Exception: " + ex.Message);
            //}
            //finally
            //{
            //    if (eventSB.Length > 0)
            //        SHM.QueueEvent.Enqueue(eventSB.ToString());
            //    if (logSB.Length > 0)
            //        SHM.QueueDatabase.Enqueue(logSB.ToString());
            //}
            #endregion
            ComboBoxItem item = (ComboBoxItem)cmbGroupPrivilege.SelectedItem;

            if (string.IsNullOrEmpty(txbGroupID.Text.Trim()))
            {
                MessageBox.Show("請輸入群組ID");
            }
            else
            {
                Group group = new Group();
                group.GroupID = txbGroupID.Text;
                group.GroupName_En = txbGroupEn.Text;
                group.GroupName_Cht = txbGroupCht.Text;
                group.GroupPrivilege = Convert.ToInt32(item.Value);
                group.Description = txbDesc.Text;
                group.InsertUserID = SHM.user[0].UserID;
                group.InsertTime = DateTime.Now;
                group.UpdateUserID = SHM.user[0].UserID;
                group.UpdateTime = DateTime.Now;
                InsertData(group);
                clearInput();
            }

        }

        private  void InsertData(Group group)
        {

           var result = WebApiCaller.Srting_GetMessageByPost(@"http://localhost:48965/api/Group", group);

            if (result.IsSuccessStatusCode)
            {
                getGroupData();
         
            }
            MessageBox.Show(result.ContentBody);

        }

        private void btnGroupU_Click(object sender, EventArgs e)
        {

            #region "舊"
            //StringBuilder logSB = new StringBuilder();
            //StringBuilder eventSB = new StringBuilder();
            //string tempMsg = string.Empty;
            //StringBuilder sqlSB = new StringBuilder();
            //string sqlCmd = string.Empty;

            //int sts = 0;
            //try
            //{
            //    ComboBoxItem item = (ComboBoxItem)cmbGroupPrivilege.SelectedItem;
            //    sqlSB.Append("UPDATE Sys_Group SET ");
            //    sqlSB.Append(" GroupName_En = '" + txbGroupEn.Text + "', ");
            //    sqlSB.Append(" GroupName_Cht = '" + txbGroupCht.Text + "', ");
            //    sqlSB.Append(" GroupPrivilege = '" + item.Value.ToString() + "', ");
            //    sqlSB.Append(" Description = '" + txbDesc.Text + "', ");
            //    sqlSB.Append(" UpdateUserID = '" + SHM.Staff + "', ");
            //    sqlSB.Append(" UpdateTime = '" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "' ");
            //    sqlSB.Append(" WHERE GroupID = '" + txbGroupID.Text + "'");


            //    sqlCmd = sqlSB.ToString();

            //    sts = DbOp.ExecuteSql(SHM.DBIp, SHM.DBName, SHM.DBUser, SHM.DBPasswd, ref sqlCmd, SHM.LOG_LEVEL_DBDetail, ref tempMsg);

            //    if (sts > 0)
            //    {
            //        MessageBox.Show("修改成功");
            //        gridGroup.DataSource = getData();
            //    }

            //    logSB.AppendLine(tempMsg);



            //}
            //catch (Exception ex)
            //{
            //    eventSB.AppendLine("Exception: " + ex.Message);
            //}
            //finally
            //{
            //    if (eventSB.Length > 0)
            //        SHM.QueueEvent.Enqueue(eventSB.ToString());
            //    if (logSB.Length > 0)
            //        SHM.QueueDatabase.Enqueue(logSB.ToString());
            //}
            #endregion
            ComboBoxItem item = (ComboBoxItem)cmbGroupPrivilege.SelectedItem;

            if (string.IsNullOrEmpty(txbGroupID.Text.Trim()))
            {
                MessageBox.Show("請選擇要修改的群組");
            }
            else
            {

                Group group = new Group();
                group.GroupID = txbGroupID.Text;
                group.GroupName_En = txbGroupEn.Text;
                group.GroupName_Cht = txbGroupCht.Text;
                group.GroupPrivilege = Convert.ToInt32(item.Value);
                group.Description = txbDesc.Text;
                group.UpdateUserID = SHM.user[0].UserID;
                group.UpdateTime = DateTime.Now;
                UpdateData(group);
                clearInput();
            }
        }

        private  void UpdateData(Group group)
        {
            var result = WebApiCaller.Srting_GetMessageByPut(@"http://localhost:48965/api/Group", group);

            if (result.IsSuccessStatusCode)
            {
                getGroupData();
            }
            MessageBox.Show(result.ContentBody);
        }

        private void btnGroupD_Click(object sender, EventArgs e)
        {
            #region "舊"
            //StringBuilder logSB = new StringBuilder();
            //StringBuilder eventSB = new StringBuilder();
            //string tempMsg = string.Empty;
            //StringBuilder sqlSB = new StringBuilder();
            //string sqlCmd = string.Empty;

            //int sts = 0;
            //try
            //{

            //    sqlSB.Append("UPDATE Sys_Group SET GroupPrivilege = 2, ");
            //    sqlSB.Append(" RetireTime = '" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "' ,");
            //    sqlSB.Append(" UpdateUserID = '" + SHM.Staff + "' ");
            //    sqlSB.Append(" WHERE GroupID = '" + txbGroupID.Text + "'");


            //    sqlCmd = sqlSB.ToString();

            //    sts = DbOp.ExecuteSql(SHM.DBIp, SHM.DBName, SHM.DBUser, SHM.DBPasswd, ref sqlCmd, SHM.LOG_LEVEL_DBDetail, ref tempMsg);

            //    if (sts > 0)
            //    {
            //        MessageBox.Show("刪除成功");
            //        gridGroup.DataSource = getData();
            //    }

            //    logSB.AppendLine(tempMsg);



            //}
            //catch (Exception ex)
            //{
            //    eventSB.AppendLine("Exception: " + ex.Message);
            //}
            //finally
            //{
            //    if (eventSB.Length > 0)
            //        SHM.QueueEvent.Enqueue(eventSB.ToString());
            //    if (logSB.Length > 0)
            //        SHM.QueueDatabase.Enqueue(logSB.ToString());
            //}
            #endregion


            if (string.IsNullOrEmpty(txbGroupID.Text.Trim()))
            {
                MessageBox.Show("請選擇要刪除的群組");
            }
            else
            {
                if (MessageBox.Show("請問要刪除此群組嗎?","",MessageBoxButtons.YesNo,MessageBoxIcon.Warning)== DialogResult.Yes)
                {
                    Group group = new Group();
                    group.GroupID = txbGroupID.Text;
                    group.UpdateUserID = SHM.user[0].UserID;
                    group.RetireTime = DateTime.Now;
                    DeleteData(group);
                    clearInput();
                }
           
            }
        }

        private  void DeleteData(Group group)
        {
            var result = WebApiCaller.Srting_GetMessageByDelete(@"http://localhost:48965/api/Group?GroupID=" + group.GroupID);

            if (result.IsSuccessStatusCode)
            {
                getGroupData();
            }

            MessageBox.Show(result.ContentBody);

        }

        private void cmbGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            #region "舊"
            //listGroup.Items.Clear();
            //listAllMember.Items.Clear();
            //DataTable groupMember = getGroupMemberData();
            //for (int i = 0; i < groupMember.Rows.Count; i++)
            //{
            //    DataRow dr = groupMember.Rows[i];
            //    ListViewItem item = new ListViewItem();
            //    item.Text = dr["GroupName_Cht"].ToString();
            //    item.SubItems.Add(dr["DeptID"].ToString());
            //    item.SubItems.Add(dr["UserID"].ToString());
            //    item.SubItems.Add(dr["UserName"].ToString());
            //    listGroup.Items.Add(item);
            //}
            //DataTable allMember = getAlllMemberData();
            //for (int i = 0; i < allMember.Rows.Count; i++)
            //{
            //    DataRow dr = allMember.Rows[i];
            //    ListViewItem item = new ListViewItem();
            //    item.Text = dr["DeptID"].ToString();
            //    item.SubItems.Add(dr["UserID"].ToString());
            //    item.SubItems.Add(dr["UserName"].ToString());
            //    listAllMember.Items.Add(item);
            //}
            #endregion

            getGroupMember(cmbGroup.SelectedValue.ToString());
        }

        private void getGroupMember(string id)
        {
            List<GroupInfo> groupAll = WebApiCaller.Json_GetModeByGet<List<GroupInfo>>(@"http://localhost:48965/api/GroupInfo").ContentBody;

            List<GroupInfo> groupMember = new List<GroupInfo>();

            for (int groupIndex = groupAll.Count() - 1; groupIndex >= 0; groupIndex--)
            {
                GroupInfo info = groupAll[groupIndex];
                if (id.Equals(info.GroupID))
                {
                    groupMember.Add(info);
                    groupAll.RemoveAt(groupIndex);
                }
            }


            listAllMember.BeginUpdate();
            listAllMember.Items.Clear();
            foreach (var info in groupAll)
            {

                ListViewItem item = new ListViewItem();
                item.Text = info.DeptID;
                item.SubItems.Add(info.UserID);
                item.SubItems.Add(info.UserName);
                listAllMember.Items.Add(item);

            }
            listAllMember.EndUpdate();


            listGroup.BeginUpdate();
            listGroup.Items.Clear();
            foreach (var info in groupMember)
            {
                ListViewItem item = new ListViewItem();
                item.Text = info.GroupName_Cht;
                item.SubItems.Add(info.DeptID);
                item.SubItems.Add(info.UserID);
                item.SubItems.Add(info.UserName);
                listGroup.Items.Add(item);
            }

            listGroup.EndUpdate();

        }

        private void btnGroupMemberI_Click(object sender, EventArgs e)
        {

            #region "舊"
            //List<string> insertList = new List<string>();
            //StringBuilder sqlSB = new StringBuilder();
            //string insertCmd = "";
            ////for (int i = 0; i < listGroup.Items.Count; i++)
            ////{
            ////    sqlSB.Length = 0;
            ////    var item = listGroup.Items[i];
            ////    string insertGroupID = cmbGroup.SelectedValue.ToString();
            ////    string insertUserID = item.SubItems[1].Text;
            ////    sqlSB.Append("Insert into Sys_GroupToUser (GroupID,USERID) ");
            ////    sqlSB.Append("SELECT '" + insertGroupID + "' , '" + insertUserID + "' ");
            ////    sqlSB.Append(" where not exists (");
            ////    sqlSB.Append(" select 1 from sys_grouptouser ");
            ////    sqlSB.Append(" where groupid = '" + insertGroupID + "' and userid = '" + insertUserID + "')");
            ////    insertCmd = sqlSB.ToString();
            ////    insertList.Add(insertCmd);
            ////}

            //sqlSB.Length = 0;
            //sqlSB.Append("Delete From Sys_GroupToUser where GroupID = '" + cmbGroup.SelectedValue.ToString() + "'");
            //insertList.Add(sqlSB.ToString());
            //for (int i = 0; i < listGroup.Items.Count; i++)
            //{
            //    sqlSB.Length = 0;
            //    var item = listGroup.Items[i];
            //    string insertGroupID = cmbGroup.SelectedValue.ToString();
            //    string insertUserID = item.SubItems[1].Text;
            //    sqlSB.Append("Insert into Sys_GroupToUser (GroupID,USERID) Values ");
            //    sqlSB.Append("('" + insertGroupID + "','" + insertUserID + "')");
            //    insertCmd = sqlSB.ToString();
            //    insertList.Add(insertCmd);
            //}

            //StringBuilder logSB = new StringBuilder();
            //StringBuilder eventSB = new StringBuilder();
            //string tempMsg = string.Empty;


            //int sts = 0;
            //try
            //{

            //    sts = DbOp.ExecuteSqlList(SHM.DBIp, SHM.DBName, SHM.DBUser, SHM.DBPasswd, insertList, SHM.LOG_LEVEL_DBDetail, ref tempMsg);
            //    if(sts>0)
            //    {
            //        MessageBox.Show("變更權限成功");
            //    }
            //    logSB.AppendLine(tempMsg);


            //}
            //catch (Exception ex)
            //{
            //    eventSB.AppendLine("Exception: " + ex.Message);
            //}
            //finally
            //{
            //    if (eventSB.Length > 0)
            //        SHM.QueueEvent.Enqueue(eventSB.ToString());
            //    if (logSB.Length > 0)
            //        SHM.QueueDatabase.Enqueue(logSB.ToString());
            //}

            #endregion
            List<GroupToUser> list = new List<GroupToUser>();
            for (int i = 0; i < listGroup.Items.Count; i++)
            {
                GroupToUser gu = new GroupToUser();
                var item = listGroup.Items[i];
                gu.GroupID = cmbGroup.SelectedValue.ToString();
                gu.UserID = item.SubItems[2].Text;
                list.Add(gu);
            }
            InsertGroupToUser(cmbGroup.SelectedValue.ToString(), list);
        }

        private async void InsertGroupToUser(string id, List<GroupToUser> list)
        {
            HttpClient client = new HttpClient();

            client.BaseAddress = new Uri("http://localhost:48965/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var result = await client.PostAsJsonAsync("api/GroupInfo?id=" + id, list);
            if (result.IsSuccessStatusCode)
                MessageBox.Show("權限變更成功");
        }
        private void gridGroup_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                txbGroupID.Text = gridGroup.Rows[e.RowIndex].Cells["GroupID"].Value?.ToString();
                txbGroupEn.Text = gridGroup.Rows[e.RowIndex].Cells["GroupName_En"].Value?.ToString();
                txbGroupCht.Text = gridGroup.Rows[e.RowIndex].Cells["GroupName_Cht"].Value?.ToString();
                txbDesc.Text = gridGroup.Rows[e.RowIndex].Cells["Description"].Value?.ToString();
                cmbGroupPrivilege.SelectedIndex = (int)gridGroup.Rows[e.RowIndex].Cells["GroupPrivilege"].Value;
                cmbGroup.SelectedValue = txbGroupID.Text;
            }

        }

        private void btnGroupC_Click(object sender, EventArgs e)
        {
            clearInput();
        }

        private void button_EnabledChanged(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            if (btn.Enabled)
                btn.FlatStyle = FlatStyle.Flat;
            else
                btn.FlatStyle = FlatStyle.Standard;
        }

        private void clearInput()
        {
            txbGroupID.Text = "";
            txbGroupEn.Text = "";
            txbGroupCht.Text = "";
            txbDesc.Text = "";
            cmbGroupPrivilege.SelectedIndex = 1;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                getGroupData("1");
            }
            else
            {
                getGroupData("0");
            }
        }

     

    
    }
}
