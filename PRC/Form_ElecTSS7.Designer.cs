﻿namespace PRC
{
    partial class Form_ElecTSS7
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label53 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.lineUC20 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label46 = new System.Windows.Forms.Label();
            this.lineUC16 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC15 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC17 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC18 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC19 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC23 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC22 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC21 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC1 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC13 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.switch05UC4 = new iSCADA.Design.Utilities.Electrical.Switch05UC();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.switch08UC1 = new iSCADA.Design.Utilities.Electrical.Switch08UC();
            this.lineUC9 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC11 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.switch06UC1 = new iSCADA.Design.Utilities.Electrical.Switch06UC();
            this.label14 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lineUC8 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.switch07UC3 = new iSCADA.Design.Utilities.Electrical.Switch07UC();
            this.switch07UC2 = new iSCADA.Design.Utilities.Electrical.Switch07UC();
            this.switch07UC4 = new iSCADA.Design.Utilities.Electrical.Switch07UC();
            this.switch07UC1 = new iSCADA.Design.Utilities.Electrical.Switch07UC();
            this.switch09UC2 = new iSCADA.Design.Utilities.Electrical.Switch09UC();
            this.switch09UC1 = new iSCADA.Design.Utilities.Electrical.Switch09UC();
            this.btnMC81DISCONECTOROff = new System.Windows.Forms.Button();
            this.btnMC41LBSOff = new System.Windows.Forms.Button();
            this.btnMC22DISCONECTOROff = new System.Windows.Forms.Button();
            this.btnMC22CBOff = new System.Windows.Forms.Button();
            this.btnMC21DISCONECTOROff = new System.Windows.Forms.Button();
            this.btnMC21CBOff = new System.Windows.Forms.Button();
            this.btnH04GISOff = new System.Windows.Forms.Button();
            this.btnH03GISOff = new System.Windows.Forms.Button();
            this.btnMC81DISCONECTOROn = new System.Windows.Forms.Button();
            this.btnMC41LBSOn = new System.Windows.Forms.Button();
            this.btnMC22DISCONECTOROn = new System.Windows.Forms.Button();
            this.btnMC22CBOn = new System.Windows.Forms.Button();
            this.btnMC21DISCONECTOROn = new System.Windows.Forms.Button();
            this.btnMC21CBOn = new System.Windows.Forms.Button();
            this.btnH04GISOn = new System.Windows.Forms.Button();
            this.btnH03GISOn = new System.Windows.Forms.Button();
            this.btnH02GISOff = new System.Windows.Forms.Button();
            this.btnH02GISOn = new System.Windows.Forms.Button();
            this.label138 = new System.Windows.Forms.Label();
            this.label139 = new System.Windows.Forms.Label();
            this.label140 = new System.Windows.Forms.Label();
            this.label141 = new System.Windows.Forms.Label();
            this.label142 = new System.Windows.Forms.Label();
            this.label143 = new System.Windows.Forms.Label();
            this.label144 = new System.Windows.Forms.Label();
            this.label145 = new System.Windows.Forms.Label();
            this.label146 = new System.Windows.Forms.Label();
            this.label147 = new System.Windows.Forms.Label();
            this.label148 = new System.Windows.Forms.Label();
            this.label149 = new System.Windows.Forms.Label();
            this.label150 = new System.Windows.Forms.Label();
            this.label151 = new System.Windows.Forms.Label();
            this.label152 = new System.Windows.Forms.Label();
            this.label153 = new System.Windows.Forms.Label();
            this.label154 = new System.Windows.Forms.Label();
            this.label155 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.label110 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.label112 = new System.Windows.Forms.Label();
            this.label113 = new System.Windows.Forms.Label();
            this.label114 = new System.Windows.Forms.Label();
            this.label115 = new System.Windows.Forms.Label();
            this.label116 = new System.Windows.Forms.Label();
            this.label117 = new System.Windows.Forms.Label();
            this.label118 = new System.Windows.Forms.Label();
            this.label119 = new System.Windows.Forms.Label();
            this.label120 = new System.Windows.Forms.Label();
            this.label121 = new System.Windows.Forms.Label();
            this.label122 = new System.Windows.Forms.Label();
            this.label123 = new System.Windows.Forms.Label();
            this.label124 = new System.Windows.Forms.Label();
            this.label125 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.retangleUC14 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC71 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC72 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC77 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC78 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC83 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC84 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC86 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC87 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC89 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC90 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC92 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC93 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC95 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC96 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC98 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC99 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC101 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC102 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC69 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC70 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label48 = new System.Windows.Forms.Label();
            this.retangleUC75 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC76 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label49 = new System.Windows.Forms.Label();
            this.retangleUC81 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC82 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label50 = new System.Windows.Forms.Label();
            this.retangleUC33 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC34 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label42 = new System.Windows.Forms.Label();
            this.retangleUC39 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC40 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label43 = new System.Windows.Forms.Label();
            this.retangleUC45 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC46 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label44 = new System.Windows.Forms.Label();
            this.retangleUC27 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC28 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label41 = new System.Windows.Forms.Label();
            this.retangleUC21 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC22 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label40 = new System.Windows.Forms.Label();
            this.retangleUC17 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC18 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label39 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.switch05UC1 = new iSCADA.Design.Utilities.Electrical.Switch05UC();
            this.switch03UC3 = new iSCADA.Design.Utilities.Electrical.Switch03UC();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.lineUC14 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.switch05UC3 = new iSCADA.Design.Utilities.Electrical.Switch05UC();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.switch05UC2 = new iSCADA.Design.Utilities.Electrical.Switch05UC();
            this.switch03UC2 = new iSCADA.Design.Utilities.Electrical.Switch03UC();
            this.circleUC4 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC3 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC2 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.airCircuitBreaker4 = new iSCADA.Design.Utilities.Electrical.AirCircuitBreaker();
            this.airCircuitBreaker3 = new iSCADA.Design.Utilities.Electrical.AirCircuitBreaker();
            this.airCircuitBreaker2 = new iSCADA.Design.Utilities.Electrical.AirCircuitBreaker();
            this.lineUC10 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.airCircuitBreaker1 = new iSCADA.Design.Utilities.Electrical.AirCircuitBreaker();
            this.transformer02UC1 = new iSCADA.Design.Utilities.Electrical.Transformer02UC();
            this.circleUC1 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.label17 = new System.Windows.Forms.Label();
            this.lineUC12 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label20 = new System.Windows.Forms.Label();
            this.transformer01UC1 = new iSCADA.Design.Utilities.Electrical.Transformer01UC();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.retangleUC6 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC5 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lineUC7 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC4 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC6 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC5 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lineUC4 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC3 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC2 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label2 = new System.Windows.Forms.Label();
            this.retangleUC2 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label1 = new System.Windows.Forms.Label();
            this.retangleUC1 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC3 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC7 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC8 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC9 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC10 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC11 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC12 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC13 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.circleUC96 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC97 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC98 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC99 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC84 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC85 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC86 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC87 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC88 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC89 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC90 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC91 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC92 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC93 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC94 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC95 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC66 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC70 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC71 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC72 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC73 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC74 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC75 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC76 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC77 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC78 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC79 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC80 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC81 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC123 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC122 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC121 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC120 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC119 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC118 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC117 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC116 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC115 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC114 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC113 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC112 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC111 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC110 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC109 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC108 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC107 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC106 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC105 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC104 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC103 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC102 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC82 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC65 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC55 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC56 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC57 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC58 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC59 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC60 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC61 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC62 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC63 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC64 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC101 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC100 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC69 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC83 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC68 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC44 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC45 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC46 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC47 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC48 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC49 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC50 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC51 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC52 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC53 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC38 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC39 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC40 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC41 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC42 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC43 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC37 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC36 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC35 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC67 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC54 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC34 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC33 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC32 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC31 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC30 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC29 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC28 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC27 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.label262 = new System.Windows.Forms.Label();
            this.label266 = new System.Windows.Forms.Label();
            this.label267 = new System.Windows.Forms.Label();
            this.label268 = new System.Windows.Forms.Label();
            this.label269 = new System.Windows.Forms.Label();
            this.label270 = new System.Windows.Forms.Label();
            this.label271 = new System.Windows.Forms.Label();
            this.label272 = new System.Windows.Forms.Label();
            this.label273 = new System.Windows.Forms.Label();
            this.label274 = new System.Windows.Forms.Label();
            this.label275 = new System.Windows.Forms.Label();
            this.label276 = new System.Windows.Forms.Label();
            this.label277 = new System.Windows.Forms.Label();
            this.label278 = new System.Windows.Forms.Label();
            this.label279 = new System.Windows.Forms.Label();
            this.label280 = new System.Windows.Forms.Label();
            this.label281 = new System.Windows.Forms.Label();
            this.label282 = new System.Windows.Forms.Label();
            this.label283 = new System.Windows.Forms.Label();
            this.label284 = new System.Windows.Forms.Label();
            this.label285 = new System.Windows.Forms.Label();
            this.label259 = new System.Windows.Forms.Label();
            this.label260 = new System.Windows.Forms.Label();
            this.label261 = new System.Windows.Forms.Label();
            this.label247 = new System.Windows.Forms.Label();
            this.label248 = new System.Windows.Forms.Label();
            this.label249 = new System.Windows.Forms.Label();
            this.label250 = new System.Windows.Forms.Label();
            this.label251 = new System.Windows.Forms.Label();
            this.label252 = new System.Windows.Forms.Label();
            this.label253 = new System.Windows.Forms.Label();
            this.label254 = new System.Windows.Forms.Label();
            this.label255 = new System.Windows.Forms.Label();
            this.label256 = new System.Windows.Forms.Label();
            this.label257 = new System.Windows.Forms.Label();
            this.label258 = new System.Windows.Forms.Label();
            this.label226 = new System.Windows.Forms.Label();
            this.label227 = new System.Windows.Forms.Label();
            this.label228 = new System.Windows.Forms.Label();
            this.label229 = new System.Windows.Forms.Label();
            this.label230 = new System.Windows.Forms.Label();
            this.label231 = new System.Windows.Forms.Label();
            this.label232 = new System.Windows.Forms.Label();
            this.label233 = new System.Windows.Forms.Label();
            this.label234 = new System.Windows.Forms.Label();
            this.label235 = new System.Windows.Forms.Label();
            this.label236 = new System.Windows.Forms.Label();
            this.label237 = new System.Windows.Forms.Label();
            this.label238 = new System.Windows.Forms.Label();
            this.label239 = new System.Windows.Forms.Label();
            this.label240 = new System.Windows.Forms.Label();
            this.label241 = new System.Windows.Forms.Label();
            this.label242 = new System.Windows.Forms.Label();
            this.label243 = new System.Windows.Forms.Label();
            this.label244 = new System.Windows.Forms.Label();
            this.label245 = new System.Windows.Forms.Label();
            this.label246 = new System.Windows.Forms.Label();
            this.label205 = new System.Windows.Forms.Label();
            this.label206 = new System.Windows.Forms.Label();
            this.label207 = new System.Windows.Forms.Label();
            this.label208 = new System.Windows.Forms.Label();
            this.label209 = new System.Windows.Forms.Label();
            this.label210 = new System.Windows.Forms.Label();
            this.label211 = new System.Windows.Forms.Label();
            this.label212 = new System.Windows.Forms.Label();
            this.label213 = new System.Windows.Forms.Label();
            this.label214 = new System.Windows.Forms.Label();
            this.label215 = new System.Windows.Forms.Label();
            this.label216 = new System.Windows.Forms.Label();
            this.label217 = new System.Windows.Forms.Label();
            this.label218 = new System.Windows.Forms.Label();
            this.label219 = new System.Windows.Forms.Label();
            this.label220 = new System.Windows.Forms.Label();
            this.label221 = new System.Windows.Forms.Label();
            this.label222 = new System.Windows.Forms.Label();
            this.label223 = new System.Windows.Forms.Label();
            this.label224 = new System.Windows.Forms.Label();
            this.label225 = new System.Windows.Forms.Label();
            this.label184 = new System.Windows.Forms.Label();
            this.label185 = new System.Windows.Forms.Label();
            this.label186 = new System.Windows.Forms.Label();
            this.label187 = new System.Windows.Forms.Label();
            this.label188 = new System.Windows.Forms.Label();
            this.label189 = new System.Windows.Forms.Label();
            this.label190 = new System.Windows.Forms.Label();
            this.label191 = new System.Windows.Forms.Label();
            this.label192 = new System.Windows.Forms.Label();
            this.label193 = new System.Windows.Forms.Label();
            this.label194 = new System.Windows.Forms.Label();
            this.label195 = new System.Windows.Forms.Label();
            this.label196 = new System.Windows.Forms.Label();
            this.label197 = new System.Windows.Forms.Label();
            this.label198 = new System.Windows.Forms.Label();
            this.label199 = new System.Windows.Forms.Label();
            this.label200 = new System.Windows.Forms.Label();
            this.label201 = new System.Windows.Forms.Label();
            this.label202 = new System.Windows.Forms.Label();
            this.label203 = new System.Windows.Forms.Label();
            this.label204 = new System.Windows.Forms.Label();
            this.label164 = new System.Windows.Forms.Label();
            this.label165 = new System.Windows.Forms.Label();
            this.label166 = new System.Windows.Forms.Label();
            this.label167 = new System.Windows.Forms.Label();
            this.label168 = new System.Windows.Forms.Label();
            this.label169 = new System.Windows.Forms.Label();
            this.label170 = new System.Windows.Forms.Label();
            this.label171 = new System.Windows.Forms.Label();
            this.label172 = new System.Windows.Forms.Label();
            this.label173 = new System.Windows.Forms.Label();
            this.label174 = new System.Windows.Forms.Label();
            this.label175 = new System.Windows.Forms.Label();
            this.label176 = new System.Windows.Forms.Label();
            this.label177 = new System.Windows.Forms.Label();
            this.label178 = new System.Windows.Forms.Label();
            this.label179 = new System.Windows.Forms.Label();
            this.label180 = new System.Windows.Forms.Label();
            this.label181 = new System.Windows.Forms.Label();
            this.label182 = new System.Windows.Forms.Label();
            this.label183 = new System.Windows.Forms.Label();
            this.label162 = new System.Windows.Forms.Label();
            this.label161 = new System.Windows.Forms.Label();
            this.label160 = new System.Windows.Forms.Label();
            this.label159 = new System.Windows.Forms.Label();
            this.label156 = new System.Windows.Forms.Label();
            this.label158 = new System.Windows.Forms.Label();
            this.label157 = new System.Windows.Forms.Label();
            this.label137 = new System.Windows.Forms.Label();
            this.label136 = new System.Windows.Forms.Label();
            this.label135 = new System.Windows.Forms.Label();
            this.label134 = new System.Windows.Forms.Label();
            this.label133 = new System.Windows.Forms.Label();
            this.label132 = new System.Windows.Forms.Label();
            this.label131 = new System.Windows.Forms.Label();
            this.label130 = new System.Windows.Forms.Label();
            this.label129 = new System.Windows.Forms.Label();
            this.label128 = new System.Windows.Forms.Label();
            this.label127 = new System.Windows.Forms.Label();
            this.label126 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label163 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.circleUC26 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC25 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC24 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC23 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC22 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC21 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC20 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC19 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC18 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC17 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC16 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC15 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC14 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC13 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC12 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC11 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC10 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC9 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC8 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC7 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC6 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC5 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.tabPage2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tabPage2.Controls.Add(this.label53);
            this.tabPage2.Controls.Add(this.label51);
            this.tabPage2.Controls.Add(this.label52);
            this.tabPage2.Controls.Add(this.label47);
            this.tabPage2.Controls.Add(this.lineUC20);
            this.tabPage2.Controls.Add(this.label46);
            this.tabPage2.Controls.Add(this.lineUC16);
            this.tabPage2.Controls.Add(this.lineUC15);
            this.tabPage2.Controls.Add(this.lineUC17);
            this.tabPage2.Controls.Add(this.lineUC18);
            this.tabPage2.Controls.Add(this.lineUC19);
            this.tabPage2.Controls.Add(this.lineUC23);
            this.tabPage2.Controls.Add(this.lineUC22);
            this.tabPage2.Controls.Add(this.lineUC21);
            this.tabPage2.Controls.Add(this.lineUC1);
            this.tabPage2.Controls.Add(this.lineUC13);
            this.tabPage2.Controls.Add(this.switch05UC4);
            this.tabPage2.Controls.Add(this.textBox2);
            this.tabPage2.Controls.Add(this.textBox1);
            this.tabPage2.Controls.Add(this.switch08UC1);
            this.tabPage2.Controls.Add(this.lineUC9);
            this.tabPage2.Controls.Add(this.lineUC11);
            this.tabPage2.Controls.Add(this.switch06UC1);
            this.tabPage2.Controls.Add(this.label14);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.lineUC8);
            this.tabPage2.Controls.Add(this.label12);
            this.tabPage2.Controls.Add(this.label11);
            this.tabPage2.Controls.Add(this.switch07UC3);
            this.tabPage2.Controls.Add(this.switch07UC2);
            this.tabPage2.Controls.Add(this.switch07UC4);
            this.tabPage2.Controls.Add(this.switch07UC1);
            this.tabPage2.Controls.Add(this.switch09UC2);
            this.tabPage2.Controls.Add(this.switch09UC1);
            this.tabPage2.Controls.Add(this.btnMC81DISCONECTOROff);
            this.tabPage2.Controls.Add(this.btnMC41LBSOff);
            this.tabPage2.Controls.Add(this.btnMC22DISCONECTOROff);
            this.tabPage2.Controls.Add(this.btnMC22CBOff);
            this.tabPage2.Controls.Add(this.btnMC21DISCONECTOROff);
            this.tabPage2.Controls.Add(this.btnMC21CBOff);
            this.tabPage2.Controls.Add(this.btnH04GISOff);
            this.tabPage2.Controls.Add(this.btnH03GISOff);
            this.tabPage2.Controls.Add(this.btnMC81DISCONECTOROn);
            this.tabPage2.Controls.Add(this.btnMC41LBSOn);
            this.tabPage2.Controls.Add(this.btnMC22DISCONECTOROn);
            this.tabPage2.Controls.Add(this.btnMC22CBOn);
            this.tabPage2.Controls.Add(this.btnMC21DISCONECTOROn);
            this.tabPage2.Controls.Add(this.btnMC21CBOn);
            this.tabPage2.Controls.Add(this.btnH04GISOn);
            this.tabPage2.Controls.Add(this.btnH03GISOn);
            this.tabPage2.Controls.Add(this.btnH02GISOff);
            this.tabPage2.Controls.Add(this.btnH02GISOn);
            this.tabPage2.Controls.Add(this.label138);
            this.tabPage2.Controls.Add(this.label139);
            this.tabPage2.Controls.Add(this.label140);
            this.tabPage2.Controls.Add(this.label141);
            this.tabPage2.Controls.Add(this.label142);
            this.tabPage2.Controls.Add(this.label143);
            this.tabPage2.Controls.Add(this.label144);
            this.tabPage2.Controls.Add(this.label145);
            this.tabPage2.Controls.Add(this.label146);
            this.tabPage2.Controls.Add(this.label147);
            this.tabPage2.Controls.Add(this.label148);
            this.tabPage2.Controls.Add(this.label149);
            this.tabPage2.Controls.Add(this.label150);
            this.tabPage2.Controls.Add(this.label151);
            this.tabPage2.Controls.Add(this.label152);
            this.tabPage2.Controls.Add(this.label153);
            this.tabPage2.Controls.Add(this.label154);
            this.tabPage2.Controls.Add(this.label155);
            this.tabPage2.Controls.Add(this.label90);
            this.tabPage2.Controls.Add(this.label91);
            this.tabPage2.Controls.Add(this.label98);
            this.tabPage2.Controls.Add(this.label99);
            this.tabPage2.Controls.Add(this.label100);
            this.tabPage2.Controls.Add(this.label101);
            this.tabPage2.Controls.Add(this.label102);
            this.tabPage2.Controls.Add(this.label103);
            this.tabPage2.Controls.Add(this.label104);
            this.tabPage2.Controls.Add(this.label105);
            this.tabPage2.Controls.Add(this.label106);
            this.tabPage2.Controls.Add(this.label107);
            this.tabPage2.Controls.Add(this.label108);
            this.tabPage2.Controls.Add(this.label109);
            this.tabPage2.Controls.Add(this.label110);
            this.tabPage2.Controls.Add(this.label111);
            this.tabPage2.Controls.Add(this.label112);
            this.tabPage2.Controls.Add(this.label113);
            this.tabPage2.Controls.Add(this.label114);
            this.tabPage2.Controls.Add(this.label115);
            this.tabPage2.Controls.Add(this.label116);
            this.tabPage2.Controls.Add(this.label117);
            this.tabPage2.Controls.Add(this.label118);
            this.tabPage2.Controls.Add(this.label119);
            this.tabPage2.Controls.Add(this.label120);
            this.tabPage2.Controls.Add(this.label121);
            this.tabPage2.Controls.Add(this.label122);
            this.tabPage2.Controls.Add(this.label123);
            this.tabPage2.Controls.Add(this.label124);
            this.tabPage2.Controls.Add(this.label125);
            this.tabPage2.Controls.Add(this.label92);
            this.tabPage2.Controls.Add(this.label93);
            this.tabPage2.Controls.Add(this.label94);
            this.tabPage2.Controls.Add(this.label95);
            this.tabPage2.Controls.Add(this.label96);
            this.tabPage2.Controls.Add(this.label97);
            this.tabPage2.Controls.Add(this.label82);
            this.tabPage2.Controls.Add(this.label83);
            this.tabPage2.Controls.Add(this.label84);
            this.tabPage2.Controls.Add(this.label85);
            this.tabPage2.Controls.Add(this.label86);
            this.tabPage2.Controls.Add(this.label87);
            this.tabPage2.Controls.Add(this.label88);
            this.tabPage2.Controls.Add(this.label89);
            this.tabPage2.Controls.Add(this.label74);
            this.tabPage2.Controls.Add(this.label75);
            this.tabPage2.Controls.Add(this.label76);
            this.tabPage2.Controls.Add(this.label77);
            this.tabPage2.Controls.Add(this.label78);
            this.tabPage2.Controls.Add(this.label79);
            this.tabPage2.Controls.Add(this.label80);
            this.tabPage2.Controls.Add(this.label81);
            this.tabPage2.Controls.Add(this.label72);
            this.tabPage2.Controls.Add(this.label73);
            this.tabPage2.Controls.Add(this.label70);
            this.tabPage2.Controls.Add(this.label71);
            this.tabPage2.Controls.Add(this.label68);
            this.tabPage2.Controls.Add(this.label69);
            this.tabPage2.Controls.Add(this.label67);
            this.tabPage2.Controls.Add(this.label66);
            this.tabPage2.Controls.Add(this.retangleUC14);
            this.tabPage2.Controls.Add(this.retangleUC71);
            this.tabPage2.Controls.Add(this.retangleUC72);
            this.tabPage2.Controls.Add(this.retangleUC77);
            this.tabPage2.Controls.Add(this.retangleUC78);
            this.tabPage2.Controls.Add(this.retangleUC83);
            this.tabPage2.Controls.Add(this.retangleUC84);
            this.tabPage2.Controls.Add(this.retangleUC86);
            this.tabPage2.Controls.Add(this.retangleUC87);
            this.tabPage2.Controls.Add(this.retangleUC89);
            this.tabPage2.Controls.Add(this.retangleUC90);
            this.tabPage2.Controls.Add(this.retangleUC92);
            this.tabPage2.Controls.Add(this.retangleUC93);
            this.tabPage2.Controls.Add(this.retangleUC95);
            this.tabPage2.Controls.Add(this.retangleUC96);
            this.tabPage2.Controls.Add(this.retangleUC98);
            this.tabPage2.Controls.Add(this.retangleUC99);
            this.tabPage2.Controls.Add(this.retangleUC101);
            this.tabPage2.Controls.Add(this.retangleUC102);
            this.tabPage2.Controls.Add(this.retangleUC69);
            this.tabPage2.Controls.Add(this.retangleUC70);
            this.tabPage2.Controls.Add(this.label48);
            this.tabPage2.Controls.Add(this.retangleUC75);
            this.tabPage2.Controls.Add(this.retangleUC76);
            this.tabPage2.Controls.Add(this.label49);
            this.tabPage2.Controls.Add(this.retangleUC81);
            this.tabPage2.Controls.Add(this.retangleUC82);
            this.tabPage2.Controls.Add(this.label50);
            this.tabPage2.Controls.Add(this.retangleUC33);
            this.tabPage2.Controls.Add(this.retangleUC34);
            this.tabPage2.Controls.Add(this.label42);
            this.tabPage2.Controls.Add(this.retangleUC39);
            this.tabPage2.Controls.Add(this.retangleUC40);
            this.tabPage2.Controls.Add(this.label43);
            this.tabPage2.Controls.Add(this.retangleUC45);
            this.tabPage2.Controls.Add(this.retangleUC46);
            this.tabPage2.Controls.Add(this.label44);
            this.tabPage2.Controls.Add(this.retangleUC27);
            this.tabPage2.Controls.Add(this.retangleUC28);
            this.tabPage2.Controls.Add(this.label41);
            this.tabPage2.Controls.Add(this.retangleUC21);
            this.tabPage2.Controls.Add(this.retangleUC22);
            this.tabPage2.Controls.Add(this.label40);
            this.tabPage2.Controls.Add(this.retangleUC17);
            this.tabPage2.Controls.Add(this.retangleUC18);
            this.tabPage2.Controls.Add(this.label39);
            this.tabPage2.Controls.Add(this.label35);
            this.tabPage2.Controls.Add(this.switch05UC1);
            this.tabPage2.Controls.Add(this.switch03UC3);
            this.tabPage2.Controls.Add(this.label34);
            this.tabPage2.Controls.Add(this.label33);
            this.tabPage2.Controls.Add(this.label32);
            this.tabPage2.Controls.Add(this.label31);
            this.tabPage2.Controls.Add(this.label30);
            this.tabPage2.Controls.Add(this.label29);
            this.tabPage2.Controls.Add(this.label28);
            this.tabPage2.Controls.Add(this.label27);
            this.tabPage2.Controls.Add(this.lineUC14);
            this.tabPage2.Controls.Add(this.switch05UC3);
            this.tabPage2.Controls.Add(this.label26);
            this.tabPage2.Controls.Add(this.label25);
            this.tabPage2.Controls.Add(this.label24);
            this.tabPage2.Controls.Add(this.label23);
            this.tabPage2.Controls.Add(this.label22);
            this.tabPage2.Controls.Add(this.label21);
            this.tabPage2.Controls.Add(this.switch05UC2);
            this.tabPage2.Controls.Add(this.switch03UC2);
            this.tabPage2.Controls.Add(this.circleUC4);
            this.tabPage2.Controls.Add(this.circleUC3);
            this.tabPage2.Controls.Add(this.circleUC2);
            this.tabPage2.Controls.Add(this.airCircuitBreaker4);
            this.tabPage2.Controls.Add(this.airCircuitBreaker3);
            this.tabPage2.Controls.Add(this.airCircuitBreaker2);
            this.tabPage2.Controls.Add(this.lineUC10);
            this.tabPage2.Controls.Add(this.airCircuitBreaker1);
            this.tabPage2.Controls.Add(this.transformer02UC1);
            this.tabPage2.Controls.Add(this.circleUC1);
            this.tabPage2.Controls.Add(this.label17);
            this.tabPage2.Controls.Add(this.lineUC12);
            this.tabPage2.Controls.Add(this.label20);
            this.tabPage2.Controls.Add(this.transformer01UC1);
            this.tabPage2.Controls.Add(this.label19);
            this.tabPage2.Controls.Add(this.label18);
            this.tabPage2.Controls.Add(this.label16);
            this.tabPage2.Controls.Add(this.label15);
            this.tabPage2.Controls.Add(this.label45);
            this.tabPage2.Controls.Add(this.label13);
            this.tabPage2.Controls.Add(this.retangleUC6);
            this.tabPage2.Controls.Add(this.retangleUC5);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.lineUC7);
            this.tabPage2.Controls.Add(this.retangleUC4);
            this.tabPage2.Controls.Add(this.lineUC6);
            this.tabPage2.Controls.Add(this.lineUC5);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.lineUC4);
            this.tabPage2.Controls.Add(this.lineUC3);
            this.tabPage2.Controls.Add(this.lineUC2);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.retangleUC2);
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Controls.Add(this.retangleUC1);
            this.tabPage2.Controls.Add(this.retangleUC3);
            this.tabPage2.Controls.Add(this.retangleUC7);
            this.tabPage2.Controls.Add(this.retangleUC8);
            this.tabPage2.Controls.Add(this.retangleUC9);
            this.tabPage2.Controls.Add(this.retangleUC10);
            this.tabPage2.Controls.Add(this.retangleUC11);
            this.tabPage2.Controls.Add(this.retangleUC12);
            this.tabPage2.Controls.Add(this.retangleUC13);
            this.tabPage2.Font = new System.Drawing.Font("微軟正黑體", 16F);
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage2.Size = new System.Drawing.Size(1902, 867);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "TSS7 ";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label53.Font = new System.Drawing.Font("微軟正黑體", 14F, System.Drawing.FontStyle.Bold);
            this.label53.ForeColor = System.Drawing.Color.Magenta;
            this.label53.Location = new System.Drawing.Point(565, 321);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(49, 26);
            this.label53.TabIndex = 2206;
            this.label53.Text = "H02";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label51.Font = new System.Drawing.Font("微軟正黑體", 14F, System.Drawing.FontStyle.Bold);
            this.label51.ForeColor = System.Drawing.Color.Magenta;
            this.label51.Location = new System.Drawing.Point(388, 378);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(49, 26);
            this.label51.TabIndex = 2206;
            this.label51.Text = "H03";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label52.Font = new System.Drawing.Font("微軟正黑體", 14F, System.Drawing.FontStyle.Bold);
            this.label52.ForeColor = System.Drawing.Color.Magenta;
            this.label52.Location = new System.Drawing.Point(448, 404);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(49, 26);
            this.label52.TabIndex = 2206;
            this.label52.Text = "H04";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label47.Font = new System.Drawing.Font("微軟正黑體", 14F, System.Drawing.FontStyle.Bold);
            this.label47.ForeColor = System.Drawing.Color.Magenta;
            this.label47.Location = new System.Drawing.Point(646, 395);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(49, 26);
            this.label47.TabIndex = 2206;
            this.label47.Text = "H01";
            // 
            // lineUC20
            // 
            this.lineUC20.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC20.ExtenderWidth = 2;
            this.lineUC20.Fill = false;
            this.lineUC20.GroupID = null;
            this.lineUC20.Location = new System.Drawing.Point(264, 728);
            this.lineUC20.Margin = new System.Windows.Forms.Padding(24, 36, 24, 36);
            this.lineUC20.Name = "lineUC20";
            this.lineUC20.Size = new System.Drawing.Size(165, 2);
            this.lineUC20.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC20.TabIndex = 2205;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("微軟正黑體", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label46.Location = new System.Drawing.Point(497, 210);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(67, 15);
            this.label46.TabIndex = 1829;
            this.label46.Text = "INTERLOCK";
            // 
            // lineUC16
            // 
            this.lineUC16.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC16.ExtenderWidth = 2;
            this.lineUC16.Fill = false;
            this.lineUC16.GroupID = null;
            this.lineUC16.Location = new System.Drawing.Point(588, 450);
            this.lineUC16.Margin = new System.Windows.Forms.Padding(24, 36, 24, 36);
            this.lineUC16.Name = "lineUC16";
            this.lineUC16.Size = new System.Drawing.Size(112, 2);
            this.lineUC16.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC16.TabIndex = 2205;
            // 
            // lineUC15
            // 
            this.lineUC15.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC15.ExtenderWidth = 2;
            this.lineUC15.Fill = false;
            this.lineUC15.GroupID = null;
            this.lineUC15.Location = new System.Drawing.Point(490, 450);
            this.lineUC15.Margin = new System.Windows.Forms.Padding(12, 16, 12, 16);
            this.lineUC15.Name = "lineUC15";
            this.lineUC15.Size = new System.Drawing.Size(98, 2);
            this.lineUC15.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC15.TabIndex = 2205;
            // 
            // lineUC17
            // 
            this.lineUC17.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC17.ExtenderWidth = 2;
            this.lineUC17.Fill = false;
            this.lineUC17.GroupID = null;
            this.lineUC17.Location = new System.Drawing.Point(324, 536);
            this.lineUC17.Margin = new System.Windows.Forms.Padding(12, 16, 12, 16);
            this.lineUC17.Name = "lineUC17";
            this.lineUC17.Size = new System.Drawing.Size(96, 2);
            this.lineUC17.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC17.TabIndex = 2205;
            // 
            // lineUC18
            // 
            this.lineUC18.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC18.ExtenderWidth = 2;
            this.lineUC18.Fill = false;
            this.lineUC18.GroupID = null;
            this.lineUC18.Location = new System.Drawing.Point(460, 187);
            this.lineUC18.Margin = new System.Windows.Forms.Padding(12, 16, 12, 16);
            this.lineUC18.Name = "lineUC18";
            this.lineUC18.Size = new System.Drawing.Size(140, 2);
            this.lineUC18.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC18.TabIndex = 2205;
            // 
            // lineUC19
            // 
            this.lineUC19.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC19.ExtenderWidth = 2;
            this.lineUC19.Fill = false;
            this.lineUC19.GroupID = null;
            this.lineUC19.Location = new System.Drawing.Point(266, 749);
            this.lineUC19.Margin = new System.Windows.Forms.Padding(12, 16, 12, 16);
            this.lineUC19.Name = "lineUC19";
            this.lineUC19.Size = new System.Drawing.Size(96, 2);
            this.lineUC19.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC19.TabIndex = 2205;
            // 
            // lineUC23
            // 
            this.lineUC23.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC23.ExtenderWidth = 2;
            this.lineUC23.Fill = false;
            this.lineUC23.GroupID = null;
            this.lineUC23.Location = new System.Drawing.Point(372, 605);
            this.lineUC23.Margin = new System.Windows.Forms.Padding(48, 81, 48, 81);
            this.lineUC23.Name = "lineUC23";
            this.lineUC23.Size = new System.Drawing.Size(44, 2);
            this.lineUC23.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC23.TabIndex = 2205;
            // 
            // lineUC22
            // 
            this.lineUC22.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC22.ExtenderWidth = 2;
            this.lineUC22.Fill = false;
            this.lineUC22.GroupID = null;
            this.lineUC22.Location = new System.Drawing.Point(282, 605);
            this.lineUC22.Margin = new System.Windows.Forms.Padding(24, 36, 24, 36);
            this.lineUC22.Name = "lineUC22";
            this.lineUC22.Size = new System.Drawing.Size(30, 2);
            this.lineUC22.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC22.TabIndex = 2205;
            // 
            // lineUC21
            // 
            this.lineUC21.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC21.ExtenderWidth = 2;
            this.lineUC21.Fill = false;
            this.lineUC21.GroupID = null;
            this.lineUC21.Location = new System.Drawing.Point(280, 600);
            this.lineUC21.Margin = new System.Windows.Forms.Padding(12, 16, 12, 16);
            this.lineUC21.Name = "lineUC21";
            this.lineUC21.Size = new System.Drawing.Size(136, 2);
            this.lineUC21.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC21.TabIndex = 2205;
            // 
            // lineUC1
            // 
            this.lineUC1.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC1.ExtenderWidth = 2;
            this.lineUC1.Fill = false;
            this.lineUC1.GroupID = null;
            this.lineUC1.Location = new System.Drawing.Point(330, 450);
            this.lineUC1.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.lineUC1.Name = "lineUC1";
            this.lineUC1.Size = new System.Drawing.Size(48, 2);
            this.lineUC1.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC1.TabIndex = 2205;
            // 
            // lineUC13
            // 
            this.lineUC13.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC13.ExtenderWidth = 2;
            this.lineUC13.Fill = false;
            this.lineUC13.GroupID = null;
            this.lineUC13.Location = new System.Drawing.Point(290, 736);
            this.lineUC13.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.lineUC13.Name = "lineUC13";
            this.lineUC13.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC13.Size = new System.Drawing.Size(2, 90);
            this.lineUC13.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC13.TabIndex = 1888;
            // 
            // switch05UC4
            // 
            this.switch05UC4.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.switch05UC4.ExtenderWidth = 2;
            this.switch05UC4.Fill = false;
            this.switch05UC4.GroupID = null;
            this.switch05UC4.Location = new System.Drawing.Point(330, 522);
            this.switch05UC4.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.switch05UC4.Name = "switch05UC4";
            this.switch05UC4.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.switch05UC4.Size = new System.Drawing.Size(47, 64);
            this.switch05UC4.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.switch05UC4.TabIndex = 2204;
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox2.Font = new System.Drawing.Font("微軟正黑體", 8F, System.Drawing.FontStyle.Bold);
            this.textBox2.Location = new System.Drawing.Point(629, 438);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(15, 15);
            this.textBox2.TabIndex = 2202;
            this.textBox2.Text = "M";
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Font = new System.Drawing.Font("微軟正黑體", 8F, System.Drawing.FontStyle.Bold);
            this.textBox1.Location = new System.Drawing.Point(629, 427);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(15, 15);
            this.textBox1.TabIndex = 2201;
            this.textBox1.Text = "W";
            // 
            // switch08UC1
            // 
            this.switch08UC1.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.switch08UC1.ExtenderWidth = 3;
            this.switch08UC1.Fill = false;
            this.switch08UC1.GroupID = null;
            this.switch08UC1.Location = new System.Drawing.Point(340, 478);
            this.switch08UC1.Name = "switch08UC1";
            this.switch08UC1.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.switch08UC1.Size = new System.Drawing.Size(22, 61);
            this.switch08UC1.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.switch08UC1.TabIndex = 2200;
            // 
            // lineUC9
            // 
            this.lineUC9.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC9.ExtenderWidth = 3;
            this.lineUC9.Fill = false;
            this.lineUC9.GroupID = null;
            this.lineUC9.Location = new System.Drawing.Point(632, 443);
            this.lineUC9.Margin = new System.Windows.Forms.Padding(8, 4, 8, 4);
            this.lineUC9.Name = "lineUC9";
            this.lineUC9.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC9.Size = new System.Drawing.Size(3, 63);
            this.lineUC9.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC9.TabIndex = 2199;
            // 
            // lineUC11
            // 
            this.lineUC11.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC11.ExtenderWidth = 3;
            this.lineUC11.Fill = false;
            this.lineUC11.GroupID = null;
            this.lineUC11.Location = new System.Drawing.Point(632, 362);
            this.lineUC11.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.lineUC11.Name = "lineUC11";
            this.lineUC11.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC11.Size = new System.Drawing.Size(3, 38);
            this.lineUC11.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC11.TabIndex = 1858;
            // 
            // switch06UC1
            // 
            this.switch06UC1.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.switch06UC1.ExtenderWidth = 2;
            this.switch06UC1.Fill = false;
            this.switch06UC1.GroupID = null;
            this.switch06UC1.Location = new System.Drawing.Point(581, 367);
            this.switch06UC1.Name = "switch06UC1";
            this.switch06UC1.Size = new System.Drawing.Size(55, 20);
            this.switch06UC1.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.switch06UC1.TabIndex = 2198;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label14.Location = new System.Drawing.Point(535, 378);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(24, 16);
            this.label14.TabIndex = 1852;
            this.label14.Text = "DS";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label10.Location = new System.Drawing.Point(497, 394);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(22, 16);
            this.label10.TabIndex = 1848;
            this.label10.Text = "ES";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label7.Location = new System.Drawing.Point(322, 398);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(22, 16);
            this.label7.TabIndex = 1839;
            this.label7.Text = "ES";
            // 
            // lineUC8
            // 
            this.lineUC8.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC8.ExtenderWidth = 3;
            this.lineUC8.Fill = false;
            this.lineUC8.GroupID = null;
            this.lineUC8.Location = new System.Drawing.Point(320, 360);
            this.lineUC8.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.lineUC8.Name = "lineUC8";
            this.lineUC8.Size = new System.Drawing.Size(350, 3);
            this.lineUC8.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC8.TabIndex = 1842;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label12.Location = new System.Drawing.Point(537, 321);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(24, 16);
            this.label12.TabIndex = 1850;
            this.label12.Text = "DS";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label11.Location = new System.Drawing.Point(497, 308);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(22, 16);
            this.label11.TabIndex = 1849;
            this.label11.Text = "ES";
            // 
            // switch07UC3
            // 
            this.switch07UC3.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.switch07UC3.ExtenderWidth = 3;
            this.switch07UC3.Fill = false;
            this.switch07UC3.GroupID = null;
            this.switch07UC3.Location = new System.Drawing.Point(485, 360);
            this.switch07UC3.Name = "switch07UC3";
            this.switch07UC3.Size = new System.Drawing.Size(55, 55);
            this.switch07UC3.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.switch07UC3.TabIndex = 2197;
            // 
            // switch07UC2
            // 
            this.switch07UC2.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.switch07UC2.ExtenderWidth = 3;
            this.switch07UC2.Fill = false;
            this.switch07UC2.GroupID = null;
            this.switch07UC2.Location = new System.Drawing.Point(311, 362);
            this.switch07UC2.Name = "switch07UC2";
            this.switch07UC2.Size = new System.Drawing.Size(55, 55);
            this.switch07UC2.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.switch07UC2.TabIndex = 2197;
            // 
            // switch07UC4
            // 
            this.switch07UC4.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.switch07UC4.ExtenderWidth = 3;
            this.switch07UC4.Fill = false;
            this.switch07UC4.GroupID = null;
            this.switch07UC4.Location = new System.Drawing.Point(593, 383);
            this.switch07UC4.Name = "switch07UC4";
            this.switch07UC4.Size = new System.Drawing.Size(55, 55);
            this.switch07UC4.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.switch07UC4.TabIndex = 2197;
            // 
            // switch07UC1
            // 
            this.switch07UC1.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.switch07UC1.ExtenderWidth = 3;
            this.switch07UC1.Fill = false;
            this.switch07UC1.GroupID = null;
            this.switch07UC1.Location = new System.Drawing.Point(482, 303);
            this.switch07UC1.Name = "switch07UC1";
            this.switch07UC1.Size = new System.Drawing.Size(58, 58);
            this.switch07UC1.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.switch07UC1.TabIndex = 2197;
            // 
            // switch09UC2
            // 
            this.switch09UC2.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.switch09UC2.ExtenderWidth = 2;
            this.switch09UC2.Fill = false;
            this.switch09UC2.GroupID = null;
            this.switch09UC2.Location = new System.Drawing.Point(554, 178);
            this.switch09UC2.Margin = new System.Windows.Forms.Padding(12, 16, 12, 16);
            this.switch09UC2.Name = "switch09UC2";
            this.switch09UC2.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.switch09UC2.Size = new System.Drawing.Size(46, 58);
            this.switch09UC2.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.switch09UC2.TabIndex = 2196;
            // 
            // switch09UC1
            // 
            this.switch09UC1.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.switch09UC1.ExtenderWidth = 2;
            this.switch09UC1.Fill = false;
            this.switch09UC1.GroupID = null;
            this.switch09UC1.Location = new System.Drawing.Point(458, 178);
            this.switch09UC1.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.switch09UC1.Name = "switch09UC1";
            this.switch09UC1.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.switch09UC1.Size = new System.Drawing.Size(46, 58);
            this.switch09UC1.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.switch09UC1.TabIndex = 2196;
            // 
            // btnMC81DISCONECTOROff
            // 
            this.btnMC81DISCONECTOROff.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnMC81DISCONECTOROff.Location = new System.Drawing.Point(1309, 326);
            this.btnMC81DISCONECTOROff.Name = "btnMC81DISCONECTOROff";
            this.btnMC81DISCONECTOROff.Size = new System.Drawing.Size(43, 29);
            this.btnMC81DISCONECTOROff.TabIndex = 2194;
            this.btnMC81DISCONECTOROff.Text = "Off";
            this.btnMC81DISCONECTOROff.UseVisualStyleBackColor = true;
            this.btnMC81DISCONECTOROff.Click += new System.EventHandler(this.btn_click);
            // 
            // btnMC41LBSOff
            // 
            this.btnMC41LBSOff.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnMC41LBSOff.Location = new System.Drawing.Point(1309, 291);
            this.btnMC41LBSOff.Name = "btnMC41LBSOff";
            this.btnMC41LBSOff.Size = new System.Drawing.Size(43, 29);
            this.btnMC41LBSOff.TabIndex = 2193;
            this.btnMC41LBSOff.Text = "Off";
            this.btnMC41LBSOff.UseVisualStyleBackColor = true;
            this.btnMC41LBSOff.Click += new System.EventHandler(this.btn_click);
            // 
            // btnMC22DISCONECTOROff
            // 
            this.btnMC22DISCONECTOROff.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnMC22DISCONECTOROff.Location = new System.Drawing.Point(1309, 258);
            this.btnMC22DISCONECTOROff.Name = "btnMC22DISCONECTOROff";
            this.btnMC22DISCONECTOROff.Size = new System.Drawing.Size(43, 29);
            this.btnMC22DISCONECTOROff.TabIndex = 2192;
            this.btnMC22DISCONECTOROff.Text = "Off";
            this.btnMC22DISCONECTOROff.UseVisualStyleBackColor = true;
            this.btnMC22DISCONECTOROff.Click += new System.EventHandler(this.btn_click);
            // 
            // btnMC22CBOff
            // 
            this.btnMC22CBOff.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnMC22CBOff.Location = new System.Drawing.Point(1309, 222);
            this.btnMC22CBOff.Name = "btnMC22CBOff";
            this.btnMC22CBOff.Size = new System.Drawing.Size(43, 29);
            this.btnMC22CBOff.TabIndex = 2191;
            this.btnMC22CBOff.Text = "Off";
            this.btnMC22CBOff.UseVisualStyleBackColor = true;
            this.btnMC22CBOff.Click += new System.EventHandler(this.btn_click);
            // 
            // btnMC21DISCONECTOROff
            // 
            this.btnMC21DISCONECTOROff.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnMC21DISCONECTOROff.Location = new System.Drawing.Point(1309, 187);
            this.btnMC21DISCONECTOROff.Name = "btnMC21DISCONECTOROff";
            this.btnMC21DISCONECTOROff.Size = new System.Drawing.Size(43, 29);
            this.btnMC21DISCONECTOROff.TabIndex = 2190;
            this.btnMC21DISCONECTOROff.Text = "Off";
            this.btnMC21DISCONECTOROff.UseVisualStyleBackColor = true;
            this.btnMC21DISCONECTOROff.Click += new System.EventHandler(this.btn_click);
            // 
            // btnMC21CBOff
            // 
            this.btnMC21CBOff.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnMC21CBOff.Location = new System.Drawing.Point(1309, 153);
            this.btnMC21CBOff.Name = "btnMC21CBOff";
            this.btnMC21CBOff.Size = new System.Drawing.Size(43, 29);
            this.btnMC21CBOff.TabIndex = 2189;
            this.btnMC21CBOff.Text = "Off";
            this.btnMC21CBOff.UseVisualStyleBackColor = true;
            this.btnMC21CBOff.Click += new System.EventHandler(this.btn_click);
            // 
            // btnH04GISOff
            // 
            this.btnH04GISOff.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnH04GISOff.Location = new System.Drawing.Point(1309, 120);
            this.btnH04GISOff.Name = "btnH04GISOff";
            this.btnH04GISOff.Size = new System.Drawing.Size(43, 29);
            this.btnH04GISOff.TabIndex = 2188;
            this.btnH04GISOff.Text = "Off";
            this.btnH04GISOff.UseVisualStyleBackColor = true;
            this.btnH04GISOff.Click += new System.EventHandler(this.btn_click);
            // 
            // btnH03GISOff
            // 
            this.btnH03GISOff.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnH03GISOff.Location = new System.Drawing.Point(1309, 85);
            this.btnH03GISOff.Name = "btnH03GISOff";
            this.btnH03GISOff.Size = new System.Drawing.Size(43, 29);
            this.btnH03GISOff.TabIndex = 2187;
            this.btnH03GISOff.Text = "Off";
            this.btnH03GISOff.UseVisualStyleBackColor = true;
            this.btnH03GISOff.Click += new System.EventHandler(this.btn_click);
            // 
            // btnMC81DISCONECTOROn
            // 
            this.btnMC81DISCONECTOROn.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnMC81DISCONECTOROn.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnMC81DISCONECTOROn.Location = new System.Drawing.Point(1231, 327);
            this.btnMC81DISCONECTOROn.Name = "btnMC81DISCONECTOROn";
            this.btnMC81DISCONECTOROn.Size = new System.Drawing.Size(43, 29);
            this.btnMC81DISCONECTOROn.TabIndex = 2186;
            this.btnMC81DISCONECTOROn.Text = "On";
            this.btnMC81DISCONECTOROn.UseVisualStyleBackColor = true;
            this.btnMC81DISCONECTOROn.Click += new System.EventHandler(this.btn_click);
            // 
            // btnMC41LBSOn
            // 
            this.btnMC41LBSOn.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnMC41LBSOn.Location = new System.Drawing.Point(1231, 292);
            this.btnMC41LBSOn.Name = "btnMC41LBSOn";
            this.btnMC41LBSOn.Size = new System.Drawing.Size(43, 29);
            this.btnMC41LBSOn.TabIndex = 2185;
            this.btnMC41LBSOn.Text = "On";
            this.btnMC41LBSOn.UseVisualStyleBackColor = true;
            this.btnMC41LBSOn.Click += new System.EventHandler(this.btn_click);
            // 
            // btnMC22DISCONECTOROn
            // 
            this.btnMC22DISCONECTOROn.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnMC22DISCONECTOROn.Location = new System.Drawing.Point(1231, 258);
            this.btnMC22DISCONECTOROn.Name = "btnMC22DISCONECTOROn";
            this.btnMC22DISCONECTOROn.Size = new System.Drawing.Size(43, 29);
            this.btnMC22DISCONECTOROn.TabIndex = 2184;
            this.btnMC22DISCONECTOROn.Text = "On";
            this.btnMC22DISCONECTOROn.UseVisualStyleBackColor = true;
            this.btnMC22DISCONECTOROn.Click += new System.EventHandler(this.btn_click);
            // 
            // btnMC22CBOn
            // 
            this.btnMC22CBOn.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnMC22CBOn.Location = new System.Drawing.Point(1231, 222);
            this.btnMC22CBOn.Name = "btnMC22CBOn";
            this.btnMC22CBOn.Size = new System.Drawing.Size(43, 29);
            this.btnMC22CBOn.TabIndex = 2183;
            this.btnMC22CBOn.Text = "On";
            this.btnMC22CBOn.UseVisualStyleBackColor = true;
            this.btnMC22CBOn.Click += new System.EventHandler(this.btn_click);
            // 
            // btnMC21DISCONECTOROn
            // 
            this.btnMC21DISCONECTOROn.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnMC21DISCONECTOROn.Location = new System.Drawing.Point(1231, 187);
            this.btnMC21DISCONECTOROn.Name = "btnMC21DISCONECTOROn";
            this.btnMC21DISCONECTOROn.Size = new System.Drawing.Size(43, 29);
            this.btnMC21DISCONECTOROn.TabIndex = 2182;
            this.btnMC21DISCONECTOROn.Text = "On";
            this.btnMC21DISCONECTOROn.UseVisualStyleBackColor = true;
            this.btnMC21DISCONECTOROn.Click += new System.EventHandler(this.btn_click);
            // 
            // btnMC21CBOn
            // 
            this.btnMC21CBOn.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnMC21CBOn.Location = new System.Drawing.Point(1231, 153);
            this.btnMC21CBOn.Name = "btnMC21CBOn";
            this.btnMC21CBOn.Size = new System.Drawing.Size(43, 29);
            this.btnMC21CBOn.TabIndex = 2181;
            this.btnMC21CBOn.Text = "On";
            this.btnMC21CBOn.UseVisualStyleBackColor = true;
            this.btnMC21CBOn.Click += new System.EventHandler(this.btn_click);
            // 
            // btnH04GISOn
            // 
            this.btnH04GISOn.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnH04GISOn.Location = new System.Drawing.Point(1231, 120);
            this.btnH04GISOn.Name = "btnH04GISOn";
            this.btnH04GISOn.Size = new System.Drawing.Size(43, 29);
            this.btnH04GISOn.TabIndex = 2180;
            this.btnH04GISOn.Text = "On";
            this.btnH04GISOn.UseVisualStyleBackColor = true;
            this.btnH04GISOn.Click += new System.EventHandler(this.btn_click);
            // 
            // btnH03GISOn
            // 
            this.btnH03GISOn.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnH03GISOn.Location = new System.Drawing.Point(1231, 85);
            this.btnH03GISOn.Name = "btnH03GISOn";
            this.btnH03GISOn.Size = new System.Drawing.Size(43, 29);
            this.btnH03GISOn.TabIndex = 2179;
            this.btnH03GISOn.Text = "On";
            this.btnH03GISOn.UseVisualStyleBackColor = true;
            this.btnH03GISOn.Click += new System.EventHandler(this.btn_click);
            // 
            // btnH02GISOff
            // 
            this.btnH02GISOff.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnH02GISOff.Location = new System.Drawing.Point(1309, 51);
            this.btnH02GISOff.Name = "btnH02GISOff";
            this.btnH02GISOff.Size = new System.Drawing.Size(43, 29);
            this.btnH02GISOff.TabIndex = 2178;
            this.btnH02GISOff.Text = "Off";
            this.btnH02GISOff.UseVisualStyleBackColor = true;
            this.btnH02GISOff.Click += new System.EventHandler(this.btn_click);
            // 
            // btnH02GISOn
            // 
            this.btnH02GISOn.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnH02GISOn.Location = new System.Drawing.Point(1231, 50);
            this.btnH02GISOn.Name = "btnH02GISOn";
            this.btnH02GISOn.Size = new System.Drawing.Size(43, 29);
            this.btnH02GISOn.TabIndex = 2177;
            this.btnH02GISOn.Text = "On";
            this.btnH02GISOn.UseVisualStyleBackColor = true;
            this.btnH02GISOn.Click += new System.EventHandler(this.btn_click);
            // 
            // label138
            // 
            this.label138.AutoSize = true;
            this.label138.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label138.Location = new System.Drawing.Point(1773, 627);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(61, 24);
            this.label138.TabIndex = 2176;
            this.label138.Text = "Value";
            // 
            // label139
            // 
            this.label139.AutoSize = true;
            this.label139.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label139.Location = new System.Drawing.Point(1520, 627);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(231, 24);
            this.label139.TabIndex = 2175;
            this.label139.Text = "MC81 DC CURRENT(A)：";
            // 
            // label140
            // 
            this.label140.AutoSize = true;
            this.label140.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label140.Location = new System.Drawing.Point(1773, 598);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(61, 24);
            this.label140.TabIndex = 2174;
            this.label140.Text = "Value";
            // 
            // label141
            // 
            this.label141.AutoSize = true;
            this.label141.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label141.Location = new System.Drawing.Point(1520, 598);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(230, 24);
            this.label141.TabIndex = 2173;
            this.label141.Text = "MC81 DC VOLTAGE(V)：";
            // 
            // label142
            // 
            this.label142.AutoSize = true;
            this.label142.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label142.Location = new System.Drawing.Point(1773, 567);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(61, 24);
            this.label142.TabIndex = 2172;
            this.label142.Text = "Value";
            // 
            // label143
            // 
            this.label143.AutoSize = true;
            this.label143.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label143.Location = new System.Drawing.Point(1520, 567);
            this.label143.Name = "label143";
            this.label143.Size = new System.Drawing.Size(231, 24);
            this.label143.TabIndex = 2171;
            this.label143.Text = "MC22 DC CURRENT(A)：";
            // 
            // label144
            // 
            this.label144.AutoSize = true;
            this.label144.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label144.Location = new System.Drawing.Point(1773, 537);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(61, 24);
            this.label144.TabIndex = 2170;
            this.label144.Text = "Value";
            // 
            // label145
            // 
            this.label145.AutoSize = true;
            this.label145.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label145.Location = new System.Drawing.Point(1520, 537);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(230, 24);
            this.label145.TabIndex = 2169;
            this.label145.Text = "MC22 DC VOLTAGE(V)：";
            // 
            // label146
            // 
            this.label146.AutoSize = true;
            this.label146.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label146.Location = new System.Drawing.Point(1773, 508);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(61, 24);
            this.label146.TabIndex = 2168;
            this.label146.Text = "Value";
            // 
            // label147
            // 
            this.label147.AutoSize = true;
            this.label147.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label147.Location = new System.Drawing.Point(1520, 508);
            this.label147.Name = "label147";
            this.label147.Size = new System.Drawing.Size(231, 24);
            this.label147.TabIndex = 2167;
            this.label147.Text = "MC21 DC CURRENT(A)：";
            // 
            // label148
            // 
            this.label148.AutoSize = true;
            this.label148.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label148.Location = new System.Drawing.Point(1773, 479);
            this.label148.Name = "label148";
            this.label148.Size = new System.Drawing.Size(61, 24);
            this.label148.TabIndex = 2166;
            this.label148.Text = "Value";
            // 
            // label149
            // 
            this.label149.AutoSize = true;
            this.label149.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label149.Location = new System.Drawing.Point(1520, 479);
            this.label149.Name = "label149";
            this.label149.Size = new System.Drawing.Size(230, 24);
            this.label149.TabIndex = 2165;
            this.label149.Text = "MC21 DC VOLTAGE(V)：";
            // 
            // label150
            // 
            this.label150.AutoSize = true;
            this.label150.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label150.Location = new System.Drawing.Point(1773, 448);
            this.label150.Name = "label150";
            this.label150.Size = new System.Drawing.Size(61, 24);
            this.label150.TabIndex = 2164;
            this.label150.Text = "Value";
            // 
            // label151
            // 
            this.label151.AutoSize = true;
            this.label151.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label151.Location = new System.Drawing.Point(1520, 448);
            this.label151.Name = "label151";
            this.label151.Size = new System.Drawing.Size(218, 24);
            this.label151.TabIndex = 2163;
            this.label151.Text = "H04 POWER (KVARH)：";
            // 
            // label152
            // 
            this.label152.AutoSize = true;
            this.label152.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label152.Location = new System.Drawing.Point(1773, 418);
            this.label152.Name = "label152";
            this.label152.Size = new System.Drawing.Size(61, 24);
            this.label152.TabIndex = 2162;
            this.label152.Text = "Value";
            // 
            // label153
            // 
            this.label153.AutoSize = true;
            this.label153.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label153.Location = new System.Drawing.Point(1520, 418);
            this.label153.Name = "label153";
            this.label153.Size = new System.Drawing.Size(223, 24);
            this.label153.TabIndex = 2161;
            this.label153.Text = "H04 FREQUENCY (HZ)：";
            // 
            // label154
            // 
            this.label154.AutoSize = true;
            this.label154.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label154.Location = new System.Drawing.Point(1773, 389);
            this.label154.Name = "label154";
            this.label154.Size = new System.Drawing.Size(61, 24);
            this.label154.TabIndex = 2160;
            this.label154.Text = "Value";
            // 
            // label155
            // 
            this.label155.AutoSize = true;
            this.label155.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label155.Location = new System.Drawing.Point(1520, 389);
            this.label155.Name = "label155";
            this.label155.Size = new System.Drawing.Size(199, 24);
            this.label155.TabIndex = 2159;
            this.label155.Text = "H04 POWER (KWH)：";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label90.Location = new System.Drawing.Point(1458, 804);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(61, 24);
            this.label90.TabIndex = 2158;
            this.label90.Text = "Value";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label91.Location = new System.Drawing.Point(1181, 804);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(250, 24);
            this.label91.TabIndex = 2157;
            this.label91.Text = "H04 POWER VALUE (KW)：";
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label98.Location = new System.Drawing.Point(1458, 774);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(61, 24);
            this.label98.TabIndex = 2156;
            this.label98.Text = "Value";
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label99.Location = new System.Drawing.Point(1181, 774);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(254, 24);
            this.label99.TabIndex = 2155;
            this.label99.Text = "H04 POWER FACTOR (PF)：";
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label100.Location = new System.Drawing.Point(1458, 745);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(61, 24);
            this.label100.TabIndex = 2154;
            this.label100.Text = "Value";
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label101.Location = new System.Drawing.Point(1181, 745);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(262, 24);
            this.label101.TabIndex = 2153;
            this.label101.Text = "H04 T PHASE CURRENT(A)：";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label102.Location = new System.Drawing.Point(1458, 718);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(61, 24);
            this.label102.TabIndex = 2152;
            this.label102.Text = "Value";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label103.Location = new System.Drawing.Point(1181, 718);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(262, 24);
            this.label103.TabIndex = 2151;
            this.label103.Text = "H04 S PHASE CURRENT(A)：";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label104.Location = new System.Drawing.Point(1458, 687);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(61, 24);
            this.label104.TabIndex = 2150;
            this.label104.Text = "Value";
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label105.Location = new System.Drawing.Point(1181, 687);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(263, 24);
            this.label105.TabIndex = 2149;
            this.label105.Text = "H04 R PHASE CURRENT(A)：";
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label106.Location = new System.Drawing.Point(1458, 657);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(61, 24);
            this.label106.TabIndex = 2148;
            this.label106.Text = "Value";
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label107.Location = new System.Drawing.Point(1181, 657);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(281, 24);
            this.label107.TabIndex = 2147;
            this.label107.Text = "H04 T-R PHASE VOLTAGE(V)：";
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label108.Location = new System.Drawing.Point(1458, 628);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(61, 24);
            this.label108.TabIndex = 2146;
            this.label108.Text = "Value";
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label109.Location = new System.Drawing.Point(1181, 628);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(280, 24);
            this.label109.TabIndex = 2145;
            this.label109.Text = "H04 S-T PHASE VOLTAGE(V)：";
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label110.Location = new System.Drawing.Point(1458, 599);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(61, 24);
            this.label110.TabIndex = 2144;
            this.label110.Text = "Value";
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label111.Location = new System.Drawing.Point(1181, 599);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(281, 24);
            this.label111.TabIndex = 2143;
            this.label111.Text = "H04 R-S PHASE VOLTAGE(V)：";
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label112.Location = new System.Drawing.Point(1458, 568);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(61, 24);
            this.label112.TabIndex = 2142;
            this.label112.Text = "Value";
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label113.Location = new System.Drawing.Point(1181, 568);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(218, 24);
            this.label113.TabIndex = 2141;
            this.label113.Text = "H03 POWER (KVARH)：";
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label114.Location = new System.Drawing.Point(1458, 538);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(61, 24);
            this.label114.TabIndex = 2140;
            this.label114.Text = "Value";
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label115.Location = new System.Drawing.Point(1181, 538);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(223, 24);
            this.label115.TabIndex = 2139;
            this.label115.Text = "H03 FREQUENCY (HZ)：";
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label116.Location = new System.Drawing.Point(1458, 509);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(61, 24);
            this.label116.TabIndex = 2138;
            this.label116.Text = "Value";
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label117.Location = new System.Drawing.Point(1181, 509);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(199, 24);
            this.label117.TabIndex = 2137;
            this.label117.Text = "H03 POWER (KWH)：";
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label118.Location = new System.Drawing.Point(1458, 480);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(61, 24);
            this.label118.TabIndex = 2136;
            this.label118.Text = "Value";
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label119.Location = new System.Drawing.Point(1181, 480);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(250, 24);
            this.label119.TabIndex = 2135;
            this.label119.Text = "H03 POWER VALUE (KW)：";
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label120.Location = new System.Drawing.Point(1458, 449);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(61, 24);
            this.label120.TabIndex = 2134;
            this.label120.Text = "Value";
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label121.Location = new System.Drawing.Point(1181, 449);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(254, 24);
            this.label121.TabIndex = 2133;
            this.label121.Text = "H03 POWER FACTOR (PF)：";
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label122.Location = new System.Drawing.Point(1458, 419);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(61, 24);
            this.label122.TabIndex = 2132;
            this.label122.Text = "Value";
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label123.Location = new System.Drawing.Point(1181, 419);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(262, 24);
            this.label123.TabIndex = 2131;
            this.label123.Text = "H03 T PHASE CURRENT(A)：";
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label124.Location = new System.Drawing.Point(1458, 390);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(61, 24);
            this.label124.TabIndex = 2130;
            this.label124.Text = "Value";
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label125.Location = new System.Drawing.Point(1181, 390);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(262, 24);
            this.label125.TabIndex = 2129;
            this.label125.Text = "H03 S PHASE CURRENT(A)：";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label92.Location = new System.Drawing.Point(1119, 803);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(61, 24);
            this.label92.TabIndex = 2128;
            this.label92.Text = "Value";
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label93.Location = new System.Drawing.Point(841, 803);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(263, 24);
            this.label93.TabIndex = 2127;
            this.label93.Text = "H03 R PHASE CURRENT(A)：";
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label94.Location = new System.Drawing.Point(1119, 773);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(61, 24);
            this.label94.TabIndex = 2126;
            this.label94.Text = "Value";
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label95.Location = new System.Drawing.Point(841, 773);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(281, 24);
            this.label95.TabIndex = 2125;
            this.label95.Text = "H03 T-R PHASE VOLTAGE(V)：";
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label96.Location = new System.Drawing.Point(1119, 744);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(61, 24);
            this.label96.TabIndex = 2124;
            this.label96.Text = "Value";
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label97.Location = new System.Drawing.Point(841, 744);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(280, 24);
            this.label97.TabIndex = 2123;
            this.label97.Text = "H03 S-T PHASE VOLTAGE(V)：";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label82.Location = new System.Drawing.Point(1119, 717);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(61, 24);
            this.label82.TabIndex = 2122;
            this.label82.Text = "Value";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label83.Location = new System.Drawing.Point(841, 717);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(281, 24);
            this.label83.TabIndex = 2121;
            this.label83.Text = "H03 R-S PHASE VOLTAGE(V)：";
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label84.Location = new System.Drawing.Point(1119, 686);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(61, 24);
            this.label84.TabIndex = 2120;
            this.label84.Text = "Value";
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label85.Location = new System.Drawing.Point(841, 686);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(218, 24);
            this.label85.TabIndex = 2119;
            this.label85.Text = "H02 POWER (KVARH)：";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label86.Location = new System.Drawing.Point(1119, 656);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(61, 24);
            this.label86.TabIndex = 2118;
            this.label86.Text = "Value";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label87.Location = new System.Drawing.Point(841, 656);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(223, 24);
            this.label87.TabIndex = 2117;
            this.label87.Text = "H02 FREQUENCY (HZ)：";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label88.Location = new System.Drawing.Point(1119, 627);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(61, 24);
            this.label88.TabIndex = 2116;
            this.label88.Text = "Value";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label89.Location = new System.Drawing.Point(841, 627);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(199, 24);
            this.label89.TabIndex = 2115;
            this.label89.Text = "H02 POWER (KWH)：";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label74.Location = new System.Drawing.Point(1119, 598);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(61, 24);
            this.label74.TabIndex = 2114;
            this.label74.Text = "Value";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label75.Location = new System.Drawing.Point(841, 598);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(250, 24);
            this.label75.TabIndex = 2113;
            this.label75.Text = "H02 POWER VALUE (KW)：";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label76.Location = new System.Drawing.Point(1119, 567);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(61, 24);
            this.label76.TabIndex = 2112;
            this.label76.Text = "Value";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label77.Location = new System.Drawing.Point(841, 567);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(254, 24);
            this.label77.TabIndex = 2111;
            this.label77.Text = "H02 POWER FACTOR (PF)：";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label78.Location = new System.Drawing.Point(1119, 537);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(61, 24);
            this.label78.TabIndex = 2110;
            this.label78.Text = "Value";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label79.Location = new System.Drawing.Point(841, 537);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(262, 24);
            this.label79.TabIndex = 2109;
            this.label79.Text = "H02 T PHASE CURRENT(A)：";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label80.Location = new System.Drawing.Point(1119, 508);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(61, 24);
            this.label80.TabIndex = 2108;
            this.label80.Text = "Value";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label81.Location = new System.Drawing.Point(841, 508);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(262, 24);
            this.label81.TabIndex = 2107;
            this.label81.Text = "H02 S PHASE CURRENT(A)：";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label72.Location = new System.Drawing.Point(1119, 479);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(61, 24);
            this.label72.TabIndex = 2106;
            this.label72.Text = "Value";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label73.Location = new System.Drawing.Point(841, 479);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(263, 24);
            this.label73.TabIndex = 2105;
            this.label73.Text = "H02 R PHASE CURRENT(A)：";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label70.Location = new System.Drawing.Point(1119, 448);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(61, 24);
            this.label70.TabIndex = 2104;
            this.label70.Text = "Value";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label71.Location = new System.Drawing.Point(841, 448);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(281, 24);
            this.label71.TabIndex = 2103;
            this.label71.Text = "H02 T-R PHASE VOLTAGE(V)：";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label68.Location = new System.Drawing.Point(1119, 418);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(61, 24);
            this.label68.TabIndex = 2102;
            this.label68.Text = "Value";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label69.Location = new System.Drawing.Point(841, 418);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(280, 24);
            this.label69.TabIndex = 2101;
            this.label69.Text = "H02 S-T PHASE VOLTAGE(V)：";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label67.Location = new System.Drawing.Point(1119, 389);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(61, 24);
            this.label67.TabIndex = 2100;
            this.label67.Text = "Value";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label66.Location = new System.Drawing.Point(841, 389);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(281, 24);
            this.label66.TabIndex = 2099;
            this.label66.Text = "H02 R-S PHASE VOLTAGE(V)：";
            // 
            // retangleUC14
            // 
            this.retangleUC14.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Dash;
            this.retangleUC14.ExtenderWidth = 3;
            this.retangleUC14.Fill = false;
            this.retangleUC14.GroupID = null;
            this.retangleUC14.Location = new System.Drawing.Point(829, 372);
            this.retangleUC14.Name = "retangleUC14";
            this.retangleUC14.Size = new System.Drawing.Size(1034, 466);
            this.retangleUC14.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC14.TabIndex = 2098;
            // 
            // retangleUC71
            // 
            this.retangleUC71.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC71.ExtenderWidth = 3;
            this.retangleUC71.Fill = false;
            this.retangleUC71.GroupID = null;
            this.retangleUC71.Location = new System.Drawing.Point(1351, 327);
            this.retangleUC71.Name = "retangleUC71";
            this.retangleUC71.Size = new System.Drawing.Size(15, 28);
            this.retangleUC71.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC71.TabIndex = 2050;
            // 
            // retangleUC72
            // 
            this.retangleUC72.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC72.ExtenderWidth = 3;
            this.retangleUC72.Fill = false;
            this.retangleUC72.GroupID = null;
            this.retangleUC72.Location = new System.Drawing.Point(1295, 327);
            this.retangleUC72.Name = "retangleUC72";
            this.retangleUC72.Size = new System.Drawing.Size(15, 28);
            this.retangleUC72.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC72.TabIndex = 2051;
            // 
            // retangleUC77
            // 
            this.retangleUC77.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC77.ExtenderWidth = 3;
            this.retangleUC77.Fill = false;
            this.retangleUC77.GroupID = null;
            this.retangleUC77.Location = new System.Drawing.Point(1351, 292);
            this.retangleUC77.Name = "retangleUC77";
            this.retangleUC77.Size = new System.Drawing.Size(15, 28);
            this.retangleUC77.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC77.TabIndex = 2047;
            // 
            // retangleUC78
            // 
            this.retangleUC78.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC78.ExtenderWidth = 3;
            this.retangleUC78.Fill = false;
            this.retangleUC78.GroupID = null;
            this.retangleUC78.Location = new System.Drawing.Point(1295, 292);
            this.retangleUC78.Name = "retangleUC78";
            this.retangleUC78.Size = new System.Drawing.Size(15, 28);
            this.retangleUC78.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC78.TabIndex = 2048;
            // 
            // retangleUC83
            // 
            this.retangleUC83.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC83.ExtenderWidth = 3;
            this.retangleUC83.Fill = false;
            this.retangleUC83.GroupID = null;
            this.retangleUC83.Location = new System.Drawing.Point(1351, 258);
            this.retangleUC83.Name = "retangleUC83";
            this.retangleUC83.Size = new System.Drawing.Size(15, 28);
            this.retangleUC83.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC83.TabIndex = 2044;
            // 
            // retangleUC84
            // 
            this.retangleUC84.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC84.ExtenderWidth = 3;
            this.retangleUC84.Fill = false;
            this.retangleUC84.GroupID = null;
            this.retangleUC84.Location = new System.Drawing.Point(1295, 258);
            this.retangleUC84.Name = "retangleUC84";
            this.retangleUC84.Size = new System.Drawing.Size(15, 28);
            this.retangleUC84.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC84.TabIndex = 2045;
            // 
            // retangleUC86
            // 
            this.retangleUC86.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC86.ExtenderWidth = 3;
            this.retangleUC86.Fill = false;
            this.retangleUC86.GroupID = null;
            this.retangleUC86.Location = new System.Drawing.Point(1351, 222);
            this.retangleUC86.Name = "retangleUC86";
            this.retangleUC86.Size = new System.Drawing.Size(15, 28);
            this.retangleUC86.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC86.TabIndex = 2041;
            // 
            // retangleUC87
            // 
            this.retangleUC87.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC87.ExtenderWidth = 3;
            this.retangleUC87.Fill = false;
            this.retangleUC87.GroupID = null;
            this.retangleUC87.Location = new System.Drawing.Point(1295, 222);
            this.retangleUC87.Name = "retangleUC87";
            this.retangleUC87.Size = new System.Drawing.Size(15, 28);
            this.retangleUC87.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC87.TabIndex = 2042;
            // 
            // retangleUC89
            // 
            this.retangleUC89.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC89.ExtenderWidth = 3;
            this.retangleUC89.Fill = false;
            this.retangleUC89.GroupID = null;
            this.retangleUC89.Location = new System.Drawing.Point(1351, 187);
            this.retangleUC89.Name = "retangleUC89";
            this.retangleUC89.Size = new System.Drawing.Size(15, 28);
            this.retangleUC89.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC89.TabIndex = 2038;
            // 
            // retangleUC90
            // 
            this.retangleUC90.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC90.ExtenderWidth = 3;
            this.retangleUC90.Fill = false;
            this.retangleUC90.GroupID = null;
            this.retangleUC90.Location = new System.Drawing.Point(1295, 187);
            this.retangleUC90.Name = "retangleUC90";
            this.retangleUC90.Size = new System.Drawing.Size(15, 28);
            this.retangleUC90.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC90.TabIndex = 2039;
            // 
            // retangleUC92
            // 
            this.retangleUC92.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC92.ExtenderWidth = 3;
            this.retangleUC92.Fill = false;
            this.retangleUC92.GroupID = null;
            this.retangleUC92.Location = new System.Drawing.Point(1351, 153);
            this.retangleUC92.Name = "retangleUC92";
            this.retangleUC92.Size = new System.Drawing.Size(15, 28);
            this.retangleUC92.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC92.TabIndex = 2035;
            // 
            // retangleUC93
            // 
            this.retangleUC93.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC93.ExtenderWidth = 3;
            this.retangleUC93.Fill = false;
            this.retangleUC93.GroupID = null;
            this.retangleUC93.Location = new System.Drawing.Point(1295, 153);
            this.retangleUC93.Name = "retangleUC93";
            this.retangleUC93.Size = new System.Drawing.Size(15, 28);
            this.retangleUC93.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC93.TabIndex = 2036;
            // 
            // retangleUC95
            // 
            this.retangleUC95.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC95.ExtenderWidth = 3;
            this.retangleUC95.Fill = false;
            this.retangleUC95.GroupID = null;
            this.retangleUC95.Location = new System.Drawing.Point(1351, 120);
            this.retangleUC95.Name = "retangleUC95";
            this.retangleUC95.Size = new System.Drawing.Size(15, 28);
            this.retangleUC95.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC95.TabIndex = 2032;
            // 
            // retangleUC96
            // 
            this.retangleUC96.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC96.ExtenderWidth = 3;
            this.retangleUC96.Fill = false;
            this.retangleUC96.GroupID = null;
            this.retangleUC96.Location = new System.Drawing.Point(1295, 120);
            this.retangleUC96.Name = "retangleUC96";
            this.retangleUC96.Size = new System.Drawing.Size(15, 28);
            this.retangleUC96.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC96.TabIndex = 2033;
            // 
            // retangleUC98
            // 
            this.retangleUC98.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC98.ExtenderWidth = 3;
            this.retangleUC98.Fill = false;
            this.retangleUC98.GroupID = null;
            this.retangleUC98.Location = new System.Drawing.Point(1351, 85);
            this.retangleUC98.Name = "retangleUC98";
            this.retangleUC98.Size = new System.Drawing.Size(15, 28);
            this.retangleUC98.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC98.TabIndex = 2029;
            // 
            // retangleUC99
            // 
            this.retangleUC99.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC99.ExtenderWidth = 3;
            this.retangleUC99.Fill = false;
            this.retangleUC99.GroupID = null;
            this.retangleUC99.Location = new System.Drawing.Point(1295, 85);
            this.retangleUC99.Name = "retangleUC99";
            this.retangleUC99.Size = new System.Drawing.Size(15, 28);
            this.retangleUC99.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC99.TabIndex = 2030;
            // 
            // retangleUC101
            // 
            this.retangleUC101.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC101.ExtenderWidth = 3;
            this.retangleUC101.Fill = false;
            this.retangleUC101.GroupID = null;
            this.retangleUC101.Location = new System.Drawing.Point(1351, 51);
            this.retangleUC101.Name = "retangleUC101";
            this.retangleUC101.Size = new System.Drawing.Size(15, 28);
            this.retangleUC101.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC101.TabIndex = 2026;
            // 
            // retangleUC102
            // 
            this.retangleUC102.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC102.ExtenderWidth = 3;
            this.retangleUC102.Fill = false;
            this.retangleUC102.GroupID = null;
            this.retangleUC102.Location = new System.Drawing.Point(1295, 51);
            this.retangleUC102.Name = "retangleUC102";
            this.retangleUC102.Size = new System.Drawing.Size(15, 28);
            this.retangleUC102.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC102.TabIndex = 2027;
            // 
            // retangleUC69
            // 
            this.retangleUC69.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC69.ExtenderWidth = 3;
            this.retangleUC69.Fill = false;
            this.retangleUC69.GroupID = null;
            this.retangleUC69.Location = new System.Drawing.Point(1273, 327);
            this.retangleUC69.Name = "retangleUC69";
            this.retangleUC69.Size = new System.Drawing.Size(15, 28);
            this.retangleUC69.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC69.TabIndex = 1960;
            // 
            // retangleUC70
            // 
            this.retangleUC70.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC70.ExtenderWidth = 3;
            this.retangleUC70.Fill = false;
            this.retangleUC70.GroupID = null;
            this.retangleUC70.Location = new System.Drawing.Point(1217, 327);
            this.retangleUC70.Name = "retangleUC70";
            this.retangleUC70.Size = new System.Drawing.Size(15, 28);
            this.retangleUC70.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC70.TabIndex = 1961;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label48.Location = new System.Drawing.Point(841, 329);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(182, 24);
            this.label48.TabIndex = 1956;
            this.label48.Text = "MC81 隔離開關命令";
            // 
            // retangleUC75
            // 
            this.retangleUC75.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC75.ExtenderWidth = 3;
            this.retangleUC75.Fill = false;
            this.retangleUC75.GroupID = null;
            this.retangleUC75.Location = new System.Drawing.Point(1273, 292);
            this.retangleUC75.Name = "retangleUC75";
            this.retangleUC75.Size = new System.Drawing.Size(15, 28);
            this.retangleUC75.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC75.TabIndex = 1953;
            // 
            // retangleUC76
            // 
            this.retangleUC76.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC76.ExtenderWidth = 3;
            this.retangleUC76.Fill = false;
            this.retangleUC76.GroupID = null;
            this.retangleUC76.Location = new System.Drawing.Point(1217, 292);
            this.retangleUC76.Name = "retangleUC76";
            this.retangleUC76.Size = new System.Drawing.Size(15, 28);
            this.retangleUC76.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC76.TabIndex = 1954;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label49.Location = new System.Drawing.Point(841, 294);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(182, 24);
            this.label49.TabIndex = 1949;
            this.label49.Text = "MC41 隔離開關命令";
            // 
            // retangleUC81
            // 
            this.retangleUC81.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC81.ExtenderWidth = 3;
            this.retangleUC81.Fill = false;
            this.retangleUC81.GroupID = null;
            this.retangleUC81.Location = new System.Drawing.Point(1273, 258);
            this.retangleUC81.Name = "retangleUC81";
            this.retangleUC81.Size = new System.Drawing.Size(15, 28);
            this.retangleUC81.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC81.TabIndex = 1946;
            // 
            // retangleUC82
            // 
            this.retangleUC82.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC82.ExtenderWidth = 3;
            this.retangleUC82.Fill = false;
            this.retangleUC82.GroupID = null;
            this.retangleUC82.Location = new System.Drawing.Point(1217, 258);
            this.retangleUC82.Name = "retangleUC82";
            this.retangleUC82.Size = new System.Drawing.Size(15, 28);
            this.retangleUC82.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC82.TabIndex = 1947;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label50.Location = new System.Drawing.Point(841, 260);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(182, 24);
            this.label50.TabIndex = 1942;
            this.label50.Text = "MC22 隔離開關命令";
            // 
            // retangleUC33
            // 
            this.retangleUC33.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC33.ExtenderWidth = 3;
            this.retangleUC33.Fill = false;
            this.retangleUC33.GroupID = null;
            this.retangleUC33.Location = new System.Drawing.Point(1273, 222);
            this.retangleUC33.Name = "retangleUC33";
            this.retangleUC33.Size = new System.Drawing.Size(15, 28);
            this.retangleUC33.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC33.TabIndex = 1939;
            // 
            // retangleUC34
            // 
            this.retangleUC34.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC34.ExtenderWidth = 3;
            this.retangleUC34.Fill = false;
            this.retangleUC34.GroupID = null;
            this.retangleUC34.Location = new System.Drawing.Point(1217, 222);
            this.retangleUC34.Name = "retangleUC34";
            this.retangleUC34.Size = new System.Drawing.Size(15, 28);
            this.retangleUC34.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC34.TabIndex = 1940;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label42.Location = new System.Drawing.Point(841, 224);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(193, 24);
            this.label42.TabIndex = 1935;
            this.label42.Text = "MC22 CB 斷路器命令";
            // 
            // retangleUC39
            // 
            this.retangleUC39.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC39.ExtenderWidth = 3;
            this.retangleUC39.Fill = false;
            this.retangleUC39.GroupID = null;
            this.retangleUC39.Location = new System.Drawing.Point(1273, 187);
            this.retangleUC39.Name = "retangleUC39";
            this.retangleUC39.Size = new System.Drawing.Size(15, 28);
            this.retangleUC39.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC39.TabIndex = 1932;
            // 
            // retangleUC40
            // 
            this.retangleUC40.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC40.ExtenderWidth = 3;
            this.retangleUC40.Fill = false;
            this.retangleUC40.GroupID = null;
            this.retangleUC40.Location = new System.Drawing.Point(1217, 187);
            this.retangleUC40.Name = "retangleUC40";
            this.retangleUC40.Size = new System.Drawing.Size(15, 28);
            this.retangleUC40.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC40.TabIndex = 1933;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label43.Location = new System.Drawing.Point(841, 189);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(182, 24);
            this.label43.TabIndex = 1928;
            this.label43.Text = "MC21 隔離開關命令";
            // 
            // retangleUC45
            // 
            this.retangleUC45.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC45.ExtenderWidth = 3;
            this.retangleUC45.Fill = false;
            this.retangleUC45.GroupID = null;
            this.retangleUC45.Location = new System.Drawing.Point(1273, 153);
            this.retangleUC45.Name = "retangleUC45";
            this.retangleUC45.Size = new System.Drawing.Size(15, 28);
            this.retangleUC45.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC45.TabIndex = 1925;
            // 
            // retangleUC46
            // 
            this.retangleUC46.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC46.ExtenderWidth = 3;
            this.retangleUC46.Fill = false;
            this.retangleUC46.GroupID = null;
            this.retangleUC46.Location = new System.Drawing.Point(1217, 153);
            this.retangleUC46.Name = "retangleUC46";
            this.retangleUC46.Size = new System.Drawing.Size(15, 28);
            this.retangleUC46.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC46.TabIndex = 1926;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label44.Location = new System.Drawing.Point(841, 155);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(193, 24);
            this.label44.TabIndex = 1921;
            this.label44.Text = "MC21 CB 斷路器命令";
            // 
            // retangleUC27
            // 
            this.retangleUC27.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC27.ExtenderWidth = 3;
            this.retangleUC27.Fill = false;
            this.retangleUC27.GroupID = null;
            this.retangleUC27.Location = new System.Drawing.Point(1273, 120);
            this.retangleUC27.Name = "retangleUC27";
            this.retangleUC27.Size = new System.Drawing.Size(15, 28);
            this.retangleUC27.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC27.TabIndex = 1918;
            // 
            // retangleUC28
            // 
            this.retangleUC28.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC28.ExtenderWidth = 3;
            this.retangleUC28.Fill = false;
            this.retangleUC28.GroupID = null;
            this.retangleUC28.Location = new System.Drawing.Point(1217, 120);
            this.retangleUC28.Name = "retangleUC28";
            this.retangleUC28.Size = new System.Drawing.Size(15, 28);
            this.retangleUC28.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC28.TabIndex = 1919;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label41.Location = new System.Drawing.Point(841, 122);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(219, 24);
            this.label41.TabIndex = 1914;
            this.label41.Text = "H04 GIS 高壓斷路器命令";
            // 
            // retangleUC21
            // 
            this.retangleUC21.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC21.ExtenderWidth = 3;
            this.retangleUC21.Fill = false;
            this.retangleUC21.GroupID = null;
            this.retangleUC21.Location = new System.Drawing.Point(1273, 85);
            this.retangleUC21.Name = "retangleUC21";
            this.retangleUC21.Size = new System.Drawing.Size(15, 28);
            this.retangleUC21.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC21.TabIndex = 1911;
            // 
            // retangleUC22
            // 
            this.retangleUC22.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC22.ExtenderWidth = 3;
            this.retangleUC22.Fill = false;
            this.retangleUC22.GroupID = null;
            this.retangleUC22.Location = new System.Drawing.Point(1217, 85);
            this.retangleUC22.Name = "retangleUC22";
            this.retangleUC22.Size = new System.Drawing.Size(15, 28);
            this.retangleUC22.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC22.TabIndex = 1912;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label40.Location = new System.Drawing.Point(841, 87);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(219, 24);
            this.label40.TabIndex = 1907;
            this.label40.Text = "H03 GIS 高壓斷路器命令";
            // 
            // retangleUC17
            // 
            this.retangleUC17.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC17.ExtenderWidth = 3;
            this.retangleUC17.Fill = false;
            this.retangleUC17.GroupID = null;
            this.retangleUC17.Location = new System.Drawing.Point(1273, 51);
            this.retangleUC17.Name = "retangleUC17";
            this.retangleUC17.Size = new System.Drawing.Size(15, 28);
            this.retangleUC17.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC17.TabIndex = 1904;
            // 
            // retangleUC18
            // 
            this.retangleUC18.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC18.ExtenderWidth = 3;
            this.retangleUC18.Fill = false;
            this.retangleUC18.GroupID = null;
            this.retangleUC18.Location = new System.Drawing.Point(1217, 51);
            this.retangleUC18.Name = "retangleUC18";
            this.retangleUC18.Size = new System.Drawing.Size(15, 28);
            this.retangleUC18.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC18.TabIndex = 1905;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label39.Location = new System.Drawing.Point(841, 53);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(219, 24);
            this.label39.TabIndex = 1901;
            this.label39.Text = "H02 GIS 高壓斷路器命令";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label35.Location = new System.Drawing.Point(546, 404);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(41, 34);
            this.label35.TabIndex = 1900;
            this.label35.Text = "24kV\r\n630A";
            // 
            // switch05UC1
            // 
            this.switch05UC1.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.switch05UC1.ExtenderWidth = 2;
            this.switch05UC1.Fill = false;
            this.switch05UC1.GroupID = null;
            this.switch05UC1.Location = new System.Drawing.Point(364, 667);
            this.switch05UC1.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.switch05UC1.Name = "switch05UC1";
            this.switch05UC1.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.switch05UC1.Size = new System.Drawing.Size(60, 82);
            this.switch05UC1.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.switch05UC1.TabIndex = 1875;
            // 
            // switch03UC3
            // 
            this.switch03UC3.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.switch03UC3.ExtenderWidth = 2;
            this.switch03UC3.Fill = false;
            this.switch03UC3.GroupID = null;
            this.switch03UC3.Location = new System.Drawing.Point(388, 590);
            this.switch03UC3.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.switch03UC3.Name = "switch03UC3";
            this.switch03UC3.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.switch03UC3.Size = new System.Drawing.Size(26, 90);
            this.switch03UC3.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.switch03UC3.TabIndex = 1899;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("微軟正黑體", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label34.Location = new System.Drawing.Point(473, 712);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(102, 36);
            this.label34.TabIndex = 1898;
            this.label34.Text = "TSS站內及號誌\r\n通信低壓用電";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("微軟正黑體", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label33.Location = new System.Drawing.Point(534, 53);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(153, 24);
            this.label33.TabIndex = 1896;
            this.label33.Text = "內惟變電所(D/S)";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("微軟正黑體", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label32.Location = new System.Drawing.Point(368, 53);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(153, 24);
            this.label32.TabIndex = 1895;
            this.label32.Text = "三民變電所(D/S)";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("微軟正黑體", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label31.Location = new System.Drawing.Point(183, 53);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(149, 24);
            this.label31.TabIndex = 1894;
            this.label31.Text = "TSS7輕軌設備室";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label30.Location = new System.Drawing.Point(421, 77);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(100, 34);
            this.label30.TabIndex = 1893;
            this.label30.Text = "FROM TPC\r\n3 Ø 3W 22.8kV\r\n";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label29.Location = new System.Drawing.Point(544, 77);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(100, 34);
            this.label29.TabIndex = 1892;
            this.label29.Text = "FROM TPC\r\n3 Ø 3W 22.8kV\r\n";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label28.Location = new System.Drawing.Point(244, 757);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(49, 15);
            this.label28.TabIndex = 1891;
            this.label28.Text = "\"MC41\"";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label27.Location = new System.Drawing.Point(317, 791);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(43, 30);
            this.label27.TabIndex = 1890;
            this.label27.Text = "NT7C\r\n4000A";
            // 
            // lineUC14
            // 
            this.lineUC14.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC14.ExtenderWidth = 2;
            this.lineUC14.Fill = false;
            this.lineUC14.GroupID = null;
            this.lineUC14.Location = new System.Drawing.Point(390, 734);
            this.lineUC14.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.lineUC14.Name = "lineUC14";
            this.lineUC14.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC14.Size = new System.Drawing.Size(2, 90);
            this.lineUC14.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC14.TabIndex = 1889;
            // 
            // switch05UC3
            // 
            this.switch05UC3.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.switch05UC3.ExtenderWidth = 2;
            this.switch05UC3.Fill = false;
            this.switch05UC3.GroupID = null;
            this.switch05UC3.Location = new System.Drawing.Point(292, 756);
            this.switch05UC3.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.switch05UC3.Name = "switch05UC3";
            this.switch05UC3.Size = new System.Drawing.Size(100, 37);
            this.switch05UC3.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.switch05UC3.TabIndex = 1886;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label26.Location = new System.Drawing.Point(332, 676);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(43, 30);
            this.label26.TabIndex = 1885;
            this.label26.Text = "NT7B\r\n4000A";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label25.Location = new System.Drawing.Point(343, 623);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(43, 30);
            this.label25.TabIndex = 1884;
            this.label25.Text = "QA72\r\n4000A";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label24.Location = new System.Drawing.Point(232, 676);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(43, 30);
            this.label24.TabIndex = 1883;
            this.label24.Text = "NT7A\r\n4000A";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label23.Location = new System.Drawing.Point(235, 623);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(43, 30);
            this.label23.TabIndex = 1882;
            this.label23.Text = "QA71\r\n4000A";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label22.Location = new System.Drawing.Point(337, 607);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(54, 16);
            this.label22.TabIndex = 1881;
            this.label22.Text = "\"MC22\"";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label21.Location = new System.Drawing.Point(228, 607);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(54, 16);
            this.label21.TabIndex = 1880;
            this.label21.Text = "\"MC21\"";
            // 
            // switch05UC2
            // 
            this.switch05UC2.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.switch05UC2.ExtenderWidth = 2;
            this.switch05UC2.Fill = false;
            this.switch05UC2.GroupID = null;
            this.switch05UC2.Location = new System.Drawing.Point(264, 672);
            this.switch05UC2.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.switch05UC2.Name = "switch05UC2";
            this.switch05UC2.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.switch05UC2.Size = new System.Drawing.Size(60, 82);
            this.switch05UC2.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.switch05UC2.TabIndex = 1877;
            // 
            // switch03UC2
            // 
            this.switch03UC2.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.switch03UC2.ExtenderWidth = 2;
            this.switch03UC2.Fill = false;
            this.switch03UC2.GroupID = null;
            this.switch03UC2.Location = new System.Drawing.Point(287, 590);
            this.switch03UC2.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.switch03UC2.Name = "switch03UC2";
            this.switch03UC2.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.switch03UC2.Size = new System.Drawing.Size(26, 90);
            this.switch03UC2.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.switch03UC2.TabIndex = 1876;
            // 
            // circleUC4
            // 
            this.circleUC4.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC4.ExtenderWidth = 3;
            this.circleUC4.Fill = false;
            this.circleUC4.GroupID = null;
            this.circleUC4.Location = new System.Drawing.Point(580, 603);
            this.circleUC4.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.circleUC4.Name = "circleUC4";
            this.circleUC4.Size = new System.Drawing.Size(10, 9);
            this.circleUC4.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC4.TabIndex = 1874;
            // 
            // circleUC3
            // 
            this.circleUC3.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC3.ExtenderWidth = 3;
            this.circleUC3.Fill = false;
            this.circleUC3.GroupID = null;
            this.circleUC3.Location = new System.Drawing.Point(460, 603);
            this.circleUC3.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.circleUC3.Name = "circleUC3";
            this.circleUC3.Size = new System.Drawing.Size(10, 9);
            this.circleUC3.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC3.TabIndex = 1873;
            // 
            // circleUC2
            // 
            this.circleUC2.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC2.ExtenderWidth = 3;
            this.circleUC2.Fill = false;
            this.circleUC2.GroupID = null;
            this.circleUC2.Location = new System.Drawing.Point(518, 603);
            this.circleUC2.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.circleUC2.Name = "circleUC2";
            this.circleUC2.Size = new System.Drawing.Size(10, 9);
            this.circleUC2.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC2.TabIndex = 1872;
            // 
            // airCircuitBreaker4
            // 
            this.airCircuitBreaker4.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.airCircuitBreaker4.ExtenderWidth = 3;
            this.airCircuitBreaker4.Fill = false;
            this.airCircuitBreaker4.GroupID = null;
            this.airCircuitBreaker4.Location = new System.Drawing.Point(460, 608);
            this.airCircuitBreaker4.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.airCircuitBreaker4.Name = "airCircuitBreaker4";
            this.airCircuitBreaker4.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.airCircuitBreaker4.Size = new System.Drawing.Size(17, 91);
            this.airCircuitBreaker4.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.airCircuitBreaker4.TabIndex = 1871;
            // 
            // airCircuitBreaker3
            // 
            this.airCircuitBreaker3.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.airCircuitBreaker3.ExtenderWidth = 3;
            this.airCircuitBreaker3.Fill = false;
            this.airCircuitBreaker3.GroupID = null;
            this.airCircuitBreaker3.Location = new System.Drawing.Point(580, 612);
            this.airCircuitBreaker3.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.airCircuitBreaker3.Name = "airCircuitBreaker3";
            this.airCircuitBreaker3.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.airCircuitBreaker3.Size = new System.Drawing.Size(17, 91);
            this.airCircuitBreaker3.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.airCircuitBreaker3.TabIndex = 1870;
            // 
            // airCircuitBreaker2
            // 
            this.airCircuitBreaker2.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.airCircuitBreaker2.ExtenderWidth = 3;
            this.airCircuitBreaker2.Fill = false;
            this.airCircuitBreaker2.GroupID = null;
            this.airCircuitBreaker2.Location = new System.Drawing.Point(518, 608);
            this.airCircuitBreaker2.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.airCircuitBreaker2.Name = "airCircuitBreaker2";
            this.airCircuitBreaker2.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.airCircuitBreaker2.Size = new System.Drawing.Size(17, 91);
            this.airCircuitBreaker2.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.airCircuitBreaker2.TabIndex = 1869;
            // 
            // lineUC10
            // 
            this.lineUC10.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC10.ExtenderWidth = 3;
            this.lineUC10.Fill = false;
            this.lineUC10.GroupID = null;
            this.lineUC10.Location = new System.Drawing.Point(448, 605);
            this.lineUC10.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.lineUC10.Name = "lineUC10";
            this.lineUC10.Size = new System.Drawing.Size(150, 3);
            this.lineUC10.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC10.TabIndex = 1868;
            // 
            // airCircuitBreaker1
            // 
            this.airCircuitBreaker1.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.airCircuitBreaker1.ExtenderWidth = 3;
            this.airCircuitBreaker1.Fill = false;
            this.airCircuitBreaker1.GroupID = null;
            this.airCircuitBreaker1.Location = new System.Drawing.Point(518, 513);
            this.airCircuitBreaker1.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.airCircuitBreaker1.Name = "airCircuitBreaker1";
            this.airCircuitBreaker1.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.airCircuitBreaker1.Size = new System.Drawing.Size(17, 91);
            this.airCircuitBreaker1.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.airCircuitBreaker1.TabIndex = 1867;
            // 
            // transformer02UC1
            // 
            this.transformer02UC1.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.transformer02UC1.ExtenderWidth = 3;
            this.transformer02UC1.Fill = false;
            this.transformer02UC1.GroupID = null;
            this.transformer02UC1.Location = new System.Drawing.Point(512, 439);
            this.transformer02UC1.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.transformer02UC1.Name = "transformer02UC1";
            this.transformer02UC1.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.transformer02UC1.Size = new System.Drawing.Size(24, 84);
            this.transformer02UC1.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.transformer02UC1.TabIndex = 1866;
            // 
            // circleUC1
            // 
            this.circleUC1.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC1.ExtenderWidth = 3;
            this.circleUC1.Fill = false;
            this.circleUC1.GroupID = null;
            this.circleUC1.Location = new System.Drawing.Point(348, 585);
            this.circleUC1.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.circleUC1.Name = "circleUC1";
            this.circleUC1.Size = new System.Drawing.Size(8, 9);
            this.circleUC1.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC1.TabIndex = 1865;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label17.Location = new System.Drawing.Point(421, 467);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(50, 48);
            this.label17.TabIndex = 1856;
            this.label17.Text = "RT.\r\n2000kV\r\n22.8kV";
            // 
            // lineUC12
            // 
            this.lineUC12.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC12.ExtenderWidth = 3;
            this.lineUC12.Fill = false;
            this.lineUC12.GroupID = null;
            this.lineUC12.Location = new System.Drawing.Point(260, 587);
            this.lineUC12.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.lineUC12.Name = "lineUC12";
            this.lineUC12.Size = new System.Drawing.Size(150, 3);
            this.lineUC12.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC12.TabIndex = 1863;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label20.Location = new System.Drawing.Point(251, 550);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(58, 17);
            this.label20.TabIndex = 1862;
            this.label20.Text = "\"MC81\"";
            // 
            // transformer01UC1
            // 
            this.transformer01UC1.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.transformer01UC1.ExtenderWidth = 3;
            this.transformer01UC1.Fill = false;
            this.transformer01UC1.GroupID = null;
            this.transformer01UC1.Location = new System.Drawing.Point(336, 436);
            this.transformer01UC1.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.transformer01UC1.Name = "transformer01UC1";
            this.transformer01UC1.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.transformer01UC1.Size = new System.Drawing.Size(33, 49);
            this.transformer01UC1.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.transformer01UC1.TabIndex = 1860;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label19.Location = new System.Drawing.Point(627, 512);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(21, 68);
            this.label19.TabIndex = 1859;
            this.label19.Text = "控\r\n制\r\n電\r\n源";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label18.Location = new System.Drawing.Point(545, 459);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(69, 80);
            this.label18.TabIndex = 1857;
            this.label18.Text = "AUX TR.\r\n100kV\r\n22.8kV-\r\n380 / 220V\r\n6%";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label16.Location = new System.Drawing.Point(267, 518);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(59, 17);
            this.label16.TabIndex = 1855;
            this.label16.Text = "\"MG01\"";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label15.Location = new System.Drawing.Point(543, 274);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 34);
            this.label15.TabIndex = 1853;
            this.label15.Text = "24kV\r\n630A";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label45.Location = new System.Drawing.Point(650, 427);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(23, 16);
            this.label45.TabIndex = 1851;
            this.label45.Text = "PT";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label13.Location = new System.Drawing.Point(653, 369);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(24, 16);
            this.label13.TabIndex = 1851;
            this.label13.Text = "DS";
            // 
            // retangleUC6
            // 
            this.retangleUC6.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC6.ExtenderWidth = 3;
            this.retangleUC6.Fill = false;
            this.retangleUC6.GroupID = null;
            this.retangleUC6.Location = new System.Drawing.Point(511, 283);
            this.retangleUC6.Name = "retangleUC6";
            this.retangleUC6.Size = new System.Drawing.Size(28, 20);
            this.retangleUC6.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC6.TabIndex = 1847;
            // 
            // retangleUC5
            // 
            this.retangleUC5.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC5.ExtenderWidth = 3;
            this.retangleUC5.Fill = false;
            this.retangleUC5.GroupID = null;
            this.retangleUC5.Location = new System.Drawing.Point(337, 417);
            this.retangleUC5.Name = "retangleUC5";
            this.retangleUC5.Size = new System.Drawing.Size(28, 20);
            this.retangleUC5.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC5.TabIndex = 1846;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label9.Location = new System.Drawing.Point(300, 274);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(42, 17);
            this.label9.TabIndex = 1841;
            this.label9.Text = "\"GIS\"";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label8.Location = new System.Drawing.Point(375, 404);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 34);
            this.label8.TabIndex = 1840;
            this.label8.Text = "24kV\r\n630A";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label6.Location = new System.Drawing.Point(365, 382);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(24, 16);
            this.label6.TabIndex = 1838;
            this.label6.Text = "DS";
            // 
            // lineUC7
            // 
            this.lineUC7.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC7.ExtenderWidth = 3;
            this.lineUC7.Fill = false;
            this.lineUC7.GroupID = null;
            this.lineUC7.Location = new System.Drawing.Point(524, 259);
            this.lineUC7.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.lineUC7.Name = "lineUC7";
            this.lineUC7.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC7.Size = new System.Drawing.Size(3, 25);
            this.lineUC7.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC7.TabIndex = 1836;
            // 
            // retangleUC4
            // 
            this.retangleUC4.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC4.ExtenderWidth = 3;
            this.retangleUC4.Fill = false;
            this.retangleUC4.GroupID = null;
            this.retangleUC4.Location = new System.Drawing.Point(511, 415);
            this.retangleUC4.Name = "retangleUC4";
            this.retangleUC4.Size = new System.Drawing.Size(28, 20);
            this.retangleUC4.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC4.TabIndex = 1835;
            // 
            // lineUC6
            // 
            this.lineUC6.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC6.ExtenderWidth = 3;
            this.lineUC6.Fill = false;
            this.lineUC6.GroupID = null;
            this.lineUC6.Location = new System.Drawing.Point(572, 223);
            this.lineUC6.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.lineUC6.Name = "lineUC6";
            this.lineUC6.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC6.Size = new System.Drawing.Size(3, 36);
            this.lineUC6.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC6.TabIndex = 1834;
            // 
            // lineUC5
            // 
            this.lineUC5.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC5.ExtenderWidth = 3;
            this.lineUC5.Fill = false;
            this.lineUC5.GroupID = null;
            this.lineUC5.Location = new System.Drawing.Point(476, 225);
            this.lineUC5.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.lineUC5.Name = "lineUC5";
            this.lineUC5.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC5.Size = new System.Drawing.Size(3, 34);
            this.lineUC5.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC5.TabIndex = 1833;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label5.Location = new System.Drawing.Point(605, 197);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 34);
            this.label5.TabIndex = 1831;
            this.label5.Text = "24kV\r\n630A";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label4.Location = new System.Drawing.Point(422, 197);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 34);
            this.label4.TabIndex = 1830;
            this.label4.Text = "24kV\r\n630A";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label3.Location = new System.Drawing.Point(300, 197);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 17);
            this.label3.TabIndex = 1829;
            this.label3.Text = "\"HVAS\"";
            // 
            // lineUC4
            // 
            this.lineUC4.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC4.ExtenderWidth = 3;
            this.lineUC4.Fill = false;
            this.lineUC4.GroupID = null;
            this.lineUC4.Location = new System.Drawing.Point(476, 126);
            this.lineUC4.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.lineUC4.Name = "lineUC4";
            this.lineUC4.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC4.Size = new System.Drawing.Size(3, 25);
            this.lineUC4.State = iSCADA.Design.Utilities.Electrical.SymbolState.Default;
            this.lineUC4.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC4.TabIndex = 1826;
            // 
            // lineUC3
            // 
            this.lineUC3.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC3.ExtenderWidth = 3;
            this.lineUC3.Fill = false;
            this.lineUC3.GroupID = null;
            this.lineUC3.Location = new System.Drawing.Point(572, 126);
            this.lineUC3.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.lineUC3.Name = "lineUC3";
            this.lineUC3.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC3.Size = new System.Drawing.Size(3, 25);
            this.lineUC3.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC3.TabIndex = 1825;
            // 
            // lineUC2
            // 
            this.lineUC2.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC2.ExtenderWidth = 3;
            this.lineUC2.Fill = false;
            this.lineUC2.GroupID = null;
            this.lineUC2.Location = new System.Drawing.Point(478, 254);
            this.lineUC2.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.lineUC2.Name = "lineUC2";
            this.lineUC2.Size = new System.Drawing.Size(96, 3);
            this.lineUC2.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC2.TabIndex = 1824;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(550, 152);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 21);
            this.label2.TabIndex = 1822;
            this.label2.Text = "MOF";
            // 
            // retangleUC2
            // 
            this.retangleUC2.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.DashDot;
            this.retangleUC2.ExtenderWidth = 3;
            this.retangleUC2.Fill = false;
            this.retangleUC2.GroupID = null;
            this.retangleUC2.Location = new System.Drawing.Point(547, 150);
            this.retangleUC2.Name = "retangleUC2";
            this.retangleUC2.Size = new System.Drawing.Size(53, 28);
            this.retangleUC2.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC2.TabIndex = 1821;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(451, 154);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 21);
            this.label1.TabIndex = 1820;
            this.label1.Text = "MOF";
            // 
            // retangleUC1
            // 
            this.retangleUC1.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.DashDot;
            this.retangleUC1.ExtenderWidth = 3;
            this.retangleUC1.Fill = false;
            this.retangleUC1.GroupID = null;
            this.retangleUC1.Location = new System.Drawing.Point(448, 150);
            this.retangleUC1.Name = "retangleUC1";
            this.retangleUC1.Size = new System.Drawing.Size(53, 28);
            this.retangleUC1.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC1.TabIndex = 1819;
            // 
            // retangleUC3
            // 
            this.retangleUC3.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC3.ExtenderWidth = 3;
            this.retangleUC3.Fill = false;
            this.retangleUC3.GroupID = null;
            this.retangleUC3.Location = new System.Drawing.Point(293, 187);
            this.retangleUC3.Name = "retangleUC3";
            this.retangleUC3.Size = new System.Drawing.Size(408, 80);
            this.retangleUC3.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC3.TabIndex = 1832;
            // 
            // retangleUC7
            // 
            this.retangleUC7.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Dash;
            this.retangleUC7.ExtenderWidth = 3;
            this.retangleUC7.Fill = false;
            this.retangleUC7.GroupID = null;
            this.retangleUC7.Location = new System.Drawing.Point(293, 270);
            this.retangleUC7.Name = "retangleUC7";
            this.retangleUC7.Size = new System.Drawing.Size(408, 182);
            this.retangleUC7.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC7.TabIndex = 1854;
            // 
            // retangleUC8
            // 
            this.retangleUC8.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Dash;
            this.retangleUC8.ExtenderWidth = 3;
            this.retangleUC8.Fill = false;
            this.retangleUC8.GroupID = null;
            this.retangleUC8.Location = new System.Drawing.Point(249, 749);
            this.retangleUC8.Name = "retangleUC8";
            this.retangleUC8.Size = new System.Drawing.Size(180, 70);
            this.retangleUC8.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC8.TabIndex = 1864;
            // 
            // retangleUC9
            // 
            this.retangleUC9.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Dash;
            this.retangleUC9.ExtenderWidth = 3;
            this.retangleUC9.Fill = false;
            this.retangleUC9.GroupID = null;
            this.retangleUC9.Location = new System.Drawing.Point(249, 605);
            this.retangleUC9.Name = "retangleUC9";
            this.retangleUC9.Size = new System.Drawing.Size(83, 125);
            this.retangleUC9.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC9.TabIndex = 1878;
            // 
            // retangleUC10
            // 
            this.retangleUC10.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Dash;
            this.retangleUC10.ExtenderWidth = 3;
            this.retangleUC10.Fill = false;
            this.retangleUC10.GroupID = null;
            this.retangleUC10.Location = new System.Drawing.Point(346, 605);
            this.retangleUC10.Name = "retangleUC10";
            this.retangleUC10.Size = new System.Drawing.Size(83, 125);
            this.retangleUC10.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC10.TabIndex = 1879;
            // 
            // retangleUC11
            // 
            this.retangleUC11.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Dash;
            this.retangleUC11.ExtenderWidth = 3;
            this.retangleUC11.Fill = false;
            this.retangleUC11.GroupID = null;
            this.retangleUC11.Location = new System.Drawing.Point(249, 536);
            this.retangleUC11.Name = "retangleUC11";
            this.retangleUC11.Size = new System.Drawing.Size(180, 66);
            this.retangleUC11.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC11.TabIndex = 1887;
            // 
            // retangleUC12
            // 
            this.retangleUC12.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Dash;
            this.retangleUC12.ExtenderWidth = 3;
            this.retangleUC12.Fill = false;
            this.retangleUC12.GroupID = null;
            this.retangleUC12.Location = new System.Drawing.Point(160, 39);
            this.retangleUC12.Name = "retangleUC12";
            this.retangleUC12.Size = new System.Drawing.Size(584, 799);
            this.retangleUC12.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC12.TabIndex = 1897;
            // 
            // retangleUC13
            // 
            this.retangleUC13.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Dash;
            this.retangleUC13.ExtenderWidth = 3;
            this.retangleUC13.Fill = false;
            this.retangleUC13.GroupID = null;
            this.retangleUC13.Location = new System.Drawing.Point(829, 38);
            this.retangleUC13.Name = "retangleUC13";
            this.retangleUC13.Size = new System.Drawing.Size(584, 325);
            this.retangleUC13.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC13.TabIndex = 2097;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tabControl1.Location = new System.Drawing.Point(0, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1910, 900);
            this.tabControl1.TabIndex = 1820;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tabPage1.Controls.Add(this.circleUC96);
            this.tabPage1.Controls.Add(this.circleUC97);
            this.tabPage1.Controls.Add(this.circleUC98);
            this.tabPage1.Controls.Add(this.circleUC99);
            this.tabPage1.Controls.Add(this.circleUC84);
            this.tabPage1.Controls.Add(this.circleUC85);
            this.tabPage1.Controls.Add(this.circleUC86);
            this.tabPage1.Controls.Add(this.circleUC87);
            this.tabPage1.Controls.Add(this.circleUC88);
            this.tabPage1.Controls.Add(this.circleUC89);
            this.tabPage1.Controls.Add(this.circleUC90);
            this.tabPage1.Controls.Add(this.circleUC91);
            this.tabPage1.Controls.Add(this.circleUC92);
            this.tabPage1.Controls.Add(this.circleUC93);
            this.tabPage1.Controls.Add(this.circleUC94);
            this.tabPage1.Controls.Add(this.circleUC95);
            this.tabPage1.Controls.Add(this.circleUC66);
            this.tabPage1.Controls.Add(this.circleUC70);
            this.tabPage1.Controls.Add(this.circleUC71);
            this.tabPage1.Controls.Add(this.circleUC72);
            this.tabPage1.Controls.Add(this.circleUC73);
            this.tabPage1.Controls.Add(this.circleUC74);
            this.tabPage1.Controls.Add(this.circleUC75);
            this.tabPage1.Controls.Add(this.circleUC76);
            this.tabPage1.Controls.Add(this.circleUC77);
            this.tabPage1.Controls.Add(this.circleUC78);
            this.tabPage1.Controls.Add(this.circleUC79);
            this.tabPage1.Controls.Add(this.circleUC80);
            this.tabPage1.Controls.Add(this.circleUC81);
            this.tabPage1.Controls.Add(this.circleUC123);
            this.tabPage1.Controls.Add(this.circleUC122);
            this.tabPage1.Controls.Add(this.circleUC121);
            this.tabPage1.Controls.Add(this.circleUC120);
            this.tabPage1.Controls.Add(this.circleUC119);
            this.tabPage1.Controls.Add(this.circleUC118);
            this.tabPage1.Controls.Add(this.circleUC117);
            this.tabPage1.Controls.Add(this.circleUC116);
            this.tabPage1.Controls.Add(this.circleUC115);
            this.tabPage1.Controls.Add(this.circleUC114);
            this.tabPage1.Controls.Add(this.circleUC113);
            this.tabPage1.Controls.Add(this.circleUC112);
            this.tabPage1.Controls.Add(this.circleUC111);
            this.tabPage1.Controls.Add(this.circleUC110);
            this.tabPage1.Controls.Add(this.circleUC109);
            this.tabPage1.Controls.Add(this.circleUC108);
            this.tabPage1.Controls.Add(this.circleUC107);
            this.tabPage1.Controls.Add(this.circleUC106);
            this.tabPage1.Controls.Add(this.circleUC105);
            this.tabPage1.Controls.Add(this.circleUC104);
            this.tabPage1.Controls.Add(this.circleUC103);
            this.tabPage1.Controls.Add(this.circleUC102);
            this.tabPage1.Controls.Add(this.circleUC82);
            this.tabPage1.Controls.Add(this.circleUC65);
            this.tabPage1.Controls.Add(this.circleUC55);
            this.tabPage1.Controls.Add(this.circleUC56);
            this.tabPage1.Controls.Add(this.circleUC57);
            this.tabPage1.Controls.Add(this.circleUC58);
            this.tabPage1.Controls.Add(this.circleUC59);
            this.tabPage1.Controls.Add(this.circleUC60);
            this.tabPage1.Controls.Add(this.circleUC61);
            this.tabPage1.Controls.Add(this.circleUC62);
            this.tabPage1.Controls.Add(this.circleUC63);
            this.tabPage1.Controls.Add(this.circleUC64);
            this.tabPage1.Controls.Add(this.circleUC101);
            this.tabPage1.Controls.Add(this.circleUC100);
            this.tabPage1.Controls.Add(this.circleUC69);
            this.tabPage1.Controls.Add(this.circleUC83);
            this.tabPage1.Controls.Add(this.circleUC68);
            this.tabPage1.Controls.Add(this.circleUC44);
            this.tabPage1.Controls.Add(this.circleUC45);
            this.tabPage1.Controls.Add(this.circleUC46);
            this.tabPage1.Controls.Add(this.circleUC47);
            this.tabPage1.Controls.Add(this.circleUC48);
            this.tabPage1.Controls.Add(this.circleUC49);
            this.tabPage1.Controls.Add(this.circleUC50);
            this.tabPage1.Controls.Add(this.circleUC51);
            this.tabPage1.Controls.Add(this.circleUC52);
            this.tabPage1.Controls.Add(this.circleUC53);
            this.tabPage1.Controls.Add(this.circleUC38);
            this.tabPage1.Controls.Add(this.circleUC39);
            this.tabPage1.Controls.Add(this.circleUC40);
            this.tabPage1.Controls.Add(this.circleUC41);
            this.tabPage1.Controls.Add(this.circleUC42);
            this.tabPage1.Controls.Add(this.circleUC43);
            this.tabPage1.Controls.Add(this.circleUC37);
            this.tabPage1.Controls.Add(this.circleUC36);
            this.tabPage1.Controls.Add(this.circleUC35);
            this.tabPage1.Controls.Add(this.circleUC67);
            this.tabPage1.Controls.Add(this.circleUC54);
            this.tabPage1.Controls.Add(this.circleUC34);
            this.tabPage1.Controls.Add(this.circleUC33);
            this.tabPage1.Controls.Add(this.circleUC32);
            this.tabPage1.Controls.Add(this.circleUC31);
            this.tabPage1.Controls.Add(this.circleUC30);
            this.tabPage1.Controls.Add(this.circleUC29);
            this.tabPage1.Controls.Add(this.circleUC28);
            this.tabPage1.Controls.Add(this.circleUC27);
            this.tabPage1.Controls.Add(this.label262);
            this.tabPage1.Controls.Add(this.label266);
            this.tabPage1.Controls.Add(this.label267);
            this.tabPage1.Controls.Add(this.label268);
            this.tabPage1.Controls.Add(this.label269);
            this.tabPage1.Controls.Add(this.label270);
            this.tabPage1.Controls.Add(this.label271);
            this.tabPage1.Controls.Add(this.label272);
            this.tabPage1.Controls.Add(this.label273);
            this.tabPage1.Controls.Add(this.label274);
            this.tabPage1.Controls.Add(this.label275);
            this.tabPage1.Controls.Add(this.label276);
            this.tabPage1.Controls.Add(this.label277);
            this.tabPage1.Controls.Add(this.label278);
            this.tabPage1.Controls.Add(this.label279);
            this.tabPage1.Controls.Add(this.label280);
            this.tabPage1.Controls.Add(this.label281);
            this.tabPage1.Controls.Add(this.label282);
            this.tabPage1.Controls.Add(this.label283);
            this.tabPage1.Controls.Add(this.label284);
            this.tabPage1.Controls.Add(this.label285);
            this.tabPage1.Controls.Add(this.label259);
            this.tabPage1.Controls.Add(this.label260);
            this.tabPage1.Controls.Add(this.label261);
            this.tabPage1.Controls.Add(this.label247);
            this.tabPage1.Controls.Add(this.label248);
            this.tabPage1.Controls.Add(this.label249);
            this.tabPage1.Controls.Add(this.label250);
            this.tabPage1.Controls.Add(this.label251);
            this.tabPage1.Controls.Add(this.label252);
            this.tabPage1.Controls.Add(this.label253);
            this.tabPage1.Controls.Add(this.label254);
            this.tabPage1.Controls.Add(this.label255);
            this.tabPage1.Controls.Add(this.label256);
            this.tabPage1.Controls.Add(this.label257);
            this.tabPage1.Controls.Add(this.label258);
            this.tabPage1.Controls.Add(this.label226);
            this.tabPage1.Controls.Add(this.label227);
            this.tabPage1.Controls.Add(this.label228);
            this.tabPage1.Controls.Add(this.label229);
            this.tabPage1.Controls.Add(this.label230);
            this.tabPage1.Controls.Add(this.label231);
            this.tabPage1.Controls.Add(this.label232);
            this.tabPage1.Controls.Add(this.label233);
            this.tabPage1.Controls.Add(this.label234);
            this.tabPage1.Controls.Add(this.label235);
            this.tabPage1.Controls.Add(this.label236);
            this.tabPage1.Controls.Add(this.label237);
            this.tabPage1.Controls.Add(this.label238);
            this.tabPage1.Controls.Add(this.label239);
            this.tabPage1.Controls.Add(this.label240);
            this.tabPage1.Controls.Add(this.label241);
            this.tabPage1.Controls.Add(this.label242);
            this.tabPage1.Controls.Add(this.label243);
            this.tabPage1.Controls.Add(this.label244);
            this.tabPage1.Controls.Add(this.label245);
            this.tabPage1.Controls.Add(this.label246);
            this.tabPage1.Controls.Add(this.label205);
            this.tabPage1.Controls.Add(this.label206);
            this.tabPage1.Controls.Add(this.label207);
            this.tabPage1.Controls.Add(this.label208);
            this.tabPage1.Controls.Add(this.label209);
            this.tabPage1.Controls.Add(this.label210);
            this.tabPage1.Controls.Add(this.label211);
            this.tabPage1.Controls.Add(this.label212);
            this.tabPage1.Controls.Add(this.label213);
            this.tabPage1.Controls.Add(this.label214);
            this.tabPage1.Controls.Add(this.label215);
            this.tabPage1.Controls.Add(this.label216);
            this.tabPage1.Controls.Add(this.label217);
            this.tabPage1.Controls.Add(this.label218);
            this.tabPage1.Controls.Add(this.label219);
            this.tabPage1.Controls.Add(this.label220);
            this.tabPage1.Controls.Add(this.label221);
            this.tabPage1.Controls.Add(this.label222);
            this.tabPage1.Controls.Add(this.label223);
            this.tabPage1.Controls.Add(this.label224);
            this.tabPage1.Controls.Add(this.label225);
            this.tabPage1.Controls.Add(this.label184);
            this.tabPage1.Controls.Add(this.label185);
            this.tabPage1.Controls.Add(this.label186);
            this.tabPage1.Controls.Add(this.label187);
            this.tabPage1.Controls.Add(this.label188);
            this.tabPage1.Controls.Add(this.label189);
            this.tabPage1.Controls.Add(this.label190);
            this.tabPage1.Controls.Add(this.label191);
            this.tabPage1.Controls.Add(this.label192);
            this.tabPage1.Controls.Add(this.label193);
            this.tabPage1.Controls.Add(this.label194);
            this.tabPage1.Controls.Add(this.label195);
            this.tabPage1.Controls.Add(this.label196);
            this.tabPage1.Controls.Add(this.label197);
            this.tabPage1.Controls.Add(this.label198);
            this.tabPage1.Controls.Add(this.label199);
            this.tabPage1.Controls.Add(this.label200);
            this.tabPage1.Controls.Add(this.label201);
            this.tabPage1.Controls.Add(this.label202);
            this.tabPage1.Controls.Add(this.label203);
            this.tabPage1.Controls.Add(this.label204);
            this.tabPage1.Controls.Add(this.label164);
            this.tabPage1.Controls.Add(this.label165);
            this.tabPage1.Controls.Add(this.label166);
            this.tabPage1.Controls.Add(this.label167);
            this.tabPage1.Controls.Add(this.label168);
            this.tabPage1.Controls.Add(this.label169);
            this.tabPage1.Controls.Add(this.label170);
            this.tabPage1.Controls.Add(this.label171);
            this.tabPage1.Controls.Add(this.label172);
            this.tabPage1.Controls.Add(this.label173);
            this.tabPage1.Controls.Add(this.label174);
            this.tabPage1.Controls.Add(this.label175);
            this.tabPage1.Controls.Add(this.label176);
            this.tabPage1.Controls.Add(this.label177);
            this.tabPage1.Controls.Add(this.label178);
            this.tabPage1.Controls.Add(this.label179);
            this.tabPage1.Controls.Add(this.label180);
            this.tabPage1.Controls.Add(this.label181);
            this.tabPage1.Controls.Add(this.label182);
            this.tabPage1.Controls.Add(this.label183);
            this.tabPage1.Controls.Add(this.label162);
            this.tabPage1.Controls.Add(this.label161);
            this.tabPage1.Controls.Add(this.label160);
            this.tabPage1.Controls.Add(this.label159);
            this.tabPage1.Controls.Add(this.label156);
            this.tabPage1.Controls.Add(this.label158);
            this.tabPage1.Controls.Add(this.label157);
            this.tabPage1.Controls.Add(this.label137);
            this.tabPage1.Controls.Add(this.label136);
            this.tabPage1.Controls.Add(this.label135);
            this.tabPage1.Controls.Add(this.label134);
            this.tabPage1.Controls.Add(this.label133);
            this.tabPage1.Controls.Add(this.label132);
            this.tabPage1.Controls.Add(this.label131);
            this.tabPage1.Controls.Add(this.label130);
            this.tabPage1.Controls.Add(this.label129);
            this.tabPage1.Controls.Add(this.label128);
            this.tabPage1.Controls.Add(this.label127);
            this.tabPage1.Controls.Add(this.label126);
            this.tabPage1.Controls.Add(this.label38);
            this.tabPage1.Controls.Add(this.label163);
            this.tabPage1.Controls.Add(this.label37);
            this.tabPage1.Controls.Add(this.label36);
            this.tabPage1.Controls.Add(this.circleUC26);
            this.tabPage1.Controls.Add(this.circleUC25);
            this.tabPage1.Controls.Add(this.circleUC24);
            this.tabPage1.Controls.Add(this.circleUC23);
            this.tabPage1.Controls.Add(this.circleUC22);
            this.tabPage1.Controls.Add(this.circleUC21);
            this.tabPage1.Controls.Add(this.circleUC20);
            this.tabPage1.Controls.Add(this.circleUC19);
            this.tabPage1.Controls.Add(this.circleUC18);
            this.tabPage1.Controls.Add(this.circleUC17);
            this.tabPage1.Controls.Add(this.circleUC16);
            this.tabPage1.Controls.Add(this.circleUC15);
            this.tabPage1.Controls.Add(this.circleUC14);
            this.tabPage1.Controls.Add(this.circleUC13);
            this.tabPage1.Controls.Add(this.circleUC12);
            this.tabPage1.Controls.Add(this.circleUC11);
            this.tabPage1.Controls.Add(this.circleUC10);
            this.tabPage1.Controls.Add(this.circleUC9);
            this.tabPage1.Controls.Add(this.circleUC8);
            this.tabPage1.Controls.Add(this.circleUC7);
            this.tabPage1.Controls.Add(this.circleUC6);
            this.tabPage1.Controls.Add(this.circleUC5);
            this.tabPage1.Font = new System.Drawing.Font("微軟正黑體", 16F);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(1902, 867);
            this.tabPage1.TabIndex = 2;
            this.tabPage1.Text = "Status";
            // 
            // circleUC96
            // 
            this.circleUC96.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC96.ExtenderWidth = 3;
            this.circleUC96.Fill = false;
            this.circleUC96.GroupID = null;
            this.circleUC96.Location = new System.Drawing.Point(714, 740);
            this.circleUC96.Margin = new System.Windows.Forms.Padding(49152, 269046, 49152, 269046);
            this.circleUC96.Name = "circleUC96";
            this.circleUC96.Size = new System.Drawing.Size(15, 15);
            this.circleUC96.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC96.TabIndex = 223;
            // 
            // circleUC97
            // 
            this.circleUC97.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC97.ExtenderWidth = 3;
            this.circleUC97.Fill = false;
            this.circleUC97.GroupID = null;
            this.circleUC97.Location = new System.Drawing.Point(714, 715);
            this.circleUC97.Margin = new System.Windows.Forms.Padding(24576, 119576, 24576, 119576);
            this.circleUC97.Name = "circleUC97";
            this.circleUC97.Size = new System.Drawing.Size(15, 15);
            this.circleUC97.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC97.TabIndex = 224;
            // 
            // circleUC98
            // 
            this.circleUC98.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC98.ExtenderWidth = 3;
            this.circleUC98.Fill = false;
            this.circleUC98.GroupID = null;
            this.circleUC98.Location = new System.Drawing.Point(714, 690);
            this.circleUC98.Margin = new System.Windows.Forms.Padding(12288, 53145, 12288, 53145);
            this.circleUC98.Name = "circleUC98";
            this.circleUC98.Size = new System.Drawing.Size(15, 15);
            this.circleUC98.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC98.TabIndex = 222;
            // 
            // circleUC99
            // 
            this.circleUC99.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC99.ExtenderWidth = 3;
            this.circleUC99.Fill = false;
            this.circleUC99.GroupID = null;
            this.circleUC99.Location = new System.Drawing.Point(714, 665);
            this.circleUC99.Margin = new System.Windows.Forms.Padding(6144, 23620, 6144, 23620);
            this.circleUC99.Name = "circleUC99";
            this.circleUC99.Size = new System.Drawing.Size(15, 15);
            this.circleUC99.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC99.TabIndex = 221;
            // 
            // circleUC84
            // 
            this.circleUC84.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC84.ExtenderWidth = 3;
            this.circleUC84.Fill = false;
            this.circleUC84.GroupID = null;
            this.circleUC84.Location = new System.Drawing.Point(714, 640);
            this.circleUC84.Margin = new System.Windows.Forms.Padding(24576, 119576, 24576, 119576);
            this.circleUC84.Name = "circleUC84";
            this.circleUC84.Size = new System.Drawing.Size(15, 15);
            this.circleUC84.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC84.TabIndex = 219;
            // 
            // circleUC85
            // 
            this.circleUC85.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC85.ExtenderWidth = 3;
            this.circleUC85.Fill = false;
            this.circleUC85.GroupID = null;
            this.circleUC85.Location = new System.Drawing.Point(714, 615);
            this.circleUC85.Margin = new System.Windows.Forms.Padding(12288, 53145, 12288, 53145);
            this.circleUC85.Name = "circleUC85";
            this.circleUC85.Size = new System.Drawing.Size(15, 15);
            this.circleUC85.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC85.TabIndex = 220;
            // 
            // circleUC86
            // 
            this.circleUC86.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC86.ExtenderWidth = 3;
            this.circleUC86.Fill = false;
            this.circleUC86.GroupID = null;
            this.circleUC86.Location = new System.Drawing.Point(714, 590);
            this.circleUC86.Margin = new System.Windows.Forms.Padding(6144, 23620, 6144, 23620);
            this.circleUC86.Name = "circleUC86";
            this.circleUC86.Size = new System.Drawing.Size(15, 15);
            this.circleUC86.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC86.TabIndex = 217;
            // 
            // circleUC87
            // 
            this.circleUC87.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC87.ExtenderWidth = 3;
            this.circleUC87.Fill = false;
            this.circleUC87.GroupID = null;
            this.circleUC87.Location = new System.Drawing.Point(714, 565);
            this.circleUC87.Margin = new System.Windows.Forms.Padding(3072, 10498, 3072, 10498);
            this.circleUC87.Name = "circleUC87";
            this.circleUC87.Size = new System.Drawing.Size(15, 15);
            this.circleUC87.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC87.TabIndex = 216;
            // 
            // circleUC88
            // 
            this.circleUC88.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC88.ExtenderWidth = 3;
            this.circleUC88.Fill = false;
            this.circleUC88.GroupID = null;
            this.circleUC88.Location = new System.Drawing.Point(714, 540);
            this.circleUC88.Margin = new System.Windows.Forms.Padding(1536, 4666, 1536, 4666);
            this.circleUC88.Name = "circleUC88";
            this.circleUC88.Size = new System.Drawing.Size(15, 15);
            this.circleUC88.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC88.TabIndex = 215;
            // 
            // circleUC89
            // 
            this.circleUC89.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC89.ExtenderWidth = 3;
            this.circleUC89.Fill = false;
            this.circleUC89.GroupID = null;
            this.circleUC89.Location = new System.Drawing.Point(714, 515);
            this.circleUC89.Margin = new System.Windows.Forms.Padding(768, 2074, 768, 2074);
            this.circleUC89.Name = "circleUC89";
            this.circleUC89.Size = new System.Drawing.Size(15, 15);
            this.circleUC89.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC89.TabIndex = 214;
            // 
            // circleUC90
            // 
            this.circleUC90.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC90.ExtenderWidth = 3;
            this.circleUC90.Fill = false;
            this.circleUC90.GroupID = null;
            this.circleUC90.Location = new System.Drawing.Point(714, 490);
            this.circleUC90.Margin = new System.Windows.Forms.Padding(384, 922, 384, 922);
            this.circleUC90.Name = "circleUC90";
            this.circleUC90.Size = new System.Drawing.Size(15, 15);
            this.circleUC90.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC90.TabIndex = 213;
            // 
            // circleUC91
            // 
            this.circleUC91.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC91.ExtenderWidth = 3;
            this.circleUC91.Fill = false;
            this.circleUC91.GroupID = null;
            this.circleUC91.Location = new System.Drawing.Point(714, 465);
            this.circleUC91.Margin = new System.Windows.Forms.Padding(384, 922, 384, 922);
            this.circleUC91.Name = "circleUC91";
            this.circleUC91.Size = new System.Drawing.Size(15, 15);
            this.circleUC91.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC91.TabIndex = 212;
            // 
            // circleUC92
            // 
            this.circleUC92.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC92.ExtenderWidth = 3;
            this.circleUC92.Fill = false;
            this.circleUC92.GroupID = null;
            this.circleUC92.Location = new System.Drawing.Point(714, 440);
            this.circleUC92.Margin = new System.Windows.Forms.Padding(192, 410, 192, 410);
            this.circleUC92.Name = "circleUC92";
            this.circleUC92.Size = new System.Drawing.Size(15, 15);
            this.circleUC92.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC92.TabIndex = 211;
            // 
            // circleUC93
            // 
            this.circleUC93.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC93.ExtenderWidth = 3;
            this.circleUC93.Fill = false;
            this.circleUC93.GroupID = null;
            this.circleUC93.Location = new System.Drawing.Point(714, 415);
            this.circleUC93.Margin = new System.Windows.Forms.Padding(192, 410, 192, 410);
            this.circleUC93.Name = "circleUC93";
            this.circleUC93.Size = new System.Drawing.Size(15, 15);
            this.circleUC93.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC93.TabIndex = 210;
            // 
            // circleUC94
            // 
            this.circleUC94.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC94.ExtenderWidth = 3;
            this.circleUC94.Fill = false;
            this.circleUC94.GroupID = null;
            this.circleUC94.Location = new System.Drawing.Point(714, 390);
            this.circleUC94.Margin = new System.Windows.Forms.Padding(96, 182, 96, 182);
            this.circleUC94.Name = "circleUC94";
            this.circleUC94.Size = new System.Drawing.Size(15, 15);
            this.circleUC94.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC94.TabIndex = 209;
            // 
            // circleUC95
            // 
            this.circleUC95.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC95.ExtenderWidth = 3;
            this.circleUC95.Fill = false;
            this.circleUC95.GroupID = null;
            this.circleUC95.Location = new System.Drawing.Point(714, 366);
            this.circleUC95.Margin = new System.Windows.Forms.Padding(48, 81, 48, 81);
            this.circleUC95.Name = "circleUC95";
            this.circleUC95.Size = new System.Drawing.Size(15, 15);
            this.circleUC95.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC95.TabIndex = 208;
            // 
            // circleUC66
            // 
            this.circleUC66.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC66.ExtenderWidth = 3;
            this.circleUC66.Fill = false;
            this.circleUC66.GroupID = null;
            this.circleUC66.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.circleUC66.Location = new System.Drawing.Point(983, 925);
            this.circleUC66.Margin = new System.Windows.Forms.Padding(393216, 3064604, 393216, 3064604);
            this.circleUC66.Name = "circleUC66";
            this.circleUC66.Size = new System.Drawing.Size(15, 15);
            this.circleUC66.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC66.TabIndex = 201;
            // 
            // circleUC70
            // 
            this.circleUC70.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC70.ExtenderWidth = 3;
            this.circleUC70.Fill = false;
            this.circleUC70.GroupID = null;
            this.circleUC70.Location = new System.Drawing.Point(714, 340);
            this.circleUC70.Margin = new System.Windows.Forms.Padding(24576, 119576, 24576, 119576);
            this.circleUC70.Name = "circleUC70";
            this.circleUC70.Size = new System.Drawing.Size(15, 15);
            this.circleUC70.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC70.TabIndex = 205;
            // 
            // circleUC71
            // 
            this.circleUC71.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC71.ExtenderWidth = 3;
            this.circleUC71.Fill = false;
            this.circleUC71.GroupID = null;
            this.circleUC71.Location = new System.Drawing.Point(714, 315);
            this.circleUC71.Margin = new System.Windows.Forms.Padding(12288, 53145, 12288, 53145);
            this.circleUC71.Name = "circleUC71";
            this.circleUC71.Size = new System.Drawing.Size(15, 15);
            this.circleUC71.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC71.TabIndex = 206;
            // 
            // circleUC72
            // 
            this.circleUC72.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC72.ExtenderWidth = 3;
            this.circleUC72.Fill = false;
            this.circleUC72.GroupID = null;
            this.circleUC72.Location = new System.Drawing.Point(714, 290);
            this.circleUC72.Margin = new System.Windows.Forms.Padding(6144, 23620, 6144, 23620);
            this.circleUC72.Name = "circleUC72";
            this.circleUC72.Size = new System.Drawing.Size(15, 15);
            this.circleUC72.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC72.TabIndex = 207;
            // 
            // circleUC73
            // 
            this.circleUC73.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC73.ExtenderWidth = 3;
            this.circleUC73.Fill = false;
            this.circleUC73.GroupID = null;
            this.circleUC73.Location = new System.Drawing.Point(714, 265);
            this.circleUC73.Margin = new System.Windows.Forms.Padding(3072, 10498, 3072, 10498);
            this.circleUC73.Name = "circleUC73";
            this.circleUC73.Size = new System.Drawing.Size(15, 15);
            this.circleUC73.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC73.TabIndex = 200;
            // 
            // circleUC74
            // 
            this.circleUC74.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC74.ExtenderWidth = 3;
            this.circleUC74.Fill = false;
            this.circleUC74.GroupID = null;
            this.circleUC74.Location = new System.Drawing.Point(714, 240);
            this.circleUC74.Margin = new System.Windows.Forms.Padding(1536, 4666, 1536, 4666);
            this.circleUC74.Name = "circleUC74";
            this.circleUC74.Size = new System.Drawing.Size(15, 15);
            this.circleUC74.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC74.TabIndex = 199;
            // 
            // circleUC75
            // 
            this.circleUC75.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC75.ExtenderWidth = 3;
            this.circleUC75.Fill = false;
            this.circleUC75.GroupID = null;
            this.circleUC75.Location = new System.Drawing.Point(714, 215);
            this.circleUC75.Margin = new System.Windows.Forms.Padding(768, 2074, 768, 2074);
            this.circleUC75.Name = "circleUC75";
            this.circleUC75.Size = new System.Drawing.Size(15, 15);
            this.circleUC75.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC75.TabIndex = 198;
            // 
            // circleUC76
            // 
            this.circleUC76.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC76.ExtenderWidth = 3;
            this.circleUC76.Fill = false;
            this.circleUC76.GroupID = null;
            this.circleUC76.Location = new System.Drawing.Point(714, 190);
            this.circleUC76.Margin = new System.Windows.Forms.Padding(384, 922, 384, 922);
            this.circleUC76.Name = "circleUC76";
            this.circleUC76.Size = new System.Drawing.Size(15, 15);
            this.circleUC76.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC76.TabIndex = 197;
            // 
            // circleUC77
            // 
            this.circleUC77.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC77.ExtenderWidth = 3;
            this.circleUC77.Fill = false;
            this.circleUC77.GroupID = null;
            this.circleUC77.Location = new System.Drawing.Point(714, 165);
            this.circleUC77.Margin = new System.Windows.Forms.Padding(192, 410, 192, 410);
            this.circleUC77.Name = "circleUC77";
            this.circleUC77.Size = new System.Drawing.Size(15, 15);
            this.circleUC77.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC77.TabIndex = 196;
            // 
            // circleUC78
            // 
            this.circleUC78.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC78.ExtenderWidth = 3;
            this.circleUC78.Fill = false;
            this.circleUC78.GroupID = null;
            this.circleUC78.Location = new System.Drawing.Point(714, 140);
            this.circleUC78.Margin = new System.Windows.Forms.Padding(192, 410, 192, 410);
            this.circleUC78.Name = "circleUC78";
            this.circleUC78.Size = new System.Drawing.Size(15, 15);
            this.circleUC78.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC78.TabIndex = 195;
            // 
            // circleUC79
            // 
            this.circleUC79.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC79.ExtenderWidth = 3;
            this.circleUC79.Fill = false;
            this.circleUC79.GroupID = null;
            this.circleUC79.Location = new System.Drawing.Point(714, 115);
            this.circleUC79.Margin = new System.Windows.Forms.Padding(96, 182, 96, 182);
            this.circleUC79.Name = "circleUC79";
            this.circleUC79.Size = new System.Drawing.Size(15, 15);
            this.circleUC79.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC79.TabIndex = 194;
            // 
            // circleUC80
            // 
            this.circleUC80.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC80.ExtenderWidth = 3;
            this.circleUC80.Fill = false;
            this.circleUC80.GroupID = null;
            this.circleUC80.Location = new System.Drawing.Point(714, 90);
            this.circleUC80.Margin = new System.Windows.Forms.Padding(96, 182, 96, 182);
            this.circleUC80.Name = "circleUC80";
            this.circleUC80.Size = new System.Drawing.Size(15, 15);
            this.circleUC80.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC80.TabIndex = 193;
            // 
            // circleUC81
            // 
            this.circleUC81.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC81.ExtenderWidth = 3;
            this.circleUC81.Fill = false;
            this.circleUC81.GroupID = null;
            this.circleUC81.Location = new System.Drawing.Point(714, 65);
            this.circleUC81.Margin = new System.Windows.Forms.Padding(48, 81, 48, 81);
            this.circleUC81.Name = "circleUC81";
            this.circleUC81.Size = new System.Drawing.Size(15, 15);
            this.circleUC81.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC81.TabIndex = 192;
            // 
            // circleUC123
            // 
            this.circleUC123.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC123.ExtenderWidth = 3;
            this.circleUC123.Fill = false;
            this.circleUC123.GroupID = null;
            this.circleUC123.Location = new System.Drawing.Point(1233, 565);
            this.circleUC123.Margin = new System.Windows.Forms.Padding(100663300, 0, 100663300, 0);
            this.circleUC123.Name = "circleUC123";
            this.circleUC123.Size = new System.Drawing.Size(15, 15);
            this.circleUC123.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC123.TabIndex = 191;
            // 
            // circleUC122
            // 
            this.circleUC122.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC122.ExtenderWidth = 3;
            this.circleUC122.Fill = false;
            this.circleUC122.GroupID = null;
            this.circleUC122.Location = new System.Drawing.Point(1233, 540);
            this.circleUC122.Margin = new System.Windows.Forms.Padding(50331650, 894647613, 50331650, 894647613);
            this.circleUC122.Name = "circleUC122";
            this.circleUC122.Size = new System.Drawing.Size(15, 15);
            this.circleUC122.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC122.TabIndex = 191;
            // 
            // circleUC121
            // 
            this.circleUC121.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC121.ExtenderWidth = 3;
            this.circleUC121.Fill = false;
            this.circleUC121.GroupID = null;
            this.circleUC121.Location = new System.Drawing.Point(1233, 515);
            this.circleUC121.Margin = new System.Windows.Forms.Padding(25165825, 397621149, 25165825, 397621149);
            this.circleUC121.Name = "circleUC121";
            this.circleUC121.Size = new System.Drawing.Size(15, 15);
            this.circleUC121.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC121.TabIndex = 191;
            // 
            // circleUC120
            // 
            this.circleUC120.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC120.ExtenderWidth = 3;
            this.circleUC120.Fill = false;
            this.circleUC120.GroupID = null;
            this.circleUC120.Location = new System.Drawing.Point(1233, 490);
            this.circleUC120.Margin = new System.Windows.Forms.Padding(12582912, 176720513, 12582912, 176720513);
            this.circleUC120.Name = "circleUC120";
            this.circleUC120.Size = new System.Drawing.Size(15, 15);
            this.circleUC120.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC120.TabIndex = 191;
            // 
            // circleUC119
            // 
            this.circleUC119.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC119.ExtenderWidth = 3;
            this.circleUC119.Fill = false;
            this.circleUC119.GroupID = null;
            this.circleUC119.Location = new System.Drawing.Point(1233, 465);
            this.circleUC119.Margin = new System.Windows.Forms.Padding(6291456, 78542451, 6291456, 78542451);
            this.circleUC119.Name = "circleUC119";
            this.circleUC119.Size = new System.Drawing.Size(15, 15);
            this.circleUC119.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC119.TabIndex = 191;
            // 
            // circleUC118
            // 
            this.circleUC118.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC118.ExtenderWidth = 3;
            this.circleUC118.Fill = false;
            this.circleUC118.GroupID = null;
            this.circleUC118.Location = new System.Drawing.Point(1233, 440);
            this.circleUC118.Margin = new System.Windows.Forms.Padding(3145728, 34907757, 3145728, 34907757);
            this.circleUC118.Name = "circleUC118";
            this.circleUC118.Size = new System.Drawing.Size(15, 15);
            this.circleUC118.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC118.TabIndex = 191;
            // 
            // circleUC117
            // 
            this.circleUC117.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC117.ExtenderWidth = 3;
            this.circleUC117.Fill = false;
            this.circleUC117.GroupID = null;
            this.circleUC117.Location = new System.Drawing.Point(1233, 415);
            this.circleUC117.Margin = new System.Windows.Forms.Padding(1572864, 15514558, 1572864, 15514558);
            this.circleUC117.Name = "circleUC117";
            this.circleUC117.Size = new System.Drawing.Size(15, 15);
            this.circleUC117.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC117.TabIndex = 191;
            // 
            // circleUC116
            // 
            this.circleUC116.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC116.ExtenderWidth = 3;
            this.circleUC116.Fill = false;
            this.circleUC116.GroupID = null;
            this.circleUC116.Location = new System.Drawing.Point(1233, 390);
            this.circleUC116.Margin = new System.Windows.Forms.Padding(786432, 6895359, 786432, 6895359);
            this.circleUC116.Name = "circleUC116";
            this.circleUC116.Size = new System.Drawing.Size(15, 15);
            this.circleUC116.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC116.TabIndex = 191;
            // 
            // circleUC115
            // 
            this.circleUC115.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC115.ExtenderWidth = 3;
            this.circleUC115.Fill = false;
            this.circleUC115.GroupID = null;
            this.circleUC115.Location = new System.Drawing.Point(1233, 365);
            this.circleUC115.Margin = new System.Windows.Forms.Padding(393216, 3064604, 393216, 3064604);
            this.circleUC115.Name = "circleUC115";
            this.circleUC115.Size = new System.Drawing.Size(15, 15);
            this.circleUC115.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC115.TabIndex = 191;
            // 
            // circleUC114
            // 
            this.circleUC114.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC114.ExtenderWidth = 3;
            this.circleUC114.Fill = false;
            this.circleUC114.GroupID = null;
            this.circleUC114.Location = new System.Drawing.Point(1233, 340);
            this.circleUC114.Margin = new System.Windows.Forms.Padding(196608, 1362046, 196608, 1362046);
            this.circleUC114.Name = "circleUC114";
            this.circleUC114.Size = new System.Drawing.Size(15, 15);
            this.circleUC114.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC114.TabIndex = 191;
            // 
            // circleUC113
            // 
            this.circleUC113.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC113.ExtenderWidth = 3;
            this.circleUC113.Fill = false;
            this.circleUC113.GroupID = null;
            this.circleUC113.Location = new System.Drawing.Point(1233, 315);
            this.circleUC113.Margin = new System.Windows.Forms.Padding(98304, 605354, 98304, 605354);
            this.circleUC113.Name = "circleUC113";
            this.circleUC113.Size = new System.Drawing.Size(15, 15);
            this.circleUC113.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC113.TabIndex = 191;
            // 
            // circleUC112
            // 
            this.circleUC112.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC112.ExtenderWidth = 3;
            this.circleUC112.Fill = false;
            this.circleUC112.GroupID = null;
            this.circleUC112.Location = new System.Drawing.Point(1233, 290);
            this.circleUC112.Margin = new System.Windows.Forms.Padding(49152, 269046, 49152, 269046);
            this.circleUC112.Name = "circleUC112";
            this.circleUC112.Size = new System.Drawing.Size(15, 15);
            this.circleUC112.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC112.TabIndex = 191;
            // 
            // circleUC111
            // 
            this.circleUC111.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC111.ExtenderWidth = 3;
            this.circleUC111.Fill = false;
            this.circleUC111.GroupID = null;
            this.circleUC111.Location = new System.Drawing.Point(1233, 265);
            this.circleUC111.Margin = new System.Windows.Forms.Padding(24576, 119576, 24576, 119576);
            this.circleUC111.Name = "circleUC111";
            this.circleUC111.Size = new System.Drawing.Size(15, 15);
            this.circleUC111.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC111.TabIndex = 191;
            // 
            // circleUC110
            // 
            this.circleUC110.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC110.ExtenderWidth = 3;
            this.circleUC110.Fill = false;
            this.circleUC110.GroupID = null;
            this.circleUC110.Location = new System.Drawing.Point(1233, 240);
            this.circleUC110.Margin = new System.Windows.Forms.Padding(12288, 53145, 12288, 53145);
            this.circleUC110.Name = "circleUC110";
            this.circleUC110.Size = new System.Drawing.Size(15, 15);
            this.circleUC110.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC110.TabIndex = 191;
            // 
            // circleUC109
            // 
            this.circleUC109.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC109.ExtenderWidth = 3;
            this.circleUC109.Fill = false;
            this.circleUC109.GroupID = null;
            this.circleUC109.Location = new System.Drawing.Point(1233, 215);
            this.circleUC109.Margin = new System.Windows.Forms.Padding(6144, 23620, 6144, 23620);
            this.circleUC109.Name = "circleUC109";
            this.circleUC109.Size = new System.Drawing.Size(15, 15);
            this.circleUC109.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC109.TabIndex = 191;
            // 
            // circleUC108
            // 
            this.circleUC108.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC108.ExtenderWidth = 3;
            this.circleUC108.Fill = false;
            this.circleUC108.GroupID = null;
            this.circleUC108.Location = new System.Drawing.Point(1233, 190);
            this.circleUC108.Margin = new System.Windows.Forms.Padding(3072, 10498, 3072, 10498);
            this.circleUC108.Name = "circleUC108";
            this.circleUC108.Size = new System.Drawing.Size(15, 15);
            this.circleUC108.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC108.TabIndex = 191;
            // 
            // circleUC107
            // 
            this.circleUC107.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC107.ExtenderWidth = 3;
            this.circleUC107.Fill = false;
            this.circleUC107.GroupID = null;
            this.circleUC107.Location = new System.Drawing.Point(1233, 165);
            this.circleUC107.Margin = new System.Windows.Forms.Padding(1536, 4666, 1536, 4666);
            this.circleUC107.Name = "circleUC107";
            this.circleUC107.Size = new System.Drawing.Size(15, 15);
            this.circleUC107.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC107.TabIndex = 191;
            // 
            // circleUC106
            // 
            this.circleUC106.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC106.ExtenderWidth = 3;
            this.circleUC106.Fill = false;
            this.circleUC106.GroupID = null;
            this.circleUC106.Location = new System.Drawing.Point(1233, 140);
            this.circleUC106.Margin = new System.Windows.Forms.Padding(768, 2074, 768, 2074);
            this.circleUC106.Name = "circleUC106";
            this.circleUC106.Size = new System.Drawing.Size(15, 15);
            this.circleUC106.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC106.TabIndex = 191;
            // 
            // circleUC105
            // 
            this.circleUC105.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC105.ExtenderWidth = 3;
            this.circleUC105.Fill = false;
            this.circleUC105.GroupID = null;
            this.circleUC105.Location = new System.Drawing.Point(1233, 115);
            this.circleUC105.Margin = new System.Windows.Forms.Padding(384, 922, 384, 922);
            this.circleUC105.Name = "circleUC105";
            this.circleUC105.Size = new System.Drawing.Size(15, 15);
            this.circleUC105.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC105.TabIndex = 191;
            // 
            // circleUC104
            // 
            this.circleUC104.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC104.ExtenderWidth = 3;
            this.circleUC104.Fill = false;
            this.circleUC104.GroupID = null;
            this.circleUC104.Location = new System.Drawing.Point(1233, 90);
            this.circleUC104.Margin = new System.Windows.Forms.Padding(192, 410, 192, 410);
            this.circleUC104.Name = "circleUC104";
            this.circleUC104.Size = new System.Drawing.Size(15, 15);
            this.circleUC104.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC104.TabIndex = 191;
            // 
            // circleUC103
            // 
            this.circleUC103.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC103.ExtenderWidth = 3;
            this.circleUC103.Fill = false;
            this.circleUC103.GroupID = null;
            this.circleUC103.Location = new System.Drawing.Point(1233, 65);
            this.circleUC103.Margin = new System.Windows.Forms.Padding(96, 182, 96, 182);
            this.circleUC103.Name = "circleUC103";
            this.circleUC103.Size = new System.Drawing.Size(15, 15);
            this.circleUC103.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC103.TabIndex = 191;
            // 
            // circleUC102
            // 
            this.circleUC102.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC102.ExtenderWidth = 3;
            this.circleUC102.Fill = false;
            this.circleUC102.GroupID = null;
            this.circleUC102.Location = new System.Drawing.Point(1233, 40);
            this.circleUC102.Margin = new System.Windows.Forms.Padding(48, 81, 48, 81);
            this.circleUC102.Name = "circleUC102";
            this.circleUC102.Size = new System.Drawing.Size(15, 15);
            this.circleUC102.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC102.TabIndex = 191;
            // 
            // circleUC82
            // 
            this.circleUC82.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC82.ExtenderWidth = 3;
            this.circleUC82.Fill = false;
            this.circleUC82.GroupID = null;
            this.circleUC82.Location = new System.Drawing.Point(714, 40);
            this.circleUC82.Margin = new System.Windows.Forms.Padding(24, 36, 24, 36);
            this.circleUC82.Name = "circleUC82";
            this.circleUC82.Size = new System.Drawing.Size(15, 15);
            this.circleUC82.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC82.TabIndex = 191;
            // 
            // circleUC65
            // 
            this.circleUC65.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC65.ExtenderWidth = 3;
            this.circleUC65.Fill = false;
            this.circleUC65.GroupID = null;
            this.circleUC65.Location = new System.Drawing.Point(360, 740);
            this.circleUC65.Margin = new System.Windows.Forms.Padding(50331650, 894647613, 50331650, 894647613);
            this.circleUC65.Name = "circleUC65";
            this.circleUC65.Size = new System.Drawing.Size(15, 15);
            this.circleUC65.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC65.TabIndex = 190;
            // 
            // circleUC55
            // 
            this.circleUC55.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC55.ExtenderWidth = 3;
            this.circleUC55.Fill = false;
            this.circleUC55.GroupID = null;
            this.circleUC55.Location = new System.Drawing.Point(360, 715);
            this.circleUC55.Margin = new System.Windows.Forms.Padding(25165825, 397621149, 25165825, 397621149);
            this.circleUC55.Name = "circleUC55";
            this.circleUC55.Size = new System.Drawing.Size(15, 15);
            this.circleUC55.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC55.TabIndex = 188;
            // 
            // circleUC56
            // 
            this.circleUC56.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC56.ExtenderWidth = 3;
            this.circleUC56.Fill = false;
            this.circleUC56.GroupID = null;
            this.circleUC56.Location = new System.Drawing.Point(360, 690);
            this.circleUC56.Margin = new System.Windows.Forms.Padding(25165825, 397621149, 25165825, 397621149);
            this.circleUC56.Name = "circleUC56";
            this.circleUC56.Size = new System.Drawing.Size(15, 15);
            this.circleUC56.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC56.TabIndex = 187;
            // 
            // circleUC57
            // 
            this.circleUC57.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC57.ExtenderWidth = 3;
            this.circleUC57.Fill = false;
            this.circleUC57.GroupID = null;
            this.circleUC57.Location = new System.Drawing.Point(360, 665);
            this.circleUC57.Margin = new System.Windows.Forms.Padding(25165825, 397621149, 25165825, 397621149);
            this.circleUC57.Name = "circleUC57";
            this.circleUC57.Size = new System.Drawing.Size(15, 15);
            this.circleUC57.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC57.TabIndex = 186;
            // 
            // circleUC58
            // 
            this.circleUC58.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC58.ExtenderWidth = 3;
            this.circleUC58.Fill = false;
            this.circleUC58.GroupID = null;
            this.circleUC58.Location = new System.Drawing.Point(360, 640);
            this.circleUC58.Margin = new System.Windows.Forms.Padding(25165825, 397621149, 25165825, 397621149);
            this.circleUC58.Name = "circleUC58";
            this.circleUC58.Size = new System.Drawing.Size(15, 15);
            this.circleUC58.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC58.TabIndex = 185;
            // 
            // circleUC59
            // 
            this.circleUC59.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC59.ExtenderWidth = 3;
            this.circleUC59.Fill = false;
            this.circleUC59.GroupID = null;
            this.circleUC59.Location = new System.Drawing.Point(360, 615);
            this.circleUC59.Margin = new System.Windows.Forms.Padding(25165825, 397621149, 25165825, 397621149);
            this.circleUC59.Name = "circleUC59";
            this.circleUC59.Size = new System.Drawing.Size(15, 15);
            this.circleUC59.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC59.TabIndex = 184;
            // 
            // circleUC60
            // 
            this.circleUC60.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC60.ExtenderWidth = 3;
            this.circleUC60.Fill = false;
            this.circleUC60.GroupID = null;
            this.circleUC60.Location = new System.Drawing.Point(360, 590);
            this.circleUC60.Margin = new System.Windows.Forms.Padding(12582912, 176720513, 12582912, 176720513);
            this.circleUC60.Name = "circleUC60";
            this.circleUC60.Size = new System.Drawing.Size(15, 15);
            this.circleUC60.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC60.TabIndex = 183;
            // 
            // circleUC61
            // 
            this.circleUC61.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC61.ExtenderWidth = 3;
            this.circleUC61.Fill = false;
            this.circleUC61.GroupID = null;
            this.circleUC61.Location = new System.Drawing.Point(360, 565);
            this.circleUC61.Margin = new System.Windows.Forms.Padding(6291456, 78542451, 6291456, 78542451);
            this.circleUC61.Name = "circleUC61";
            this.circleUC61.Size = new System.Drawing.Size(15, 15);
            this.circleUC61.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC61.TabIndex = 180;
            // 
            // circleUC62
            // 
            this.circleUC62.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC62.ExtenderWidth = 3;
            this.circleUC62.Fill = false;
            this.circleUC62.GroupID = null;
            this.circleUC62.Location = new System.Drawing.Point(360, 540);
            this.circleUC62.Margin = new System.Windows.Forms.Padding(3145728, 34907757, 3145728, 34907757);
            this.circleUC62.Name = "circleUC62";
            this.circleUC62.Size = new System.Drawing.Size(15, 15);
            this.circleUC62.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC62.TabIndex = 181;
            // 
            // circleUC63
            // 
            this.circleUC63.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC63.ExtenderWidth = 3;
            this.circleUC63.Fill = false;
            this.circleUC63.GroupID = null;
            this.circleUC63.Location = new System.Drawing.Point(360, 515);
            this.circleUC63.Margin = new System.Windows.Forms.Padding(1572864, 15514558, 1572864, 15514558);
            this.circleUC63.Name = "circleUC63";
            this.circleUC63.Size = new System.Drawing.Size(15, 15);
            this.circleUC63.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC63.TabIndex = 182;
            // 
            // circleUC64
            // 
            this.circleUC64.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC64.ExtenderWidth = 3;
            this.circleUC64.Fill = false;
            this.circleUC64.GroupID = null;
            this.circleUC64.Location = new System.Drawing.Point(360, 490);
            this.circleUC64.Margin = new System.Windows.Forms.Padding(786432, 6895359, 786432, 6895359);
            this.circleUC64.Name = "circleUC64";
            this.circleUC64.Size = new System.Drawing.Size(15, 15);
            this.circleUC64.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC64.TabIndex = 179;
            // 
            // circleUC101
            // 
            this.circleUC101.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC101.ExtenderWidth = 3;
            this.circleUC101.Fill = false;
            this.circleUC101.GroupID = null;
            this.circleUC101.Location = new System.Drawing.Point(714, 790);
            this.circleUC101.Margin = new System.Windows.Forms.Padding(12582912, 176720513, 12582912, 176720513);
            this.circleUC101.Name = "circleUC101";
            this.circleUC101.Size = new System.Drawing.Size(15, 15);
            this.circleUC101.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC101.TabIndex = 178;
            // 
            // circleUC100
            // 
            this.circleUC100.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC100.ExtenderWidth = 3;
            this.circleUC100.Fill = false;
            this.circleUC100.GroupID = null;
            this.circleUC100.Location = new System.Drawing.Point(714, 815);
            this.circleUC100.Margin = new System.Windows.Forms.Padding(6291456, 78542451, 6291456, 78542451);
            this.circleUC100.Name = "circleUC100";
            this.circleUC100.Size = new System.Drawing.Size(15, 15);
            this.circleUC100.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC100.TabIndex = 178;
            // 
            // circleUC69
            // 
            this.circleUC69.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC69.ExtenderWidth = 3;
            this.circleUC69.Fill = false;
            this.circleUC69.GroupID = null;
            this.circleUC69.Location = new System.Drawing.Point(714, 765);
            this.circleUC69.Margin = new System.Windows.Forms.Padding(3145728, 34907757, 3145728, 34907757);
            this.circleUC69.Name = "circleUC69";
            this.circleUC69.Size = new System.Drawing.Size(15, 15);
            this.circleUC69.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC69.TabIndex = 178;
            // 
            // circleUC83
            // 
            this.circleUC83.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC83.ExtenderWidth = 3;
            this.circleUC83.Fill = false;
            this.circleUC83.GroupID = null;
            this.circleUC83.Location = new System.Drawing.Point(360, 815);
            this.circleUC83.Margin = new System.Windows.Forms.Padding(3145728, 34907757, 3145728, 34907757);
            this.circleUC83.Name = "circleUC83";
            this.circleUC83.Size = new System.Drawing.Size(15, 15);
            this.circleUC83.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC83.TabIndex = 178;
            // 
            // circleUC68
            // 
            this.circleUC68.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC68.ExtenderWidth = 3;
            this.circleUC68.Fill = false;
            this.circleUC68.GroupID = null;
            this.circleUC68.Location = new System.Drawing.Point(360, 790);
            this.circleUC68.Margin = new System.Windows.Forms.Padding(1572864, 15514558, 1572864, 15514558);
            this.circleUC68.Name = "circleUC68";
            this.circleUC68.Size = new System.Drawing.Size(15, 15);
            this.circleUC68.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC68.TabIndex = 178;
            // 
            // circleUC44
            // 
            this.circleUC44.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC44.ExtenderWidth = 3;
            this.circleUC44.Fill = false;
            this.circleUC44.GroupID = null;
            this.circleUC44.Location = new System.Drawing.Point(360, 765);
            this.circleUC44.Margin = new System.Windows.Forms.Padding(786432, 6895359, 786432, 6895359);
            this.circleUC44.Name = "circleUC44";
            this.circleUC44.Size = new System.Drawing.Size(15, 15);
            this.circleUC44.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC44.TabIndex = 178;
            // 
            // circleUC45
            // 
            this.circleUC45.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC45.ExtenderWidth = 3;
            this.circleUC45.Fill = false;
            this.circleUC45.GroupID = null;
            this.circleUC45.Location = new System.Drawing.Point(360, 465);
            this.circleUC45.Margin = new System.Windows.Forms.Padding(393216, 3064604, 393216, 3064604);
            this.circleUC45.Name = "circleUC45";
            this.circleUC45.Size = new System.Drawing.Size(15, 15);
            this.circleUC45.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC45.TabIndex = 177;
            // 
            // circleUC46
            // 
            this.circleUC46.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC46.ExtenderWidth = 3;
            this.circleUC46.Fill = false;
            this.circleUC46.GroupID = null;
            this.circleUC46.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.circleUC46.Location = new System.Drawing.Point(360, 440);
            this.circleUC46.Margin = new System.Windows.Forms.Padding(196608, 1362046, 196608, 1362046);
            this.circleUC46.Name = "circleUC46";
            this.circleUC46.Size = new System.Drawing.Size(15, 15);
            this.circleUC46.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC46.TabIndex = 170;
            // 
            // circleUC47
            // 
            this.circleUC47.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC47.ExtenderWidth = 3;
            this.circleUC47.Fill = false;
            this.circleUC47.GroupID = null;
            this.circleUC47.Location = new System.Drawing.Point(360, 415);
            this.circleUC47.Margin = new System.Windows.Forms.Padding(98304, 605354, 98304, 605354);
            this.circleUC47.Name = "circleUC47";
            this.circleUC47.Size = new System.Drawing.Size(15, 15);
            this.circleUC47.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC47.TabIndex = 171;
            // 
            // circleUC48
            // 
            this.circleUC48.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC48.ExtenderWidth = 3;
            this.circleUC48.Fill = false;
            this.circleUC48.GroupID = null;
            this.circleUC48.Location = new System.Drawing.Point(360, 390);
            this.circleUC48.Margin = new System.Windows.Forms.Padding(49152, 269046, 49152, 269046);
            this.circleUC48.Name = "circleUC48";
            this.circleUC48.Size = new System.Drawing.Size(15, 15);
            this.circleUC48.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC48.TabIndex = 172;
            // 
            // circleUC49
            // 
            this.circleUC49.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC49.ExtenderWidth = 3;
            this.circleUC49.Fill = false;
            this.circleUC49.GroupID = null;
            this.circleUC49.Location = new System.Drawing.Point(360, 365);
            this.circleUC49.Margin = new System.Windows.Forms.Padding(24576, 119576, 24576, 119576);
            this.circleUC49.Name = "circleUC49";
            this.circleUC49.Size = new System.Drawing.Size(15, 15);
            this.circleUC49.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC49.TabIndex = 173;
            // 
            // circleUC50
            // 
            this.circleUC50.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC50.ExtenderWidth = 3;
            this.circleUC50.Fill = false;
            this.circleUC50.GroupID = null;
            this.circleUC50.Location = new System.Drawing.Point(360, 340);
            this.circleUC50.Margin = new System.Windows.Forms.Padding(12288, 53145, 12288, 53145);
            this.circleUC50.Name = "circleUC50";
            this.circleUC50.Size = new System.Drawing.Size(15, 15);
            this.circleUC50.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC50.TabIndex = 174;
            // 
            // circleUC51
            // 
            this.circleUC51.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC51.ExtenderWidth = 3;
            this.circleUC51.Fill = false;
            this.circleUC51.GroupID = null;
            this.circleUC51.Location = new System.Drawing.Point(360, 315);
            this.circleUC51.Margin = new System.Windows.Forms.Padding(6144, 23620, 6144, 23620);
            this.circleUC51.Name = "circleUC51";
            this.circleUC51.Size = new System.Drawing.Size(15, 15);
            this.circleUC51.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC51.TabIndex = 175;
            // 
            // circleUC52
            // 
            this.circleUC52.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC52.ExtenderWidth = 3;
            this.circleUC52.Fill = false;
            this.circleUC52.GroupID = null;
            this.circleUC52.Location = new System.Drawing.Point(360, 290);
            this.circleUC52.Margin = new System.Windows.Forms.Padding(3072, 10498, 3072, 10498);
            this.circleUC52.Name = "circleUC52";
            this.circleUC52.Size = new System.Drawing.Size(15, 15);
            this.circleUC52.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC52.TabIndex = 176;
            // 
            // circleUC53
            // 
            this.circleUC53.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC53.ExtenderWidth = 3;
            this.circleUC53.Fill = false;
            this.circleUC53.GroupID = null;
            this.circleUC53.Location = new System.Drawing.Point(360, 265);
            this.circleUC53.Margin = new System.Windows.Forms.Padding(1536, 4666, 1536, 4666);
            this.circleUC53.Name = "circleUC53";
            this.circleUC53.Size = new System.Drawing.Size(15, 15);
            this.circleUC53.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC53.TabIndex = 169;
            // 
            // circleUC38
            // 
            this.circleUC38.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC38.ExtenderWidth = 3;
            this.circleUC38.Fill = false;
            this.circleUC38.GroupID = null;
            this.circleUC38.Location = new System.Drawing.Point(360, 240);
            this.circleUC38.Margin = new System.Windows.Forms.Padding(768, 2074, 768, 2074);
            this.circleUC38.Name = "circleUC38";
            this.circleUC38.Size = new System.Drawing.Size(15, 15);
            this.circleUC38.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC38.TabIndex = 168;
            // 
            // circleUC39
            // 
            this.circleUC39.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC39.ExtenderWidth = 3;
            this.circleUC39.Fill = false;
            this.circleUC39.GroupID = null;
            this.circleUC39.Location = new System.Drawing.Point(360, 215);
            this.circleUC39.Margin = new System.Windows.Forms.Padding(384, 922, 384, 922);
            this.circleUC39.Name = "circleUC39";
            this.circleUC39.Size = new System.Drawing.Size(15, 15);
            this.circleUC39.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC39.TabIndex = 167;
            // 
            // circleUC40
            // 
            this.circleUC40.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC40.ExtenderWidth = 3;
            this.circleUC40.Fill = false;
            this.circleUC40.GroupID = null;
            this.circleUC40.Location = new System.Drawing.Point(360, 190);
            this.circleUC40.Margin = new System.Windows.Forms.Padding(192, 410, 192, 410);
            this.circleUC40.Name = "circleUC40";
            this.circleUC40.Size = new System.Drawing.Size(15, 15);
            this.circleUC40.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC40.TabIndex = 166;
            // 
            // circleUC41
            // 
            this.circleUC41.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC41.ExtenderWidth = 3;
            this.circleUC41.Fill = false;
            this.circleUC41.GroupID = null;
            this.circleUC41.Location = new System.Drawing.Point(360, 165);
            this.circleUC41.Margin = new System.Windows.Forms.Padding(96, 182, 96, 182);
            this.circleUC41.Name = "circleUC41";
            this.circleUC41.Size = new System.Drawing.Size(15, 15);
            this.circleUC41.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC41.TabIndex = 165;
            // 
            // circleUC42
            // 
            this.circleUC42.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC42.ExtenderWidth = 3;
            this.circleUC42.Fill = false;
            this.circleUC42.GroupID = null;
            this.circleUC42.Location = new System.Drawing.Point(360, 140);
            this.circleUC42.Margin = new System.Windows.Forms.Padding(96, 182, 96, 182);
            this.circleUC42.Name = "circleUC42";
            this.circleUC42.Size = new System.Drawing.Size(15, 15);
            this.circleUC42.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC42.TabIndex = 164;
            // 
            // circleUC43
            // 
            this.circleUC43.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC43.ExtenderWidth = 3;
            this.circleUC43.Fill = false;
            this.circleUC43.GroupID = null;
            this.circleUC43.Location = new System.Drawing.Point(360, 115);
            this.circleUC43.Margin = new System.Windows.Forms.Padding(48, 81, 48, 81);
            this.circleUC43.Name = "circleUC43";
            this.circleUC43.Size = new System.Drawing.Size(15, 15);
            this.circleUC43.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC43.TabIndex = 163;
            // 
            // circleUC37
            // 
            this.circleUC37.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC37.ExtenderWidth = 3;
            this.circleUC37.Fill = false;
            this.circleUC37.GroupID = null;
            this.circleUC37.Location = new System.Drawing.Point(360, 90);
            this.circleUC37.Margin = new System.Windows.Forms.Padding(48, 81, 48, 81);
            this.circleUC37.Name = "circleUC37";
            this.circleUC37.Size = new System.Drawing.Size(15, 15);
            this.circleUC37.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC37.TabIndex = 162;
            // 
            // circleUC36
            // 
            this.circleUC36.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC36.ExtenderWidth = 3;
            this.circleUC36.Fill = false;
            this.circleUC36.GroupID = null;
            this.circleUC36.Location = new System.Drawing.Point(360, 65);
            this.circleUC36.Margin = new System.Windows.Forms.Padding(24, 36, 24, 36);
            this.circleUC36.Name = "circleUC36";
            this.circleUC36.Size = new System.Drawing.Size(15, 15);
            this.circleUC36.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC36.TabIndex = 161;
            // 
            // circleUC35
            // 
            this.circleUC35.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC35.ExtenderWidth = 3;
            this.circleUC35.Fill = false;
            this.circleUC35.GroupID = null;
            this.circleUC35.Location = new System.Drawing.Point(360, 40);
            this.circleUC35.Margin = new System.Windows.Forms.Padding(12, 16, 12, 16);
            this.circleUC35.Name = "circleUC35";
            this.circleUC35.Size = new System.Drawing.Size(15, 15);
            this.circleUC35.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC35.TabIndex = 160;
            // 
            // circleUC67
            // 
            this.circleUC67.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC67.ExtenderWidth = 3;
            this.circleUC67.Fill = false;
            this.circleUC67.GroupID = null;
            this.circleUC67.Location = new System.Drawing.Point(12, 815);
            this.circleUC67.Margin = new System.Windows.Forms.Padding(50331650, 894647613, 50331650, 894647613);
            this.circleUC67.Name = "circleUC67";
            this.circleUC67.Size = new System.Drawing.Size(15, 15);
            this.circleUC67.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC67.TabIndex = 159;
            // 
            // circleUC54
            // 
            this.circleUC54.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC54.ExtenderWidth = 3;
            this.circleUC54.Fill = false;
            this.circleUC54.GroupID = null;
            this.circleUC54.Location = new System.Drawing.Point(12, 790);
            this.circleUC54.Margin = new System.Windows.Forms.Padding(25165825, 397621149, 25165825, 397621149);
            this.circleUC54.Name = "circleUC54";
            this.circleUC54.Size = new System.Drawing.Size(15, 15);
            this.circleUC54.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC54.TabIndex = 159;
            // 
            // circleUC34
            // 
            this.circleUC34.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC34.ExtenderWidth = 3;
            this.circleUC34.Fill = false;
            this.circleUC34.GroupID = null;
            this.circleUC34.Location = new System.Drawing.Point(12, 765);
            this.circleUC34.Margin = new System.Windows.Forms.Padding(12582912, 176720513, 12582912, 176720513);
            this.circleUC34.Name = "circleUC34";
            this.circleUC34.Size = new System.Drawing.Size(15, 15);
            this.circleUC34.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC34.TabIndex = 159;
            // 
            // circleUC33
            // 
            this.circleUC33.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC33.ExtenderWidth = 3;
            this.circleUC33.Fill = false;
            this.circleUC33.GroupID = null;
            this.circleUC33.Location = new System.Drawing.Point(12, 740);
            this.circleUC33.Margin = new System.Windows.Forms.Padding(12582912, 176720513, 12582912, 176720513);
            this.circleUC33.Name = "circleUC33";
            this.circleUC33.Size = new System.Drawing.Size(15, 15);
            this.circleUC33.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC33.TabIndex = 158;
            // 
            // circleUC32
            // 
            this.circleUC32.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC32.ExtenderWidth = 3;
            this.circleUC32.Fill = false;
            this.circleUC32.GroupID = null;
            this.circleUC32.Location = new System.Drawing.Point(12, 715);
            this.circleUC32.Margin = new System.Windows.Forms.Padding(12582912, 176720513, 12582912, 176720513);
            this.circleUC32.Name = "circleUC32";
            this.circleUC32.Size = new System.Drawing.Size(15, 15);
            this.circleUC32.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC32.TabIndex = 157;
            // 
            // circleUC31
            // 
            this.circleUC31.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC31.ExtenderWidth = 3;
            this.circleUC31.Fill = false;
            this.circleUC31.GroupID = null;
            this.circleUC31.Location = new System.Drawing.Point(12, 690);
            this.circleUC31.Margin = new System.Windows.Forms.Padding(12582912, 176720513, 12582912, 176720513);
            this.circleUC31.Name = "circleUC31";
            this.circleUC31.Size = new System.Drawing.Size(15, 15);
            this.circleUC31.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC31.TabIndex = 156;
            // 
            // circleUC30
            // 
            this.circleUC30.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC30.ExtenderWidth = 3;
            this.circleUC30.Fill = false;
            this.circleUC30.GroupID = null;
            this.circleUC30.Location = new System.Drawing.Point(12, 665);
            this.circleUC30.Margin = new System.Windows.Forms.Padding(12582912, 176720513, 12582912, 176720513);
            this.circleUC30.Name = "circleUC30";
            this.circleUC30.Size = new System.Drawing.Size(15, 15);
            this.circleUC30.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC30.TabIndex = 155;
            // 
            // circleUC29
            // 
            this.circleUC29.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC29.ExtenderWidth = 3;
            this.circleUC29.Fill = false;
            this.circleUC29.GroupID = null;
            this.circleUC29.Location = new System.Drawing.Point(12, 640);
            this.circleUC29.Margin = new System.Windows.Forms.Padding(12582912, 176720513, 12582912, 176720513);
            this.circleUC29.Name = "circleUC29";
            this.circleUC29.Size = new System.Drawing.Size(15, 15);
            this.circleUC29.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC29.TabIndex = 154;
            // 
            // circleUC28
            // 
            this.circleUC28.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC28.ExtenderWidth = 3;
            this.circleUC28.Fill = false;
            this.circleUC28.GroupID = null;
            this.circleUC28.Location = new System.Drawing.Point(12, 615);
            this.circleUC28.Margin = new System.Windows.Forms.Padding(12582912, 176720513, 12582912, 176720513);
            this.circleUC28.Name = "circleUC28";
            this.circleUC28.Size = new System.Drawing.Size(15, 15);
            this.circleUC28.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC28.TabIndex = 153;
            // 
            // circleUC27
            // 
            this.circleUC27.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC27.ExtenderWidth = 3;
            this.circleUC27.Fill = false;
            this.circleUC27.GroupID = null;
            this.circleUC27.Location = new System.Drawing.Point(12, 590);
            this.circleUC27.Margin = new System.Windows.Forms.Padding(6291456, 78542451, 6291456, 78542451);
            this.circleUC27.Name = "circleUC27";
            this.circleUC27.Size = new System.Drawing.Size(15, 15);
            this.circleUC27.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC27.TabIndex = 152;
            // 
            // label262
            // 
            this.label262.AutoSize = true;
            this.label262.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label262.Location = new System.Drawing.Point(1693, 851);
            this.label262.Name = "label262";
            this.label262.Size = new System.Drawing.Size(0, 18);
            this.label262.TabIndex = 151;
            // 
            // label266
            // 
            this.label266.AutoSize = true;
            this.label266.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label266.Location = new System.Drawing.Point(1598, 290);
            this.label266.Name = "label266";
            this.label266.Size = new System.Drawing.Size(157, 18);
            this.label266.TabIndex = 147;
            this.label266.Text = "MC81 REMOTE MODE";
            // 
            // label267
            // 
            this.label267.AutoSize = true;
            this.label267.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label267.Location = new System.Drawing.Point(1598, 265);
            this.label267.Name = "label267";
            this.label267.Size = new System.Drawing.Size(143, 18);
            this.label267.TabIndex = 146;
            this.label267.Text = "MC81 LOCAL MODE";
            // 
            // label268
            // 
            this.label268.AutoSize = true;
            this.label268.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label268.Location = new System.Drawing.Point(1598, 240);
            this.label268.Name = "label268";
            this.label268.Size = new System.Drawing.Size(227, 18);
            this.label268.TabIndex = 145;
            this.label268.Text = "MC81 DISCONNECTOR TROUBLE";
            // 
            // label269
            // 
            this.label269.AutoSize = true;
            this.label269.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label269.Location = new System.Drawing.Point(1598, 165);
            this.label269.Name = "label269";
            this.label269.Size = new System.Drawing.Size(225, 18);
            this.label269.TabIndex = 143;
            this.label269.Text = "MG01 TEMPERATURE WARNING";
            // 
            // label270
            // 
            this.label270.AutoSize = true;
            this.label270.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label270.Location = new System.Drawing.Point(1598, 215);
            this.label270.Name = "label270";
            this.label270.Size = new System.Drawing.Size(269, 18);
            this.label270.TabIndex = 142;
            this.label270.Text = "MC81 FRAME FAULT PROTECTION TRIP";
            // 
            // label271
            // 
            this.label271.AutoSize = true;
            this.label271.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label271.Location = new System.Drawing.Point(1598, 190);
            this.label271.Name = "label271";
            this.label271.Size = new System.Drawing.Size(185, 18);
            this.label271.TabIndex = 141;
            this.label271.Text = "MG01 TEMPERATURE TRIP";
            // 
            // label272
            // 
            this.label272.AutoSize = true;
            this.label272.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label272.Location = new System.Drawing.Point(1598, 140);
            this.label272.Name = "label272";
            this.label272.Size = new System.Drawing.Size(116, 18);
            this.label272.TabIndex = 140;
            this.label272.Text = "MC81 MCB TRIP";
            // 
            // label273
            // 
            this.label273.AutoSize = true;
            this.label273.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label273.Location = new System.Drawing.Point(1598, 115);
            this.label273.Name = "label273";
            this.label273.Size = new System.Drawing.Size(294, 18);
            this.label273.TabIndex = 144;
            this.label273.Text = "MC81 TIME SYNCHONISATION DISTURBER";
            // 
            // label274
            // 
            this.label274.AutoSize = true;
            this.label274.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label274.Location = new System.Drawing.Point(1598, 90);
            this.label274.Name = "label274";
            this.label274.Size = new System.Drawing.Size(157, 18);
            this.label274.TabIndex = 139;
            this.label274.Text = "MC41 REMOTE MODE";
            // 
            // label275
            // 
            this.label275.AutoSize = true;
            this.label275.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label275.Location = new System.Drawing.Point(1250, 815);
            this.label275.Name = "label275";
            this.label275.Size = new System.Drawing.Size(143, 18);
            this.label275.TabIndex = 138;
            this.label275.Text = "MC41 LOCAL MODE";
            // 
            // label276
            // 
            this.label276.AutoSize = true;
            this.label276.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label276.Location = new System.Drawing.Point(1250, 790);
            this.label276.Name = "label276";
            this.label276.Size = new System.Drawing.Size(227, 18);
            this.label276.TabIndex = 137;
            this.label276.Text = "MC41 DISCONNECTOR TROUBLE";
            // 
            // label277
            // 
            this.label277.AutoSize = true;
            this.label277.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label277.Location = new System.Drawing.Point(1250, 765);
            this.label277.Name = "label277";
            this.label277.Size = new System.Drawing.Size(116, 18);
            this.label277.TabIndex = 136;
            this.label277.Text = "MC41 MCB TRIP";
            // 
            // label278
            // 
            this.label278.AutoSize = true;
            this.label278.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label278.Location = new System.Drawing.Point(1250, 740);
            this.label278.Name = "label278";
            this.label278.Size = new System.Drawing.Size(223, 18);
            this.label278.TabIndex = 135;
            this.label278.Text = "MC41 COMMUNICATION FAULT";
            // 
            // label279
            // 
            this.label279.AutoSize = true;
            this.label279.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label279.Location = new System.Drawing.Point(1250, 715);
            this.label279.Name = "label279";
            this.label279.Size = new System.Drawing.Size(284, 18);
            this.label279.TabIndex = 134;
            this.label279.Text = "MC22 DRAWER IN OPERATION POSITION";
            // 
            // label280
            // 
            this.label280.AutoSize = true;
            this.label280.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label280.Location = new System.Drawing.Point(1250, 690);
            this.label280.Name = "label280";
            this.label280.Size = new System.Drawing.Size(236, 18);
            this.label280.TabIndex = 133;
            this.label280.Text = "MC22 DRAWER IN TEST POSITION";
            // 
            // label281
            // 
            this.label281.AutoSize = true;
            this.label281.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label281.Location = new System.Drawing.Point(1250, 665);
            this.label281.Name = "label281";
            this.label281.Size = new System.Drawing.Size(198, 18);
            this.label281.TabIndex = 132;
            this.label281.Text = "MC22 Switch on CB possible";
            // 
            // label282
            // 
            this.label282.AutoSize = true;
            this.label282.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label282.Location = new System.Drawing.Point(1250, 640);
            this.label282.Name = "label282";
            this.label282.Size = new System.Drawing.Size(157, 18);
            this.label282.TabIndex = 131;
            this.label282.Text = "MC22 REMOTE MODE";
            // 
            // label283
            // 
            this.label283.AutoSize = true;
            this.label283.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label283.Location = new System.Drawing.Point(1250, 615);
            this.label283.Name = "label283";
            this.label283.Size = new System.Drawing.Size(143, 18);
            this.label283.TabIndex = 130;
            this.label283.Text = "MC22 LOCAL MODE";
            // 
            // label284
            // 
            this.label284.AutoSize = true;
            this.label284.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label284.Location = new System.Drawing.Point(1250, 590);
            this.label284.Name = "label284";
            this.label284.Size = new System.Drawing.Size(187, 18);
            this.label284.TabIndex = 129;
            this.label284.Text = "MC22 LINE TEST RUNNING";
            // 
            // label285
            // 
            this.label285.AutoSize = true;
            this.label285.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label285.Location = new System.Drawing.Point(1250, 565);
            this.label285.Name = "label285";
            this.label285.Size = new System.Drawing.Size(227, 18);
            this.label285.TabIndex = 128;
            this.label285.Text = "MC22 DRAWER POSITION FAULT";
            // 
            // label259
            // 
            this.label259.AutoSize = true;
            this.label259.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label259.Location = new System.Drawing.Point(1598, 65);
            this.label259.Name = "label259";
            this.label259.Size = new System.Drawing.Size(311, 18);
            this.label259.TabIndex = 127;
            this.label259.Text = "MC22 LINE TEST FAULT : CONT.SHORT CIRUIT";
            // 
            // label260
            // 
            this.label260.AutoSize = true;
            this.label260.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label260.Location = new System.Drawing.Point(1598, 40);
            this.label260.Name = "label260";
            this.label260.Size = new System.Drawing.Size(224, 18);
            this.label260.TabIndex = 126;
            this.label260.Text = "MC22 TRANSFER TRIP RECEIVED";
            // 
            // label261
            // 
            this.label261.AutoSize = true;
            this.label261.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label261.Location = new System.Drawing.Point(1250, 540);
            this.label261.Name = "label261";
            this.label261.Size = new System.Drawing.Size(193, 18);
            this.label261.TabIndex = 125;
            this.label261.Text = "MC22 TRANSFER TRIP SENT";
            // 
            // label247
            // 
            this.label247.AutoSize = true;
            this.label247.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label247.Location = new System.Drawing.Point(1250, 65);
            this.label247.Name = "label247";
            this.label247.Size = new System.Drawing.Size(198, 18);
            this.label247.TabIndex = 124;
            this.label247.Text = "MC21 Switch on CB possible";
            // 
            // label248
            // 
            this.label248.AutoSize = true;
            this.label248.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label248.Location = new System.Drawing.Point(1250, 40);
            this.label248.Name = "label248";
            this.label248.Size = new System.Drawing.Size(157, 18);
            this.label248.TabIndex = 123;
            this.label248.Text = "MC21 REMOTE MODE";
            // 
            // label249
            // 
            this.label249.AutoSize = true;
            this.label249.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label249.Location = new System.Drawing.Point(736, 690);
            this.label249.Name = "label249";
            this.label249.Size = new System.Drawing.Size(143, 18);
            this.label249.TabIndex = 122;
            this.label249.Text = "MC21 LOCAL MODE";
            // 
            // label250
            // 
            this.label250.AutoSize = true;
            this.label250.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label250.Location = new System.Drawing.Point(736, 665);
            this.label250.Name = "label250";
            this.label250.Size = new System.Drawing.Size(317, 18);
            this.label250.TabIndex = 121;
            this.label250.Text = "MC21 LINE TEST FAULT : CONT.SHORT CURUIT";
            // 
            // label251
            // 
            this.label251.AutoSize = true;
            this.label251.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label251.Location = new System.Drawing.Point(736, 640);
            this.label251.Name = "label251";
            this.label251.Size = new System.Drawing.Size(224, 18);
            this.label251.TabIndex = 120;
            this.label251.Text = "MC21 TRANSFER TRIP RECEIVED";
            // 
            // label252
            // 
            this.label252.AutoSize = true;
            this.label252.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label252.Location = new System.Drawing.Point(736, 615);
            this.label252.Name = "label252";
            this.label252.Size = new System.Drawing.Size(193, 18);
            this.label252.TabIndex = 119;
            this.label252.Text = "MC21 TRANSFER TRIP SENT";
            // 
            // label253
            // 
            this.label253.AutoSize = true;
            this.label253.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label253.Location = new System.Drawing.Point(736, 590);
            this.label253.Name = "label253";
            this.label253.Size = new System.Drawing.Size(225, 18);
            this.label253.TabIndex = 118;
            this.label253.Text = "MC21 TRIP : CABLE PROTECTION";
            // 
            // label254
            // 
            this.label254.AutoSize = true;
            this.label254.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label254.Location = new System.Drawing.Point(736, 565);
            this.label254.Name = "label254";
            this.label254.Size = new System.Drawing.Size(132, 18);
            this.label254.TabIndex = 117;
            this.label254.Text = "MC21 TRIP : I DMT";
            // 
            // label255
            // 
            this.label255.AutoSize = true;
            this.label255.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label255.Location = new System.Drawing.Point(736, 540);
            this.label255.Name = "label255";
            this.label255.Size = new System.Drawing.Size(168, 18);
            this.label255.TabIndex = 116;
            this.label255.Text = "MC21 WARNING: I DMT";
            // 
            // label256
            // 
            this.label256.AutoSize = true;
            this.label256.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label256.Location = new System.Drawing.Point(736, 515);
            this.label256.Name = "label256";
            this.label256.Size = new System.Drawing.Size(117, 18);
            this.label256.TabIndex = 115;
            this.label256.Text = "MC21 di/dt TRIP";
            // 
            // label257
            // 
            this.label257.AutoSize = true;
            this.label257.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label257.Location = new System.Drawing.Point(736, 490);
            this.label257.Name = "label257";
            this.label257.Size = new System.Drawing.Size(157, 18);
            this.label257.TabIndex = 114;
            this.label257.Text = "MC21 di/dt WARNING";
            // 
            // label258
            // 
            this.label258.AutoSize = true;
            this.label258.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label258.Location = new System.Drawing.Point(736, 465);
            this.label258.Name = "label258";
            this.label258.Size = new System.Drawing.Size(136, 18);
            this.label258.TabIndex = 113;
            this.label258.Text = "MC21 DELTA I TRIP";
            // 
            // label226
            // 
            this.label226.AutoSize = true;
            this.label226.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label226.Location = new System.Drawing.Point(1250, 515);
            this.label226.Name = "label226";
            this.label226.Size = new System.Drawing.Size(225, 18);
            this.label226.TabIndex = 112;
            this.label226.Text = "MC22 TRIP : CABLE PROTECTION";
            // 
            // label227
            // 
            this.label227.AutoSize = true;
            this.label227.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label227.Location = new System.Drawing.Point(1250, 490);
            this.label227.Name = "label227";
            this.label227.Size = new System.Drawing.Size(132, 18);
            this.label227.TabIndex = 111;
            this.label227.Text = "MC22 TRIP : I DMT";
            // 
            // label228
            // 
            this.label228.AutoSize = true;
            this.label228.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label228.Location = new System.Drawing.Point(1250, 465);
            this.label228.Name = "label228";
            this.label228.Size = new System.Drawing.Size(172, 18);
            this.label228.TabIndex = 110;
            this.label228.Text = "MC22 WARNING : I DMT";
            // 
            // label229
            // 
            this.label229.AutoSize = true;
            this.label229.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label229.Location = new System.Drawing.Point(1250, 440);
            this.label229.Name = "label229";
            this.label229.Size = new System.Drawing.Size(117, 18);
            this.label229.TabIndex = 109;
            this.label229.Text = "MC22 di/dt TRIP";
            // 
            // label230
            // 
            this.label230.AutoSize = true;
            this.label230.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label230.Location = new System.Drawing.Point(1250, 365);
            this.label230.Name = "label230";
            this.label230.Size = new System.Drawing.Size(176, 18);
            this.label230.TabIndex = 107;
            this.label230.Text = "MC22 DELTA I WARNING";
            // 
            // label231
            // 
            this.label231.AutoSize = true;
            this.label231.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label231.Location = new System.Drawing.Point(1250, 415);
            this.label231.Name = "label231";
            this.label231.Size = new System.Drawing.Size(157, 18);
            this.label231.TabIndex = 106;
            this.label231.Text = "MC22 di/dt WARNING";
            // 
            // label232
            // 
            this.label232.AutoSize = true;
            this.label232.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label232.Location = new System.Drawing.Point(1250, 390);
            this.label232.Name = "label232";
            this.label232.Size = new System.Drawing.Size(136, 18);
            this.label232.TabIndex = 105;
            this.label232.Text = "MC22 DELTA I TRIP";
            // 
            // label233
            // 
            this.label233.AutoSize = true;
            this.label233.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label233.Location = new System.Drawing.Point(1250, 340);
            this.label233.Name = "label233";
            this.label233.Size = new System.Drawing.Size(132, 18);
            this.label233.TabIndex = 104;
            this.label233.Text = "MC22 TRIP : I MAX";
            // 
            // label234
            // 
            this.label234.AutoSize = true;
            this.label234.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label234.Location = new System.Drawing.Point(1250, 315);
            this.label234.Name = "label234";
            this.label234.Size = new System.Drawing.Size(172, 18);
            this.label234.TabIndex = 108;
            this.label234.Text = "MC22 WARNING : I MAX";
            // 
            // label235
            // 
            this.label235.AutoSize = true;
            this.label235.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label235.Location = new System.Drawing.Point(1250, 290);
            this.label235.Name = "label235";
            this.label235.Size = new System.Drawing.Size(218, 18);
            this.label235.TabIndex = 103;
            this.label235.Text = "MC22 OVERCURRENT TRIPPING";
            // 
            // label236
            // 
            this.label236.AutoSize = true;
            this.label236.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label236.Location = new System.Drawing.Point(1250, 265);
            this.label236.Name = "label236";
            this.label236.Size = new System.Drawing.Size(288, 18);
            this.label236.TabIndex = 102;
            this.label236.Text = "MC22 FRAME FAULT PROTECTION ACTIVE";
            // 
            // label237
            // 
            this.label237.AutoSize = true;
            this.label237.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label237.Location = new System.Drawing.Point(1250, 240);
            this.label237.Name = "label237";
            this.label237.Size = new System.Drawing.Size(137, 18);
            this.label237.TabIndex = 101;
            this.label237.Text = "MC22 SHUNT LOSS";
            // 
            // label238
            // 
            this.label238.AutoSize = true;
            this.label238.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label238.Location = new System.Drawing.Point(1250, 215);
            this.label238.Name = "label238";
            this.label238.Size = new System.Drawing.Size(116, 18);
            this.label238.TabIndex = 100;
            this.label238.Text = "MC22 LOSS FOC";
            // 
            // label239
            // 
            this.label239.AutoSize = true;
            this.label239.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label239.Location = new System.Drawing.Point(1250, 190);
            this.label239.Name = "label239";
            this.label239.Size = new System.Drawing.Size(209, 18);
            this.label239.TabIndex = 99;
            this.label239.Text = "MC22 MAINTENANCE ALARM";
            // 
            // label240
            // 
            this.label240.AutoSize = true;
            this.label240.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label240.Location = new System.Drawing.Point(1250, 165);
            this.label240.Name = "label240";
            this.label240.Size = new System.Drawing.Size(234, 18);
            this.label240.TabIndex = 98;
            this.label240.Text = "MC22 SITRAS PRO SYSTEM FAULT";
            // 
            // label241
            // 
            this.label241.AutoSize = true;
            this.label241.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label241.Location = new System.Drawing.Point(1250, 140);
            this.label241.Name = "label241";
            this.label241.Size = new System.Drawing.Size(296, 18);
            this.label241.TabIndex = 97;
            this.label241.Text = "MC22 TIME SYNCHONISATION DISTURBED";
            // 
            // label242
            // 
            this.label242.AutoSize = true;
            this.label242.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label242.Location = new System.Drawing.Point(1250, 115);
            this.label242.Name = "label242";
            this.label242.Size = new System.Drawing.Size(116, 18);
            this.label242.TabIndex = 96;
            this.label242.Text = "MC22 MCB TRIP";
            // 
            // label243
            // 
            this.label243.AutoSize = true;
            this.label243.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label243.Location = new System.Drawing.Point(1250, 90);
            this.label243.Name = "label243";
            this.label243.Size = new System.Drawing.Size(153, 18);
            this.label243.TabIndex = 95;
            this.label243.Text = "MC22 GROUP ALARM";
            // 
            // label244
            // 
            this.label244.AutoSize = true;
            this.label244.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label244.Location = new System.Drawing.Point(736, 765);
            this.label244.Name = "label244";
            this.label244.Size = new System.Drawing.Size(223, 18);
            this.label244.TabIndex = 94;
            this.label244.Text = "MC22 COMMUNICATION FAULT";
            // 
            // label245
            // 
            this.label245.AutoSize = true;
            this.label245.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label245.Location = new System.Drawing.Point(736, 740);
            this.label245.Name = "label245";
            this.label245.Size = new System.Drawing.Size(284, 18);
            this.label245.TabIndex = 93;
            this.label245.Text = "MC21 DRAWER IN OPERATION POSITION";
            // 
            // label246
            // 
            this.label246.AutoSize = true;
            this.label246.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label246.Location = new System.Drawing.Point(736, 715);
            this.label246.Name = "label246";
            this.label246.Size = new System.Drawing.Size(236, 18);
            this.label246.TabIndex = 92;
            this.label246.Text = "MC21 DRAWER IN TEST POSITION";
            // 
            // label205
            // 
            this.label205.AutoSize = true;
            this.label205.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label205.Location = new System.Drawing.Point(736, 440);
            this.label205.Name = "label205";
            this.label205.Size = new System.Drawing.Size(176, 18);
            this.label205.TabIndex = 91;
            this.label205.Text = "MC21 DELTA I WARNING";
            // 
            // label206
            // 
            this.label206.AutoSize = true;
            this.label206.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label206.Location = new System.Drawing.Point(736, 415);
            this.label206.Name = "label206";
            this.label206.Size = new System.Drawing.Size(132, 18);
            this.label206.TabIndex = 90;
            this.label206.Text = "MC21 TRIP : I MAX";
            // 
            // label207
            // 
            this.label207.AutoSize = true;
            this.label207.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label207.Location = new System.Drawing.Point(1012, 366);
            this.label207.Name = "label207";
            this.label207.Size = new System.Drawing.Size(172, 18);
            this.label207.TabIndex = 89;
            this.label207.Text = "MC21 WARNING : I MAX";
            // 
            // label208
            // 
            this.label208.AutoSize = true;
            this.label208.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label208.Location = new System.Drawing.Point(736, 390);
            this.label208.Name = "label208";
            this.label208.Size = new System.Drawing.Size(218, 18);
            this.label208.TabIndex = 88;
            this.label208.Text = "MC21 OVERCURRENT TRIPPING";
            // 
            // label209
            // 
            this.label209.AutoSize = true;
            this.label209.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label209.Location = new System.Drawing.Point(736, 315);
            this.label209.Name = "label209";
            this.label209.Size = new System.Drawing.Size(116, 18);
            this.label209.TabIndex = 86;
            this.label209.Text = "MC21 LOSS FOC";
            // 
            // label210
            // 
            this.label210.AutoSize = true;
            this.label210.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label210.Location = new System.Drawing.Point(736, 365);
            this.label210.Name = "label210";
            this.label210.Size = new System.Drawing.Size(288, 18);
            this.label210.TabIndex = 85;
            this.label210.Text = "MC21 FRAME FAULT PROTECTION ACTIVE";
            // 
            // label211
            // 
            this.label211.AutoSize = true;
            this.label211.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label211.Location = new System.Drawing.Point(736, 340);
            this.label211.Name = "label211";
            this.label211.Size = new System.Drawing.Size(137, 18);
            this.label211.TabIndex = 84;
            this.label211.Text = "MC21 SHUNT LOSS";
            // 
            // label212
            // 
            this.label212.AutoSize = true;
            this.label212.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label212.Location = new System.Drawing.Point(736, 290);
            this.label212.Name = "label212";
            this.label212.Size = new System.Drawing.Size(209, 18);
            this.label212.TabIndex = 83;
            this.label212.Text = "MC21 MAINTENANCE ALARM";
            // 
            // label213
            // 
            this.label213.AutoSize = true;
            this.label213.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label213.Location = new System.Drawing.Point(736, 265);
            this.label213.Name = "label213";
            this.label213.Size = new System.Drawing.Size(234, 18);
            this.label213.TabIndex = 87;
            this.label213.Text = "MC21 SITRAS PRO SYSTEM FAULT";
            // 
            // label214
            // 
            this.label214.AutoSize = true;
            this.label214.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label214.Location = new System.Drawing.Point(736, 240);
            this.label214.Name = "label214";
            this.label214.Size = new System.Drawing.Size(296, 18);
            this.label214.TabIndex = 82;
            this.label214.Text = "MC21 TIME SYNCHONISATION DISTURBED";
            // 
            // label215
            // 
            this.label215.AutoSize = true;
            this.label215.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label215.Location = new System.Drawing.Point(736, 215);
            this.label215.Name = "label215";
            this.label215.Size = new System.Drawing.Size(116, 18);
            this.label215.TabIndex = 81;
            this.label215.Text = "MC21 MCB TRIP";
            // 
            // label216
            // 
            this.label216.AutoSize = true;
            this.label216.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label216.Location = new System.Drawing.Point(736, 190);
            this.label216.Name = "label216";
            this.label216.Size = new System.Drawing.Size(153, 18);
            this.label216.TabIndex = 80;
            this.label216.Text = "MC21 GROUP ALARM";
            // 
            // label217
            // 
            this.label217.AutoSize = true;
            this.label217.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label217.Location = new System.Drawing.Point(736, 815);
            this.label217.Name = "label217";
            this.label217.Size = new System.Drawing.Size(223, 18);
            this.label217.TabIndex = 79;
            this.label217.Text = "MC21 COMMUNICATION FAULT";
            // 
            // label218
            // 
            this.label218.AutoSize = true;
            this.label218.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label218.Location = new System.Drawing.Point(736, 790);
            this.label218.Name = "label218";
            this.label218.Size = new System.Drawing.Size(126, 18);
            this.label218.TabIndex = 78;
            this.label218.Text = "COM UPS ALARM";
            // 
            // label219
            // 
            this.label219.AutoSize = true;
            this.label219.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label219.Location = new System.Drawing.Point(736, 165);
            this.label219.Name = "label219";
            this.label219.Size = new System.Drawing.Size(118, 18);
            this.label219.TabIndex = 77;
            this.label219.Text = "COM UPS FAULT";
            // 
            // label220
            // 
            this.label220.AutoSize = true;
            this.label220.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label220.Location = new System.Drawing.Point(736, 140);
            this.label220.Name = "label220";
            this.label220.Size = new System.Drawing.Size(119, 18);
            this.label220.TabIndex = 76;
            this.label220.Text = "ESN UPS ALARM";
            // 
            // label221
            // 
            this.label221.AutoSize = true;
            this.label221.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label221.Location = new System.Drawing.Point(736, 115);
            this.label221.Name = "label221";
            this.label221.Size = new System.Drawing.Size(111, 18);
            this.label221.TabIndex = 75;
            this.label221.Text = "ESN UPS FAULT";
            // 
            // label222
            // 
            this.label222.AutoSize = true;
            this.label222.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label222.Location = new System.Drawing.Point(736, 90);
            this.label222.Name = "label222";
            this.label222.Size = new System.Drawing.Size(196, 18);
            this.label222.TabIndex = 74;
            this.label222.Text = "UPS Air Conditioning FAULT";
            // 
            // label223
            // 
            this.label223.AutoSize = true;
            this.label223.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label223.Location = new System.Drawing.Point(736, 65);
            this.label223.Name = "label223";
            this.label223.Size = new System.Drawing.Size(147, 18);
            this.label223.TabIndex = 73;
            this.label223.Text = "DDP MCCB OFF/TRIP";
            // 
            // label224
            // 
            this.label224.AutoSize = true;
            this.label224.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label224.Location = new System.Drawing.Point(736, 40);
            this.label224.Name = "label224";
            this.label224.Size = new System.Drawing.Size(146, 18);
            this.label224.TabIndex = 72;
            this.label224.Text = "ADP MCCB OFF/TRIP";
            // 
            // label225
            // 
            this.label225.AutoSize = true;
            this.label225.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label225.Location = new System.Drawing.Point(388, 765);
            this.label225.Name = "label225";
            this.label225.Size = new System.Drawing.Size(99, 18);
            this.label225.TabIndex = 71;
            this.label225.Text = "BC SID FAULT";
            // 
            // label184
            // 
            this.label184.AutoSize = true;
            this.label184.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label184.Location = new System.Drawing.Point(388, 740);
            this.label184.Name = "label184";
            this.label184.Size = new System.Drawing.Size(138, 18);
            this.label184.TabIndex = 70;
            this.label184.Text = "BC GROUND FAULT";
            // 
            // label185
            // 
            this.label185.AutoSize = true;
            this.label185.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label185.Location = new System.Drawing.Point(388, 715);
            this.label185.Name = "label185";
            this.label185.Size = new System.Drawing.Size(83, 18);
            this.label185.TabIndex = 69;
            this.label185.Text = "BC DC TRIP";
            // 
            // label186
            // 
            this.label186.AutoSize = true;
            this.label186.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label186.Location = new System.Drawing.Point(388, 690);
            this.label186.Name = "label186";
            this.label186.Size = new System.Drawing.Size(119, 18);
            this.label186.TabIndex = 68;
            this.label186.Text = "BC DC OUT LOW";
            // 
            // label187
            // 
            this.label187.AutoSize = true;
            this.label187.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label187.Location = new System.Drawing.Point(388, 665);
            this.label187.Name = "label187";
            this.label187.Size = new System.Drawing.Size(123, 18);
            this.label187.TabIndex = 67;
            this.label187.Text = "BC DC OUT HIGH";
            // 
            // label188
            // 
            this.label188.AutoSize = true;
            this.label188.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label188.Location = new System.Drawing.Point(388, 590);
            this.label188.Name = "label188";
            this.label188.Size = new System.Drawing.Size(82, 18);
            this.label188.TabIndex = 65;
            this.label188.Text = "BC AC TRIP";
            // 
            // label189
            // 
            this.label189.AutoSize = true;
            this.label189.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label189.Location = new System.Drawing.Point(388, 640);
            this.label189.Name = "label189";
            this.label189.Size = new System.Drawing.Size(142, 18);
            this.label189.TabIndex = 64;
            this.label189.Text = "BC CHARGER FAULT";
            // 
            // label190
            // 
            this.label190.AutoSize = true;
            this.label190.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label190.Location = new System.Drawing.Point(388, 615);
            this.label190.Name = "label190";
            this.label190.Size = new System.Drawing.Size(139, 18);
            this.label190.TabIndex = 63;
            this.label190.Text = "BC AC POWER LOW";
            // 
            // label191
            // 
            this.label191.AutoSize = true;
            this.label191.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label191.Location = new System.Drawing.Point(388, 565);
            this.label191.Name = "label191";
            this.label191.Size = new System.Drawing.Size(181, 18);
            this.label191.TabIndex = 62;
            this.label191.Text = "TR1、TR2 MCCB OFF/TRIP";
            // 
            // label192
            // 
            this.label192.AutoSize = true;
            this.label192.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label192.Location = new System.Drawing.Point(388, 540);
            this.label192.Name = "label192";
            this.label192.Size = new System.Drawing.Size(90, 18);
            this.label192.TabIndex = 66;
            this.label192.Text = "H04 CB TRIP";
            // 
            // label193
            // 
            this.label193.AutoSize = true;
            this.label193.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label193.Location = new System.Drawing.Point(388, 515);
            this.label193.Name = "label193";
            this.label193.Size = new System.Drawing.Size(139, 18);
            this.label193.TabIndex = 61;
            this.label193.Text = "H04 IED RELAY 51N";
            // 
            // label194
            // 
            this.label194.AutoSize = true;
            this.label194.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label194.Location = new System.Drawing.Point(388, 490);
            this.label194.Name = "label194";
            this.label194.Size = new System.Drawing.Size(136, 18);
            this.label194.TabIndex = 60;
            this.label194.Text = "H04 IED RELEAY 51";
            // 
            // label195
            // 
            this.label195.AutoSize = true;
            this.label195.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label195.Location = new System.Drawing.Point(388, 465);
            this.label195.Name = "label195";
            this.label195.Size = new System.Drawing.Size(139, 18);
            this.label195.TabIndex = 59;
            this.label195.Text = "H04 IED RELAY 50N";
            // 
            // label196
            // 
            this.label196.AutoSize = true;
            this.label196.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label196.Location = new System.Drawing.Point(388, 440);
            this.label196.Name = "label196";
            this.label196.Size = new System.Drawing.Size(128, 18);
            this.label196.TabIndex = 58;
            this.label196.Text = "H04 IED RELAY 50";
            // 
            // label197
            // 
            this.label197.AutoSize = true;
            this.label197.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label197.Location = new System.Drawing.Point(388, 415);
            this.label197.Name = "label197";
            this.label197.Size = new System.Drawing.Size(208, 18);
            this.label197.TabIndex = 57;
            this.label197.Text = "H04 IED RELAY FAULT ALARM";
            // 
            // label198
            // 
            this.label198.AutoSize = true;
            this.label198.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label198.Location = new System.Drawing.Point(388, 390);
            this.label198.Name = "label198";
            this.label198.Size = new System.Drawing.Size(278, 18);
            this.label198.TabIndex = 56;
            this.label198.Text = "H04 GCB PRESSURE LOW LATCH ALARM";
            // 
            // label199
            // 
            this.label199.AutoSize = true;
            this.label199.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label199.Location = new System.Drawing.Point(388, 815);
            this.label199.Name = "label199";
            this.label199.Size = new System.Drawing.Size(229, 18);
            this.label199.TabIndex = 55;
            this.label199.Text = "H04 GCB PRESSUSR LOW ALARM";
            // 
            // label200
            // 
            this.label200.AutoSize = true;
            this.label200.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label200.Location = new System.Drawing.Point(388, 790);
            this.label200.Name = "label200";
            this.label200.Size = new System.Drawing.Size(234, 18);
            this.label200.TabIndex = 54;
            this.label200.Text = "H04 CB PRESSURE LEVEL2 ALARM";
            // 
            // label201
            // 
            this.label201.AutoSize = true;
            this.label201.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label201.Location = new System.Drawing.Point(388, 365);
            this.label201.Name = "label201";
            this.label201.Size = new System.Drawing.Size(234, 18);
            this.label201.TabIndex = 53;
            this.label201.Text = "H04 CB PRESSURE LEVEL1 ALARM";
            // 
            // label202
            // 
            this.label202.AutoSize = true;
            this.label202.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label202.Location = new System.Drawing.Point(388, 340);
            this.label202.Name = "label202";
            this.label202.Size = new System.Drawing.Size(86, 18);
            this.label202.TabIndex = 52;
            this.label202.Text = "H04 CB OFF";
            // 
            // label203
            // 
            this.label203.AutoSize = true;
            this.label203.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label203.Location = new System.Drawing.Point(388, 315);
            this.label203.Name = "label203";
            this.label203.Size = new System.Drawing.Size(83, 18);
            this.label203.TabIndex = 51;
            this.label203.Text = "H04 CB ON";
            // 
            // label204
            // 
            this.label204.AutoSize = true;
            this.label204.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label204.Location = new System.Drawing.Point(388, 290);
            this.label204.Name = "label204";
            this.label204.Size = new System.Drawing.Size(157, 18);
            this.label204.TabIndex = 50;
            this.label204.Text = "H04 GIS LOCAL MODE";
            // 
            // label164
            // 
            this.label164.AutoSize = true;
            this.label164.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label164.Location = new System.Drawing.Point(388, 265);
            this.label164.Name = "label164";
            this.label164.Size = new System.Drawing.Size(171, 18);
            this.label164.TabIndex = 49;
            this.label164.Text = "H04 GIS REMOTE MODE";
            // 
            // label165
            // 
            this.label165.AutoSize = true;
            this.label165.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label165.Location = new System.Drawing.Point(388, 240);
            this.label165.Name = "label165";
            this.label165.Size = new System.Drawing.Size(144, 18);
            this.label165.TabIndex = 48;
            this.label165.Text = "H04 MCCB OFF/TRIP";
            // 
            // label166
            // 
            this.label166.AutoSize = true;
            this.label166.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label166.Location = new System.Drawing.Point(388, 215);
            this.label166.Name = "label166";
            this.label166.Size = new System.Drawing.Size(90, 18);
            this.label166.TabIndex = 47;
            this.label166.Text = "H03 CB TRIP";
            // 
            // label167
            // 
            this.label167.AutoSize = true;
            this.label167.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label167.Location = new System.Drawing.Point(388, 190);
            this.label167.Name = "label167";
            this.label167.Size = new System.Drawing.Size(139, 18);
            this.label167.TabIndex = 46;
            this.label167.Text = "H03 IED RELAY 51N";
            // 
            // label168
            // 
            this.label168.AutoSize = true;
            this.label168.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label168.Location = new System.Drawing.Point(388, 115);
            this.label168.Name = "label168";
            this.label168.Size = new System.Drawing.Size(128, 18);
            this.label168.TabIndex = 44;
            this.label168.Text = "H03 IED RELAY 50";
            // 
            // label169
            // 
            this.label169.AutoSize = true;
            this.label169.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label169.Location = new System.Drawing.Point(388, 165);
            this.label169.Name = "label169";
            this.label169.Size = new System.Drawing.Size(128, 18);
            this.label169.TabIndex = 43;
            this.label169.Text = "H03 IED RELAY 51";
            // 
            // label170
            // 
            this.label170.AutoSize = true;
            this.label170.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label170.Location = new System.Drawing.Point(388, 140);
            this.label170.Name = "label170";
            this.label170.Size = new System.Drawing.Size(139, 18);
            this.label170.TabIndex = 42;
            this.label170.Text = "H03 IED RELAY 50N";
            // 
            // label171
            // 
            this.label171.AutoSize = true;
            this.label171.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label171.Location = new System.Drawing.Point(388, 90);
            this.label171.Name = "label171";
            this.label171.Size = new System.Drawing.Size(208, 18);
            this.label171.TabIndex = 41;
            this.label171.Text = "H03 IED RELAY FAULT ALARM";
            // 
            // label172
            // 
            this.label172.AutoSize = true;
            this.label172.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label172.Location = new System.Drawing.Point(388, 65);
            this.label172.Name = "label172";
            this.label172.Size = new System.Drawing.Size(278, 18);
            this.label172.TabIndex = 45;
            this.label172.Text = "H03 GCB PERSSURE LOW LATCH ALARM";
            // 
            // label173
            // 
            this.label173.AutoSize = true;
            this.label173.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label173.Location = new System.Drawing.Point(388, 40);
            this.label173.Name = "label173";
            this.label173.Size = new System.Drawing.Size(229, 18);
            this.label173.TabIndex = 40;
            this.label173.Text = "H03 GCB PRESSURE LOW ALARM";
            // 
            // label174
            // 
            this.label174.AutoSize = true;
            this.label174.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label174.Location = new System.Drawing.Point(40, 765);
            this.label174.Name = "label174";
            this.label174.Size = new System.Drawing.Size(234, 18);
            this.label174.TabIndex = 39;
            this.label174.Text = "H03 CB PRESSURE LEVEL2 ALARM";
            // 
            // label175
            // 
            this.label175.AutoSize = true;
            this.label175.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label175.Location = new System.Drawing.Point(40, 740);
            this.label175.Name = "label175";
            this.label175.Size = new System.Drawing.Size(234, 18);
            this.label175.TabIndex = 38;
            this.label175.Text = "H03 CB PRESSURE LEVLE1 ALARM";
            // 
            // label176
            // 
            this.label176.AutoSize = true;
            this.label176.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label176.Location = new System.Drawing.Point(40, 715);
            this.label176.Name = "label176";
            this.label176.Size = new System.Drawing.Size(86, 18);
            this.label176.TabIndex = 37;
            this.label176.Text = "H03 CB OFF";
            // 
            // label177
            // 
            this.label177.AutoSize = true;
            this.label177.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label177.Location = new System.Drawing.Point(40, 690);
            this.label177.Name = "label177";
            this.label177.Size = new System.Drawing.Size(83, 18);
            this.label177.TabIndex = 36;
            this.label177.Text = "H03 CB ON";
            // 
            // label178
            // 
            this.label178.AutoSize = true;
            this.label178.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label178.Location = new System.Drawing.Point(40, 665);
            this.label178.Name = "label178";
            this.label178.Size = new System.Drawing.Size(157, 18);
            this.label178.TabIndex = 35;
            this.label178.Text = "H03 GIS LOCAL MODE";
            // 
            // label179
            // 
            this.label179.AutoSize = true;
            this.label179.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label179.Location = new System.Drawing.Point(40, 640);
            this.label179.Name = "label179";
            this.label179.Size = new System.Drawing.Size(171, 18);
            this.label179.TabIndex = 34;
            this.label179.Text = "H03 GIS REMOTE MODE";
            // 
            // label180
            // 
            this.label180.AutoSize = true;
            this.label180.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label180.Location = new System.Drawing.Point(40, 615);
            this.label180.Name = "label180";
            this.label180.Size = new System.Drawing.Size(144, 18);
            this.label180.TabIndex = 33;
            this.label180.Text = "H03 MCCB OFF/TRIP";
            // 
            // label181
            // 
            this.label181.AutoSize = true;
            this.label181.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label181.Location = new System.Drawing.Point(40, 590);
            this.label181.Name = "label181";
            this.label181.Size = new System.Drawing.Size(90, 18);
            this.label181.TabIndex = 32;
            this.label181.Text = "H02 CB TRIP";
            // 
            // label182
            // 
            this.label182.AutoSize = true;
            this.label182.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label182.Location = new System.Drawing.Point(40, 815);
            this.label182.Name = "label182";
            this.label182.Size = new System.Drawing.Size(128, 18);
            this.label182.TabIndex = 31;
            this.label182.Text = "H02 IED RELAY 59";
            // 
            // label183
            // 
            this.label183.AutoSize = true;
            this.label183.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label183.Location = new System.Drawing.Point(40, 790);
            this.label183.Name = "label183";
            this.label183.Size = new System.Drawing.Size(128, 18);
            this.label183.TabIndex = 30;
            this.label183.Text = "H02 IED RELAY 27";
            // 
            // label162
            // 
            this.label162.AutoSize = true;
            this.label162.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label162.Location = new System.Drawing.Point(40, 540);
            this.label162.Name = "label162";
            this.label162.Size = new System.Drawing.Size(128, 18);
            this.label162.TabIndex = 28;
            this.label162.Text = "H02 IED RELAY 51";
            // 
            // label161
            // 
            this.label161.AutoSize = true;
            this.label161.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label161.Location = new System.Drawing.Point(40, 515);
            this.label161.Name = "label161";
            this.label161.Size = new System.Drawing.Size(139, 18);
            this.label161.TabIndex = 27;
            this.label161.Text = "H02 IED RELAY 50N";
            // 
            // label160
            // 
            this.label160.AutoSize = true;
            this.label160.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label160.Location = new System.Drawing.Point(40, 490);
            this.label160.Name = "label160";
            this.label160.Size = new System.Drawing.Size(128, 18);
            this.label160.TabIndex = 25;
            this.label160.Text = "H02 IED RELAY 50";
            // 
            // label159
            // 
            this.label159.AutoSize = true;
            this.label159.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label159.Location = new System.Drawing.Point(40, 465);
            this.label159.Name = "label159";
            this.label159.Size = new System.Drawing.Size(208, 18);
            this.label159.TabIndex = 23;
            this.label159.Text = "H02 IED RELAY FAULT ALARM";
            // 
            // label156
            // 
            this.label156.AutoSize = true;
            this.label156.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label156.Location = new System.Drawing.Point(40, 390);
            this.label156.Name = "label156";
            this.label156.Size = new System.Drawing.Size(234, 18);
            this.label156.TabIndex = 21;
            this.label156.Text = "H02 CB PRESSURE LEVEL2 ALARM";
            // 
            // label158
            // 
            this.label158.AutoSize = true;
            this.label158.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label158.Location = new System.Drawing.Point(40, 440);
            this.label158.Name = "label158";
            this.label158.Size = new System.Drawing.Size(278, 18);
            this.label158.TabIndex = 21;
            this.label158.Text = "H02 GCB PRESSURE LOW LATCH ALARM";
            // 
            // label157
            // 
            this.label157.AutoSize = true;
            this.label157.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label157.Location = new System.Drawing.Point(40, 415);
            this.label157.Name = "label157";
            this.label157.Size = new System.Drawing.Size(229, 18);
            this.label157.TabIndex = 21;
            this.label157.Text = "H02 GCB PRESSURE LOW ALARM";
            // 
            // label137
            // 
            this.label137.AutoSize = true;
            this.label137.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label137.Location = new System.Drawing.Point(40, 365);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(234, 18);
            this.label137.TabIndex = 21;
            this.label137.Text = "H02 CB PRESSURE LEVEL1 ALARM";
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label136.Location = new System.Drawing.Point(40, 340);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(86, 18);
            this.label136.TabIndex = 21;
            this.label136.Text = "H02 CB OFF";
            // 
            // label135
            // 
            this.label135.AutoSize = true;
            this.label135.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label135.Location = new System.Drawing.Point(40, 315);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(83, 18);
            this.label135.TabIndex = 21;
            this.label135.Text = "H02 CB ON";
            // 
            // label134
            // 
            this.label134.AutoSize = true;
            this.label134.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label134.Location = new System.Drawing.Point(40, 290);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(157, 18);
            this.label134.TabIndex = 21;
            this.label134.Text = "H02 GIS LOCAL MODE";
            // 
            // label133
            // 
            this.label133.AutoSize = true;
            this.label133.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label133.Location = new System.Drawing.Point(40, 265);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(171, 18);
            this.label133.TabIndex = 19;
            this.label133.Text = "H02 GIS REMOTE MODE";
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label132.Location = new System.Drawing.Point(40, 240);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(144, 18);
            this.label132.TabIndex = 17;
            this.label132.Text = "H02 MCCB OFF/TRIP";
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label131.Location = new System.Drawing.Point(40, 215);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(235, 18);
            this.label131.TabIndex = 15;
            this.label131.Text = "H01 PT SECOND SIDE FUSE FAULT";
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label130.Location = new System.Drawing.Point(40, 190);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(232, 18);
            this.label130.TabIndex = 13;
            this.label130.Text = "H01 PT PRESSURE LEVEL2 ALARM";
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label129.Location = new System.Drawing.Point(40, 165);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(232, 18);
            this.label129.TabIndex = 11;
            this.label129.Text = "H01 PT PRESSURE LEVEL1 ALARM";
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label128.Location = new System.Drawing.Point(40, 140);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(301, 18);
            this.label128.TabIndex = 9;
            this.label128.Text = "H01 BUS AIRCELL PRESSURE LEVEL2 ALARM";
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label127.Location = new System.Drawing.Point(40, 115);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(301, 18);
            this.label127.TabIndex = 7;
            this.label127.Text = "H01 BUS AIRCELL PRESSURE LEVEL1 ALARM";
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label126.Location = new System.Drawing.Point(40, 90);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(144, 18);
            this.label126.TabIndex = 4;
            this.label126.Text = "H01 MCCB OFF/TRIP";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label38.Location = new System.Drawing.Point(40, 65);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(179, 18);
            this.label38.TabIndex = 2;
            this.label38.Text = "HVAS AC220V Power Loss";
            // 
            // label163
            // 
            this.label163.AutoSize = true;
            this.label163.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label163.Location = new System.Drawing.Point(40, 565);
            this.label163.Name = "label163";
            this.label163.Size = new System.Drawing.Size(139, 18);
            this.label163.TabIndex = 2;
            this.label163.Text = "H02 IED RELAY 51N";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label37.Location = new System.Drawing.Point(40, 40);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(128, 18);
            this.label37.TabIndex = 2;
            this.label37.Text = "HVAS Battery Low";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(5, 7);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(172, 28);
            this.label36.TabIndex = 0;
            this.label36.Text = "TIME STAMPED";
            // 
            // circleUC26
            // 
            this.circleUC26.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC26.ExtenderWidth = 3;
            this.circleUC26.Fill = false;
            this.circleUC26.GroupID = null;
            this.circleUC26.Location = new System.Drawing.Point(12, 565);
            this.circleUC26.Margin = new System.Windows.Forms.Padding(3145728, 34907757, 3145728, 34907757);
            this.circleUC26.Name = "circleUC26";
            this.circleUC26.Size = new System.Drawing.Size(15, 15);
            this.circleUC26.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC26.TabIndex = 26;
            // 
            // circleUC25
            // 
            this.circleUC25.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC25.ExtenderWidth = 3;
            this.circleUC25.Fill = false;
            this.circleUC25.GroupID = null;
            this.circleUC25.Location = new System.Drawing.Point(12, 540);
            this.circleUC25.Margin = new System.Windows.Forms.Padding(1572864, 15514558, 1572864, 15514558);
            this.circleUC25.Name = "circleUC25";
            this.circleUC25.Size = new System.Drawing.Size(15, 15);
            this.circleUC25.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC25.TabIndex = 26;
            // 
            // circleUC24
            // 
            this.circleUC24.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC24.ExtenderWidth = 3;
            this.circleUC24.Fill = false;
            this.circleUC24.GroupID = null;
            this.circleUC24.Location = new System.Drawing.Point(12, 515);
            this.circleUC24.Margin = new System.Windows.Forms.Padding(786432, 6895359, 786432, 6895359);
            this.circleUC24.Name = "circleUC24";
            this.circleUC24.Size = new System.Drawing.Size(15, 15);
            this.circleUC24.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC24.TabIndex = 26;
            // 
            // circleUC23
            // 
            this.circleUC23.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC23.ExtenderWidth = 3;
            this.circleUC23.Fill = false;
            this.circleUC23.GroupID = null;
            this.circleUC23.Location = new System.Drawing.Point(12, 490);
            this.circleUC23.Margin = new System.Windows.Forms.Padding(393216, 3064604, 393216, 3064604);
            this.circleUC23.Name = "circleUC23";
            this.circleUC23.Size = new System.Drawing.Size(15, 15);
            this.circleUC23.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC23.TabIndex = 24;
            // 
            // circleUC22
            // 
            this.circleUC22.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC22.ExtenderWidth = 3;
            this.circleUC22.Fill = false;
            this.circleUC22.GroupID = null;
            this.circleUC22.Location = new System.Drawing.Point(12, 465);
            this.circleUC22.Margin = new System.Windows.Forms.Padding(196608, 1362046, 196608, 1362046);
            this.circleUC22.Name = "circleUC22";
            this.circleUC22.Size = new System.Drawing.Size(15, 15);
            this.circleUC22.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC22.TabIndex = 22;
            // 
            // circleUC21
            // 
            this.circleUC21.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC21.ExtenderWidth = 3;
            this.circleUC21.Fill = false;
            this.circleUC21.GroupID = null;
            this.circleUC21.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.circleUC21.Location = new System.Drawing.Point(12, 440);
            this.circleUC21.Margin = new System.Windows.Forms.Padding(98304, 605354, 98304, 605354);
            this.circleUC21.Name = "circleUC21";
            this.circleUC21.Size = new System.Drawing.Size(15, 15);
            this.circleUC21.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC21.TabIndex = 20;
            // 
            // circleUC20
            // 
            this.circleUC20.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC20.ExtenderWidth = 3;
            this.circleUC20.Fill = false;
            this.circleUC20.GroupID = null;
            this.circleUC20.Location = new System.Drawing.Point(12, 415);
            this.circleUC20.Margin = new System.Windows.Forms.Padding(49152, 269046, 49152, 269046);
            this.circleUC20.Name = "circleUC20";
            this.circleUC20.Size = new System.Drawing.Size(15, 15);
            this.circleUC20.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC20.TabIndex = 20;
            // 
            // circleUC19
            // 
            this.circleUC19.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC19.ExtenderWidth = 3;
            this.circleUC19.Fill = false;
            this.circleUC19.GroupID = null;
            this.circleUC19.Location = new System.Drawing.Point(12, 390);
            this.circleUC19.Margin = new System.Windows.Forms.Padding(24576, 119576, 24576, 119576);
            this.circleUC19.Name = "circleUC19";
            this.circleUC19.Size = new System.Drawing.Size(15, 15);
            this.circleUC19.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC19.TabIndex = 20;
            // 
            // circleUC18
            // 
            this.circleUC18.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC18.ExtenderWidth = 3;
            this.circleUC18.Fill = false;
            this.circleUC18.GroupID = null;
            this.circleUC18.Location = new System.Drawing.Point(12, 365);
            this.circleUC18.Margin = new System.Windows.Forms.Padding(12288, 53145, 12288, 53145);
            this.circleUC18.Name = "circleUC18";
            this.circleUC18.Size = new System.Drawing.Size(15, 15);
            this.circleUC18.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC18.TabIndex = 20;
            // 
            // circleUC17
            // 
            this.circleUC17.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC17.ExtenderWidth = 3;
            this.circleUC17.Fill = false;
            this.circleUC17.GroupID = null;
            this.circleUC17.Location = new System.Drawing.Point(12, 340);
            this.circleUC17.Margin = new System.Windows.Forms.Padding(6144, 23620, 6144, 23620);
            this.circleUC17.Name = "circleUC17";
            this.circleUC17.Size = new System.Drawing.Size(15, 15);
            this.circleUC17.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC17.TabIndex = 20;
            // 
            // circleUC16
            // 
            this.circleUC16.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC16.ExtenderWidth = 3;
            this.circleUC16.Fill = false;
            this.circleUC16.GroupID = null;
            this.circleUC16.Location = new System.Drawing.Point(12, 315);
            this.circleUC16.Margin = new System.Windows.Forms.Padding(3072, 10498, 3072, 10498);
            this.circleUC16.Name = "circleUC16";
            this.circleUC16.Size = new System.Drawing.Size(15, 15);
            this.circleUC16.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC16.TabIndex = 20;
            // 
            // circleUC15
            // 
            this.circleUC15.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC15.ExtenderWidth = 3;
            this.circleUC15.Fill = false;
            this.circleUC15.GroupID = null;
            this.circleUC15.Location = new System.Drawing.Point(12, 290);
            this.circleUC15.Margin = new System.Windows.Forms.Padding(1536, 4666, 1536, 4666);
            this.circleUC15.Name = "circleUC15";
            this.circleUC15.Size = new System.Drawing.Size(15, 15);
            this.circleUC15.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC15.TabIndex = 20;
            // 
            // circleUC14
            // 
            this.circleUC14.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC14.ExtenderWidth = 3;
            this.circleUC14.Fill = false;
            this.circleUC14.GroupID = null;
            this.circleUC14.Location = new System.Drawing.Point(12, 265);
            this.circleUC14.Margin = new System.Windows.Forms.Padding(768, 2074, 768, 2074);
            this.circleUC14.Name = "circleUC14";
            this.circleUC14.Size = new System.Drawing.Size(15, 15);
            this.circleUC14.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC14.TabIndex = 18;
            // 
            // circleUC13
            // 
            this.circleUC13.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC13.ExtenderWidth = 3;
            this.circleUC13.Fill = false;
            this.circleUC13.GroupID = null;
            this.circleUC13.Location = new System.Drawing.Point(12, 240);
            this.circleUC13.Margin = new System.Windows.Forms.Padding(384, 922, 384, 922);
            this.circleUC13.Name = "circleUC13";
            this.circleUC13.Size = new System.Drawing.Size(15, 15);
            this.circleUC13.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC13.TabIndex = 16;
            // 
            // circleUC12
            // 
            this.circleUC12.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC12.ExtenderWidth = 3;
            this.circleUC12.Fill = false;
            this.circleUC12.GroupID = null;
            this.circleUC12.Location = new System.Drawing.Point(12, 215);
            this.circleUC12.Margin = new System.Windows.Forms.Padding(192, 410, 192, 410);
            this.circleUC12.Name = "circleUC12";
            this.circleUC12.Size = new System.Drawing.Size(15, 15);
            this.circleUC12.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC12.TabIndex = 14;
            // 
            // circleUC11
            // 
            this.circleUC11.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC11.ExtenderWidth = 3;
            this.circleUC11.Fill = false;
            this.circleUC11.GroupID = null;
            this.circleUC11.Location = new System.Drawing.Point(12, 190);
            this.circleUC11.Margin = new System.Windows.Forms.Padding(96, 182, 96, 182);
            this.circleUC11.Name = "circleUC11";
            this.circleUC11.Size = new System.Drawing.Size(15, 15);
            this.circleUC11.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC11.TabIndex = 12;
            // 
            // circleUC10
            // 
            this.circleUC10.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC10.ExtenderWidth = 3;
            this.circleUC10.Fill = false;
            this.circleUC10.GroupID = null;
            this.circleUC10.Location = new System.Drawing.Point(12, 165);
            this.circleUC10.Margin = new System.Windows.Forms.Padding(48, 81, 48, 81);
            this.circleUC10.Name = "circleUC10";
            this.circleUC10.Size = new System.Drawing.Size(15, 15);
            this.circleUC10.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC10.TabIndex = 10;
            // 
            // circleUC9
            // 
            this.circleUC9.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC9.ExtenderWidth = 3;
            this.circleUC9.Fill = false;
            this.circleUC9.GroupID = null;
            this.circleUC9.Location = new System.Drawing.Point(12, 140);
            this.circleUC9.Margin = new System.Windows.Forms.Padding(48, 81, 48, 81);
            this.circleUC9.Name = "circleUC9";
            this.circleUC9.Size = new System.Drawing.Size(15, 15);
            this.circleUC9.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC9.TabIndex = 8;
            // 
            // circleUC8
            // 
            this.circleUC8.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC8.ExtenderWidth = 3;
            this.circleUC8.Fill = false;
            this.circleUC8.GroupID = null;
            this.circleUC8.Location = new System.Drawing.Point(12, 115);
            this.circleUC8.Margin = new System.Windows.Forms.Padding(24, 36, 24, 36);
            this.circleUC8.Name = "circleUC8";
            this.circleUC8.Size = new System.Drawing.Size(15, 15);
            this.circleUC8.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC8.TabIndex = 6;
            // 
            // circleUC7
            // 
            this.circleUC7.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC7.ExtenderWidth = 3;
            this.circleUC7.Fill = false;
            this.circleUC7.GroupID = null;
            this.circleUC7.Location = new System.Drawing.Point(12, 90);
            this.circleUC7.Margin = new System.Windows.Forms.Padding(12, 16, 12, 16);
            this.circleUC7.Name = "circleUC7";
            this.circleUC7.Size = new System.Drawing.Size(15, 15);
            this.circleUC7.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC7.TabIndex = 5;
            // 
            // circleUC6
            // 
            this.circleUC6.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC6.ExtenderWidth = 3;
            this.circleUC6.Fill = false;
            this.circleUC6.GroupID = null;
            this.circleUC6.Location = new System.Drawing.Point(12, 65);
            this.circleUC6.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.circleUC6.Name = "circleUC6";
            this.circleUC6.Size = new System.Drawing.Size(15, 15);
            this.circleUC6.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC6.TabIndex = 3;
            // 
            // circleUC5
            // 
            this.circleUC5.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC5.ExtenderWidth = 3;
            this.circleUC5.Fill = false;
            this.circleUC5.GroupID = null;
            this.circleUC5.Location = new System.Drawing.Point(12, 40);
            this.circleUC5.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.circleUC5.Name = "circleUC5";
            this.circleUC5.Size = new System.Drawing.Size(15, 15);
            this.circleUC5.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC5.TabIndex = 1;
            // 
            // Form_ElecTSS7
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(1910, 655);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form_ElecTSS7";
            this.Text = "Form_ElecDetail1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_ElecTSS7_FormClosing);
            this.Load += new System.EventHandler(this.Form_ElecTSS7_Load);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label35;
        private iSCADA.Design.Utilities.Electrical.Switch05UC switch05UC1;
        private iSCADA.Design.Utilities.Electrical.Switch03UC switch03UC3;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC14;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC13;
        private iSCADA.Design.Utilities.Electrical.Switch05UC switch05UC3;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private iSCADA.Design.Utilities.Electrical.Switch05UC switch05UC2;
        private iSCADA.Design.Utilities.Electrical.Switch03UC switch03UC2;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC4;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC3;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC2;
        private iSCADA.Design.Utilities.Electrical.AirCircuitBreaker airCircuitBreaker4;
        private iSCADA.Design.Utilities.Electrical.AirCircuitBreaker airCircuitBreaker3;
        private iSCADA.Design.Utilities.Electrical.AirCircuitBreaker airCircuitBreaker2;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC10;
        private iSCADA.Design.Utilities.Electrical.AirCircuitBreaker airCircuitBreaker1;
        private iSCADA.Design.Utilities.Electrical.Transformer02UC transformer02UC1;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC1;
        private System.Windows.Forms.Label label17;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC12;
        private System.Windows.Forms.Label label20;
        private iSCADA.Design.Utilities.Electrical.Transformer01UC transformer01UC1;
        private System.Windows.Forms.Label label19;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC11;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC6;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC5;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC7;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC4;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC6;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC4;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC3;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC2;
        private System.Windows.Forms.Label label2;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC2;
        private System.Windows.Forms.Label label1;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC1;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC3;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC7;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC8;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC9;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC10;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC11;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC12;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label36;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC6;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC5;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC71;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC72;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC77;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC78;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC83;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC84;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC86;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC87;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC89;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC90;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC92;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC93;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC95;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC96;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC98;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC99;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC101;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC102;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC69;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC70;
        private System.Windows.Forms.Label label48;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC75;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC76;
        private System.Windows.Forms.Label label49;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC81;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC82;
        private System.Windows.Forms.Label label50;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC33;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC34;
        private System.Windows.Forms.Label label42;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC39;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC40;
        private System.Windows.Forms.Label label43;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC45;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC46;
        private System.Windows.Forms.Label label44;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC27;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC28;
        private System.Windows.Forms.Label label41;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC21;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC22;
        private System.Windows.Forms.Label label40;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC17;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC18;
        private System.Windows.Forms.Label label39;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC14;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC13;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label138;
        private System.Windows.Forms.Label label139;
        private System.Windows.Forms.Label label140;
        private System.Windows.Forms.Label label141;
        private System.Windows.Forms.Label label142;
        private System.Windows.Forms.Label label143;
        private System.Windows.Forms.Label label144;
        private System.Windows.Forms.Label label145;
        private System.Windows.Forms.Label label146;
        private System.Windows.Forms.Label label147;
        private System.Windows.Forms.Label label148;
        private System.Windows.Forms.Label label149;
        private System.Windows.Forms.Label label150;
        private System.Windows.Forms.Label label151;
        private System.Windows.Forms.Label label152;
        private System.Windows.Forms.Label label153;
        private System.Windows.Forms.Label label154;
        private System.Windows.Forms.Label label155;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.Label label119;
        private System.Windows.Forms.Label label120;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.Label label122;
        private System.Windows.Forms.Label label123;
        private System.Windows.Forms.Label label124;
        private System.Windows.Forms.Label label125;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label129;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC10;
        private System.Windows.Forms.Label label128;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC9;
        private System.Windows.Forms.Label label127;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC8;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC7;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.Label label131;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC12;
        private System.Windows.Forms.Label label130;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC11;
        private System.Windows.Forms.Label label134;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC15;
        private System.Windows.Forms.Label label133;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC14;
        private System.Windows.Forms.Label label132;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC13;
        private System.Windows.Forms.Label label159;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC22;
        private System.Windows.Forms.Label label156;
        private System.Windows.Forms.Label label158;
        private System.Windows.Forms.Label label157;
        private System.Windows.Forms.Label label137;
        private System.Windows.Forms.Label label136;
        private System.Windows.Forms.Label label135;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC21;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC20;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC19;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC18;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC17;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC16;
        private System.Windows.Forms.Label label164;
        private System.Windows.Forms.Label label165;
        private System.Windows.Forms.Label label166;
        private System.Windows.Forms.Label label167;
        private System.Windows.Forms.Label label168;
        private System.Windows.Forms.Label label169;
        private System.Windows.Forms.Label label170;
        private System.Windows.Forms.Label label171;
        private System.Windows.Forms.Label label172;
        private System.Windows.Forms.Label label173;
        private System.Windows.Forms.Label label174;
        private System.Windows.Forms.Label label175;
        private System.Windows.Forms.Label label176;
        private System.Windows.Forms.Label label177;
        private System.Windows.Forms.Label label178;
        private System.Windows.Forms.Label label179;
        private System.Windows.Forms.Label label180;
        private System.Windows.Forms.Label label181;
        private System.Windows.Forms.Label label182;
        private System.Windows.Forms.Label label183;
        private System.Windows.Forms.Label label162;
        private System.Windows.Forms.Label label161;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC25;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC24;
        private System.Windows.Forms.Label label160;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC23;
        private System.Windows.Forms.Label label163;
        private System.Windows.Forms.Label label184;
        private System.Windows.Forms.Label label185;
        private System.Windows.Forms.Label label186;
        private System.Windows.Forms.Label label187;
        private System.Windows.Forms.Label label188;
        private System.Windows.Forms.Label label189;
        private System.Windows.Forms.Label label190;
        private System.Windows.Forms.Label label191;
        private System.Windows.Forms.Label label192;
        private System.Windows.Forms.Label label193;
        private System.Windows.Forms.Label label194;
        private System.Windows.Forms.Label label195;
        private System.Windows.Forms.Label label196;
        private System.Windows.Forms.Label label197;
        private System.Windows.Forms.Label label198;
        private System.Windows.Forms.Label label199;
        private System.Windows.Forms.Label label200;
        private System.Windows.Forms.Label label201;
        private System.Windows.Forms.Label label202;
        private System.Windows.Forms.Label label203;
        private System.Windows.Forms.Label label204;
        private System.Windows.Forms.Label label226;
        private System.Windows.Forms.Label label227;
        private System.Windows.Forms.Label label228;
        private System.Windows.Forms.Label label229;
        private System.Windows.Forms.Label label230;
        private System.Windows.Forms.Label label231;
        private System.Windows.Forms.Label label232;
        private System.Windows.Forms.Label label233;
        private System.Windows.Forms.Label label234;
        private System.Windows.Forms.Label label235;
        private System.Windows.Forms.Label label236;
        private System.Windows.Forms.Label label237;
        private System.Windows.Forms.Label label238;
        private System.Windows.Forms.Label label239;
        private System.Windows.Forms.Label label240;
        private System.Windows.Forms.Label label241;
        private System.Windows.Forms.Label label242;
        private System.Windows.Forms.Label label243;
        private System.Windows.Forms.Label label244;
        private System.Windows.Forms.Label label245;
        private System.Windows.Forms.Label label246;
        private System.Windows.Forms.Label label205;
        private System.Windows.Forms.Label label206;
        private System.Windows.Forms.Label label207;
        private System.Windows.Forms.Label label208;
        private System.Windows.Forms.Label label209;
        private System.Windows.Forms.Label label210;
        private System.Windows.Forms.Label label211;
        private System.Windows.Forms.Label label212;
        private System.Windows.Forms.Label label213;
        private System.Windows.Forms.Label label214;
        private System.Windows.Forms.Label label215;
        private System.Windows.Forms.Label label216;
        private System.Windows.Forms.Label label217;
        private System.Windows.Forms.Label label218;
        private System.Windows.Forms.Label label219;
        private System.Windows.Forms.Label label220;
        private System.Windows.Forms.Label label221;
        private System.Windows.Forms.Label label222;
        private System.Windows.Forms.Label label223;
        private System.Windows.Forms.Label label224;
        private System.Windows.Forms.Label label225;
        private System.Windows.Forms.Label label247;
        private System.Windows.Forms.Label label248;
        private System.Windows.Forms.Label label249;
        private System.Windows.Forms.Label label250;
        private System.Windows.Forms.Label label251;
        private System.Windows.Forms.Label label252;
        private System.Windows.Forms.Label label253;
        private System.Windows.Forms.Label label254;
        private System.Windows.Forms.Label label255;
        private System.Windows.Forms.Label label256;
        private System.Windows.Forms.Label label257;
        private System.Windows.Forms.Label label258;
        private System.Windows.Forms.Label label259;
        private System.Windows.Forms.Label label260;
        private System.Windows.Forms.Label label261;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC34;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC33;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC32;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC31;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC30;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC29;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC28;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC27;
        private System.Windows.Forms.Label label262;
        private System.Windows.Forms.Label label266;
        private System.Windows.Forms.Label label267;
        private System.Windows.Forms.Label label268;
        private System.Windows.Forms.Label label269;
        private System.Windows.Forms.Label label270;
        private System.Windows.Forms.Label label271;
        private System.Windows.Forms.Label label272;
        private System.Windows.Forms.Label label273;
        private System.Windows.Forms.Label label274;
        private System.Windows.Forms.Label label275;
        private System.Windows.Forms.Label label276;
        private System.Windows.Forms.Label label277;
        private System.Windows.Forms.Label label278;
        private System.Windows.Forms.Label label279;
        private System.Windows.Forms.Label label280;
        private System.Windows.Forms.Label label281;
        private System.Windows.Forms.Label label282;
        private System.Windows.Forms.Label label283;
        private System.Windows.Forms.Label label284;
        private System.Windows.Forms.Label label285;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC26;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC35;
        private System.Windows.Forms.Button btnMC81DISCONECTOROff;
        private System.Windows.Forms.Button btnMC41LBSOff;
        private System.Windows.Forms.Button btnMC22DISCONECTOROff;
        private System.Windows.Forms.Button btnMC22CBOff;
        private System.Windows.Forms.Button btnMC21DISCONECTOROff;
        private System.Windows.Forms.Button btnMC21CBOff;
        private System.Windows.Forms.Button btnH04GISOff;
        private System.Windows.Forms.Button btnH03GISOff;
        private System.Windows.Forms.Button btnMC81DISCONECTOROn;
        private System.Windows.Forms.Button btnMC41LBSOn;
        private System.Windows.Forms.Button btnMC22DISCONECTOROn;
        private System.Windows.Forms.Button btnMC22CBOn;
        private System.Windows.Forms.Button btnMC21DISCONECTOROn;
        private System.Windows.Forms.Button btnMC21CBOn;
        private System.Windows.Forms.Button btnH04GISOn;
        private System.Windows.Forms.Button btnH03GISOn;
        private System.Windows.Forms.Button btnH02GISOff;
        private System.Windows.Forms.Button btnH02GISOn;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC37;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC36;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC44;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC45;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC46;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC47;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC48;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC49;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC50;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC51;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC52;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC53;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC38;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC39;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC40;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC41;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC42;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC43;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC65;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC55;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC56;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC57;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC58;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC59;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC60;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC61;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC62;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC63;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC64;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC66;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC70;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC71;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC72;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC73;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC74;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC75;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC76;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC77;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC78;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC79;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC80;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC81;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC82;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC96;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC97;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC98;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC99;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC84;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC85;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC86;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC87;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC88;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC89;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC90;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC91;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC92;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC93;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC94;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC95;
        private iSCADA.Design.Utilities.Electrical.Switch09UC switch09UC2;
        private iSCADA.Design.Utilities.Electrical.Switch09UC switch09UC1;
        private iSCADA.Design.Utilities.Electrical.Switch07UC switch07UC1;
        private iSCADA.Design.Utilities.Electrical.Switch07UC switch07UC2;
        private iSCADA.Design.Utilities.Electrical.Switch07UC switch07UC3;
        private iSCADA.Design.Utilities.Electrical.Switch06UC switch06UC1;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC9;
        private iSCADA.Design.Utilities.Electrical.Switch07UC switch07UC4;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private iSCADA.Design.Utilities.Electrical.Switch08UC switch08UC1;
        private System.Windows.Forms.Label label45;
        private iSCADA.Design.Utilities.Electrical.Switch05UC switch05UC4;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC20;
        private System.Windows.Forms.Label label46;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC16;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC15;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC17;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC18;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC19;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC23;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC22;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC21;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC1;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC101;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC100;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC69;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC83;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC68;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC67;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC54;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC102;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC123;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC122;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC121;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC120;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC119;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC118;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC117;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC116;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC115;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC114;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC113;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC112;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC111;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC110;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC109;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC108;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC107;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC106;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC105;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC104;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC103;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label47;
    }
}