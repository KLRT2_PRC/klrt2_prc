﻿using ICSC.Database.MSSQL;
using PRC.MyClass;
using PRC.Tools;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PRC
{
    public partial class Form_DeptMgt : Form
    {
        public Form_DeptMgt()
        {
            InitializeComponent();
            ComboBoxItem item = new ComboBoxItem();
            item.Text = "停用";
            item.Value = 0;
            cmbDeptPrivilege.Items.Add(item);
            item = new ComboBoxItem();
            item.Text = "啟用";
            item.Value = 1;
            cmbDeptPrivilege.Items.Add(item);
            //item = new CheckBoxItem();
            //item.Text = "刪除留存";
            //item.Value = 2;
            //cmbUserPrivilege.Items.Add(item);
            cmbDeptPrivilege.SelectedIndex = 1;
        }

        public Form_DeptMgt(PermissionObject permission):this()
        {
            btnDeptI.Enabled = permission.CanCreate;
            btnDeptU.Enabled = permission.CanUpdate;
            btnDeptD.Enabled = permission.CanDelete;
        }

        private void Form_Dept_Load(object sender, EventArgs e)
        {
            gridDept.DataSource = getData();
        
        }

        private void getDeptInfo()
        {

            List<Dept> deptpData = WebApiCaller.Json_GetModeByGet<List<Dept>>(@"http://localhost:48965/api/Dept").ContentBody;
            gridDept.DataSource = deptpData;

        }
        private DataTable getData()
        {
            StringBuilder logSB = new StringBuilder();
            StringBuilder eventSB = new StringBuilder();
            string tempMsg = string.Empty;
            StringBuilder sqlSB = new StringBuilder();
            string sqlCmd = string.Empty;
            DataTable dt = new DataTable();
            int sts = 0;
            try
            {

                sqlSB.Append("SELECT * FROM Sys_DeptID where DeptPrivilege <> 2");

                sqlCmd = sqlSB.ToString();

                sts = DbOp.QuerySql(SHM.DBIp, SHM.DBName, SHM.DBUser, SHM.DBPasswd, ref sqlCmd, ref dt, SHM.LOG_LEVEL_DBDetail, ref tempMsg);

                logSB.AppendLine(tempMsg);



            }
            catch (Exception ex)
            {
                eventSB.AppendLine("Exception: " + ex.Message);
            }
            finally
            {
                if (eventSB.Length > 0)
                    SHM.QueueEvent.Enqueue(eventSB.ToString());
                if (logSB.Length > 0)
                    SHM.QueueDatabase.Enqueue(logSB.ToString());
            }
            return dt;
        }

        private void btnDeptI_Click(object sender, EventArgs e)
        {
            #region "舊"
            //StringBuilder logSB = new StringBuilder();
            //StringBuilder eventSB = new StringBuilder();
            //string tempMsg = string.Empty;
            //StringBuilder sqlSB = new StringBuilder();
            //string sqlCmd = string.Empty;

            //int sts = 0;
            //try
            //{
            //    ComboBoxItem item = (ComboBoxItem)cmbDeptPrivilege.SelectedItem;

            //    sqlSB.Append("INSERT INTO Sys_DeptID (DeptID, DeptPrivilege, UpdateUserID, InsertUserID, InsertTime, UpdateTime, Description ) ");
            //    sqlSB.Append(" VALUES (");
            //    sqlSB.Append("'" + txbDeptID.Text + "',");

            //    sqlSB.Append("'" + item.Value.ToString() + "',");
            //    sqlSB.Append("'" + SHM.Staff + "',");
            //    sqlSB.Append("'" + SHM.Staff + "',");
            //    sqlSB.Append("'" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "',");
            //    sqlSB.Append("'" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "',");
            //    sqlSB.Append("'" + txbDesc.Text + "') ;");

            //    sqlCmd = sqlSB.ToString();

            //    sts = DbOp.ExecuteSql(SHM.DBIp, SHM.DBName, SHM.DBUser, SHM.DBPasswd, ref sqlCmd, SHM.LOG_LEVEL_DBDetail, ref tempMsg);

            //    if (sts > 0)
            //    {
            //        MessageBox.Show("新增成功");                    
            //        gridDept.DataSource = getData();
            //    }

            //    logSB.AppendLine(tempMsg);



            //}
            //catch (Exception ex)
            //{
            //    eventSB.AppendLine("Exception: " + ex.Message);
            //}
            //finally
            //{
            //    if (eventSB.Length > 0)
            //        SHM.QueueEvent.Enqueue(eventSB.ToString());
            //    if (logSB.Length > 0)
            //        SHM.QueueDatabase.Enqueue(logSB.ToString());
            //}
            //clearInput();
            #endregion
            ComboBoxItem item = (ComboBoxItem)cmbDeptPrivilege.SelectedItem;
            Dept dept = new Dept();
            dept.DeptID = txbDeptID.Text;
            dept.DeptPrivilege = Convert.ToInt32(item.Value);
            dept.Description = txbDesc.Text;
            dept.InsertUserID = SHM.user[0].UserID;
            dept.InsertTime = DateTime.Now;
            dept.UpdateUserID = SHM.user[0].UserID;
            dept.UpdateTime = DateTime.Now;
            InsertData(dept);
            clearInput();
        }

        private void InsertData(Dept dept)
        {
            var response = WebApiCaller.Srting_GetMessageByPost(@"http://localhost:48965/api/Dept", dept);

            if (response.IsSuccessStatusCode)
            {
                getDeptInfo();
            }

            MessageBox.Show(response.ContentBody);
        }


        private void btnDeptU_Click(object sender, EventArgs e)
        {
            #region "舊"
            //StringBuilder logSB = new StringBuilder();
            //StringBuilder eventSB = new StringBuilder();
            //string tempMsg = string.Empty;
            //StringBuilder sqlSB = new StringBuilder();
            //string sqlCmd = string.Empty;

            //int sts = 0;
            //try
            //{
            //    ComboBoxItem item = (ComboBoxItem)cmbDeptPrivilege.SelectedItem;

            //    sqlSB.Append("UPDATE Sys_DeptID SET ");
            //    sqlSB.Append(" DeptID = '" + txbDeptID.Text + "', ");
            //    sqlSB.Append(" DeptPrivilege = '" + item.Value.ToString() + "', ");
            //    sqlSB.Append(" Description = '" + txbDesc.Text + "' ,");
            //    sqlSB.Append(" UpdateUserID = '" + SHM.Staff + "' ,");
            //    sqlSB.Append(" UpdateTime = '" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "'");

            //    sqlSB.Append(" WHERE SerialNo = '" + lblSerialNo.Text + "'");

            //    sqlCmd = sqlSB.ToString();

            //    sts = DbOp.ExecuteSql(SHM.DBIp, SHM.DBName, SHM.DBUser, SHM.DBPasswd, ref sqlCmd, SHM.LOG_LEVEL_DBDetail, ref tempMsg);

            //    if (sts > 0)
            //    {
            //        MessageBox.Show("修改成功");
            //        gridDept.DataSource = getData();
            //    }

            //    logSB.AppendLine(tempMsg);



            //}
            //catch (Exception ex)
            //{
            //    eventSB.AppendLine("Exception: " + ex.Message);
            //}
            //finally
            //{
            //    if (eventSB.Length > 0)
            //        SHM.QueueEvent.Enqueue(eventSB.ToString());
            //    if (logSB.Length > 0)
            //        SHM.QueueDatabase.Enqueue(logSB.ToString());
            //}
            //clearInput();
            #endregion

            ComboBoxItem item = (ComboBoxItem)cmbDeptPrivilege.SelectedItem;
            Dept dept = new Dept();
            dept.DeptID = txbDeptID.Text;
            dept.DeptPrivilege = Convert.ToInt32(item.Value);
            dept.Description = txbDesc.Text;

            dept.UpdateUserID = SHM.user[0].UserID;
            dept.UpdateTime = DateTime.Now;
            UpdateData(dept);
            clearInput();
        }

        private  void UpdateData(Dept dept)
        {
            var result = WebApiCaller.Srting_GetMessageByPut(@"http://localhost:48965/api/Dept",dept);

            if (result.IsSuccessStatusCode)
            {
                getDeptInfo();
            }
            MessageBox.Show(result.ContentBody);
          
        }

        private void btnDeptD_Click(object sender, EventArgs e)
        {
            #region "舊"
            //StringBuilder logSB = new StringBuilder();
            //StringBuilder eventSB = new StringBuilder();
            //string tempMsg = string.Empty;
            //StringBuilder sqlSB = new StringBuilder();
            //string sqlCmd = string.Empty;

            //int sts = 0;
            //try
            //{
            //    ComboBoxItem item = (ComboBoxItem)cmbDeptPrivilege.SelectedItem;

            //    sqlSB.Append("UPDATE Sys_DeptID SET DeptPrivilege = '2' ,");
            //    sqlSB.Append(" UpdateUserID = '" + SHM.Staff + "' ,");
            //    sqlSB.Append(" RetireTime = '" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "' ,");
            //    sqlSB.Append(" UpdateTime = '" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "'");


            //    sqlSB.Append(" WHERE SerialNo = '" + lblSerialNo.Text + "'");

            //    sqlCmd = sqlSB.ToString();

            //    sts = DbOp.ExecuteSql(SHM.DBIp, SHM.DBName, SHM.DBUser, SHM.DBPasswd, ref sqlCmd, SHM.LOG_LEVEL_DBDetail, ref tempMsg);

            //    if (sts > 0)
            //    {
            //        MessageBox.Show("刪除成功");
            //        gridDept.DataSource = getData();
            //    }

            //    logSB.AppendLine(tempMsg);



            //}
            //catch (Exception ex)
            //{
            //    eventSB.AppendLine("Exception: " + ex.Message);
            //}
            //finally
            //{
            //    if (eventSB.Length > 0)
            //        SHM.QueueEvent.Enqueue(eventSB.ToString());
            //    if (logSB.Length > 0)
            //        SHM.QueueDatabase.Enqueue(logSB.ToString());
            //}
            //clearInput();
            #endregion

            if (MessageBox.Show("請問要刪除此部門嗎?","",MessageBoxButtons.YesNo,MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                ComboBoxItem item = (ComboBoxItem)cmbDeptPrivilege.SelectedItem;
                Dept dept = new Dept();
                dept.DeptID = txbDeptID.Text;
                dept.UpdateUserID = SHM.user[0].UserID;
                dept.RetireTime = DateTime.Now;
                DeleteData(dept);
                clearInput();
            }
     
        }

        private  void DeleteData(Dept dept)
        {
            var result = WebApiCaller.Srting_GetMessageByDelete(@"http://localhost:48965/api/Dept?DeptID=" + dept.DeptID);

            if (result.IsSuccessStatusCode)
            {
                getDeptInfo();
            }
            MessageBox.Show(result.ContentBody);
        }


        private void btnDeptC_Click(object sender, EventArgs e)
        {
            clearInput();
        }

        void clearInput()
        {
            txbDeptID.Text = "";
            txbDesc.Text = "";
        }

        private void gridDept_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            txbDeptID.Text = gridDept.Rows[e.RowIndex].Cells["DeptID"].Value.ToString();
            txbDesc.Text = string.Format("{0}", gridDept.Rows[e.RowIndex].Cells["Description"].Value);;
            cmbDeptPrivilege.SelectedIndex = (int)gridDept.Rows[e.RowIndex].Cells["DeptPrivilege"].Value;

        }

        private void button_EnabledChanged(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            if (btn.Enabled)
                btn.FlatStyle = FlatStyle.Flat;
            else
                btn.FlatStyle = FlatStyle.Standard;
        }



    
    }
}
