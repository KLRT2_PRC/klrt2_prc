﻿using PRC.MyClass;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PRC
{
    public partial class Form_ElecStationBase : Form
    {
        public Form_ElecStationBase()
        {
            InitializeComponent();
        }

        private void btnCheckedChanged(object sender, EventArgs e)
        {
            RadioButton rbtn = sender as RadioButton;
            if (rbtn.Checked)
            {
                switch (rbtn.Text)
                {
                    case "C15":
                        CreateMdiChildOrActive<Form_StationC15>();
                        break;
                    case "C16":
                        CreateMdiChildOrActive<Form_StationC16>();
                        break;
                    case "C17":
                        CreateMdiChildOrActive<Form_StationC17>();
                        break;
                }
            }
        }

        public T CreateMdiChildOrActive<T>(params object[] parameters) where T : Form
        {
            Form instance;
            MainFrame.DictAllForms.TryGetValue(typeof(T), out instance);
            if (instance == null || instance.IsDisposed)
            {
                instance = (Form)Activator.CreateInstance(typeof(T), parameters);
                MainFrame.DictAllForms[typeof(T)] = instance;
            }

            InitFormInstance(instance);

            return (T)instance;
        }
        private void InitFormInstance(Form instance)
        {
            pnl_Container.Controls.Clear();
            instance.TopLevel = false;
            pnl_Container.Controls.Add(instance);
            instance.WindowState = FormWindowState.Maximized;
            instance.Show();
        }
    }
}
