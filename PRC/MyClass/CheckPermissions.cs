﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using ICSC.Database.MSSQL;
using System.Data;
using System.Net.Http;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Net.Http.Headers;



namespace PRC.MyClass
{
    class CheckPermissions
    {
        public static bool CheckLogin(string UserID, string Password)
        {
            StringBuilder logSB = new StringBuilder();
            StringBuilder eventSB = new StringBuilder();
            string tempMsg = string.Empty;
            StringBuilder sqlSB = new StringBuilder();
            string sqlCmd = string.Empty;
            DataTable dt = new DataTable();
            int sts = 0;
            bool result = false;
            try
            {
                if (UserID.Trim().Equals("000000"))
                {
                    sqlSB.Append("SELECT * FROM Sys_User WHERE UserID = '" + UserID + "' AND UserPw ='" + Password + "'  and UserPrivilege = '1'");
                }
                else
                {
                    MD5 md5 = MD5.Create();//建立一個MD5
                    byte[] source = Encoding.Default.GetBytes(Password);//將字串轉為Byte[]
                    byte[] crypto = md5.ComputeHash(source);//進行MD5加密
                    string resultPw = Convert.ToBase64String(crypto);//把加密後的字串從Byte[]轉為字串
                    sqlSB.Append("SELECT * FROM Sys_User WHERE UserID = '" + UserID + "' AND UserPw ='" + resultPw + "' and UserPrivilege = '1'");
                }

                //sqlSB.Append("SELECT * FROM Sys_User WHERE UserID = '" + UserID + "' AND UserPw ='" + Password + "' and UserPrivilege = '1'");
                sqlCmd = sqlSB.ToString();

                sts = DbOp.QuerySql(SHM.DBIp, SHM.DBName, SHM.DBUser, SHM.DBPasswd, ref sqlCmd, ref dt, SHM.LOG_LEVEL_DBDetail, ref tempMsg);
                if (sts > 0)
                {
                    SHM.Staff = dt.Rows[0]["UserID"].ToString();
                    result = true;
                }

                logSB.AppendLine(tempMsg);



            }
            catch (Exception ex)
            {
                eventSB.AppendLine("Exception: " + ex.Message);
            }
            finally
            {
                if (eventSB.Length > 0)
                    SHM.QueueEvent.Enqueue(eventSB.ToString());
                if (logSB.Length > 0)
                    SHM.QueueDatabase.Enqueue(logSB.ToString());
            }
            return result;
        }

        public static DataTable VaildPermissions(string UserID)
        {
            StringBuilder logSB = new StringBuilder();
            StringBuilder eventSB = new StringBuilder();
            string tempMsg = string.Empty;
            StringBuilder sqlSB = new StringBuilder();
            string sqlCmd = string.Empty;
            DataTable dt = new DataTable();
            int sts = 0;
            try
            {
              

                sqlSB.Append("select * from Sys_GroupToUser u join Sys_GroupToPrograms p ");
                sqlSB.Append("on u.GroupID = p.GroupID where u.UserID = '" + UserID + "' order by u.GroupID");
                sqlCmd = sqlSB.ToString();

                sts = DbOp.QuerySql(SHM.DBIp, SHM.DBName, SHM.DBUser, SHM.DBPasswd, ref sqlCmd, ref dt, SHM.LOG_LEVEL_DBDetail, ref tempMsg);
                if (sts > 0)
                {
                    
                }

                logSB.AppendLine(tempMsg);


            }
            catch (Exception ex)
            {
                eventSB.AppendLine("Exception: " + ex.Message);
            }
            finally
            {
                if (eventSB.Length > 0)
                    SHM.QueueEvent.Enqueue(eventSB.ToString());
                if (logSB.Length > 0)
                    SHM.QueueDatabase.Enqueue(logSB.ToString());
            }
            return dt;
        }

        public static async void GetUserInfo(string userID,string userPw)
        {
            HttpClient client = new HttpClient();
            string uritmp = "http://localhost:48965/api/UserInfo?UserID=" + userID + "&UserPw=" + userPw;
            Uri uri = new Uri(uritmp);
            
            HttpResponseMessage response = await client.GetAsync(uri);
            response.EnsureSuccessStatusCode();
            string responseBody = await response.Content.ReadAsStringAsync();
            List<UserInfos> temp = JsonConvert.DeserializeObject<List<UserInfos>>(responseBody);
            SHM.user = temp;
        }

        public static async Task<UserInfos> GetUser(string userID,string userPw)
        {
            HttpClient client = new HttpClient();
            UserInfos info = null;
            string uritmp = "http://localhost:48965/api/UserInfo?UserID=" + userID + "&UserPw=" + userPw;
            HttpResponseMessage response = await client.GetAsync(uritmp);
            if(response.IsSuccessStatusCode)
            {
                info = await response.Content.ReadAsAsync<UserInfos>();
            }
            return info;
        }
    }
    

    public static class UserInfo
    {
        public static string UserID = "";

        public static List<string> GroupName = new List<string>();

        //public static List<Dictionary<string, bool>> Permissions = new List<Dictionary<string, bool>>();

        public static Dictionary<string, Dictionary<string, bool>> Permissions = new Dictionary<string, Dictionary<string, bool>>();
    }

    public class UserInfos
    {
        public bool isUser { get; set; }
        public string UserID { get; set; }
        public string UserPw { get; set; }

        public List<GroupInfo> GroupInfo = new List<GroupInfo>();
        public Dictionary<string, Dictionary<string, bool>> Permissions = new Dictionary<string, Dictionary<string, bool>>();
        
    }

    public class GroupInfo
    {
        public string GroupID { get; set; }
        public string ProgID_En { get; set; }
        public string ProgID_Cht { get; set; }
        public bool DisplayFun { get; set; }
        public bool SearchFun { get; set; }
        public bool InsertFun { get; set; }
        public bool DeleteFun { get; set; }
        public bool UpdateFun { get; set; }
        public string UserID { get; set; }
        public string DeptID { get; set; }
        public string UserName { get; set; }
        public string GroupName_Cht { get; set; }
        public string Description { get; set; }
    }


}
