﻿using ICSC.SysUtil.Log;
using ICSC.SysUtil.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Windows.Forms;

namespace PRC.MyClass
{
    class SHM
    {
        #region "Database Settings"
        /// <summary>
        /// Database Server IP
        /// </summary>
        public static string DBIp = @".";
        /// <summary>
        /// Database Name
        /// </summary>
        public static string DBName = "KLRT2_PRC";
        /// <summary>
        /// Database User Account
        /// </summary>
        public static string DBUser = "sa";
        /// <summary>
        /// Database User passwd
        /// </summary>
        public static string DBPasswd = "as";
        #endregion

        #region "Log Level Flag"
        public static bool LOG_LEVEL_ENABLE = true;
        public static bool LOG_LEVEL_DBDetail = false;
        public static bool LOG_LEVEL_DBACT = true;
        public static bool LOG_LEVEL_DEBUG = false;
        public static bool LOG_LEVEL_DEBUG2 = true;
        #endregion

        #region "系統共用參數"
        /// <summary>
        /// AP Action Event Log
        /// </summary>
        public static LogOperation EventLog;
        /// <summary>
        /// Database Operation Log
        /// </summary>
        public static LogOperation DatabaseLog;
        /// <summary>
        /// Socket Raw Data Log
        /// </summary>
        public static LogOperation SocketLog;
        /// <summary>
        /// MSMQ Raw Data Log
        /// </summary>
        public static LogOperation MSMQLog;
        /// <summary>
        /// Debug Log
        /// </summary>
        public static LogOperation DebugLog;

        public static SafeQueue<string> QueueEvent = new SafeQueue<string>();
        public static SafeQueue<string> QueueDatabase = new SafeQueue<string>();
        public static SafeQueue<string> QueueSocket = new SafeQueue<string>();
        public static SafeQueue<string> QueueMSMQ = new SafeQueue<string>();
        public static SafeQueue<string> QueueDebug = new SafeQueue<string>();

        public static string MsgBoxTitle = "";

        #endregion

        public static DataTable UserData = new DataTable();

        public static Boolean IsAdmin = true;

        public static MainFrame Frm_mf = (MainFrame)Application.OpenForms[0];
        public static int MainPanelHeight = 120;

        //登入者職工編號
        public static string Staff = "";

        public static List<UserInfos> user = new List<UserInfos>();

        public static List<LoginRecord> loginRec = new List<LoginRecord>();
        
    }

    public class LoginRecord
    {
        public string UserID { get; set; }
        public DateTime LoginInTime { get; set; }
        public DateTime LoginOutTime { get; set; }
        public int logCnt = 0;

        public List<ErrRecord> errRecord = new List<ErrRecord>();
    }

    public class ErrRecord
    {
        public int ErrCnt { get; set; }
        public DateTime ErrTime { get; set; }
    }

    public class ComboBoxItem
    {
        public string Text { get; set; }
        public object Value { get; set; }

        public override string ToString()
        {
            return Text;
        }
    }

    public class User
    {
        public int SerialNo { get; set; }
        public string UserID { get; set; }
        public string UserName { get; set; }
        public string UserPw { get; set; }
        public string DeptID { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int UserPrivilege { get; set; }
        public DateTime UpdateTime { get; set; }
        public string UpdateUserID { get; set; }
        public DateTime InsertTime { get; set; }
        public string InsertUserID { get; set; }
        public string Description { get; set; }
        public DateTime RetireTime { get; set; }
    }

    public class Dept
    {
        public string DeptID { get; set; }
        public string Description { get; set; }

        public int DeptPrivilege { get; set; }
        public DateTime UpdateTime { get; set; }
        public string UpdateUserID { get; set; }
        public DateTime InsertTime { get; set; }
        public string InsertUserID { get; set; }

        public DateTime RetireTime { get; set; }
    }

    public class Group
    {
        public int SerialNo { get; set; }
        public string GroupID { get; set; }
        public string GroupName_En { get; set; }
        public string GroupName_Cht { get; set; }

        public int GroupPrivilege { get; set; }
        public DateTime UpdateTime { get; set; }
        public string UpdateUserID { get; set; }
        public DateTime InsertTime { get; set; }
        public string InsertUserID { get; set; }
        public string Description { get; set; }
        public DateTime RetireTime { get; set; }
    }

    public class GroupToProgram
    {
        public int SerialNo { get; set; }
        public string GroupID { get; set; }
        public string ProgID_En { get; set; }
        public bool DisplayFun { get; set; }
        public bool SearchFun { get; set; }
        public bool InsertFun { get; set; }
        public bool DeleteFun { get; set; }
        public bool UpdateFun { get; set; }
        public bool ControlFun { get; set; }

        public int UserPrivilege { get; set; }
        public DateTime UpdateTime { get; set; }
        public string UpdateUserID { get; set; }
        public DateTime InsertTime { get; set; }
        public string InsertUserID { get; set; }

        public DateTime RetireTime { get; set; }
    }

    public class Programs
    {
        public int SerialNo { get; set; }

        public string ProgID_En { get; set; }
        public string ProgID_Cht { get; set; }

        public int ProgPrivilege { get; set; }
        public DateTime UpdateTime { get; set; }
        public string UpdateUserID { get; set; }
        public DateTime InsertTime { get; set; }
        public string InsertUserID { get; set; }
        public string Description { get; set; }
        public DateTime RetireTime { get; set; }
        public bool DisplayFun { get; set; }
        public bool SearchFun { get; set; }
        public bool InsertFun { get; set; }
        public bool UpdateFun { get; set; }
        public bool DeleteFun { get; set; }
    }

    public class GroupToUser
    {
        public string GroupID { get; set; }
        public string UserID { get; set; }
    }


    public class Rpt1Day
    {
        
        public int SerialNo { get; set; }
        public DateTime rptDate { get; set; }
       
        public string rptTime { get; set; }
        public string loc { get; set; }
        public string point { get; set; }
        public string maxI { get; set; }
        public string minI { get; set; }
        public string maxV { get; set; }
        public string minV { get; set; }
        public DateTime insertTime { get; set; }
    }

    public class Rpt1Month
    {
        public int SerialNo { get; set; }
        public DateTime rptDate { get; set; }        
        public string loc { get; set; }
        public string point { get; set; }
        public string maxI { get; set; }
        public string minI { get; set; }
        public string maxV { get; set; }
        public string minV { get; set; }
        public DateTime insertTime { get; set; }
    }

    public class Rpt1Quarter
    {
        public int SerialNo { get; set; }
        public DateTime rptDate { get; set; }
        public string loc { get; set; }
        public string point { get; set; }
        public string maxI { get; set; }
        public string minI { get; set; }
        public string maxV { get; set; }
        public string minV { get; set; }
        public DateTime insertTime { get; set; }
    }

    public class Rpt1Year
    {
        public int SerialNo { get; set; }
        public DateTime rptDate { get; set; }
        public string loc { get; set; }
        public string point { get; set; }
        public string maxI { get; set; }
        public string minI { get; set; }
        public string maxV { get; set; }
        public string minV { get; set; }
        public DateTime insertTime { get; set; }
    }

    public class Rpt2Day
    {
        public int SerialNo { get; set; }
        public DateTime rptDate { get; set; }
        public string rptTime { get; set; }
        public string loc { get; set; }
        public string point { get; set; }
        public string elec { get; set; }
        public string totalElec { get; set; }
        public DateTime insertTime { get; set; }
    }

    public class Rpt2Month
    {
        public int SerialNo { get; set; }
        public DateTime rptDate { get; set; }
        public string loc { get; set; }
        public string point { get; set; }
        public string elec { get; set; }
        public string totalElec { get; set; }
        public DateTime insertTime { get; set; }
    }

    public class Rpt2Quarter
    {
        public int SerialNo { get; set; }
        public DateTime rptDate { get; set; }
        public string loc { get; set; }
        public string point { get; set; }
        public string elec { get; set; }
        public string totalElec { get; set; }
        public DateTime insertTime { get; set; }
    }

    public class Rpt2Year
    {
        public int SerialNo { get; set; }
        public DateTime rptDate { get; set; }
        public string loc { get; set; }
        public string point { get; set; }
        public string elec { get; set; }
        public string totalElec { get; set; }
        public DateTime insertTime { get; set; }
    }
}
