﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ICSC.Database.MSSQL;
using PRC.Tools;
using System.Net.Http;

namespace PRC.MyClass
{
    class WriteLog
    {
        public static void writeLog(string processName, string processEvent, DateTime eventTime, List<string> cmdLog)
        {
            List<string> sqlList = new List<string>();
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i <= sqlList.Count; i++)
            {
                sb.Length = 0;
                sb.Append("Insert into Event_ProcessLog (CreateTime,ProcessName) Values ");
                sb.Append("()");
                sqlList.Add(sb.ToString());
            }

            StringBuilder logSB = new StringBuilder();
            StringBuilder eventSB = new StringBuilder();
            string tempMsg = string.Empty;


            int sts = 0;
            try
            {

                sts = DbOp.ExecuteSqlList(SHM.DBIp, SHM.DBName, SHM.DBUser, SHM.DBPasswd, sqlList, SHM.LOG_LEVEL_DBDetail, ref tempMsg);
                if (sts > 0)
                {

                }
                logSB.AppendLine(tempMsg);


            }
            catch (Exception ex)
            {
                eventSB.AppendLine("Exception: " + ex.Message);
            }
            finally
            {
                if (eventSB.Length > 0)
                    SHM.QueueEvent.Enqueue(eventSB.ToString());
                if (logSB.Length > 0)
                    SHM.QueueDatabase.Enqueue(logSB.ToString());
            }
        }

        public static HttpResponseMessage WriteLogByWebApi(string formName ,string message,EventLog_OperatorNotifyDto.LogType type)
        {
            string eventLog_LogOperator_Url = "http://localhost:48965/api/EventLog_OperatorLog";

            EventLog_OperatorNotifyDto dto = new EventLog_OperatorNotifyDto() {
                UserID = SHM.user[0].UserID,
                EventTime = DateTime.Now,
                FormName = formName,
                Type = type,
                Message = message
            };

            return WebApiCaller.Json_GetResponseByPost(eventLog_LogOperator_Url, dto);

        }
    }

    public class EventLog_OperatorNotifyDto
    {
        public DateTime EventTime { get; set; }
        public string UserID { get; set; }
        public string FormName { get; set; }
        public LogType Type { get; set; }
        public string Message { get; set; }

        public enum LogType
        {
            INFO,
            ERROR
        }
    }
}
