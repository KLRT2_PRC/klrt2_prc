﻿namespace PRC
{
    partial class Form_Total2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.retangleUC1 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC1 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC2 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC3 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC4 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC5 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.retangleUC7 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label8 = new System.Windows.Forms.Label();
            this.retangleUC10 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label9 = new System.Windows.Forms.Label();
            this.retangleUC11 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label12 = new System.Windows.Forms.Label();
            this.retangleUC14 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.retangleUC13 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label17 = new System.Windows.Forms.Label();
            this.retangleUC16 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label18 = new System.Windows.Forms.Label();
            this.retangleUC17 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label20 = new System.Windows.Forms.Label();
            this.retangleUC19 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.retangleUC20 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC21 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC22 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC23 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC2 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC24 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label25 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.retangleUC26 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label29 = new System.Windows.Forms.Label();
            this.retangleUC28 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label30 = new System.Windows.Forms.Label();
            this.retangleUC29 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label32 = new System.Windows.Forms.Label();
            this.retangleUC31 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.retangleUC32 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC33 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC34 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC35 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC3 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC36 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label37 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.retangleUC38 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label41 = new System.Windows.Forms.Label();
            this.retangleUC40 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label42 = new System.Windows.Forms.Label();
            this.retangleUC41 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label44 = new System.Windows.Forms.Label();
            this.retangleUC43 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.retangleUC44 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC45 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC46 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC47 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC4 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC48 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label49 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.retangleUC50 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label53 = new System.Windows.Forms.Label();
            this.retangleUC52 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label54 = new System.Windows.Forms.Label();
            this.retangleUC53 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label56 = new System.Windows.Forms.Label();
            this.retangleUC55 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.retangleUC56 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC57 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC58 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC59 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC5 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC60 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label61 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.retangleUC62 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label65 = new System.Windows.Forms.Label();
            this.retangleUC64 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label66 = new System.Windows.Forms.Label();
            this.retangleUC65 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label68 = new System.Windows.Forms.Label();
            this.retangleUC67 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label69 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.retangleUC68 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC69 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC70 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC71 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC6 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC72 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label73 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.retangleUC74 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label77 = new System.Windows.Forms.Label();
            this.retangleUC76 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label78 = new System.Windows.Forms.Label();
            this.retangleUC77 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label80 = new System.Windows.Forms.Label();
            this.retangleUC79 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label81 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.retangleUC80 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC81 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC82 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC83 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC7 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC84 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC8 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC9 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC10 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC11 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC12 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC13 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC14 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC15 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label85 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.retangleUC85 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label94 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.retangleUC93 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC94 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC95 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC17 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC96 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label88 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.retangleUC87 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label91 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.retangleUC89 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC90 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC91 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC16 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC92 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label97 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.retangleUC97 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label100 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.retangleUC99 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC100 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC101 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC18 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC102 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label103 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.retangleUC103 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label106 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this.retangleUC105 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC106 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC107 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC19 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC108 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label109 = new System.Windows.Forms.Label();
            this.label110 = new System.Windows.Forms.Label();
            this.retangleUC109 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label112 = new System.Windows.Forms.Label();
            this.label113 = new System.Windows.Forms.Label();
            this.label114 = new System.Windows.Forms.Label();
            this.retangleUC111 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC112 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC113 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC20 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC114 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label115 = new System.Windows.Forms.Label();
            this.label116 = new System.Windows.Forms.Label();
            this.retangleUC115 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label118 = new System.Windows.Forms.Label();
            this.label119 = new System.Windows.Forms.Label();
            this.label120 = new System.Windows.Forms.Label();
            this.retangleUC117 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC118 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC119 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC21 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC120 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label121 = new System.Windows.Forms.Label();
            this.label122 = new System.Windows.Forms.Label();
            this.retangleUC121 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label124 = new System.Windows.Forms.Label();
            this.label125 = new System.Windows.Forms.Label();
            this.label126 = new System.Windows.Forms.Label();
            this.retangleUC123 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC124 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC125 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC22 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC126 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label127 = new System.Windows.Forms.Label();
            this.label128 = new System.Windows.Forms.Label();
            this.retangleUC127 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label130 = new System.Windows.Forms.Label();
            this.label131 = new System.Windows.Forms.Label();
            this.label132 = new System.Windows.Forms.Label();
            this.retangleUC129 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC130 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC131 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC23 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC132 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label133 = new System.Windows.Forms.Label();
            this.label134 = new System.Windows.Forms.Label();
            this.retangleUC133 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label136 = new System.Windows.Forms.Label();
            this.label137 = new System.Windows.Forms.Label();
            this.label138 = new System.Windows.Forms.Label();
            this.retangleUC135 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC136 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC137 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC24 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC138 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label139 = new System.Windows.Forms.Label();
            this.label140 = new System.Windows.Forms.Label();
            this.retangleUC139 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label142 = new System.Windows.Forms.Label();
            this.label143 = new System.Windows.Forms.Label();
            this.label144 = new System.Windows.Forms.Label();
            this.retangleUC141 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC142 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC143 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC25 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC144 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label145 = new System.Windows.Forms.Label();
            this.label146 = new System.Windows.Forms.Label();
            this.retangleUC145 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label148 = new System.Windows.Forms.Label();
            this.label149 = new System.Windows.Forms.Label();
            this.label150 = new System.Windows.Forms.Label();
            this.retangleUC147 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC148 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC149 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC26 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC150 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label151 = new System.Windows.Forms.Label();
            this.label152 = new System.Windows.Forms.Label();
            this.retangleUC151 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label154 = new System.Windows.Forms.Label();
            this.label155 = new System.Windows.Forms.Label();
            this.label156 = new System.Windows.Forms.Label();
            this.retangleUC153 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC154 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC155 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC27 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC156 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label157 = new System.Windows.Forms.Label();
            this.label158 = new System.Windows.Forms.Label();
            this.retangleUC157 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label160 = new System.Windows.Forms.Label();
            this.label161 = new System.Windows.Forms.Label();
            this.label162 = new System.Windows.Forms.Label();
            this.retangleUC159 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC160 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC161 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC28 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC162 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label163 = new System.Windows.Forms.Label();
            this.label164 = new System.Windows.Forms.Label();
            this.retangleUC163 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label166 = new System.Windows.Forms.Label();
            this.label167 = new System.Windows.Forms.Label();
            this.label168 = new System.Windows.Forms.Label();
            this.retangleUC165 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC166 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC167 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC29 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC168 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label169 = new System.Windows.Forms.Label();
            this.label170 = new System.Windows.Forms.Label();
            this.retangleUC169 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label172 = new System.Windows.Forms.Label();
            this.label173 = new System.Windows.Forms.Label();
            this.label174 = new System.Windows.Forms.Label();
            this.retangleUC171 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC172 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC173 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC30 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC174 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label175 = new System.Windows.Forms.Label();
            this.label176 = new System.Windows.Forms.Label();
            this.retangleUC175 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label178 = new System.Windows.Forms.Label();
            this.label179 = new System.Windows.Forms.Label();
            this.label180 = new System.Windows.Forms.Label();
            this.retangleUC177 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC178 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC179 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC31 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC180 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label181 = new System.Windows.Forms.Label();
            this.label182 = new System.Windows.Forms.Label();
            this.retangleUC181 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label184 = new System.Windows.Forms.Label();
            this.label185 = new System.Windows.Forms.Label();
            this.label186 = new System.Windows.Forms.Label();
            this.retangleUC183 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC184 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC185 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC32 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC186 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label187 = new System.Windows.Forms.Label();
            this.label188 = new System.Windows.Forms.Label();
            this.retangleUC187 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label190 = new System.Windows.Forms.Label();
            this.label191 = new System.Windows.Forms.Label();
            this.label192 = new System.Windows.Forms.Label();
            this.retangleUC189 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC190 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC191 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC33 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC192 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label193 = new System.Windows.Forms.Label();
            this.label194 = new System.Windows.Forms.Label();
            this.retangleUC193 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label196 = new System.Windows.Forms.Label();
            this.label197 = new System.Windows.Forms.Label();
            this.label198 = new System.Windows.Forms.Label();
            this.retangleUC195 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC196 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC197 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC34 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC198 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label199 = new System.Windows.Forms.Label();
            this.label200 = new System.Windows.Forms.Label();
            this.retangleUC199 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label202 = new System.Windows.Forms.Label();
            this.label203 = new System.Windows.Forms.Label();
            this.label204 = new System.Windows.Forms.Label();
            this.retangleUC201 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC202 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC203 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC35 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC204 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label205 = new System.Windows.Forms.Label();
            this.label206 = new System.Windows.Forms.Label();
            this.retangleUC205 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label208 = new System.Windows.Forms.Label();
            this.label209 = new System.Windows.Forms.Label();
            this.label210 = new System.Windows.Forms.Label();
            this.retangleUC207 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC208 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC209 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC36 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC210 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label211 = new System.Windows.Forms.Label();
            this.label212 = new System.Windows.Forms.Label();
            this.retangleUC211 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label214 = new System.Windows.Forms.Label();
            this.label215 = new System.Windows.Forms.Label();
            this.label216 = new System.Windows.Forms.Label();
            this.retangleUC213 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC214 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC215 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC37 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC216 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC38 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label217 = new System.Windows.Forms.Label();
            this.label218 = new System.Windows.Forms.Label();
            this.retangleUC217 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label220 = new System.Windows.Forms.Label();
            this.label221 = new System.Windows.Forms.Label();
            this.label222 = new System.Windows.Forms.Label();
            this.retangleUC219 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC220 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC221 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC39 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC222 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC40 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC41 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC42 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC43 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC44 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC45 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC46 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC47 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC48 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC49 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC50 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC51 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC52 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC53 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC54 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC55 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC56 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC57 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC58 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC59 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC60 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC61 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC62 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC6 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC63 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC64 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // retangleUC1
            // 
            this.retangleUC1.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC1.ExtenderWidth = 3;
            this.retangleUC1.Fill = false;
            this.retangleUC1.GroupID = null;
            this.retangleUC1.Location = new System.Drawing.Point(39, 10);
            this.retangleUC1.Name = "retangleUC1";
            this.retangleUC1.Size = new System.Drawing.Size(200, 240);
            this.retangleUC1.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC1.TabIndex = 0;
            // 
            // lineUC1
            // 
            this.lineUC1.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC1.ExtenderWidth = 3;
            this.lineUC1.Fill = false;
            this.lineUC1.GroupID = null;
            this.lineUC1.Location = new System.Drawing.Point(39, 37);
            this.lineUC1.Name = "lineUC1";
            this.lineUC1.Size = new System.Drawing.Size(200, 3);
            this.lineUC1.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC1.TabIndex = 1;
            // 
            // retangleUC2
            // 
            this.retangleUC2.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC2.ExtenderWidth = 2;
            this.retangleUC2.Fill = false;
            this.retangleUC2.GroupID = null;
            this.retangleUC2.Location = new System.Drawing.Point(62, 66);
            this.retangleUC2.Name = "retangleUC2";
            this.retangleUC2.Size = new System.Drawing.Size(20, 20);
            this.retangleUC2.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC2.TabIndex = 2;
            // 
            // retangleUC3
            // 
            this.retangleUC3.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC3.ExtenderWidth = 2;
            this.retangleUC3.Fill = false;
            this.retangleUC3.GroupID = null;
            this.retangleUC3.Location = new System.Drawing.Point(127, 66);
            this.retangleUC3.Name = "retangleUC3";
            this.retangleUC3.Size = new System.Drawing.Size(20, 20);
            this.retangleUC3.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC3.TabIndex = 3;
            // 
            // retangleUC4
            // 
            this.retangleUC4.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC4.ExtenderWidth = 2;
            this.retangleUC4.Fill = false;
            this.retangleUC4.GroupID = null;
            this.retangleUC4.Location = new System.Drawing.Point(192, 66);
            this.retangleUC4.Name = "retangleUC4";
            this.retangleUC4.Size = new System.Drawing.Size(20, 20);
            this.retangleUC4.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC4.TabIndex = 4;
            // 
            // retangleUC5
            // 
            this.retangleUC5.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC5.ExtenderWidth = 2;
            this.retangleUC5.Fill = false;
            this.retangleUC5.GroupID = null;
            this.retangleUC5.Location = new System.Drawing.Point(62, 118);
            this.retangleUC5.Name = "retangleUC5";
            this.retangleUC5.Size = new System.Drawing.Size(20, 20);
            this.retangleUC5.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC5.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(48, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 12);
            this.label1.TabIndex = 7;
            this.label1.Text = "cpu運轉中";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(112, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 12);
            this.label2.TabIndex = 8;
            this.label2.Text = "cpu異常";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(164, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 12);
            this.label3.TabIndex = 9;
            this.label3.Text = "cpu通訊異常";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(41, 92);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 24);
            this.label4.TabIndex = 10;
            this.label4.Text = "RS485模組1\r\n     運轉中";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(106, 92);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 24);
            this.label5.TabIndex = 12;
            this.label5.Text = "RS485模組1\r\n     異常";
            // 
            // retangleUC7
            // 
            this.retangleUC7.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC7.ExtenderWidth = 2;
            this.retangleUC7.Fill = false;
            this.retangleUC7.GroupID = null;
            this.retangleUC7.Location = new System.Drawing.Point(127, 118);
            this.retangleUC7.Name = "retangleUC7";
            this.retangleUC7.Size = new System.Drawing.Size(20, 20);
            this.retangleUC7.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC7.TabIndex = 11;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(106, 143);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 24);
            this.label8.TabIndex = 18;
            this.label8.Text = "RS485模組2\r\n     異常";
            // 
            // retangleUC10
            // 
            this.retangleUC10.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC10.ExtenderWidth = 2;
            this.retangleUC10.Fill = false;
            this.retangleUC10.GroupID = null;
            this.retangleUC10.Location = new System.Drawing.Point(127, 169);
            this.retangleUC10.Name = "retangleUC10";
            this.retangleUC10.Size = new System.Drawing.Size(20, 20);
            this.retangleUC10.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC10.TabIndex = 17;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(41, 143);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(67, 24);
            this.label9.TabIndex = 16;
            this.label9.Text = "RS485模組2\r\n     運轉中";
            // 
            // retangleUC11
            // 
            this.retangleUC11.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC11.ExtenderWidth = 2;
            this.retangleUC11.Fill = false;
            this.retangleUC11.GroupID = null;
            this.retangleUC11.Location = new System.Drawing.Point(62, 169);
            this.retangleUC11.Name = "retangleUC11";
            this.retangleUC11.Size = new System.Drawing.Size(20, 20);
            this.retangleUC11.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC11.TabIndex = 15;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(47, 195);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 24);
            this.label12.TabIndex = 22;
            this.label12.Text = "連鎖迴路\r\n通訊異常";
            // 
            // retangleUC14
            // 
            this.retangleUC14.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC14.ExtenderWidth = 2;
            this.retangleUC14.Fill = false;
            this.retangleUC14.GroupID = null;
            this.retangleUC14.Location = new System.Drawing.Point(62, 219);
            this.retangleUC14.Name = "retangleUC14";
            this.retangleUC14.Size = new System.Drawing.Size(20, 20);
            this.retangleUC14.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC14.TabIndex = 21;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("微軟正黑體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label11.Location = new System.Drawing.Point(110, 13);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(54, 24);
            this.label11.TabIndex = 27;
            this.label11.Text = "TSS7";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("微軟正黑體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label13.Location = new System.Drawing.Point(334, 13);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(54, 24);
            this.label13.TabIndex = 52;
            this.label13.Text = "TSS8";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(271, 195);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(53, 24);
            this.label15.TabIndex = 49;
            this.label15.Text = "連鎖迴路\r\n通訊異常";
            // 
            // retangleUC13
            // 
            this.retangleUC13.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC13.ExtenderWidth = 2;
            this.retangleUC13.Fill = false;
            this.retangleUC13.GroupID = null;
            this.retangleUC13.Location = new System.Drawing.Point(286, 219);
            this.retangleUC13.Name = "retangleUC13";
            this.retangleUC13.Size = new System.Drawing.Size(20, 20);
            this.retangleUC13.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC13.TabIndex = 48;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(330, 143);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(67, 24);
            this.label17.TabIndex = 45;
            this.label17.Text = "RS485模組2\r\n     異常";
            // 
            // retangleUC16
            // 
            this.retangleUC16.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC16.ExtenderWidth = 2;
            this.retangleUC16.Fill = false;
            this.retangleUC16.GroupID = null;
            this.retangleUC16.Location = new System.Drawing.Point(351, 169);
            this.retangleUC16.Name = "retangleUC16";
            this.retangleUC16.Size = new System.Drawing.Size(20, 20);
            this.retangleUC16.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC16.TabIndex = 44;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(265, 143);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(67, 24);
            this.label18.TabIndex = 43;
            this.label18.Text = "RS485模組2\r\n     運轉中";
            // 
            // retangleUC17
            // 
            this.retangleUC17.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC17.ExtenderWidth = 2;
            this.retangleUC17.Fill = false;
            this.retangleUC17.GroupID = null;
            this.retangleUC17.Location = new System.Drawing.Point(286, 169);
            this.retangleUC17.Name = "retangleUC17";
            this.retangleUC17.Size = new System.Drawing.Size(20, 20);
            this.retangleUC17.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC17.TabIndex = 42;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(330, 92);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(67, 24);
            this.label20.TabIndex = 39;
            this.label20.Text = "RS485模組1\r\n     異常";
            // 
            // retangleUC19
            // 
            this.retangleUC19.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC19.ExtenderWidth = 2;
            this.retangleUC19.Fill = false;
            this.retangleUC19.GroupID = null;
            this.retangleUC19.Location = new System.Drawing.Point(351, 118);
            this.retangleUC19.Name = "retangleUC19";
            this.retangleUC19.Size = new System.Drawing.Size(20, 20);
            this.retangleUC19.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC19.TabIndex = 38;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(265, 92);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(67, 24);
            this.label21.TabIndex = 37;
            this.label21.Text = "RS485模組1\r\n     運轉中";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(388, 48);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(70, 12);
            this.label22.TabIndex = 36;
            this.label22.Text = "cpu通訊異常";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(336, 48);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(46, 12);
            this.label23.TabIndex = 35;
            this.label23.Text = "cpu異常";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(272, 48);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(58, 12);
            this.label24.TabIndex = 34;
            this.label24.Text = "cpu運轉中";
            // 
            // retangleUC20
            // 
            this.retangleUC20.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC20.ExtenderWidth = 2;
            this.retangleUC20.Fill = false;
            this.retangleUC20.GroupID = null;
            this.retangleUC20.Location = new System.Drawing.Point(286, 118);
            this.retangleUC20.Name = "retangleUC20";
            this.retangleUC20.Size = new System.Drawing.Size(20, 20);
            this.retangleUC20.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC20.TabIndex = 33;
            // 
            // retangleUC21
            // 
            this.retangleUC21.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC21.ExtenderWidth = 2;
            this.retangleUC21.Fill = false;
            this.retangleUC21.GroupID = null;
            this.retangleUC21.Location = new System.Drawing.Point(416, 66);
            this.retangleUC21.Name = "retangleUC21";
            this.retangleUC21.Size = new System.Drawing.Size(20, 20);
            this.retangleUC21.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC21.TabIndex = 32;
            // 
            // retangleUC22
            // 
            this.retangleUC22.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC22.ExtenderWidth = 2;
            this.retangleUC22.Fill = false;
            this.retangleUC22.GroupID = null;
            this.retangleUC22.Location = new System.Drawing.Point(351, 66);
            this.retangleUC22.Name = "retangleUC22";
            this.retangleUC22.Size = new System.Drawing.Size(20, 20);
            this.retangleUC22.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC22.TabIndex = 31;
            // 
            // retangleUC23
            // 
            this.retangleUC23.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC23.ExtenderWidth = 2;
            this.retangleUC23.Fill = false;
            this.retangleUC23.GroupID = null;
            this.retangleUC23.Location = new System.Drawing.Point(286, 66);
            this.retangleUC23.Name = "retangleUC23";
            this.retangleUC23.Size = new System.Drawing.Size(20, 20);
            this.retangleUC23.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC23.TabIndex = 30;
            // 
            // lineUC2
            // 
            this.lineUC2.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC2.ExtenderWidth = 3;
            this.lineUC2.Fill = false;
            this.lineUC2.GroupID = null;
            this.lineUC2.Location = new System.Drawing.Point(263, 37);
            this.lineUC2.Name = "lineUC2";
            this.lineUC2.Size = new System.Drawing.Size(200, 3);
            this.lineUC2.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC2.TabIndex = 29;
            // 
            // retangleUC24
            // 
            this.retangleUC24.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC24.ExtenderWidth = 3;
            this.retangleUC24.Fill = false;
            this.retangleUC24.GroupID = null;
            this.retangleUC24.Location = new System.Drawing.Point(263, 10);
            this.retangleUC24.Name = "retangleUC24";
            this.retangleUC24.Size = new System.Drawing.Size(200, 240);
            this.retangleUC24.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC24.TabIndex = 28;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("微軟正黑體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label25.Location = new System.Drawing.Point(558, 13);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(54, 24);
            this.label25.TabIndex = 77;
            this.label25.Text = "TSS9";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(495, 195);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(53, 24);
            this.label27.TabIndex = 74;
            this.label27.Text = "連鎖迴路\r\n通訊異常";
            // 
            // retangleUC26
            // 
            this.retangleUC26.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC26.ExtenderWidth = 2;
            this.retangleUC26.Fill = false;
            this.retangleUC26.GroupID = null;
            this.retangleUC26.Location = new System.Drawing.Point(510, 219);
            this.retangleUC26.Name = "retangleUC26";
            this.retangleUC26.Size = new System.Drawing.Size(20, 20);
            this.retangleUC26.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC26.TabIndex = 73;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(554, 143);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(67, 24);
            this.label29.TabIndex = 70;
            this.label29.Text = "RS485模組2\r\n     異常";
            // 
            // retangleUC28
            // 
            this.retangleUC28.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC28.ExtenderWidth = 2;
            this.retangleUC28.Fill = false;
            this.retangleUC28.GroupID = null;
            this.retangleUC28.Location = new System.Drawing.Point(575, 169);
            this.retangleUC28.Name = "retangleUC28";
            this.retangleUC28.Size = new System.Drawing.Size(20, 20);
            this.retangleUC28.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC28.TabIndex = 69;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(489, 143);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(67, 24);
            this.label30.TabIndex = 68;
            this.label30.Text = "RS485模組2\r\n     運轉中";
            // 
            // retangleUC29
            // 
            this.retangleUC29.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC29.ExtenderWidth = 2;
            this.retangleUC29.Fill = false;
            this.retangleUC29.GroupID = null;
            this.retangleUC29.Location = new System.Drawing.Point(510, 169);
            this.retangleUC29.Name = "retangleUC29";
            this.retangleUC29.Size = new System.Drawing.Size(20, 20);
            this.retangleUC29.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC29.TabIndex = 67;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(554, 92);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(67, 24);
            this.label32.TabIndex = 64;
            this.label32.Text = "RS485模組1\r\n     異常";
            // 
            // retangleUC31
            // 
            this.retangleUC31.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC31.ExtenderWidth = 2;
            this.retangleUC31.Fill = false;
            this.retangleUC31.GroupID = null;
            this.retangleUC31.Location = new System.Drawing.Point(575, 118);
            this.retangleUC31.Name = "retangleUC31";
            this.retangleUC31.Size = new System.Drawing.Size(20, 20);
            this.retangleUC31.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC31.TabIndex = 63;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(489, 92);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(67, 24);
            this.label33.TabIndex = 62;
            this.label33.Text = "RS485模組1\r\n     運轉中";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(612, 48);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(70, 12);
            this.label34.TabIndex = 61;
            this.label34.Text = "cpu通訊異常";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(560, 48);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(46, 12);
            this.label35.TabIndex = 60;
            this.label35.Text = "cpu異常";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(496, 48);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(58, 12);
            this.label36.TabIndex = 59;
            this.label36.Text = "cpu運轉中";
            // 
            // retangleUC32
            // 
            this.retangleUC32.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC32.ExtenderWidth = 2;
            this.retangleUC32.Fill = false;
            this.retangleUC32.GroupID = null;
            this.retangleUC32.Location = new System.Drawing.Point(510, 118);
            this.retangleUC32.Name = "retangleUC32";
            this.retangleUC32.Size = new System.Drawing.Size(20, 20);
            this.retangleUC32.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC32.TabIndex = 58;
            // 
            // retangleUC33
            // 
            this.retangleUC33.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC33.ExtenderWidth = 2;
            this.retangleUC33.Fill = false;
            this.retangleUC33.GroupID = null;
            this.retangleUC33.Location = new System.Drawing.Point(640, 66);
            this.retangleUC33.Name = "retangleUC33";
            this.retangleUC33.Size = new System.Drawing.Size(20, 20);
            this.retangleUC33.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC33.TabIndex = 57;
            // 
            // retangleUC34
            // 
            this.retangleUC34.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC34.ExtenderWidth = 2;
            this.retangleUC34.Fill = false;
            this.retangleUC34.GroupID = null;
            this.retangleUC34.Location = new System.Drawing.Point(575, 66);
            this.retangleUC34.Name = "retangleUC34";
            this.retangleUC34.Size = new System.Drawing.Size(20, 20);
            this.retangleUC34.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC34.TabIndex = 56;
            // 
            // retangleUC35
            // 
            this.retangleUC35.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC35.ExtenderWidth = 2;
            this.retangleUC35.Fill = false;
            this.retangleUC35.GroupID = null;
            this.retangleUC35.Location = new System.Drawing.Point(510, 66);
            this.retangleUC35.Name = "retangleUC35";
            this.retangleUC35.Size = new System.Drawing.Size(20, 20);
            this.retangleUC35.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC35.TabIndex = 55;
            // 
            // lineUC3
            // 
            this.lineUC3.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC3.ExtenderWidth = 3;
            this.lineUC3.Fill = false;
            this.lineUC3.GroupID = null;
            this.lineUC3.Location = new System.Drawing.Point(487, 37);
            this.lineUC3.Name = "lineUC3";
            this.lineUC3.Size = new System.Drawing.Size(200, 3);
            this.lineUC3.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC3.TabIndex = 54;
            // 
            // retangleUC36
            // 
            this.retangleUC36.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC36.ExtenderWidth = 3;
            this.retangleUC36.Fill = false;
            this.retangleUC36.GroupID = null;
            this.retangleUC36.Location = new System.Drawing.Point(487, 10);
            this.retangleUC36.Name = "retangleUC36";
            this.retangleUC36.Size = new System.Drawing.Size(200, 240);
            this.retangleUC36.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC36.TabIndex = 53;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("微軟正黑體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label37.Location = new System.Drawing.Point(780, 13);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(65, 24);
            this.label37.TabIndex = 102;
            this.label37.Text = "TSS10";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(717, 195);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(53, 24);
            this.label39.TabIndex = 99;
            this.label39.Text = "連鎖迴路\r\n通訊異常";
            // 
            // retangleUC38
            // 
            this.retangleUC38.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC38.ExtenderWidth = 2;
            this.retangleUC38.Fill = false;
            this.retangleUC38.GroupID = null;
            this.retangleUC38.Location = new System.Drawing.Point(732, 219);
            this.retangleUC38.Name = "retangleUC38";
            this.retangleUC38.Size = new System.Drawing.Size(20, 20);
            this.retangleUC38.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC38.TabIndex = 98;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(776, 143);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(67, 24);
            this.label41.TabIndex = 95;
            this.label41.Text = "RS485模組2\r\n     異常";
            // 
            // retangleUC40
            // 
            this.retangleUC40.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC40.ExtenderWidth = 2;
            this.retangleUC40.Fill = false;
            this.retangleUC40.GroupID = null;
            this.retangleUC40.Location = new System.Drawing.Point(797, 169);
            this.retangleUC40.Name = "retangleUC40";
            this.retangleUC40.Size = new System.Drawing.Size(20, 20);
            this.retangleUC40.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC40.TabIndex = 94;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(711, 143);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(67, 24);
            this.label42.TabIndex = 93;
            this.label42.Text = "RS485模組2\r\n     運轉中";
            // 
            // retangleUC41
            // 
            this.retangleUC41.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC41.ExtenderWidth = 2;
            this.retangleUC41.Fill = false;
            this.retangleUC41.GroupID = null;
            this.retangleUC41.Location = new System.Drawing.Point(732, 169);
            this.retangleUC41.Name = "retangleUC41";
            this.retangleUC41.Size = new System.Drawing.Size(20, 20);
            this.retangleUC41.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC41.TabIndex = 92;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(776, 92);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(67, 24);
            this.label44.TabIndex = 89;
            this.label44.Text = "RS485模組1\r\n     異常";
            // 
            // retangleUC43
            // 
            this.retangleUC43.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC43.ExtenderWidth = 2;
            this.retangleUC43.Fill = false;
            this.retangleUC43.GroupID = null;
            this.retangleUC43.Location = new System.Drawing.Point(797, 118);
            this.retangleUC43.Name = "retangleUC43";
            this.retangleUC43.Size = new System.Drawing.Size(20, 20);
            this.retangleUC43.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC43.TabIndex = 88;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(711, 92);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(67, 24);
            this.label45.TabIndex = 87;
            this.label45.Text = "RS485模組1\r\n     運轉中";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(834, 48);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(70, 12);
            this.label46.TabIndex = 86;
            this.label46.Text = "cpu通訊異常";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(782, 48);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(46, 12);
            this.label47.TabIndex = 85;
            this.label47.Text = "cpu異常";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(718, 48);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(58, 12);
            this.label48.TabIndex = 84;
            this.label48.Text = "cpu運轉中";
            // 
            // retangleUC44
            // 
            this.retangleUC44.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC44.ExtenderWidth = 2;
            this.retangleUC44.Fill = false;
            this.retangleUC44.GroupID = null;
            this.retangleUC44.Location = new System.Drawing.Point(732, 118);
            this.retangleUC44.Name = "retangleUC44";
            this.retangleUC44.Size = new System.Drawing.Size(20, 20);
            this.retangleUC44.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC44.TabIndex = 83;
            // 
            // retangleUC45
            // 
            this.retangleUC45.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC45.ExtenderWidth = 2;
            this.retangleUC45.Fill = false;
            this.retangleUC45.GroupID = null;
            this.retangleUC45.Location = new System.Drawing.Point(862, 66);
            this.retangleUC45.Name = "retangleUC45";
            this.retangleUC45.Size = new System.Drawing.Size(20, 20);
            this.retangleUC45.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC45.TabIndex = 82;
            // 
            // retangleUC46
            // 
            this.retangleUC46.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC46.ExtenderWidth = 2;
            this.retangleUC46.Fill = false;
            this.retangleUC46.GroupID = null;
            this.retangleUC46.Location = new System.Drawing.Point(797, 66);
            this.retangleUC46.Name = "retangleUC46";
            this.retangleUC46.Size = new System.Drawing.Size(20, 20);
            this.retangleUC46.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC46.TabIndex = 81;
            // 
            // retangleUC47
            // 
            this.retangleUC47.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC47.ExtenderWidth = 2;
            this.retangleUC47.Fill = false;
            this.retangleUC47.GroupID = null;
            this.retangleUC47.Location = new System.Drawing.Point(732, 66);
            this.retangleUC47.Name = "retangleUC47";
            this.retangleUC47.Size = new System.Drawing.Size(20, 20);
            this.retangleUC47.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC47.TabIndex = 80;
            // 
            // lineUC4
            // 
            this.lineUC4.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC4.ExtenderWidth = 3;
            this.lineUC4.Fill = false;
            this.lineUC4.GroupID = null;
            this.lineUC4.Location = new System.Drawing.Point(709, 37);
            this.lineUC4.Name = "lineUC4";
            this.lineUC4.Size = new System.Drawing.Size(200, 3);
            this.lineUC4.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC4.TabIndex = 79;
            // 
            // retangleUC48
            // 
            this.retangleUC48.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC48.ExtenderWidth = 3;
            this.retangleUC48.Fill = false;
            this.retangleUC48.GroupID = null;
            this.retangleUC48.Location = new System.Drawing.Point(709, 10);
            this.retangleUC48.Name = "retangleUC48";
            this.retangleUC48.Size = new System.Drawing.Size(200, 240);
            this.retangleUC48.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC48.TabIndex = 78;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("微軟正黑體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label49.Location = new System.Drawing.Point(1003, 13);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(65, 24);
            this.label49.TabIndex = 127;
            this.label49.Text = "TSS11";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(940, 195);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(53, 24);
            this.label51.TabIndex = 124;
            this.label51.Text = "連鎖迴路\r\n通訊異常";
            // 
            // retangleUC50
            // 
            this.retangleUC50.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC50.ExtenderWidth = 2;
            this.retangleUC50.Fill = false;
            this.retangleUC50.GroupID = null;
            this.retangleUC50.Location = new System.Drawing.Point(955, 219);
            this.retangleUC50.Name = "retangleUC50";
            this.retangleUC50.Size = new System.Drawing.Size(20, 20);
            this.retangleUC50.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC50.TabIndex = 123;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(999, 143);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(67, 24);
            this.label53.TabIndex = 120;
            this.label53.Text = "RS485模組2\r\n     異常";
            // 
            // retangleUC52
            // 
            this.retangleUC52.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC52.ExtenderWidth = 2;
            this.retangleUC52.Fill = false;
            this.retangleUC52.GroupID = null;
            this.retangleUC52.Location = new System.Drawing.Point(1020, 169);
            this.retangleUC52.Name = "retangleUC52";
            this.retangleUC52.Size = new System.Drawing.Size(20, 20);
            this.retangleUC52.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC52.TabIndex = 119;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(934, 143);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(67, 24);
            this.label54.TabIndex = 118;
            this.label54.Text = "RS485模組2\r\n     運轉中";
            // 
            // retangleUC53
            // 
            this.retangleUC53.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC53.ExtenderWidth = 2;
            this.retangleUC53.Fill = false;
            this.retangleUC53.GroupID = null;
            this.retangleUC53.Location = new System.Drawing.Point(955, 169);
            this.retangleUC53.Name = "retangleUC53";
            this.retangleUC53.Size = new System.Drawing.Size(20, 20);
            this.retangleUC53.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC53.TabIndex = 117;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(999, 92);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(67, 24);
            this.label56.TabIndex = 114;
            this.label56.Text = "RS485模組1\r\n     異常";
            // 
            // retangleUC55
            // 
            this.retangleUC55.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC55.ExtenderWidth = 2;
            this.retangleUC55.Fill = false;
            this.retangleUC55.GroupID = null;
            this.retangleUC55.Location = new System.Drawing.Point(1020, 118);
            this.retangleUC55.Name = "retangleUC55";
            this.retangleUC55.Size = new System.Drawing.Size(20, 20);
            this.retangleUC55.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC55.TabIndex = 113;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(934, 92);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(67, 24);
            this.label57.TabIndex = 112;
            this.label57.Text = "RS485模組1\r\n     運轉中";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(1057, 48);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(70, 12);
            this.label58.TabIndex = 111;
            this.label58.Text = "cpu通訊異常";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(1005, 48);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(46, 12);
            this.label59.TabIndex = 110;
            this.label59.Text = "cpu異常";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(941, 48);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(58, 12);
            this.label60.TabIndex = 109;
            this.label60.Text = "cpu運轉中";
            // 
            // retangleUC56
            // 
            this.retangleUC56.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC56.ExtenderWidth = 2;
            this.retangleUC56.Fill = false;
            this.retangleUC56.GroupID = null;
            this.retangleUC56.Location = new System.Drawing.Point(955, 118);
            this.retangleUC56.Name = "retangleUC56";
            this.retangleUC56.Size = new System.Drawing.Size(20, 20);
            this.retangleUC56.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC56.TabIndex = 108;
            // 
            // retangleUC57
            // 
            this.retangleUC57.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC57.ExtenderWidth = 2;
            this.retangleUC57.Fill = false;
            this.retangleUC57.GroupID = null;
            this.retangleUC57.Location = new System.Drawing.Point(1085, 66);
            this.retangleUC57.Name = "retangleUC57";
            this.retangleUC57.Size = new System.Drawing.Size(20, 20);
            this.retangleUC57.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC57.TabIndex = 107;
            // 
            // retangleUC58
            // 
            this.retangleUC58.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC58.ExtenderWidth = 2;
            this.retangleUC58.Fill = false;
            this.retangleUC58.GroupID = null;
            this.retangleUC58.Location = new System.Drawing.Point(1020, 66);
            this.retangleUC58.Name = "retangleUC58";
            this.retangleUC58.Size = new System.Drawing.Size(20, 20);
            this.retangleUC58.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC58.TabIndex = 106;
            // 
            // retangleUC59
            // 
            this.retangleUC59.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC59.ExtenderWidth = 2;
            this.retangleUC59.Fill = false;
            this.retangleUC59.GroupID = null;
            this.retangleUC59.Location = new System.Drawing.Point(955, 66);
            this.retangleUC59.Name = "retangleUC59";
            this.retangleUC59.Size = new System.Drawing.Size(20, 20);
            this.retangleUC59.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC59.TabIndex = 105;
            // 
            // lineUC5
            // 
            this.lineUC5.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC5.ExtenderWidth = 3;
            this.lineUC5.Fill = false;
            this.lineUC5.GroupID = null;
            this.lineUC5.Location = new System.Drawing.Point(932, 37);
            this.lineUC5.Name = "lineUC5";
            this.lineUC5.Size = new System.Drawing.Size(200, 3);
            this.lineUC5.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC5.TabIndex = 104;
            // 
            // retangleUC60
            // 
            this.retangleUC60.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC60.ExtenderWidth = 3;
            this.retangleUC60.Fill = false;
            this.retangleUC60.GroupID = null;
            this.retangleUC60.Location = new System.Drawing.Point(932, 10);
            this.retangleUC60.Name = "retangleUC60";
            this.retangleUC60.Size = new System.Drawing.Size(200, 240);
            this.retangleUC60.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC60.TabIndex = 103;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("微軟正黑體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label61.Location = new System.Drawing.Point(1227, 13);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(65, 24);
            this.label61.TabIndex = 152;
            this.label61.Text = "TSS12";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(1164, 195);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(53, 24);
            this.label63.TabIndex = 149;
            this.label63.Text = "連鎖迴路\r\n通訊異常";
            // 
            // retangleUC62
            // 
            this.retangleUC62.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC62.ExtenderWidth = 2;
            this.retangleUC62.Fill = false;
            this.retangleUC62.GroupID = null;
            this.retangleUC62.Location = new System.Drawing.Point(1179, 219);
            this.retangleUC62.Name = "retangleUC62";
            this.retangleUC62.Size = new System.Drawing.Size(20, 20);
            this.retangleUC62.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC62.TabIndex = 148;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(1223, 143);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(67, 24);
            this.label65.TabIndex = 145;
            this.label65.Text = "RS485模組2\r\n     異常";
            // 
            // retangleUC64
            // 
            this.retangleUC64.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC64.ExtenderWidth = 2;
            this.retangleUC64.Fill = false;
            this.retangleUC64.GroupID = null;
            this.retangleUC64.Location = new System.Drawing.Point(1244, 169);
            this.retangleUC64.Name = "retangleUC64";
            this.retangleUC64.Size = new System.Drawing.Size(20, 20);
            this.retangleUC64.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC64.TabIndex = 144;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(1158, 143);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(67, 24);
            this.label66.TabIndex = 143;
            this.label66.Text = "RS485模組2\r\n     運轉中";
            // 
            // retangleUC65
            // 
            this.retangleUC65.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC65.ExtenderWidth = 2;
            this.retangleUC65.Fill = false;
            this.retangleUC65.GroupID = null;
            this.retangleUC65.Location = new System.Drawing.Point(1179, 169);
            this.retangleUC65.Name = "retangleUC65";
            this.retangleUC65.Size = new System.Drawing.Size(20, 20);
            this.retangleUC65.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC65.TabIndex = 142;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(1223, 92);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(67, 24);
            this.label68.TabIndex = 139;
            this.label68.Text = "RS485模組1\r\n     異常";
            // 
            // retangleUC67
            // 
            this.retangleUC67.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC67.ExtenderWidth = 2;
            this.retangleUC67.Fill = false;
            this.retangleUC67.GroupID = null;
            this.retangleUC67.Location = new System.Drawing.Point(1244, 118);
            this.retangleUC67.Name = "retangleUC67";
            this.retangleUC67.Size = new System.Drawing.Size(20, 20);
            this.retangleUC67.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC67.TabIndex = 138;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(1158, 92);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(67, 24);
            this.label69.TabIndex = 137;
            this.label69.Text = "RS485模組1\r\n     運轉中";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(1281, 48);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(70, 12);
            this.label70.TabIndex = 136;
            this.label70.Text = "cpu通訊異常";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(1229, 48);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(46, 12);
            this.label71.TabIndex = 135;
            this.label71.Text = "cpu異常";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(1165, 48);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(58, 12);
            this.label72.TabIndex = 134;
            this.label72.Text = "cpu運轉中";
            // 
            // retangleUC68
            // 
            this.retangleUC68.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC68.ExtenderWidth = 2;
            this.retangleUC68.Fill = false;
            this.retangleUC68.GroupID = null;
            this.retangleUC68.Location = new System.Drawing.Point(1179, 118);
            this.retangleUC68.Name = "retangleUC68";
            this.retangleUC68.Size = new System.Drawing.Size(20, 20);
            this.retangleUC68.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC68.TabIndex = 133;
            // 
            // retangleUC69
            // 
            this.retangleUC69.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC69.ExtenderWidth = 2;
            this.retangleUC69.Fill = false;
            this.retangleUC69.GroupID = null;
            this.retangleUC69.Location = new System.Drawing.Point(1309, 66);
            this.retangleUC69.Name = "retangleUC69";
            this.retangleUC69.Size = new System.Drawing.Size(20, 20);
            this.retangleUC69.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC69.TabIndex = 132;
            // 
            // retangleUC70
            // 
            this.retangleUC70.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC70.ExtenderWidth = 2;
            this.retangleUC70.Fill = false;
            this.retangleUC70.GroupID = null;
            this.retangleUC70.Location = new System.Drawing.Point(1244, 66);
            this.retangleUC70.Name = "retangleUC70";
            this.retangleUC70.Size = new System.Drawing.Size(20, 20);
            this.retangleUC70.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC70.TabIndex = 131;
            // 
            // retangleUC71
            // 
            this.retangleUC71.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC71.ExtenderWidth = 2;
            this.retangleUC71.Fill = false;
            this.retangleUC71.GroupID = null;
            this.retangleUC71.Location = new System.Drawing.Point(1179, 66);
            this.retangleUC71.Name = "retangleUC71";
            this.retangleUC71.Size = new System.Drawing.Size(20, 20);
            this.retangleUC71.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC71.TabIndex = 130;
            // 
            // lineUC6
            // 
            this.lineUC6.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC6.ExtenderWidth = 3;
            this.lineUC6.Fill = false;
            this.lineUC6.GroupID = null;
            this.lineUC6.Location = new System.Drawing.Point(1156, 37);
            this.lineUC6.Name = "lineUC6";
            this.lineUC6.Size = new System.Drawing.Size(200, 3);
            this.lineUC6.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC6.TabIndex = 129;
            // 
            // retangleUC72
            // 
            this.retangleUC72.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC72.ExtenderWidth = 3;
            this.retangleUC72.Fill = false;
            this.retangleUC72.GroupID = null;
            this.retangleUC72.Location = new System.Drawing.Point(1156, 10);
            this.retangleUC72.Name = "retangleUC72";
            this.retangleUC72.Size = new System.Drawing.Size(200, 240);
            this.retangleUC72.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC72.TabIndex = 128;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("微軟正黑體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label73.Location = new System.Drawing.Point(1452, 13);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(57, 24);
            this.label73.TabIndex = 177;
            this.label73.Text = "DTSS";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(1389, 195);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(53, 24);
            this.label75.TabIndex = 174;
            this.label75.Text = "連鎖迴路\r\n通訊異常";
            // 
            // retangleUC74
            // 
            this.retangleUC74.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC74.ExtenderWidth = 2;
            this.retangleUC74.Fill = false;
            this.retangleUC74.GroupID = null;
            this.retangleUC74.Location = new System.Drawing.Point(1404, 219);
            this.retangleUC74.Name = "retangleUC74";
            this.retangleUC74.Size = new System.Drawing.Size(20, 20);
            this.retangleUC74.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC74.TabIndex = 173;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(1448, 143);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(67, 24);
            this.label77.TabIndex = 170;
            this.label77.Text = "RS485模組2\r\n     異常";
            // 
            // retangleUC76
            // 
            this.retangleUC76.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC76.ExtenderWidth = 2;
            this.retangleUC76.Fill = false;
            this.retangleUC76.GroupID = null;
            this.retangleUC76.Location = new System.Drawing.Point(1469, 169);
            this.retangleUC76.Name = "retangleUC76";
            this.retangleUC76.Size = new System.Drawing.Size(20, 20);
            this.retangleUC76.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC76.TabIndex = 169;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(1383, 143);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(67, 24);
            this.label78.TabIndex = 168;
            this.label78.Text = "RS485模組2\r\n     運轉中";
            // 
            // retangleUC77
            // 
            this.retangleUC77.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC77.ExtenderWidth = 2;
            this.retangleUC77.Fill = false;
            this.retangleUC77.GroupID = null;
            this.retangleUC77.Location = new System.Drawing.Point(1404, 169);
            this.retangleUC77.Name = "retangleUC77";
            this.retangleUC77.Size = new System.Drawing.Size(20, 20);
            this.retangleUC77.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC77.TabIndex = 167;
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(1448, 92);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(67, 24);
            this.label80.TabIndex = 164;
            this.label80.Text = "RS485模組1\r\n     異常";
            // 
            // retangleUC79
            // 
            this.retangleUC79.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC79.ExtenderWidth = 2;
            this.retangleUC79.Fill = false;
            this.retangleUC79.GroupID = null;
            this.retangleUC79.Location = new System.Drawing.Point(1469, 118);
            this.retangleUC79.Name = "retangleUC79";
            this.retangleUC79.Size = new System.Drawing.Size(20, 20);
            this.retangleUC79.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC79.TabIndex = 163;
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(1383, 92);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(67, 24);
            this.label81.TabIndex = 162;
            this.label81.Text = "RS485模組1\r\n     運轉中";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(1506, 48);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(70, 12);
            this.label82.TabIndex = 161;
            this.label82.Text = "cpu通訊異常";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(1454, 48);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(46, 12);
            this.label83.TabIndex = 160;
            this.label83.Text = "cpu異常";
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(1390, 48);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(58, 12);
            this.label84.TabIndex = 159;
            this.label84.Text = "cpu運轉中";
            // 
            // retangleUC80
            // 
            this.retangleUC80.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC80.ExtenderWidth = 2;
            this.retangleUC80.Fill = false;
            this.retangleUC80.GroupID = null;
            this.retangleUC80.Location = new System.Drawing.Point(1404, 118);
            this.retangleUC80.Name = "retangleUC80";
            this.retangleUC80.Size = new System.Drawing.Size(20, 20);
            this.retangleUC80.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC80.TabIndex = 158;
            // 
            // retangleUC81
            // 
            this.retangleUC81.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC81.ExtenderWidth = 2;
            this.retangleUC81.Fill = false;
            this.retangleUC81.GroupID = null;
            this.retangleUC81.Location = new System.Drawing.Point(1534, 66);
            this.retangleUC81.Name = "retangleUC81";
            this.retangleUC81.Size = new System.Drawing.Size(20, 20);
            this.retangleUC81.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC81.TabIndex = 157;
            // 
            // retangleUC82
            // 
            this.retangleUC82.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC82.ExtenderWidth = 2;
            this.retangleUC82.Fill = false;
            this.retangleUC82.GroupID = null;
            this.retangleUC82.Location = new System.Drawing.Point(1469, 66);
            this.retangleUC82.Name = "retangleUC82";
            this.retangleUC82.Size = new System.Drawing.Size(20, 20);
            this.retangleUC82.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC82.TabIndex = 156;
            // 
            // retangleUC83
            // 
            this.retangleUC83.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC83.ExtenderWidth = 2;
            this.retangleUC83.Fill = false;
            this.retangleUC83.GroupID = null;
            this.retangleUC83.Location = new System.Drawing.Point(1404, 66);
            this.retangleUC83.Name = "retangleUC83";
            this.retangleUC83.Size = new System.Drawing.Size(20, 20);
            this.retangleUC83.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC83.TabIndex = 155;
            // 
            // lineUC7
            // 
            this.lineUC7.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC7.ExtenderWidth = 3;
            this.lineUC7.Fill = false;
            this.lineUC7.GroupID = null;
            this.lineUC7.Location = new System.Drawing.Point(1381, 37);
            this.lineUC7.Name = "lineUC7";
            this.lineUC7.Size = new System.Drawing.Size(200, 3);
            this.lineUC7.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC7.TabIndex = 154;
            // 
            // retangleUC84
            // 
            this.retangleUC84.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC84.ExtenderWidth = 3;
            this.retangleUC84.Fill = false;
            this.retangleUC84.GroupID = null;
            this.retangleUC84.Location = new System.Drawing.Point(1381, 10);
            this.retangleUC84.Name = "retangleUC84";
            this.retangleUC84.Size = new System.Drawing.Size(200, 240);
            this.retangleUC84.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC84.TabIndex = 153;
            // 
            // lineUC8
            // 
            this.lineUC8.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC8.ExtenderWidth = 5;
            this.lineUC8.Fill = false;
            this.lineUC8.GroupID = null;
            this.lineUC8.Location = new System.Drawing.Point(12, 282);
            this.lineUC8.Name = "lineUC8";
            this.lineUC8.Size = new System.Drawing.Size(1900, 10);
            this.lineUC8.State = iSCADA.Design.Utilities.Electrical.SymbolState.Disconnected;
            this.lineUC8.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC8.TabIndex = 178;
            // 
            // lineUC9
            // 
            this.lineUC9.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC9.ExtenderWidth = 5;
            this.lineUC9.Fill = false;
            this.lineUC9.GroupID = null;
            this.lineUC9.Location = new System.Drawing.Point(127, 249);
            this.lineUC9.Name = "lineUC9";
            this.lineUC9.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC9.Size = new System.Drawing.Size(10, 38);
            this.lineUC9.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC9.TabIndex = 179;
            // 
            // lineUC10
            // 
            this.lineUC10.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC10.ExtenderWidth = 5;
            this.lineUC10.Fill = false;
            this.lineUC10.GroupID = null;
            this.lineUC10.Location = new System.Drawing.Point(357, 250);
            this.lineUC10.Name = "lineUC10";
            this.lineUC10.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC10.Size = new System.Drawing.Size(10, 35);
            this.lineUC10.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC10.TabIndex = 180;
            // 
            // lineUC11
            // 
            this.lineUC11.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC11.ExtenderWidth = 5;
            this.lineUC11.Fill = false;
            this.lineUC11.GroupID = null;
            this.lineUC11.Location = new System.Drawing.Point(584, 250);
            this.lineUC11.Name = "lineUC11";
            this.lineUC11.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC11.Size = new System.Drawing.Size(10, 35);
            this.lineUC11.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC11.TabIndex = 181;
            // 
            // lineUC12
            // 
            this.lineUC12.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC12.ExtenderWidth = 5;
            this.lineUC12.Fill = false;
            this.lineUC12.GroupID = null;
            this.lineUC12.Location = new System.Drawing.Point(807, 250);
            this.lineUC12.Name = "lineUC12";
            this.lineUC12.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC12.Size = new System.Drawing.Size(10, 35);
            this.lineUC12.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC12.TabIndex = 182;
            // 
            // lineUC13
            // 
            this.lineUC13.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC13.ExtenderWidth = 5;
            this.lineUC13.Fill = false;
            this.lineUC13.GroupID = null;
            this.lineUC13.Location = new System.Drawing.Point(1030, 250);
            this.lineUC13.Name = "lineUC13";
            this.lineUC13.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC13.Size = new System.Drawing.Size(11, 35);
            this.lineUC13.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC13.TabIndex = 183;
            // 
            // lineUC14
            // 
            this.lineUC14.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC14.ExtenderWidth = 5;
            this.lineUC14.Fill = false;
            this.lineUC14.GroupID = null;
            this.lineUC14.Location = new System.Drawing.Point(1255, 249);
            this.lineUC14.Name = "lineUC14";
            this.lineUC14.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC14.Size = new System.Drawing.Size(10, 37);
            this.lineUC14.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC14.TabIndex = 184;
            // 
            // lineUC15
            // 
            this.lineUC15.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC15.ExtenderWidth = 5;
            this.lineUC15.Fill = false;
            this.lineUC15.GroupID = null;
            this.lineUC15.Location = new System.Drawing.Point(1480, 249);
            this.lineUC15.Name = "lineUC15";
            this.lineUC15.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC15.Size = new System.Drawing.Size(10, 36);
            this.lineUC15.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC15.TabIndex = 185;
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label85.Location = new System.Drawing.Point(118, 307);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(39, 19);
            this.label85.TabIndex = 210;
            this.label85.Text = "C15";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(181, 376);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(53, 24);
            this.label86.TabIndex = 209;
            this.label86.Text = "連鎖迴路\r\n通訊斷訊";
            // 
            // retangleUC85
            // 
            this.retangleUC85.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC85.ExtenderWidth = 2;
            this.retangleUC85.Fill = false;
            this.retangleUC85.GroupID = null;
            this.retangleUC85.Location = new System.Drawing.Point(192, 400);
            this.retangleUC85.Name = "retangleUC85";
            this.retangleUC85.Size = new System.Drawing.Size(20, 20);
            this.retangleUC85.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC85.TabIndex = 208;
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Location = new System.Drawing.Point(164, 335);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(70, 12);
            this.label94.TabIndex = 194;
            this.label94.Text = "cpu通訊異常";
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Location = new System.Drawing.Point(112, 335);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(46, 12);
            this.label95.TabIndex = 193;
            this.label95.Text = "cpu異常";
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(48, 335);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(58, 12);
            this.label96.TabIndex = 192;
            this.label96.Text = "cpu運轉中";
            // 
            // retangleUC93
            // 
            this.retangleUC93.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC93.ExtenderWidth = 2;
            this.retangleUC93.Fill = false;
            this.retangleUC93.GroupID = null;
            this.retangleUC93.Location = new System.Drawing.Point(192, 351);
            this.retangleUC93.Name = "retangleUC93";
            this.retangleUC93.Size = new System.Drawing.Size(20, 20);
            this.retangleUC93.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC93.TabIndex = 190;
            // 
            // retangleUC94
            // 
            this.retangleUC94.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC94.ExtenderWidth = 2;
            this.retangleUC94.Fill = false;
            this.retangleUC94.GroupID = null;
            this.retangleUC94.Location = new System.Drawing.Point(127, 351);
            this.retangleUC94.Name = "retangleUC94";
            this.retangleUC94.Size = new System.Drawing.Size(20, 20);
            this.retangleUC94.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC94.TabIndex = 189;
            // 
            // retangleUC95
            // 
            this.retangleUC95.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC95.ExtenderWidth = 2;
            this.retangleUC95.Fill = false;
            this.retangleUC95.GroupID = null;
            this.retangleUC95.Location = new System.Drawing.Point(62, 351);
            this.retangleUC95.Name = "retangleUC95";
            this.retangleUC95.Size = new System.Drawing.Size(20, 20);
            this.retangleUC95.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC95.TabIndex = 188;
            // 
            // lineUC17
            // 
            this.lineUC17.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC17.ExtenderWidth = 3;
            this.lineUC17.Fill = false;
            this.lineUC17.GroupID = null;
            this.lineUC17.Location = new System.Drawing.Point(39, 327);
            this.lineUC17.Name = "lineUC17";
            this.lineUC17.Size = new System.Drawing.Size(200, 3);
            this.lineUC17.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC17.TabIndex = 187;
            // 
            // retangleUC96
            // 
            this.retangleUC96.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC96.ExtenderWidth = 3;
            this.retangleUC96.Fill = false;
            this.retangleUC96.GroupID = null;
            this.retangleUC96.Location = new System.Drawing.Point(39, 305);
            this.retangleUC96.Name = "retangleUC96";
            this.retangleUC96.Size = new System.Drawing.Size(200, 124);
            this.retangleUC96.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC96.TabIndex = 186;
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label88.Location = new System.Drawing.Point(118, 766);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(39, 19);
            this.label88.TabIndex = 223;
            this.label88.Text = "C27";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(181, 835);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(53, 24);
            this.label89.TabIndex = 222;
            this.label89.Text = "連鎖迴路\r\n通訊斷訊";
            // 
            // retangleUC87
            // 
            this.retangleUC87.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC87.ExtenderWidth = 2;
            this.retangleUC87.Fill = false;
            this.retangleUC87.GroupID = null;
            this.retangleUC87.Location = new System.Drawing.Point(192, 859);
            this.retangleUC87.Name = "retangleUC87";
            this.retangleUC87.Size = new System.Drawing.Size(20, 20);
            this.retangleUC87.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC87.TabIndex = 221;
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Location = new System.Drawing.Point(164, 794);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(70, 12);
            this.label91.TabIndex = 218;
            this.label91.Text = "cpu通訊異常";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Location = new System.Drawing.Point(112, 794);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(46, 12);
            this.label92.TabIndex = 217;
            this.label92.Text = "cpu異常";
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Location = new System.Drawing.Point(48, 794);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(58, 12);
            this.label93.TabIndex = 216;
            this.label93.Text = "cpu運轉中";
            // 
            // retangleUC89
            // 
            this.retangleUC89.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC89.ExtenderWidth = 2;
            this.retangleUC89.Fill = false;
            this.retangleUC89.GroupID = null;
            this.retangleUC89.Location = new System.Drawing.Point(192, 810);
            this.retangleUC89.Name = "retangleUC89";
            this.retangleUC89.Size = new System.Drawing.Size(20, 20);
            this.retangleUC89.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC89.TabIndex = 215;
            // 
            // retangleUC90
            // 
            this.retangleUC90.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC90.ExtenderWidth = 2;
            this.retangleUC90.Fill = false;
            this.retangleUC90.GroupID = null;
            this.retangleUC90.Location = new System.Drawing.Point(127, 810);
            this.retangleUC90.Name = "retangleUC90";
            this.retangleUC90.Size = new System.Drawing.Size(20, 20);
            this.retangleUC90.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC90.TabIndex = 214;
            // 
            // retangleUC91
            // 
            this.retangleUC91.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC91.ExtenderWidth = 2;
            this.retangleUC91.Fill = false;
            this.retangleUC91.GroupID = null;
            this.retangleUC91.Location = new System.Drawing.Point(62, 810);
            this.retangleUC91.Name = "retangleUC91";
            this.retangleUC91.Size = new System.Drawing.Size(20, 20);
            this.retangleUC91.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC91.TabIndex = 213;
            // 
            // lineUC16
            // 
            this.lineUC16.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC16.ExtenderWidth = 3;
            this.lineUC16.Fill = false;
            this.lineUC16.GroupID = null;
            this.lineUC16.Location = new System.Drawing.Point(39, 786);
            this.lineUC16.Name = "lineUC16";
            this.lineUC16.Size = new System.Drawing.Size(200, 3);
            this.lineUC16.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC16.TabIndex = 212;
            // 
            // retangleUC92
            // 
            this.retangleUC92.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC92.ExtenderWidth = 3;
            this.retangleUC92.Fill = false;
            this.retangleUC92.GroupID = null;
            this.retangleUC92.Location = new System.Drawing.Point(39, 764);
            this.retangleUC92.Name = "retangleUC92";
            this.retangleUC92.Size = new System.Drawing.Size(200, 124);
            this.retangleUC92.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC92.TabIndex = 211;
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label97.Location = new System.Drawing.Point(271, 446);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(39, 19);
            this.label97.TabIndex = 236;
            this.label97.Text = "C16";
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(334, 515);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(53, 24);
            this.label98.TabIndex = 235;
            this.label98.Text = "連鎖迴路\r\n通訊斷訊";
            // 
            // retangleUC97
            // 
            this.retangleUC97.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC97.ExtenderWidth = 2;
            this.retangleUC97.Fill = false;
            this.retangleUC97.GroupID = null;
            this.retangleUC97.Location = new System.Drawing.Point(345, 539);
            this.retangleUC97.Name = "retangleUC97";
            this.retangleUC97.Size = new System.Drawing.Size(20, 20);
            this.retangleUC97.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC97.TabIndex = 234;
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(317, 474);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(70, 12);
            this.label100.TabIndex = 231;
            this.label100.Text = "cpu通訊異常";
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(265, 474);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(46, 12);
            this.label101.TabIndex = 230;
            this.label101.Text = "cpu異常";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Location = new System.Drawing.Point(201, 474);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(58, 12);
            this.label102.TabIndex = 229;
            this.label102.Text = "cpu運轉中";
            // 
            // retangleUC99
            // 
            this.retangleUC99.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC99.ExtenderWidth = 2;
            this.retangleUC99.Fill = false;
            this.retangleUC99.GroupID = null;
            this.retangleUC99.Location = new System.Drawing.Point(345, 490);
            this.retangleUC99.Name = "retangleUC99";
            this.retangleUC99.Size = new System.Drawing.Size(20, 20);
            this.retangleUC99.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC99.TabIndex = 228;
            // 
            // retangleUC100
            // 
            this.retangleUC100.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC100.ExtenderWidth = 2;
            this.retangleUC100.Fill = false;
            this.retangleUC100.GroupID = null;
            this.retangleUC100.Location = new System.Drawing.Point(280, 490);
            this.retangleUC100.Name = "retangleUC100";
            this.retangleUC100.Size = new System.Drawing.Size(20, 20);
            this.retangleUC100.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC100.TabIndex = 227;
            // 
            // retangleUC101
            // 
            this.retangleUC101.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC101.ExtenderWidth = 2;
            this.retangleUC101.Fill = false;
            this.retangleUC101.GroupID = null;
            this.retangleUC101.Location = new System.Drawing.Point(215, 490);
            this.retangleUC101.Name = "retangleUC101";
            this.retangleUC101.Size = new System.Drawing.Size(20, 20);
            this.retangleUC101.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC101.TabIndex = 226;
            // 
            // lineUC18
            // 
            this.lineUC18.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC18.ExtenderWidth = 3;
            this.lineUC18.Fill = false;
            this.lineUC18.GroupID = null;
            this.lineUC18.Location = new System.Drawing.Point(192, 466);
            this.lineUC18.Name = "lineUC18";
            this.lineUC18.Size = new System.Drawing.Size(200, 3);
            this.lineUC18.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC18.TabIndex = 225;
            // 
            // retangleUC102
            // 
            this.retangleUC102.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC102.ExtenderWidth = 3;
            this.retangleUC102.Fill = false;
            this.retangleUC102.GroupID = null;
            this.retangleUC102.Location = new System.Drawing.Point(192, 444);
            this.retangleUC102.Name = "retangleUC102";
            this.retangleUC102.Size = new System.Drawing.Size(200, 124);
            this.retangleUC102.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC102.TabIndex = 224;
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label103.Location = new System.Drawing.Point(271, 627);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(39, 19);
            this.label103.TabIndex = 249;
            this.label103.Text = "C28";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Location = new System.Drawing.Point(334, 696);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(53, 24);
            this.label104.TabIndex = 248;
            this.label104.Text = "連鎖迴路\r\n通訊斷訊";
            // 
            // retangleUC103
            // 
            this.retangleUC103.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC103.ExtenderWidth = 2;
            this.retangleUC103.Fill = false;
            this.retangleUC103.GroupID = null;
            this.retangleUC103.Location = new System.Drawing.Point(345, 720);
            this.retangleUC103.Name = "retangleUC103";
            this.retangleUC103.Size = new System.Drawing.Size(20, 20);
            this.retangleUC103.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC103.TabIndex = 247;
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Location = new System.Drawing.Point(317, 655);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(70, 12);
            this.label106.TabIndex = 244;
            this.label106.Text = "cpu通訊異常";
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Location = new System.Drawing.Point(265, 655);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(46, 12);
            this.label107.TabIndex = 243;
            this.label107.Text = "cpu異常";
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Location = new System.Drawing.Point(201, 655);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(58, 12);
            this.label108.TabIndex = 242;
            this.label108.Text = "cpu運轉中";
            // 
            // retangleUC105
            // 
            this.retangleUC105.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC105.ExtenderWidth = 2;
            this.retangleUC105.Fill = false;
            this.retangleUC105.GroupID = null;
            this.retangleUC105.Location = new System.Drawing.Point(345, 671);
            this.retangleUC105.Name = "retangleUC105";
            this.retangleUC105.Size = new System.Drawing.Size(20, 20);
            this.retangleUC105.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC105.TabIndex = 241;
            // 
            // retangleUC106
            // 
            this.retangleUC106.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC106.ExtenderWidth = 2;
            this.retangleUC106.Fill = false;
            this.retangleUC106.GroupID = null;
            this.retangleUC106.Location = new System.Drawing.Point(280, 671);
            this.retangleUC106.Name = "retangleUC106";
            this.retangleUC106.Size = new System.Drawing.Size(20, 20);
            this.retangleUC106.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC106.TabIndex = 240;
            // 
            // retangleUC107
            // 
            this.retangleUC107.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC107.ExtenderWidth = 2;
            this.retangleUC107.Fill = false;
            this.retangleUC107.GroupID = null;
            this.retangleUC107.Location = new System.Drawing.Point(215, 671);
            this.retangleUC107.Name = "retangleUC107";
            this.retangleUC107.Size = new System.Drawing.Size(20, 20);
            this.retangleUC107.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC107.TabIndex = 239;
            // 
            // lineUC19
            // 
            this.lineUC19.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC19.ExtenderWidth = 3;
            this.lineUC19.Fill = false;
            this.lineUC19.GroupID = null;
            this.lineUC19.Location = new System.Drawing.Point(192, 647);
            this.lineUC19.Name = "lineUC19";
            this.lineUC19.Size = new System.Drawing.Size(200, 3);
            this.lineUC19.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC19.TabIndex = 238;
            // 
            // retangleUC108
            // 
            this.retangleUC108.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC108.ExtenderWidth = 3;
            this.retangleUC108.Fill = false;
            this.retangleUC108.GroupID = null;
            this.retangleUC108.Location = new System.Drawing.Point(192, 625);
            this.retangleUC108.Name = "retangleUC108";
            this.retangleUC108.Size = new System.Drawing.Size(200, 124);
            this.retangleUC108.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC108.TabIndex = 237;
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label109.Location = new System.Drawing.Point(576, 627);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(39, 19);
            this.label109.TabIndex = 301;
            this.label109.Text = "C30";
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Location = new System.Drawing.Point(639, 696);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(53, 24);
            this.label110.TabIndex = 300;
            this.label110.Text = "連鎖迴路\r\n通訊斷訊";
            // 
            // retangleUC109
            // 
            this.retangleUC109.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC109.ExtenderWidth = 2;
            this.retangleUC109.Fill = false;
            this.retangleUC109.GroupID = null;
            this.retangleUC109.Location = new System.Drawing.Point(650, 720);
            this.retangleUC109.Name = "retangleUC109";
            this.retangleUC109.Size = new System.Drawing.Size(20, 20);
            this.retangleUC109.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC109.TabIndex = 299;
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Location = new System.Drawing.Point(622, 655);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(70, 12);
            this.label112.TabIndex = 296;
            this.label112.Text = "cpu通訊異常";
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Location = new System.Drawing.Point(570, 655);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(46, 12);
            this.label113.TabIndex = 295;
            this.label113.Text = "cpu異常";
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Location = new System.Drawing.Point(506, 655);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(58, 12);
            this.label114.TabIndex = 294;
            this.label114.Text = "cpu運轉中";
            // 
            // retangleUC111
            // 
            this.retangleUC111.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC111.ExtenderWidth = 2;
            this.retangleUC111.Fill = false;
            this.retangleUC111.GroupID = null;
            this.retangleUC111.Location = new System.Drawing.Point(650, 671);
            this.retangleUC111.Name = "retangleUC111";
            this.retangleUC111.Size = new System.Drawing.Size(20, 20);
            this.retangleUC111.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC111.TabIndex = 293;
            // 
            // retangleUC112
            // 
            this.retangleUC112.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC112.ExtenderWidth = 2;
            this.retangleUC112.Fill = false;
            this.retangleUC112.GroupID = null;
            this.retangleUC112.Location = new System.Drawing.Point(585, 671);
            this.retangleUC112.Name = "retangleUC112";
            this.retangleUC112.Size = new System.Drawing.Size(20, 20);
            this.retangleUC112.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC112.TabIndex = 292;
            // 
            // retangleUC113
            // 
            this.retangleUC113.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC113.ExtenderWidth = 2;
            this.retangleUC113.Fill = false;
            this.retangleUC113.GroupID = null;
            this.retangleUC113.Location = new System.Drawing.Point(520, 671);
            this.retangleUC113.Name = "retangleUC113";
            this.retangleUC113.Size = new System.Drawing.Size(20, 20);
            this.retangleUC113.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC113.TabIndex = 291;
            // 
            // lineUC20
            // 
            this.lineUC20.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC20.ExtenderWidth = 3;
            this.lineUC20.Fill = false;
            this.lineUC20.GroupID = null;
            this.lineUC20.Location = new System.Drawing.Point(497, 647);
            this.lineUC20.Name = "lineUC20";
            this.lineUC20.Size = new System.Drawing.Size(200, 3);
            this.lineUC20.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC20.TabIndex = 290;
            // 
            // retangleUC114
            // 
            this.retangleUC114.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC114.ExtenderWidth = 3;
            this.retangleUC114.Fill = false;
            this.retangleUC114.GroupID = null;
            this.retangleUC114.Location = new System.Drawing.Point(497, 625);
            this.retangleUC114.Name = "retangleUC114";
            this.retangleUC114.Size = new System.Drawing.Size(200, 124);
            this.retangleUC114.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC114.TabIndex = 289;
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label115.Location = new System.Drawing.Point(576, 446);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(39, 19);
            this.label115.TabIndex = 288;
            this.label115.Text = "C18";
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Location = new System.Drawing.Point(639, 515);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(53, 24);
            this.label116.TabIndex = 287;
            this.label116.Text = "連鎖迴路\r\n通訊斷訊";
            // 
            // retangleUC115
            // 
            this.retangleUC115.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC115.ExtenderWidth = 2;
            this.retangleUC115.Fill = false;
            this.retangleUC115.GroupID = null;
            this.retangleUC115.Location = new System.Drawing.Point(650, 539);
            this.retangleUC115.Name = "retangleUC115";
            this.retangleUC115.Size = new System.Drawing.Size(20, 20);
            this.retangleUC115.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC115.TabIndex = 286;
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Location = new System.Drawing.Point(622, 474);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(70, 12);
            this.label118.TabIndex = 283;
            this.label118.Text = "cpu通訊異常";
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Location = new System.Drawing.Point(570, 474);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(46, 12);
            this.label119.TabIndex = 282;
            this.label119.Text = "cpu異常";
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Location = new System.Drawing.Point(506, 474);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(58, 12);
            this.label120.TabIndex = 281;
            this.label120.Text = "cpu運轉中";
            // 
            // retangleUC117
            // 
            this.retangleUC117.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC117.ExtenderWidth = 2;
            this.retangleUC117.Fill = false;
            this.retangleUC117.GroupID = null;
            this.retangleUC117.Location = new System.Drawing.Point(650, 490);
            this.retangleUC117.Name = "retangleUC117";
            this.retangleUC117.Size = new System.Drawing.Size(20, 20);
            this.retangleUC117.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC117.TabIndex = 280;
            // 
            // retangleUC118
            // 
            this.retangleUC118.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC118.ExtenderWidth = 2;
            this.retangleUC118.Fill = false;
            this.retangleUC118.GroupID = null;
            this.retangleUC118.Location = new System.Drawing.Point(585, 490);
            this.retangleUC118.Name = "retangleUC118";
            this.retangleUC118.Size = new System.Drawing.Size(20, 20);
            this.retangleUC118.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC118.TabIndex = 279;
            // 
            // retangleUC119
            // 
            this.retangleUC119.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC119.ExtenderWidth = 2;
            this.retangleUC119.Fill = false;
            this.retangleUC119.GroupID = null;
            this.retangleUC119.Location = new System.Drawing.Point(520, 490);
            this.retangleUC119.Name = "retangleUC119";
            this.retangleUC119.Size = new System.Drawing.Size(20, 20);
            this.retangleUC119.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC119.TabIndex = 278;
            // 
            // lineUC21
            // 
            this.lineUC21.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC21.ExtenderWidth = 3;
            this.lineUC21.Fill = false;
            this.lineUC21.GroupID = null;
            this.lineUC21.Location = new System.Drawing.Point(497, 466);
            this.lineUC21.Name = "lineUC21";
            this.lineUC21.Size = new System.Drawing.Size(200, 3);
            this.lineUC21.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC21.TabIndex = 277;
            // 
            // retangleUC120
            // 
            this.retangleUC120.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC120.ExtenderWidth = 3;
            this.retangleUC120.Fill = false;
            this.retangleUC120.GroupID = null;
            this.retangleUC120.Location = new System.Drawing.Point(497, 444);
            this.retangleUC120.Name = "retangleUC120";
            this.retangleUC120.Size = new System.Drawing.Size(200, 124);
            this.retangleUC120.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC120.TabIndex = 276;
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label121.Location = new System.Drawing.Point(427, 766);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(39, 19);
            this.label121.TabIndex = 275;
            this.label121.Text = "C29";
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Location = new System.Drawing.Point(490, 835);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(53, 24);
            this.label122.TabIndex = 274;
            this.label122.Text = "連鎖迴路\r\n通訊斷訊";
            // 
            // retangleUC121
            // 
            this.retangleUC121.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC121.ExtenderWidth = 2;
            this.retangleUC121.Fill = false;
            this.retangleUC121.GroupID = null;
            this.retangleUC121.Location = new System.Drawing.Point(501, 859);
            this.retangleUC121.Name = "retangleUC121";
            this.retangleUC121.Size = new System.Drawing.Size(20, 20);
            this.retangleUC121.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC121.TabIndex = 273;
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Location = new System.Drawing.Point(473, 794);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(70, 12);
            this.label124.TabIndex = 270;
            this.label124.Text = "cpu通訊異常";
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Location = new System.Drawing.Point(421, 794);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(46, 12);
            this.label125.TabIndex = 269;
            this.label125.Text = "cpu異常";
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Location = new System.Drawing.Point(357, 794);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(58, 12);
            this.label126.TabIndex = 268;
            this.label126.Text = "cpu運轉中";
            // 
            // retangleUC123
            // 
            this.retangleUC123.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC123.ExtenderWidth = 2;
            this.retangleUC123.Fill = false;
            this.retangleUC123.GroupID = null;
            this.retangleUC123.Location = new System.Drawing.Point(501, 810);
            this.retangleUC123.Name = "retangleUC123";
            this.retangleUC123.Size = new System.Drawing.Size(20, 20);
            this.retangleUC123.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC123.TabIndex = 267;
            // 
            // retangleUC124
            // 
            this.retangleUC124.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC124.ExtenderWidth = 2;
            this.retangleUC124.Fill = false;
            this.retangleUC124.GroupID = null;
            this.retangleUC124.Location = new System.Drawing.Point(436, 810);
            this.retangleUC124.Name = "retangleUC124";
            this.retangleUC124.Size = new System.Drawing.Size(20, 20);
            this.retangleUC124.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC124.TabIndex = 266;
            // 
            // retangleUC125
            // 
            this.retangleUC125.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC125.ExtenderWidth = 2;
            this.retangleUC125.Fill = false;
            this.retangleUC125.GroupID = null;
            this.retangleUC125.Location = new System.Drawing.Point(371, 810);
            this.retangleUC125.Name = "retangleUC125";
            this.retangleUC125.Size = new System.Drawing.Size(20, 20);
            this.retangleUC125.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC125.TabIndex = 265;
            // 
            // lineUC22
            // 
            this.lineUC22.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC22.ExtenderWidth = 3;
            this.lineUC22.Fill = false;
            this.lineUC22.GroupID = null;
            this.lineUC22.Location = new System.Drawing.Point(348, 786);
            this.lineUC22.Name = "lineUC22";
            this.lineUC22.Size = new System.Drawing.Size(200, 3);
            this.lineUC22.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC22.TabIndex = 264;
            // 
            // retangleUC126
            // 
            this.retangleUC126.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC126.ExtenderWidth = 3;
            this.retangleUC126.Fill = false;
            this.retangleUC126.GroupID = null;
            this.retangleUC126.Location = new System.Drawing.Point(348, 764);
            this.retangleUC126.Name = "retangleUC126";
            this.retangleUC126.Size = new System.Drawing.Size(200, 124);
            this.retangleUC126.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC126.TabIndex = 263;
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label127.Location = new System.Drawing.Point(427, 307);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(39, 19);
            this.label127.TabIndex = 262;
            this.label127.Text = "C17";
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.Location = new System.Drawing.Point(490, 376);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(53, 24);
            this.label128.TabIndex = 261;
            this.label128.Text = "連鎖迴路\r\n通訊斷訊";
            // 
            // retangleUC127
            // 
            this.retangleUC127.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC127.ExtenderWidth = 2;
            this.retangleUC127.Fill = false;
            this.retangleUC127.GroupID = null;
            this.retangleUC127.Location = new System.Drawing.Point(501, 400);
            this.retangleUC127.Name = "retangleUC127";
            this.retangleUC127.Size = new System.Drawing.Size(20, 20);
            this.retangleUC127.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC127.TabIndex = 260;
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Location = new System.Drawing.Point(473, 335);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(70, 12);
            this.label130.TabIndex = 257;
            this.label130.Text = "cpu通訊異常";
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Location = new System.Drawing.Point(421, 335);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(46, 12);
            this.label131.TabIndex = 256;
            this.label131.Text = "cpu異常";
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.Location = new System.Drawing.Point(357, 335);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(58, 12);
            this.label132.TabIndex = 255;
            this.label132.Text = "cpu運轉中";
            // 
            // retangleUC129
            // 
            this.retangleUC129.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC129.ExtenderWidth = 2;
            this.retangleUC129.Fill = false;
            this.retangleUC129.GroupID = null;
            this.retangleUC129.Location = new System.Drawing.Point(501, 351);
            this.retangleUC129.Name = "retangleUC129";
            this.retangleUC129.Size = new System.Drawing.Size(20, 20);
            this.retangleUC129.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC129.TabIndex = 254;
            // 
            // retangleUC130
            // 
            this.retangleUC130.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC130.ExtenderWidth = 2;
            this.retangleUC130.Fill = false;
            this.retangleUC130.GroupID = null;
            this.retangleUC130.Location = new System.Drawing.Point(436, 351);
            this.retangleUC130.Name = "retangleUC130";
            this.retangleUC130.Size = new System.Drawing.Size(20, 20);
            this.retangleUC130.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC130.TabIndex = 253;
            // 
            // retangleUC131
            // 
            this.retangleUC131.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC131.ExtenderWidth = 2;
            this.retangleUC131.Fill = false;
            this.retangleUC131.GroupID = null;
            this.retangleUC131.Location = new System.Drawing.Point(371, 351);
            this.retangleUC131.Name = "retangleUC131";
            this.retangleUC131.Size = new System.Drawing.Size(20, 20);
            this.retangleUC131.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC131.TabIndex = 252;
            // 
            // lineUC23
            // 
            this.lineUC23.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC23.ExtenderWidth = 3;
            this.lineUC23.Fill = false;
            this.lineUC23.GroupID = null;
            this.lineUC23.Location = new System.Drawing.Point(348, 327);
            this.lineUC23.Name = "lineUC23";
            this.lineUC23.Size = new System.Drawing.Size(200, 3);
            this.lineUC23.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC23.TabIndex = 251;
            // 
            // retangleUC132
            // 
            this.retangleUC132.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC132.ExtenderWidth = 3;
            this.retangleUC132.Fill = false;
            this.retangleUC132.GroupID = null;
            this.retangleUC132.Location = new System.Drawing.Point(348, 305);
            this.retangleUC132.Name = "retangleUC132";
            this.retangleUC132.Size = new System.Drawing.Size(200, 124);
            this.retangleUC132.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC132.TabIndex = 250;
            // 
            // label133
            // 
            this.label133.AutoSize = true;
            this.label133.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label133.Location = new System.Drawing.Point(882, 629);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(39, 19);
            this.label133.TabIndex = 353;
            this.label133.Text = "C32";
            // 
            // label134
            // 
            this.label134.AutoSize = true;
            this.label134.Location = new System.Drawing.Point(945, 698);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(53, 24);
            this.label134.TabIndex = 352;
            this.label134.Text = "連鎖迴路\r\n通訊斷訊";
            // 
            // retangleUC133
            // 
            this.retangleUC133.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC133.ExtenderWidth = 2;
            this.retangleUC133.Fill = false;
            this.retangleUC133.GroupID = null;
            this.retangleUC133.Location = new System.Drawing.Point(956, 722);
            this.retangleUC133.Name = "retangleUC133";
            this.retangleUC133.Size = new System.Drawing.Size(20, 20);
            this.retangleUC133.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC133.TabIndex = 351;
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.Location = new System.Drawing.Point(928, 657);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(70, 12);
            this.label136.TabIndex = 348;
            this.label136.Text = "cpu通訊異常";
            // 
            // label137
            // 
            this.label137.AutoSize = true;
            this.label137.Location = new System.Drawing.Point(876, 657);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(46, 12);
            this.label137.TabIndex = 347;
            this.label137.Text = "cpu異常";
            // 
            // label138
            // 
            this.label138.AutoSize = true;
            this.label138.Location = new System.Drawing.Point(812, 657);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(58, 12);
            this.label138.TabIndex = 346;
            this.label138.Text = "cpu運轉中";
            // 
            // retangleUC135
            // 
            this.retangleUC135.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC135.ExtenderWidth = 2;
            this.retangleUC135.Fill = false;
            this.retangleUC135.GroupID = null;
            this.retangleUC135.Location = new System.Drawing.Point(956, 673);
            this.retangleUC135.Name = "retangleUC135";
            this.retangleUC135.Size = new System.Drawing.Size(20, 20);
            this.retangleUC135.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC135.TabIndex = 345;
            // 
            // retangleUC136
            // 
            this.retangleUC136.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC136.ExtenderWidth = 2;
            this.retangleUC136.Fill = false;
            this.retangleUC136.GroupID = null;
            this.retangleUC136.Location = new System.Drawing.Point(891, 673);
            this.retangleUC136.Name = "retangleUC136";
            this.retangleUC136.Size = new System.Drawing.Size(20, 20);
            this.retangleUC136.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC136.TabIndex = 344;
            // 
            // retangleUC137
            // 
            this.retangleUC137.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC137.ExtenderWidth = 2;
            this.retangleUC137.Fill = false;
            this.retangleUC137.GroupID = null;
            this.retangleUC137.Location = new System.Drawing.Point(826, 673);
            this.retangleUC137.Name = "retangleUC137";
            this.retangleUC137.Size = new System.Drawing.Size(20, 20);
            this.retangleUC137.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC137.TabIndex = 343;
            // 
            // lineUC24
            // 
            this.lineUC24.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC24.ExtenderWidth = 3;
            this.lineUC24.Fill = false;
            this.lineUC24.GroupID = null;
            this.lineUC24.Location = new System.Drawing.Point(803, 649);
            this.lineUC24.Name = "lineUC24";
            this.lineUC24.Size = new System.Drawing.Size(200, 3);
            this.lineUC24.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC24.TabIndex = 342;
            // 
            // retangleUC138
            // 
            this.retangleUC138.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC138.ExtenderWidth = 3;
            this.retangleUC138.Fill = false;
            this.retangleUC138.GroupID = null;
            this.retangleUC138.Location = new System.Drawing.Point(803, 627);
            this.retangleUC138.Name = "retangleUC138";
            this.retangleUC138.Size = new System.Drawing.Size(200, 124);
            this.retangleUC138.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC138.TabIndex = 341;
            // 
            // label139
            // 
            this.label139.AutoSize = true;
            this.label139.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label139.Location = new System.Drawing.Point(882, 446);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(39, 19);
            this.label139.TabIndex = 340;
            this.label139.Text = "C20";
            // 
            // label140
            // 
            this.label140.AutoSize = true;
            this.label140.Location = new System.Drawing.Point(945, 515);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(53, 24);
            this.label140.TabIndex = 339;
            this.label140.Text = "連鎖迴路\r\n通訊斷訊";
            // 
            // retangleUC139
            // 
            this.retangleUC139.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC139.ExtenderWidth = 2;
            this.retangleUC139.Fill = false;
            this.retangleUC139.GroupID = null;
            this.retangleUC139.Location = new System.Drawing.Point(956, 539);
            this.retangleUC139.Name = "retangleUC139";
            this.retangleUC139.Size = new System.Drawing.Size(20, 20);
            this.retangleUC139.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC139.TabIndex = 338;
            // 
            // label142
            // 
            this.label142.AutoSize = true;
            this.label142.Location = new System.Drawing.Point(928, 474);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(70, 12);
            this.label142.TabIndex = 335;
            this.label142.Text = "cpu通訊異常";
            // 
            // label143
            // 
            this.label143.AutoSize = true;
            this.label143.Location = new System.Drawing.Point(876, 474);
            this.label143.Name = "label143";
            this.label143.Size = new System.Drawing.Size(46, 12);
            this.label143.TabIndex = 334;
            this.label143.Text = "cpu異常";
            // 
            // label144
            // 
            this.label144.AutoSize = true;
            this.label144.Location = new System.Drawing.Point(812, 474);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(58, 12);
            this.label144.TabIndex = 333;
            this.label144.Text = "cpu運轉中";
            // 
            // retangleUC141
            // 
            this.retangleUC141.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC141.ExtenderWidth = 2;
            this.retangleUC141.Fill = false;
            this.retangleUC141.GroupID = null;
            this.retangleUC141.Location = new System.Drawing.Point(956, 490);
            this.retangleUC141.Name = "retangleUC141";
            this.retangleUC141.Size = new System.Drawing.Size(20, 20);
            this.retangleUC141.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC141.TabIndex = 332;
            // 
            // retangleUC142
            // 
            this.retangleUC142.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC142.ExtenderWidth = 2;
            this.retangleUC142.Fill = false;
            this.retangleUC142.GroupID = null;
            this.retangleUC142.Location = new System.Drawing.Point(891, 490);
            this.retangleUC142.Name = "retangleUC142";
            this.retangleUC142.Size = new System.Drawing.Size(20, 20);
            this.retangleUC142.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC142.TabIndex = 331;
            // 
            // retangleUC143
            // 
            this.retangleUC143.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC143.ExtenderWidth = 2;
            this.retangleUC143.Fill = false;
            this.retangleUC143.GroupID = null;
            this.retangleUC143.Location = new System.Drawing.Point(826, 490);
            this.retangleUC143.Name = "retangleUC143";
            this.retangleUC143.Size = new System.Drawing.Size(20, 20);
            this.retangleUC143.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC143.TabIndex = 330;
            // 
            // lineUC25
            // 
            this.lineUC25.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC25.ExtenderWidth = 3;
            this.lineUC25.Fill = false;
            this.lineUC25.GroupID = null;
            this.lineUC25.Location = new System.Drawing.Point(803, 466);
            this.lineUC25.Name = "lineUC25";
            this.lineUC25.Size = new System.Drawing.Size(200, 3);
            this.lineUC25.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC25.TabIndex = 329;
            // 
            // retangleUC144
            // 
            this.retangleUC144.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC144.ExtenderWidth = 3;
            this.retangleUC144.Fill = false;
            this.retangleUC144.GroupID = null;
            this.retangleUC144.Location = new System.Drawing.Point(803, 444);
            this.retangleUC144.Name = "retangleUC144";
            this.retangleUC144.Size = new System.Drawing.Size(200, 124);
            this.retangleUC144.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC144.TabIndex = 328;
            // 
            // label145
            // 
            this.label145.AutoSize = true;
            this.label145.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label145.Location = new System.Drawing.Point(729, 766);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(39, 19);
            this.label145.TabIndex = 327;
            this.label145.Text = "C31";
            // 
            // label146
            // 
            this.label146.AutoSize = true;
            this.label146.Location = new System.Drawing.Point(792, 835);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(53, 24);
            this.label146.TabIndex = 326;
            this.label146.Text = "連鎖迴路\r\n通訊斷訊";
            // 
            // retangleUC145
            // 
            this.retangleUC145.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC145.ExtenderWidth = 2;
            this.retangleUC145.Fill = false;
            this.retangleUC145.GroupID = null;
            this.retangleUC145.Location = new System.Drawing.Point(803, 859);
            this.retangleUC145.Name = "retangleUC145";
            this.retangleUC145.Size = new System.Drawing.Size(20, 20);
            this.retangleUC145.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC145.TabIndex = 325;
            // 
            // label148
            // 
            this.label148.AutoSize = true;
            this.label148.Location = new System.Drawing.Point(775, 794);
            this.label148.Name = "label148";
            this.label148.Size = new System.Drawing.Size(70, 12);
            this.label148.TabIndex = 322;
            this.label148.Text = "cpu通訊異常";
            // 
            // label149
            // 
            this.label149.AutoSize = true;
            this.label149.Location = new System.Drawing.Point(723, 794);
            this.label149.Name = "label149";
            this.label149.Size = new System.Drawing.Size(46, 12);
            this.label149.TabIndex = 321;
            this.label149.Text = "cpu異常";
            // 
            // label150
            // 
            this.label150.AutoSize = true;
            this.label150.Location = new System.Drawing.Point(659, 794);
            this.label150.Name = "label150";
            this.label150.Size = new System.Drawing.Size(58, 12);
            this.label150.TabIndex = 320;
            this.label150.Text = "cpu運轉中";
            // 
            // retangleUC147
            // 
            this.retangleUC147.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC147.ExtenderWidth = 2;
            this.retangleUC147.Fill = false;
            this.retangleUC147.GroupID = null;
            this.retangleUC147.Location = new System.Drawing.Point(803, 810);
            this.retangleUC147.Name = "retangleUC147";
            this.retangleUC147.Size = new System.Drawing.Size(20, 20);
            this.retangleUC147.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC147.TabIndex = 319;
            // 
            // retangleUC148
            // 
            this.retangleUC148.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC148.ExtenderWidth = 2;
            this.retangleUC148.Fill = false;
            this.retangleUC148.GroupID = null;
            this.retangleUC148.Location = new System.Drawing.Point(738, 810);
            this.retangleUC148.Name = "retangleUC148";
            this.retangleUC148.Size = new System.Drawing.Size(20, 20);
            this.retangleUC148.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC148.TabIndex = 318;
            // 
            // retangleUC149
            // 
            this.retangleUC149.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC149.ExtenderWidth = 2;
            this.retangleUC149.Fill = false;
            this.retangleUC149.GroupID = null;
            this.retangleUC149.Location = new System.Drawing.Point(673, 810);
            this.retangleUC149.Name = "retangleUC149";
            this.retangleUC149.Size = new System.Drawing.Size(20, 20);
            this.retangleUC149.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC149.TabIndex = 317;
            // 
            // lineUC26
            // 
            this.lineUC26.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC26.ExtenderWidth = 3;
            this.lineUC26.Fill = false;
            this.lineUC26.GroupID = null;
            this.lineUC26.Location = new System.Drawing.Point(650, 786);
            this.lineUC26.Name = "lineUC26";
            this.lineUC26.Size = new System.Drawing.Size(200, 3);
            this.lineUC26.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC26.TabIndex = 316;
            // 
            // retangleUC150
            // 
            this.retangleUC150.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC150.ExtenderWidth = 3;
            this.retangleUC150.Fill = false;
            this.retangleUC150.GroupID = null;
            this.retangleUC150.Location = new System.Drawing.Point(650, 764);
            this.retangleUC150.Name = "retangleUC150";
            this.retangleUC150.Size = new System.Drawing.Size(200, 124);
            this.retangleUC150.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC150.TabIndex = 315;
            // 
            // label151
            // 
            this.label151.AutoSize = true;
            this.label151.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label151.Location = new System.Drawing.Point(729, 307);
            this.label151.Name = "label151";
            this.label151.Size = new System.Drawing.Size(39, 19);
            this.label151.TabIndex = 314;
            this.label151.Text = "C19";
            // 
            // label152
            // 
            this.label152.AutoSize = true;
            this.label152.Location = new System.Drawing.Point(792, 376);
            this.label152.Name = "label152";
            this.label152.Size = new System.Drawing.Size(53, 24);
            this.label152.TabIndex = 313;
            this.label152.Text = "連鎖迴路\r\n通訊斷訊";
            // 
            // retangleUC151
            // 
            this.retangleUC151.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC151.ExtenderWidth = 2;
            this.retangleUC151.Fill = false;
            this.retangleUC151.GroupID = null;
            this.retangleUC151.Location = new System.Drawing.Point(803, 400);
            this.retangleUC151.Name = "retangleUC151";
            this.retangleUC151.Size = new System.Drawing.Size(20, 20);
            this.retangleUC151.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC151.TabIndex = 312;
            // 
            // label154
            // 
            this.label154.AutoSize = true;
            this.label154.Location = new System.Drawing.Point(775, 335);
            this.label154.Name = "label154";
            this.label154.Size = new System.Drawing.Size(70, 12);
            this.label154.TabIndex = 309;
            this.label154.Text = "cpu通訊異常";
            // 
            // label155
            // 
            this.label155.AutoSize = true;
            this.label155.Location = new System.Drawing.Point(723, 335);
            this.label155.Name = "label155";
            this.label155.Size = new System.Drawing.Size(46, 12);
            this.label155.TabIndex = 308;
            this.label155.Text = "cpu異常";
            // 
            // label156
            // 
            this.label156.AutoSize = true;
            this.label156.Location = new System.Drawing.Point(659, 335);
            this.label156.Name = "label156";
            this.label156.Size = new System.Drawing.Size(58, 12);
            this.label156.TabIndex = 307;
            this.label156.Text = "cpu運轉中";
            // 
            // retangleUC153
            // 
            this.retangleUC153.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC153.ExtenderWidth = 2;
            this.retangleUC153.Fill = false;
            this.retangleUC153.GroupID = null;
            this.retangleUC153.Location = new System.Drawing.Point(803, 351);
            this.retangleUC153.Name = "retangleUC153";
            this.retangleUC153.Size = new System.Drawing.Size(20, 20);
            this.retangleUC153.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC153.TabIndex = 306;
            // 
            // retangleUC154
            // 
            this.retangleUC154.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC154.ExtenderWidth = 2;
            this.retangleUC154.Fill = false;
            this.retangleUC154.GroupID = null;
            this.retangleUC154.Location = new System.Drawing.Point(738, 351);
            this.retangleUC154.Name = "retangleUC154";
            this.retangleUC154.Size = new System.Drawing.Size(20, 20);
            this.retangleUC154.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC154.TabIndex = 305;
            // 
            // retangleUC155
            // 
            this.retangleUC155.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC155.ExtenderWidth = 2;
            this.retangleUC155.Fill = false;
            this.retangleUC155.GroupID = null;
            this.retangleUC155.Location = new System.Drawing.Point(673, 351);
            this.retangleUC155.Name = "retangleUC155";
            this.retangleUC155.Size = new System.Drawing.Size(20, 20);
            this.retangleUC155.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC155.TabIndex = 304;
            // 
            // lineUC27
            // 
            this.lineUC27.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC27.ExtenderWidth = 3;
            this.lineUC27.Fill = false;
            this.lineUC27.GroupID = null;
            this.lineUC27.Location = new System.Drawing.Point(650, 327);
            this.lineUC27.Name = "lineUC27";
            this.lineUC27.Size = new System.Drawing.Size(200, 3);
            this.lineUC27.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC27.TabIndex = 303;
            // 
            // retangleUC156
            // 
            this.retangleUC156.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC156.ExtenderWidth = 3;
            this.retangleUC156.Fill = false;
            this.retangleUC156.GroupID = null;
            this.retangleUC156.Location = new System.Drawing.Point(650, 305);
            this.retangleUC156.Name = "retangleUC156";
            this.retangleUC156.Size = new System.Drawing.Size(200, 124);
            this.retangleUC156.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC156.TabIndex = 302;
            // 
            // label157
            // 
            this.label157.AutoSize = true;
            this.label157.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label157.Location = new System.Drawing.Point(1188, 627);
            this.label157.Name = "label157";
            this.label157.Size = new System.Drawing.Size(39, 19);
            this.label157.TabIndex = 405;
            this.label157.Text = "C34";
            // 
            // label158
            // 
            this.label158.AutoSize = true;
            this.label158.Location = new System.Drawing.Point(1251, 696);
            this.label158.Name = "label158";
            this.label158.Size = new System.Drawing.Size(53, 24);
            this.label158.TabIndex = 404;
            this.label158.Text = "連鎖迴路\r\n通訊斷訊";
            // 
            // retangleUC157
            // 
            this.retangleUC157.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC157.ExtenderWidth = 2;
            this.retangleUC157.Fill = false;
            this.retangleUC157.GroupID = null;
            this.retangleUC157.Location = new System.Drawing.Point(1262, 720);
            this.retangleUC157.Name = "retangleUC157";
            this.retangleUC157.Size = new System.Drawing.Size(20, 20);
            this.retangleUC157.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC157.TabIndex = 403;
            // 
            // label160
            // 
            this.label160.AutoSize = true;
            this.label160.Location = new System.Drawing.Point(1234, 655);
            this.label160.Name = "label160";
            this.label160.Size = new System.Drawing.Size(70, 12);
            this.label160.TabIndex = 400;
            this.label160.Text = "cpu通訊異常";
            // 
            // label161
            // 
            this.label161.AutoSize = true;
            this.label161.Location = new System.Drawing.Point(1182, 655);
            this.label161.Name = "label161";
            this.label161.Size = new System.Drawing.Size(46, 12);
            this.label161.TabIndex = 399;
            this.label161.Text = "cpu異常";
            // 
            // label162
            // 
            this.label162.AutoSize = true;
            this.label162.Location = new System.Drawing.Point(1118, 655);
            this.label162.Name = "label162";
            this.label162.Size = new System.Drawing.Size(58, 12);
            this.label162.TabIndex = 398;
            this.label162.Text = "cpu運轉中";
            // 
            // retangleUC159
            // 
            this.retangleUC159.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC159.ExtenderWidth = 2;
            this.retangleUC159.Fill = false;
            this.retangleUC159.GroupID = null;
            this.retangleUC159.Location = new System.Drawing.Point(1262, 671);
            this.retangleUC159.Name = "retangleUC159";
            this.retangleUC159.Size = new System.Drawing.Size(20, 20);
            this.retangleUC159.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC159.TabIndex = 397;
            // 
            // retangleUC160
            // 
            this.retangleUC160.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC160.ExtenderWidth = 2;
            this.retangleUC160.Fill = false;
            this.retangleUC160.GroupID = null;
            this.retangleUC160.Location = new System.Drawing.Point(1197, 671);
            this.retangleUC160.Name = "retangleUC160";
            this.retangleUC160.Size = new System.Drawing.Size(20, 20);
            this.retangleUC160.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC160.TabIndex = 396;
            // 
            // retangleUC161
            // 
            this.retangleUC161.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC161.ExtenderWidth = 2;
            this.retangleUC161.Fill = false;
            this.retangleUC161.GroupID = null;
            this.retangleUC161.Location = new System.Drawing.Point(1132, 671);
            this.retangleUC161.Name = "retangleUC161";
            this.retangleUC161.Size = new System.Drawing.Size(20, 20);
            this.retangleUC161.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC161.TabIndex = 395;
            // 
            // lineUC28
            // 
            this.lineUC28.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC28.ExtenderWidth = 3;
            this.lineUC28.Fill = false;
            this.lineUC28.GroupID = null;
            this.lineUC28.Location = new System.Drawing.Point(1109, 647);
            this.lineUC28.Name = "lineUC28";
            this.lineUC28.Size = new System.Drawing.Size(200, 3);
            this.lineUC28.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC28.TabIndex = 394;
            // 
            // retangleUC162
            // 
            this.retangleUC162.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC162.ExtenderWidth = 3;
            this.retangleUC162.Fill = false;
            this.retangleUC162.GroupID = null;
            this.retangleUC162.Location = new System.Drawing.Point(1109, 625);
            this.retangleUC162.Name = "retangleUC162";
            this.retangleUC162.Size = new System.Drawing.Size(200, 124);
            this.retangleUC162.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC162.TabIndex = 393;
            // 
            // label163
            // 
            this.label163.AutoSize = true;
            this.label163.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label163.Location = new System.Drawing.Point(1188, 446);
            this.label163.Name = "label163";
            this.label163.Size = new System.Drawing.Size(39, 19);
            this.label163.TabIndex = 392;
            this.label163.Text = "C22";
            // 
            // label164
            // 
            this.label164.AutoSize = true;
            this.label164.Location = new System.Drawing.Point(1251, 515);
            this.label164.Name = "label164";
            this.label164.Size = new System.Drawing.Size(53, 24);
            this.label164.TabIndex = 391;
            this.label164.Text = "連鎖迴路\r\n通訊斷訊";
            // 
            // retangleUC163
            // 
            this.retangleUC163.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC163.ExtenderWidth = 2;
            this.retangleUC163.Fill = false;
            this.retangleUC163.GroupID = null;
            this.retangleUC163.Location = new System.Drawing.Point(1262, 539);
            this.retangleUC163.Name = "retangleUC163";
            this.retangleUC163.Size = new System.Drawing.Size(20, 20);
            this.retangleUC163.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC163.TabIndex = 390;
            // 
            // label166
            // 
            this.label166.AutoSize = true;
            this.label166.Location = new System.Drawing.Point(1234, 474);
            this.label166.Name = "label166";
            this.label166.Size = new System.Drawing.Size(70, 12);
            this.label166.TabIndex = 387;
            this.label166.Text = "cpu通訊異常";
            // 
            // label167
            // 
            this.label167.AutoSize = true;
            this.label167.Location = new System.Drawing.Point(1182, 474);
            this.label167.Name = "label167";
            this.label167.Size = new System.Drawing.Size(46, 12);
            this.label167.TabIndex = 386;
            this.label167.Text = "cpu異常";
            // 
            // label168
            // 
            this.label168.AutoSize = true;
            this.label168.Location = new System.Drawing.Point(1118, 474);
            this.label168.Name = "label168";
            this.label168.Size = new System.Drawing.Size(58, 12);
            this.label168.TabIndex = 385;
            this.label168.Text = "cpu運轉中";
            // 
            // retangleUC165
            // 
            this.retangleUC165.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC165.ExtenderWidth = 2;
            this.retangleUC165.Fill = false;
            this.retangleUC165.GroupID = null;
            this.retangleUC165.Location = new System.Drawing.Point(1262, 490);
            this.retangleUC165.Name = "retangleUC165";
            this.retangleUC165.Size = new System.Drawing.Size(20, 20);
            this.retangleUC165.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC165.TabIndex = 384;
            // 
            // retangleUC166
            // 
            this.retangleUC166.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC166.ExtenderWidth = 2;
            this.retangleUC166.Fill = false;
            this.retangleUC166.GroupID = null;
            this.retangleUC166.Location = new System.Drawing.Point(1197, 490);
            this.retangleUC166.Name = "retangleUC166";
            this.retangleUC166.Size = new System.Drawing.Size(20, 20);
            this.retangleUC166.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC166.TabIndex = 383;
            // 
            // retangleUC167
            // 
            this.retangleUC167.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC167.ExtenderWidth = 2;
            this.retangleUC167.Fill = false;
            this.retangleUC167.GroupID = null;
            this.retangleUC167.Location = new System.Drawing.Point(1132, 490);
            this.retangleUC167.Name = "retangleUC167";
            this.retangleUC167.Size = new System.Drawing.Size(20, 20);
            this.retangleUC167.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC167.TabIndex = 382;
            // 
            // lineUC29
            // 
            this.lineUC29.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC29.ExtenderWidth = 3;
            this.lineUC29.Fill = false;
            this.lineUC29.GroupID = null;
            this.lineUC29.Location = new System.Drawing.Point(1109, 466);
            this.lineUC29.Name = "lineUC29";
            this.lineUC29.Size = new System.Drawing.Size(200, 3);
            this.lineUC29.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC29.TabIndex = 381;
            // 
            // retangleUC168
            // 
            this.retangleUC168.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC168.ExtenderWidth = 3;
            this.retangleUC168.Fill = false;
            this.retangleUC168.GroupID = null;
            this.retangleUC168.Location = new System.Drawing.Point(1109, 444);
            this.retangleUC168.Name = "retangleUC168";
            this.retangleUC168.Size = new System.Drawing.Size(200, 124);
            this.retangleUC168.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC168.TabIndex = 380;
            // 
            // label169
            // 
            this.label169.AutoSize = true;
            this.label169.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label169.Location = new System.Drawing.Point(1035, 766);
            this.label169.Name = "label169";
            this.label169.Size = new System.Drawing.Size(39, 19);
            this.label169.TabIndex = 379;
            this.label169.Text = "C33";
            // 
            // label170
            // 
            this.label170.AutoSize = true;
            this.label170.Location = new System.Drawing.Point(1098, 835);
            this.label170.Name = "label170";
            this.label170.Size = new System.Drawing.Size(53, 24);
            this.label170.TabIndex = 378;
            this.label170.Text = "連鎖迴路\r\n通訊斷訊";
            // 
            // retangleUC169
            // 
            this.retangleUC169.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC169.ExtenderWidth = 2;
            this.retangleUC169.Fill = false;
            this.retangleUC169.GroupID = null;
            this.retangleUC169.Location = new System.Drawing.Point(1109, 859);
            this.retangleUC169.Name = "retangleUC169";
            this.retangleUC169.Size = new System.Drawing.Size(20, 20);
            this.retangleUC169.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC169.TabIndex = 377;
            // 
            // label172
            // 
            this.label172.AutoSize = true;
            this.label172.Location = new System.Drawing.Point(1081, 794);
            this.label172.Name = "label172";
            this.label172.Size = new System.Drawing.Size(70, 12);
            this.label172.TabIndex = 374;
            this.label172.Text = "cpu通訊異常";
            // 
            // label173
            // 
            this.label173.AutoSize = true;
            this.label173.Location = new System.Drawing.Point(1029, 794);
            this.label173.Name = "label173";
            this.label173.Size = new System.Drawing.Size(46, 12);
            this.label173.TabIndex = 373;
            this.label173.Text = "cpu異常";
            // 
            // label174
            // 
            this.label174.AutoSize = true;
            this.label174.Location = new System.Drawing.Point(965, 794);
            this.label174.Name = "label174";
            this.label174.Size = new System.Drawing.Size(58, 12);
            this.label174.TabIndex = 372;
            this.label174.Text = "cpu運轉中";
            // 
            // retangleUC171
            // 
            this.retangleUC171.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC171.ExtenderWidth = 2;
            this.retangleUC171.Fill = false;
            this.retangleUC171.GroupID = null;
            this.retangleUC171.Location = new System.Drawing.Point(1109, 810);
            this.retangleUC171.Name = "retangleUC171";
            this.retangleUC171.Size = new System.Drawing.Size(20, 20);
            this.retangleUC171.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC171.TabIndex = 371;
            // 
            // retangleUC172
            // 
            this.retangleUC172.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC172.ExtenderWidth = 2;
            this.retangleUC172.Fill = false;
            this.retangleUC172.GroupID = null;
            this.retangleUC172.Location = new System.Drawing.Point(1044, 810);
            this.retangleUC172.Name = "retangleUC172";
            this.retangleUC172.Size = new System.Drawing.Size(20, 20);
            this.retangleUC172.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC172.TabIndex = 370;
            // 
            // retangleUC173
            // 
            this.retangleUC173.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC173.ExtenderWidth = 2;
            this.retangleUC173.Fill = false;
            this.retangleUC173.GroupID = null;
            this.retangleUC173.Location = new System.Drawing.Point(979, 810);
            this.retangleUC173.Name = "retangleUC173";
            this.retangleUC173.Size = new System.Drawing.Size(20, 20);
            this.retangleUC173.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC173.TabIndex = 369;
            // 
            // lineUC30
            // 
            this.lineUC30.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC30.ExtenderWidth = 3;
            this.lineUC30.Fill = false;
            this.lineUC30.GroupID = null;
            this.lineUC30.Location = new System.Drawing.Point(956, 786);
            this.lineUC30.Name = "lineUC30";
            this.lineUC30.Size = new System.Drawing.Size(200, 3);
            this.lineUC30.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC30.TabIndex = 368;
            // 
            // retangleUC174
            // 
            this.retangleUC174.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC174.ExtenderWidth = 3;
            this.retangleUC174.Fill = false;
            this.retangleUC174.GroupID = null;
            this.retangleUC174.Location = new System.Drawing.Point(956, 764);
            this.retangleUC174.Name = "retangleUC174";
            this.retangleUC174.Size = new System.Drawing.Size(200, 124);
            this.retangleUC174.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC174.TabIndex = 367;
            // 
            // label175
            // 
            this.label175.AutoSize = true;
            this.label175.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label175.Location = new System.Drawing.Point(1035, 307);
            this.label175.Name = "label175";
            this.label175.Size = new System.Drawing.Size(39, 19);
            this.label175.TabIndex = 366;
            this.label175.Text = "C21";
            // 
            // label176
            // 
            this.label176.AutoSize = true;
            this.label176.Location = new System.Drawing.Point(1098, 376);
            this.label176.Name = "label176";
            this.label176.Size = new System.Drawing.Size(53, 24);
            this.label176.TabIndex = 365;
            this.label176.Text = "連鎖迴路\r\n通訊斷訊";
            // 
            // retangleUC175
            // 
            this.retangleUC175.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC175.ExtenderWidth = 2;
            this.retangleUC175.Fill = false;
            this.retangleUC175.GroupID = null;
            this.retangleUC175.Location = new System.Drawing.Point(1109, 400);
            this.retangleUC175.Name = "retangleUC175";
            this.retangleUC175.Size = new System.Drawing.Size(20, 20);
            this.retangleUC175.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC175.TabIndex = 364;
            // 
            // label178
            // 
            this.label178.AutoSize = true;
            this.label178.Location = new System.Drawing.Point(1081, 335);
            this.label178.Name = "label178";
            this.label178.Size = new System.Drawing.Size(70, 12);
            this.label178.TabIndex = 361;
            this.label178.Text = "cpu通訊異常";
            // 
            // label179
            // 
            this.label179.AutoSize = true;
            this.label179.Location = new System.Drawing.Point(1029, 335);
            this.label179.Name = "label179";
            this.label179.Size = new System.Drawing.Size(46, 12);
            this.label179.TabIndex = 360;
            this.label179.Text = "cpu異常";
            // 
            // label180
            // 
            this.label180.AutoSize = true;
            this.label180.Location = new System.Drawing.Point(965, 335);
            this.label180.Name = "label180";
            this.label180.Size = new System.Drawing.Size(58, 12);
            this.label180.TabIndex = 359;
            this.label180.Text = "cpu運轉中";
            // 
            // retangleUC177
            // 
            this.retangleUC177.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC177.ExtenderWidth = 2;
            this.retangleUC177.Fill = false;
            this.retangleUC177.GroupID = null;
            this.retangleUC177.Location = new System.Drawing.Point(1109, 351);
            this.retangleUC177.Name = "retangleUC177";
            this.retangleUC177.Size = new System.Drawing.Size(20, 20);
            this.retangleUC177.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC177.TabIndex = 358;
            // 
            // retangleUC178
            // 
            this.retangleUC178.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC178.ExtenderWidth = 2;
            this.retangleUC178.Fill = false;
            this.retangleUC178.GroupID = null;
            this.retangleUC178.Location = new System.Drawing.Point(1044, 351);
            this.retangleUC178.Name = "retangleUC178";
            this.retangleUC178.Size = new System.Drawing.Size(20, 20);
            this.retangleUC178.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC178.TabIndex = 357;
            // 
            // retangleUC179
            // 
            this.retangleUC179.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC179.ExtenderWidth = 2;
            this.retangleUC179.Fill = false;
            this.retangleUC179.GroupID = null;
            this.retangleUC179.Location = new System.Drawing.Point(979, 351);
            this.retangleUC179.Name = "retangleUC179";
            this.retangleUC179.Size = new System.Drawing.Size(20, 20);
            this.retangleUC179.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC179.TabIndex = 356;
            // 
            // lineUC31
            // 
            this.lineUC31.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC31.ExtenderWidth = 3;
            this.lineUC31.Fill = false;
            this.lineUC31.GroupID = null;
            this.lineUC31.Location = new System.Drawing.Point(956, 327);
            this.lineUC31.Name = "lineUC31";
            this.lineUC31.Size = new System.Drawing.Size(200, 3);
            this.lineUC31.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC31.TabIndex = 355;
            // 
            // retangleUC180
            // 
            this.retangleUC180.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC180.ExtenderWidth = 3;
            this.retangleUC180.Fill = false;
            this.retangleUC180.GroupID = null;
            this.retangleUC180.Location = new System.Drawing.Point(956, 305);
            this.retangleUC180.Name = "retangleUC180";
            this.retangleUC180.Size = new System.Drawing.Size(200, 124);
            this.retangleUC180.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC180.TabIndex = 354;
            // 
            // label181
            // 
            this.label181.AutoSize = true;
            this.label181.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label181.Location = new System.Drawing.Point(1494, 629);
            this.label181.Name = "label181";
            this.label181.Size = new System.Drawing.Size(39, 19);
            this.label181.TabIndex = 457;
            this.label181.Text = "C36";
            // 
            // label182
            // 
            this.label182.AutoSize = true;
            this.label182.Location = new System.Drawing.Point(1557, 698);
            this.label182.Name = "label182";
            this.label182.Size = new System.Drawing.Size(53, 24);
            this.label182.TabIndex = 456;
            this.label182.Text = "連鎖迴路\r\n通訊斷訊";
            // 
            // retangleUC181
            // 
            this.retangleUC181.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC181.ExtenderWidth = 2;
            this.retangleUC181.Fill = false;
            this.retangleUC181.GroupID = null;
            this.retangleUC181.Location = new System.Drawing.Point(1568, 722);
            this.retangleUC181.Name = "retangleUC181";
            this.retangleUC181.Size = new System.Drawing.Size(20, 20);
            this.retangleUC181.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC181.TabIndex = 455;
            // 
            // label184
            // 
            this.label184.AutoSize = true;
            this.label184.Location = new System.Drawing.Point(1540, 657);
            this.label184.Name = "label184";
            this.label184.Size = new System.Drawing.Size(70, 12);
            this.label184.TabIndex = 452;
            this.label184.Text = "cpu通訊異常";
            // 
            // label185
            // 
            this.label185.AutoSize = true;
            this.label185.Location = new System.Drawing.Point(1488, 657);
            this.label185.Name = "label185";
            this.label185.Size = new System.Drawing.Size(46, 12);
            this.label185.TabIndex = 451;
            this.label185.Text = "cpu異常";
            // 
            // label186
            // 
            this.label186.AutoSize = true;
            this.label186.Location = new System.Drawing.Point(1424, 657);
            this.label186.Name = "label186";
            this.label186.Size = new System.Drawing.Size(58, 12);
            this.label186.TabIndex = 450;
            this.label186.Text = "cpu運轉中";
            // 
            // retangleUC183
            // 
            this.retangleUC183.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC183.ExtenderWidth = 2;
            this.retangleUC183.Fill = false;
            this.retangleUC183.GroupID = null;
            this.retangleUC183.Location = new System.Drawing.Point(1568, 673);
            this.retangleUC183.Name = "retangleUC183";
            this.retangleUC183.Size = new System.Drawing.Size(20, 20);
            this.retangleUC183.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC183.TabIndex = 449;
            // 
            // retangleUC184
            // 
            this.retangleUC184.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC184.ExtenderWidth = 2;
            this.retangleUC184.Fill = false;
            this.retangleUC184.GroupID = null;
            this.retangleUC184.Location = new System.Drawing.Point(1503, 673);
            this.retangleUC184.Name = "retangleUC184";
            this.retangleUC184.Size = new System.Drawing.Size(20, 20);
            this.retangleUC184.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC184.TabIndex = 448;
            // 
            // retangleUC185
            // 
            this.retangleUC185.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC185.ExtenderWidth = 2;
            this.retangleUC185.Fill = false;
            this.retangleUC185.GroupID = null;
            this.retangleUC185.Location = new System.Drawing.Point(1438, 673);
            this.retangleUC185.Name = "retangleUC185";
            this.retangleUC185.Size = new System.Drawing.Size(20, 20);
            this.retangleUC185.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC185.TabIndex = 447;
            // 
            // lineUC32
            // 
            this.lineUC32.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC32.ExtenderWidth = 3;
            this.lineUC32.Fill = false;
            this.lineUC32.GroupID = null;
            this.lineUC32.Location = new System.Drawing.Point(1415, 649);
            this.lineUC32.Name = "lineUC32";
            this.lineUC32.Size = new System.Drawing.Size(200, 3);
            this.lineUC32.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC32.TabIndex = 446;
            // 
            // retangleUC186
            // 
            this.retangleUC186.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC186.ExtenderWidth = 3;
            this.retangleUC186.Fill = false;
            this.retangleUC186.GroupID = null;
            this.retangleUC186.Location = new System.Drawing.Point(1415, 627);
            this.retangleUC186.Name = "retangleUC186";
            this.retangleUC186.Size = new System.Drawing.Size(200, 124);
            this.retangleUC186.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC186.TabIndex = 445;
            // 
            // label187
            // 
            this.label187.AutoSize = true;
            this.label187.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label187.Location = new System.Drawing.Point(1494, 446);
            this.label187.Name = "label187";
            this.label187.Size = new System.Drawing.Size(39, 19);
            this.label187.TabIndex = 444;
            this.label187.Text = "C24";
            // 
            // label188
            // 
            this.label188.AutoSize = true;
            this.label188.Location = new System.Drawing.Point(1557, 515);
            this.label188.Name = "label188";
            this.label188.Size = new System.Drawing.Size(53, 24);
            this.label188.TabIndex = 443;
            this.label188.Text = "連鎖迴路\r\n通訊斷訊";
            // 
            // retangleUC187
            // 
            this.retangleUC187.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC187.ExtenderWidth = 2;
            this.retangleUC187.Fill = false;
            this.retangleUC187.GroupID = null;
            this.retangleUC187.Location = new System.Drawing.Point(1568, 539);
            this.retangleUC187.Name = "retangleUC187";
            this.retangleUC187.Size = new System.Drawing.Size(20, 20);
            this.retangleUC187.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC187.TabIndex = 442;
            // 
            // label190
            // 
            this.label190.AutoSize = true;
            this.label190.Location = new System.Drawing.Point(1540, 474);
            this.label190.Name = "label190";
            this.label190.Size = new System.Drawing.Size(70, 12);
            this.label190.TabIndex = 439;
            this.label190.Text = "cpu通訊異常";
            // 
            // label191
            // 
            this.label191.AutoSize = true;
            this.label191.Location = new System.Drawing.Point(1488, 474);
            this.label191.Name = "label191";
            this.label191.Size = new System.Drawing.Size(46, 12);
            this.label191.TabIndex = 438;
            this.label191.Text = "cpu異常";
            // 
            // label192
            // 
            this.label192.AutoSize = true;
            this.label192.Location = new System.Drawing.Point(1424, 474);
            this.label192.Name = "label192";
            this.label192.Size = new System.Drawing.Size(58, 12);
            this.label192.TabIndex = 437;
            this.label192.Text = "cpu運轉中";
            // 
            // retangleUC189
            // 
            this.retangleUC189.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC189.ExtenderWidth = 2;
            this.retangleUC189.Fill = false;
            this.retangleUC189.GroupID = null;
            this.retangleUC189.Location = new System.Drawing.Point(1568, 490);
            this.retangleUC189.Name = "retangleUC189";
            this.retangleUC189.Size = new System.Drawing.Size(20, 20);
            this.retangleUC189.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC189.TabIndex = 436;
            // 
            // retangleUC190
            // 
            this.retangleUC190.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC190.ExtenderWidth = 2;
            this.retangleUC190.Fill = false;
            this.retangleUC190.GroupID = null;
            this.retangleUC190.Location = new System.Drawing.Point(1503, 490);
            this.retangleUC190.Name = "retangleUC190";
            this.retangleUC190.Size = new System.Drawing.Size(20, 20);
            this.retangleUC190.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC190.TabIndex = 435;
            // 
            // retangleUC191
            // 
            this.retangleUC191.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC191.ExtenderWidth = 2;
            this.retangleUC191.Fill = false;
            this.retangleUC191.GroupID = null;
            this.retangleUC191.Location = new System.Drawing.Point(1438, 490);
            this.retangleUC191.Name = "retangleUC191";
            this.retangleUC191.Size = new System.Drawing.Size(20, 20);
            this.retangleUC191.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC191.TabIndex = 434;
            // 
            // lineUC33
            // 
            this.lineUC33.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC33.ExtenderWidth = 3;
            this.lineUC33.Fill = false;
            this.lineUC33.GroupID = null;
            this.lineUC33.Location = new System.Drawing.Point(1415, 466);
            this.lineUC33.Name = "lineUC33";
            this.lineUC33.Size = new System.Drawing.Size(200, 3);
            this.lineUC33.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC33.TabIndex = 433;
            // 
            // retangleUC192
            // 
            this.retangleUC192.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC192.ExtenderWidth = 3;
            this.retangleUC192.Fill = false;
            this.retangleUC192.GroupID = null;
            this.retangleUC192.Location = new System.Drawing.Point(1415, 444);
            this.retangleUC192.Name = "retangleUC192";
            this.retangleUC192.Size = new System.Drawing.Size(200, 124);
            this.retangleUC192.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC192.TabIndex = 432;
            // 
            // label193
            // 
            this.label193.AutoSize = true;
            this.label193.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label193.Location = new System.Drawing.Point(1341, 766);
            this.label193.Name = "label193";
            this.label193.Size = new System.Drawing.Size(39, 19);
            this.label193.TabIndex = 431;
            this.label193.Text = "C35";
            // 
            // label194
            // 
            this.label194.AutoSize = true;
            this.label194.Location = new System.Drawing.Point(1404, 835);
            this.label194.Name = "label194";
            this.label194.Size = new System.Drawing.Size(53, 24);
            this.label194.TabIndex = 430;
            this.label194.Text = "連鎖迴路\r\n通訊斷訊";
            // 
            // retangleUC193
            // 
            this.retangleUC193.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC193.ExtenderWidth = 2;
            this.retangleUC193.Fill = false;
            this.retangleUC193.GroupID = null;
            this.retangleUC193.Location = new System.Drawing.Point(1415, 859);
            this.retangleUC193.Name = "retangleUC193";
            this.retangleUC193.Size = new System.Drawing.Size(20, 20);
            this.retangleUC193.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC193.TabIndex = 429;
            // 
            // label196
            // 
            this.label196.AutoSize = true;
            this.label196.Location = new System.Drawing.Point(1387, 794);
            this.label196.Name = "label196";
            this.label196.Size = new System.Drawing.Size(70, 12);
            this.label196.TabIndex = 426;
            this.label196.Text = "cpu通訊異常";
            // 
            // label197
            // 
            this.label197.AutoSize = true;
            this.label197.Location = new System.Drawing.Point(1335, 794);
            this.label197.Name = "label197";
            this.label197.Size = new System.Drawing.Size(46, 12);
            this.label197.TabIndex = 425;
            this.label197.Text = "cpu異常";
            // 
            // label198
            // 
            this.label198.AutoSize = true;
            this.label198.Location = new System.Drawing.Point(1271, 794);
            this.label198.Name = "label198";
            this.label198.Size = new System.Drawing.Size(58, 12);
            this.label198.TabIndex = 424;
            this.label198.Text = "cpu運轉中";
            // 
            // retangleUC195
            // 
            this.retangleUC195.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC195.ExtenderWidth = 2;
            this.retangleUC195.Fill = false;
            this.retangleUC195.GroupID = null;
            this.retangleUC195.Location = new System.Drawing.Point(1415, 810);
            this.retangleUC195.Name = "retangleUC195";
            this.retangleUC195.Size = new System.Drawing.Size(20, 20);
            this.retangleUC195.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC195.TabIndex = 423;
            // 
            // retangleUC196
            // 
            this.retangleUC196.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC196.ExtenderWidth = 2;
            this.retangleUC196.Fill = false;
            this.retangleUC196.GroupID = null;
            this.retangleUC196.Location = new System.Drawing.Point(1350, 810);
            this.retangleUC196.Name = "retangleUC196";
            this.retangleUC196.Size = new System.Drawing.Size(20, 20);
            this.retangleUC196.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC196.TabIndex = 422;
            // 
            // retangleUC197
            // 
            this.retangleUC197.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC197.ExtenderWidth = 2;
            this.retangleUC197.Fill = false;
            this.retangleUC197.GroupID = null;
            this.retangleUC197.Location = new System.Drawing.Point(1285, 810);
            this.retangleUC197.Name = "retangleUC197";
            this.retangleUC197.Size = new System.Drawing.Size(20, 20);
            this.retangleUC197.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC197.TabIndex = 421;
            // 
            // lineUC34
            // 
            this.lineUC34.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC34.ExtenderWidth = 3;
            this.lineUC34.Fill = false;
            this.lineUC34.GroupID = null;
            this.lineUC34.Location = new System.Drawing.Point(1262, 786);
            this.lineUC34.Name = "lineUC34";
            this.lineUC34.Size = new System.Drawing.Size(200, 3);
            this.lineUC34.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC34.TabIndex = 420;
            // 
            // retangleUC198
            // 
            this.retangleUC198.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC198.ExtenderWidth = 3;
            this.retangleUC198.Fill = false;
            this.retangleUC198.GroupID = null;
            this.retangleUC198.Location = new System.Drawing.Point(1262, 764);
            this.retangleUC198.Name = "retangleUC198";
            this.retangleUC198.Size = new System.Drawing.Size(200, 124);
            this.retangleUC198.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC198.TabIndex = 419;
            // 
            // label199
            // 
            this.label199.AutoSize = true;
            this.label199.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label199.Location = new System.Drawing.Point(1341, 307);
            this.label199.Name = "label199";
            this.label199.Size = new System.Drawing.Size(39, 19);
            this.label199.TabIndex = 418;
            this.label199.Text = "C23";
            // 
            // label200
            // 
            this.label200.AutoSize = true;
            this.label200.Location = new System.Drawing.Point(1404, 376);
            this.label200.Name = "label200";
            this.label200.Size = new System.Drawing.Size(53, 24);
            this.label200.TabIndex = 417;
            this.label200.Text = "連鎖迴路\r\n通訊斷訊";
            // 
            // retangleUC199
            // 
            this.retangleUC199.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC199.ExtenderWidth = 2;
            this.retangleUC199.Fill = false;
            this.retangleUC199.GroupID = null;
            this.retangleUC199.Location = new System.Drawing.Point(1415, 400);
            this.retangleUC199.Name = "retangleUC199";
            this.retangleUC199.Size = new System.Drawing.Size(20, 20);
            this.retangleUC199.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC199.TabIndex = 416;
            // 
            // label202
            // 
            this.label202.AutoSize = true;
            this.label202.Location = new System.Drawing.Point(1387, 335);
            this.label202.Name = "label202";
            this.label202.Size = new System.Drawing.Size(70, 12);
            this.label202.TabIndex = 413;
            this.label202.Text = "cpu通訊異常";
            // 
            // label203
            // 
            this.label203.AutoSize = true;
            this.label203.Location = new System.Drawing.Point(1335, 335);
            this.label203.Name = "label203";
            this.label203.Size = new System.Drawing.Size(46, 12);
            this.label203.TabIndex = 412;
            this.label203.Text = "cpu異常";
            // 
            // label204
            // 
            this.label204.AutoSize = true;
            this.label204.Location = new System.Drawing.Point(1271, 335);
            this.label204.Name = "label204";
            this.label204.Size = new System.Drawing.Size(58, 12);
            this.label204.TabIndex = 411;
            this.label204.Text = "cpu運轉中";
            // 
            // retangleUC201
            // 
            this.retangleUC201.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC201.ExtenderWidth = 2;
            this.retangleUC201.Fill = false;
            this.retangleUC201.GroupID = null;
            this.retangleUC201.Location = new System.Drawing.Point(1415, 351);
            this.retangleUC201.Name = "retangleUC201";
            this.retangleUC201.Size = new System.Drawing.Size(20, 20);
            this.retangleUC201.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC201.TabIndex = 410;
            // 
            // retangleUC202
            // 
            this.retangleUC202.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC202.ExtenderWidth = 2;
            this.retangleUC202.Fill = false;
            this.retangleUC202.GroupID = null;
            this.retangleUC202.Location = new System.Drawing.Point(1350, 351);
            this.retangleUC202.Name = "retangleUC202";
            this.retangleUC202.Size = new System.Drawing.Size(20, 20);
            this.retangleUC202.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC202.TabIndex = 409;
            // 
            // retangleUC203
            // 
            this.retangleUC203.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC203.ExtenderWidth = 2;
            this.retangleUC203.Fill = false;
            this.retangleUC203.GroupID = null;
            this.retangleUC203.Location = new System.Drawing.Point(1285, 351);
            this.retangleUC203.Name = "retangleUC203";
            this.retangleUC203.Size = new System.Drawing.Size(20, 20);
            this.retangleUC203.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC203.TabIndex = 408;
            // 
            // lineUC35
            // 
            this.lineUC35.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC35.ExtenderWidth = 3;
            this.lineUC35.Fill = false;
            this.lineUC35.GroupID = null;
            this.lineUC35.Location = new System.Drawing.Point(1262, 327);
            this.lineUC35.Name = "lineUC35";
            this.lineUC35.Size = new System.Drawing.Size(200, 3);
            this.lineUC35.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC35.TabIndex = 407;
            // 
            // retangleUC204
            // 
            this.retangleUC204.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC204.ExtenderWidth = 3;
            this.retangleUC204.Fill = false;
            this.retangleUC204.GroupID = null;
            this.retangleUC204.Location = new System.Drawing.Point(1262, 305);
            this.retangleUC204.Name = "retangleUC204";
            this.retangleUC204.Size = new System.Drawing.Size(200, 124);
            this.retangleUC204.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC204.TabIndex = 406;
            // 
            // label205
            // 
            this.label205.AutoSize = true;
            this.label205.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label205.Location = new System.Drawing.Point(1647, 766);
            this.label205.Name = "label205";
            this.label205.Size = new System.Drawing.Size(39, 19);
            this.label205.TabIndex = 483;
            this.label205.Text = "C37";
            // 
            // label206
            // 
            this.label206.AutoSize = true;
            this.label206.Location = new System.Drawing.Point(1710, 835);
            this.label206.Name = "label206";
            this.label206.Size = new System.Drawing.Size(53, 24);
            this.label206.TabIndex = 482;
            this.label206.Text = "連鎖迴路\r\n通訊斷訊";
            // 
            // retangleUC205
            // 
            this.retangleUC205.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC205.ExtenderWidth = 2;
            this.retangleUC205.Fill = false;
            this.retangleUC205.GroupID = null;
            this.retangleUC205.Location = new System.Drawing.Point(1721, 859);
            this.retangleUC205.Name = "retangleUC205";
            this.retangleUC205.Size = new System.Drawing.Size(20, 20);
            this.retangleUC205.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC205.TabIndex = 481;
            // 
            // label208
            // 
            this.label208.AutoSize = true;
            this.label208.Location = new System.Drawing.Point(1693, 794);
            this.label208.Name = "label208";
            this.label208.Size = new System.Drawing.Size(70, 12);
            this.label208.TabIndex = 478;
            this.label208.Text = "cpu通訊異常";
            // 
            // label209
            // 
            this.label209.AutoSize = true;
            this.label209.Location = new System.Drawing.Point(1641, 794);
            this.label209.Name = "label209";
            this.label209.Size = new System.Drawing.Size(46, 12);
            this.label209.TabIndex = 477;
            this.label209.Text = "cpu異常";
            // 
            // label210
            // 
            this.label210.AutoSize = true;
            this.label210.Location = new System.Drawing.Point(1577, 794);
            this.label210.Name = "label210";
            this.label210.Size = new System.Drawing.Size(58, 12);
            this.label210.TabIndex = 476;
            this.label210.Text = "cpu運轉中";
            // 
            // retangleUC207
            // 
            this.retangleUC207.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC207.ExtenderWidth = 2;
            this.retangleUC207.Fill = false;
            this.retangleUC207.GroupID = null;
            this.retangleUC207.Location = new System.Drawing.Point(1721, 810);
            this.retangleUC207.Name = "retangleUC207";
            this.retangleUC207.Size = new System.Drawing.Size(20, 20);
            this.retangleUC207.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC207.TabIndex = 475;
            // 
            // retangleUC208
            // 
            this.retangleUC208.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC208.ExtenderWidth = 2;
            this.retangleUC208.Fill = false;
            this.retangleUC208.GroupID = null;
            this.retangleUC208.Location = new System.Drawing.Point(1656, 810);
            this.retangleUC208.Name = "retangleUC208";
            this.retangleUC208.Size = new System.Drawing.Size(20, 20);
            this.retangleUC208.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC208.TabIndex = 474;
            // 
            // retangleUC209
            // 
            this.retangleUC209.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC209.ExtenderWidth = 2;
            this.retangleUC209.Fill = false;
            this.retangleUC209.GroupID = null;
            this.retangleUC209.Location = new System.Drawing.Point(1591, 810);
            this.retangleUC209.Name = "retangleUC209";
            this.retangleUC209.Size = new System.Drawing.Size(20, 20);
            this.retangleUC209.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC209.TabIndex = 473;
            // 
            // lineUC36
            // 
            this.lineUC36.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC36.ExtenderWidth = 3;
            this.lineUC36.Fill = false;
            this.lineUC36.GroupID = null;
            this.lineUC36.Location = new System.Drawing.Point(1568, 786);
            this.lineUC36.Name = "lineUC36";
            this.lineUC36.Size = new System.Drawing.Size(200, 3);
            this.lineUC36.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC36.TabIndex = 472;
            // 
            // retangleUC210
            // 
            this.retangleUC210.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC210.ExtenderWidth = 3;
            this.retangleUC210.Fill = false;
            this.retangleUC210.GroupID = null;
            this.retangleUC210.Location = new System.Drawing.Point(1568, 764);
            this.retangleUC210.Name = "retangleUC210";
            this.retangleUC210.Size = new System.Drawing.Size(200, 124);
            this.retangleUC210.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC210.TabIndex = 471;
            // 
            // label211
            // 
            this.label211.AutoSize = true;
            this.label211.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label211.Location = new System.Drawing.Point(1647, 307);
            this.label211.Name = "label211";
            this.label211.Size = new System.Drawing.Size(39, 19);
            this.label211.TabIndex = 470;
            this.label211.Text = "C25";
            // 
            // label212
            // 
            this.label212.AutoSize = true;
            this.label212.Location = new System.Drawing.Point(1710, 376);
            this.label212.Name = "label212";
            this.label212.Size = new System.Drawing.Size(53, 24);
            this.label212.TabIndex = 469;
            this.label212.Text = "連鎖迴路\r\n通訊斷訊";
            // 
            // retangleUC211
            // 
            this.retangleUC211.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC211.ExtenderWidth = 2;
            this.retangleUC211.Fill = false;
            this.retangleUC211.GroupID = null;
            this.retangleUC211.Location = new System.Drawing.Point(1721, 400);
            this.retangleUC211.Name = "retangleUC211";
            this.retangleUC211.Size = new System.Drawing.Size(20, 20);
            this.retangleUC211.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC211.TabIndex = 468;
            // 
            // label214
            // 
            this.label214.AutoSize = true;
            this.label214.Location = new System.Drawing.Point(1693, 335);
            this.label214.Name = "label214";
            this.label214.Size = new System.Drawing.Size(70, 12);
            this.label214.TabIndex = 465;
            this.label214.Text = "cpu通訊異常";
            // 
            // label215
            // 
            this.label215.AutoSize = true;
            this.label215.Location = new System.Drawing.Point(1641, 335);
            this.label215.Name = "label215";
            this.label215.Size = new System.Drawing.Size(46, 12);
            this.label215.TabIndex = 464;
            this.label215.Text = "cpu異常";
            // 
            // label216
            // 
            this.label216.AutoSize = true;
            this.label216.Location = new System.Drawing.Point(1577, 335);
            this.label216.Name = "label216";
            this.label216.Size = new System.Drawing.Size(58, 12);
            this.label216.TabIndex = 463;
            this.label216.Text = "cpu運轉中";
            // 
            // retangleUC213
            // 
            this.retangleUC213.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC213.ExtenderWidth = 2;
            this.retangleUC213.Fill = false;
            this.retangleUC213.GroupID = null;
            this.retangleUC213.Location = new System.Drawing.Point(1721, 351);
            this.retangleUC213.Name = "retangleUC213";
            this.retangleUC213.Size = new System.Drawing.Size(20, 20);
            this.retangleUC213.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC213.TabIndex = 462;
            // 
            // retangleUC214
            // 
            this.retangleUC214.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC214.ExtenderWidth = 2;
            this.retangleUC214.Fill = false;
            this.retangleUC214.GroupID = null;
            this.retangleUC214.Location = new System.Drawing.Point(1656, 351);
            this.retangleUC214.Name = "retangleUC214";
            this.retangleUC214.Size = new System.Drawing.Size(20, 20);
            this.retangleUC214.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC214.TabIndex = 461;
            // 
            // retangleUC215
            // 
            this.retangleUC215.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC215.ExtenderWidth = 2;
            this.retangleUC215.Fill = false;
            this.retangleUC215.GroupID = null;
            this.retangleUC215.Location = new System.Drawing.Point(1591, 351);
            this.retangleUC215.Name = "retangleUC215";
            this.retangleUC215.Size = new System.Drawing.Size(20, 20);
            this.retangleUC215.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC215.TabIndex = 460;
            // 
            // lineUC37
            // 
            this.lineUC37.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC37.ExtenderWidth = 3;
            this.lineUC37.Fill = false;
            this.lineUC37.GroupID = null;
            this.lineUC37.Location = new System.Drawing.Point(1568, 327);
            this.lineUC37.Name = "lineUC37";
            this.lineUC37.Size = new System.Drawing.Size(200, 3);
            this.lineUC37.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC37.TabIndex = 459;
            // 
            // retangleUC216
            // 
            this.retangleUC216.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC216.ExtenderWidth = 3;
            this.retangleUC216.Fill = false;
            this.retangleUC216.GroupID = null;
            this.retangleUC216.Location = new System.Drawing.Point(1568, 305);
            this.retangleUC216.Name = "retangleUC216";
            this.retangleUC216.Size = new System.Drawing.Size(200, 124);
            this.retangleUC216.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC216.TabIndex = 458;
            // 
            // lineUC38
            // 
            this.lineUC38.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC38.ExtenderWidth = 5;
            this.lineUC38.Fill = false;
            this.lineUC38.GroupID = null;
            this.lineUC38.Location = new System.Drawing.Point(5, 588);
            this.lineUC38.Name = "lineUC38";
            this.lineUC38.Size = new System.Drawing.Size(1900, 10);
            this.lineUC38.State = iSCADA.Design.Utilities.Electrical.SymbolState.Disconnected;
            this.lineUC38.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC38.TabIndex = 484;
            // 
            // label217
            // 
            this.label217.AutoSize = true;
            this.label217.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label217.Location = new System.Drawing.Point(1784, 446);
            this.label217.Name = "label217";
            this.label217.Size = new System.Drawing.Size(39, 19);
            this.label217.TabIndex = 497;
            this.label217.Text = "C26";
            // 
            // label218
            // 
            this.label218.AutoSize = true;
            this.label218.Location = new System.Drawing.Point(1847, 515);
            this.label218.Name = "label218";
            this.label218.Size = new System.Drawing.Size(53, 24);
            this.label218.TabIndex = 496;
            this.label218.Text = "連鎖迴路\r\n通訊斷訊";
            // 
            // retangleUC217
            // 
            this.retangleUC217.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC217.ExtenderWidth = 2;
            this.retangleUC217.Fill = false;
            this.retangleUC217.GroupID = null;
            this.retangleUC217.Location = new System.Drawing.Point(1858, 539);
            this.retangleUC217.Name = "retangleUC217";
            this.retangleUC217.Size = new System.Drawing.Size(20, 20);
            this.retangleUC217.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC217.TabIndex = 495;
            // 
            // label220
            // 
            this.label220.AutoSize = true;
            this.label220.Location = new System.Drawing.Point(1830, 474);
            this.label220.Name = "label220";
            this.label220.Size = new System.Drawing.Size(70, 12);
            this.label220.TabIndex = 492;
            this.label220.Text = "cpu通訊異常";
            // 
            // label221
            // 
            this.label221.AutoSize = true;
            this.label221.Location = new System.Drawing.Point(1778, 474);
            this.label221.Name = "label221";
            this.label221.Size = new System.Drawing.Size(46, 12);
            this.label221.TabIndex = 491;
            this.label221.Text = "cpu異常";
            // 
            // label222
            // 
            this.label222.AutoSize = true;
            this.label222.Location = new System.Drawing.Point(1714, 474);
            this.label222.Name = "label222";
            this.label222.Size = new System.Drawing.Size(58, 12);
            this.label222.TabIndex = 490;
            this.label222.Text = "cpu運轉中";
            // 
            // retangleUC219
            // 
            this.retangleUC219.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC219.ExtenderWidth = 2;
            this.retangleUC219.Fill = false;
            this.retangleUC219.GroupID = null;
            this.retangleUC219.Location = new System.Drawing.Point(1858, 490);
            this.retangleUC219.Name = "retangleUC219";
            this.retangleUC219.Size = new System.Drawing.Size(20, 20);
            this.retangleUC219.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC219.TabIndex = 489;
            // 
            // retangleUC220
            // 
            this.retangleUC220.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC220.ExtenderWidth = 2;
            this.retangleUC220.Fill = false;
            this.retangleUC220.GroupID = null;
            this.retangleUC220.Location = new System.Drawing.Point(1793, 490);
            this.retangleUC220.Name = "retangleUC220";
            this.retangleUC220.Size = new System.Drawing.Size(20, 20);
            this.retangleUC220.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC220.TabIndex = 488;
            // 
            // retangleUC221
            // 
            this.retangleUC221.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC221.ExtenderWidth = 2;
            this.retangleUC221.Fill = false;
            this.retangleUC221.GroupID = null;
            this.retangleUC221.Location = new System.Drawing.Point(1728, 490);
            this.retangleUC221.Name = "retangleUC221";
            this.retangleUC221.Size = new System.Drawing.Size(20, 20);
            this.retangleUC221.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC221.TabIndex = 487;
            // 
            // lineUC39
            // 
            this.lineUC39.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC39.ExtenderWidth = 3;
            this.lineUC39.Fill = false;
            this.lineUC39.GroupID = null;
            this.lineUC39.Location = new System.Drawing.Point(1705, 466);
            this.lineUC39.Name = "lineUC39";
            this.lineUC39.Size = new System.Drawing.Size(200, 3);
            this.lineUC39.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC39.TabIndex = 486;
            // 
            // retangleUC222
            // 
            this.retangleUC222.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC222.ExtenderWidth = 3;
            this.retangleUC222.Fill = false;
            this.retangleUC222.GroupID = null;
            this.retangleUC222.Location = new System.Drawing.Point(1705, 444);
            this.retangleUC222.Name = "retangleUC222";
            this.retangleUC222.Size = new System.Drawing.Size(200, 124);
            this.retangleUC222.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC222.TabIndex = 485;
            // 
            // lineUC40
            // 
            this.lineUC40.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC40.ExtenderWidth = 5;
            this.lineUC40.Fill = false;
            this.lineUC40.GroupID = null;
            this.lineUC40.Location = new System.Drawing.Point(127, 429);
            this.lineUC40.Name = "lineUC40";
            this.lineUC40.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC40.Size = new System.Drawing.Size(13, 163);
            this.lineUC40.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC40.TabIndex = 498;
            // 
            // lineUC41
            // 
            this.lineUC41.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC41.ExtenderWidth = 5;
            this.lineUC41.Fill = false;
            this.lineUC41.GroupID = null;
            this.lineUC41.Location = new System.Drawing.Point(1053, 429);
            this.lineUC41.Name = "lineUC41";
            this.lineUC41.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC41.Size = new System.Drawing.Size(10, 168);
            this.lineUC41.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC41.TabIndex = 499;
            // 
            // lineUC42
            // 
            this.lineUC42.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC42.ExtenderWidth = 5;
            this.lineUC42.Fill = false;
            this.lineUC42.GroupID = null;
            this.lineUC42.Location = new System.Drawing.Point(1053, 596);
            this.lineUC42.Name = "lineUC42";
            this.lineUC42.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC42.Size = new System.Drawing.Size(10, 168);
            this.lineUC42.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC42.TabIndex = 500;
            // 
            // lineUC43
            // 
            this.lineUC43.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC43.ExtenderWidth = 5;
            this.lineUC43.Fill = false;
            this.lineUC43.GroupID = null;
            this.lineUC43.Location = new System.Drawing.Point(749, 429);
            this.lineUC43.Name = "lineUC43";
            this.lineUC43.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC43.Size = new System.Drawing.Size(10, 168);
            this.lineUC43.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC43.TabIndex = 501;
            // 
            // lineUC44
            // 
            this.lineUC44.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC44.ExtenderWidth = 5;
            this.lineUC44.Fill = false;
            this.lineUC44.GroupID = null;
            this.lineUC44.Location = new System.Drawing.Point(447, 429);
            this.lineUC44.Name = "lineUC44";
            this.lineUC44.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC44.Size = new System.Drawing.Size(10, 168);
            this.lineUC44.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC44.TabIndex = 502;
            // 
            // lineUC45
            // 
            this.lineUC45.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC45.ExtenderWidth = 5;
            this.lineUC45.Fill = false;
            this.lineUC45.GroupID = null;
            this.lineUC45.Location = new System.Drawing.Point(1363, 429);
            this.lineUC45.Name = "lineUC45";
            this.lineUC45.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC45.Size = new System.Drawing.Size(10, 168);
            this.lineUC45.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC45.TabIndex = 503;
            // 
            // lineUC46
            // 
            this.lineUC46.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC46.ExtenderWidth = 5;
            this.lineUC46.Fill = false;
            this.lineUC46.GroupID = null;
            this.lineUC46.Location = new System.Drawing.Point(1669, 429);
            this.lineUC46.Name = "lineUC46";
            this.lineUC46.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC46.Size = new System.Drawing.Size(10, 168);
            this.lineUC46.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC46.TabIndex = 504;
            // 
            // lineUC47
            // 
            this.lineUC47.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC47.ExtenderWidth = 5;
            this.lineUC47.Fill = false;
            this.lineUC47.GroupID = null;
            this.lineUC47.Location = new System.Drawing.Point(127, 596);
            this.lineUC47.Name = "lineUC47";
            this.lineUC47.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC47.Size = new System.Drawing.Size(10, 168);
            this.lineUC47.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC47.TabIndex = 505;
            // 
            // lineUC48
            // 
            this.lineUC48.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC48.ExtenderWidth = 5;
            this.lineUC48.Fill = false;
            this.lineUC48.GroupID = null;
            this.lineUC48.Location = new System.Drawing.Point(447, 596);
            this.lineUC48.Name = "lineUC48";
            this.lineUC48.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC48.Size = new System.Drawing.Size(10, 168);
            this.lineUC48.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC48.TabIndex = 506;
            // 
            // lineUC49
            // 
            this.lineUC49.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC49.ExtenderWidth = 5;
            this.lineUC49.Fill = false;
            this.lineUC49.GroupID = null;
            this.lineUC49.Location = new System.Drawing.Point(749, 596);
            this.lineUC49.Name = "lineUC49";
            this.lineUC49.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC49.Size = new System.Drawing.Size(10, 168);
            this.lineUC49.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC49.TabIndex = 507;
            // 
            // lineUC50
            // 
            this.lineUC50.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC50.ExtenderWidth = 5;
            this.lineUC50.Fill = false;
            this.lineUC50.GroupID = null;
            this.lineUC50.Location = new System.Drawing.Point(1363, 596);
            this.lineUC50.Name = "lineUC50";
            this.lineUC50.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC50.Size = new System.Drawing.Size(10, 168);
            this.lineUC50.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC50.TabIndex = 508;
            // 
            // lineUC51
            // 
            this.lineUC51.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC51.ExtenderWidth = 5;
            this.lineUC51.Fill = false;
            this.lineUC51.GroupID = null;
            this.lineUC51.Location = new System.Drawing.Point(1669, 596);
            this.lineUC51.Name = "lineUC51";
            this.lineUC51.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC51.Size = new System.Drawing.Size(10, 168);
            this.lineUC51.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC51.TabIndex = 509;
            // 
            // lineUC52
            // 
            this.lineUC52.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC52.ExtenderWidth = 5;
            this.lineUC52.Fill = false;
            this.lineUC52.GroupID = null;
            this.lineUC52.Location = new System.Drawing.Point(286, 567);
            this.lineUC52.Name = "lineUC52";
            this.lineUC52.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC52.Size = new System.Drawing.Size(10, 30);
            this.lineUC52.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC52.TabIndex = 510;
            // 
            // lineUC53
            // 
            this.lineUC53.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC53.ExtenderWidth = 2;
            this.lineUC53.Fill = false;
            this.lineUC53.GroupID = null;
            this.lineUC53.Location = new System.Drawing.Point(593, 567);
            this.lineUC53.Name = "lineUC53";
            this.lineUC53.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC53.Size = new System.Drawing.Size(2, 30);
            this.lineUC53.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC53.TabIndex = 511;
            // 
            // lineUC54
            // 
            this.lineUC54.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC54.ExtenderWidth = 5;
            this.lineUC54.Fill = false;
            this.lineUC54.GroupID = null;
            this.lineUC54.Location = new System.Drawing.Point(902, 568);
            this.lineUC54.Name = "lineUC54";
            this.lineUC54.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC54.Size = new System.Drawing.Size(10, 30);
            this.lineUC54.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC54.TabIndex = 512;
            // 
            // lineUC55
            // 
            this.lineUC55.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC55.ExtenderWidth = 5;
            this.lineUC55.Fill = false;
            this.lineUC55.GroupID = null;
            this.lineUC55.Location = new System.Drawing.Point(1206, 568);
            this.lineUC55.Name = "lineUC55";
            this.lineUC55.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC55.Size = new System.Drawing.Size(10, 30);
            this.lineUC55.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC55.TabIndex = 513;
            // 
            // lineUC56
            // 
            this.lineUC56.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC56.ExtenderWidth = 5;
            this.lineUC56.Fill = false;
            this.lineUC56.GroupID = null;
            this.lineUC56.Location = new System.Drawing.Point(1513, 567);
            this.lineUC56.Name = "lineUC56";
            this.lineUC56.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC56.Size = new System.Drawing.Size(10, 30);
            this.lineUC56.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC56.TabIndex = 514;
            // 
            // lineUC57
            // 
            this.lineUC57.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC57.ExtenderWidth = 5;
            this.lineUC57.Fill = false;
            this.lineUC57.GroupID = null;
            this.lineUC57.Location = new System.Drawing.Point(1804, 567);
            this.lineUC57.Name = "lineUC57";
            this.lineUC57.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC57.Size = new System.Drawing.Size(10, 30);
            this.lineUC57.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC57.TabIndex = 515;
            // 
            // lineUC58
            // 
            this.lineUC58.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC58.ExtenderWidth = 5;
            this.lineUC58.Fill = false;
            this.lineUC58.GroupID = null;
            this.lineUC58.Location = new System.Drawing.Point(286, 596);
            this.lineUC58.Name = "lineUC58";
            this.lineUC58.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC58.Size = new System.Drawing.Size(10, 30);
            this.lineUC58.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC58.TabIndex = 516;
            // 
            // lineUC59
            // 
            this.lineUC59.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC59.ExtenderWidth = 5;
            this.lineUC59.Fill = false;
            this.lineUC59.GroupID = null;
            this.lineUC59.Location = new System.Drawing.Point(902, 597);
            this.lineUC59.Name = "lineUC59";
            this.lineUC59.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC59.Size = new System.Drawing.Size(10, 30);
            this.lineUC59.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC59.TabIndex = 517;
            // 
            // lineUC60
            // 
            this.lineUC60.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC60.ExtenderWidth = 2;
            this.lineUC60.Fill = false;
            this.lineUC60.GroupID = null;
            this.lineUC60.Location = new System.Drawing.Point(593, 596);
            this.lineUC60.Name = "lineUC60";
            this.lineUC60.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC60.Size = new System.Drawing.Size(2, 30);
            this.lineUC60.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC60.TabIndex = 518;
            // 
            // lineUC61
            // 
            this.lineUC61.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC61.ExtenderWidth = 5;
            this.lineUC61.Fill = false;
            this.lineUC61.GroupID = null;
            this.lineUC61.Location = new System.Drawing.Point(1206, 597);
            this.lineUC61.Name = "lineUC61";
            this.lineUC61.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC61.Size = new System.Drawing.Size(10, 30);
            this.lineUC61.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC61.TabIndex = 519;
            // 
            // lineUC62
            // 
            this.lineUC62.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC62.ExtenderWidth = 5;
            this.lineUC62.Fill = false;
            this.lineUC62.GroupID = null;
            this.lineUC62.Location = new System.Drawing.Point(1513, 597);
            this.lineUC62.Name = "lineUC62";
            this.lineUC62.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC62.Size = new System.Drawing.Size(10, 30);
            this.lineUC62.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC62.TabIndex = 520;
            // 
            // retangleUC6
            // 
            this.retangleUC6.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC6.ExtenderWidth = 3;
            this.retangleUC6.Fill = false;
            this.retangleUC6.GroupID = null;
            this.retangleUC6.Location = new System.Drawing.Point(1656, 10);
            this.retangleUC6.Name = "retangleUC6";
            this.retangleUC6.Size = new System.Drawing.Size(242, 95);
            this.retangleUC6.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC6.TabIndex = 521;
            // 
            // lineUC63
            // 
            this.lineUC63.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC63.ExtenderWidth = 3;
            this.lineUC63.Fill = false;
            this.lineUC63.GroupID = null;
            this.lineUC63.Location = new System.Drawing.Point(1670, 37);
            this.lineUC63.Name = "lineUC63";
            this.lineUC63.Size = new System.Drawing.Size(102, 10);
            this.lineUC63.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC63.TabIndex = 522;
            // 
            // lineUC64
            // 
            this.lineUC64.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC64.ExtenderWidth = 3;
            this.lineUC64.Fill = false;
            this.lineUC64.GroupID = null;
            this.lineUC64.Location = new System.Drawing.Point(1670, 76);
            this.lineUC64.Name = "lineUC64";
            this.lineUC64.Size = new System.Drawing.Size(102, 10);
            this.lineUC64.State = iSCADA.Design.Utilities.Electrical.SymbolState.Disconnected;
            this.lineUC64.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC64.TabIndex = 522;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("微軟正黑體", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label6.Location = new System.Drawing.Point(1802, 29);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 24);
            this.label6.TabIndex = 523;
            this.label6.Text = "RJ45";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("微軟正黑體", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label7.Location = new System.Drawing.Point(1802, 66);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(45, 24);
            this.label7.TabIndex = 523;
            this.label7.Text = "CTS";
            // 
            // Form_Total2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1910, 900);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lineUC64);
            this.Controls.Add(this.lineUC63);
            this.Controls.Add(this.retangleUC6);
            this.Controls.Add(this.lineUC62);
            this.Controls.Add(this.lineUC61);
            this.Controls.Add(this.lineUC60);
            this.Controls.Add(this.lineUC59);
            this.Controls.Add(this.lineUC58);
            this.Controls.Add(this.lineUC57);
            this.Controls.Add(this.lineUC56);
            this.Controls.Add(this.lineUC55);
            this.Controls.Add(this.lineUC54);
            this.Controls.Add(this.lineUC53);
            this.Controls.Add(this.lineUC52);
            this.Controls.Add(this.lineUC51);
            this.Controls.Add(this.lineUC50);
            this.Controls.Add(this.lineUC49);
            this.Controls.Add(this.lineUC48);
            this.Controls.Add(this.lineUC47);
            this.Controls.Add(this.lineUC46);
            this.Controls.Add(this.lineUC45);
            this.Controls.Add(this.lineUC44);
            this.Controls.Add(this.lineUC43);
            this.Controls.Add(this.lineUC42);
            this.Controls.Add(this.lineUC41);
            this.Controls.Add(this.lineUC40);
            this.Controls.Add(this.label217);
            this.Controls.Add(this.label218);
            this.Controls.Add(this.retangleUC217);
            this.Controls.Add(this.label220);
            this.Controls.Add(this.label221);
            this.Controls.Add(this.label222);
            this.Controls.Add(this.retangleUC219);
            this.Controls.Add(this.retangleUC220);
            this.Controls.Add(this.retangleUC221);
            this.Controls.Add(this.lineUC39);
            this.Controls.Add(this.retangleUC222);
            this.Controls.Add(this.lineUC38);
            this.Controls.Add(this.label205);
            this.Controls.Add(this.label206);
            this.Controls.Add(this.retangleUC205);
            this.Controls.Add(this.label208);
            this.Controls.Add(this.label209);
            this.Controls.Add(this.label210);
            this.Controls.Add(this.retangleUC207);
            this.Controls.Add(this.retangleUC208);
            this.Controls.Add(this.retangleUC209);
            this.Controls.Add(this.lineUC36);
            this.Controls.Add(this.retangleUC210);
            this.Controls.Add(this.label211);
            this.Controls.Add(this.label212);
            this.Controls.Add(this.retangleUC211);
            this.Controls.Add(this.label214);
            this.Controls.Add(this.label215);
            this.Controls.Add(this.label216);
            this.Controls.Add(this.retangleUC213);
            this.Controls.Add(this.retangleUC214);
            this.Controls.Add(this.retangleUC215);
            this.Controls.Add(this.lineUC37);
            this.Controls.Add(this.retangleUC216);
            this.Controls.Add(this.label181);
            this.Controls.Add(this.label182);
            this.Controls.Add(this.retangleUC181);
            this.Controls.Add(this.label184);
            this.Controls.Add(this.label185);
            this.Controls.Add(this.label186);
            this.Controls.Add(this.retangleUC183);
            this.Controls.Add(this.retangleUC184);
            this.Controls.Add(this.retangleUC185);
            this.Controls.Add(this.lineUC32);
            this.Controls.Add(this.retangleUC186);
            this.Controls.Add(this.label187);
            this.Controls.Add(this.label188);
            this.Controls.Add(this.retangleUC187);
            this.Controls.Add(this.label190);
            this.Controls.Add(this.label191);
            this.Controls.Add(this.label192);
            this.Controls.Add(this.retangleUC189);
            this.Controls.Add(this.retangleUC190);
            this.Controls.Add(this.retangleUC191);
            this.Controls.Add(this.lineUC33);
            this.Controls.Add(this.retangleUC192);
            this.Controls.Add(this.label193);
            this.Controls.Add(this.label194);
            this.Controls.Add(this.retangleUC193);
            this.Controls.Add(this.label196);
            this.Controls.Add(this.label197);
            this.Controls.Add(this.label198);
            this.Controls.Add(this.retangleUC195);
            this.Controls.Add(this.retangleUC196);
            this.Controls.Add(this.retangleUC197);
            this.Controls.Add(this.lineUC34);
            this.Controls.Add(this.retangleUC198);
            this.Controls.Add(this.label199);
            this.Controls.Add(this.label200);
            this.Controls.Add(this.retangleUC199);
            this.Controls.Add(this.label202);
            this.Controls.Add(this.label203);
            this.Controls.Add(this.label204);
            this.Controls.Add(this.retangleUC201);
            this.Controls.Add(this.retangleUC202);
            this.Controls.Add(this.retangleUC203);
            this.Controls.Add(this.lineUC35);
            this.Controls.Add(this.retangleUC204);
            this.Controls.Add(this.label157);
            this.Controls.Add(this.label158);
            this.Controls.Add(this.retangleUC157);
            this.Controls.Add(this.label160);
            this.Controls.Add(this.label161);
            this.Controls.Add(this.label162);
            this.Controls.Add(this.retangleUC159);
            this.Controls.Add(this.retangleUC160);
            this.Controls.Add(this.retangleUC161);
            this.Controls.Add(this.lineUC28);
            this.Controls.Add(this.retangleUC162);
            this.Controls.Add(this.label163);
            this.Controls.Add(this.label164);
            this.Controls.Add(this.retangleUC163);
            this.Controls.Add(this.label166);
            this.Controls.Add(this.label167);
            this.Controls.Add(this.label168);
            this.Controls.Add(this.retangleUC165);
            this.Controls.Add(this.retangleUC166);
            this.Controls.Add(this.retangleUC167);
            this.Controls.Add(this.lineUC29);
            this.Controls.Add(this.retangleUC168);
            this.Controls.Add(this.label169);
            this.Controls.Add(this.label170);
            this.Controls.Add(this.retangleUC169);
            this.Controls.Add(this.label172);
            this.Controls.Add(this.label173);
            this.Controls.Add(this.label174);
            this.Controls.Add(this.retangleUC171);
            this.Controls.Add(this.retangleUC172);
            this.Controls.Add(this.retangleUC173);
            this.Controls.Add(this.lineUC30);
            this.Controls.Add(this.retangleUC174);
            this.Controls.Add(this.label175);
            this.Controls.Add(this.label176);
            this.Controls.Add(this.retangleUC175);
            this.Controls.Add(this.label178);
            this.Controls.Add(this.label179);
            this.Controls.Add(this.label180);
            this.Controls.Add(this.retangleUC177);
            this.Controls.Add(this.retangleUC178);
            this.Controls.Add(this.retangleUC179);
            this.Controls.Add(this.lineUC31);
            this.Controls.Add(this.retangleUC180);
            this.Controls.Add(this.label133);
            this.Controls.Add(this.label134);
            this.Controls.Add(this.retangleUC133);
            this.Controls.Add(this.label136);
            this.Controls.Add(this.label137);
            this.Controls.Add(this.label138);
            this.Controls.Add(this.retangleUC135);
            this.Controls.Add(this.retangleUC136);
            this.Controls.Add(this.retangleUC137);
            this.Controls.Add(this.lineUC24);
            this.Controls.Add(this.retangleUC138);
            this.Controls.Add(this.label139);
            this.Controls.Add(this.label140);
            this.Controls.Add(this.retangleUC139);
            this.Controls.Add(this.label142);
            this.Controls.Add(this.label143);
            this.Controls.Add(this.label144);
            this.Controls.Add(this.retangleUC141);
            this.Controls.Add(this.retangleUC142);
            this.Controls.Add(this.retangleUC143);
            this.Controls.Add(this.lineUC25);
            this.Controls.Add(this.retangleUC144);
            this.Controls.Add(this.label145);
            this.Controls.Add(this.label146);
            this.Controls.Add(this.retangleUC145);
            this.Controls.Add(this.label148);
            this.Controls.Add(this.label149);
            this.Controls.Add(this.label150);
            this.Controls.Add(this.retangleUC147);
            this.Controls.Add(this.retangleUC148);
            this.Controls.Add(this.retangleUC149);
            this.Controls.Add(this.lineUC26);
            this.Controls.Add(this.retangleUC150);
            this.Controls.Add(this.label151);
            this.Controls.Add(this.label152);
            this.Controls.Add(this.retangleUC151);
            this.Controls.Add(this.label154);
            this.Controls.Add(this.label155);
            this.Controls.Add(this.label156);
            this.Controls.Add(this.retangleUC153);
            this.Controls.Add(this.retangleUC154);
            this.Controls.Add(this.retangleUC155);
            this.Controls.Add(this.lineUC27);
            this.Controls.Add(this.retangleUC156);
            this.Controls.Add(this.label109);
            this.Controls.Add(this.label110);
            this.Controls.Add(this.retangleUC109);
            this.Controls.Add(this.label112);
            this.Controls.Add(this.label113);
            this.Controls.Add(this.label114);
            this.Controls.Add(this.retangleUC111);
            this.Controls.Add(this.retangleUC112);
            this.Controls.Add(this.retangleUC113);
            this.Controls.Add(this.lineUC20);
            this.Controls.Add(this.retangleUC114);
            this.Controls.Add(this.label115);
            this.Controls.Add(this.label116);
            this.Controls.Add(this.retangleUC115);
            this.Controls.Add(this.label118);
            this.Controls.Add(this.label119);
            this.Controls.Add(this.label120);
            this.Controls.Add(this.retangleUC117);
            this.Controls.Add(this.retangleUC118);
            this.Controls.Add(this.retangleUC119);
            this.Controls.Add(this.lineUC21);
            this.Controls.Add(this.retangleUC120);
            this.Controls.Add(this.label121);
            this.Controls.Add(this.label122);
            this.Controls.Add(this.retangleUC121);
            this.Controls.Add(this.label124);
            this.Controls.Add(this.label125);
            this.Controls.Add(this.label126);
            this.Controls.Add(this.retangleUC123);
            this.Controls.Add(this.retangleUC124);
            this.Controls.Add(this.retangleUC125);
            this.Controls.Add(this.lineUC22);
            this.Controls.Add(this.retangleUC126);
            this.Controls.Add(this.label127);
            this.Controls.Add(this.label128);
            this.Controls.Add(this.retangleUC127);
            this.Controls.Add(this.label130);
            this.Controls.Add(this.label131);
            this.Controls.Add(this.label132);
            this.Controls.Add(this.retangleUC129);
            this.Controls.Add(this.retangleUC130);
            this.Controls.Add(this.retangleUC131);
            this.Controls.Add(this.lineUC23);
            this.Controls.Add(this.retangleUC132);
            this.Controls.Add(this.label103);
            this.Controls.Add(this.label104);
            this.Controls.Add(this.retangleUC103);
            this.Controls.Add(this.label106);
            this.Controls.Add(this.label107);
            this.Controls.Add(this.label108);
            this.Controls.Add(this.retangleUC105);
            this.Controls.Add(this.retangleUC106);
            this.Controls.Add(this.retangleUC107);
            this.Controls.Add(this.lineUC19);
            this.Controls.Add(this.retangleUC108);
            this.Controls.Add(this.label97);
            this.Controls.Add(this.label98);
            this.Controls.Add(this.retangleUC97);
            this.Controls.Add(this.label100);
            this.Controls.Add(this.label101);
            this.Controls.Add(this.label102);
            this.Controls.Add(this.retangleUC99);
            this.Controls.Add(this.retangleUC100);
            this.Controls.Add(this.retangleUC101);
            this.Controls.Add(this.lineUC18);
            this.Controls.Add(this.retangleUC102);
            this.Controls.Add(this.label88);
            this.Controls.Add(this.label89);
            this.Controls.Add(this.retangleUC87);
            this.Controls.Add(this.label91);
            this.Controls.Add(this.label92);
            this.Controls.Add(this.label93);
            this.Controls.Add(this.retangleUC89);
            this.Controls.Add(this.retangleUC90);
            this.Controls.Add(this.retangleUC91);
            this.Controls.Add(this.lineUC16);
            this.Controls.Add(this.retangleUC92);
            this.Controls.Add(this.label85);
            this.Controls.Add(this.label86);
            this.Controls.Add(this.retangleUC85);
            this.Controls.Add(this.label94);
            this.Controls.Add(this.label95);
            this.Controls.Add(this.label96);
            this.Controls.Add(this.retangleUC93);
            this.Controls.Add(this.retangleUC94);
            this.Controls.Add(this.retangleUC95);
            this.Controls.Add(this.lineUC17);
            this.Controls.Add(this.retangleUC96);
            this.Controls.Add(this.lineUC15);
            this.Controls.Add(this.lineUC14);
            this.Controls.Add(this.lineUC13);
            this.Controls.Add(this.lineUC12);
            this.Controls.Add(this.lineUC11);
            this.Controls.Add(this.lineUC10);
            this.Controls.Add(this.lineUC9);
            this.Controls.Add(this.lineUC8);
            this.Controls.Add(this.label73);
            this.Controls.Add(this.label75);
            this.Controls.Add(this.retangleUC74);
            this.Controls.Add(this.label77);
            this.Controls.Add(this.retangleUC76);
            this.Controls.Add(this.label78);
            this.Controls.Add(this.retangleUC77);
            this.Controls.Add(this.label80);
            this.Controls.Add(this.retangleUC79);
            this.Controls.Add(this.label81);
            this.Controls.Add(this.label82);
            this.Controls.Add(this.label83);
            this.Controls.Add(this.label84);
            this.Controls.Add(this.retangleUC80);
            this.Controls.Add(this.retangleUC81);
            this.Controls.Add(this.retangleUC82);
            this.Controls.Add(this.retangleUC83);
            this.Controls.Add(this.lineUC7);
            this.Controls.Add(this.retangleUC84);
            this.Controls.Add(this.label61);
            this.Controls.Add(this.label63);
            this.Controls.Add(this.retangleUC62);
            this.Controls.Add(this.label65);
            this.Controls.Add(this.retangleUC64);
            this.Controls.Add(this.label66);
            this.Controls.Add(this.retangleUC65);
            this.Controls.Add(this.label68);
            this.Controls.Add(this.retangleUC67);
            this.Controls.Add(this.label69);
            this.Controls.Add(this.label70);
            this.Controls.Add(this.label71);
            this.Controls.Add(this.label72);
            this.Controls.Add(this.retangleUC68);
            this.Controls.Add(this.retangleUC69);
            this.Controls.Add(this.retangleUC70);
            this.Controls.Add(this.retangleUC71);
            this.Controls.Add(this.lineUC6);
            this.Controls.Add(this.retangleUC72);
            this.Controls.Add(this.label49);
            this.Controls.Add(this.label51);
            this.Controls.Add(this.retangleUC50);
            this.Controls.Add(this.label53);
            this.Controls.Add(this.retangleUC52);
            this.Controls.Add(this.label54);
            this.Controls.Add(this.retangleUC53);
            this.Controls.Add(this.label56);
            this.Controls.Add(this.retangleUC55);
            this.Controls.Add(this.label57);
            this.Controls.Add(this.label58);
            this.Controls.Add(this.label59);
            this.Controls.Add(this.label60);
            this.Controls.Add(this.retangleUC56);
            this.Controls.Add(this.retangleUC57);
            this.Controls.Add(this.retangleUC58);
            this.Controls.Add(this.retangleUC59);
            this.Controls.Add(this.lineUC5);
            this.Controls.Add(this.retangleUC60);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.retangleUC38);
            this.Controls.Add(this.label41);
            this.Controls.Add(this.retangleUC40);
            this.Controls.Add(this.label42);
            this.Controls.Add(this.retangleUC41);
            this.Controls.Add(this.label44);
            this.Controls.Add(this.retangleUC43);
            this.Controls.Add(this.label45);
            this.Controls.Add(this.label46);
            this.Controls.Add(this.label47);
            this.Controls.Add(this.label48);
            this.Controls.Add(this.retangleUC44);
            this.Controls.Add(this.retangleUC45);
            this.Controls.Add(this.retangleUC46);
            this.Controls.Add(this.retangleUC47);
            this.Controls.Add(this.lineUC4);
            this.Controls.Add(this.retangleUC48);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.retangleUC26);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.retangleUC28);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.retangleUC29);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.retangleUC31);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.retangleUC32);
            this.Controls.Add(this.retangleUC33);
            this.Controls.Add(this.retangleUC34);
            this.Controls.Add(this.retangleUC35);
            this.Controls.Add(this.lineUC3);
            this.Controls.Add(this.retangleUC36);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.retangleUC13);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.retangleUC16);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.retangleUC17);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.retangleUC19);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.retangleUC20);
            this.Controls.Add(this.retangleUC21);
            this.Controls.Add(this.retangleUC22);
            this.Controls.Add(this.retangleUC23);
            this.Controls.Add(this.lineUC2);
            this.Controls.Add(this.retangleUC24);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.retangleUC14);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.retangleUC10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.retangleUC11);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.retangleUC7);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.retangleUC5);
            this.Controls.Add(this.retangleUC4);
            this.Controls.Add(this.retangleUC3);
            this.Controls.Add(this.retangleUC2);
            this.Controls.Add(this.lineUC1);
            this.Controls.Add(this.retangleUC1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form_Total2";
            this.Text = "Form_Total2";
            this.Load += new System.EventHandler(this.Form_Total2_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC1;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC1;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC2;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC3;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC4;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC7;
        private System.Windows.Forms.Label label8;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC10;
        private System.Windows.Forms.Label label9;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC11;
        private System.Windows.Forms.Label label12;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC14;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC13;
        private System.Windows.Forms.Label label17;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC16;
        private System.Windows.Forms.Label label18;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC17;
        private System.Windows.Forms.Label label20;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC19;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC20;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC21;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC22;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC23;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC2;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label27;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC26;
        private System.Windows.Forms.Label label29;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC28;
        private System.Windows.Forms.Label label30;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC29;
        private System.Windows.Forms.Label label32;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC31;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC32;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC33;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC34;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC35;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC3;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label39;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC38;
        private System.Windows.Forms.Label label41;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC40;
        private System.Windows.Forms.Label label42;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC41;
        private System.Windows.Forms.Label label44;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC43;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC44;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC45;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC46;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC47;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC4;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label51;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC50;
        private System.Windows.Forms.Label label53;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC52;
        private System.Windows.Forms.Label label54;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC53;
        private System.Windows.Forms.Label label56;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC55;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC56;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC57;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC58;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC59;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC5;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC60;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label63;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC62;
        private System.Windows.Forms.Label label65;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC64;
        private System.Windows.Forms.Label label66;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC65;
        private System.Windows.Forms.Label label68;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC67;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label72;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC68;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC69;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC70;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC71;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC6;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC72;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label75;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC74;
        private System.Windows.Forms.Label label77;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC76;
        private System.Windows.Forms.Label label78;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC77;
        private System.Windows.Forms.Label label80;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC79;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label84;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC80;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC81;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC82;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC83;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC7;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC84;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC8;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC9;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC10;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC11;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC12;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC13;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC14;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC15;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label86;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC85;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label label96;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC93;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC94;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC95;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC17;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC96;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label label89;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC87;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.Label label93;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC89;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC90;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC91;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC16;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC92;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label98;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC97;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label label102;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC99;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC100;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC101;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC18;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC102;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label104;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC103;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label label108;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC105;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC106;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC107;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC19;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC108;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.Label label110;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC109;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.Label label114;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC111;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC112;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC113;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC20;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC114;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.Label label116;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC115;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.Label label119;
        private System.Windows.Forms.Label label120;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC117;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC118;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC119;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC21;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC120;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.Label label122;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC121;
        private System.Windows.Forms.Label label124;
        private System.Windows.Forms.Label label125;
        private System.Windows.Forms.Label label126;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC123;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC124;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC125;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC22;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC126;
        private System.Windows.Forms.Label label127;
        private System.Windows.Forms.Label label128;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC127;
        private System.Windows.Forms.Label label130;
        private System.Windows.Forms.Label label131;
        private System.Windows.Forms.Label label132;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC129;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC130;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC131;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC23;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC132;
        private System.Windows.Forms.Label label133;
        private System.Windows.Forms.Label label134;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC133;
        private System.Windows.Forms.Label label136;
        private System.Windows.Forms.Label label137;
        private System.Windows.Forms.Label label138;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC135;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC136;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC137;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC24;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC138;
        private System.Windows.Forms.Label label139;
        private System.Windows.Forms.Label label140;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC139;
        private System.Windows.Forms.Label label142;
        private System.Windows.Forms.Label label143;
        private System.Windows.Forms.Label label144;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC141;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC142;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC143;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC25;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC144;
        private System.Windows.Forms.Label label145;
        private System.Windows.Forms.Label label146;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC145;
        private System.Windows.Forms.Label label148;
        private System.Windows.Forms.Label label149;
        private System.Windows.Forms.Label label150;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC147;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC148;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC149;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC26;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC150;
        private System.Windows.Forms.Label label151;
        private System.Windows.Forms.Label label152;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC151;
        private System.Windows.Forms.Label label154;
        private System.Windows.Forms.Label label155;
        private System.Windows.Forms.Label label156;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC153;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC154;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC155;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC27;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC156;
        private System.Windows.Forms.Label label157;
        private System.Windows.Forms.Label label158;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC157;
        private System.Windows.Forms.Label label160;
        private System.Windows.Forms.Label label161;
        private System.Windows.Forms.Label label162;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC159;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC160;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC161;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC28;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC162;
        private System.Windows.Forms.Label label163;
        private System.Windows.Forms.Label label164;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC163;
        private System.Windows.Forms.Label label166;
        private System.Windows.Forms.Label label167;
        private System.Windows.Forms.Label label168;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC165;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC166;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC167;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC29;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC168;
        private System.Windows.Forms.Label label169;
        private System.Windows.Forms.Label label170;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC169;
        private System.Windows.Forms.Label label172;
        private System.Windows.Forms.Label label173;
        private System.Windows.Forms.Label label174;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC171;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC172;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC173;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC30;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC174;
        private System.Windows.Forms.Label label175;
        private System.Windows.Forms.Label label176;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC175;
        private System.Windows.Forms.Label label178;
        private System.Windows.Forms.Label label179;
        private System.Windows.Forms.Label label180;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC177;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC178;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC179;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC31;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC180;
        private System.Windows.Forms.Label label181;
        private System.Windows.Forms.Label label182;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC181;
        private System.Windows.Forms.Label label184;
        private System.Windows.Forms.Label label185;
        private System.Windows.Forms.Label label186;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC183;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC184;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC185;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC32;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC186;
        private System.Windows.Forms.Label label187;
        private System.Windows.Forms.Label label188;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC187;
        private System.Windows.Forms.Label label190;
        private System.Windows.Forms.Label label191;
        private System.Windows.Forms.Label label192;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC189;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC190;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC191;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC33;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC192;
        private System.Windows.Forms.Label label193;
        private System.Windows.Forms.Label label194;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC193;
        private System.Windows.Forms.Label label196;
        private System.Windows.Forms.Label label197;
        private System.Windows.Forms.Label label198;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC195;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC196;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC197;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC34;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC198;
        private System.Windows.Forms.Label label199;
        private System.Windows.Forms.Label label200;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC199;
        private System.Windows.Forms.Label label202;
        private System.Windows.Forms.Label label203;
        private System.Windows.Forms.Label label204;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC201;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC202;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC203;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC35;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC204;
        private System.Windows.Forms.Label label205;
        private System.Windows.Forms.Label label206;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC205;
        private System.Windows.Forms.Label label208;
        private System.Windows.Forms.Label label209;
        private System.Windows.Forms.Label label210;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC207;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC208;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC209;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC36;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC210;
        private System.Windows.Forms.Label label211;
        private System.Windows.Forms.Label label212;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC211;
        private System.Windows.Forms.Label label214;
        private System.Windows.Forms.Label label215;
        private System.Windows.Forms.Label label216;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC213;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC214;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC215;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC37;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC216;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC38;
        private System.Windows.Forms.Label label217;
        private System.Windows.Forms.Label label218;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC217;
        private System.Windows.Forms.Label label220;
        private System.Windows.Forms.Label label221;
        private System.Windows.Forms.Label label222;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC219;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC220;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC221;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC39;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC222;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC40;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC41;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC42;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC43;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC44;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC45;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC46;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC47;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC48;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC49;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC50;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC51;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC52;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC53;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC54;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC55;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC56;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC57;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC58;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC59;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC60;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC61;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC62;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC6;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC63;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC64;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
    }
}