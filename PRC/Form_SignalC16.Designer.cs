﻿namespace PRC
{
    partial class Form_SignalC16
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.stringSymbol86 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol45 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol74 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol84 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol50 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol82 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol83 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol81 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol79 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol80 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol78 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol15 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol16 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.retangleUC7 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC51 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC52 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.stringSymbol77 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol76 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol75 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol73 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol72 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol71 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol70 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol68 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol67 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol66 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol63 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol62 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol61 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol60 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol59 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol58 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol57 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol56 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol55 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol54 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol53 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol52 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol51 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol49 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol48 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol47 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol87 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol46 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol44 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol43 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol41 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol40 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol39 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol38 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.retangleUC6 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.stringSymbol36 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol35 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.retangleUC5 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.stringSymbol34 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol33 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol32 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol31 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol30 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol29 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol28 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol27 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol25 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol24 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol23 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol22 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol21 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol20 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol19 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol18 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol17 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.lineUC48 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC49 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC50 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC45 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC46 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC47 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC42 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC43 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC44 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC39 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC40 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC41 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.stringSymbol8 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.lineUC28 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC29 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC30 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC25 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC26 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC27 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC22 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC23 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC24 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC19 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC20 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC21 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC16 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC17 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC18 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC15 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC14 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC13 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.circleUC18 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC22 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC6 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC25 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC10 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.tram_Traffic_Light8 = new iSCADA.Design.Utilities.SIG.Tram_Traffic_Light();
            this.stringSymbol11 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.retangleUC4 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC9 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC10 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.tram_Traffic_Light7 = new iSCADA.Design.Utilities.SIG.Tram_Traffic_Light();
            this.route_Point7 = new iSCADA.Design.Utilities.SIG.Route_Point();
            this.route_Point8 = new iSCADA.Design.Utilities.SIG.Route_Point();
            this.route_Point5 = new iSCADA.Design.Utilities.SIG.Route_Point();
            this.route_Point6 = new iSCADA.Design.Utilities.SIG.Route_Point();
            this.tram_Traffic_Light6 = new iSCADA.Design.Utilities.SIG.Tram_Traffic_Light();
            this.tram_Traffic_Light5 = new iSCADA.Design.Utilities.SIG.Tram_Traffic_Light();
            this.stringSymbol6 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.retangleUC3 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC7 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC8 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.tram_Traffic_Light4 = new iSCADA.Design.Utilities.SIG.Tram_Traffic_Light();
            this.tram_Traffic_Light3 = new iSCADA.Design.Utilities.SIG.Tram_Traffic_Light();
            this.stringSymbol7 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.retangleUC2 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC5 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC6 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.route_Point2 = new iSCADA.Design.Utilities.SIG.Route_Point();
            this.route_Point4 = new iSCADA.Design.Utilities.SIG.Route_Point();
            this.tram_Traffic_Light2 = new iSCADA.Design.Utilities.SIG.Tram_Traffic_Light();
            this.circleUC16 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC12 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.route_Point1 = new iSCADA.Design.Utilities.SIG.Route_Point();
            this.route_Point3 = new iSCADA.Design.Utilities.SIG.Route_Point();
            this.circleUC4 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.stringSymbol2 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol1 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.retangleUC1 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC4 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.tram_Traffic_Light1 = new iSCADA.Design.Utilities.SIG.Tram_Traffic_Light();
            this.lineUC3 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC2 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.circleUC3 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.lineUC1 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.circleUC13 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC17 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC7 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC14 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC19 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC23 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC26 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.stringSymbol12 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.circleUC24 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.stringSymbol3 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.circleUC9 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.stringSymbol13 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.circleUC27 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.stringSymbol10 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.circleUC21 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.stringSymbol9 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.circleUC20 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.stringSymbol4 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.circleUC11 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC5 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.lineUC11 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC12 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC35 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC36 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC37 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC38 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC31 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC32 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC33 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC34 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.stringSymbol14 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol37 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol64 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol65 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol69 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.stringSymbol5 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.circleUC8 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.stringSymbol85 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.circleUC1 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC2 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.stringSymbol26 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.speed_Limit_Sign1 = new iSCADA.Design.Utilities.SIG.Speed_Limit_Sign();
            this.speed_Limit_Sign2 = new iSCADA.Design.Utilities.SIG.Speed_Limit_Sign();
            this.stringSymbol42 = new iSCADA.Design.Utilities.SIG.StringSymbol();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(1, 1);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1892, 858);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tabPage1.Controls.Add(this.stringSymbol86);
            this.tabPage1.Controls.Add(this.stringSymbol45);
            this.tabPage1.Controls.Add(this.stringSymbol74);
            this.tabPage1.Controls.Add(this.stringSymbol84);
            this.tabPage1.Controls.Add(this.stringSymbol50);
            this.tabPage1.Controls.Add(this.stringSymbol82);
            this.tabPage1.Controls.Add(this.stringSymbol83);
            this.tabPage1.Controls.Add(this.stringSymbol81);
            this.tabPage1.Controls.Add(this.stringSymbol79);
            this.tabPage1.Controls.Add(this.stringSymbol80);
            this.tabPage1.Controls.Add(this.stringSymbol78);
            this.tabPage1.Controls.Add(this.stringSymbol15);
            this.tabPage1.Controls.Add(this.stringSymbol16);
            this.tabPage1.Controls.Add(this.retangleUC7);
            this.tabPage1.Controls.Add(this.lineUC51);
            this.tabPage1.Controls.Add(this.lineUC52);
            this.tabPage1.Controls.Add(this.stringSymbol77);
            this.tabPage1.Controls.Add(this.stringSymbol76);
            this.tabPage1.Controls.Add(this.stringSymbol75);
            this.tabPage1.Controls.Add(this.stringSymbol73);
            this.tabPage1.Controls.Add(this.stringSymbol72);
            this.tabPage1.Controls.Add(this.stringSymbol71);
            this.tabPage1.Controls.Add(this.stringSymbol70);
            this.tabPage1.Controls.Add(this.stringSymbol68);
            this.tabPage1.Controls.Add(this.stringSymbol67);
            this.tabPage1.Controls.Add(this.stringSymbol66);
            this.tabPage1.Controls.Add(this.stringSymbol63);
            this.tabPage1.Controls.Add(this.stringSymbol62);
            this.tabPage1.Controls.Add(this.stringSymbol61);
            this.tabPage1.Controls.Add(this.stringSymbol60);
            this.tabPage1.Controls.Add(this.stringSymbol59);
            this.tabPage1.Controls.Add(this.stringSymbol58);
            this.tabPage1.Controls.Add(this.stringSymbol57);
            this.tabPage1.Controls.Add(this.stringSymbol56);
            this.tabPage1.Controls.Add(this.stringSymbol55);
            this.tabPage1.Controls.Add(this.stringSymbol54);
            this.tabPage1.Controls.Add(this.stringSymbol53);
            this.tabPage1.Controls.Add(this.stringSymbol52);
            this.tabPage1.Controls.Add(this.stringSymbol51);
            this.tabPage1.Controls.Add(this.stringSymbol49);
            this.tabPage1.Controls.Add(this.stringSymbol48);
            this.tabPage1.Controls.Add(this.stringSymbol47);
            this.tabPage1.Controls.Add(this.stringSymbol87);
            this.tabPage1.Controls.Add(this.stringSymbol46);
            this.tabPage1.Controls.Add(this.stringSymbol44);
            this.tabPage1.Controls.Add(this.stringSymbol43);
            this.tabPage1.Controls.Add(this.stringSymbol41);
            this.tabPage1.Controls.Add(this.stringSymbol40);
            this.tabPage1.Controls.Add(this.stringSymbol39);
            this.tabPage1.Controls.Add(this.stringSymbol38);
            this.tabPage1.Controls.Add(this.retangleUC6);
            this.tabPage1.Controls.Add(this.stringSymbol36);
            this.tabPage1.Controls.Add(this.stringSymbol35);
            this.tabPage1.Controls.Add(this.retangleUC5);
            this.tabPage1.Controls.Add(this.stringSymbol34);
            this.tabPage1.Controls.Add(this.stringSymbol33);
            this.tabPage1.Controls.Add(this.stringSymbol32);
            this.tabPage1.Controls.Add(this.stringSymbol31);
            this.tabPage1.Controls.Add(this.stringSymbol30);
            this.tabPage1.Controls.Add(this.stringSymbol29);
            this.tabPage1.Controls.Add(this.stringSymbol28);
            this.tabPage1.Controls.Add(this.stringSymbol27);
            this.tabPage1.Controls.Add(this.stringSymbol25);
            this.tabPage1.Controls.Add(this.stringSymbol24);
            this.tabPage1.Controls.Add(this.stringSymbol23);
            this.tabPage1.Controls.Add(this.stringSymbol22);
            this.tabPage1.Controls.Add(this.stringSymbol21);
            this.tabPage1.Controls.Add(this.stringSymbol20);
            this.tabPage1.Controls.Add(this.stringSymbol19);
            this.tabPage1.Controls.Add(this.stringSymbol18);
            this.tabPage1.Controls.Add(this.stringSymbol17);
            this.tabPage1.Controls.Add(this.lineUC48);
            this.tabPage1.Controls.Add(this.lineUC49);
            this.tabPage1.Controls.Add(this.lineUC50);
            this.tabPage1.Controls.Add(this.lineUC45);
            this.tabPage1.Controls.Add(this.lineUC46);
            this.tabPage1.Controls.Add(this.lineUC47);
            this.tabPage1.Controls.Add(this.lineUC42);
            this.tabPage1.Controls.Add(this.lineUC43);
            this.tabPage1.Controls.Add(this.lineUC44);
            this.tabPage1.Controls.Add(this.lineUC39);
            this.tabPage1.Controls.Add(this.lineUC40);
            this.tabPage1.Controls.Add(this.lineUC41);
            this.tabPage1.Controls.Add(this.stringSymbol8);
            this.tabPage1.Controls.Add(this.lineUC28);
            this.tabPage1.Controls.Add(this.lineUC29);
            this.tabPage1.Controls.Add(this.lineUC30);
            this.tabPage1.Controls.Add(this.lineUC25);
            this.tabPage1.Controls.Add(this.lineUC26);
            this.tabPage1.Controls.Add(this.lineUC27);
            this.tabPage1.Controls.Add(this.lineUC22);
            this.tabPage1.Controls.Add(this.lineUC23);
            this.tabPage1.Controls.Add(this.lineUC24);
            this.tabPage1.Controls.Add(this.lineUC19);
            this.tabPage1.Controls.Add(this.lineUC20);
            this.tabPage1.Controls.Add(this.lineUC21);
            this.tabPage1.Controls.Add(this.lineUC16);
            this.tabPage1.Controls.Add(this.lineUC17);
            this.tabPage1.Controls.Add(this.lineUC18);
            this.tabPage1.Controls.Add(this.lineUC15);
            this.tabPage1.Controls.Add(this.lineUC14);
            this.tabPage1.Controls.Add(this.lineUC13);
            this.tabPage1.Controls.Add(this.circleUC18);
            this.tabPage1.Controls.Add(this.circleUC22);
            this.tabPage1.Controls.Add(this.circleUC6);
            this.tabPage1.Controls.Add(this.circleUC25);
            this.tabPage1.Controls.Add(this.circleUC10);
            this.tabPage1.Controls.Add(this.tram_Traffic_Light8);
            this.tabPage1.Controls.Add(this.stringSymbol11);
            this.tabPage1.Controls.Add(this.retangleUC4);
            this.tabPage1.Controls.Add(this.lineUC9);
            this.tabPage1.Controls.Add(this.lineUC10);
            this.tabPage1.Controls.Add(this.tram_Traffic_Light7);
            this.tabPage1.Controls.Add(this.route_Point7);
            this.tabPage1.Controls.Add(this.route_Point8);
            this.tabPage1.Controls.Add(this.route_Point5);
            this.tabPage1.Controls.Add(this.route_Point6);
            this.tabPage1.Controls.Add(this.tram_Traffic_Light6);
            this.tabPage1.Controls.Add(this.tram_Traffic_Light5);
            this.tabPage1.Controls.Add(this.stringSymbol6);
            this.tabPage1.Controls.Add(this.retangleUC3);
            this.tabPage1.Controls.Add(this.lineUC7);
            this.tabPage1.Controls.Add(this.lineUC8);
            this.tabPage1.Controls.Add(this.tram_Traffic_Light4);
            this.tabPage1.Controls.Add(this.tram_Traffic_Light3);
            this.tabPage1.Controls.Add(this.stringSymbol7);
            this.tabPage1.Controls.Add(this.retangleUC2);
            this.tabPage1.Controls.Add(this.lineUC5);
            this.tabPage1.Controls.Add(this.lineUC6);
            this.tabPage1.Controls.Add(this.route_Point2);
            this.tabPage1.Controls.Add(this.route_Point4);
            this.tabPage1.Controls.Add(this.tram_Traffic_Light2);
            this.tabPage1.Controls.Add(this.circleUC16);
            this.tabPage1.Controls.Add(this.circleUC12);
            this.tabPage1.Controls.Add(this.route_Point1);
            this.tabPage1.Controls.Add(this.route_Point3);
            this.tabPage1.Controls.Add(this.circleUC4);
            this.tabPage1.Controls.Add(this.stringSymbol2);
            this.tabPage1.Controls.Add(this.stringSymbol1);
            this.tabPage1.Controls.Add(this.retangleUC1);
            this.tabPage1.Controls.Add(this.lineUC4);
            this.tabPage1.Controls.Add(this.tram_Traffic_Light1);
            this.tabPage1.Controls.Add(this.lineUC3);
            this.tabPage1.Controls.Add(this.lineUC2);
            this.tabPage1.Controls.Add(this.circleUC3);
            this.tabPage1.Controls.Add(this.lineUC1);
            this.tabPage1.Controls.Add(this.circleUC13);
            this.tabPage1.Controls.Add(this.circleUC17);
            this.tabPage1.Controls.Add(this.circleUC7);
            this.tabPage1.Controls.Add(this.circleUC14);
            this.tabPage1.Controls.Add(this.circleUC19);
            this.tabPage1.Controls.Add(this.circleUC23);
            this.tabPage1.Controls.Add(this.circleUC26);
            this.tabPage1.Controls.Add(this.stringSymbol12);
            this.tabPage1.Controls.Add(this.circleUC24);
            this.tabPage1.Controls.Add(this.stringSymbol3);
            this.tabPage1.Controls.Add(this.circleUC9);
            this.tabPage1.Controls.Add(this.stringSymbol13);
            this.tabPage1.Controls.Add(this.circleUC27);
            this.tabPage1.Controls.Add(this.stringSymbol10);
            this.tabPage1.Controls.Add(this.circleUC21);
            this.tabPage1.Controls.Add(this.stringSymbol9);
            this.tabPage1.Controls.Add(this.circleUC20);
            this.tabPage1.Controls.Add(this.stringSymbol4);
            this.tabPage1.Controls.Add(this.circleUC11);
            this.tabPage1.Controls.Add(this.circleUC5);
            this.tabPage1.Controls.Add(this.lineUC11);
            this.tabPage1.Controls.Add(this.lineUC12);
            this.tabPage1.Controls.Add(this.lineUC35);
            this.tabPage1.Controls.Add(this.lineUC36);
            this.tabPage1.Controls.Add(this.lineUC37);
            this.tabPage1.Controls.Add(this.lineUC38);
            this.tabPage1.Controls.Add(this.lineUC31);
            this.tabPage1.Controls.Add(this.lineUC32);
            this.tabPage1.Controls.Add(this.lineUC33);
            this.tabPage1.Controls.Add(this.lineUC34);
            this.tabPage1.Controls.Add(this.stringSymbol14);
            this.tabPage1.Controls.Add(this.stringSymbol37);
            this.tabPage1.Controls.Add(this.stringSymbol64);
            this.tabPage1.Controls.Add(this.stringSymbol65);
            this.tabPage1.Controls.Add(this.stringSymbol69);
            this.tabPage1.Controls.Add(this.stringSymbol5);
            this.tabPage1.Controls.Add(this.circleUC8);
            this.tabPage1.Controls.Add(this.stringSymbol85);
            this.tabPage1.Controls.Add(this.circleUC1);
            this.tabPage1.Controls.Add(this.circleUC2);
            this.tabPage1.Controls.Add(this.stringSymbol26);
            this.tabPage1.Controls.Add(this.speed_Limit_Sign1);
            this.tabPage1.Controls.Add(this.speed_Limit_Sign2);
            this.tabPage1.Controls.Add(this.stringSymbol42);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1884, 832);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = " C16";
            // 
            // stringSymbol86
            // 
            this.stringSymbol86.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol86.ExtenderWidth = 3;
            this.stringSymbol86.GroupID = "";
            this.stringSymbol86.InnerText = "P.K. 09+311.99";
            this.stringSymbol86.Location = new System.Drawing.Point(932, 4);
            this.stringSymbol86.Name = "stringSymbol86";
            this.stringSymbol86.Reversion = true;
            this.stringSymbol86.Size = new System.Drawing.Size(16, 93);
            this.stringSymbol86.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol86.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.stringSymbol86.TabIndex = 418;
            // 
            // stringSymbol45
            // 
            this.stringSymbol45.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol45.ExtenderWidth = 3;
            this.stringSymbol45.GroupID = "";
            this.stringSymbol45.InnerText = "P.K. 09+683.68";
            this.stringSymbol45.Location = new System.Drawing.Point(1608, 630);
            this.stringSymbol45.Name = "stringSymbol45";
            this.stringSymbol45.Reversion = true;
            this.stringSymbol45.Size = new System.Drawing.Size(16, 93);
            this.stringSymbol45.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol45.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.stringSymbol45.TabIndex = 362;
            // 
            // stringSymbol74
            // 
            this.stringSymbol74.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol74.ExtenderWidth = 3;
            this.stringSymbol74.GroupID = "SIG-C16-ETS-002";
            this.stringSymbol74.InnerText = "SIG-C16-ETS-002";
            this.stringSymbol74.Location = new System.Drawing.Point(127, 233);
            this.stringSymbol74.Name = "stringSymbol74";
            this.stringSymbol74.Reversion = false;
            this.stringSymbol74.Size = new System.Drawing.Size(99, 18);
            this.stringSymbol74.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol74.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Verticle;
            this.stringSymbol74.TabIndex = 391;
            // 
            // stringSymbol84
            // 
            this.stringSymbol84.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol84.ExtenderWidth = 3;
            this.stringSymbol84.GroupID = "";
            this.stringSymbol84.InnerText = "P.K. 09+706.58";
            this.stringSymbol84.Location = new System.Drawing.Point(1638, 45);
            this.stringSymbol84.Name = "stringSymbol84";
            this.stringSymbol84.Reversion = true;
            this.stringSymbol84.Size = new System.Drawing.Size(16, 93);
            this.stringSymbol84.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol84.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.stringSymbol84.TabIndex = 412;
            // 
            // stringSymbol50
            // 
            this.stringSymbol50.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol50.ExtenderWidth = 3;
            this.stringSymbol50.GroupID = "SIG-C16-ETS-010";
            this.stringSymbol50.InnerText = "SIG-C16-ETS-010";
            this.stringSymbol50.Location = new System.Drawing.Point(1589, 234);
            this.stringSymbol50.Name = "stringSymbol50";
            this.stringSymbol50.Reversion = false;
            this.stringSymbol50.Size = new System.Drawing.Size(99, 18);
            this.stringSymbol50.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol50.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Verticle;
            this.stringSymbol50.TabIndex = 367;
            // 
            // stringSymbol82
            // 
            this.stringSymbol82.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol82.ExtenderWidth = 3;
            this.stringSymbol82.GroupID = "SIG-C16-EFS-008";
            this.stringSymbol82.InnerText = "SIG-C16-EFS-008";
            this.stringSymbol82.Location = new System.Drawing.Point(1656, 170);
            this.stringSymbol82.Name = "stringSymbol82";
            this.stringSymbol82.Reversion = false;
            this.stringSymbol82.Size = new System.Drawing.Size(99, 18);
            this.stringSymbol82.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol82.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Verticle;
            this.stringSymbol82.TabIndex = 411;
            // 
            // stringSymbol83
            // 
            this.stringSymbol83.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol83.ExtenderWidth = 3;
            this.stringSymbol83.GroupID = "SIG-C16-EFS-008";
            this.stringSymbol83.InnerText = "50";
            this.stringSymbol83.Location = new System.Drawing.Point(1687, 211);
            this.stringSymbol83.Name = "stringSymbol83";
            this.stringSymbol83.Reversion = true;
            this.stringSymbol83.Size = new System.Drawing.Size(17, 17);
            this.stringSymbol83.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol83.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.stringSymbol83.TabIndex = 410;
            // 
            // stringSymbol81
            // 
            this.stringSymbol81.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol81.ExtenderWidth = 3;
            this.stringSymbol81.GroupID = "";
            this.stringSymbol81.InnerText = "P.K. 09+713.26";
            this.stringSymbol81.Location = new System.Drawing.Point(1750, 630);
            this.stringSymbol81.Name = "stringSymbol81";
            this.stringSymbol81.Reversion = true;
            this.stringSymbol81.Size = new System.Drawing.Size(16, 93);
            this.stringSymbol81.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol81.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.stringSymbol81.TabIndex = 406;
            // 
            // stringSymbol79
            // 
            this.stringSymbol79.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol79.ExtenderWidth = 3;
            this.stringSymbol79.GroupID = "SIG-C16-EFS-007";
            this.stringSymbol79.InnerText = "SIG-C16-EFS-007";
            this.stringSymbol79.Location = new System.Drawing.Point(1733, 568);
            this.stringSymbol79.Name = "stringSymbol79";
            this.stringSymbol79.Reversion = false;
            this.stringSymbol79.Size = new System.Drawing.Size(99, 18);
            this.stringSymbol79.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol79.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Verticle;
            this.stringSymbol79.TabIndex = 405;
            // 
            // stringSymbol80
            // 
            this.stringSymbol80.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol80.ExtenderWidth = 3;
            this.stringSymbol80.GroupID = "SIG-C16-EFS-007";
            this.stringSymbol80.InnerText = "35";
            this.stringSymbol80.Location = new System.Drawing.Point(1796, 599);
            this.stringSymbol80.Name = "stringSymbol80";
            this.stringSymbol80.Reversion = false;
            this.stringSymbol80.Size = new System.Drawing.Size(16, 16);
            this.stringSymbol80.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol80.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.stringSymbol80.TabIndex = 404;
            // 
            // stringSymbol78
            // 
            this.stringSymbol78.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol78.ExtenderWidth = 3;
            this.stringSymbol78.GroupID = "";
            this.stringSymbol78.InnerText = "P.K. 09+601.67";
            this.stringSymbol78.Location = new System.Drawing.Point(1161, 713);
            this.stringSymbol78.Name = "stringSymbol78";
            this.stringSymbol78.Reversion = true;
            this.stringSymbol78.Size = new System.Drawing.Size(16, 93);
            this.stringSymbol78.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol78.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.stringSymbol78.TabIndex = 400;
            // 
            // stringSymbol15
            // 
            this.stringSymbol15.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol15.ExtenderWidth = 3;
            this.stringSymbol15.GroupID = "SIG-C16-EFS-003";
            this.stringSymbol15.InnerText = "SIG-C16-EFS-003";
            this.stringSymbol15.Location = new System.Drawing.Point(1146, 568);
            this.stringSymbol15.Name = "stringSymbol15";
            this.stringSymbol15.Reversion = false;
            this.stringSymbol15.Size = new System.Drawing.Size(99, 18);
            this.stringSymbol15.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol15.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Verticle;
            this.stringSymbol15.TabIndex = 399;
            // 
            // stringSymbol16
            // 
            this.stringSymbol16.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol16.ExtenderWidth = 3;
            this.stringSymbol16.GroupID = "SIG-C16-EFS-003";
            this.stringSymbol16.InnerText = "D";
            this.stringSymbol16.Location = new System.Drawing.Point(1203, 596);
            this.stringSymbol16.Name = "stringSymbol16";
            this.stringSymbol16.Reversion = false;
            this.stringSymbol16.Size = new System.Drawing.Size(20, 18);
            this.stringSymbol16.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol16.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.stringSymbol16.TabIndex = 398;
            // 
            // retangleUC7
            // 
            this.retangleUC7.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC7.ExtenderWidth = 3;
            this.retangleUC7.Fill = false;
            this.retangleUC7.GroupID = "SIG-C16-EFS-003";
            this.retangleUC7.Location = new System.Drawing.Point(1201, 593);
            this.retangleUC7.Name = "retangleUC7";
            this.retangleUC7.Size = new System.Drawing.Size(26, 25);
            this.retangleUC7.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC7.TabIndex = 396;
            // 
            // lineUC51
            // 
            this.lineUC51.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC51.ExtenderWidth = 3;
            this.lineUC51.Fill = false;
            this.lineUC51.GroupID = "SIG-C16-EFS-003";
            this.lineUC51.Location = new System.Drawing.Point(1161, 597);
            this.lineUC51.Name = "lineUC51";
            this.lineUC51.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC51.Size = new System.Drawing.Size(3, 16);
            this.lineUC51.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC51.TabIndex = 397;
            // 
            // lineUC52
            // 
            this.lineUC52.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC52.ExtenderWidth = 3;
            this.lineUC52.Fill = false;
            this.lineUC52.GroupID = "SIG-C16-EFS-003";
            this.lineUC52.Location = new System.Drawing.Point(1161, 604);
            this.lineUC52.Name = "lineUC52";
            this.lineUC52.Size = new System.Drawing.Size(43, 3);
            this.lineUC52.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC52.TabIndex = 395;
            // 
            // stringSymbol77
            // 
            this.stringSymbol77.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol77.ExtenderWidth = 3;
            this.stringSymbol77.GroupID = "";
            this.stringSymbol77.InnerText = "UP TRACK";
            this.stringSymbol77.Location = new System.Drawing.Point(1781, 526);
            this.stringSymbol77.Name = "stringSymbol77";
            this.stringSymbol77.Reversion = false;
            this.stringSymbol77.Size = new System.Drawing.Size(99, 18);
            this.stringSymbol77.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol77.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Verticle;
            this.stringSymbol77.TabIndex = 394;
            // 
            // stringSymbol76
            // 
            this.stringSymbol76.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol76.ExtenderWidth = 3;
            this.stringSymbol76.GroupID = "";
            this.stringSymbol76.InnerText = "DOWN TRACK";
            this.stringSymbol76.Location = new System.Drawing.Point(1781, 266);
            this.stringSymbol76.Name = "stringSymbol76";
            this.stringSymbol76.Reversion = false;
            this.stringSymbol76.Size = new System.Drawing.Size(99, 18);
            this.stringSymbol76.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol76.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Verticle;
            this.stringSymbol76.TabIndex = 393;
            // 
            // stringSymbol75
            // 
            this.stringSymbol75.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol75.ExtenderWidth = 3;
            this.stringSymbol75.GroupID = "";
            this.stringSymbol75.InnerText = "P.K. 09+236.33";
            this.stringSymbol75.Location = new System.Drawing.Point(171, 45);
            this.stringSymbol75.Name = "stringSymbol75";
            this.stringSymbol75.Reversion = true;
            this.stringSymbol75.Size = new System.Drawing.Size(16, 93);
            this.stringSymbol75.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol75.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.stringSymbol75.TabIndex = 392;
            // 
            // stringSymbol73
            // 
            this.stringSymbol73.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol73.ExtenderWidth = 3;
            this.stringSymbol73.GroupID = "";
            this.stringSymbol73.InnerText = "P.K. 09+241.33";
            this.stringSymbol73.Location = new System.Drawing.Point(213, 45);
            this.stringSymbol73.Name = "stringSymbol73";
            this.stringSymbol73.Reversion = true;
            this.stringSymbol73.Size = new System.Drawing.Size(16, 93);
            this.stringSymbol73.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol73.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.stringSymbol73.TabIndex = 390;
            // 
            // stringSymbol72
            // 
            this.stringSymbol72.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol72.ExtenderWidth = 3;
            this.stringSymbol72.GroupID = "SIG-J03-ETL-004";
            this.stringSymbol72.InnerText = "SIG-J03-ETL-004";
            this.stringSymbol72.Location = new System.Drawing.Point(225, 188);
            this.stringSymbol72.Name = "stringSymbol72";
            this.stringSymbol72.Reversion = false;
            this.stringSymbol72.Size = new System.Drawing.Size(99, 18);
            this.stringSymbol72.State = iSCADA.Design.Utilities.SIG.SymbolState.On;
            this.stringSymbol72.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol72.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Verticle;
            this.stringSymbol72.TabIndex = 389;
            // 
            // stringSymbol71
            // 
            this.stringSymbol71.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol71.ExtenderWidth = 3;
            this.stringSymbol71.GroupID = "";
            this.stringSymbol71.InnerText = "P.K. 9+242.33";
            this.stringSymbol71.Location = new System.Drawing.Point(312, 24);
            this.stringSymbol71.Name = "stringSymbol71";
            this.stringSymbol71.Reversion = true;
            this.stringSymbol71.Size = new System.Drawing.Size(15, 102);
            this.stringSymbol71.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol71.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.stringSymbol71.TabIndex = 388;
            // 
            // stringSymbol70
            // 
            this.stringSymbol70.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol70.ExtenderWidth = 3;
            this.stringSymbol70.GroupID = "";
            this.stringSymbol70.InnerText = "P.K. 9+267.49";
            this.stringSymbol70.Location = new System.Drawing.Point(423, 24);
            this.stringSymbol70.Name = "stringSymbol70";
            this.stringSymbol70.Reversion = true;
            this.stringSymbol70.Size = new System.Drawing.Size(15, 102);
            this.stringSymbol70.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol70.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.stringSymbol70.TabIndex = 387;
            // 
            // stringSymbol68
            // 
            this.stringSymbol68.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol68.ExtenderWidth = 3;
            this.stringSymbol68.GroupID = "SIG-C16-EFS-004";
            this.stringSymbol68.InnerText = "SIG-C16-EFS-004";
            this.stringSymbol68.Location = new System.Drawing.Point(453, 120);
            this.stringSymbol68.Name = "stringSymbol68";
            this.stringSymbol68.Reversion = false;
            this.stringSymbol68.Size = new System.Drawing.Size(99, 18);
            this.stringSymbol68.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol68.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Verticle;
            this.stringSymbol68.TabIndex = 385;
            // 
            // stringSymbol67
            // 
            this.stringSymbol67.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol67.ExtenderWidth = 3;
            this.stringSymbol67.GroupID = "SIG-J03-ETL-002";
            this.stringSymbol67.InnerText = "SIG-J03-ETL-002";
            this.stringSymbol67.Location = new System.Drawing.Point(453, 188);
            this.stringSymbol67.Name = "stringSymbol67";
            this.stringSymbol67.Reversion = false;
            this.stringSymbol67.Size = new System.Drawing.Size(99, 18);
            this.stringSymbol67.State = iSCADA.Design.Utilities.SIG.SymbolState.On;
            this.stringSymbol67.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol67.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Verticle;
            this.stringSymbol67.TabIndex = 384;
            // 
            // stringSymbol66
            // 
            this.stringSymbol66.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol66.ExtenderWidth = 3;
            this.stringSymbol66.GroupID = "SIG-C16-ETS-004";
            this.stringSymbol66.InnerText = "SIG-C16-ETS-004";
            this.stringSymbol66.Location = new System.Drawing.Point(585, 233);
            this.stringSymbol66.Name = "stringSymbol66";
            this.stringSymbol66.Reversion = false;
            this.stringSymbol66.Size = new System.Drawing.Size(99, 18);
            this.stringSymbol66.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol66.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Verticle;
            this.stringSymbol66.TabIndex = 383;
            // 
            // stringSymbol63
            // 
            this.stringSymbol63.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol63.ExtenderWidth = 3;
            this.stringSymbol63.GroupID = "";
            this.stringSymbol63.InnerText = "P.K. 09+278.49";
            this.stringSymbol63.Location = new System.Drawing.Point(622, 4);
            this.stringSymbol63.Name = "stringSymbol63";
            this.stringSymbol63.Reversion = true;
            this.stringSymbol63.Size = new System.Drawing.Size(16, 93);
            this.stringSymbol63.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol63.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.stringSymbol63.TabIndex = 380;
            // 
            // stringSymbol62
            // 
            this.stringSymbol62.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol62.ExtenderWidth = 3;
            this.stringSymbol62.GroupID = "";
            this.stringSymbol62.InnerText = "P.K. 09+283.99";
            this.stringSymbol62.Location = new System.Drawing.Point(720, 4);
            this.stringSymbol62.Name = "stringSymbol62";
            this.stringSymbol62.Reversion = true;
            this.stringSymbol62.Size = new System.Drawing.Size(16, 93);
            this.stringSymbol62.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol62.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.stringSymbol62.TabIndex = 379;
            // 
            // stringSymbol61
            // 
            this.stringSymbol61.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol61.ExtenderWidth = 3;
            this.stringSymbol61.GroupID = "";
            this.stringSymbol61.InnerText = "P.K. 9+297.99";
            this.stringSymbol61.Location = new System.Drawing.Point(836, 4);
            this.stringSymbol61.Name = "stringSymbol61";
            this.stringSymbol61.Reversion = true;
            this.stringSymbol61.Size = new System.Drawing.Size(16, 93);
            this.stringSymbol61.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol61.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.stringSymbol61.TabIndex = 378;
            // 
            // stringSymbol60
            // 
            this.stringSymbol60.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol60.ExtenderWidth = 3;
            this.stringSymbol60.GroupID = "";
            this.stringSymbol60.InnerText = "P.K. 09+327.50";
            this.stringSymbol60.Location = new System.Drawing.Point(1058, 45);
            this.stringSymbol60.Name = "stringSymbol60";
            this.stringSymbol60.Reversion = true;
            this.stringSymbol60.Size = new System.Drawing.Size(16, 93);
            this.stringSymbol60.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol60.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.stringSymbol60.TabIndex = 377;
            // 
            // stringSymbol59
            // 
            this.stringSymbol59.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol59.ExtenderWidth = 3;
            this.stringSymbol59.GroupID = "";
            this.stringSymbol59.InnerText = "P.K. 09+659.47";
            this.stringSymbol59.Location = new System.Drawing.Point(1287, 45);
            this.stringSymbol59.Name = "stringSymbol59";
            this.stringSymbol59.Reversion = true;
            this.stringSymbol59.Size = new System.Drawing.Size(16, 93);
            this.stringSymbol59.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol59.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.stringSymbol59.TabIndex = 376;
            // 
            // stringSymbol58
            // 
            this.stringSymbol58.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol58.ExtenderWidth = 3;
            this.stringSymbol58.GroupID = "";
            this.stringSymbol58.InnerText = "P.K. 09+664.47";
            this.stringSymbol58.Location = new System.Drawing.Point(1317, 45);
            this.stringSymbol58.Name = "stringSymbol58";
            this.stringSymbol58.Reversion = true;
            this.stringSymbol58.Size = new System.Drawing.Size(16, 93);
            this.stringSymbol58.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol58.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.stringSymbol58.TabIndex = 375;
            // 
            // stringSymbol57
            // 
            this.stringSymbol57.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol57.ExtenderWidth = 3;
            this.stringSymbol57.GroupID = "SIG-C16-ETS-008";
            this.stringSymbol57.InnerText = "SIG-C16-ETS-008";
            this.stringSymbol57.Location = new System.Drawing.Point(1255, 233);
            this.stringSymbol57.Name = "stringSymbol57";
            this.stringSymbol57.Reversion = false;
            this.stringSymbol57.Size = new System.Drawing.Size(99, 18);
            this.stringSymbol57.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol57.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Verticle;
            this.stringSymbol57.TabIndex = 374;
            // 
            // stringSymbol56
            // 
            this.stringSymbol56.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol56.ExtenderWidth = 3;
            this.stringSymbol56.GroupID = "SIG-J04-ETL-004";
            this.stringSymbol56.InnerText = "SIG-J04-ETL-004";
            this.stringSymbol56.Location = new System.Drawing.Point(1322, 188);
            this.stringSymbol56.Name = "stringSymbol56";
            this.stringSymbol56.Reversion = false;
            this.stringSymbol56.Size = new System.Drawing.Size(99, 18);
            this.stringSymbol56.State = iSCADA.Design.Utilities.SIG.SymbolState.On;
            this.stringSymbol56.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol56.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Verticle;
            this.stringSymbol56.TabIndex = 373;
            // 
            // stringSymbol55
            // 
            this.stringSymbol55.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol55.ExtenderWidth = 3;
            this.stringSymbol55.GroupID = "";
            this.stringSymbol55.InnerText = "P.K. 9+665.47";
            this.stringSymbol55.Location = new System.Drawing.Point(1406, 23);
            this.stringSymbol55.Name = "stringSymbol55";
            this.stringSymbol55.Reversion = true;
            this.stringSymbol55.Size = new System.Drawing.Size(15, 102);
            this.stringSymbol55.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol55.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.stringSymbol55.TabIndex = 372;
            // 
            // stringSymbol54
            // 
            this.stringSymbol54.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol54.ExtenderWidth = 3;
            this.stringSymbol54.GroupID = "";
            this.stringSymbol54.InnerText = "P.K. 9+695.58";
            this.stringSymbol54.Location = new System.Drawing.Point(1502, 23);
            this.stringSymbol54.Name = "stringSymbol54";
            this.stringSymbol54.Reversion = true;
            this.stringSymbol54.Size = new System.Drawing.Size(15, 102);
            this.stringSymbol54.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol54.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.stringSymbol54.TabIndex = 371;
            // 
            // stringSymbol53
            // 
            this.stringSymbol53.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol53.ExtenderWidth = 3;
            this.stringSymbol53.GroupID = "";
            this.stringSymbol53.InnerText = "P.K. 09+696.58";
            this.stringSymbol53.Location = new System.Drawing.Point(1608, 45);
            this.stringSymbol53.Name = "stringSymbol53";
            this.stringSymbol53.Reversion = true;
            this.stringSymbol53.Size = new System.Drawing.Size(16, 93);
            this.stringSymbol53.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol53.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.stringSymbol53.TabIndex = 370;
            // 
            // stringSymbol52
            // 
            this.stringSymbol52.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol52.ExtenderWidth = 3;
            this.stringSymbol52.GroupID = "SIG-J04-ETL-002";
            this.stringSymbol52.InnerText = "SIG-J04-ETL-002";
            this.stringSymbol52.Location = new System.Drawing.Point(1532, 188);
            this.stringSymbol52.Name = "stringSymbol52";
            this.stringSymbol52.Reversion = false;
            this.stringSymbol52.Size = new System.Drawing.Size(99, 18);
            this.stringSymbol52.State = iSCADA.Design.Utilities.SIG.SymbolState.On;
            this.stringSymbol52.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol52.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Verticle;
            this.stringSymbol52.TabIndex = 369;
            // 
            // stringSymbol51
            // 
            this.stringSymbol51.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol51.ExtenderWidth = 3;
            this.stringSymbol51.GroupID = "";
            this.stringSymbol51.InnerText = "P.K. 09+725.61";
            this.stringSymbol51.Location = new System.Drawing.Point(1735, 45);
            this.stringSymbol51.Name = "stringSymbol51";
            this.stringSymbol51.Reversion = true;
            this.stringSymbol51.Size = new System.Drawing.Size(16, 93);
            this.stringSymbol51.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol51.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.stringSymbol51.TabIndex = 368;
            // 
            // stringSymbol49
            // 
            this.stringSymbol49.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol49.ExtenderWidth = 3;
            this.stringSymbol49.GroupID = "";
            this.stringSymbol49.InnerText = "P.K. 09+746.58";
            this.stringSymbol49.Location = new System.Drawing.Point(1841, 45);
            this.stringSymbol49.Name = "stringSymbol49";
            this.stringSymbol49.Reversion = true;
            this.stringSymbol49.Size = new System.Drawing.Size(16, 93);
            this.stringSymbol49.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol49.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.stringSymbol49.TabIndex = 366;
            // 
            // stringSymbol48
            // 
            this.stringSymbol48.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol48.ExtenderWidth = 3;
            this.stringSymbol48.GroupID = "SIG-C16-EFS-002";
            this.stringSymbol48.InnerText = "SIG-C16-EFS-002";
            this.stringSymbol48.Location = new System.Drawing.Point(1771, 170);
            this.stringSymbol48.Name = "stringSymbol48";
            this.stringSymbol48.Reversion = false;
            this.stringSymbol48.Size = new System.Drawing.Size(99, 18);
            this.stringSymbol48.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol48.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Verticle;
            this.stringSymbol48.TabIndex = 365;
            // 
            // stringSymbol47
            // 
            this.stringSymbol47.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol47.ExtenderWidth = 3;
            this.stringSymbol47.GroupID = "";
            this.stringSymbol47.InnerText = "P.K. 09+688.68";
            this.stringSymbol47.Location = new System.Drawing.Point(1656, 625);
            this.stringSymbol47.Name = "stringSymbol47";
            this.stringSymbol47.Reversion = true;
            this.stringSymbol47.Size = new System.Drawing.Size(16, 93);
            this.stringSymbol47.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol47.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.stringSymbol47.TabIndex = 364;
            // 
            // stringSymbol87
            // 
            this.stringSymbol87.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol87.ExtenderWidth = 3;
            this.stringSymbol87.GroupID = "SIG-C16-ETS-009";
            this.stringSymbol87.InnerText = "SIG-C16-ETS-009";
            this.stringSymbol87.Location = new System.Drawing.Point(1605, 526);
            this.stringSymbol87.Name = "stringSymbol87";
            this.stringSymbol87.Reversion = false;
            this.stringSymbol87.Size = new System.Drawing.Size(99, 18);
            this.stringSymbol87.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol87.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Verticle;
            this.stringSymbol87.TabIndex = 363;
            // 
            // stringSymbol46
            // 
            this.stringSymbol46.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol46.ExtenderWidth = 3;
            this.stringSymbol46.GroupID = "SIG-C16-ETS-009";
            this.stringSymbol46.InnerText = "SIG-C16-ETS-009";
            this.stringSymbol46.Location = new System.Drawing.Point(1607, 526);
            this.stringSymbol46.Name = "stringSymbol46";
            this.stringSymbol46.Reversion = false;
            this.stringSymbol46.Size = new System.Drawing.Size(99, 18);
            this.stringSymbol46.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol46.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Verticle;
            this.stringSymbol46.TabIndex = 363;
            // 
            // stringSymbol44
            // 
            this.stringSymbol44.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol44.ExtenderWidth = 3;
            this.stringSymbol44.GroupID = "";
            this.stringSymbol44.InnerText = "P.K. 09+682.68";
            this.stringSymbol44.Location = new System.Drawing.Point(1502, 690);
            this.stringSymbol44.Name = "stringSymbol44";
            this.stringSymbol44.Reversion = true;
            this.stringSymbol44.Size = new System.Drawing.Size(15, 102);
            this.stringSymbol44.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol44.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.stringSymbol44.TabIndex = 361;
            // 
            // stringSymbol43
            // 
            this.stringSymbol43.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol43.ExtenderWidth = 3;
            this.stringSymbol43.GroupID = "";
            this.stringSymbol43.InnerText = "P.K. 09+652.67";
            this.stringSymbol43.Location = new System.Drawing.Point(1406, 690);
            this.stringSymbol43.Name = "stringSymbol43";
            this.stringSymbol43.Reversion = true;
            this.stringSymbol43.Size = new System.Drawing.Size(15, 102);
            this.stringSymbol43.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol43.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.stringSymbol43.TabIndex = 360;
            // 
            // stringSymbol41
            // 
            this.stringSymbol41.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol41.ExtenderWidth = 3;
            this.stringSymbol41.GroupID = "";
            this.stringSymbol41.InnerText = "P.K. 09+641.67";
            this.stringSymbol41.Location = new System.Drawing.Point(1268, 622);
            this.stringSymbol41.Name = "stringSymbol41";
            this.stringSymbol41.Reversion = true;
            this.stringSymbol41.Size = new System.Drawing.Size(16, 93);
            this.stringSymbol41.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol41.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.stringSymbol41.TabIndex = 358;
            // 
            // stringSymbol40
            // 
            this.stringSymbol40.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol40.ExtenderWidth = 3;
            this.stringSymbol40.GroupID = "";
            this.stringSymbol40.InnerText = "P.K. 09+314.77";
            this.stringSymbol40.Location = new System.Drawing.Point(1065, 710);
            this.stringSymbol40.Name = "stringSymbol40";
            this.stringSymbol40.Reversion = true;
            this.stringSymbol40.Size = new System.Drawing.Size(16, 93);
            this.stringSymbol40.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol40.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.stringSymbol40.TabIndex = 357;
            // 
            // stringSymbol39
            // 
            this.stringSymbol39.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol39.ExtenderWidth = 3;
            this.stringSymbol39.GroupID = "";
            this.stringSymbol39.InnerText = "P.K. 09+298.77";
            this.stringSymbol39.Location = new System.Drawing.Point(932, 716);
            this.stringSymbol39.Name = "stringSymbol39";
            this.stringSymbol39.Reversion = true;
            this.stringSymbol39.Size = new System.Drawing.Size(16, 93);
            this.stringSymbol39.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol39.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.stringSymbol39.TabIndex = 356;
            // 
            // stringSymbol38
            // 
            this.stringSymbol38.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol38.ExtenderWidth = 3;
            this.stringSymbol38.GroupID = "STATION C16";
            this.stringSymbol38.InnerText = "STATION C16";
            this.stringSymbol38.Location = new System.Drawing.Point(755, 189);
            this.stringSymbol38.Name = "stringSymbol38";
            this.stringSymbol38.Reversion = false;
            this.stringSymbol38.Size = new System.Drawing.Size(154, 27);
            this.stringSymbol38.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol38.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Verticle;
            this.stringSymbol38.TabIndex = 355;
            // 
            // retangleUC6
            // 
            this.retangleUC6.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC6.ExtenderWidth = 3;
            this.retangleUC6.Fill = false;
            this.retangleUC6.GroupID = null;
            this.retangleUC6.Location = new System.Drawing.Point(623, 127);
            this.retangleUC6.Name = "retangleUC6";
            this.retangleUC6.Size = new System.Drawing.Size(400, 56);
            this.retangleUC6.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC6.TabIndex = 354;
            // 
            // stringSymbol36
            // 
            this.stringSymbol36.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol36.ExtenderWidth = 3;
            this.stringSymbol36.GroupID = "STATION C16";
            this.stringSymbol36.InnerText = "STATION C16";
            this.stringSymbol36.Location = new System.Drawing.Point(755, 617);
            this.stringSymbol36.Name = "stringSymbol36";
            this.stringSymbol36.Reversion = false;
            this.stringSymbol36.Size = new System.Drawing.Size(154, 27);
            this.stringSymbol36.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol36.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Verticle;
            this.stringSymbol36.TabIndex = 352;
            // 
            // stringSymbol35
            // 
            this.stringSymbol35.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol35.ExtenderWidth = 3;
            this.stringSymbol35.GroupID = "";
            this.stringSymbol35.InnerText = "P.K. 09+270.77";
            this.stringSymbol35.Location = new System.Drawing.Point(720, 716);
            this.stringSymbol35.Name = "stringSymbol35";
            this.stringSymbol35.Reversion = true;
            this.stringSymbol35.Size = new System.Drawing.Size(16, 93);
            this.stringSymbol35.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol35.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.stringSymbol35.TabIndex = 351;
            // 
            // retangleUC5
            // 
            this.retangleUC5.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC5.ExtenderWidth = 3;
            this.retangleUC5.Fill = false;
            this.retangleUC5.GroupID = null;
            this.retangleUC5.Location = new System.Drawing.Point(622, 654);
            this.retangleUC5.Name = "retangleUC5";
            this.retangleUC5.Size = new System.Drawing.Size(401, 56);
            this.retangleUC5.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC5.TabIndex = 350;
            // 
            // stringSymbol34
            // 
            this.stringSymbol34.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol34.ExtenderWidth = 3;
            this.stringSymbol34.GroupID = "SIG-J04-ETL-003";
            this.stringSymbol34.InnerText = "SIG-J04-ETL-003";
            this.stringSymbol34.Location = new System.Drawing.Point(1530, 621);
            this.stringSymbol34.Name = "stringSymbol34";
            this.stringSymbol34.Reversion = false;
            this.stringSymbol34.Size = new System.Drawing.Size(99, 18);
            this.stringSymbol34.State = iSCADA.Design.Utilities.SIG.SymbolState.On;
            this.stringSymbol34.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol34.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Verticle;
            this.stringSymbol34.TabIndex = 349;
            // 
            // stringSymbol33
            // 
            this.stringSymbol33.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol33.ExtenderWidth = 3;
            this.stringSymbol33.GroupID = "SIG-J04-ETL-001";
            this.stringSymbol33.InnerText = "SIG-J04-ETL-001";
            this.stringSymbol33.Location = new System.Drawing.Point(1317, 621);
            this.stringSymbol33.Name = "stringSymbol33";
            this.stringSymbol33.Reversion = false;
            this.stringSymbol33.Size = new System.Drawing.Size(99, 18);
            this.stringSymbol33.State = iSCADA.Design.Utilities.SIG.SymbolState.On;
            this.stringSymbol33.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol33.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Verticle;
            this.stringSymbol33.TabIndex = 348;
            // 
            // stringSymbol32
            // 
            this.stringSymbol32.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol32.ExtenderWidth = 3;
            this.stringSymbol32.GroupID = "SIG-C16-ETS-007";
            this.stringSymbol32.InnerText = "SIG-C16-ETS-007";
            this.stringSymbol32.Location = new System.Drawing.Point(1233, 526);
            this.stringSymbol32.Name = "stringSymbol32";
            this.stringSymbol32.Reversion = false;
            this.stringSymbol32.Size = new System.Drawing.Size(99, 18);
            this.stringSymbol32.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol32.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Verticle;
            this.stringSymbol32.TabIndex = 347;
            // 
            // stringSymbol31
            // 
            this.stringSymbol31.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol31.ExtenderWidth = 3;
            this.stringSymbol31.GroupID = "SIG-C16-EFS-009";
            this.stringSymbol31.InnerText = "SIG-C16-EFS-009";
            this.stringSymbol31.Location = new System.Drawing.Point(1049, 595);
            this.stringSymbol31.Name = "stringSymbol31";
            this.stringSymbol31.Reversion = false;
            this.stringSymbol31.Size = new System.Drawing.Size(99, 18);
            this.stringSymbol31.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol31.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Verticle;
            this.stringSymbol31.TabIndex = 346;
            // 
            // stringSymbol30
            // 
            this.stringSymbol30.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol30.ExtenderWidth = 3;
            this.stringSymbol30.GroupID = "SIG-C16-ELA-003";
            this.stringSymbol30.InnerText = "SIG-C16-ELA-003";
            this.stringSymbol30.Location = new System.Drawing.Point(892, 526);
            this.stringSymbol30.Name = "stringSymbol30";
            this.stringSymbol30.Reversion = false;
            this.stringSymbol30.Size = new System.Drawing.Size(99, 18);
            this.stringSymbol30.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol30.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Verticle;
            this.stringSymbol30.TabIndex = 345;
            // 
            // stringSymbol29
            // 
            this.stringSymbol29.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol29.ExtenderWidth = 3;
            this.stringSymbol29.GroupID = "SIG-C16-ELA-001";
            this.stringSymbol29.InnerText = "SIG-C16-ELA-001";
            this.stringSymbol29.Location = new System.Drawing.Point(689, 526);
            this.stringSymbol29.Name = "stringSymbol29";
            this.stringSymbol29.Reversion = false;
            this.stringSymbol29.Size = new System.Drawing.Size(99, 18);
            this.stringSymbol29.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol29.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Verticle;
            this.stringSymbol29.TabIndex = 344;
            // 
            // stringSymbol28
            // 
            this.stringSymbol28.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol28.ExtenderWidth = 3;
            this.stringSymbol28.GroupID = "";
            this.stringSymbol28.InnerText = "P.K. 09+259.27";
            this.stringSymbol28.Location = new System.Drawing.Point(581, 630);
            this.stringSymbol28.Name = "stringSymbol28";
            this.stringSymbol28.Reversion = true;
            this.stringSymbol28.Size = new System.Drawing.Size(15, 102);
            this.stringSymbol28.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol28.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.stringSymbol28.TabIndex = 343;
            // 
            // stringSymbol27
            // 
            this.stringSymbol27.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol27.ExtenderWidth = 3;
            this.stringSymbol27.GroupID = "SIG-C16-ETS-003";
            this.stringSymbol27.InnerText = "SIG-C16-ETS-003";
            this.stringSymbol27.Location = new System.Drawing.Point(557, 526);
            this.stringSymbol27.Name = "stringSymbol27";
            this.stringSymbol27.Reversion = false;
            this.stringSymbol27.Size = new System.Drawing.Size(99, 18);
            this.stringSymbol27.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol27.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Verticle;
            this.stringSymbol27.TabIndex = 342;
            // 
            // stringSymbol25
            // 
            this.stringSymbol25.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol25.ExtenderWidth = 3;
            this.stringSymbol25.GroupID = "SIG-J03-ETL-003";
            this.stringSymbol25.InnerText = "SIG-J03-ETL-003";
            this.stringSymbol25.Location = new System.Drawing.Point(452, 621);
            this.stringSymbol25.Name = "stringSymbol25";
            this.stringSymbol25.Reversion = false;
            this.stringSymbol25.Size = new System.Drawing.Size(99, 18);
            this.stringSymbol25.State = iSCADA.Design.Utilities.SIG.SymbolState.On;
            this.stringSymbol25.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol25.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Verticle;
            this.stringSymbol25.TabIndex = 340;
            // 
            // stringSymbol24
            // 
            this.stringSymbol24.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol24.ExtenderWidth = 3;
            this.stringSymbol24.GroupID = "";
            this.stringSymbol24.InnerText = "P.K. 09+253.27";
            this.stringSymbol24.Location = new System.Drawing.Point(422, 690);
            this.stringSymbol24.Name = "stringSymbol24";
            this.stringSymbol24.Reversion = true;
            this.stringSymbol24.Size = new System.Drawing.Size(15, 102);
            this.stringSymbol24.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol24.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.stringSymbol24.TabIndex = 339;
            // 
            // stringSymbol23
            // 
            this.stringSymbol23.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol23.ExtenderWidth = 3;
            this.stringSymbol23.GroupID = "";
            this.stringSymbol23.InnerText = "P.K. 09+229.01";
            this.stringSymbol23.Location = new System.Drawing.Point(311, 690);
            this.stringSymbol23.Name = "stringSymbol23";
            this.stringSymbol23.Reversion = true;
            this.stringSymbol23.Size = new System.Drawing.Size(15, 102);
            this.stringSymbol23.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol23.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.stringSymbol23.TabIndex = 338;
            // 
            // stringSymbol22
            // 
            this.stringSymbol22.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol22.ExtenderWidth = 3;
            this.stringSymbol22.GroupID = "";
            this.stringSymbol22.InnerText = "P.K. 09+226.51";
            this.stringSymbol22.Location = new System.Drawing.Point(211, 633);
            this.stringSymbol22.Name = "stringSymbol22";
            this.stringSymbol22.Reversion = true;
            this.stringSymbol22.Size = new System.Drawing.Size(15, 102);
            this.stringSymbol22.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol22.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.stringSymbol22.TabIndex = 337;
            // 
            // stringSymbol21
            // 
            this.stringSymbol21.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol21.ExtenderWidth = 3;
            this.stringSymbol21.GroupID = "SIG-J03-ETL-001";
            this.stringSymbol21.InnerText = "SIG-J03-ETL-001";
            this.stringSymbol21.Location = new System.Drawing.Point(227, 621);
            this.stringSymbol21.Name = "stringSymbol21";
            this.stringSymbol21.Reversion = false;
            this.stringSymbol21.Size = new System.Drawing.Size(99, 18);
            this.stringSymbol21.State = iSCADA.Design.Utilities.SIG.SymbolState.On;
            this.stringSymbol21.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol21.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Verticle;
            this.stringSymbol21.TabIndex = 336;
            // 
            // stringSymbol20
            // 
            this.stringSymbol20.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol20.ExtenderWidth = 3;
            this.stringSymbol20.GroupID = "";
            this.stringSymbol20.InnerText = "P.K. 09+218.01";
            this.stringSymbol20.Location = new System.Drawing.Point(127, 625);
            this.stringSymbol20.Name = "stringSymbol20";
            this.stringSymbol20.Reversion = true;
            this.stringSymbol20.Size = new System.Drawing.Size(15, 102);
            this.stringSymbol20.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol20.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.stringSymbol20.TabIndex = 335;
            // 
            // stringSymbol19
            // 
            this.stringSymbol19.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol19.ExtenderWidth = 3;
            this.stringSymbol19.GroupID = "SIG-C16-ETS-001";
            this.stringSymbol19.InnerText = "SIG-C16-ETS-001";
            this.stringSymbol19.Location = new System.Drawing.Point(95, 527);
            this.stringSymbol19.Name = "stringSymbol19";
            this.stringSymbol19.Reversion = false;
            this.stringSymbol19.Size = new System.Drawing.Size(99, 18);
            this.stringSymbol19.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol19.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Verticle;
            this.stringSymbol19.TabIndex = 334;
            // 
            // stringSymbol18
            // 
            this.stringSymbol18.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol18.ExtenderWidth = 3;
            this.stringSymbol18.GroupID = "SIG-C16-EFS-001";
            this.stringSymbol18.InnerText = "SIG-C16-EFS-001";
            this.stringSymbol18.Location = new System.Drawing.Point(0, 574);
            this.stringSymbol18.Name = "stringSymbol18";
            this.stringSymbol18.Reversion = false;
            this.stringSymbol18.Size = new System.Drawing.Size(99, 18);
            this.stringSymbol18.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol18.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Verticle;
            this.stringSymbol18.TabIndex = 333;
            // 
            // stringSymbol17
            // 
            this.stringSymbol17.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol17.ExtenderWidth = 3;
            this.stringSymbol17.GroupID = "";
            this.stringSymbol17.InnerText = "P.K. 09+178.01";
            this.stringSymbol17.Location = new System.Drawing.Point(26, 633);
            this.stringSymbol17.Name = "stringSymbol17";
            this.stringSymbol17.Reversion = true;
            this.stringSymbol17.Size = new System.Drawing.Size(15, 102);
            this.stringSymbol17.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol17.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.stringSymbol17.TabIndex = 332;
            // 
            // lineUC48
            // 
            this.lineUC48.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC48.ExtenderWidth = 3;
            this.lineUC48.Fill = false;
            this.lineUC48.GroupID = "SIG-C15-EFS-001";
            this.lineUC48.Location = new System.Drawing.Point(1523, 188);
            this.lineUC48.Name = "lineUC48";
            this.lineUC48.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC48.Size = new System.Drawing.Size(3, 46);
            this.lineUC48.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC48.TabIndex = 329;
            // 
            // lineUC49
            // 
            this.lineUC49.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC49.ExtenderWidth = 3;
            this.lineUC49.Fill = false;
            this.lineUC49.GroupID = "SIG-C15-EFS-001";
            this.lineUC49.Location = new System.Drawing.Point(1523, 106);
            this.lineUC49.Name = "lineUC49";
            this.lineUC49.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC49.Size = new System.Drawing.Size(3, 46);
            this.lineUC49.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC49.TabIndex = 328;
            // 
            // lineUC50
            // 
            this.lineUC50.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC50.ExtenderWidth = 3;
            this.lineUC50.Fill = false;
            this.lineUC50.GroupID = "SIG-C15-EFS-001";
            this.lineUC50.Location = new System.Drawing.Point(1523, 21);
            this.lineUC50.Name = "lineUC50";
            this.lineUC50.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC50.Size = new System.Drawing.Size(3, 46);
            this.lineUC50.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC50.TabIndex = 327;
            // 
            // lineUC45
            // 
            this.lineUC45.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC45.ExtenderWidth = 3;
            this.lineUC45.Fill = false;
            this.lineUC45.GroupID = "SIG-C15-EFS-001";
            this.lineUC45.Location = new System.Drawing.Point(1427, 190);
            this.lineUC45.Name = "lineUC45";
            this.lineUC45.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC45.Size = new System.Drawing.Size(3, 46);
            this.lineUC45.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC45.TabIndex = 326;
            // 
            // lineUC46
            // 
            this.lineUC46.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC46.ExtenderWidth = 3;
            this.lineUC46.Fill = false;
            this.lineUC46.GroupID = "SIG-C15-EFS-001";
            this.lineUC46.Location = new System.Drawing.Point(1427, 108);
            this.lineUC46.Name = "lineUC46";
            this.lineUC46.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC46.Size = new System.Drawing.Size(3, 46);
            this.lineUC46.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC46.TabIndex = 325;
            // 
            // lineUC47
            // 
            this.lineUC47.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC47.ExtenderWidth = 3;
            this.lineUC47.Fill = false;
            this.lineUC47.GroupID = "SIG-C15-EFS-001";
            this.lineUC47.Location = new System.Drawing.Point(1427, 23);
            this.lineUC47.Name = "lineUC47";
            this.lineUC47.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC47.Size = new System.Drawing.Size(3, 46);
            this.lineUC47.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC47.TabIndex = 324;
            // 
            // lineUC42
            // 
            this.lineUC42.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC42.ExtenderWidth = 3;
            this.lineUC42.Fill = false;
            this.lineUC42.GroupID = "SIG-C15-EFS-001";
            this.lineUC42.Location = new System.Drawing.Point(1523, 746);
            this.lineUC42.Name = "lineUC42";
            this.lineUC42.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC42.Size = new System.Drawing.Size(3, 46);
            this.lineUC42.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC42.TabIndex = 323;
            // 
            // lineUC43
            // 
            this.lineUC43.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC43.ExtenderWidth = 3;
            this.lineUC43.Fill = false;
            this.lineUC43.GroupID = "SIG-C15-EFS-001";
            this.lineUC43.Location = new System.Drawing.Point(1523, 664);
            this.lineUC43.Name = "lineUC43";
            this.lineUC43.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC43.Size = new System.Drawing.Size(3, 46);
            this.lineUC43.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC43.TabIndex = 322;
            // 
            // lineUC44
            // 
            this.lineUC44.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC44.ExtenderWidth = 3;
            this.lineUC44.Fill = false;
            this.lineUC44.GroupID = "SIG-C15-EFS-001";
            this.lineUC44.Location = new System.Drawing.Point(1523, 579);
            this.lineUC44.Name = "lineUC44";
            this.lineUC44.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC44.Size = new System.Drawing.Size(3, 46);
            this.lineUC44.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC44.TabIndex = 321;
            // 
            // lineUC39
            // 
            this.lineUC39.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC39.ExtenderWidth = 3;
            this.lineUC39.Fill = false;
            this.lineUC39.GroupID = "SIG-C15-EFS-001";
            this.lineUC39.Location = new System.Drawing.Point(1427, 746);
            this.lineUC39.Name = "lineUC39";
            this.lineUC39.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC39.Size = new System.Drawing.Size(3, 46);
            this.lineUC39.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC39.TabIndex = 320;
            // 
            // lineUC40
            // 
            this.lineUC40.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC40.ExtenderWidth = 3;
            this.lineUC40.Fill = false;
            this.lineUC40.GroupID = "SIG-C15-EFS-001";
            this.lineUC40.Location = new System.Drawing.Point(1427, 664);
            this.lineUC40.Name = "lineUC40";
            this.lineUC40.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC40.Size = new System.Drawing.Size(3, 46);
            this.lineUC40.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC40.TabIndex = 319;
            // 
            // lineUC41
            // 
            this.lineUC41.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC41.ExtenderWidth = 3;
            this.lineUC41.Fill = false;
            this.lineUC41.GroupID = "SIG-C15-EFS-001";
            this.lineUC41.Location = new System.Drawing.Point(1427, 579);
            this.lineUC41.Name = "lineUC41";
            this.lineUC41.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC41.Size = new System.Drawing.Size(3, 46);
            this.lineUC41.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC41.TabIndex = 318;
            // 
            // stringSymbol8
            // 
            this.stringSymbol8.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol8.ExtenderWidth = 3;
            this.stringSymbol8.GroupID = "";
            this.stringSymbol8.InnerText = "3";
            this.stringSymbol8.Location = new System.Drawing.Point(341, 366);
            this.stringSymbol8.Name = "stringSymbol8";
            this.stringSymbol8.Reversion = false;
            this.stringSymbol8.Size = new System.Drawing.Size(96, 83);
            this.stringSymbol8.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol8.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Verticle;
            this.stringSymbol8.TabIndex = 308;
            // 
            // lineUC28
            // 
            this.lineUC28.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC28.ExtenderWidth = 3;
            this.lineUC28.Fill = false;
            this.lineUC28.GroupID = "SIG-C15-EFS-001";
            this.lineUC28.Location = new System.Drawing.Point(443, 746);
            this.lineUC28.Name = "lineUC28";
            this.lineUC28.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC28.Size = new System.Drawing.Size(3, 46);
            this.lineUC28.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC28.TabIndex = 307;
            // 
            // lineUC29
            // 
            this.lineUC29.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC29.ExtenderWidth = 3;
            this.lineUC29.Fill = false;
            this.lineUC29.GroupID = "SIG-C15-EFS-001";
            this.lineUC29.Location = new System.Drawing.Point(443, 664);
            this.lineUC29.Name = "lineUC29";
            this.lineUC29.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC29.Size = new System.Drawing.Size(3, 46);
            this.lineUC29.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC29.TabIndex = 306;
            // 
            // lineUC30
            // 
            this.lineUC30.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC30.ExtenderWidth = 3;
            this.lineUC30.Fill = false;
            this.lineUC30.GroupID = "SIG-C15-EFS-001";
            this.lineUC30.Location = new System.Drawing.Point(443, 579);
            this.lineUC30.Name = "lineUC30";
            this.lineUC30.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC30.Size = new System.Drawing.Size(3, 46);
            this.lineUC30.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC30.TabIndex = 305;
            // 
            // lineUC25
            // 
            this.lineUC25.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC25.ExtenderWidth = 3;
            this.lineUC25.Fill = false;
            this.lineUC25.GroupID = "SIG-C15-EFS-001";
            this.lineUC25.Location = new System.Drawing.Point(332, 746);
            this.lineUC25.Name = "lineUC25";
            this.lineUC25.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC25.Size = new System.Drawing.Size(3, 46);
            this.lineUC25.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC25.TabIndex = 304;
            // 
            // lineUC26
            // 
            this.lineUC26.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC26.ExtenderWidth = 3;
            this.lineUC26.Fill = false;
            this.lineUC26.GroupID = "SIG-C15-EFS-001";
            this.lineUC26.Location = new System.Drawing.Point(332, 664);
            this.lineUC26.Name = "lineUC26";
            this.lineUC26.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC26.Size = new System.Drawing.Size(3, 46);
            this.lineUC26.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC26.TabIndex = 303;
            // 
            // lineUC27
            // 
            this.lineUC27.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC27.ExtenderWidth = 3;
            this.lineUC27.Fill = false;
            this.lineUC27.GroupID = "SIG-C15-EFS-001";
            this.lineUC27.Location = new System.Drawing.Point(332, 579);
            this.lineUC27.Name = "lineUC27";
            this.lineUC27.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC27.Size = new System.Drawing.Size(3, 46);
            this.lineUC27.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC27.TabIndex = 302;
            // 
            // lineUC22
            // 
            this.lineUC22.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC22.ExtenderWidth = 3;
            this.lineUC22.Fill = false;
            this.lineUC22.GroupID = "SIG-C15-EFS-001";
            this.lineUC22.Location = new System.Drawing.Point(444, 189);
            this.lineUC22.Name = "lineUC22";
            this.lineUC22.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC22.Size = new System.Drawing.Size(3, 46);
            this.lineUC22.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC22.TabIndex = 301;
            // 
            // lineUC23
            // 
            this.lineUC23.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC23.ExtenderWidth = 3;
            this.lineUC23.Fill = false;
            this.lineUC23.GroupID = "SIG-C15-EFS-001";
            this.lineUC23.Location = new System.Drawing.Point(444, 107);
            this.lineUC23.Name = "lineUC23";
            this.lineUC23.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC23.Size = new System.Drawing.Size(3, 46);
            this.lineUC23.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC23.TabIndex = 300;
            // 
            // lineUC24
            // 
            this.lineUC24.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC24.ExtenderWidth = 3;
            this.lineUC24.Fill = false;
            this.lineUC24.GroupID = "SIG-C15-EFS-001";
            this.lineUC24.Location = new System.Drawing.Point(444, 22);
            this.lineUC24.Name = "lineUC24";
            this.lineUC24.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC24.Size = new System.Drawing.Size(3, 46);
            this.lineUC24.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC24.TabIndex = 299;
            // 
            // lineUC19
            // 
            this.lineUC19.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC19.ExtenderWidth = 3;
            this.lineUC19.Fill = false;
            this.lineUC19.GroupID = "SIG-C15-EFS-001";
            this.lineUC19.Location = new System.Drawing.Point(333, 189);
            this.lineUC19.Name = "lineUC19";
            this.lineUC19.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC19.Size = new System.Drawing.Size(3, 46);
            this.lineUC19.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC19.TabIndex = 298;
            // 
            // lineUC20
            // 
            this.lineUC20.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC20.ExtenderWidth = 3;
            this.lineUC20.Fill = false;
            this.lineUC20.GroupID = "SIG-C15-EFS-001";
            this.lineUC20.Location = new System.Drawing.Point(333, 107);
            this.lineUC20.Name = "lineUC20";
            this.lineUC20.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC20.Size = new System.Drawing.Size(3, 46);
            this.lineUC20.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC20.TabIndex = 297;
            // 
            // lineUC21
            // 
            this.lineUC21.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC21.ExtenderWidth = 3;
            this.lineUC21.Fill = false;
            this.lineUC21.GroupID = "SIG-C15-EFS-001";
            this.lineUC21.Location = new System.Drawing.Point(333, 22);
            this.lineUC21.Name = "lineUC21";
            this.lineUC21.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC21.Size = new System.Drawing.Size(3, 46);
            this.lineUC21.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC21.TabIndex = 296;
            // 
            // lineUC16
            // 
            this.lineUC16.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC16.ExtenderWidth = 3;
            this.lineUC16.Fill = false;
            this.lineUC16.GroupID = "SIG-C15-EFS-001";
            this.lineUC16.Location = new System.Drawing.Point(332, 505);
            this.lineUC16.Name = "lineUC16";
            this.lineUC16.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC16.Size = new System.Drawing.Size(3, 46);
            this.lineUC16.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC16.TabIndex = 295;
            // 
            // lineUC17
            // 
            this.lineUC17.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC17.ExtenderWidth = 3;
            this.lineUC17.Fill = false;
            this.lineUC17.GroupID = "SIG-C15-EFS-001";
            this.lineUC17.Location = new System.Drawing.Point(332, 424);
            this.lineUC17.Name = "lineUC17";
            this.lineUC17.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC17.Size = new System.Drawing.Size(3, 46);
            this.lineUC17.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC17.TabIndex = 294;
            // 
            // lineUC18
            // 
            this.lineUC18.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC18.ExtenderWidth = 3;
            this.lineUC18.Fill = false;
            this.lineUC18.GroupID = "SIG-C15-EFS-001";
            this.lineUC18.Location = new System.Drawing.Point(332, 342);
            this.lineUC18.Name = "lineUC18";
            this.lineUC18.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC18.Size = new System.Drawing.Size(3, 46);
            this.lineUC18.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC18.TabIndex = 293;
            // 
            // lineUC15
            // 
            this.lineUC15.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC15.ExtenderWidth = 3;
            this.lineUC15.Fill = false;
            this.lineUC15.GroupID = "SIG-C15-EFS-001";
            this.lineUC15.Location = new System.Drawing.Point(443, 505);
            this.lineUC15.Name = "lineUC15";
            this.lineUC15.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC15.Size = new System.Drawing.Size(3, 46);
            this.lineUC15.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC15.TabIndex = 292;
            // 
            // lineUC14
            // 
            this.lineUC14.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC14.ExtenderWidth = 3;
            this.lineUC14.Fill = false;
            this.lineUC14.GroupID = "SIG-C15-EFS-001";
            this.lineUC14.Location = new System.Drawing.Point(443, 424);
            this.lineUC14.Name = "lineUC14";
            this.lineUC14.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC14.Size = new System.Drawing.Size(3, 46);
            this.lineUC14.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC14.TabIndex = 291;
            // 
            // lineUC13
            // 
            this.lineUC13.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC13.ExtenderWidth = 3;
            this.lineUC13.Fill = false;
            this.lineUC13.GroupID = "SIG-C15-EFS-001";
            this.lineUC13.Location = new System.Drawing.Point(443, 342);
            this.lineUC13.Name = "lineUC13";
            this.lineUC13.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC13.Size = new System.Drawing.Size(3, 46);
            this.lineUC13.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC13.TabIndex = 290;
            // 
            // circleUC18
            // 
            this.circleUC18.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC18.ExtenderWidth = 3;
            this.circleUC18.GroupID = "SIG-C16-ETS-009";
            this.circleUC18.Location = new System.Drawing.Point(1638, 551);
            this.circleUC18.Name = "circleUC18";
            this.circleUC18.Size = new System.Drawing.Size(15, 15);
            this.circleUC18.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC18.TabIndex = 253;
            // 
            // circleUC22
            // 
            this.circleUC22.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC22.ExtenderWidth = 3;
            this.circleUC22.GroupID = "SIG-C16-ETS-004";
            this.circleUC22.Location = new System.Drawing.Point(623, 251);
            this.circleUC22.Name = "circleUC22";
            this.circleUC22.Size = new System.Drawing.Size(15, 15);
            this.circleUC22.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC22.TabIndex = 265;
            // 
            // circleUC6
            // 
            this.circleUC6.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC6.ExtenderWidth = 3;
            this.circleUC6.GroupID = "SIG-C16-ETS-003";
            this.circleUC6.Location = new System.Drawing.Point(581, 551);
            this.circleUC6.Name = "circleUC6";
            this.circleUC6.Size = new System.Drawing.Size(15, 15);
            this.circleUC6.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC6.TabIndex = 241;
            // 
            // circleUC25
            // 
            this.circleUC25.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC25.ExtenderWidth = 3;
            this.circleUC25.GroupID = "SIG-C16-ETS-002";
            this.circleUC25.Location = new System.Drawing.Point(159, 251);
            this.circleUC25.Name = "circleUC25";
            this.circleUC25.Size = new System.Drawing.Size(15, 15);
            this.circleUC25.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC25.TabIndex = 273;
            // 
            // circleUC10
            // 
            this.circleUC10.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC10.ExtenderWidth = 3;
            this.circleUC10.GroupID = "SIG-C16-ETS-007";
            this.circleUC10.Location = new System.Drawing.Point(1269, 551);
            this.circleUC10.Name = "circleUC10";
            this.circleUC10.Size = new System.Drawing.Size(15, 15);
            this.circleUC10.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC10.TabIndex = 249;
            // 
            // tram_Traffic_Light8
            // 
            this.tram_Traffic_Light8.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.tram_Traffic_Light8.ExtenderWidth = 3;
            this.tram_Traffic_Light8.GroupID = "SIG-J03-ETL-004";
            this.tram_Traffic_Light8.Location = new System.Drawing.Point(213, 198);
            this.tram_Traffic_Light8.Name = "tram_Traffic_Light8";
            this.tram_Traffic_Light8.Reversion = true;
            this.tram_Traffic_Light8.Size = new System.Drawing.Size(97, 48);
            this.tram_Traffic_Light8.State = iSCADA.Design.Utilities.SIG.SymbolState.On;
            this.tram_Traffic_Light8.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.tram_Traffic_Light8.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.tram_Traffic_Light8.TabIndex = 271;
            // 
            // stringSymbol11
            // 
            this.stringSymbol11.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol11.ExtenderWidth = 3;
            this.stringSymbol11.GroupID = "SIG-C16-EFS-004";
            this.stringSymbol11.InnerText = "RTS";
            this.stringSymbol11.Location = new System.Drawing.Point(480, 149);
            this.stringSymbol11.Name = "stringSymbol11";
            this.stringSymbol11.Reversion = true;
            this.stringSymbol11.Size = new System.Drawing.Size(15, 19);
            this.stringSymbol11.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol11.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.stringSymbol11.TabIndex = 270;
            // 
            // retangleUC4
            // 
            this.retangleUC4.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC4.ExtenderWidth = 3;
            this.retangleUC4.Fill = false;
            this.retangleUC4.GroupID = "SIG-C16-EFS-004";
            this.retangleUC4.Location = new System.Drawing.Point(474, 146);
            this.retangleUC4.Name = "retangleUC4";
            this.retangleUC4.Size = new System.Drawing.Size(26, 25);
            this.retangleUC4.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC4.TabIndex = 268;
            // 
            // lineUC9
            // 
            this.lineUC9.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC9.ExtenderWidth = 3;
            this.lineUC9.Fill = false;
            this.lineUC9.GroupID = "SIG-C16-EFS-004";
            this.lineUC9.Location = new System.Drawing.Point(541, 151);
            this.lineUC9.Name = "lineUC9";
            this.lineUC9.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC9.Size = new System.Drawing.Size(3, 16);
            this.lineUC9.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC9.TabIndex = 269;
            // 
            // lineUC10
            // 
            this.lineUC10.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC10.ExtenderWidth = 3;
            this.lineUC10.Fill = false;
            this.lineUC10.GroupID = "SIG-C16-EFS-004";
            this.lineUC10.Location = new System.Drawing.Point(501, 158);
            this.lineUC10.Name = "lineUC10";
            this.lineUC10.Size = new System.Drawing.Size(43, 3);
            this.lineUC10.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC10.TabIndex = 267;
            // 
            // tram_Traffic_Light7
            // 
            this.tram_Traffic_Light7.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.tram_Traffic_Light7.ExtenderWidth = 3;
            this.tram_Traffic_Light7.GroupID = "SIG-J03-ETL-002";
            this.tram_Traffic_Light7.Location = new System.Drawing.Point(455, 198);
            this.tram_Traffic_Light7.Name = "tram_Traffic_Light7";
            this.tram_Traffic_Light7.Reversion = false;
            this.tram_Traffic_Light7.Size = new System.Drawing.Size(97, 48);
            this.tram_Traffic_Light7.State = iSCADA.Design.Utilities.SIG.SymbolState.On;
            this.tram_Traffic_Light7.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.tram_Traffic_Light7.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.tram_Traffic_Light7.TabIndex = 266;
            // 
            // route_Point7
            // 
            this.route_Point7.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.route_Point7.ExtenderWidth = 3;
            this.route_Point7.Fill = true;
            this.route_Point7.GroupID = "SIG-C16-ELA-002";
            this.route_Point7.InnerText = null;
            this.route_Point7.Location = new System.Drawing.Point(712, 247);
            this.route_Point7.Name = "route_Point7";
            this.route_Point7.Reversion = false;
            this.route_Point7.Size = new System.Drawing.Size(25, 20);
            this.route_Point7.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.route_Point7.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.route_Point7.TabIndex = 262;
            // 
            // route_Point8
            // 
            this.route_Point8.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.route_Point8.ExtenderWidth = 3;
            this.route_Point8.Fill = true;
            this.route_Point8.GroupID = "SIG-C16-ELA-002";
            this.route_Point8.InnerText = null;
            this.route_Point8.Location = new System.Drawing.Point(734, 247);
            this.route_Point8.Name = "route_Point8";
            this.route_Point8.Reversion = true;
            this.route_Point8.Size = new System.Drawing.Size(25, 20);
            this.route_Point8.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.route_Point8.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.route_Point8.TabIndex = 263;
            // 
            // route_Point5
            // 
            this.route_Point5.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.route_Point5.ExtenderWidth = 3;
            this.route_Point5.Fill = true;
            this.route_Point5.GroupID = "SIG-C16-ELA-004";
            this.route_Point5.InnerText = null;
            this.route_Point5.Location = new System.Drawing.Point(923, 246);
            this.route_Point5.Name = "route_Point5";
            this.route_Point5.Reversion = false;
            this.route_Point5.Size = new System.Drawing.Size(25, 20);
            this.route_Point5.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.route_Point5.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.route_Point5.TabIndex = 260;
            // 
            // route_Point6
            // 
            this.route_Point6.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.route_Point6.ExtenderWidth = 3;
            this.route_Point6.Fill = true;
            this.route_Point6.GroupID = "SIG-C16-ELA-004";
            this.route_Point6.InnerText = null;
            this.route_Point6.Location = new System.Drawing.Point(945, 247);
            this.route_Point6.Name = "route_Point6";
            this.route_Point6.Reversion = true;
            this.route_Point6.Size = new System.Drawing.Size(25, 20);
            this.route_Point6.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.route_Point6.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.route_Point6.TabIndex = 261;
            // 
            // tram_Traffic_Light6
            // 
            this.tram_Traffic_Light6.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.tram_Traffic_Light6.ExtenderWidth = 3;
            this.tram_Traffic_Light6.GroupID = "SIG-J04-ETL-004";
            this.tram_Traffic_Light6.Location = new System.Drawing.Point(1317, 194);
            this.tram_Traffic_Light6.Name = "tram_Traffic_Light6";
            this.tram_Traffic_Light6.Reversion = true;
            this.tram_Traffic_Light6.Size = new System.Drawing.Size(97, 48);
            this.tram_Traffic_Light6.State = iSCADA.Design.Utilities.SIG.SymbolState.On;
            this.tram_Traffic_Light6.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.tram_Traffic_Light6.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.tram_Traffic_Light6.TabIndex = 259;
            // 
            // tram_Traffic_Light5
            // 
            this.tram_Traffic_Light5.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.tram_Traffic_Light5.ExtenderWidth = 3;
            this.tram_Traffic_Light5.GroupID = "SIG-J04-ETL-002";
            this.tram_Traffic_Light5.Location = new System.Drawing.Point(1532, 194);
            this.tram_Traffic_Light5.Name = "tram_Traffic_Light5";
            this.tram_Traffic_Light5.Reversion = false;
            this.tram_Traffic_Light5.Size = new System.Drawing.Size(97, 48);
            this.tram_Traffic_Light5.State = iSCADA.Design.Utilities.SIG.SymbolState.On;
            this.tram_Traffic_Light5.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.tram_Traffic_Light5.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.tram_Traffic_Light5.TabIndex = 258;
            // 
            // stringSymbol6
            // 
            this.stringSymbol6.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol6.ExtenderWidth = 3;
            this.stringSymbol6.GroupID = "SIG-C16-EFS-002";
            this.stringSymbol6.InnerText = "D";
            this.stringSymbol6.Location = new System.Drawing.Point(1793, 209);
            this.stringSymbol6.Name = "stringSymbol6";
            this.stringSymbol6.Reversion = true;
            this.stringSymbol6.Size = new System.Drawing.Size(20, 18);
            this.stringSymbol6.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol6.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.stringSymbol6.TabIndex = 257;
            // 
            // retangleUC3
            // 
            this.retangleUC3.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC3.ExtenderWidth = 3;
            this.retangleUC3.Fill = false;
            this.retangleUC3.GroupID = "SIG-C16-EFS-002";
            this.retangleUC3.Location = new System.Drawing.Point(1790, 206);
            this.retangleUC3.Name = "retangleUC3";
            this.retangleUC3.Size = new System.Drawing.Size(26, 25);
            this.retangleUC3.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC3.TabIndex = 255;
            // 
            // lineUC7
            // 
            this.lineUC7.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC7.ExtenderWidth = 3;
            this.lineUC7.Fill = false;
            this.lineUC7.GroupID = "SIG-C16-EFS-002";
            this.lineUC7.Location = new System.Drawing.Point(1854, 209);
            this.lineUC7.Name = "lineUC7";
            this.lineUC7.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC7.Size = new System.Drawing.Size(3, 16);
            this.lineUC7.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC7.TabIndex = 256;
            // 
            // lineUC8
            // 
            this.lineUC8.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC8.ExtenderWidth = 3;
            this.lineUC8.Fill = false;
            this.lineUC8.GroupID = "SIG-C16-EFS-002";
            this.lineUC8.Location = new System.Drawing.Point(1814, 217);
            this.lineUC8.Name = "lineUC8";
            this.lineUC8.Size = new System.Drawing.Size(43, 3);
            this.lineUC8.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC8.TabIndex = 254;
            // 
            // tram_Traffic_Light4
            // 
            this.tram_Traffic_Light4.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.tram_Traffic_Light4.ExtenderWidth = 3;
            this.tram_Traffic_Light4.GroupID = "SIG-J04-ETL-003";
            this.tram_Traffic_Light4.Location = new System.Drawing.Point(1532, 576);
            this.tram_Traffic_Light4.Name = "tram_Traffic_Light4";
            this.tram_Traffic_Light4.Reversion = false;
            this.tram_Traffic_Light4.Size = new System.Drawing.Size(97, 48);
            this.tram_Traffic_Light4.State = iSCADA.Design.Utilities.SIG.SymbolState.On;
            this.tram_Traffic_Light4.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.tram_Traffic_Light4.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.tram_Traffic_Light4.TabIndex = 251;
            // 
            // tram_Traffic_Light3
            // 
            this.tram_Traffic_Light3.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.tram_Traffic_Light3.ExtenderWidth = 3;
            this.tram_Traffic_Light3.GroupID = "SIG-J04-ETL-001";
            this.tram_Traffic_Light3.Location = new System.Drawing.Point(1317, 576);
            this.tram_Traffic_Light3.Name = "tram_Traffic_Light3";
            this.tram_Traffic_Light3.Reversion = true;
            this.tram_Traffic_Light3.Size = new System.Drawing.Size(97, 48);
            this.tram_Traffic_Light3.State = iSCADA.Design.Utilities.SIG.SymbolState.On;
            this.tram_Traffic_Light3.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.tram_Traffic_Light3.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.tram_Traffic_Light3.TabIndex = 250;
            // 
            // stringSymbol7
            // 
            this.stringSymbol7.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol7.ExtenderWidth = 3;
            this.stringSymbol7.GroupID = "SIG-C16-EFS-009";
            this.stringSymbol7.InnerText = "RTS";
            this.stringSymbol7.Location = new System.Drawing.Point(1112, 622);
            this.stringSymbol7.Name = "stringSymbol7";
            this.stringSymbol7.Reversion = false;
            this.stringSymbol7.Size = new System.Drawing.Size(15, 19);
            this.stringSymbol7.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol7.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.stringSymbol7.TabIndex = 247;
            // 
            // retangleUC2
            // 
            this.retangleUC2.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC2.ExtenderWidth = 3;
            this.retangleUC2.Fill = false;
            this.retangleUC2.GroupID = "SIG-C16-EFS-009";
            this.retangleUC2.Location = new System.Drawing.Point(1105, 619);
            this.retangleUC2.Name = "retangleUC2";
            this.retangleUC2.Size = new System.Drawing.Size(26, 25);
            this.retangleUC2.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC2.TabIndex = 245;
            // 
            // lineUC5
            // 
            this.lineUC5.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC5.ExtenderWidth = 3;
            this.lineUC5.Fill = false;
            this.lineUC5.GroupID = "SIG-C16-EFS-009";
            this.lineUC5.Location = new System.Drawing.Point(1065, 624);
            this.lineUC5.Name = "lineUC5";
            this.lineUC5.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC5.Size = new System.Drawing.Size(3, 16);
            this.lineUC5.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC5.TabIndex = 246;
            // 
            // lineUC6
            // 
            this.lineUC6.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC6.ExtenderWidth = 3;
            this.lineUC6.Fill = false;
            this.lineUC6.GroupID = "SIG-C16-EFS-009";
            this.lineUC6.Location = new System.Drawing.Point(1065, 630);
            this.lineUC6.Name = "lineUC6";
            this.lineUC6.Size = new System.Drawing.Size(43, 3);
            this.lineUC6.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC6.TabIndex = 244;
            // 
            // route_Point2
            // 
            this.route_Point2.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.route_Point2.ExtenderWidth = 3;
            this.route_Point2.Fill = true;
            this.route_Point2.GroupID = "SIG-C16-ELA-003";
            this.route_Point2.InnerText = null;
            this.route_Point2.Location = new System.Drawing.Point(923, 549);
            this.route_Point2.Name = "route_Point2";
            this.route_Point2.Reversion = false;
            this.route_Point2.Size = new System.Drawing.Size(25, 20);
            this.route_Point2.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.route_Point2.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.route_Point2.TabIndex = 242;
            // 
            // route_Point4
            // 
            this.route_Point4.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.route_Point4.ExtenderWidth = 3;
            this.route_Point4.Fill = true;
            this.route_Point4.GroupID = "SIG-C16-ELA-003";
            this.route_Point4.InnerText = null;
            this.route_Point4.Location = new System.Drawing.Point(945, 549);
            this.route_Point4.Name = "route_Point4";
            this.route_Point4.Reversion = true;
            this.route_Point4.Size = new System.Drawing.Size(25, 20);
            this.route_Point4.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.route_Point4.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.route_Point4.TabIndex = 243;
            // 
            // tram_Traffic_Light2
            // 
            this.tram_Traffic_Light2.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.tram_Traffic_Light2.ExtenderWidth = 3;
            this.tram_Traffic_Light2.GroupID = "SIG-J03-ETL-003";
            this.tram_Traffic_Light2.Location = new System.Drawing.Point(453, 576);
            this.tram_Traffic_Light2.Name = "tram_Traffic_Light2";
            this.tram_Traffic_Light2.Reversion = false;
            this.tram_Traffic_Light2.Size = new System.Drawing.Size(97, 48);
            this.tram_Traffic_Light2.State = iSCADA.Design.Utilities.SIG.SymbolState.On;
            this.tram_Traffic_Light2.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.tram_Traffic_Light2.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.tram_Traffic_Light2.TabIndex = 239;
            // 
            // circleUC16
            // 
            this.circleUC16.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC16.ExtenderWidth = 3;
            this.circleUC16.GroupID = "SIG-C16-ETS-008";
            this.circleUC16.Location = new System.Drawing.Point(1288, 251);
            this.circleUC16.Name = "circleUC16";
            this.circleUC16.Size = new System.Drawing.Size(15, 15);
            this.circleUC16.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC16.TabIndex = 238;
            // 
            // circleUC12
            // 
            this.circleUC12.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC12.ExtenderWidth = 3;
            this.circleUC12.GroupID = "SIG-C16-ETS-010";
            this.circleUC12.Location = new System.Drawing.Point(1638, 251);
            this.circleUC12.Name = "circleUC12";
            this.circleUC12.Size = new System.Drawing.Size(15, 15);
            this.circleUC12.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC12.TabIndex = 236;
            // 
            // route_Point1
            // 
            this.route_Point1.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.route_Point1.ExtenderWidth = 3;
            this.route_Point1.Fill = true;
            this.route_Point1.GroupID = "SIG-C16-ELA-001";
            this.route_Point1.InnerText = null;
            this.route_Point1.Location = new System.Drawing.Point(711, 549);
            this.route_Point1.Name = "route_Point1";
            this.route_Point1.Reversion = false;
            this.route_Point1.Size = new System.Drawing.Size(25, 20);
            this.route_Point1.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.route_Point1.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.route_Point1.TabIndex = 233;
            // 
            // route_Point3
            // 
            this.route_Point3.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.route_Point3.ExtenderWidth = 3;
            this.route_Point3.Fill = true;
            this.route_Point3.GroupID = "SIG-C16-ELA-001";
            this.route_Point3.InnerText = null;
            this.route_Point3.Location = new System.Drawing.Point(733, 549);
            this.route_Point3.Name = "route_Point3";
            this.route_Point3.Reversion = true;
            this.route_Point3.Size = new System.Drawing.Size(25, 20);
            this.route_Point3.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.route_Point3.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.route_Point3.TabIndex = 234;
            // 
            // circleUC4
            // 
            this.circleUC4.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC4.ExtenderWidth = 3;
            this.circleUC4.GroupID = "SIG-C16-ETS-001";
            this.circleUC4.Location = new System.Drawing.Point(130, 551);
            this.circleUC4.Name = "circleUC4";
            this.circleUC4.Size = new System.Drawing.Size(15, 15);
            this.circleUC4.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC4.TabIndex = 232;
            // 
            // stringSymbol2
            // 
            this.stringSymbol2.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol2.ExtenderWidth = 3;
            this.stringSymbol2.GroupID = "SIG-C16PRSY1";
            this.stringSymbol2.InnerText = "X1";
            this.stringSymbol2.Location = new System.Drawing.Point(54, 541);
            this.stringSymbol2.Name = "stringSymbol2";
            this.stringSymbol2.Reversion = false;
            this.stringSymbol2.Size = new System.Drawing.Size(15, 15);
            this.stringSymbol2.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol2.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Verticle;
            this.stringSymbol2.TabIndex = 230;
            // 
            // stringSymbol1
            // 
            this.stringSymbol1.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol1.ExtenderWidth = 3;
            this.stringSymbol1.GroupID = "SIG-C16-EFS-001";
            this.stringSymbol1.InnerText = "D";
            this.stringSymbol1.Location = new System.Drawing.Point(58, 595);
            this.stringSymbol1.Name = "stringSymbol1";
            this.stringSymbol1.Reversion = false;
            this.stringSymbol1.Size = new System.Drawing.Size(20, 18);
            this.stringSymbol1.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol1.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.stringSymbol1.TabIndex = 229;
            // 
            // retangleUC1
            // 
            this.retangleUC1.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC1.ExtenderWidth = 3;
            this.retangleUC1.Fill = false;
            this.retangleUC1.GroupID = "SIG-C16-EFS-001";
            this.retangleUC1.Location = new System.Drawing.Point(56, 592);
            this.retangleUC1.Name = "retangleUC1";
            this.retangleUC1.Size = new System.Drawing.Size(26, 25);
            this.retangleUC1.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC1.TabIndex = 227;
            // 
            // lineUC4
            // 
            this.lineUC4.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC4.ExtenderWidth = 3;
            this.lineUC4.Fill = false;
            this.lineUC4.GroupID = "SIG-C16-EFS-001";
            this.lineUC4.Location = new System.Drawing.Point(13, 598);
            this.lineUC4.Name = "lineUC4";
            this.lineUC4.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC4.Size = new System.Drawing.Size(3, 16);
            this.lineUC4.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC4.TabIndex = 228;
            // 
            // tram_Traffic_Light1
            // 
            this.tram_Traffic_Light1.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.tram_Traffic_Light1.ExtenderWidth = 3;
            this.tram_Traffic_Light1.GroupID = "SIG-J03-ETL-001";
            this.tram_Traffic_Light1.Location = new System.Drawing.Point(211, 576);
            this.tram_Traffic_Light1.Name = "tram_Traffic_Light1";
            this.tram_Traffic_Light1.Reversion = true;
            this.tram_Traffic_Light1.Size = new System.Drawing.Size(97, 48);
            this.tram_Traffic_Light1.State = iSCADA.Design.Utilities.SIG.SymbolState.On;
            this.tram_Traffic_Light1.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.tram_Traffic_Light1.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.tram_Traffic_Light1.TabIndex = 225;
            // 
            // lineUC3
            // 
            this.lineUC3.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC3.ExtenderWidth = 3;
            this.lineUC3.Fill = false;
            this.lineUC3.GroupID = null;
            this.lineUC3.Location = new System.Drawing.Point(0, 257);
            this.lineUC3.Name = "lineUC3";
            this.lineUC3.Size = new System.Drawing.Size(1888, 3);
            this.lineUC3.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC3.TabIndex = 221;
            // 
            // lineUC2
            // 
            this.lineUC2.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC2.ExtenderWidth = 3;
            this.lineUC2.Fill = false;
            this.lineUC2.GroupID = null;
            this.lineUC2.Location = new System.Drawing.Point(0, 557);
            this.lineUC2.Name = "lineUC2";
            this.lineUC2.Size = new System.Drawing.Size(1887, 3);
            this.lineUC2.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC2.TabIndex = 220;
            // 
            // circleUC3
            // 
            this.circleUC3.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC3.ExtenderWidth = 3;
            this.circleUC3.Fill = false;
            this.circleUC3.GroupID = "SIG-C16PRSY1";
            this.circleUC3.Location = new System.Drawing.Point(49, 535);
            this.circleUC3.Name = "circleUC3";
            this.circleUC3.Size = new System.Drawing.Size(25, 25);
            this.circleUC3.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC3.TabIndex = 224;
            // 
            // lineUC1
            // 
            this.lineUC1.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC1.ExtenderWidth = 3;
            this.lineUC1.Fill = false;
            this.lineUC1.GroupID = "SIG-C16-EFS-001";
            this.lineUC1.Location = new System.Drawing.Point(13, 605);
            this.lineUC1.Name = "lineUC1";
            this.lineUC1.Size = new System.Drawing.Size(43, 3);
            this.lineUC1.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC1.TabIndex = 226;
            // 
            // circleUC13
            // 
            this.circleUC13.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC13.ExtenderWidth = 3;
            this.circleUC13.Fill = false;
            this.circleUC13.GroupID = "SIG-C16-ETS-010";
            this.circleUC13.Location = new System.Drawing.Point(1653, 251);
            this.circleUC13.Name = "circleUC13";
            this.circleUC13.Size = new System.Drawing.Size(15, 15);
            this.circleUC13.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC13.TabIndex = 235;
            // 
            // circleUC17
            // 
            this.circleUC17.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC17.ExtenderWidth = 3;
            this.circleUC17.Fill = false;
            this.circleUC17.GroupID = "SIG-C16-ETS-008";
            this.circleUC17.Location = new System.Drawing.Point(1303, 251);
            this.circleUC17.Name = "circleUC17";
            this.circleUC17.Size = new System.Drawing.Size(15, 15);
            this.circleUC17.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC17.TabIndex = 237;
            // 
            // circleUC7
            // 
            this.circleUC7.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC7.ExtenderWidth = 3;
            this.circleUC7.Fill = false;
            this.circleUC7.GroupID = "SIG-C16-ETS-003";
            this.circleUC7.Location = new System.Drawing.Point(596, 551);
            this.circleUC7.Name = "circleUC7";
            this.circleUC7.Size = new System.Drawing.Size(15, 15);
            this.circleUC7.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC7.TabIndex = 240;
            // 
            // circleUC14
            // 
            this.circleUC14.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC14.ExtenderWidth = 3;
            this.circleUC14.Fill = false;
            this.circleUC14.GroupID = "SIG-C16-ETS-007";
            this.circleUC14.Location = new System.Drawing.Point(1284, 551);
            this.circleUC14.Name = "circleUC14";
            this.circleUC14.Size = new System.Drawing.Size(15, 15);
            this.circleUC14.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC14.TabIndex = 248;
            // 
            // circleUC19
            // 
            this.circleUC19.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC19.ExtenderWidth = 3;
            this.circleUC19.Fill = false;
            this.circleUC19.GroupID = "SIG-C16-ETS-009";
            this.circleUC19.Location = new System.Drawing.Point(1653, 551);
            this.circleUC19.Name = "circleUC19";
            this.circleUC19.Size = new System.Drawing.Size(15, 15);
            this.circleUC19.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC19.TabIndex = 252;
            // 
            // circleUC23
            // 
            this.circleUC23.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC23.ExtenderWidth = 3;
            this.circleUC23.Fill = false;
            this.circleUC23.GroupID = "SIG-C16-ETS-004";
            this.circleUC23.Location = new System.Drawing.Point(638, 251);
            this.circleUC23.Name = "circleUC23";
            this.circleUC23.Size = new System.Drawing.Size(15, 15);
            this.circleUC23.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC23.TabIndex = 264;
            // 
            // circleUC26
            // 
            this.circleUC26.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC26.ExtenderWidth = 3;
            this.circleUC26.Fill = false;
            this.circleUC26.GroupID = "SIG-C16-ETS-002";
            this.circleUC26.Location = new System.Drawing.Point(173, 251);
            this.circleUC26.Name = "circleUC26";
            this.circleUC26.Size = new System.Drawing.Size(15, 15);
            this.circleUC26.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC26.TabIndex = 272;
            // 
            // stringSymbol12
            // 
            this.stringSymbol12.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol12.ExtenderWidth = 3;
            this.stringSymbol12.GroupID = "SIG-C16PRSY2";
            this.stringSymbol12.InnerText = "X2";
            this.stringSymbol12.Location = new System.Drawing.Point(384, 240);
            this.stringSymbol12.Name = "stringSymbol12";
            this.stringSymbol12.Reversion = false;
            this.stringSymbol12.Size = new System.Drawing.Size(15, 15);
            this.stringSymbol12.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol12.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Verticle;
            this.stringSymbol12.TabIndex = 275;
            // 
            // circleUC24
            // 
            this.circleUC24.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC24.ExtenderWidth = 3;
            this.circleUC24.Fill = false;
            this.circleUC24.GroupID = "SIG-C16PRSY2";
            this.circleUC24.Location = new System.Drawing.Point(379, 234);
            this.circleUC24.Name = "circleUC24";
            this.circleUC24.Size = new System.Drawing.Size(25, 25);
            this.circleUC24.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC24.TabIndex = 274;
            // 
            // stringSymbol3
            // 
            this.stringSymbol3.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol3.ExtenderWidth = 3;
            this.stringSymbol3.GroupID = "SIG-C16PRSY3";
            this.stringSymbol3.InnerText = "X3";
            this.stringSymbol3.Location = new System.Drawing.Point(383, 541);
            this.stringSymbol3.Name = "stringSymbol3";
            this.stringSymbol3.Reversion = false;
            this.stringSymbol3.Size = new System.Drawing.Size(15, 15);
            this.stringSymbol3.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol3.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Verticle;
            this.stringSymbol3.TabIndex = 277;
            // 
            // circleUC9
            // 
            this.circleUC9.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC9.ExtenderWidth = 3;
            this.circleUC9.Fill = false;
            this.circleUC9.GroupID = "SIG-C16PRSY3";
            this.circleUC9.Location = new System.Drawing.Point(377, 535);
            this.circleUC9.Name = "circleUC9";
            this.circleUC9.Size = new System.Drawing.Size(25, 25);
            this.circleUC9.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC9.TabIndex = 276;
            // 
            // stringSymbol13
            // 
            this.stringSymbol13.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol13.ExtenderWidth = 3;
            this.stringSymbol13.GroupID = "SIG-C16PRSY4";
            this.stringSymbol13.InnerText = "X4";
            this.stringSymbol13.Location = new System.Drawing.Point(836, 239);
            this.stringSymbol13.Name = "stringSymbol13";
            this.stringSymbol13.Reversion = false;
            this.stringSymbol13.Size = new System.Drawing.Size(15, 15);
            this.stringSymbol13.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol13.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Verticle;
            this.stringSymbol13.TabIndex = 279;
            // 
            // circleUC27
            // 
            this.circleUC27.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC27.ExtenderWidth = 3;
            this.circleUC27.Fill = false;
            this.circleUC27.GroupID = "SIG-C16PRSY4";
            this.circleUC27.Location = new System.Drawing.Point(831, 233);
            this.circleUC27.Name = "circleUC27";
            this.circleUC27.Size = new System.Drawing.Size(25, 25);
            this.circleUC27.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC27.TabIndex = 278;
            // 
            // stringSymbol10
            // 
            this.stringSymbol10.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol10.ExtenderWidth = 3;
            this.stringSymbol10.GroupID = "SIG-C16PRSY5";
            this.stringSymbol10.InnerText = "X5";
            this.stringSymbol10.Location = new System.Drawing.Point(1194, 541);
            this.stringSymbol10.Name = "stringSymbol10";
            this.stringSymbol10.Reversion = false;
            this.stringSymbol10.Size = new System.Drawing.Size(15, 15);
            this.stringSymbol10.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol10.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Verticle;
            this.stringSymbol10.TabIndex = 281;
            // 
            // circleUC21
            // 
            this.circleUC21.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC21.ExtenderWidth = 3;
            this.circleUC21.Fill = false;
            this.circleUC21.GroupID = "SIG-C16PRSY5";
            this.circleUC21.Location = new System.Drawing.Point(1189, 535);
            this.circleUC21.Name = "circleUC21";
            this.circleUC21.Size = new System.Drawing.Size(25, 25);
            this.circleUC21.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC21.TabIndex = 280;
            // 
            // stringSymbol9
            // 
            this.stringSymbol9.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol9.ExtenderWidth = 3;
            this.stringSymbol9.GroupID = "SIG-C16PRSY7";
            this.stringSymbol9.InnerText = "X7";
            this.stringSymbol9.Location = new System.Drawing.Point(1470, 541);
            this.stringSymbol9.Name = "stringSymbol9";
            this.stringSymbol9.Reversion = false;
            this.stringSymbol9.Size = new System.Drawing.Size(15, 15);
            this.stringSymbol9.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol9.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Verticle;
            this.stringSymbol9.TabIndex = 285;
            // 
            // circleUC20
            // 
            this.circleUC20.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC20.ExtenderWidth = 3;
            this.circleUC20.Fill = false;
            this.circleUC20.GroupID = "SIG-C16PRSY7";
            this.circleUC20.Location = new System.Drawing.Point(1465, 535);
            this.circleUC20.Name = "circleUC20";
            this.circleUC20.Size = new System.Drawing.Size(25, 25);
            this.circleUC20.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC20.TabIndex = 284;
            // 
            // stringSymbol4
            // 
            this.stringSymbol4.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol4.ExtenderWidth = 3;
            this.stringSymbol4.GroupID = "SIG-C16PRSY8";
            this.stringSymbol4.InnerText = "X8";
            this.stringSymbol4.Location = new System.Drawing.Point(1862, 240);
            this.stringSymbol4.Name = "stringSymbol4";
            this.stringSymbol4.Reversion = false;
            this.stringSymbol4.Size = new System.Drawing.Size(15, 15);
            this.stringSymbol4.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol4.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Verticle;
            this.stringSymbol4.TabIndex = 287;
            // 
            // circleUC11
            // 
            this.circleUC11.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC11.ExtenderWidth = 3;
            this.circleUC11.Fill = false;
            this.circleUC11.GroupID = "SIG-C16PRSY8";
            this.circleUC11.Location = new System.Drawing.Point(1857, 234);
            this.circleUC11.Name = "circleUC11";
            this.circleUC11.Size = new System.Drawing.Size(25, 25);
            this.circleUC11.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC11.TabIndex = 286;
            // 
            // circleUC5
            // 
            this.circleUC5.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC5.ExtenderWidth = 3;
            this.circleUC5.Fill = false;
            this.circleUC5.GroupID = "SIG-C16-ETS-001";
            this.circleUC5.Location = new System.Drawing.Point(145, 551);
            this.circleUC5.Name = "circleUC5";
            this.circleUC5.Size = new System.Drawing.Size(15, 15);
            this.circleUC5.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC5.TabIndex = 231;
            // 
            // lineUC11
            // 
            this.lineUC11.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC11.ExtenderWidth = 3;
            this.lineUC11.Fill = false;
            this.lineUC11.GroupID = "SIG-C15-EFS-001";
            this.lineUC11.Location = new System.Drawing.Point(332, 257);
            this.lineUC11.Name = "lineUC11";
            this.lineUC11.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC11.Size = new System.Drawing.Size(3, 46);
            this.lineUC11.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC11.TabIndex = 288;
            // 
            // lineUC12
            // 
            this.lineUC12.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC12.ExtenderWidth = 3;
            this.lineUC12.Fill = false;
            this.lineUC12.GroupID = "SIG-C15-EFS-001";
            this.lineUC12.Location = new System.Drawing.Point(443, 257);
            this.lineUC12.Name = "lineUC12";
            this.lineUC12.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC12.Size = new System.Drawing.Size(3, 46);
            this.lineUC12.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC12.TabIndex = 289;
            // 
            // lineUC35
            // 
            this.lineUC35.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC35.ExtenderWidth = 3;
            this.lineUC35.Fill = false;
            this.lineUC35.GroupID = "SIG-C15-EFS-001";
            this.lineUC35.Location = new System.Drawing.Point(1427, 505);
            this.lineUC35.Name = "lineUC35";
            this.lineUC35.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC35.Size = new System.Drawing.Size(3, 46);
            this.lineUC35.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC35.TabIndex = 316;
            // 
            // lineUC36
            // 
            this.lineUC36.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC36.ExtenderWidth = 3;
            this.lineUC36.Fill = false;
            this.lineUC36.GroupID = "SIG-C15-EFS-001";
            this.lineUC36.Location = new System.Drawing.Point(1427, 424);
            this.lineUC36.Name = "lineUC36";
            this.lineUC36.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC36.Size = new System.Drawing.Size(3, 46);
            this.lineUC36.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC36.TabIndex = 315;
            // 
            // lineUC37
            // 
            this.lineUC37.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC37.ExtenderWidth = 3;
            this.lineUC37.Fill = false;
            this.lineUC37.GroupID = "SIG-C15-EFS-001";
            this.lineUC37.Location = new System.Drawing.Point(1427, 342);
            this.lineUC37.Name = "lineUC37";
            this.lineUC37.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC37.Size = new System.Drawing.Size(3, 46);
            this.lineUC37.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC37.TabIndex = 314;
            // 
            // lineUC38
            // 
            this.lineUC38.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC38.ExtenderWidth = 3;
            this.lineUC38.Fill = false;
            this.lineUC38.GroupID = "SIG-C15-EFS-001";
            this.lineUC38.Location = new System.Drawing.Point(1427, 257);
            this.lineUC38.Name = "lineUC38";
            this.lineUC38.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC38.Size = new System.Drawing.Size(3, 46);
            this.lineUC38.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC38.TabIndex = 313;
            // 
            // lineUC31
            // 
            this.lineUC31.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC31.ExtenderWidth = 3;
            this.lineUC31.Fill = false;
            this.lineUC31.GroupID = "SIG-C15-EFS-001";
            this.lineUC31.Location = new System.Drawing.Point(1523, 505);
            this.lineUC31.Name = "lineUC31";
            this.lineUC31.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC31.Size = new System.Drawing.Size(3, 46);
            this.lineUC31.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC31.TabIndex = 312;
            // 
            // lineUC32
            // 
            this.lineUC32.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC32.ExtenderWidth = 3;
            this.lineUC32.Fill = false;
            this.lineUC32.GroupID = "SIG-C15-EFS-001";
            this.lineUC32.Location = new System.Drawing.Point(1523, 424);
            this.lineUC32.Name = "lineUC32";
            this.lineUC32.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC32.Size = new System.Drawing.Size(3, 46);
            this.lineUC32.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC32.TabIndex = 311;
            // 
            // lineUC33
            // 
            this.lineUC33.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC33.ExtenderWidth = 3;
            this.lineUC33.Fill = false;
            this.lineUC33.GroupID = "SIG-C15-EFS-001";
            this.lineUC33.Location = new System.Drawing.Point(1523, 342);
            this.lineUC33.Name = "lineUC33";
            this.lineUC33.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC33.Size = new System.Drawing.Size(3, 46);
            this.lineUC33.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC33.TabIndex = 310;
            // 
            // lineUC34
            // 
            this.lineUC34.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC34.ExtenderWidth = 3;
            this.lineUC34.Fill = false;
            this.lineUC34.GroupID = "SIG-C15-EFS-001";
            this.lineUC34.Location = new System.Drawing.Point(1523, 257);
            this.lineUC34.Name = "lineUC34";
            this.lineUC34.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC34.Size = new System.Drawing.Size(3, 46);
            this.lineUC34.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC34.TabIndex = 309;
            // 
            // stringSymbol14
            // 
            this.stringSymbol14.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol14.ExtenderWidth = 3;
            this.stringSymbol14.GroupID = "";
            this.stringSymbol14.InnerText = "4";
            this.stringSymbol14.Location = new System.Drawing.Point(1427, 366);
            this.stringSymbol14.Name = "stringSymbol14";
            this.stringSymbol14.Reversion = false;
            this.stringSymbol14.Size = new System.Drawing.Size(96, 83);
            this.stringSymbol14.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol14.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Verticle;
            this.stringSymbol14.TabIndex = 317;
            // 
            // stringSymbol37
            // 
            this.stringSymbol37.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol37.ExtenderWidth = 3;
            this.stringSymbol37.GroupID = "";
            this.stringSymbol37.InnerText = "P.K. 9+284.77";
            this.stringSymbol37.Location = new System.Drawing.Point(848, 716);
            this.stringSymbol37.Name = "stringSymbol37";
            this.stringSymbol37.Reversion = true;
            this.stringSymbol37.Size = new System.Drawing.Size(16, 93);
            this.stringSymbol37.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol37.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.stringSymbol37.TabIndex = 353;
            // 
            // stringSymbol64
            // 
            this.stringSymbol64.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol64.ExtenderWidth = 3;
            this.stringSymbol64.GroupID = "SIG-C16-ELA-004";
            this.stringSymbol64.InnerText = "SIG-C16-ELA-004";
            this.stringSymbol64.Location = new System.Drawing.Point(892, 233);
            this.stringSymbol64.Name = "stringSymbol64";
            this.stringSymbol64.Reversion = false;
            this.stringSymbol64.Size = new System.Drawing.Size(99, 18);
            this.stringSymbol64.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol64.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Verticle;
            this.stringSymbol64.TabIndex = 381;
            // 
            // stringSymbol65
            // 
            this.stringSymbol65.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol65.ExtenderWidth = 3;
            this.stringSymbol65.GroupID = "SIG-C16-ELA-002";
            this.stringSymbol65.InnerText = "SIG-C16-ELA-002";
            this.stringSymbol65.Location = new System.Drawing.Point(689, 233);
            this.stringSymbol65.Name = "stringSymbol65";
            this.stringSymbol65.Reversion = false;
            this.stringSymbol65.Size = new System.Drawing.Size(99, 18);
            this.stringSymbol65.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol65.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Verticle;
            this.stringSymbol65.TabIndex = 382;
            // 
            // stringSymbol69
            // 
            this.stringSymbol69.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol69.ExtenderWidth = 3;
            this.stringSymbol69.GroupID = "";
            this.stringSymbol69.InnerText = "P.K. 09+266.85";
            this.stringSymbol69.Location = new System.Drawing.Point(536, 31);
            this.stringSymbol69.Name = "stringSymbol69";
            this.stringSymbol69.Reversion = true;
            this.stringSymbol69.Size = new System.Drawing.Size(16, 93);
            this.stringSymbol69.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol69.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.stringSymbol69.TabIndex = 386;
            // 
            // stringSymbol5
            // 
            this.stringSymbol5.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol5.ExtenderWidth = 3;
            this.stringSymbol5.GroupID = "SIG-C16PRSY6";
            this.stringSymbol5.InnerText = "X6";
            this.stringSymbol5.Location = new System.Drawing.Point(1471, 239);
            this.stringSymbol5.Name = "stringSymbol5";
            this.stringSymbol5.Reversion = false;
            this.stringSymbol5.Size = new System.Drawing.Size(15, 15);
            this.stringSymbol5.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol5.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Verticle;
            this.stringSymbol5.TabIndex = 283;
            // 
            // circleUC8
            // 
            this.circleUC8.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC8.ExtenderWidth = 3;
            this.circleUC8.Fill = false;
            this.circleUC8.GroupID = "SIG-C16PRSY6";
            this.circleUC8.Location = new System.Drawing.Point(1466, 233);
            this.circleUC8.Name = "circleUC8";
            this.circleUC8.Size = new System.Drawing.Size(25, 25);
            this.circleUC8.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC8.TabIndex = 282;
            // 
            // stringSymbol85
            // 
            this.stringSymbol85.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol85.ExtenderWidth = 3;
            this.stringSymbol85.GroupID = "SIG-C16-ETS-006";
            this.stringSymbol85.InnerText = "SIG-C16-ETS-006";
            this.stringSymbol85.Location = new System.Drawing.Point(1033, 233);
            this.stringSymbol85.Name = "stringSymbol85";
            this.stringSymbol85.Reversion = false;
            this.stringSymbol85.Size = new System.Drawing.Size(99, 18);
            this.stringSymbol85.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol85.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Verticle;
            this.stringSymbol85.TabIndex = 415;
            // 
            // circleUC1
            // 
            this.circleUC1.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC1.ExtenderWidth = 3;
            this.circleUC1.GroupID = "SIG-C16-ETS-006";
            this.circleUC1.Location = new System.Drawing.Point(1058, 251);
            this.circleUC1.Name = "circleUC1";
            this.circleUC1.Size = new System.Drawing.Size(15, 15);
            this.circleUC1.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC1.TabIndex = 414;
            // 
            // circleUC2
            // 
            this.circleUC2.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC2.ExtenderWidth = 3;
            this.circleUC2.Fill = false;
            this.circleUC2.GroupID = "SIG-C16-ETS-006";
            this.circleUC2.Location = new System.Drawing.Point(1073, 251);
            this.circleUC2.Name = "circleUC2";
            this.circleUC2.Size = new System.Drawing.Size(15, 15);
            this.circleUC2.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC2.TabIndex = 413;
            // 
            // stringSymbol26
            // 
            this.stringSymbol26.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol26.ExtenderWidth = 3;
            this.stringSymbol26.GroupID = "";
            this.stringSymbol26.InnerText = "P.K. 09+254.27";
            this.stringSymbol26.Location = new System.Drawing.Point(529, 633);
            this.stringSymbol26.Name = "stringSymbol26";
            this.stringSymbol26.Reversion = true;
            this.stringSymbol26.Size = new System.Drawing.Size(15, 102);
            this.stringSymbol26.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol26.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.stringSymbol26.TabIndex = 341;
            // 
            // speed_Limit_Sign1
            // 
            this.speed_Limit_Sign1.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.speed_Limit_Sign1.ExtenderWidth = 3;
            this.speed_Limit_Sign1.GroupID = "SIG-C16-EFS-008";
            this.speed_Limit_Sign1.Location = new System.Drawing.Point(1675, 198);
            this.speed_Limit_Sign1.Name = "speed_Limit_Sign1";
            this.speed_Limit_Sign1.Reversion = false;
            this.speed_Limit_Sign1.Size = new System.Drawing.Size(91, 44);
            this.speed_Limit_Sign1.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.speed_Limit_Sign1.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.speed_Limit_Sign1.TabIndex = 416;
            // 
            // speed_Limit_Sign2
            // 
            this.speed_Limit_Sign2.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.speed_Limit_Sign2.ExtenderWidth = 3;
            this.speed_Limit_Sign2.GroupID = "SIG-C16-EFS-007";
            this.speed_Limit_Sign2.Location = new System.Drawing.Point(1733, 583);
            this.speed_Limit_Sign2.Name = "speed_Limit_Sign2";
            this.speed_Limit_Sign2.Reversion = true;
            this.speed_Limit_Sign2.Size = new System.Drawing.Size(91, 44);
            this.speed_Limit_Sign2.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.speed_Limit_Sign2.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.speed_Limit_Sign2.TabIndex = 417;
            // 
            // stringSymbol42
            // 
            this.stringSymbol42.dashstyle = iSCADA.Design.Utilities.SIG.DashStyle.Solid;
            this.stringSymbol42.ExtenderWidth = 3;
            this.stringSymbol42.GroupID = "";
            this.stringSymbol42.InnerText = "P.K. 09+651.67";
            this.stringSymbol42.Location = new System.Drawing.Point(1322, 634);
            this.stringSymbol42.Name = "stringSymbol42";
            this.stringSymbol42.Reversion = true;
            this.stringSymbol42.Size = new System.Drawing.Size(16, 93);
            this.stringSymbol42.Style = iSCADA.Design.Utilities.SIG.SymbolStyle.Default;
            this.stringSymbol42.SymbolOrientation = iSCADA.Design.Utilities.SIG.SymbolOrientation.Horizontal;
            this.stringSymbol42.TabIndex = 359;
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1884, 832);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Status";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // Form_SignalC16
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(1894, 861);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form_SignalC16";
            this.Text = "Form_SignalC16";
            this.Load += new System.EventHandler(this.Form_SignalC16_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol77;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol76;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol75;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol73;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol72;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol71;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol70;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol68;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol67;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol66;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol63;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol62;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol61;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol60;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol59;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol58;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol57;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol56;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol55;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol54;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol53;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol52;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol51;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol49;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol48;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol47;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol46;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol45;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol44;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol43;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol42;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol41;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol40;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol39;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol38;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC6;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol36;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol35;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC5;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol34;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol33;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol32;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol31;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol30;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol29;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol28;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol27;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol26;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol25;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol24;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol23;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol22;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol21;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol20;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol19;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol18;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol17;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC48;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC49;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC50;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC45;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC46;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC47;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC42;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC43;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC44;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC39;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC40;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC41;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol8;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC28;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC29;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC30;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC25;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC26;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC27;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC22;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC23;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC24;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC19;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC20;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC21;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC16;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC17;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC18;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC15;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC14;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC13;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC18;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC22;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC6;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC25;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC10;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol5;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC8;
        private iSCADA.Design.Utilities.SIG.Tram_Traffic_Light tram_Traffic_Light8;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol11;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC4;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC9;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC10;
        private iSCADA.Design.Utilities.SIG.Tram_Traffic_Light tram_Traffic_Light7;
        private iSCADA.Design.Utilities.SIG.Route_Point route_Point7;
        private iSCADA.Design.Utilities.SIG.Route_Point route_Point8;
        private iSCADA.Design.Utilities.SIG.Route_Point route_Point5;
        private iSCADA.Design.Utilities.SIG.Route_Point route_Point6;
        private iSCADA.Design.Utilities.SIG.Tram_Traffic_Light tram_Traffic_Light6;
        private iSCADA.Design.Utilities.SIG.Tram_Traffic_Light tram_Traffic_Light5;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol6;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC3;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC7;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC8;
        private iSCADA.Design.Utilities.SIG.Tram_Traffic_Light tram_Traffic_Light4;
        private iSCADA.Design.Utilities.SIG.Tram_Traffic_Light tram_Traffic_Light3;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol7;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC2;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC5;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC6;
        private iSCADA.Design.Utilities.SIG.Route_Point route_Point2;
        private iSCADA.Design.Utilities.SIG.Route_Point route_Point4;
        private iSCADA.Design.Utilities.SIG.Tram_Traffic_Light tram_Traffic_Light2;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC16;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC12;
        private iSCADA.Design.Utilities.SIG.Route_Point route_Point1;
        private iSCADA.Design.Utilities.SIG.Route_Point route_Point3;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC4;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol2;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol1;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC1;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC4;
        private iSCADA.Design.Utilities.SIG.Tram_Traffic_Light tram_Traffic_Light1;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC3;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC2;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC3;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC1;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC13;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC17;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC7;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC14;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC19;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC23;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC26;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol12;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC24;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol3;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC9;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol13;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC27;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol10;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC21;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol9;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC20;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol4;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC11;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC5;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC11;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC12;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC35;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC36;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC37;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC38;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC31;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC32;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC33;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC34;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol14;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol37;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol50;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol64;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol65;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol69;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol74;
        private System.Windows.Forms.TabPage tabPage2;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol78;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol15;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol16;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC7;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC51;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC52;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol84;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol82;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol83;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol81;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol79;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol80;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol85;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC1;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC2;
        private iSCADA.Design.Utilities.SIG.Speed_Limit_Sign speed_Limit_Sign1;
        private iSCADA.Design.Utilities.SIG.Speed_Limit_Sign speed_Limit_Sign2;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol86;
        private iSCADA.Design.Utilities.SIG.StringSymbol stringSymbol87;
    }
}