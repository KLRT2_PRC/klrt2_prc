﻿namespace PRC
{
    partial class Form_GroupProg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.cmbGroup = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.dgv_Permission = new System.Windows.Forms.DataGridView();
            this.ProgID_En = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProgID_Cht = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DisplayFun = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.SearchFun = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.InsertFun = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.DeleteFun = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.UpdateFun = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn4 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn5 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Permission)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbGroup
            // 
            this.cmbGroup.BackColor = System.Drawing.SystemColors.ControlDark;
            this.cmbGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGroup.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.cmbGroup.FormattingEnabled = true;
            this.cmbGroup.Location = new System.Drawing.Point(217, 109);
            this.cmbGroup.Name = "cmbGroup";
            this.cmbGroup.Size = new System.Drawing.Size(243, 35);
            this.cmbGroup.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.ForeColor = System.Drawing.SystemColors.Info;
            this.label1.Location = new System.Drawing.Point(212, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 27);
            this.label1.TabIndex = 3;
            this.label1.Text = "群組名稱：";
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Transparent;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.button3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button3.Location = new System.Drawing.Point(720, 717);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(277, 55);
            this.button3.TabIndex = 6;
            this.button3.Text = "確認變更";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.EnabledChanged += new System.EventHandler(this.button3_EnabledChanged);
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Checked = true;
            this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox1.Font = new System.Drawing.Font("新細明體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.checkBox1.ForeColor = System.Drawing.SystemColors.Control;
            this.checkBox1.Location = new System.Drawing.Point(498, 115);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(113, 25);
            this.checkBox1.TabIndex = 10;
            this.checkBox1.Text = "顯示所有";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // dgv_Permission
            // 
            this.dgv_Permission.AllowUserToAddRows = false;
            this.dgv_Permission.AllowUserToDeleteRows = false;
            this.dgv_Permission.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_Permission.BackgroundColor = System.Drawing.Color.White;
            this.dgv_Permission.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("微軟正黑體", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.Menu;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_Permission.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_Permission.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_Permission.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ProgID_En,
            this.ProgID_Cht,
            this.DisplayFun,
            this.SearchFun,
            this.InsertFun,
            this.DeleteFun,
            this.UpdateFun});
            this.dgv_Permission.EnableHeadersVisualStyles = false;
            this.dgv_Permission.Location = new System.Drawing.Point(217, 168);
            this.dgv_Permission.MultiSelect = false;
            this.dgv_Permission.Name = "dgv_Permission";
            this.dgv_Permission.RowTemplate.Height = 36;
            this.dgv_Permission.Size = new System.Drawing.Size(1353, 534);
            this.dgv_Permission.TabIndex = 11;
            // 
            // ProgID_En
            // 
            this.ProgID_En.DataPropertyName = "ProgID_En";
            this.ProgID_En.HeaderText = "表單名稱";
            this.ProgID_En.Name = "ProgID_En";
            this.ProgID_En.Visible = false;
            // 
            // ProgID_Cht
            // 
            this.ProgID_Cht.DataPropertyName = "ProgID_Cht";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.ProgID_Cht.DefaultCellStyle = dataGridViewCellStyle2;
            this.ProgID_Cht.HeaderText = "中文名稱";
            this.ProgID_Cht.Name = "ProgID_Cht";
            this.ProgID_Cht.ReadOnly = true;
            // 
            // DisplayFun
            // 
            this.DisplayFun.DataPropertyName = "DisplayFun";
            this.DisplayFun.FillWeight = 20F;
            this.DisplayFun.HeaderText = "讀取權限";
            this.DisplayFun.Name = "DisplayFun";
            this.DisplayFun.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // SearchFun
            // 
            this.SearchFun.DataPropertyName = "SearchFun";
            this.SearchFun.FillWeight = 20F;
            this.SearchFun.HeaderText = "搜尋權限";
            this.SearchFun.Name = "SearchFun";
            // 
            // InsertFun
            // 
            this.InsertFun.DataPropertyName = "InsertFun";
            this.InsertFun.FillWeight = 20F;
            this.InsertFun.HeaderText = "新增權限";
            this.InsertFun.Name = "InsertFun";
            // 
            // DeleteFun
            // 
            this.DeleteFun.DataPropertyName = "DeleteFun";
            this.DeleteFun.FillWeight = 20F;
            this.DeleteFun.HeaderText = "刪除權限";
            this.DeleteFun.Name = "DeleteFun";
            // 
            // UpdateFun
            // 
            this.UpdateFun.DataPropertyName = "UpdateFun";
            this.UpdateFun.FillWeight = 20F;
            this.UpdateFun.HeaderText = "更新權限";
            this.UpdateFun.Name = "UpdateFun";
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.DataPropertyName = "SearchFun";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            dataGridViewCellStyle3.NullValue = false;
            this.dataGridViewCheckBoxColumn1.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewCheckBoxColumn1.HeaderText = "搜尋權限";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCheckBoxColumn1.Width = 183;
            // 
            // dataGridViewCheckBoxColumn2
            // 
            this.dataGridViewCheckBoxColumn2.DataPropertyName = "InsertFun";
            this.dataGridViewCheckBoxColumn2.HeaderText = "新增權限";
            this.dataGridViewCheckBoxColumn2.Name = "dataGridViewCheckBoxColumn2";
            this.dataGridViewCheckBoxColumn2.Width = 182;
            // 
            // dataGridViewCheckBoxColumn3
            // 
            this.dataGridViewCheckBoxColumn3.DataPropertyName = "DeleteFun";
            this.dataGridViewCheckBoxColumn3.HeaderText = "刪除權限";
            this.dataGridViewCheckBoxColumn3.Name = "dataGridViewCheckBoxColumn3";
            this.dataGridViewCheckBoxColumn3.Width = 183;
            // 
            // dataGridViewCheckBoxColumn4
            // 
            this.dataGridViewCheckBoxColumn4.DataPropertyName = "UpdateFun";
            this.dataGridViewCheckBoxColumn4.HeaderText = "更新權限";
            this.dataGridViewCheckBoxColumn4.Name = "dataGridViewCheckBoxColumn4";
            this.dataGridViewCheckBoxColumn4.Width = 182;
            // 
            // dataGridViewCheckBoxColumn5
            // 
            this.dataGridViewCheckBoxColumn5.DataPropertyName = "UpdateFun";
            this.dataGridViewCheckBoxColumn5.HeaderText = "更新權限";
            this.dataGridViewCheckBoxColumn5.Name = "dataGridViewCheckBoxColumn5";
            this.dataGridViewCheckBoxColumn5.Width = 182;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "ProgID_En";
            this.dataGridViewTextBoxColumn1.HeaderText = "表單名稱";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 160;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "ProgID_Cht";
            this.dataGridViewTextBoxColumn2.HeaderText = "中文名稱";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 159;
            // 
            // Form_GroupProg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(1910, 800);
            this.Controls.Add(this.dgv_Permission);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbGroup);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form_GroupProg";
            this.Tag = "群組程式管理";
            this.Text = "Form_GroupProg";
            this.Load += new System.EventHandler(this.Form_GroupProg_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Permission)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox cmbGroup;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.DataGridView dgv_Permission;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn4;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProgID_En;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProgID_Cht;
        private System.Windows.Forms.DataGridViewCheckBoxColumn DisplayFun;
        private System.Windows.Forms.DataGridViewCheckBoxColumn SearchFun;
        private System.Windows.Forms.DataGridViewCheckBoxColumn InsertFun;
        private System.Windows.Forms.DataGridViewCheckBoxColumn DeleteFun;
        private System.Windows.Forms.DataGridViewCheckBoxColumn UpdateFun;
    }
}