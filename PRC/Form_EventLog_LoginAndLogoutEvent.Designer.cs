﻿namespace PRC
{
    partial class Form_EventLog_LoginAndLogoutEvent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cmb_DateRangeTyoe = new System.Windows.Forms.ComboBox();
            this.txt_UserID = new System.Windows.Forms.TextBox();
            this.btn_SearchEventLogOfLogin = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dtp_StartDate = new System.Windows.Forms.DateTimePicker();
            this.dtp_EndDate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.col_UserID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_LoginInTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_LoginOutTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.cmb_DateRangeTyoe);
            this.groupBox2.Controls.Add(this.txt_UserID);
            this.groupBox2.Controls.Add(this.btn_SearchEventLogOfLogin);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.dtp_StartDate);
            this.groupBox2.Controls.Add(this.dtp_EndDate);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Font = new System.Drawing.Font("微軟正黑體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.groupBox2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1910, 111);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "鍵值區";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label3.Location = new System.Drawing.Point(21, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(128, 24);
            this.label3.TabIndex = 17;
            this.label3.Text = "時間範圍依據:";
            // 
            // cmb_DateRangeTyoe
            // 
            this.cmb_DateRangeTyoe.Font = new System.Drawing.Font("新細明體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.cmb_DateRangeTyoe.FormattingEnabled = true;
            this.cmb_DateRangeTyoe.Location = new System.Drawing.Point(155, 72);
            this.cmb_DateRangeTyoe.Name = "cmb_DateRangeTyoe";
            this.cmb_DateRangeTyoe.Size = new System.Drawing.Size(242, 29);
            this.cmb_DateRangeTyoe.TabIndex = 16;
            // 
            // txt_UserID
            // 
            this.txt_UserID.Location = new System.Drawing.Point(495, 68);
            this.txt_UserID.Name = "txt_UserID";
            this.txt_UserID.Size = new System.Drawing.Size(284, 33);
            this.txt_UserID.TabIndex = 11;
            // 
            // btn_SearchEventLogOfLogin
            // 
            this.btn_SearchEventLogOfLogin.BackColor = System.Drawing.Color.Transparent;
            this.btn_SearchEventLogOfLogin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_SearchEventLogOfLogin.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btn_SearchEventLogOfLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_SearchEventLogOfLogin.Font = new System.Drawing.Font("微軟正黑體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btn_SearchEventLogOfLogin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btn_SearchEventLogOfLogin.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_SearchEventLogOfLogin.Location = new System.Drawing.Point(808, 64);
            this.btn_SearchEventLogOfLogin.Name = "btn_SearchEventLogOfLogin";
            this.btn_SearchEventLogOfLogin.Size = new System.Drawing.Size(88, 37);
            this.btn_SearchEventLogOfLogin.TabIndex = 3;
            this.btn_SearchEventLogOfLogin.Text = "搜尋";
            this.btn_SearchEventLogOfLogin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_SearchEventLogOfLogin.UseVisualStyleBackColor = false;
            this.btn_SearchEventLogOfLogin.EnabledChanged += new System.EventHandler(this.btn_SearchEventLogOfLogin_EnabledChanged);
            this.btn_SearchEventLogOfLogin.Click += new System.EventHandler(this.btn_SearchEventLogOfLogin_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label1.Location = new System.Drawing.Point(21, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 24);
            this.label1.TabIndex = 6;
            this.label1.Text = "起始日期";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label5.Location = new System.Drawing.Point(403, 71);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 24);
            this.label5.TabIndex = 10;
            this.label5.Text = "使用者ID";
            // 
            // dtp_StartDate
            // 
            this.dtp_StartDate.CalendarForeColor = System.Drawing.Color.Yellow;
            this.dtp_StartDate.CalendarMonthBackground = System.Drawing.Color.Black;
            this.dtp_StartDate.CalendarTitleBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.dtp_StartDate.CalendarTitleForeColor = System.Drawing.Color.Cyan;
            this.dtp_StartDate.CalendarTrailingForeColor = System.Drawing.Color.Cyan;
            this.dtp_StartDate.Location = new System.Drawing.Point(113, 29);
            this.dtp_StartDate.Name = "dtp_StartDate";
            this.dtp_StartDate.Size = new System.Drawing.Size(284, 33);
            this.dtp_StartDate.TabIndex = 0;
            // 
            // dtp_EndDate
            // 
            this.dtp_EndDate.Location = new System.Drawing.Point(495, 26);
            this.dtp_EndDate.Name = "dtp_EndDate";
            this.dtp_EndDate.Size = new System.Drawing.Size(284, 33);
            this.dtp_EndDate.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label2.Location = new System.Drawing.Point(403, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 24);
            this.label2.TabIndex = 7;
            this.label2.Text = "結束日期";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_UserID,
            this.col_LoginInTime,
            this.col_LoginOutTime});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 111);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(5);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(1910, 789);
            this.dataGridView1.TabIndex = 15;
            // 
            // col_UserID
            // 
            this.col_UserID.DataPropertyName = "UserID";
            this.col_UserID.HeaderText = "使用者編號";
            this.col_UserID.Name = "col_UserID";
            // 
            // col_LoginInTime
            // 
            this.col_LoginInTime.DataPropertyName = "LoginInTime";
            this.col_LoginInTime.HeaderText = "登入日期";
            this.col_LoginInTime.Name = "col_LoginInTime";
            // 
            // col_LoginOutTime
            // 
            this.col_LoginOutTime.DataPropertyName = "LoginOutTime";
            this.col_LoginOutTime.HeaderText = "登出日期";
            this.col_LoginOutTime.Name = "col_LoginOutTime";
            // 
            // Form_EventLog_LoginAndLogoutEvent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1910, 900);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.groupBox2);
            this.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "Form_EventLog_LoginAndLogoutEvent";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Form_EventLog_LoginAndLogoutEvent";
            this.Load += new System.EventHandler(this.Form_EventLog_LoginAndLogoutEvent_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txt_UserID;
        private System.Windows.Forms.Button btn_SearchEventLogOfLogin;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dtp_StartDate;
        private System.Windows.Forms.DateTimePicker dtp_EndDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_UserID;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_LoginInTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_LoginOutTime;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmb_DateRangeTyoe;
    }
}