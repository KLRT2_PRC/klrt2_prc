﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PRC
{
    public partial class Form_About : Form
    {
        public Form_About()
        {
            InitializeComponent();
            Assembly _assembly = Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(_assembly.Location);
            label1.Text = "版本資訊："+fvi.FileVersion;
        }
    }
}
