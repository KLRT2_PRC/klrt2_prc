﻿namespace PRC
{
    partial class Form_GroupMgt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.listGroup = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.listAllMember = new System.Windows.Forms.ListView();
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnGroupC = new System.Windows.Forms.Button();
            this.cmbGroupPrivilege = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txbGroupID = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnGroupD = new System.Windows.Forms.Button();
            this.btnGroupU = new System.Windows.Forms.Button();
            this.btnGroupI = new System.Windows.Forms.Button();
            this.txbDesc = new System.Windows.Forms.TextBox();
            this.txbGroupCht = new System.Windows.Forms.TextBox();
            this.txbGroupEn = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.gridGroup = new System.Windows.Forms.DataGridView();
            this.GroupID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GroupName_En = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GroupName_Cht = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GroupPrivilege = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmbGroup = new System.Windows.Forms.ComboBox();
            this.btnGroupMemberI = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridGroup)).BeginInit();
            this.SuspendLayout();
            // 
            // listGroup
            // 
            this.listGroup.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader7,
            this.columnHeader6,
            this.columnHeader2});
            this.listGroup.Font = new System.Drawing.Font("新細明體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.listGroup.FullRowSelect = true;
            this.listGroup.GridLines = true;
            this.listGroup.Location = new System.Drawing.Point(851, 141);
            this.listGroup.Name = "listGroup";
            this.listGroup.Size = new System.Drawing.Size(450, 500);
            this.listGroup.TabIndex = 0;
            this.listGroup.UseCompatibleStateImageBehavior = false;
            this.listGroup.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "群組名稱";
            this.columnHeader1.Width = 120;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "部門";
            this.columnHeader7.Width = 100;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "職工編號";
            this.columnHeader6.Width = 110;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "姓名";
            this.columnHeader2.Width = 120;
            // 
            // listAllMember
            // 
            this.listAllMember.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5});
            this.listAllMember.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.listAllMember.FullRowSelect = true;
            this.listAllMember.GridLines = true;
            this.listAllMember.Location = new System.Drawing.Point(1427, 141);
            this.listAllMember.Name = "listAllMember";
            this.listAllMember.Size = new System.Drawing.Size(400, 500);
            this.listAllMember.TabIndex = 1;
            this.listAllMember.UseCompatibleStateImageBehavior = false;
            this.listAllMember.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "部門";
            this.columnHeader3.Width = 100;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "職工編號";
            this.columnHeader4.Width = 120;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "姓名";
            this.columnHeader5.Width = 150;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.button1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button1.Location = new System.Drawing.Point(1311, 299);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(110, 36);
            this.button1.TabIndex = 2;
            this.button1.Text = ">>>";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.EnabledChanged += new System.EventHandler(this.button_EnabledChanged);
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Transparent;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.button2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button2.Location = new System.Drawing.Point(1311, 388);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(110, 36);
            this.button2.TabIndex = 3;
            this.button2.Text = "<<<";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.EnabledChanged += new System.EventHandler(this.button_EnabledChanged);
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnGroupC);
            this.groupBox1.Controls.Add(this.cmbGroupPrivilege);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txbGroupID);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.btnGroupD);
            this.groupBox1.Controls.Add(this.btnGroupU);
            this.groupBox1.Controls.Add(this.btnGroupI);
            this.groupBox1.Controls.Add(this.txbDesc);
            this.groupBox1.Controls.Add(this.txbGroupCht);
            this.groupBox1.Controls.Add(this.txbGroupEn);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.gridGroup);
            this.groupBox1.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.groupBox1.Location = new System.Drawing.Point(44, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(649, 776);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            // 
            // btnGroupC
            // 
            this.btnGroupC.BackColor = System.Drawing.Color.Transparent;
            this.btnGroupC.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGroupC.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnGroupC.Location = new System.Drawing.Point(481, 717);
            this.btnGroupC.Name = "btnGroupC";
            this.btnGroupC.Size = new System.Drawing.Size(90, 38);
            this.btnGroupC.TabIndex = 13;
            this.btnGroupC.Text = "取消";
            this.btnGroupC.UseVisualStyleBackColor = false;
            this.btnGroupC.EnabledChanged += new System.EventHandler(this.button_EnabledChanged);
            this.btnGroupC.Click += new System.EventHandler(this.btnGroupC_Click);
            // 
            // cmbGroupPrivilege
            // 
            this.cmbGroupPrivilege.BackColor = System.Drawing.SystemColors.ControlDark;
            this.cmbGroupPrivilege.FormattingEnabled = true;
            this.cmbGroupPrivilege.Location = new System.Drawing.Point(266, 499);
            this.cmbGroupPrivilege.Name = "cmbGroupPrivilege";
            this.cmbGroupPrivilege.Size = new System.Drawing.Size(275, 35);
            this.cmbGroupPrivilege.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.SystemColors.Control;
            this.label5.Location = new System.Drawing.Point(77, 502);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(117, 27);
            this.label5.TabIndex = 12;
            this.label5.Text = "是否啟用：";
            // 
            // txbGroupID
            // 
            this.txbGroupID.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txbGroupID.Location = new System.Drawing.Point(266, 333);
            this.txbGroupID.Name = "txbGroupID";
            this.txbGroupID.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txbGroupID.Size = new System.Drawing.Size(275, 35);
            this.txbGroupID.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.Control;
            this.label4.Location = new System.Drawing.Point(77, 336);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label4.Size = new System.Drawing.Size(97, 27);
            this.label4.TabIndex = 10;
            this.label4.Text = "群組ID：";
            // 
            // btnGroupD
            // 
            this.btnGroupD.BackColor = System.Drawing.Color.Transparent;
            this.btnGroupD.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGroupD.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnGroupD.Location = new System.Drawing.Point(351, 717);
            this.btnGroupD.Name = "btnGroupD";
            this.btnGroupD.Size = new System.Drawing.Size(90, 38);
            this.btnGroupD.TabIndex = 9;
            this.btnGroupD.Text = "刪除";
            this.btnGroupD.UseVisualStyleBackColor = false;
            this.btnGroupD.EnabledChanged += new System.EventHandler(this.button_EnabledChanged);
            this.btnGroupD.Click += new System.EventHandler(this.btnGroupD_Click);
            // 
            // btnGroupU
            // 
            this.btnGroupU.BackColor = System.Drawing.Color.Transparent;
            this.btnGroupU.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGroupU.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnGroupU.Location = new System.Drawing.Point(221, 717);
            this.btnGroupU.Name = "btnGroupU";
            this.btnGroupU.Size = new System.Drawing.Size(90, 38);
            this.btnGroupU.TabIndex = 8;
            this.btnGroupU.Text = "修改";
            this.btnGroupU.UseVisualStyleBackColor = false;
            this.btnGroupU.EnabledChanged += new System.EventHandler(this.button_EnabledChanged);
            this.btnGroupU.Click += new System.EventHandler(this.btnGroupU_Click);
            // 
            // btnGroupI
            // 
            this.btnGroupI.BackColor = System.Drawing.Color.Transparent;
            this.btnGroupI.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGroupI.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnGroupI.Location = new System.Drawing.Point(91, 717);
            this.btnGroupI.Name = "btnGroupI";
            this.btnGroupI.Size = new System.Drawing.Size(90, 38);
            this.btnGroupI.TabIndex = 7;
            this.btnGroupI.Text = "新增";
            this.btnGroupI.UseVisualStyleBackColor = false;
            this.btnGroupI.EnabledChanged += new System.EventHandler(this.button_EnabledChanged);
            this.btnGroupI.Click += new System.EventHandler(this.btnGroupI_Click);
            // 
            // txbDesc
            // 
            this.txbDesc.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txbDesc.Location = new System.Drawing.Point(266, 554);
            this.txbDesc.Multiline = true;
            this.txbDesc.Name = "txbDesc";
            this.txbDesc.Size = new System.Drawing.Size(275, 143);
            this.txbDesc.TabIndex = 6;
            // 
            // txbGroupCht
            // 
            this.txbGroupCht.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txbGroupCht.Location = new System.Drawing.Point(266, 446);
            this.txbGroupCht.Name = "txbGroupCht";
            this.txbGroupCht.Size = new System.Drawing.Size(275, 35);
            this.txbGroupCht.TabIndex = 5;
            // 
            // txbGroupEn
            // 
            this.txbGroupEn.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txbGroupEn.Location = new System.Drawing.Point(266, 388);
            this.txbGroupEn.Name = "txbGroupEn";
            this.txbGroupEn.Size = new System.Drawing.Size(275, 35);
            this.txbGroupEn.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.Control;
            this.label3.Location = new System.Drawing.Point(77, 554);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 27);
            this.label3.TabIndex = 3;
            this.label3.Text = "描述：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.Control;
            this.label2.Location = new System.Drawing.Point(77, 449);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(159, 27);
            this.label2.TabIndex = 2;
            this.label2.Text = "群組中文名稱：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.Control;
            this.label1.Location = new System.Drawing.Point(77, 391);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(159, 27);
            this.label1.TabIndex = 1;
            this.label1.Text = "群組英文名稱：";
            // 
            // gridGroup
            // 
            this.gridGroup.AllowUserToAddRows = false;
            this.gridGroup.AllowUserToDeleteRows = false;
            this.gridGroup.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.gridGroup.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.Menu;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridGroup.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.gridGroup.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridGroup.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.GroupID,
            this.GroupName_En,
            this.GroupName_Cht,
            this.GroupPrivilege,
            this.Description});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridGroup.DefaultCellStyle = dataGridViewCellStyle2;
            this.gridGroup.EnableHeadersVisualStyles = false;
            this.gridGroup.GridColor = System.Drawing.SystemColors.Info;
            this.gridGroup.Location = new System.Drawing.Point(25, 75);
            this.gridGroup.MultiSelect = false;
            this.gridGroup.Name = "gridGroup";
            this.gridGroup.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridGroup.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.gridGroup.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.gridGroup.RowTemplate.Height = 24;
            this.gridGroup.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridGroup.Size = new System.Drawing.Size(592, 227);
            this.gridGroup.TabIndex = 0;
            this.gridGroup.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridGroup_CellClick);
            // 
            // GroupID
            // 
            this.GroupID.DataPropertyName = "GroupID";
            this.GroupID.HeaderText = "群組ID";
            this.GroupID.Name = "GroupID";
            this.GroupID.ReadOnly = true;
            this.GroupID.Width = 120;
            // 
            // GroupName_En
            // 
            this.GroupName_En.DataPropertyName = "GroupName_En";
            this.GroupName_En.HeaderText = "群組名稱";
            this.GroupName_En.Name = "GroupName_En";
            this.GroupName_En.ReadOnly = true;
            this.GroupName_En.Width = 130;
            // 
            // GroupName_Cht
            // 
            this.GroupName_Cht.DataPropertyName = "GroupName_Cht";
            this.GroupName_Cht.HeaderText = "群組名稱";
            this.GroupName_Cht.Name = "GroupName_Cht";
            this.GroupName_Cht.ReadOnly = true;
            this.GroupName_Cht.Width = 130;
            // 
            // GroupPrivilege
            // 
            this.GroupPrivilege.DataPropertyName = "GroupPrivilege";
            this.GroupPrivilege.HeaderText = "啟用";
            this.GroupPrivilege.Name = "GroupPrivilege";
            this.GroupPrivilege.ReadOnly = true;
            // 
            // Description
            // 
            this.Description.DataPropertyName = "Description";
            this.Description.HeaderText = "描述";
            this.Description.Name = "Description";
            this.Description.ReadOnly = true;
            // 
            // cmbGroup
            // 
            this.cmbGroup.BackColor = System.Drawing.SystemColors.ControlDark;
            this.cmbGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGroup.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.cmbGroup.FormattingEnabled = true;
            this.cmbGroup.Location = new System.Drawing.Point(851, 54);
            this.cmbGroup.Name = "cmbGroup";
            this.cmbGroup.Size = new System.Drawing.Size(209, 35);
            this.cmbGroup.TabIndex = 5;
            this.cmbGroup.SelectedIndexChanged += new System.EventHandler(this.cmbGroup_SelectedIndexChanged);
            // 
            // btnGroupMemberI
            // 
            this.btnGroupMemberI.BackColor = System.Drawing.Color.Transparent;
            this.btnGroupMemberI.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGroupMemberI.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnGroupMemberI.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnGroupMemberI.Location = new System.Drawing.Point(1217, 722);
            this.btnGroupMemberI.Name = "btnGroupMemberI";
            this.btnGroupMemberI.Size = new System.Drawing.Size(209, 53);
            this.btnGroupMemberI.TabIndex = 6;
            this.btnGroupMemberI.Text = "確認群組人員";
            this.btnGroupMemberI.UseVisualStyleBackColor = false;
            this.btnGroupMemberI.EnabledChanged += new System.EventHandler(this.button_EnabledChanged);
            this.btnGroupMemberI.Click += new System.EventHandler(this.btnGroupMemberI_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label6.ForeColor = System.Drawing.SystemColors.Control;
            this.label6.Location = new System.Drawing.Point(846, 101);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(117, 27);
            this.label6.TabIndex = 7;
            this.label6.Text = "群組成員：";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label7.ForeColor = System.Drawing.SystemColors.Control;
            this.label7.Location = new System.Drawing.Point(1422, 101);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(117, 27);
            this.label7.TabIndex = 8;
            this.label7.Text = "所有成員：";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Checked = true;
            this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox1.Font = new System.Drawing.Font("新細明體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.checkBox1.ForeColor = System.Drawing.SystemColors.Control;
            this.checkBox1.Location = new System.Drawing.Point(1092, 60);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(113, 25);
            this.checkBox1.TabIndex = 9;
            this.checkBox1.Text = "顯示所有";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // Form_GroupMgt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(1910, 800);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnGroupMemberI);
            this.Controls.Add(this.cmbGroup);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.listAllMember);
            this.Controls.Add(this.listGroup);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form_GroupMgt";
            this.Text = "Form_SystemMgt";
            this.Load += new System.EventHandler(this.Form_SystemMgt_Load);
            this.Enter += new System.EventHandler(this.Form_GroupMgt_Enter);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridGroup)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView listGroup;
        private System.Windows.Forms.ListView listAllMember;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnGroupD;
        private System.Windows.Forms.Button btnGroupU;
        private System.Windows.Forms.Button btnGroupI;
        private System.Windows.Forms.TextBox txbDesc;
        private System.Windows.Forms.TextBox txbGroupCht;
        private System.Windows.Forms.TextBox txbGroupEn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView gridGroup;
        private System.Windows.Forms.DataGridViewTextBoxColumn GroupID;
        private System.Windows.Forms.DataGridViewTextBoxColumn GroupName_En;
        private System.Windows.Forms.DataGridViewTextBoxColumn GroupName_Cht;
        private System.Windows.Forms.DataGridViewTextBoxColumn GroupPrivilege;
        private System.Windows.Forms.DataGridViewTextBoxColumn Description;
        private System.Windows.Forms.TextBox txbGroupID;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbGroupPrivilege;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbGroup;
        private System.Windows.Forms.Button btnGroupMemberI;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.Button btnGroupC;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.CheckBox checkBox1;
    }
}