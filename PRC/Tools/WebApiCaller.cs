﻿using Newtonsoft.Json;
using PRC.WebApiResults;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace PRC.Tools
{
    public class WebApiCaller
    {
        
        private WebApiCaller(string url)
        {

        }

        public static ApiCallResult<TModel> Json_GetModeByGet<TModel>(string url) where TModel : class, new()
        {
            ApiCallResult<TModel> result = null;
            using (HttpClient client = new HttpClient())
            {

                client.Timeout = TimeSpan.FromMilliseconds(_connectiontimerOut);
                //要求回傳的格式為json
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                using (HttpResponseMessage response = client.GetAsync(url).Result)
                {
                    result = new ApiCallResult<TModel>()
                    {
                        IsSuccessStatusCode = response.IsSuccessStatusCode,
                        StatusCode = response.StatusCode
                    };
                    if (response.IsSuccessStatusCode)
                    {
                        result.ContentBody = JsonConvert.DeserializeObject<TModel>(response.Content.ReadAsStringAsync().Result);
                    }
                }

            }
            return result;
        }

        public static ApiCallResult<TModel> Json_GetModelByPost<TModel>(string url, object body) where TModel : class, new()
        {
            ApiCallResult<TModel> result = null;
            using (HttpClient client = new HttpClient())
            {

                client.Timeout = TimeSpan.FromMilliseconds(_connectiontimerOut);
                //要求回傳的格式為json
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                StringContent content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");
                using (HttpResponseMessage response = client.PostAsync(url, content).Result)
                {
                    result = new ApiCallResult<TModel>()
                    {
                        IsSuccessStatusCode = response.IsSuccessStatusCode,
                        StatusCode = response.StatusCode
                    };

                    if (response.IsSuccessStatusCode)
                    {
                        result.ContentBody = JsonConvert.DeserializeObject<TModel>(response.Content.ReadAsStringAsync().Result);
                    }
                }

            }

            return result;

        }

        public static HttpResponseMessage Json_GetResponseByPost(string url, object body)
        {
            using (HttpClient client = new HttpClient())
            {
                client.Timeout = TimeSpan.FromMilliseconds(_connectiontimerOut);
                //要求回傳的格式為json
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                StringContent content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");
                using (HttpResponseMessage response = client.PostAsync(url, content).Result)
                {
                    return response;
                }
            }

        }

        public static ApiCallResult<string> Srting_GetMessageByPost(string url, object body)
        {
            using (HttpClient client = new HttpClient())
            {
                client.Timeout = TimeSpan.FromMilliseconds(_connectiontimerOut);
                StringContent content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");

                using (HttpResponseMessage response = client.PostAsync(url, content).Result)
                {
                    ApiCallResult<string> callResult = new ApiCallResult<string>()
                    {
                        IsSuccessStatusCode = response.IsSuccessStatusCode,
                        StatusCode = response.StatusCode,
                        ContentBody = response.Content.ReadAsStringAsync().Result
                    };

                    return callResult;
                }
            }
        }

        public static ApiCallResult<string> Srting_GetMessageByPut(string url, object body)
        {
            using (HttpClient client = new HttpClient())
            {
                client.Timeout = TimeSpan.FromMilliseconds(_connectiontimerOut);
                StringContent content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");

                using (HttpResponseMessage response = client.PutAsync(url, content).Result)
                {
                    ApiCallResult<string> callResult = new ApiCallResult<string>()
                    {
                        IsSuccessStatusCode = response.IsSuccessStatusCode,
                        StatusCode = response.StatusCode,
                        ContentBody = response.Content.ReadAsStringAsync().Result
                    };

                    return callResult;
                }
            }
        }

        public static ApiCallResult<string> Srting_GetMessageByDelete(string url)
        {
            using (HttpClient client = new HttpClient())
            {
                client.Timeout = TimeSpan.FromMilliseconds(_connectiontimerOut);

                using (HttpResponseMessage response = client.DeleteAsync(url).Result)
                {
                    ApiCallResult<string> callResult = new ApiCallResult<string>()
                    {
                        IsSuccessStatusCode = response.IsSuccessStatusCode,
                        StatusCode = response.StatusCode,
                        ContentBody = response.Content.ReadAsStringAsync().Result
                    };

                    return callResult;
                }
            }
        }

        private const int _connectiontimerOut = 5000;

    }

}
