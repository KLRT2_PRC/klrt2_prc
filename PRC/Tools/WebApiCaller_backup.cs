﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace PRC.Tools
{
    public class WebApiCaller
    {
        public WebApiCaller(string url)
        {

        }

        public static TModel Json_GetModeByGet<TModel>(string url) where TModel : class, new()
        {
            TModel model = null;
            using (HttpClient client = new HttpClient())
            {

                client.Timeout = TimeSpan.FromMilliseconds(5000);
                //要求回傳的格式為json
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                using (HttpResponseMessage response = client.GetAsync(url).Result)
                {
                    
                    if (response.IsSuccessStatusCode)
                    {
                        model = JsonConvert.DeserializeObject<TModel>(response.Content.ReadAsStringAsync().Result);
                    }
                    else
                    {
                        throw new Exception(response.Content.ReadAsStringAsync().Result);
                    }
                }

            }
            return model;
        }

        public static TModel Json_GetModelByPost<TModel>(string url ,object body)where TModel :class,new()
        {
            TModel model = null;
            using (HttpClient client = new HttpClient())
            {

                client.Timeout = TimeSpan.FromMilliseconds(5000);
                //要求回傳的格式為json
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                StringContent content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");
                using (HttpResponseMessage response =  client.PostAsync(url,content).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        model = JsonConvert.DeserializeObject<TModel>( response.Content.ReadAsStringAsync().Result);
                    }
                    else
                    {
                        throw new Exception( response.Content.ReadAsStringAsync().Result);
                    }
                }
                
            }

            return model;

        }

        public static HttpResponseMessage Json_GetResponseByPost(string url, object body)
        {
            using (HttpClient client = new HttpClient())
            {
                client.Timeout = TimeSpan.FromMilliseconds(5000);
                //要求回傳的格式為json
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                StringContent content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");
                using (HttpResponseMessage response = client.PostAsync(url, content).Result)
                {
                    return response;
                }
            }

        }
    }
}
