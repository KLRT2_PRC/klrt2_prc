﻿using PRC.MyClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRC.Tools
{
    public class PermissionManager
    {
        public static void InitForm(IPermissionForm form, PERMISSION_FORM_ID[] permissionNames = null)
        {
            if (permissionNames == null)
                permissionNames = new PERMISSION_FORM_ID[] { PERMISSION_FORM_ID.All };

            List<PermissionObject> permissions = AskPermission(permissionNames);
            form.InitByPermission(permissions);
        }

        private static List<PermissionObject> AskPermission(PERMISSION_FORM_ID[] permissionNames)
        {
            List<GroupInfo> groupList = SHM.user[0].GroupInfo;

            Dictionary<PERMISSION_FORM_ID, PermissionObject> permissionMapper = new Dictionary<PERMISSION_FORM_ID, PermissionObject>();

            if (permissionNames.Contains(PERMISSION_FORM_ID.All))
                permissionNames = (PERMISSION_FORM_ID[])Enum.GetValues(typeof(PERMISSION_FORM_ID));

            foreach (PERMISSION_FORM_ID mapperID in permissionNames)
            {
                string mapperIDName = Enum.GetName(typeof(PERMISSION_FORM_ID), mapperID);
                foreach (GroupInfo gInfo in groupList)
                {
                    if (gInfo.ProgID_En.Equals(mapperIDName))
                    {
                        if (permissionMapper.Keys.Contains(mapperID))
                        {
                            PermissionObject obj = permissionMapper[mapperID];
                            obj.CanDisplay = gInfo.DisplayFun ? gInfo.DisplayFun : obj.CanDisplay;
                            obj.CanCreate = gInfo.InsertFun ? gInfo.InsertFun : obj.CanCreate;
                            obj.CanRead = gInfo.SearchFun ? gInfo.SearchFun : obj.CanRead;
                            obj.CanUpdate = gInfo.UpdateFun ? gInfo.UpdateFun : obj.CanUpdate;
                            obj.CanDelete = gInfo.DeleteFun ? gInfo.DeleteFun : obj.CanDelete;
                        }
                        else
                        {
                            PermissionObject obj = new PermissionObject()
                            {
                                PermissionName = gInfo.ProgID_En,
                                PermissionCode = (PERMISSION_FORM_ID)Enum.Parse(typeof(PERMISSION_FORM_ID),gInfo.ProgID_En),
                                CanDisplay = gInfo.DisplayFun,
                                CanCreate = gInfo.InsertFun,
                                CanRead = gInfo.SearchFun,
                                CanUpdate = gInfo.UpdateFun,
                                CanDelete = gInfo.DeleteFun
                            };
                            permissionMapper.Add(mapperID, obj);
                        }

                        break;
                    }
                }
        
            }

            return permissionMapper.Select(x => x.Value).ToList();
        }
    }

    public class PermissionObject
    {
        public string PermissionName { get; set; }
        public PERMISSION_FORM_ID PermissionCode { get; set; }

        public bool CanDisplay { get; set; }
        public bool CanCreate { get; set; }
        public bool CanRead { get; set; }
        public bool CanUpdate { get; set; }
        public bool CanDelete { get; set; }


    }

    public interface IPermissionForm
    {
        void InitByPermission(List<PermissionObject> permissionObjects);
    }

    public enum PERMISSION_FORM_ID
    {
        All,
        UserMgt,
        Settings,
        Group,
        Elec,
        Alarm,
        Report,
        DeptMgt,
        GroupProg,
        Signal,
        EventLog,
        BMS,
    }

}
