﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PRC
{
    public partial class Form_Total2 : Form
    {
        MessageFormat.TSS7 _tss7;
        MessageFormat.C15 _c15;

        public Form_Total2()
        {
            InitializeComponent();
        }

        private void Form_Total2_Load(object sender, EventArgs e)
        {
           
            MainFrame._udpserver.RecievedData += _udpserver_RecievedData;   
        }

        private void _udpserver_RecievedData(object sender, RecieveDataEventArgs e)
        {
            //throw new NotImplementedException();
            UpdateControls();
        }

        public void UpdateControls()
        {
            if (InvokeRequired)
            {
                BeginInvoke(new Action(UpdateControls));
                return;
            }
            else
            {
                UpdateCTStatus();
            }
        }
        private void UpdateCTStatus()
        {
            retangleUC2.State = MainFrame._mtss7.CPURunning == true ? iSCADA.Design.Utilities.Electrical.SymbolState.On : iSCADA.Design.Utilities.Electrical.SymbolState.Error;
            retangleUC2.Fill = true;//  MainFrame._mtss7.CPURunning;

            retangleUC3.State = MainFrame._mtss7.CPUError == true ? iSCADA.Design.Utilities.Electrical.SymbolState.On : iSCADA.Design.Utilities.Electrical.SymbolState.Error;
            retangleUC3.Fill = true;//  MainFrame._mtss7.CPUError;

            retangleUC4.State = MainFrame._mtss7.CPUCommFault == true ? iSCADA.Design.Utilities.Electrical.SymbolState.On : iSCADA.Design.Utilities.Electrical.SymbolState.Error;
            retangleUC4.Fill = true;// MainFrame._mtss7.CPUCommFault;

            retangleUC5.State = MainFrame._mtss7.RS485Module1Running == true ? iSCADA.Design.Utilities.Electrical.SymbolState.On : iSCADA.Design.Utilities.Electrical.SymbolState.Error;
            retangleUC5.Fill = true;//  MainFrame._mtss7.RS485Module1Running;

            retangleUC7.State = MainFrame._mtss7.RS485Module1Error == true ? iSCADA.Design.Utilities.Electrical.SymbolState.On : iSCADA.Design.Utilities.Electrical.SymbolState.Error;
            retangleUC7.Fill = true;// MainFrame._mtss7.RS485Module1Error;


            retangleUC11.State = MainFrame._mtss7.RS485Module2Running == true ? iSCADA.Design.Utilities.Electrical.SymbolState.On : iSCADA.Design.Utilities.Electrical.SymbolState.Error;
            retangleUC11.Fill = true;// MainFrame._mtss7.RS485Module2Running;

            retangleUC10.State = MainFrame._mtss7.RS485Module2Error == true ? iSCADA.Design.Utilities.Electrical.SymbolState.On : iSCADA.Design.Utilities.Electrical.SymbolState.Error;
            retangleUC10.Fill = true;// MainFrame._mtss7.RS485Module2Error;


            //retangleUC95.State = MainFrame._mc15.CPURunning == true ? iSCADA.Design.Utilities.Electrical.SymbolState.On : iSCADA.Design.Utilities.Electrical.SymbolState.Error;
            //retangleUC95.Fill = true;// MainFrame._mc15.CPURunning;

            //retangleUC94.State = MainFrame._mc15.CPUError == true ? iSCADA.Design.Utilities.Electrical.SymbolState.On : iSCADA.Design.Utilities.Electrical.SymbolState.Error;
            //retangleUC94.Fill = true;// MainFrame._mc15.CPUError;

            //retangleUC93.State = MainFrame._mc15.CPUCommFault == true ? iSCADA.Design.Utilities.Electrical.SymbolState.On : iSCADA.Design.Utilities.Electrical.SymbolState.Error;
            //retangleUC93.Fill = true;//  MainFrame._mc15.CPUCommFault;



            //if (Form_ElecTSS7._tss7.CPURunning == true)
            //{
            //    retangleUC2.Fill = true;
            //    retangleUC2.State = iSCADA.Design.Utilities.Electrical.SymbolState.On;
            //}
            //else
            //{
            //    retangleUC2.Fill = true;
            //    retangleUC2.State = iSCADA.Design.Utilities.Electrical.SymbolState.Error;
            //}

            //if (Form_ElecTSS7._tss7.CPUCommFault == true)
            //{
            //    retangleUC4.Fill = true;
            //    retangleUC4.State = iSCADA.Design.Utilities.Electrical.SymbolState.Error;
            //}
            //else
            //{
            //    retangleUC4.Fill = true;
            //    retangleUC4.State = iSCADA.Design.Utilities.Electrical.SymbolState.On;
            //}

            //if (Form_ElecTSS7._tss7.CPUError == true)
            //{
            //    retangleUC3.Fill = true;
            //    retangleUC3.State = iSCADA.Design.Utilities.Electrical.SymbolState.Error;
            //}
            //else
            //{
            //    retangleUC3.Fill = true;
            //    retangleUC3.State = iSCADA.Design.Utilities.Electrical.SymbolState.On;
            //}
        }
    }
}
