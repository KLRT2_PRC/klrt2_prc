﻿namespace PRC
{
    partial class Form_EventLog_OperatorEvent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.col_EventTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_UserID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_ProgID_En = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_EventType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_EventMessage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btn_SearchEventLog = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.dtp_StartDate = new System.Windows.Forms.DateTimePicker();
            this.dtp_EndDate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_EventTime,
            this.col_UserID,
            this.col_ProgID_En,
            this.col_EventType,
            this.col_EventMessage});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(8);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(1910, 816);
            this.dataGridView1.TabIndex = 17;
            // 
            // col_EventTime
            // 
            this.col_EventTime.DataPropertyName = "EventTime";
            this.col_EventTime.HeaderText = "事件時間";
            this.col_EventTime.Name = "col_EventTime";
            // 
            // col_UserID
            // 
            this.col_UserID.DataPropertyName = "UserID";
            this.col_UserID.HeaderText = "使用者編號";
            this.col_UserID.Name = "col_UserID";
            // 
            // col_ProgID_En
            // 
            this.col_ProgID_En.DataPropertyName = "ProgID_En";
            this.col_ProgID_En.HeaderText = "表單名稱";
            this.col_ProgID_En.Name = "col_ProgID_En";
            // 
            // col_EventType
            // 
            this.col_EventType.DataPropertyName = "EventType";
            this.col_EventType.HeaderText = "事件類型";
            this.col_EventType.Name = "col_EventType";
            // 
            // col_EventMessage
            // 
            this.col_EventMessage.DataPropertyName = "EventMessage";
            this.col_EventMessage.HeaderText = "事件訊息";
            this.col_EventMessage.Name = "col_EventMessage";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.groupBox2.Controls.Add(this.btn_SearchEventLog);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.dtp_StartDate);
            this.groupBox2.Controls.Add(this.dtp_EndDate);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Font = new System.Drawing.Font("微軟正黑體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.groupBox2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1910, 74);
            this.groupBox2.TabIndex = 19;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "鍵值區";
            // 
            // btn_SearchEventLog
            // 
            this.btn_SearchEventLog.BackColor = System.Drawing.Color.Transparent;
            this.btn_SearchEventLog.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_SearchEventLog.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btn_SearchEventLog.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_SearchEventLog.Font = new System.Drawing.Font("微軟正黑體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btn_SearchEventLog.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btn_SearchEventLog.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_SearchEventLog.Location = new System.Drawing.Point(809, 22);
            this.btn_SearchEventLog.Name = "btn_SearchEventLog";
            this.btn_SearchEventLog.Size = new System.Drawing.Size(88, 36);
            this.btn_SearchEventLog.TabIndex = 3;
            this.btn_SearchEventLog.Text = "搜尋";
            this.btn_SearchEventLog.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_SearchEventLog.UseVisualStyleBackColor = false;
            this.btn_SearchEventLog.EnabledChanged += new System.EventHandler(this.btn_SearchEventLog_EnabledChanged);
            this.btn_SearchEventLog.Click += new System.EventHandler(this.btn_SearchEventLog_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label1.Location = new System.Drawing.Point(21, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 24);
            this.label1.TabIndex = 6;
            this.label1.Text = "起始日期";
            // 
            // dtp_StartDate
            // 
            this.dtp_StartDate.CalendarForeColor = System.Drawing.Color.Yellow;
            this.dtp_StartDate.CalendarMonthBackground = System.Drawing.Color.Black;
            this.dtp_StartDate.CalendarTitleBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.dtp_StartDate.CalendarTitleForeColor = System.Drawing.Color.Cyan;
            this.dtp_StartDate.CalendarTrailingForeColor = System.Drawing.Color.Cyan;
            this.dtp_StartDate.Location = new System.Drawing.Point(113, 29);
            this.dtp_StartDate.Name = "dtp_StartDate";
            this.dtp_StartDate.Size = new System.Drawing.Size(285, 33);
            this.dtp_StartDate.TabIndex = 0;
            // 
            // dtp_EndDate
            // 
            this.dtp_EndDate.Location = new System.Drawing.Point(495, 27);
            this.dtp_EndDate.Name = "dtp_EndDate";
            this.dtp_EndDate.Size = new System.Drawing.Size(285, 33);
            this.dtp_EndDate.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label2.Location = new System.Drawing.Point(403, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 24);
            this.label2.TabIndex = 7;
            this.label2.Text = "結束日期";
            // 
            // Form_EventLog_OperatorEvent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1910, 816);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.dataGridView1);
            this.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "Form_EventLog_OperatorEvent";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Form_EventLog_Operator";
            this.Load += new System.EventHandler(this.Form_EventLog_OperatorEvent_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_EventTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_UserID;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_ProgID_En;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_EventType;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_EventMessage;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btn_SearchEventLog;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtp_StartDate;
        private System.Windows.Forms.DateTimePicker dtp_EndDate;
        private System.Windows.Forms.Label label2;
    }
}