﻿namespace PRC
{
    partial class Form_BMS2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.retangleUC11 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.retangleUC2 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC3 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC16 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC17 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC18 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC22 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonStatus2 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.buttonStatus1 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.buttonStatus3 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.buttonStatus4 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.buttonStatus5 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.buttonStatus6 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.buttonStatus7 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.buttonStatus8 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.buttonStatus9 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.buttonStatus10 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.buttonStatus11 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.buttonStatus12 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.buttonStatus13 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.buttonStatus14 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.buttonStatus15 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.buttonStatus16 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.buttonStatus17 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.buttonStatus18 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.retangleUC1 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC4 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label11 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.retangleUC5 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.retangleUC6 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC7 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC8 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC9 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC10 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC12 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.buttonStatus19 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.buttonStatus20 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.panel11 = new System.Windows.Forms.Panel();
            this.buttonStatus21 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.buttonStatus22 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.panel12 = new System.Windows.Forms.Panel();
            this.buttonStatus23 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.buttonStatus24 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.panel13 = new System.Windows.Forms.Panel();
            this.buttonStatus25 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.buttonStatus26 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.panel14 = new System.Windows.Forms.Panel();
            this.buttonStatus27 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.buttonStatus28 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.panel15 = new System.Windows.Forms.Panel();
            this.buttonStatus29 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.buttonStatus30 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.panel16 = new System.Windows.Forms.Panel();
            this.buttonStatus31 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.buttonStatus32 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.label22 = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.retangleUC15 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.retangleUC19 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC20 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC21 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC23 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC24 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC25 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.panel19 = new System.Windows.Forms.Panel();
            this.buttonStatus37 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.buttonStatus38 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.panel20 = new System.Windows.Forms.Panel();
            this.buttonStatus39 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.buttonStatus40 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.panel21 = new System.Windows.Forms.Panel();
            this.buttonStatus41 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.buttonStatus42 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.panel22 = new System.Windows.Forms.Panel();
            this.buttonStatus43 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.buttonStatus44 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.panel23 = new System.Windows.Forms.Panel();
            this.buttonStatus45 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.buttonStatus46 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.panel24 = new System.Windows.Forms.Panel();
            this.buttonStatus47 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.buttonStatus48 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.panel25 = new System.Windows.Forms.Panel();
            this.buttonStatus49 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.buttonStatus50 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.panel26 = new System.Windows.Forms.Panel();
            this.buttonStatus51 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.buttonStatus52 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.panel27 = new System.Windows.Forms.Panel();
            this.buttonStatus53 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.buttonStatus54 = new iSCADA.Design.Utilities.Electrical.ButtonStatus();
            this.retangleUC26 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC27 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label33 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel16.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel21.SuspendLayout();
            this.panel22.SuspendLayout();
            this.panel23.SuspendLayout();
            this.panel24.SuspendLayout();
            this.panel25.SuspendLayout();
            this.panel26.SuspendLayout();
            this.panel27.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(133)))), ((int)(((byte)(132)))));
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Controls.Add(this.retangleUC11, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label9, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.retangleUC2, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.retangleUC3, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.retangleUC16, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.retangleUC17, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.retangleUC18, 2, 6);
            this.tableLayoutPanel1.Controls.Add(this.retangleUC22, 2, 7);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.label10, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel3, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.panel4, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.panel5, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.panel6, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.panel7, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.panel8, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.panel9, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.retangleUC1, 2, 8);
            this.tableLayoutPanel1.Controls.Add(this.retangleUC4, 2, 9);
            this.tableLayoutPanel1.Controls.Add(this.label11, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(57, 180);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 12;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(560, 630);
            this.tableLayoutPanel1.TabIndex = 2;
            this.tableLayoutPanel1.CellPaint += new System.Windows.Forms.TableLayoutCellPaintEventHandler(this.tableLayoutPanel1_CellPaint);
            // 
            // retangleUC11
            // 
            this.retangleUC11.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.retangleUC11.BackColor = System.Drawing.Color.Red;
            this.retangleUC11.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC11.ExtenderWidth = 1;
            this.retangleUC11.Fill = false;
            this.retangleUC11.GroupID = null;
            this.retangleUC11.Location = new System.Drawing.Point(474, 259);
            this.retangleUC11.Name = "retangleUC11";
            this.retangleUC11.Size = new System.Drawing.Size(32, 32);
            this.retangleUC11.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC11.TabIndex = 19;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微軟正黑體", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(36, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 35);
            this.label1.TabIndex = 2;
            this.label1.Text = "排氣機";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微軟正黑體", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(28, 107);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 35);
            this.label2.TabIndex = 3;
            this.label2.Text = "空調機1";
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("微軟正黑體", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label9.Location = new System.Drawing.Point(455, 7);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(69, 35);
            this.label9.TabIndex = 13;
            this.label9.Text = "狀態";
            // 
            // retangleUC2
            // 
            this.retangleUC2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.retangleUC2.BackColor = System.Drawing.Color.Red;
            this.retangleUC2.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC2.ExtenderWidth = 1;
            this.retangleUC2.Fill = false;
            this.retangleUC2.GroupID = null;
            this.retangleUC2.Location = new System.Drawing.Point(474, 59);
            this.retangleUC2.Name = "retangleUC2";
            this.retangleUC2.Size = new System.Drawing.Size(32, 32);
            this.retangleUC2.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC2.TabIndex = 16;
            // 
            // retangleUC3
            // 
            this.retangleUC3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.retangleUC3.BackColor = System.Drawing.Color.Red;
            this.retangleUC3.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC3.ExtenderWidth = 1;
            this.retangleUC3.Fill = false;
            this.retangleUC3.GroupID = null;
            this.retangleUC3.Location = new System.Drawing.Point(474, 159);
            this.retangleUC3.Name = "retangleUC3";
            this.retangleUC3.Size = new System.Drawing.Size(32, 32);
            this.retangleUC3.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC3.TabIndex = 17;
            // 
            // retangleUC16
            // 
            this.retangleUC16.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.retangleUC16.BackColor = System.Drawing.Color.Lime;
            this.retangleUC16.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC16.ExtenderWidth = 1;
            this.retangleUC16.Fill = false;
            this.retangleUC16.GroupID = null;
            this.retangleUC16.Location = new System.Drawing.Point(474, 109);
            this.retangleUC16.Name = "retangleUC16";
            this.retangleUC16.Size = new System.Drawing.Size(32, 32);
            this.retangleUC16.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC16.TabIndex = 27;
            // 
            // retangleUC17
            // 
            this.retangleUC17.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.retangleUC17.BackColor = System.Drawing.Color.Lime;
            this.retangleUC17.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC17.ExtenderWidth = 1;
            this.retangleUC17.Fill = false;
            this.retangleUC17.GroupID = null;
            this.retangleUC17.Location = new System.Drawing.Point(474, 209);
            this.retangleUC17.Name = "retangleUC17";
            this.retangleUC17.Size = new System.Drawing.Size(32, 32);
            this.retangleUC17.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC17.TabIndex = 28;
            // 
            // retangleUC18
            // 
            this.retangleUC18.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.retangleUC18.BackColor = System.Drawing.Color.Lime;
            this.retangleUC18.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC18.ExtenderWidth = 1;
            this.retangleUC18.Fill = false;
            this.retangleUC18.GroupID = null;
            this.retangleUC18.Location = new System.Drawing.Point(474, 309);
            this.retangleUC18.Name = "retangleUC18";
            this.retangleUC18.Size = new System.Drawing.Size(32, 32);
            this.retangleUC18.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC18.TabIndex = 29;
            // 
            // retangleUC22
            // 
            this.retangleUC22.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.retangleUC22.BackColor = System.Drawing.Color.Lime;
            this.retangleUC22.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC22.ExtenderWidth = 1;
            this.retangleUC22.Fill = false;
            this.retangleUC22.GroupID = null;
            this.retangleUC22.Location = new System.Drawing.Point(474, 359);
            this.retangleUC22.Name = "retangleUC22";
            this.retangleUC22.Size = new System.Drawing.Size(32, 32);
            this.retangleUC22.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC22.TabIndex = 33;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微軟正黑體", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label3.Location = new System.Drawing.Point(28, 157);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 35);
            this.label3.TabIndex = 34;
            this.label3.Text = "空調機2";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微軟正黑體", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label4.Location = new System.Drawing.Point(28, 207);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(112, 35);
            this.label4.TabIndex = 35;
            this.label4.Text = "空調機3";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微軟正黑體", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label5.Location = new System.Drawing.Point(28, 257);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(112, 35);
            this.label5.TabIndex = 36;
            this.label5.Text = "空調機4";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("微軟正黑體", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label6.Location = new System.Drawing.Point(28, 307);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(112, 35);
            this.label6.TabIndex = 37;
            this.label6.Text = "空調機5";
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("微軟正黑體", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label7.Location = new System.Drawing.Point(28, 357);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(112, 35);
            this.label7.TabIndex = 38;
            this.label7.Text = "空調機6";
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("微軟正黑體", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label8.Location = new System.Drawing.Point(28, 407);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(112, 35);
            this.label8.TabIndex = 39;
            this.label8.Text = "空調機7";
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("微軟正黑體", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label10.Location = new System.Drawing.Point(28, 457);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(112, 35);
            this.label10.TabIndex = 40;
            this.label10.Text = "空調機8";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonStatus2);
            this.panel1.Controls.Add(this.buttonStatus1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(171, 53);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(246, 44);
            this.panel1.TabIndex = 41;
            // 
            // buttonStatus2
            // 
            this.buttonStatus2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus2.BtnBackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus2.BtnText = "停止";
            this.buttonStatus2.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus2.ExtenderWidth = 3;
            this.buttonStatus2.Fill = false;
            this.buttonStatus2.GroupID = null;
            this.buttonStatus2.Location = new System.Drawing.Point(121, 4);
            this.buttonStatus2.Name = "buttonStatus2";
            this.buttonStatus2.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus2.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus2.TabIndex = 18;
            // 
            // buttonStatus1
            // 
            this.buttonStatus1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus1.BtnBackColor = System.Drawing.Color.Red;
            this.buttonStatus1.BtnText = "運轉";
            this.buttonStatus1.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus1.ExtenderWidth = 3;
            this.buttonStatus1.Fill = false;
            this.buttonStatus1.GroupID = null;
            this.buttonStatus1.Location = new System.Drawing.Point(15, 4);
            this.buttonStatus1.Name = "buttonStatus1";
            this.buttonStatus1.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus1.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus1.TabIndex = 17;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.buttonStatus3);
            this.panel2.Controls.Add(this.buttonStatus4);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(171, 103);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(246, 44);
            this.panel2.TabIndex = 42;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.buttonStatus5);
            this.panel3.Controls.Add(this.buttonStatus6);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(171, 153);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(246, 44);
            this.panel3.TabIndex = 43;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.buttonStatus7);
            this.panel4.Controls.Add(this.buttonStatus8);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(171, 203);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(246, 44);
            this.panel4.TabIndex = 44;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.buttonStatus9);
            this.panel5.Controls.Add(this.buttonStatus10);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(171, 253);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(246, 44);
            this.panel5.TabIndex = 45;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.buttonStatus11);
            this.panel6.Controls.Add(this.buttonStatus12);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(171, 303);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(246, 44);
            this.panel6.TabIndex = 46;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.buttonStatus13);
            this.panel7.Controls.Add(this.buttonStatus14);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(171, 353);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(246, 44);
            this.panel7.TabIndex = 47;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.buttonStatus15);
            this.panel8.Controls.Add(this.buttonStatus16);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(171, 403);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(246, 44);
            this.panel8.TabIndex = 48;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.buttonStatus17);
            this.panel9.Controls.Add(this.buttonStatus18);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel9.Location = new System.Drawing.Point(171, 453);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(246, 44);
            this.panel9.TabIndex = 49;
            // 
            // buttonStatus3
            // 
            this.buttonStatus3.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus3.BtnBackColor = System.Drawing.Color.Lime;
            this.buttonStatus3.BtnText = "停止";
            this.buttonStatus3.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus3.ExtenderWidth = 3;
            this.buttonStatus3.Fill = false;
            this.buttonStatus3.GroupID = null;
            this.buttonStatus3.Location = new System.Drawing.Point(121, 6);
            this.buttonStatus3.Name = "buttonStatus3";
            this.buttonStatus3.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus3.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus3.TabIndex = 20;
            // 
            // buttonStatus4
            // 
            this.buttonStatus4.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus4.BtnBackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus4.BtnText = "運轉";
            this.buttonStatus4.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus4.ExtenderWidth = 3;
            this.buttonStatus4.Fill = false;
            this.buttonStatus4.GroupID = null;
            this.buttonStatus4.Location = new System.Drawing.Point(15, 6);
            this.buttonStatus4.Name = "buttonStatus4";
            this.buttonStatus4.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus4.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus4.TabIndex = 19;
            // 
            // buttonStatus5
            // 
            this.buttonStatus5.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus5.BtnBackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus5.BtnText = "停止";
            this.buttonStatus5.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus5.ExtenderWidth = 3;
            this.buttonStatus5.Fill = false;
            this.buttonStatus5.GroupID = null;
            this.buttonStatus5.Location = new System.Drawing.Point(121, 6);
            this.buttonStatus5.Name = "buttonStatus5";
            this.buttonStatus5.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus5.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus5.TabIndex = 20;
            // 
            // buttonStatus6
            // 
            this.buttonStatus6.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus6.BtnBackColor = System.Drawing.Color.Red;
            this.buttonStatus6.BtnText = "運轉";
            this.buttonStatus6.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus6.ExtenderWidth = 3;
            this.buttonStatus6.Fill = false;
            this.buttonStatus6.GroupID = null;
            this.buttonStatus6.Location = new System.Drawing.Point(15, 6);
            this.buttonStatus6.Name = "buttonStatus6";
            this.buttonStatus6.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus6.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus6.TabIndex = 19;
            // 
            // buttonStatus7
            // 
            this.buttonStatus7.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus7.BtnBackColor = System.Drawing.Color.Lime;
            this.buttonStatus7.BtnText = "停止";
            this.buttonStatus7.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus7.ExtenderWidth = 3;
            this.buttonStatus7.Fill = false;
            this.buttonStatus7.GroupID = null;
            this.buttonStatus7.Location = new System.Drawing.Point(121, 9);
            this.buttonStatus7.Name = "buttonStatus7";
            this.buttonStatus7.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus7.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus7.TabIndex = 22;
            // 
            // buttonStatus8
            // 
            this.buttonStatus8.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus8.BtnBackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus8.BtnText = "運轉";
            this.buttonStatus8.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus8.ExtenderWidth = 3;
            this.buttonStatus8.Fill = false;
            this.buttonStatus8.GroupID = null;
            this.buttonStatus8.Location = new System.Drawing.Point(15, 9);
            this.buttonStatus8.Name = "buttonStatus8";
            this.buttonStatus8.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus8.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus8.TabIndex = 21;
            // 
            // buttonStatus9
            // 
            this.buttonStatus9.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus9.BtnBackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus9.BtnText = "停止";
            this.buttonStatus9.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus9.ExtenderWidth = 3;
            this.buttonStatus9.Fill = false;
            this.buttonStatus9.GroupID = null;
            this.buttonStatus9.Location = new System.Drawing.Point(121, 6);
            this.buttonStatus9.Name = "buttonStatus9";
            this.buttonStatus9.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus9.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus9.TabIndex = 24;
            // 
            // buttonStatus10
            // 
            this.buttonStatus10.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus10.BtnBackColor = System.Drawing.Color.Red;
            this.buttonStatus10.BtnText = "運轉";
            this.buttonStatus10.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus10.ExtenderWidth = 3;
            this.buttonStatus10.Fill = false;
            this.buttonStatus10.GroupID = null;
            this.buttonStatus10.Location = new System.Drawing.Point(15, 6);
            this.buttonStatus10.Name = "buttonStatus10";
            this.buttonStatus10.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus10.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus10.TabIndex = 23;
            // 
            // buttonStatus11
            // 
            this.buttonStatus11.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus11.BtnBackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus11.BtnText = "停止";
            this.buttonStatus11.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus11.ExtenderWidth = 3;
            this.buttonStatus11.Fill = false;
            this.buttonStatus11.GroupID = null;
            this.buttonStatus11.Location = new System.Drawing.Point(121, 6);
            this.buttonStatus11.Name = "buttonStatus11";
            this.buttonStatus11.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus11.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus11.TabIndex = 26;
            // 
            // buttonStatus12
            // 
            this.buttonStatus12.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus12.BtnBackColor = System.Drawing.Color.Red;
            this.buttonStatus12.BtnText = "運轉";
            this.buttonStatus12.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus12.ExtenderWidth = 3;
            this.buttonStatus12.Fill = false;
            this.buttonStatus12.GroupID = null;
            this.buttonStatus12.Location = new System.Drawing.Point(15, 6);
            this.buttonStatus12.Name = "buttonStatus12";
            this.buttonStatus12.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus12.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus12.TabIndex = 25;
            // 
            // buttonStatus13
            // 
            this.buttonStatus13.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus13.BtnBackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus13.BtnText = "停止";
            this.buttonStatus13.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus13.ExtenderWidth = 3;
            this.buttonStatus13.Fill = false;
            this.buttonStatus13.GroupID = null;
            this.buttonStatus13.Location = new System.Drawing.Point(121, 6);
            this.buttonStatus13.Name = "buttonStatus13";
            this.buttonStatus13.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus13.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus13.TabIndex = 28;
            // 
            // buttonStatus14
            // 
            this.buttonStatus14.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus14.BtnBackColor = System.Drawing.Color.Red;
            this.buttonStatus14.BtnText = "運轉";
            this.buttonStatus14.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus14.ExtenderWidth = 3;
            this.buttonStatus14.Fill = false;
            this.buttonStatus14.GroupID = null;
            this.buttonStatus14.Location = new System.Drawing.Point(15, 6);
            this.buttonStatus14.Name = "buttonStatus14";
            this.buttonStatus14.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus14.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus14.TabIndex = 27;
            // 
            // buttonStatus15
            // 
            this.buttonStatus15.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus15.BtnBackColor = System.Drawing.Color.Lime;
            this.buttonStatus15.BtnText = "停止";
            this.buttonStatus15.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus15.ExtenderWidth = 3;
            this.buttonStatus15.Fill = false;
            this.buttonStatus15.GroupID = null;
            this.buttonStatus15.Location = new System.Drawing.Point(121, 6);
            this.buttonStatus15.Name = "buttonStatus15";
            this.buttonStatus15.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus15.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus15.TabIndex = 30;
            // 
            // buttonStatus16
            // 
            this.buttonStatus16.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus16.BtnBackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus16.BtnText = "運轉";
            this.buttonStatus16.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus16.ExtenderWidth = 3;
            this.buttonStatus16.Fill = false;
            this.buttonStatus16.GroupID = null;
            this.buttonStatus16.Location = new System.Drawing.Point(15, 6);
            this.buttonStatus16.Name = "buttonStatus16";
            this.buttonStatus16.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus16.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus16.TabIndex = 29;
            // 
            // buttonStatus17
            // 
            this.buttonStatus17.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus17.BtnBackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus17.BtnText = "停止";
            this.buttonStatus17.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus17.ExtenderWidth = 3;
            this.buttonStatus17.Fill = false;
            this.buttonStatus17.GroupID = null;
            this.buttonStatus17.Location = new System.Drawing.Point(121, 4);
            this.buttonStatus17.Name = "buttonStatus17";
            this.buttonStatus17.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus17.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus17.TabIndex = 32;
            // 
            // buttonStatus18
            // 
            this.buttonStatus18.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus18.BtnBackColor = System.Drawing.Color.Red;
            this.buttonStatus18.BtnText = "運轉";
            this.buttonStatus18.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus18.ExtenderWidth = 3;
            this.buttonStatus18.Fill = false;
            this.buttonStatus18.GroupID = null;
            this.buttonStatus18.Location = new System.Drawing.Point(15, 4);
            this.buttonStatus18.Name = "buttonStatus18";
            this.buttonStatus18.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus18.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus18.TabIndex = 31;
            // 
            // retangleUC1
            // 
            this.retangleUC1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.retangleUC1.BackColor = System.Drawing.Color.Red;
            this.retangleUC1.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC1.ExtenderWidth = 1;
            this.retangleUC1.Fill = false;
            this.retangleUC1.GroupID = null;
            this.retangleUC1.Location = new System.Drawing.Point(474, 409);
            this.retangleUC1.Name = "retangleUC1";
            this.retangleUC1.Size = new System.Drawing.Size(32, 32);
            this.retangleUC1.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC1.TabIndex = 50;
            // 
            // retangleUC4
            // 
            this.retangleUC4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.retangleUC4.BackColor = System.Drawing.Color.Lime;
            this.retangleUC4.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC4.ExtenderWidth = 1;
            this.retangleUC4.Fill = false;
            this.retangleUC4.GroupID = null;
            this.retangleUC4.Location = new System.Drawing.Point(474, 459);
            this.retangleUC4.Name = "retangleUC4";
            this.retangleUC4.Size = new System.Drawing.Size(32, 32);
            this.retangleUC4.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC4.TabIndex = 51;
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("微軟正黑體", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label11.Location = new System.Drawing.Point(44, 7);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(79, 35);
            this.label11.TabIndex = 3;
            this.label11.Text = "TSS7";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(133)))), ((int)(((byte)(132)))));
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.Controls.Add(this.retangleUC5, 2, 5);
            this.tableLayoutPanel2.Controls.Add(this.label12, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label13, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.label14, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.retangleUC6, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.retangleUC7, 2, 3);
            this.tableLayoutPanel2.Controls.Add(this.retangleUC8, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.retangleUC9, 2, 4);
            this.tableLayoutPanel2.Controls.Add(this.retangleUC10, 2, 6);
            this.tableLayoutPanel2.Controls.Add(this.retangleUC12, 2, 7);
            this.tableLayoutPanel2.Controls.Add(this.label15, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.label16, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.label17, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.label18, 0, 6);
            this.tableLayoutPanel2.Controls.Add(this.label19, 0, 7);
            this.tableLayoutPanel2.Controls.Add(this.panel10, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.panel11, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.panel12, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.panel13, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.panel14, 1, 5);
            this.tableLayoutPanel2.Controls.Add(this.panel15, 1, 6);
            this.tableLayoutPanel2.Controls.Add(this.panel16, 1, 7);
            this.tableLayoutPanel2.Controls.Add(this.label22, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(678, 180);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 12;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(560, 630);
            this.tableLayoutPanel2.TabIndex = 3;
            this.tableLayoutPanel2.CellPaint += new System.Windows.Forms.TableLayoutCellPaintEventHandler(this.tableLayoutPanel2_CellPaint);
            // 
            // retangleUC5
            // 
            this.retangleUC5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.retangleUC5.BackColor = System.Drawing.Color.Red;
            this.retangleUC5.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC5.ExtenderWidth = 1;
            this.retangleUC5.Fill = false;
            this.retangleUC5.GroupID = null;
            this.retangleUC5.Location = new System.Drawing.Point(474, 259);
            this.retangleUC5.Name = "retangleUC5";
            this.retangleUC5.Size = new System.Drawing.Size(32, 32);
            this.retangleUC5.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC5.TabIndex = 19;
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("微軟正黑體", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label12.Location = new System.Drawing.Point(36, 57);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(96, 35);
            this.label12.TabIndex = 2;
            this.label12.Text = "排氣機";
            // 
            // label13
            // 
            this.label13.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("微軟正黑體", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label13.Location = new System.Drawing.Point(28, 107);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(112, 35);
            this.label13.TabIndex = 3;
            this.label13.Text = "空調機1";
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("微軟正黑體", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label14.Location = new System.Drawing.Point(455, 7);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(69, 35);
            this.label14.TabIndex = 13;
            this.label14.Text = "狀態";
            // 
            // retangleUC6
            // 
            this.retangleUC6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.retangleUC6.BackColor = System.Drawing.Color.Red;
            this.retangleUC6.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC6.ExtenderWidth = 1;
            this.retangleUC6.Fill = false;
            this.retangleUC6.GroupID = null;
            this.retangleUC6.Location = new System.Drawing.Point(474, 59);
            this.retangleUC6.Name = "retangleUC6";
            this.retangleUC6.Size = new System.Drawing.Size(32, 32);
            this.retangleUC6.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC6.TabIndex = 16;
            // 
            // retangleUC7
            // 
            this.retangleUC7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.retangleUC7.BackColor = System.Drawing.Color.Red;
            this.retangleUC7.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC7.ExtenderWidth = 1;
            this.retangleUC7.Fill = false;
            this.retangleUC7.GroupID = null;
            this.retangleUC7.Location = new System.Drawing.Point(474, 159);
            this.retangleUC7.Name = "retangleUC7";
            this.retangleUC7.Size = new System.Drawing.Size(32, 32);
            this.retangleUC7.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC7.TabIndex = 17;
            // 
            // retangleUC8
            // 
            this.retangleUC8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.retangleUC8.BackColor = System.Drawing.Color.Lime;
            this.retangleUC8.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC8.ExtenderWidth = 1;
            this.retangleUC8.Fill = false;
            this.retangleUC8.GroupID = null;
            this.retangleUC8.Location = new System.Drawing.Point(474, 109);
            this.retangleUC8.Name = "retangleUC8";
            this.retangleUC8.Size = new System.Drawing.Size(32, 32);
            this.retangleUC8.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC8.TabIndex = 27;
            // 
            // retangleUC9
            // 
            this.retangleUC9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.retangleUC9.BackColor = System.Drawing.Color.Lime;
            this.retangleUC9.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC9.ExtenderWidth = 1;
            this.retangleUC9.Fill = false;
            this.retangleUC9.GroupID = null;
            this.retangleUC9.Location = new System.Drawing.Point(474, 209);
            this.retangleUC9.Name = "retangleUC9";
            this.retangleUC9.Size = new System.Drawing.Size(32, 32);
            this.retangleUC9.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC9.TabIndex = 28;
            // 
            // retangleUC10
            // 
            this.retangleUC10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.retangleUC10.BackColor = System.Drawing.Color.Lime;
            this.retangleUC10.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC10.ExtenderWidth = 1;
            this.retangleUC10.Fill = false;
            this.retangleUC10.GroupID = null;
            this.retangleUC10.Location = new System.Drawing.Point(474, 309);
            this.retangleUC10.Name = "retangleUC10";
            this.retangleUC10.Size = new System.Drawing.Size(32, 32);
            this.retangleUC10.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC10.TabIndex = 29;
            // 
            // retangleUC12
            // 
            this.retangleUC12.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.retangleUC12.BackColor = System.Drawing.Color.Lime;
            this.retangleUC12.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC12.ExtenderWidth = 1;
            this.retangleUC12.Fill = false;
            this.retangleUC12.GroupID = null;
            this.retangleUC12.Location = new System.Drawing.Point(474, 359);
            this.retangleUC12.Name = "retangleUC12";
            this.retangleUC12.Size = new System.Drawing.Size(32, 32);
            this.retangleUC12.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC12.TabIndex = 33;
            // 
            // label15
            // 
            this.label15.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("微軟正黑體", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label15.Location = new System.Drawing.Point(28, 157);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(112, 35);
            this.label15.TabIndex = 34;
            this.label15.Text = "空調機2";
            // 
            // label16
            // 
            this.label16.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("微軟正黑體", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label16.Location = new System.Drawing.Point(28, 207);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(112, 35);
            this.label16.TabIndex = 35;
            this.label16.Text = "空調機3";
            // 
            // label17
            // 
            this.label17.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("微軟正黑體", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label17.Location = new System.Drawing.Point(28, 257);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(112, 35);
            this.label17.TabIndex = 36;
            this.label17.Text = "空調機4";
            // 
            // label18
            // 
            this.label18.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("微軟正黑體", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label18.Location = new System.Drawing.Point(28, 307);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(112, 35);
            this.label18.TabIndex = 37;
            this.label18.Text = "空調機5";
            // 
            // label19
            // 
            this.label19.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("微軟正黑體", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label19.Location = new System.Drawing.Point(28, 357);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(112, 35);
            this.label19.TabIndex = 38;
            this.label19.Text = "空調機6";
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.buttonStatus19);
            this.panel10.Controls.Add(this.buttonStatus20);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel10.Location = new System.Drawing.Point(171, 53);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(246, 44);
            this.panel10.TabIndex = 41;
            // 
            // buttonStatus19
            // 
            this.buttonStatus19.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus19.BtnBackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus19.BtnText = "停止";
            this.buttonStatus19.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus19.ExtenderWidth = 3;
            this.buttonStatus19.Fill = false;
            this.buttonStatus19.GroupID = null;
            this.buttonStatus19.Location = new System.Drawing.Point(121, 4);
            this.buttonStatus19.Name = "buttonStatus19";
            this.buttonStatus19.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus19.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus19.TabIndex = 18;
            // 
            // buttonStatus20
            // 
            this.buttonStatus20.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus20.BtnBackColor = System.Drawing.Color.Red;
            this.buttonStatus20.BtnText = "運轉";
            this.buttonStatus20.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus20.ExtenderWidth = 3;
            this.buttonStatus20.Fill = false;
            this.buttonStatus20.GroupID = null;
            this.buttonStatus20.Location = new System.Drawing.Point(15, 4);
            this.buttonStatus20.Name = "buttonStatus20";
            this.buttonStatus20.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus20.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus20.TabIndex = 17;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.buttonStatus21);
            this.panel11.Controls.Add(this.buttonStatus22);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel11.Location = new System.Drawing.Point(171, 103);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(246, 44);
            this.panel11.TabIndex = 42;
            // 
            // buttonStatus21
            // 
            this.buttonStatus21.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus21.BtnBackColor = System.Drawing.Color.Lime;
            this.buttonStatus21.BtnText = "停止";
            this.buttonStatus21.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus21.ExtenderWidth = 3;
            this.buttonStatus21.Fill = false;
            this.buttonStatus21.GroupID = null;
            this.buttonStatus21.Location = new System.Drawing.Point(121, 6);
            this.buttonStatus21.Name = "buttonStatus21";
            this.buttonStatus21.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus21.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus21.TabIndex = 20;
            // 
            // buttonStatus22
            // 
            this.buttonStatus22.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus22.BtnBackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus22.BtnText = "運轉";
            this.buttonStatus22.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus22.ExtenderWidth = 3;
            this.buttonStatus22.Fill = false;
            this.buttonStatus22.GroupID = null;
            this.buttonStatus22.Location = new System.Drawing.Point(15, 6);
            this.buttonStatus22.Name = "buttonStatus22";
            this.buttonStatus22.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus22.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus22.TabIndex = 19;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.buttonStatus23);
            this.panel12.Controls.Add(this.buttonStatus24);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel12.Location = new System.Drawing.Point(171, 153);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(246, 44);
            this.panel12.TabIndex = 43;
            // 
            // buttonStatus23
            // 
            this.buttonStatus23.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus23.BtnBackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus23.BtnText = "停止";
            this.buttonStatus23.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus23.ExtenderWidth = 3;
            this.buttonStatus23.Fill = false;
            this.buttonStatus23.GroupID = null;
            this.buttonStatus23.Location = new System.Drawing.Point(121, 6);
            this.buttonStatus23.Name = "buttonStatus23";
            this.buttonStatus23.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus23.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus23.TabIndex = 20;
            // 
            // buttonStatus24
            // 
            this.buttonStatus24.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus24.BtnBackColor = System.Drawing.Color.Red;
            this.buttonStatus24.BtnText = "運轉";
            this.buttonStatus24.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus24.ExtenderWidth = 3;
            this.buttonStatus24.Fill = false;
            this.buttonStatus24.GroupID = null;
            this.buttonStatus24.Location = new System.Drawing.Point(15, 6);
            this.buttonStatus24.Name = "buttonStatus24";
            this.buttonStatus24.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus24.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus24.TabIndex = 19;
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.buttonStatus25);
            this.panel13.Controls.Add(this.buttonStatus26);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel13.Location = new System.Drawing.Point(171, 203);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(246, 44);
            this.panel13.TabIndex = 44;
            // 
            // buttonStatus25
            // 
            this.buttonStatus25.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus25.BtnBackColor = System.Drawing.Color.Lime;
            this.buttonStatus25.BtnText = "停止";
            this.buttonStatus25.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus25.ExtenderWidth = 3;
            this.buttonStatus25.Fill = false;
            this.buttonStatus25.GroupID = null;
            this.buttonStatus25.Location = new System.Drawing.Point(121, 9);
            this.buttonStatus25.Name = "buttonStatus25";
            this.buttonStatus25.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus25.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus25.TabIndex = 22;
            // 
            // buttonStatus26
            // 
            this.buttonStatus26.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus26.BtnBackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus26.BtnText = "運轉";
            this.buttonStatus26.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus26.ExtenderWidth = 3;
            this.buttonStatus26.Fill = false;
            this.buttonStatus26.GroupID = null;
            this.buttonStatus26.Location = new System.Drawing.Point(15, 9);
            this.buttonStatus26.Name = "buttonStatus26";
            this.buttonStatus26.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus26.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus26.TabIndex = 21;
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.buttonStatus27);
            this.panel14.Controls.Add(this.buttonStatus28);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel14.Location = new System.Drawing.Point(171, 253);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(246, 44);
            this.panel14.TabIndex = 45;
            // 
            // buttonStatus27
            // 
            this.buttonStatus27.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus27.BtnBackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus27.BtnText = "停止";
            this.buttonStatus27.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus27.ExtenderWidth = 3;
            this.buttonStatus27.Fill = false;
            this.buttonStatus27.GroupID = null;
            this.buttonStatus27.Location = new System.Drawing.Point(121, 6);
            this.buttonStatus27.Name = "buttonStatus27";
            this.buttonStatus27.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus27.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus27.TabIndex = 24;
            // 
            // buttonStatus28
            // 
            this.buttonStatus28.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus28.BtnBackColor = System.Drawing.Color.Red;
            this.buttonStatus28.BtnText = "運轉";
            this.buttonStatus28.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus28.ExtenderWidth = 3;
            this.buttonStatus28.Fill = false;
            this.buttonStatus28.GroupID = null;
            this.buttonStatus28.Location = new System.Drawing.Point(15, 6);
            this.buttonStatus28.Name = "buttonStatus28";
            this.buttonStatus28.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus28.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus28.TabIndex = 23;
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.buttonStatus29);
            this.panel15.Controls.Add(this.buttonStatus30);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel15.Location = new System.Drawing.Point(171, 303);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(246, 44);
            this.panel15.TabIndex = 46;
            // 
            // buttonStatus29
            // 
            this.buttonStatus29.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus29.BtnBackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus29.BtnText = "停止";
            this.buttonStatus29.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus29.ExtenderWidth = 3;
            this.buttonStatus29.Fill = false;
            this.buttonStatus29.GroupID = null;
            this.buttonStatus29.Location = new System.Drawing.Point(121, 6);
            this.buttonStatus29.Name = "buttonStatus29";
            this.buttonStatus29.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus29.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus29.TabIndex = 26;
            // 
            // buttonStatus30
            // 
            this.buttonStatus30.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus30.BtnBackColor = System.Drawing.Color.Red;
            this.buttonStatus30.BtnText = "運轉";
            this.buttonStatus30.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus30.ExtenderWidth = 3;
            this.buttonStatus30.Fill = false;
            this.buttonStatus30.GroupID = null;
            this.buttonStatus30.Location = new System.Drawing.Point(15, 6);
            this.buttonStatus30.Name = "buttonStatus30";
            this.buttonStatus30.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus30.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus30.TabIndex = 25;
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.buttonStatus31);
            this.panel16.Controls.Add(this.buttonStatus32);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel16.Location = new System.Drawing.Point(171, 353);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(246, 44);
            this.panel16.TabIndex = 47;
            // 
            // buttonStatus31
            // 
            this.buttonStatus31.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus31.BtnBackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus31.BtnText = "停止";
            this.buttonStatus31.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus31.ExtenderWidth = 3;
            this.buttonStatus31.Fill = false;
            this.buttonStatus31.GroupID = null;
            this.buttonStatus31.Location = new System.Drawing.Point(121, 6);
            this.buttonStatus31.Name = "buttonStatus31";
            this.buttonStatus31.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus31.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus31.TabIndex = 28;
            // 
            // buttonStatus32
            // 
            this.buttonStatus32.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus32.BtnBackColor = System.Drawing.Color.Red;
            this.buttonStatus32.BtnText = "運轉";
            this.buttonStatus32.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus32.ExtenderWidth = 3;
            this.buttonStatus32.Fill = false;
            this.buttonStatus32.GroupID = null;
            this.buttonStatus32.Location = new System.Drawing.Point(15, 6);
            this.buttonStatus32.Name = "buttonStatus32";
            this.buttonStatus32.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus32.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus32.TabIndex = 27;
            // 
            // label22
            // 
            this.label22.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("微軟正黑體", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label22.Location = new System.Drawing.Point(44, 7);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(79, 35);
            this.label22.TabIndex = 3;
            this.label22.Text = "TSS8";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(133)))), ((int)(((byte)(132)))));
            this.tableLayoutPanel3.ColumnCount = 3;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.Controls.Add(this.retangleUC15, 2, 5);
            this.tableLayoutPanel3.Controls.Add(this.label23, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.label24, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.label25, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.retangleUC19, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.retangleUC20, 2, 3);
            this.tableLayoutPanel3.Controls.Add(this.retangleUC21, 2, 2);
            this.tableLayoutPanel3.Controls.Add(this.retangleUC23, 2, 4);
            this.tableLayoutPanel3.Controls.Add(this.retangleUC24, 2, 6);
            this.tableLayoutPanel3.Controls.Add(this.retangleUC25, 2, 7);
            this.tableLayoutPanel3.Controls.Add(this.label26, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.label27, 0, 4);
            this.tableLayoutPanel3.Controls.Add(this.label28, 0, 5);
            this.tableLayoutPanel3.Controls.Add(this.label29, 0, 6);
            this.tableLayoutPanel3.Controls.Add(this.label30, 0, 7);
            this.tableLayoutPanel3.Controls.Add(this.label31, 0, 8);
            this.tableLayoutPanel3.Controls.Add(this.label32, 0, 9);
            this.tableLayoutPanel3.Controls.Add(this.panel19, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.panel20, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.panel21, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.panel22, 1, 4);
            this.tableLayoutPanel3.Controls.Add(this.panel23, 1, 5);
            this.tableLayoutPanel3.Controls.Add(this.panel24, 1, 6);
            this.tableLayoutPanel3.Controls.Add(this.panel25, 1, 7);
            this.tableLayoutPanel3.Controls.Add(this.panel26, 1, 8);
            this.tableLayoutPanel3.Controls.Add(this.panel27, 1, 9);
            this.tableLayoutPanel3.Controls.Add(this.retangleUC26, 2, 8);
            this.tableLayoutPanel3.Controls.Add(this.retangleUC27, 2, 9);
            this.tableLayoutPanel3.Controls.Add(this.label33, 0, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(1299, 180);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 12;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(560, 630);
            this.tableLayoutPanel3.TabIndex = 4;
            this.tableLayoutPanel3.CellPaint += new System.Windows.Forms.TableLayoutCellPaintEventHandler(this.tableLayoutPanel3_CellPaint);
            // 
            // retangleUC15
            // 
            this.retangleUC15.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.retangleUC15.BackColor = System.Drawing.Color.Red;
            this.retangleUC15.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC15.ExtenderWidth = 1;
            this.retangleUC15.Fill = false;
            this.retangleUC15.GroupID = null;
            this.retangleUC15.Location = new System.Drawing.Point(474, 259);
            this.retangleUC15.Name = "retangleUC15";
            this.retangleUC15.Size = new System.Drawing.Size(32, 32);
            this.retangleUC15.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC15.TabIndex = 19;
            // 
            // label23
            // 
            this.label23.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("微軟正黑體", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label23.Location = new System.Drawing.Point(36, 57);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(96, 35);
            this.label23.TabIndex = 2;
            this.label23.Text = "排氣機";
            // 
            // label24
            // 
            this.label24.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("微軟正黑體", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label24.Location = new System.Drawing.Point(28, 107);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(112, 35);
            this.label24.TabIndex = 3;
            this.label24.Text = "空調機1";
            // 
            // label25
            // 
            this.label25.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("微軟正黑體", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label25.Location = new System.Drawing.Point(455, 7);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(69, 35);
            this.label25.TabIndex = 13;
            this.label25.Text = "狀態";
            // 
            // retangleUC19
            // 
            this.retangleUC19.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.retangleUC19.BackColor = System.Drawing.Color.Red;
            this.retangleUC19.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC19.ExtenderWidth = 1;
            this.retangleUC19.Fill = false;
            this.retangleUC19.GroupID = null;
            this.retangleUC19.Location = new System.Drawing.Point(474, 59);
            this.retangleUC19.Name = "retangleUC19";
            this.retangleUC19.Size = new System.Drawing.Size(32, 32);
            this.retangleUC19.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC19.TabIndex = 16;
            // 
            // retangleUC20
            // 
            this.retangleUC20.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.retangleUC20.BackColor = System.Drawing.Color.Red;
            this.retangleUC20.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC20.ExtenderWidth = 1;
            this.retangleUC20.Fill = false;
            this.retangleUC20.GroupID = null;
            this.retangleUC20.Location = new System.Drawing.Point(474, 159);
            this.retangleUC20.Name = "retangleUC20";
            this.retangleUC20.Size = new System.Drawing.Size(32, 32);
            this.retangleUC20.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC20.TabIndex = 17;
            // 
            // retangleUC21
            // 
            this.retangleUC21.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.retangleUC21.BackColor = System.Drawing.Color.Lime;
            this.retangleUC21.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC21.ExtenderWidth = 1;
            this.retangleUC21.Fill = false;
            this.retangleUC21.GroupID = null;
            this.retangleUC21.Location = new System.Drawing.Point(474, 109);
            this.retangleUC21.Name = "retangleUC21";
            this.retangleUC21.Size = new System.Drawing.Size(32, 32);
            this.retangleUC21.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC21.TabIndex = 27;
            // 
            // retangleUC23
            // 
            this.retangleUC23.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.retangleUC23.BackColor = System.Drawing.Color.Lime;
            this.retangleUC23.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC23.ExtenderWidth = 1;
            this.retangleUC23.Fill = false;
            this.retangleUC23.GroupID = null;
            this.retangleUC23.Location = new System.Drawing.Point(474, 209);
            this.retangleUC23.Name = "retangleUC23";
            this.retangleUC23.Size = new System.Drawing.Size(32, 32);
            this.retangleUC23.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC23.TabIndex = 28;
            // 
            // retangleUC24
            // 
            this.retangleUC24.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.retangleUC24.BackColor = System.Drawing.Color.Lime;
            this.retangleUC24.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC24.ExtenderWidth = 1;
            this.retangleUC24.Fill = false;
            this.retangleUC24.GroupID = null;
            this.retangleUC24.Location = new System.Drawing.Point(474, 309);
            this.retangleUC24.Name = "retangleUC24";
            this.retangleUC24.Size = new System.Drawing.Size(32, 32);
            this.retangleUC24.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC24.TabIndex = 29;
            // 
            // retangleUC25
            // 
            this.retangleUC25.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.retangleUC25.BackColor = System.Drawing.Color.Lime;
            this.retangleUC25.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC25.ExtenderWidth = 1;
            this.retangleUC25.Fill = false;
            this.retangleUC25.GroupID = null;
            this.retangleUC25.Location = new System.Drawing.Point(474, 359);
            this.retangleUC25.Name = "retangleUC25";
            this.retangleUC25.Size = new System.Drawing.Size(32, 32);
            this.retangleUC25.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC25.TabIndex = 33;
            // 
            // label26
            // 
            this.label26.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("微軟正黑體", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label26.Location = new System.Drawing.Point(28, 157);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(112, 35);
            this.label26.TabIndex = 34;
            this.label26.Text = "空調機2";
            // 
            // label27
            // 
            this.label27.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("微軟正黑體", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label27.Location = new System.Drawing.Point(28, 207);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(112, 35);
            this.label27.TabIndex = 35;
            this.label27.Text = "空調機3";
            // 
            // label28
            // 
            this.label28.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("微軟正黑體", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label28.Location = new System.Drawing.Point(28, 257);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(112, 35);
            this.label28.TabIndex = 36;
            this.label28.Text = "空調機4";
            // 
            // label29
            // 
            this.label29.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("微軟正黑體", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label29.Location = new System.Drawing.Point(28, 307);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(112, 35);
            this.label29.TabIndex = 37;
            this.label29.Text = "空調機5";
            // 
            // label30
            // 
            this.label30.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("微軟正黑體", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label30.Location = new System.Drawing.Point(28, 357);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(112, 35);
            this.label30.TabIndex = 38;
            this.label30.Text = "空調機6";
            // 
            // label31
            // 
            this.label31.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("微軟正黑體", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label31.Location = new System.Drawing.Point(28, 407);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(112, 35);
            this.label31.TabIndex = 39;
            this.label31.Text = "空調機7";
            // 
            // label32
            // 
            this.label32.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("微軟正黑體", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label32.Location = new System.Drawing.Point(28, 457);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(112, 35);
            this.label32.TabIndex = 40;
            this.label32.Text = "空調機8";
            // 
            // panel19
            // 
            this.panel19.Controls.Add(this.buttonStatus37);
            this.panel19.Controls.Add(this.buttonStatus38);
            this.panel19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel19.Location = new System.Drawing.Point(171, 53);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(246, 44);
            this.panel19.TabIndex = 41;
            // 
            // buttonStatus37
            // 
            this.buttonStatus37.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus37.BtnBackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus37.BtnText = "停止";
            this.buttonStatus37.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus37.ExtenderWidth = 3;
            this.buttonStatus37.Fill = false;
            this.buttonStatus37.GroupID = null;
            this.buttonStatus37.Location = new System.Drawing.Point(121, 4);
            this.buttonStatus37.Name = "buttonStatus37";
            this.buttonStatus37.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus37.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus37.TabIndex = 18;
            // 
            // buttonStatus38
            // 
            this.buttonStatus38.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus38.BtnBackColor = System.Drawing.Color.Red;
            this.buttonStatus38.BtnText = "運轉";
            this.buttonStatus38.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus38.ExtenderWidth = 3;
            this.buttonStatus38.Fill = false;
            this.buttonStatus38.GroupID = null;
            this.buttonStatus38.Location = new System.Drawing.Point(15, 4);
            this.buttonStatus38.Name = "buttonStatus38";
            this.buttonStatus38.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus38.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus38.TabIndex = 17;
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.buttonStatus39);
            this.panel20.Controls.Add(this.buttonStatus40);
            this.panel20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel20.Location = new System.Drawing.Point(171, 103);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(246, 44);
            this.panel20.TabIndex = 42;
            // 
            // buttonStatus39
            // 
            this.buttonStatus39.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus39.BtnBackColor = System.Drawing.Color.Lime;
            this.buttonStatus39.BtnText = "停止";
            this.buttonStatus39.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus39.ExtenderWidth = 3;
            this.buttonStatus39.Fill = false;
            this.buttonStatus39.GroupID = null;
            this.buttonStatus39.Location = new System.Drawing.Point(121, 6);
            this.buttonStatus39.Name = "buttonStatus39";
            this.buttonStatus39.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus39.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus39.TabIndex = 20;
            // 
            // buttonStatus40
            // 
            this.buttonStatus40.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus40.BtnBackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus40.BtnText = "運轉";
            this.buttonStatus40.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus40.ExtenderWidth = 3;
            this.buttonStatus40.Fill = false;
            this.buttonStatus40.GroupID = null;
            this.buttonStatus40.Location = new System.Drawing.Point(15, 6);
            this.buttonStatus40.Name = "buttonStatus40";
            this.buttonStatus40.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus40.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus40.TabIndex = 19;
            // 
            // panel21
            // 
            this.panel21.Controls.Add(this.buttonStatus41);
            this.panel21.Controls.Add(this.buttonStatus42);
            this.panel21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel21.Location = new System.Drawing.Point(171, 153);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(246, 44);
            this.panel21.TabIndex = 43;
            // 
            // buttonStatus41
            // 
            this.buttonStatus41.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus41.BtnBackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus41.BtnText = "停止";
            this.buttonStatus41.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus41.ExtenderWidth = 3;
            this.buttonStatus41.Fill = false;
            this.buttonStatus41.GroupID = null;
            this.buttonStatus41.Location = new System.Drawing.Point(121, 6);
            this.buttonStatus41.Name = "buttonStatus41";
            this.buttonStatus41.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus41.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus41.TabIndex = 20;
            // 
            // buttonStatus42
            // 
            this.buttonStatus42.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus42.BtnBackColor = System.Drawing.Color.Red;
            this.buttonStatus42.BtnText = "運轉";
            this.buttonStatus42.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus42.ExtenderWidth = 3;
            this.buttonStatus42.Fill = false;
            this.buttonStatus42.GroupID = null;
            this.buttonStatus42.Location = new System.Drawing.Point(15, 6);
            this.buttonStatus42.Name = "buttonStatus42";
            this.buttonStatus42.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus42.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus42.TabIndex = 19;
            // 
            // panel22
            // 
            this.panel22.Controls.Add(this.buttonStatus43);
            this.panel22.Controls.Add(this.buttonStatus44);
            this.panel22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel22.Location = new System.Drawing.Point(171, 203);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(246, 44);
            this.panel22.TabIndex = 44;
            // 
            // buttonStatus43
            // 
            this.buttonStatus43.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus43.BtnBackColor = System.Drawing.Color.Lime;
            this.buttonStatus43.BtnText = "停止";
            this.buttonStatus43.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus43.ExtenderWidth = 3;
            this.buttonStatus43.Fill = false;
            this.buttonStatus43.GroupID = null;
            this.buttonStatus43.Location = new System.Drawing.Point(121, 9);
            this.buttonStatus43.Name = "buttonStatus43";
            this.buttonStatus43.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus43.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus43.TabIndex = 22;
            // 
            // buttonStatus44
            // 
            this.buttonStatus44.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus44.BtnBackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus44.BtnText = "運轉";
            this.buttonStatus44.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus44.ExtenderWidth = 3;
            this.buttonStatus44.Fill = false;
            this.buttonStatus44.GroupID = null;
            this.buttonStatus44.Location = new System.Drawing.Point(15, 9);
            this.buttonStatus44.Name = "buttonStatus44";
            this.buttonStatus44.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus44.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus44.TabIndex = 21;
            // 
            // panel23
            // 
            this.panel23.Controls.Add(this.buttonStatus45);
            this.panel23.Controls.Add(this.buttonStatus46);
            this.panel23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel23.Location = new System.Drawing.Point(171, 253);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(246, 44);
            this.panel23.TabIndex = 45;
            // 
            // buttonStatus45
            // 
            this.buttonStatus45.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus45.BtnBackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus45.BtnText = "停止";
            this.buttonStatus45.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus45.ExtenderWidth = 3;
            this.buttonStatus45.Fill = false;
            this.buttonStatus45.GroupID = null;
            this.buttonStatus45.Location = new System.Drawing.Point(121, 6);
            this.buttonStatus45.Name = "buttonStatus45";
            this.buttonStatus45.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus45.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus45.TabIndex = 24;
            // 
            // buttonStatus46
            // 
            this.buttonStatus46.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus46.BtnBackColor = System.Drawing.Color.Red;
            this.buttonStatus46.BtnText = "運轉";
            this.buttonStatus46.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus46.ExtenderWidth = 3;
            this.buttonStatus46.Fill = false;
            this.buttonStatus46.GroupID = null;
            this.buttonStatus46.Location = new System.Drawing.Point(15, 6);
            this.buttonStatus46.Name = "buttonStatus46";
            this.buttonStatus46.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus46.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus46.TabIndex = 23;
            // 
            // panel24
            // 
            this.panel24.Controls.Add(this.buttonStatus47);
            this.panel24.Controls.Add(this.buttonStatus48);
            this.panel24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel24.Location = new System.Drawing.Point(171, 303);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(246, 44);
            this.panel24.TabIndex = 46;
            // 
            // buttonStatus47
            // 
            this.buttonStatus47.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus47.BtnBackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus47.BtnText = "停止";
            this.buttonStatus47.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus47.ExtenderWidth = 3;
            this.buttonStatus47.Fill = false;
            this.buttonStatus47.GroupID = null;
            this.buttonStatus47.Location = new System.Drawing.Point(121, 6);
            this.buttonStatus47.Name = "buttonStatus47";
            this.buttonStatus47.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus47.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus47.TabIndex = 26;
            // 
            // buttonStatus48
            // 
            this.buttonStatus48.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus48.BtnBackColor = System.Drawing.Color.Red;
            this.buttonStatus48.BtnText = "運轉";
            this.buttonStatus48.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus48.ExtenderWidth = 3;
            this.buttonStatus48.Fill = false;
            this.buttonStatus48.GroupID = null;
            this.buttonStatus48.Location = new System.Drawing.Point(15, 6);
            this.buttonStatus48.Name = "buttonStatus48";
            this.buttonStatus48.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus48.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus48.TabIndex = 25;
            // 
            // panel25
            // 
            this.panel25.Controls.Add(this.buttonStatus49);
            this.panel25.Controls.Add(this.buttonStatus50);
            this.panel25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel25.Location = new System.Drawing.Point(171, 353);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(246, 44);
            this.panel25.TabIndex = 47;
            // 
            // buttonStatus49
            // 
            this.buttonStatus49.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus49.BtnBackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus49.BtnText = "停止";
            this.buttonStatus49.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus49.ExtenderWidth = 3;
            this.buttonStatus49.Fill = false;
            this.buttonStatus49.GroupID = null;
            this.buttonStatus49.Location = new System.Drawing.Point(121, 6);
            this.buttonStatus49.Name = "buttonStatus49";
            this.buttonStatus49.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus49.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus49.TabIndex = 28;
            // 
            // buttonStatus50
            // 
            this.buttonStatus50.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus50.BtnBackColor = System.Drawing.Color.Red;
            this.buttonStatus50.BtnText = "運轉";
            this.buttonStatus50.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus50.ExtenderWidth = 3;
            this.buttonStatus50.Fill = false;
            this.buttonStatus50.GroupID = null;
            this.buttonStatus50.Location = new System.Drawing.Point(15, 6);
            this.buttonStatus50.Name = "buttonStatus50";
            this.buttonStatus50.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus50.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus50.TabIndex = 27;
            // 
            // panel26
            // 
            this.panel26.Controls.Add(this.buttonStatus51);
            this.panel26.Controls.Add(this.buttonStatus52);
            this.panel26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel26.Location = new System.Drawing.Point(171, 403);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(246, 44);
            this.panel26.TabIndex = 48;
            // 
            // buttonStatus51
            // 
            this.buttonStatus51.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus51.BtnBackColor = System.Drawing.Color.Lime;
            this.buttonStatus51.BtnText = "停止";
            this.buttonStatus51.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus51.ExtenderWidth = 3;
            this.buttonStatus51.Fill = false;
            this.buttonStatus51.GroupID = null;
            this.buttonStatus51.Location = new System.Drawing.Point(121, 6);
            this.buttonStatus51.Name = "buttonStatus51";
            this.buttonStatus51.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus51.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus51.TabIndex = 30;
            // 
            // buttonStatus52
            // 
            this.buttonStatus52.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus52.BtnBackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus52.BtnText = "運轉";
            this.buttonStatus52.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus52.ExtenderWidth = 3;
            this.buttonStatus52.Fill = false;
            this.buttonStatus52.GroupID = null;
            this.buttonStatus52.Location = new System.Drawing.Point(15, 6);
            this.buttonStatus52.Name = "buttonStatus52";
            this.buttonStatus52.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus52.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus52.TabIndex = 29;
            // 
            // panel27
            // 
            this.panel27.Controls.Add(this.buttonStatus53);
            this.panel27.Controls.Add(this.buttonStatus54);
            this.panel27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel27.Location = new System.Drawing.Point(171, 453);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(246, 44);
            this.panel27.TabIndex = 49;
            // 
            // buttonStatus53
            // 
            this.buttonStatus53.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus53.BtnBackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus53.BtnText = "停止";
            this.buttonStatus53.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus53.ExtenderWidth = 3;
            this.buttonStatus53.Fill = false;
            this.buttonStatus53.GroupID = null;
            this.buttonStatus53.Location = new System.Drawing.Point(121, 4);
            this.buttonStatus53.Name = "buttonStatus53";
            this.buttonStatus53.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus53.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus53.TabIndex = 32;
            // 
            // buttonStatus54
            // 
            this.buttonStatus54.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStatus54.BtnBackColor = System.Drawing.Color.Red;
            this.buttonStatus54.BtnText = "運轉";
            this.buttonStatus54.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.buttonStatus54.ExtenderWidth = 3;
            this.buttonStatus54.Fill = false;
            this.buttonStatus54.GroupID = null;
            this.buttonStatus54.Location = new System.Drawing.Point(15, 4);
            this.buttonStatus54.Name = "buttonStatus54";
            this.buttonStatus54.Size = new System.Drawing.Size(100, 35);
            this.buttonStatus54.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.buttonStatus54.TabIndex = 31;
            // 
            // retangleUC26
            // 
            this.retangleUC26.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.retangleUC26.BackColor = System.Drawing.Color.Red;
            this.retangleUC26.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC26.ExtenderWidth = 1;
            this.retangleUC26.Fill = false;
            this.retangleUC26.GroupID = null;
            this.retangleUC26.Location = new System.Drawing.Point(474, 409);
            this.retangleUC26.Name = "retangleUC26";
            this.retangleUC26.Size = new System.Drawing.Size(32, 32);
            this.retangleUC26.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC26.TabIndex = 50;
            // 
            // retangleUC27
            // 
            this.retangleUC27.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.retangleUC27.BackColor = System.Drawing.Color.Lime;
            this.retangleUC27.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC27.ExtenderWidth = 1;
            this.retangleUC27.Fill = false;
            this.retangleUC27.GroupID = null;
            this.retangleUC27.Location = new System.Drawing.Point(474, 459);
            this.retangleUC27.Name = "retangleUC27";
            this.retangleUC27.Size = new System.Drawing.Size(32, 32);
            this.retangleUC27.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC27.TabIndex = 51;
            // 
            // label33
            // 
            this.label33.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("微軟正黑體", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label33.Location = new System.Drawing.Point(44, 7);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(79, 35);
            this.label33.TabIndex = 3;
            this.label33.Text = "TSS9";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("微軟正黑體", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label20.Location = new System.Drawing.Point(721, 50);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(475, 80);
            this.label20.TabIndex = 8;
            this.label20.Text = "  高雄輕軌二期設施機電(BMS)\r\n輕軌設備室排氣機與空調機(1/2)";
            // 
            // FormBMS2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkGray;
            this.ClientSize = new System.Drawing.Size(1904, 900);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.tableLayoutPanel3);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormBMS2";
            this.Text = "FormBMS2";
            this.Load += new System.EventHandler(this.FormBMS2_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.panel19.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.panel21.ResumeLayout(false);
            this.panel22.ResumeLayout(false);
            this.panel23.ResumeLayout(false);
            this.panel24.ResumeLayout(false);
            this.panel25.ResumeLayout(false);
            this.panel26.ResumeLayout(false);
            this.panel27.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC11;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label9;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC2;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC3;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC16;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC17;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC18;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC22;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel1;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus2;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus1;
        private System.Windows.Forms.Panel panel2;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus3;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus4;
        private System.Windows.Forms.Panel panel3;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus5;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus6;
        private System.Windows.Forms.Panel panel4;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus7;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus8;
        private System.Windows.Forms.Panel panel5;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus9;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus10;
        private System.Windows.Forms.Panel panel6;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus11;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus12;
        private System.Windows.Forms.Panel panel7;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus13;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus14;
        private System.Windows.Forms.Panel panel8;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus15;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus16;
        private System.Windows.Forms.Panel panel9;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus17;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus18;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC1;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC4;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC5;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC6;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC7;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC8;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC9;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC10;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC12;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Panel panel10;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus19;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus20;
        private System.Windows.Forms.Panel panel11;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus21;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus22;
        private System.Windows.Forms.Panel panel12;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus23;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus24;
        private System.Windows.Forms.Panel panel13;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus25;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus26;
        private System.Windows.Forms.Panel panel14;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus27;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus28;
        private System.Windows.Forms.Panel panel15;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus29;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus30;
        private System.Windows.Forms.Panel panel16;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus31;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus32;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC15;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC19;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC20;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC21;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC23;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC24;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Panel panel19;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus37;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus38;
        private System.Windows.Forms.Panel panel20;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus39;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus40;
        private System.Windows.Forms.Panel panel21;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus41;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus42;
        private System.Windows.Forms.Panel panel22;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus43;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus44;
        private System.Windows.Forms.Panel panel23;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus45;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus46;
        private System.Windows.Forms.Panel panel24;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus47;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus48;
        private System.Windows.Forms.Panel panel25;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus49;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus50;
        private System.Windows.Forms.Panel panel26;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus51;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus52;
        private System.Windows.Forms.Panel panel27;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus53;
        private iSCADA.Design.Utilities.Electrical.ButtonStatus buttonStatus54;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC26;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC27;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label20;
    }
}