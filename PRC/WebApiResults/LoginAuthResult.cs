﻿using PRC.MyClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.WebApiResults
{
    public class LoginAuthResult
    {
        public UserInfos UserInfo { get; set; }
        public AuthStatus StatusCode { get; set; }
        public string ErrMessage { get; set; }

        public enum AuthStatus
        {
            Success,
            AccountError,
            PasswordError
        }
    }
}