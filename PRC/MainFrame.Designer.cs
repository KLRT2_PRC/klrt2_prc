﻿using System.Drawing;
namespace PRC
{
    partial class MainFrame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelMain = new System.Windows.Forms.Panel();
            this.pnl_SubBtn = new System.Windows.Forms.Panel();
            this.datagridview = new System.Windows.Forms.DataGridView();
            this.EventTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SystemID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SubsystemID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Location = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EquipID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Value = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AlarmLevel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AlarmMsg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UserID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label16 = new System.Windows.Forms.Label();
            this.label123 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnAbout = new System.Windows.Forms.Button();
            this.btnTimeSync = new System.Windows.Forms.Button();
            this.btnLogin = new System.Windows.Forms.Button();
            this.btnEvents = new System.Windows.Forms.RadioButton();
            this.btnSignal = new System.Windows.Forms.RadioButton();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnElecs = new System.Windows.Forms.ToolStripSplitButton();
            this.total1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.total2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tSS7ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.c15ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.c16ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.c17ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSettings = new System.Windows.Forms.RadioButton();
            this.btnAlarm = new System.Windows.Forms.RadioButton();
            this.btnReport = new System.Windows.Forms.RadioButton();
            this.btnBMS = new System.Windows.Forms.RadioButton();
            this.btnElec = new System.Windows.Forms.RadioButton();
            this.btnIndex = new System.Windows.Forms.RadioButton();
            this.lbStaff = new System.Windows.Forms.Label();
            this.pnl_Body = new System.Windows.Forms.Panel();
            this.panelMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datagridview)).BeginInit();
            this.panel1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelMain
            // 
            this.panelMain.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panelMain.Controls.Add(this.pnl_SubBtn);
            this.panelMain.Controls.Add(this.datagridview);
            this.panelMain.Controls.Add(this.label16);
            this.panelMain.Controls.Add(this.label123);
            this.panelMain.Controls.Add(this.label15);
            this.panelMain.Controls.Add(this.panel1);
            this.panelMain.Controls.Add(this.lbStaff);
            this.panelMain.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelMain.Location = new System.Drawing.Point(0, 0);
            this.panelMain.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(1904, 120);
            this.panelMain.TabIndex = 1;
            // 
            // pnl_SubBtn
            // 
            this.pnl_SubBtn.Location = new System.Drawing.Point(6, 46);
            this.pnl_SubBtn.Name = "pnl_SubBtn";
            this.pnl_SubBtn.Size = new System.Drawing.Size(907, 67);
            this.pnl_SubBtn.TabIndex = 3;
            // 
            // datagridview
            // 
            this.datagridview.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.datagridview.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EventTime,
            this.SystemID,
            this.SubsystemID,
            this.Location,
            this.EquipID,
            this.Value,
            this.AlarmLevel,
            this.AlarmMsg,
            this.UserID});
            this.datagridview.Location = new System.Drawing.Point(919, 6);
            this.datagridview.Name = "datagridview";
            this.datagridview.RowTemplate.Height = 24;
            this.datagridview.Size = new System.Drawing.Size(808, 104);
            this.datagridview.TabIndex = 3;
            // 
            // EventTime
            // 
            this.EventTime.DataPropertyName = "EventTime";
            this.EventTime.HeaderText = "事件時間";
            this.EventTime.Name = "EventTime";
            // 
            // SystemID
            // 
            this.SystemID.DataPropertyName = "SystemID";
            this.SystemID.HeaderText = "系統";
            this.SystemID.Name = "SystemID";
            // 
            // SubsystemID
            // 
            this.SubsystemID.DataPropertyName = "SubsystemID";
            this.SubsystemID.HeaderText = "子系統";
            this.SubsystemID.Name = "SubsystemID";
            // 
            // Location
            // 
            this.Location.DataPropertyName = "Location";
            this.Location.HeaderText = "位置";
            this.Location.Name = "Location";
            // 
            // EquipID
            // 
            this.EquipID.DataPropertyName = "EquipID";
            this.EquipID.HeaderText = "設備";
            this.EquipID.Name = "EquipID";
            // 
            // Value
            // 
            this.Value.DataPropertyName = "Value";
            this.Value.HeaderText = "值";
            this.Value.Name = "Value";
            // 
            // AlarmLevel
            // 
            this.AlarmLevel.DataPropertyName = "AlarmLevel";
            this.AlarmLevel.HeaderText = "警報等級";
            this.AlarmLevel.Name = "AlarmLevel";
            // 
            // AlarmMsg
            // 
            this.AlarmMsg.DataPropertyName = "AlarmMsg";
            this.AlarmMsg.HeaderText = "警報訊息";
            this.AlarmMsg.Name = "AlarmMsg";
            // 
            // UserID
            // 
            this.UserID.DataPropertyName = "UserID";
            this.UserID.HeaderText = "驗證者";
            this.UserID.Name = "UserID";
            this.UserID.Visible = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label16.ForeColor = System.Drawing.Color.White;
            this.label16.Location = new System.Drawing.Point(1746, 84);
            this.label16.Margin = new System.Windows.Forms.Padding(0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(111, 16);
            this.label16.TabIndex = 2;
            this.label16.Text = "2017/06/06  13:30";
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label123.ForeColor = System.Drawing.Color.White;
            this.label123.Location = new System.Drawing.Point(1745, 11);
            this.label123.Margin = new System.Windows.Forms.Padding(0);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(105, 20);
            this.label123.TabIndex = 2;
            this.label123.Text = "目前使用者：";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(1745, 60);
            this.label15.Margin = new System.Windows.Forms.Padding(0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(89, 20);
            this.label15.TabIndex = 2;
            this.label15.Text = "系統時間：";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnAbout);
            this.panel1.Controls.Add(this.btnTimeSync);
            this.panel1.Controls.Add(this.btnLogin);
            this.panel1.Controls.Add(this.btnEvents);
            this.panel1.Controls.Add(this.btnSignal);
            this.panel1.Controls.Add(this.toolStrip1);
            this.panel1.Controls.Add(this.btnSettings);
            this.panel1.Controls.Add(this.btnAlarm);
            this.panel1.Controls.Add(this.btnReport);
            this.panel1.Controls.Add(this.btnBMS);
            this.panel1.Controls.Add(this.btnElec);
            this.panel1.Controls.Add(this.btnIndex);
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(910, 37);
            this.panel1.TabIndex = 5;
            // 
            // btnAbout
            // 
            this.btnAbout.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.btnAbout.Enabled = false;
            this.btnAbout.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnAbout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAbout.Location = new System.Drawing.Point(662, 4);
            this.btnAbout.Name = "btnAbout";
            this.btnAbout.Size = new System.Drawing.Size(72, 28);
            this.btnAbout.TabIndex = 18;
            this.btnAbout.Text = "說明";
            this.btnAbout.UseVisualStyleBackColor = false;
            this.btnAbout.Click += new System.EventHandler(this.btnAbout_Click);
            // 
            // btnTimeSync
            // 
            this.btnTimeSync.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.btnTimeSync.Enabled = false;
            this.btnTimeSync.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnTimeSync.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTimeSync.Location = new System.Drawing.Point(506, 4);
            this.btnTimeSync.Name = "btnTimeSync";
            this.btnTimeSync.Size = new System.Drawing.Size(72, 28);
            this.btnTimeSync.TabIndex = 17;
            this.btnTimeSync.Text = "時間同步";
            this.btnTimeSync.UseVisualStyleBackColor = false;
            this.btnTimeSync.Click += new System.EventHandler(this.btnTimeSync_Click);
            // 
            // btnLogin
            // 
            this.btnLogin.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.btnLogin.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogin.Location = new System.Drawing.Point(740, 4);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(80, 28);
            this.btnLogin.TabIndex = 0;
            this.btnLogin.Text = "登入/登出";
            this.btnLogin.UseVisualStyleBackColor = false;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // btnEvents
            // 
            this.btnEvents.Appearance = System.Windows.Forms.Appearance.Button;
            this.btnEvents.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.btnEvents.Enabled = false;
            this.btnEvents.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnEvents.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnEvents.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEvents.Font = new System.Drawing.Font("新細明體", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnEvents.Location = new System.Drawing.Point(293, 4);
            this.btnEvents.Name = "btnEvents";
            this.btnEvents.Size = new System.Drawing.Size(71, 28);
            this.btnEvents.TabIndex = 10;
            this.btnEvents.TabStop = true;
            this.btnEvents.Text = "事件紀錄";
            this.btnEvents.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnEvents.UseVisualStyleBackColor = false;
            this.btnEvents.CheckedChanged += new System.EventHandler(this.menu_CheckedChanged);
            this.btnEvents.Click += new System.EventHandler(this.btnEvents_Click);
            // 
            // btnSignal
            // 
            this.btnSignal.Appearance = System.Windows.Forms.Appearance.Button;
            this.btnSignal.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.btnSignal.Enabled = false;
            this.btnSignal.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnSignal.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnSignal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSignal.Font = new System.Drawing.Font("新細明體", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnSignal.Location = new System.Drawing.Point(148, 4);
            this.btnSignal.Name = "btnSignal";
            this.btnSignal.Size = new System.Drawing.Size(71, 29);
            this.btnSignal.TabIndex = 6;
            this.btnSignal.TabStop = true;
            this.btnSignal.Text = "號誌警報";
            this.btnSignal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnSignal.UseVisualStyleBackColor = false;
            this.btnSignal.CheckedChanged += new System.EventHandler(this.menu_CheckedChanged);
            this.btnSignal.Click += new System.EventHandler(this.btnSignal_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(70, 63);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnElecs});
            this.toolStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow;
            this.toolStrip1.Location = new System.Drawing.Point(100, 63);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Padding = new System.Windows.Forms.Padding(0);
            this.toolStrip1.Size = new System.Drawing.Size(80, 29);
            this.toolStrip1.TabIndex = 3;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btnElecs
            // 
            this.btnElecs.AutoSize = false;
            this.btnElecs.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnElecs.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.total1ToolStripMenuItem,
            this.total2ToolStripMenuItem,
            this.tSS7ToolStripMenuItem,
            this.c15ToolStripMenuItem,
            this.c16ToolStripMenuItem,
            this.c17ToolStripMenuItem});
            this.btnElecs.Enabled = false;
            this.btnElecs.Image = global::PRC.Properties.Resources.power;
            this.btnElecs.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnElecs.Name = "btnElecs";
            this.btnElecs.Size = new System.Drawing.Size(80, 70);
            this.btnElecs.Text = "toolStripSplitButton1";
            // 
            // total1ToolStripMenuItem
            // 
            this.total1ToolStripMenuItem.Name = "total1ToolStripMenuItem";
            this.total1ToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.total1ToolStripMenuItem.Text = "Total1";
            this.total1ToolStripMenuItem.Click += new System.EventHandler(this.total1ToolStripMenuItem_Click);
            // 
            // total2ToolStripMenuItem
            // 
            this.total2ToolStripMenuItem.Name = "total2ToolStripMenuItem";
            this.total2ToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.total2ToolStripMenuItem.Text = "Total2";
            this.total2ToolStripMenuItem.Click += new System.EventHandler(this.total2ToolStripMenuItem_Click);
            // 
            // tSS7ToolStripMenuItem
            // 
            this.tSS7ToolStripMenuItem.Name = "tSS7ToolStripMenuItem";
            this.tSS7ToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.tSS7ToolStripMenuItem.Text = "TSS7";
            this.tSS7ToolStripMenuItem.Click += new System.EventHandler(this.tSS7ToolStripMenuItem_Click);
            // 
            // c15ToolStripMenuItem
            // 
            this.c15ToolStripMenuItem.Name = "c15ToolStripMenuItem";
            this.c15ToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.c15ToolStripMenuItem.Text = "C15";
            this.c15ToolStripMenuItem.Click += new System.EventHandler(this.c15ToolStripMenuItem_Click);
            // 
            // c16ToolStripMenuItem
            // 
            this.c16ToolStripMenuItem.Name = "c16ToolStripMenuItem";
            this.c16ToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.c16ToolStripMenuItem.Text = "C16";
            this.c16ToolStripMenuItem.Click += new System.EventHandler(this.c16ToolStripMenuItem_Click);
            // 
            // c17ToolStripMenuItem
            // 
            this.c17ToolStripMenuItem.Name = "c17ToolStripMenuItem";
            this.c17ToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.c17ToolStripMenuItem.Text = "C17";
            this.c17ToolStripMenuItem.Click += new System.EventHandler(this.c17ToolStripMenuItem_Click);
            // 
            // btnSettings
            // 
            this.btnSettings.Appearance = System.Windows.Forms.Appearance.Button;
            this.btnSettings.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.btnSettings.Enabled = false;
            this.btnSettings.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnSettings.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSettings.Font = new System.Drawing.Font("新細明體", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnSettings.Location = new System.Drawing.Point(584, 4);
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.Size = new System.Drawing.Size(72, 28);
            this.btnSettings.TabIndex = 14;
            this.btnSettings.TabStop = true;
            this.btnSettings.Text = "系統設定";
            this.btnSettings.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnSettings.UseVisualStyleBackColor = false;
            this.btnSettings.CheckedChanged += new System.EventHandler(this.menu_CheckedChanged);
            this.btnSettings.Click += new System.EventHandler(this.btnSettings_Click);
            // 
            // btnAlarm
            // 
            this.btnAlarm.Appearance = System.Windows.Forms.Appearance.Button;
            this.btnAlarm.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.btnAlarm.Enabled = false;
            this.btnAlarm.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnAlarm.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnAlarm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAlarm.Font = new System.Drawing.Font("新細明體", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnAlarm.Location = new System.Drawing.Point(438, 4);
            this.btnAlarm.Name = "btnAlarm";
            this.btnAlarm.Size = new System.Drawing.Size(62, 28);
            this.btnAlarm.TabIndex = 12;
            this.btnAlarm.TabStop = true;
            this.btnAlarm.Text = "警報";
            this.btnAlarm.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnAlarm.UseVisualStyleBackColor = false;
            this.btnAlarm.CheckedChanged += new System.EventHandler(this.menu_CheckedChanged);
            this.btnAlarm.Click += new System.EventHandler(this.btnAlarm_Click);
            // 
            // btnReport
            // 
            this.btnReport.Appearance = System.Windows.Forms.Appearance.Button;
            this.btnReport.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.btnReport.Enabled = false;
            this.btnReport.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnReport.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReport.Font = new System.Drawing.Font("新細明體", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnReport.Location = new System.Drawing.Point(370, 4);
            this.btnReport.Name = "btnReport";
            this.btnReport.Size = new System.Drawing.Size(62, 28);
            this.btnReport.TabIndex = 11;
            this.btnReport.TabStop = true;
            this.btnReport.Text = "報表";
            this.btnReport.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnReport.UseVisualStyleBackColor = false;
            this.btnReport.CheckedChanged += new System.EventHandler(this.menu_CheckedChanged);
            this.btnReport.Click += new System.EventHandler(this.btnReport_Click);
            // 
            // btnBMS
            // 
            this.btnBMS.Appearance = System.Windows.Forms.Appearance.Button;
            this.btnBMS.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.btnBMS.Enabled = false;
            this.btnBMS.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnBMS.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnBMS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBMS.Font = new System.Drawing.Font("新細明體", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnBMS.Location = new System.Drawing.Point(225, 4);
            this.btnBMS.Name = "btnBMS";
            this.btnBMS.Size = new System.Drawing.Size(62, 28);
            this.btnBMS.TabIndex = 9;
            this.btnBMS.TabStop = true;
            this.btnBMS.Text = "BMS";
            this.btnBMS.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnBMS.UseVisualStyleBackColor = false;
            this.btnBMS.CheckedChanged += new System.EventHandler(this.menu_CheckedChanged);
            this.btnBMS.Click += new System.EventHandler(this.btnBMS_Click);
            // 
            // btnElec
            // 
            this.btnElec.Appearance = System.Windows.Forms.Appearance.Button;
            this.btnElec.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.btnElec.Enabled = false;
            this.btnElec.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnElec.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnElec.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnElec.Font = new System.Drawing.Font("新細明體", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnElec.Location = new System.Drawing.Point(71, 4);
            this.btnElec.Name = "btnElec";
            this.btnElec.Size = new System.Drawing.Size(71, 29);
            this.btnElec.TabIndex = 5;
            this.btnElec.TabStop = true;
            this.btnElec.Text = "電力供應";
            this.btnElec.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnElec.UseVisualStyleBackColor = false;
            this.btnElec.CheckedChanged += new System.EventHandler(this.menu_CheckedChanged);
            this.btnElec.Click += new System.EventHandler(this.btnElec_Click);
            // 
            // btnIndex
            // 
            this.btnIndex.Appearance = System.Windows.Forms.Appearance.Button;
            this.btnIndex.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.btnIndex.Enabled = false;
            this.btnIndex.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnIndex.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnIndex.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIndex.Font = new System.Drawing.Font("新細明體", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnIndex.Location = new System.Drawing.Point(3, 4);
            this.btnIndex.Name = "btnIndex";
            this.btnIndex.Size = new System.Drawing.Size(62, 30);
            this.btnIndex.TabIndex = 3;
            this.btnIndex.TabStop = true;
            this.btnIndex.Text = "首頁";
            this.btnIndex.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnIndex.UseVisualStyleBackColor = false;
            // 
            // lbStaff
            // 
            this.lbStaff.AutoSize = true;
            this.lbStaff.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lbStaff.ForeColor = System.Drawing.Color.White;
            this.lbStaff.Location = new System.Drawing.Point(1745, 31);
            this.lbStaff.Margin = new System.Windows.Forms.Padding(0);
            this.lbStaff.Name = "lbStaff";
            this.lbStaff.Size = new System.Drawing.Size(45, 20);
            this.lbStaff.TabIndex = 2;
            this.lbStaff.Text = "TEST";
            // 
            // pnl_Body
            // 
            this.pnl_Body.BackColor = System.Drawing.Color.White;
            this.pnl_Body.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl_Body.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnl_Body.Location = new System.Drawing.Point(0, 120);
            this.pnl_Body.Name = "pnl_Body";
            this.pnl_Body.Size = new System.Drawing.Size(1904, 921);
            this.pnl_Body.TabIndex = 3;
            // 
            // MainFrame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.ClientSize = new System.Drawing.Size(1904, 1041);
            this.Controls.Add(this.pnl_Body);
            this.Controls.Add(this.panelMain);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.IsMdiContainer = true;
            this.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.MaximizeBox = false;
            this.Name = "MainFrame";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PRC";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainFrame_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainFrame_FormClosed);
            this.Load += new System.EventHandler(this.MainFrame_Load);
            this.panelMain.ResumeLayout(false);
            this.panelMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datagridview)).EndInit();
            this.panel1.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panelMain;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lbStaff;
        private System.Windows.Forms.Label label123;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripSplitButton btnElecs;
        private System.Windows.Forms.ToolStripMenuItem total1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem total2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tSS7ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem c15ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem c16ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem c17ToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton btnSettings;
        private System.Windows.Forms.RadioButton btnAlarm;
        private System.Windows.Forms.RadioButton btnReport;
        private System.Windows.Forms.RadioButton btnEvents;
        private System.Windows.Forms.RadioButton btnBMS;
        private System.Windows.Forms.RadioButton btnSignal;
        private System.Windows.Forms.RadioButton btnElec;
        private System.Windows.Forms.RadioButton btnIndex;
        private System.Windows.Forms.DataGridView datagridview;
        private System.Windows.Forms.Panel pnl_SubBtn;
        private System.Windows.Forms.Panel pnl_Body;
        private System.Windows.Forms.Button btnTimeSync;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.DataGridViewTextBoxColumn EventTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn SystemID;
        private System.Windows.Forms.DataGridViewTextBoxColumn SubsystemID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Location;
        private System.Windows.Forms.DataGridViewTextBoxColumn EquipID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Value;
        private System.Windows.Forms.DataGridViewTextBoxColumn AlarmLevel;
        private System.Windows.Forms.DataGridViewTextBoxColumn AlarmMsg;
        private System.Windows.Forms.DataGridViewTextBoxColumn UserID;
        private System.Windows.Forms.Button btnAbout;
    }
}