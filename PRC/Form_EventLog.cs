﻿using PRC.Tools;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PRC
{
    public partial class Form_EventLog : Form ,IPermissionForm
    {
        public Form_EventLog()
        {
            InitializeComponent();
        
        }

        private void Form_EventLog_Load(object sender, EventArgs e)
        {
            PermissionManager.InitForm(this, new PERMISSION_FORM_ID[] { PERMISSION_FORM_ID.EventLog });
        }

        public void SubMenu_Click<T>(object sender, EventArgs e) where T:class 
        {
            RadioButton rb = (RadioButton)sender;
            if (rb.Checked)
            {
                ClearPnlBody();

                if (typeof(T) == typeof(Form_EventLog_LoginAndLogoutEvent))
                {
                    Form_EventLog_LoginAndLogoutEvent loginEvent = new Form_EventLog_LoginAndLogoutEvent(this._permission);
                    ShowFormInPanel(loginEvent);
                }
                else if (typeof(T) == typeof(Form_EventLog_OperatorEvent))
                {
                    Form_EventLog_OperatorEvent operatorEvent = new Form_EventLog_OperatorEvent(this._permission);
                    ShowFormInPanel(operatorEvent);
                }

            }
        }


        #region Panel_Body Operator 

        private void ShowFormInPanel<TForm>(TForm form)where TForm :Form
        {
       
            form.WindowState = FormWindowState.Maximized;
            form.TopLevel = false;
            pnl_Body.Controls.Add(form);
            form.Show();
        }

        private void ClearPnlBody()
        {
            foreach (Control contorl in pnl_Body.Controls)
            {
                contorl.Dispose();
            }
            pnl_Body.Controls.Clear();
        }

        public void InitByPermission(List<PermissionObject> permissionObjects)
        {
            this._permission = permissionObjects[0];
        }


        #endregion

        #region Field

        private PermissionObject _permission;

        #endregion


    }
}
