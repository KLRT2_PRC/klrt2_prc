﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PRC.MyClass;
using System.Security.Cryptography;
using ICSC.Database.MSSQL;
using System.Net.Http;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Threading;
using PRC.Tools;
using WebApplication1.WebApiResults;

namespace PRC
{
    public partial class Form_Login : Form
    {
        public Form_Login()
        {
            InitializeComponent();
        }

        private int MaxCnt = 5;

        private void Form_Login_Load(object sender, EventArgs e)
        {

        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txbUserID.Text) && !string.IsNullOrEmpty(txbUserPw.Text))
            {
                DoLogin(txbUserID.Text, txbUserPw.Text);
            }
            else
            {
                txbUserID.Text = "";
                txbUserPw.Text = "";
                MessageBox.Show("帳號或密碼不可為空值!");
            }
        }


        private void DoLogin(string userid, string userpw)
        {
            MD5 md5 = MD5.Create();//建立一個MD5
            byte[] source = Encoding.Default.GetBytes(userpw);//將字串轉為Byte[]
            byte[] crypto = md5.ComputeHash(source);//進行MD5加密
            string crypPw = Convert.ToBase64String(crypto);//把加密後的字串從Byte[]轉為字串

            string uritmp = "http://localhost:48965/api/UserInfo?UserID=" + userid + "&UserPw=" + crypPw;
            User authUser = new User() { UserID = userid, UserPw = userpw };
            LoginAuthResult result = WebApiCaller.Json_GetModeByGet<LoginAuthResult>(uritmp).ContentBody;

            if (result.StatusCode == LoginAuthResult.AuthStatus.Success)
            {
                SHM.user = new List<UserInfos>() { result.UserInfo };

                //紀錄登入時間
                LoginRecord login = new LoginRecord();
                login.UserID = userid;
                login.LoginInTime = DateTime.Now;
                login.LoginOutTime = new DateTime(2000, 01, 01);
                login.errRecord.Clear();
                SHM.loginRec.Add(login);
                InsertLoginRec(login.UserID, login.LoginInTime, login.LoginOutTime);


                txbUserID.Text = "";
                txbUserPw.Text = "";
                ((MainFrame)ParentForm).setUserID();
                PermissionManager.InitForm(((MainFrame)ParentForm));
                ((MainFrame)ParentForm).FormReLoad();
            }
            else
            {
                txbUserPw.Text = "";

                if (result.StatusCode == LoginAuthResult.AuthStatus.AccountError)
                {
                    MessageBox.Show("此帳號不存在!");
                }
                else if (result.StatusCode == LoginAuthResult.AuthStatus.PasswordError)
                {
                    if (SHM.loginRec.Count > 0)
                    {
                        var find = from data in SHM.loginRec
                                   where data.UserID == userid
                                   select data;
                        if (find.Count() > 0)
                        {
                            foreach (var item in find)
                            {
                                ErrRecord err = new ErrRecord();
                                item.logCnt++;
                                err.ErrCnt = item.logCnt;
                                err.ErrTime = DateTime.Now;
                                item.errRecord.Add(err);
                                InsertErrRec(userid, err.ErrCnt, err.ErrTime);
                                if (item.logCnt >= MaxCnt)
                                {
                                    MessageBox.Show("你已經失敗" + MaxCnt + "次，帳號鎖定請洽管理員!");
                                    UpdateErrorUser(userid);
                                }
                                else
                                {
                                    MessageBox.Show("密碼輸入錯誤次數：" + err.ErrCnt + "次，還剩下 " + (MaxCnt - err.ErrCnt) + "次");
                                }
                            }

                        }
                        else
                        {
                            LoginRecord errLogin = new LoginRecord();
                            errLogin.logCnt++;
                            errLogin.UserID = userid;
                            ErrRecord err = new ErrRecord();
                            err.ErrCnt = errLogin.logCnt;
                            err.ErrTime = DateTime.Now;
                            errLogin.errRecord.Add(err);
                            SHM.loginRec.Add(errLogin);
                            InsertErrRec(userid, err.ErrCnt, err.ErrTime);
                            MessageBox.Show("密碼輸入錯誤次數：" + err.ErrCnt + "次，還剩下 " + (MaxCnt - err.ErrCnt) + "次");
                        }

                    }
                    else
                    {
                        LoginRecord errLogin = new LoginRecord();
                        errLogin.logCnt++;
                        errLogin.UserID = userid;
                        ErrRecord err = new ErrRecord();
                        err.ErrCnt = errLogin.logCnt;
                        err.ErrTime = DateTime.Now;
                        errLogin.errRecord.Add(err);
                        SHM.loginRec.Add(errLogin);
                        InsertErrRec(userid, err.ErrCnt, err.ErrTime);
                        MessageBox.Show("密碼輸入錯誤次數：" + err.ErrCnt + "次，還剩下 " + (MaxCnt - err.ErrCnt) + "次");
                    }

                }
            }




        }

        private void InsertErrRec(string userid, int errCnt, DateTime errTime)
        {

            string tempMsg = string.Empty;
            StringBuilder sqlSB = new StringBuilder();
            string sqlCmd = string.Empty;
            int sts = 0;
            try
            {

                sqlSB.Append("INSERT INTO ErrorLoginRecord (UserID, ErrCnt, ErrTime ) ");
                sqlSB.Append(" VALUES (");
                sqlSB.Append("'" + userid + "',");
                sqlSB.Append("'" + errCnt + "',");

                sqlSB.Append("'" + errTime.ToString("yyyy/MM/dd HH:mm:ss") + "') ;");

                sqlCmd = sqlSB.ToString();

                sts = DbOp.ExecuteSql(SHM.DBIp, SHM.DBName, SHM.DBUser, SHM.DBPasswd, ref sqlCmd, SHM.LOG_LEVEL_DBDetail, ref tempMsg);

            }
            catch (Exception ex)
            {

            }
            finally
            {

            }
        }

        private void InsertLoginRec(string userid, DateTime LoginIn, DateTime LoginOut)
        {

            string tempMsg = string.Empty;
            StringBuilder sqlSB = new StringBuilder();
            string sqlCmd = string.Empty;
            int sts = 0;
            try
            {

                sqlSB.Append("INSERT INTO Sys_LoginRecord (UserID, LoginInTime, LoginOutTime ) ");
                sqlSB.Append(" VALUES (");
                sqlSB.Append("'" + userid + "',");
                sqlSB.Append("'" + LoginIn.ToString("yyyy/MM/dd HH:mm:ss") + "',");

                sqlSB.Append("'" + LoginOut.ToString("yyyy/MM/dd HH:mm:ss") + "') ;");

                sqlCmd = sqlSB.ToString();

                sts = DbOp.ExecuteSql(SHM.DBIp, SHM.DBName, SHM.DBUser, SHM.DBPasswd, ref sqlCmd, SHM.LOG_LEVEL_DBDetail, ref tempMsg);

            }
            catch (Exception ex)
            {

            }
            finally
            {

            }
        }

        private void UpdateErrorUser(string userid)
        {

            string tempMsg = string.Empty;
            StringBuilder sqlSB = new StringBuilder();
            string sqlCmd = string.Empty;
            int sts = 0;
            try
            {

                StringBuilder sb = new StringBuilder();
                sb.Append("UPDATE Sys_User SET ");

                sb.Append(" UserPrivilege = '0', ");
                sb.Append(" Description = '帳號鎖定' ,");

                sb.Append(" UpdateTime = '" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "'");

                sb.Append(" WHERE UserID = '" + userid + "'");
                sqlCmd = sb.ToString();

                sts = DbOp.ExecuteSql(SHM.DBIp, SHM.DBName, SHM.DBUser, SHM.DBPasswd, ref sqlCmd, SHM.LOG_LEVEL_DBDetail, ref tempMsg);

            }
            catch (Exception ex)
            {

            }
            finally
            {

            }
        }

    }


}
