﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PRC.MyClass;
using ICSC.Database.MSSQL;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Net.Sockets;
using System.Threading;
using PRC;
using PRC.Tools;
using System.Reflection;
using System.Diagnostics;

namespace PRC
{
    public partial class MainFrame : Form, IPermissionForm
    {
        #region Field(5)

        public static Dictionary<Type, Form> DictAllForms = new Dictionary<Type, Form>();

        public static UdpSocketServer _udpserver;
        public static MessageFormat.TSS7 _mtss7;
        public static MessageFormat.C15 _mc15;

        private ICSC.NTP.TimeSyncs.TimeSync _timeSync;

        private System.Timers.Timer _systemTimer;


        #endregion

        public MainFrame()
        {
            InitializeComponent();
        }

        #region Init Method

        #region Init UDP Server

        private void InitUDPServer()
        {
            if (_udpserver == null)
            {
                _udpserver = new UdpSocketServer();
                _udpserver.RecievedData += OnReceiveData;
                _udpserver.Start("10.0.118.117", 8005);
            }
        }

        private void OnReceiveData(object sender, RecieveDataEventArgs e)
        {
            byte[] prcdata = (byte[])e.Data;
            int lgt = prcdata.Length / 2;
            ushort[] data = new ushort[prcdata.Length / 2];
            try
            {
                ITMSMessageFormat itmsFormat = new ITMSMessageFormat(prcdata);
                if (itmsFormat.IsItmsMessage)
                {
                    itmsFormat.NotifyForm();
                }
                else
                {
                    Task.Run(() =>
                    {
                        for (ushort objindex = 0; objindex < lgt; ++objindex)
                        {
                            ushort length = sizeof(UInt16);
                            data[objindex] = (ushort)((ushort)(prcdata[objindex * length] << (ushort)8) + prcdata[objindex * length + 1]);
                        }
                    }).Wait();
                    if (data.Length < 50)
                    {
                        _mc15 = new MessageFormat.C15(data);
                    }
                    else
                    {
                        _mtss7 = new MessageFormat.TSS7(data);
                    }
                }


            }
            catch (Exception ab)
            { }
        }
        #endregion

        private void InitTimeSync()
        {
            if (this._timeSync == null)
            {
                IPAddress address = IPAddress.Parse("10.201.18.202");
                this._timeSync = new ICSC.NTP.TimeSyncs.TimeSync(address, 1);
                this._timeSync.UpdateSystemTime();
            }

        }

        #region InitSystemTimer

        private void InitSystemTimer()
        {
            if (this._systemTimer == null)
            {
                this._systemTimer = new System.Timers.Timer();
                this._systemTimer.Interval = 1000;
                this._systemTimer.Elapsed += _systemTimer_Elapsed;
                this._systemTimer.Start();
            }

        }

        private void _systemTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Action updateUiDelegate = () =>
            {
                if (SHM.user.Count() > 0)
                    lbStaff.Text = SHM.user[0].UserID;
                else
                    lbStaff.Text = "使用者尚未登入";
                label16.Text = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
            };

            if (InvokeRequired)
                Invoke(updateUiDelegate);
            else
                updateUiDelegate();
        }

        #endregion

        private void InitITMSFormMapper()
        {
            ITMSMessageFormat.UnRegistAll();

            //ITMSMessageFormat.RegistItmsForm("C16", (Form_SignalC16)DictAllForms[typeof(Form_SignalC16)]);
        }

        public void InitByPermission(List<PermissionObject> permissionObjects)
        {
            btnAbout.Enabled = true;
            foreach (PermissionObject obj in permissionObjects)
            {
                switch (obj.PermissionCode)
                {
                    case PERMISSION_FORM_ID.Elec:
                        btnElec.Enabled = obj.CanDisplay;
                        break;
                    case PERMISSION_FORM_ID.Signal:
                        btnSignal.Enabled = obj.CanDisplay;
                        break;
                    case PERMISSION_FORM_ID.EventLog:
                        btnEvents.Enabled = obj.CanDisplay;
                        break;
                    case PERMISSION_FORM_ID.Report:
                        btnReport.Enabled = obj.CanDisplay;
                        break;
                    case PERMISSION_FORM_ID.Alarm:
                        btnAlarm.Enabled = obj.CanDisplay;
                        break;
                    case PERMISSION_FORM_ID.Settings:
                        btnSettings.Enabled = obj.CanDisplay;
                        break;
                    case PERMISSION_FORM_ID.BMS:
                        btnBMS.Enabled = obj.CanDisplay;
                        break;
                }

            }
        }

        #endregion

        #region Form Contorl Event

        #region Form 

        private void MainFrame_Load(object sender, EventArgs e)
        {
            CreateMdiChildOrActive<Form_Login>();
        }

        private void MainFrame_FormClosing(object sender, FormClosingEventArgs e)
        {

            //if (SHM.loginRec.Count() > 0)
            //{
            //    var find = from data in SHM.loginRec
            //               where data.UserID == UserInfo.UserID
            //               select data;
            //    foreach (var item in find)
            //    {

            //    }
            //}
        }

        private void MainFrame_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                if (SHM.loginRec.Count() > 0)
                {

                    string tempMsg = string.Empty;
                    StringBuilder sqlSB = new StringBuilder();
                    string sqlCmd = string.Empty;
                    int sts = 0;
                    var find = from data in SHM.loginRec
                               where data.UserID == UserInfo.UserID
                               select data;
                    foreach (var item in find)
                    {
                        try
                        {

                            sqlSB.Append("UPDATE Sys_LoginRecord SET ");

                            sqlSB.Append(" LoginOutTime = '" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "'");

                            sqlSB.Append(" WHERE UserID = '" + item.UserID + "' AND ");
                            sqlSB.Append(" LoginInTime = '" + item.LoginInTime.ToString("yyyy/MM/dd HH:mm:ss") + "'");
                            sqlCmd = sqlSB.ToString();

                            sts = DbOp.ExecuteSql(SHM.DBIp, SHM.DBName, SHM.DBUser, SHM.DBPasswd, ref sqlCmd, SHM.LOG_LEVEL_DBDetail, ref tempMsg);

                        }
                        catch (Exception ex)
                        {

                        }
                        finally
                        {

                        }
                    }
                }
                else
                {
                    string tempMsg = string.Empty;
                    StringBuilder sqlSB = new StringBuilder();
                    string sqlCmd = string.Empty;
                    int sts = 0;
                    try
                    {

                        sqlSB.Append("INSERT INTO Sys_LoginRecord (UserID, LoginInTime, LoginOutTime ) ");
                        sqlSB.Append(" VALUES (");
                        sqlSB.Append("'error',");
                        sqlSB.Append("'" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "',");

                        sqlSB.Append("'" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "') ;");

                        sqlCmd = sqlSB.ToString();

                        sts = DbOp.ExecuteSql(SHM.DBIp, SHM.DBName, SHM.DBUser, SHM.DBPasswd, ref sqlCmd, SHM.LOG_LEVEL_DBDetail, ref tempMsg);

                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                string tempMsg = string.Empty;
                StringBuilder sqlSB = new StringBuilder();
                string sqlCmd = string.Empty;
                int sts = 0;
                try
                {

                    sqlSB.Append("INSERT INTO Sys_LoginRecord (UserID, LoginInTime, LoginOutTime ) ");
                    sqlSB.Append(" VALUES (");
                    sqlSB.Append("'error',");
                    sqlSB.Append("'" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "',");

                    sqlSB.Append("'" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "') ;");

                    sqlCmd = sqlSB.ToString();

                    sts = DbOp.ExecuteSql(SHM.DBIp, SHM.DBName, SHM.DBUser, SHM.DBPasswd, ref sqlCmd, SHM.LOG_LEVEL_DBDetail, ref tempMsg);

                }
                catch (Exception ex1)
                {

                }
                finally
                {

                }
            }


        }

        #endregion

        #region Button 

        private void btnIndex_Click(object sender, EventArgs e)
        {

        }

        //private void btnElec_Click(object sender, EventArgs e)
        //{
        //    CreateMdiChildOrActive<Form_Total1>();
        //}

        private void btnReport_Click(object sender, EventArgs e)
        {
            CreateMdiChildOrActive<Form_Report1>();
        }



        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (SHM.user.Count > 0)
            {
                if (MessageBox.Show("請問是否登出?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    Clear();
                    InitializeComponent();

                    //Resize 
                    this.Height++;
                    this.Height--;

                    CreateMdiChildOrActive<Form_Login>();
                }
            }
            else
            {
                CreateMdiChildOrActive<Form_Login>();
            }

        }

        private void tSS7ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //ShowInitForm("TSS7");
            CreateMdiChildOrActive<Form_ElecTSS7>();
        }

        private void c15ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CreateMdiChildOrActive<Form_StationC15>();
        }

        private void c16ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CreateMdiChildOrActive<Form_StationC16>();
        }

        private void c17ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CreateMdiChildOrActive<Form_StationC17>();
        }

        private void total1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CreateMdiChildOrActive<Form_Total1>();
        }

        private void total2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CreateMdiChildOrActive<Form_Total2>();
        }



        private void btnTimeSync_Click(object sender, EventArgs e)
        {
            this._timeSync.UpdateSystemTime();

            string strMsg = "";

            //判斷同步後是不是有值
            if (this._timeSync.LastUpdateTime.HasValue)
                strMsg = string.Format(@"同步完成，目前時間為 : {0}", this._timeSync.LastUpdateTime);
            else
                strMsg = "同步失敗";

            MessageBox.Show(strMsg);
        }



        #endregion

        #region Menu Button

        #region BtnElec
        private void btnElec_Click(object sender, EventArgs e)
        {
            ResetPnl_SubPanel(new List<SubPanelButtonSetting>() {
                new SubPanelButtonSetting() {
                    Click=CreatePSS_OverAll_Form,
                    Text="整體圖",
                    IsDefaultClick=true,
                },
                new SubPanelButtonSetting(){
                    Click = CreatePSS_TSS_Form,
                    Text = "變電站",
                },
                new SubPanelButtonSetting(){
                    Click = CreatePSS_Station_Form,
                    Text = "車站",
                }
            });
        }
        private void CreatePSS_OverAll_Form(object sender, EventArgs e)
        {
            CreateMdiChildOrActive<Form_ElecOverAllBase>();
        }
        private void CreatePSS_TSS_Form(object sender, EventArgs e)
        {
            CreateMdiChildOrActive<Form_ElecTSSBase>();
        }
        private void CreatePSS_Station_Form(object sender, EventArgs e)
        {
            CreateMdiChildOrActive<Form_ElecStationBase>();
        }
        #endregion

        #region BtnSignal

        private void btnSignal_Click(object sender, EventArgs e)
        {
            // CreateMdiChildOrActive<Form_SignalC16>();
            ResetPnl_SubPanel(new List<SubPanelButtonSetting>() {
                new SubPanelButtonSetting() {
                    Click = CreateITMS_Station_Form ,
                    Text = "車站",
                    IsDefaultClick = true
                },
               new SubPanelButtonSetting() {
                    Click = CreateITMS_Train_Form ,
                       Text = "車輛",
                },

            });
        }
        private void CreateITMS_Station_Form(object sender, EventArgs e)
        {
            CreateMdiChildOrActive<Form_SignalStationBase>();
        }
        private void CreateITMS_Train_Form(object sender, EventArgs e)
        {
            CreateMdiChildOrActive<Form_SignalTrainBase>();
        }
        #endregion

        #region BtnBMS
        private void btnBMS_Click(object sender, EventArgs e)
        {
            ResetPnl_SubPanel(new List<SubPanelButtonSetting>() {
                new SubPanelButtonSetting(){
                    Click = CreateBMS_PowerSupply_Form,
                    Text = "車站及駐車場區AC220V供電",
                },
                new SubPanelButtonSetting(){
                    Click = CreateBMS_Light_Form,
                    Text = "車站照明",
                },
                new SubPanelButtonSetting(){
                    Click = CreateBMS_AirCondition01_Form,
                    Text = "排氣機與空調機01",
                },
                new SubPanelButtonSetting(){
                    Click = CreateBMS_AirCondition02_Form,
                    Text = "排氣機與空調機02",
                }
            });
        }
        private void CreateBMS_PowerSupply_Form(object sender, EventArgs e)
        {
            CreateMdiChildOrActive<Form_BMS>();
        }
        private void CreateBMS_Light_Form(object sender, EventArgs e)
        {
            CreateMdiChildOrActive<Form_BMSlight>();
        }
        private void CreateBMS_AirCondition01_Form(object sender, EventArgs e)
        {
            CreateMdiChildOrActive<Form_BMS2>();
        }
        private void CreateBMS_AirCondition02_Form(object sender, EventArgs e)
        {
            CreateMdiChildOrActive<Form_BMS2_1>();
        }
        #endregion

        #region BtnEvent
        private void btnEvents_Click(object sender, EventArgs e)
        {
            Form_EventLog form = CreateMdiChildOrActive<Form_EventLog>();
            ResetPnl_SubPanel(new List<SubPanelButtonSetting>() {
                new SubPanelButtonSetting() {
                    Click =(obj,ev)=> {
                        form.SubMenu_Click<Form_EventLog_LoginAndLogoutEvent>(obj,e);
                    },
                    Text = "登入/登出紀錄",
                    IsDefaultClick = true
                },
               new SubPanelButtonSetting() {
                   Click =(obj,ev)=> {
                       form.SubMenu_Click<Form_EventLog_OperatorEvent>(obj,e);
                    },
                   Text = "使用者操作紀錄"
               },

            });
        }
        #endregion

        #region BtnAlarm

        private void btnAlarm_Click(object sender, EventArgs e)
        {
            Form_AlarmSystem form = CreateMdiChildOrActive<Form_AlarmSystem>("PSS_BMS_ITMS");
            ResetPnl_SubPanel(new List<SubPanelButtonSetting>() {
                new SubPanelButtonSetting() {
                    Text = "警報查詢",
                    Click = form.btn_AlarmSearch_Click,
                    IsDefaultClick = true
                },
               new SubPanelButtonSetting() {
                   Text = "警報設定查詢",
                   Click = form.btn_AlarmSettingSearch_Click,
               },
                 new SubPanelButtonSetting() {
                   Text = "警報設定",
                   Click =  form.btnAlarmSetting_Click,
               },
            });
        }

        #endregion

        #region BtnSetting

        private void btnSettings_Click(object sender, EventArgs e)
        {
            Form_Settings form = CreateMdiChildOrActive<Form_Settings>();
            form.ResetSettings();
            ResetPnl_SubPanel(new List<SubPanelButtonSetting>() {
                new SubPanelButtonSetting() {
                    Text = "使用者管理",
                    Click = form.btnUserMgt_Click,
                    IsDefaultClick = true,
                    Disable = !form.UserMgtPermission.CanDisplay
                },
               new SubPanelButtonSetting() {
                   Text = "部門管理",
                   Click = form.btnDeptMgt_Click,
                  Disable = !form.DeptMgtPermission.CanDisplay
               },
                 new SubPanelButtonSetting() {
                   Text = "群組管理",
                   Click =  form.btnGroupMgt_Click,
                   Disable = !form.GroupPermission.CanDisplay
               },
                  new SubPanelButtonSetting() {
                   Text = "群組系統程式管理",
                   Click =  form.btnGroupProg_Click,
                   Disable = !form.GroupProgPermission.CanDisplay
               },
            });
        }

        #endregion

        #region BtnAbout
        private void btnAbout_Click(object sender, EventArgs e)
        {
            //Assembly _assembly = Assembly.GetExecutingAssembly();
            //FileVersionInfo fvi=Fil 
            //CreateMdiChildOrActive<Form_About>();
            Form_About fa = new Form_About();
            fa.Activate();
            fa.ShowDialog();
        }
        #endregion

        private void menu_CheckedChanged(object sender, EventArgs e)
        {
            if (!(sender as RadioButton).Checked)
            {
                pnl_SubBtn.Controls.Clear();
                pnl_Body.Controls.Clear();
            }
        }

        #endregion

        #endregion

        #region Method

        public void FormReLoad()
        {
            this.Cursor = Cursors.WaitCursor;

            InitSystemTimer();

            //InitTimeSync();

            //InitChildForm();

            InitITMSFormMapper();

            InitUDPServer();


            this.Cursor = Cursors.Default;
        }

        public void setUserID()
        {
            lbStaff.Text = UserInfo.UserID;
        }

        public void setUserIDNone()
        {
            lbStaff.Text = "";
        }

        public T CreateMdiChildOrActive<T>() where T : Form, new()
        {
            Form instance;
            DictAllForms.TryGetValue(typeof(T), out instance);
            if (instance == null || instance.IsDisposed)
            {
                instance = new T();
                DictAllForms[typeof(T)] = instance;
            }
                InitFormInstance(instance);

            return (T)instance;
        }

        public T CreateMdiChildOrActive<T>(params object[] parameters) where T : Form
        {
            Form instance;
            DictAllForms.TryGetValue(typeof(T), out instance);
            if (instance == null || instance.IsDisposed)
            {
                instance = (Form)Activator.CreateInstance(typeof(T), parameters);
                DictAllForms[typeof(T)] = instance;
            }

            InitFormInstance(instance);

            return (T)instance;
        }

        /// <summary>
        /// 在主畫面上顯示供電子表單
        /// </summary>
        /// <param name="formCode"></param>
        public void ShowElecChildFormInMainFrame(string formCode)
        {
            switch (formCode)
            {
                case "TSS7":
                    SHM.Frm_mf.CreateMdiChildOrActive<Form_ElecTSS7>();
                    break;
                case "TSS8":
                    SHM.Frm_mf.CreateMdiChildOrActive<Form_ElecTSS8>();
                    break;
                case "TSS9":
                    SHM.Frm_mf.CreateMdiChildOrActive<Form_ElecTSS9>();
                    break;
            }

        }

        private void InitFormInstance(Form instance)
        {
            pnl_Body.Controls.Clear();
            instance.TopLevel = false;
            pnl_Body.Controls.Add(instance);

            instance.WindowState = FormWindowState.Maximized;

            instance.Show();
        }

        /// <summary>
        /// 重置子按鈕容器
        /// </summary>
        /// <param name="btnList">子按鈕設定</param>
        private void ResetPnl_SubPanel(List<SubPanelButtonSetting> btnList)
        {
            pnl_SubBtn.Controls.Clear();

            int totalXpoint = 10;

            for (int index = 0; index < btnList.Count(); index++)
            {
                SubPanelButtonSetting btnSetting = btnList[index];

                RadioButton rb = new RadioButton();

                //外觀設置
                rb.Appearance = Appearance.Button;
                rb.FlatStyle = FlatStyle.Flat;
                rb.Enabled = !btnSetting.Disable;

                ///字型設置
                rb.Text = btnSetting.Text;
                rb.Font = new Font("微軟正黑體", 14, FontStyle.Regular);
                rb.TextAlign = ContentAlignment.MiddleCenter;

                //位置、大小設置
                rb.AutoSize = true;
                int xPoint = (totalXpoint);
                int yPoint = (pnl_SubBtn.Height - rb.Height) / 2; //確保按鈕在正中央

                rb.Location = new Point(xPoint, yPoint);


                //顏色設置
                if (rb.Enabled)
                    rb.BackColor = Color.Transparent;
                else
                    rb.BackColor = Color.FromArgb(25,233,233,233);

                rb.FlatAppearance.CheckedBackColor = Color.Gray;
                rb.ForeColor = Color.Lime;

                //判斷是否預設點擊
                if (btnSetting.IsDefaultClick)
                {
                    rb.Checked = btnSetting.IsDefaultClick;
                    btnSetting.Click?.Invoke(rb, new EventArgs());
                }



                //按鈕狀態變換為True時，觸發特定事件
                rb.CheckedChanged += (obj, e) =>
                {
                    RadioButton sender = obj as RadioButton;
                    if (sender.Checked)
                    {
                        btnSetting.Click?.Invoke(sender, e);
                    }
                };


                pnl_SubBtn.Controls.Add(rb);

                totalXpoint = xPoint + rb.Width + 10;
            }

        }

        private void Clear()
        {
            //清除使用者
            SHM.user.Clear();

            //清空表單對應表
            DictAllForms.Clear();

            //清空MDI Child
            foreach (Form form in this.MdiChildren)
            {
                form.Dispose();
                form.Close();
            }

            //清空控制項
            foreach (Control control in this.Controls)
            {
                control.Dispose();
            }

            this.Controls.Clear();


        }


        #endregion


    }

    public class UdpSocketServer
    {
        private Thread mListenThread;
        private int mRecieverBuffer = 1024 * 12;
        private Socket mSocket;
        #region Properties 
        public string Ip
        {
            get;
            set;
        }

        public int Port
        {
            get;
            set;
        }

        public int RecieverBuffer
        {
            get { return mRecieverBuffer; }
            set { mRecieverBuffer = value; }
        }

        #endregion Properties 

        #region Delegates and Events (3) 

        // Events (3) 

        public event EventHandler<RecieveDataEventArgs> RecievedData;

        public event EventHandler StartListening;

        public event EventHandler StopListening;

        #endregion Delegates and Events 

        #region Methods (4) 

        // Public Methods (3) 

        public void Start()
        {
            if (StartListening != null)
            {
                StartListening(this, new EventArgs());
            }

            mListenThread = new Thread(new ParameterizedThreadStart(startListen));
            mListenThread.IsBackground = true;
            mListenThread.Start();
        }

        public void Start(string ip, int port)
        {
            this.Ip = ip;
            this.Port = port;

            Start();
        }

        public void Stop()
        {
            if (StopListening != null)
            {
                StopListening(this, new EventArgs());
            }

            mSocket.Close();
            mListenThread.Abort();
        }
        // Private Methods (1) 

        private void startListen(object sender)
        {
            //Setting Endpoint
            IPEndPoint endpoint = new IPEndPoint(IPAddress.Any, Port);
            mSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

            //Binding Endpoint
            mSocket.Bind(endpoint);

            //Getting Client Ip
            IPEndPoint clientEndpoint = new IPEndPoint(IPAddress.Any, 0);
            EndPoint Remote = (EndPoint)(clientEndpoint);

            //Start loop for receiving data
            while (true)
            {
                try
                {
                    int recv;
                    byte[] receivePackage = new byte[mRecieverBuffer];

                    //Receive data from client
                    recv = mSocket.ReceiveFrom(receivePackage, ref Remote);

                    //Deserialize data
                    BinaryFormatter bf = new BinaryFormatter();
                    MemoryStream stream = new MemoryStream(receivePackage);
                    object data = bf.Deserialize(stream);

                    if (RecievedData != null)
                    {
                        RecievedData(this, new RecieveDataEventArgs(data));
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }
        #endregion Methods 
    }

    public class RecieveDataEventArgs : EventArgs
    {
        #region Fields (1) 

        protected object mData;

        #endregion Fields 

        #region Constructors (1) 

        public RecieveDataEventArgs(object data)
        {
            this.mData = data;
        }

        #endregion Constructors 

        #region Properties (1) 

        public object Data
        {
            get
            {
                return mData;
            }
        }

        #endregion Properties 
    }

    public class SubPanelButtonSetting
    {

        public string Text { get; set; }
        public string ToolTipMessage { get; set; }
        public bool IsDefaultClick { get; set; }
        public bool Disable { get; set; }

        public ClickFunc Click;
        public delegate void ClickFunc(RadioButton sender, EventArgs e);
    }

}
