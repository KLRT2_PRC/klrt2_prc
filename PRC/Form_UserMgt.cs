﻿using ICSC.Database.MSSQL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PRC.MyClass;
using System.Collections;
using System.Security.Cryptography;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using PRC.Tools;
using System.IO;

namespace PRC
{
    public partial class Form_UserMgt : Form
    {
        public Form_UserMgt()
        {
            InitializeComponent();
            initPrivilegeItem();
            getDeptInfo();
        }

        public Form_UserMgt(PermissionObject permission):this()
        {

            btnUserMgtR.Enabled = permission.CanRead;
            btnUserMgtI.Enabled = permission.CanCreate;
            btnUserMgtU.Enabled = permission.CanUpdate;
            btnUserMgtD.Enabled = permission.CanDelete;
        }


        private void initPrivilegeItem()
        {

            ComboBoxItem item = new ComboBoxItem();
            item.Text = "停用";
            item.Value = 0;
            cmbUserPrivilege.Items.Add(item);
            item = new ComboBoxItem();
            item.Text = "啟用";
            item.Value = 1;
            cmbUserPrivilege.Items.Add(item);
            //item = new CheckBoxItem();
            //item.Text = "刪除留存";
            //item.Value = 2;
            //cmbUserPrivilege.Items.Add(item);
            cmbUserPrivilege.SelectedIndex = 1;

            //cmbDept.DataSource = getDept();
            //cmbDept.DisplayMember = "DeptID";
            //cmbDept.ValueMember = "SerialNo";


        }



        private void Form_UserMgt_Load(object sender, EventArgs e)
        {

            this._isAdmin = CheckAdmin();

            if (this._isAdmin)
            {
                getUserData("");
            }
            else
            {
                getUserData(SHM.user[0].UserID);
                userClose();
            }
        }


        private void getUserData(string UserID)
        {
            List<User> userData = WebApiCaller.Json_GetModeByGet<List<User>>(@"http://localhost:48965/api/UserInfo?UserID=" + UserID).ContentBody;

            UpdateDataGridViewDatasource(userData);

        }

        void userClose()
        {
            txbUserID.Enabled = false;
            cmbUserPrivilege.Enabled = false;
            cmbDept.Enabled = false;
            txbDesc.Enabled = false;
            txbUserName.Enabled = false;

        }

        public void setDept()
        {
            getDeptInfo();
        }

        /// <summary>
        /// 取得部門資訊
        /// </summary>
        private void getDeptInfo()
        {

            List<Dept> DeptData = WebApiCaller.Json_GetModeByGet<List<Dept>>(@"http://localhost:48965/api/DeptInfo").ContentBody;

            //string responseBody = await response.Content.ReadAsStringAsync();
            //DeptData = JsonConvert.DeserializeObject<List<Dept>>(responseBody);

            Dept dept1 = new Dept();
            dept1.DeptID = "請選擇";
            DeptData.Insert(0, dept1);


            cmbDept.DataSource = DeptData.ToArray();
            cmbDept.DisplayMember = "DeptID";
            cmbDept.ValueMember = "DeptID";


            cmbSDeptID.DataSource = DeptData.ToArray();
            cmbSDeptID.DisplayMember = "DeptID";
            cmbSDeptID.ValueMember = "DeptID";

        }


        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                txbUserID.Text = string.Format("{0}", dataGridView1.Rows[e.RowIndex].Cells["UserID"].Value);
                txbUserName.Text = string.Format("{0}", dataGridView1.Rows[e.RowIndex].Cells["UserName"].Value);
                //txbUserPw.Text = dataGridView1.Rows[e.RowIndex].Cells["UserPw"].Value.ToString();
                txbEmail.Text = string.Format("{0}", dataGridView1.Rows[e.RowIndex].Cells["Email"].Value);
                txbPhone.Text = string.Format("{0}", dataGridView1.Rows[e.RowIndex].Cells["Phone"].Value);
                txbDesc.Text = string.Format("{0}", dataGridView1.Rows[e.RowIndex].Cells["Description"].Value);
                cmbUserPrivilege.SelectedIndex = (int)dataGridView1.Rows[e.RowIndex].Cells["UserPrivilege"].Value;

                //部門
                if (dataGridView1.Rows[e.RowIndex].Cells["DeptID"].Value == null)
                    cmbDept.SelectedIndex = 0;
                else
                    cmbDept.SelectedValue = string.Format("{0}", dataGridView1.Rows[e.RowIndex].Cells["DeptID"].Value);

                txbUserID.Enabled = false;
            }
        }

        private void btnInsert_Click(object sender, EventArgs e)
        {

            #region "舊新增方法"
            //StringBuilder logSB = new StringBuilder();
            //StringBuilder eventSB = new StringBuilder();
            //string tempMsg = string.Empty;
            //StringBuilder sqlSB = new StringBuilder();
            //string sqlCmd = string.Empty;         
            //int sts = 0;
            //try
            //{

            //    sqlSB.Append("INSERT INTO Sys_User (UserID, UserName, UserPw, DeptID, Email, Phone , UserPrivilege, UpdateUserID, InsertUserID, InsertTime, UpdateTime, Description ) ");
            //    sqlSB.Append(" VALUES (");
            //    sqlSB.Append("'" + txbUserID.Text + "',");
            //    sqlSB.Append("'" + txbUserName.Text + "',");
            //    sqlSB.Append("'" + pwMd5 + "',");
            //    sqlSB.Append("'" + cmbDept.SelectedValue.ToString() + "',");
            //    sqlSB.Append("'" + txbEmail.Text + "',");
            //    sqlSB.Append("'" + txbPhone.Text + "',");
            //    sqlSB.Append("'" + item.Value.ToString() + "',");
            //    sqlSB.Append("'" + SHM.Staff + "',");
            //    sqlSB.Append("'" + SHM.Staff + "',");
            //    sqlSB.Append("'" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "',");
            //    sqlSB.Append("'" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "',");
            //    sqlSB.Append("'" + txbDesc.Text + "') ;");

            //    sqlCmd = sqlSB.ToString();

            //    sts = DbOp.ExecuteSql(SHM.DBIp, SHM.DBName, SHM.DBUser, SHM.DBPasswd, ref sqlCmd, SHM.LOG_LEVEL_DBDetail, ref tempMsg);

            //    if (sts > 0)
            //    {
            //        MessageBox.Show("新增成功");
            //        getData("");
            //        dataGridView1.DataSource = SHM.UserData;
            //    }

            //    logSB.AppendLine(tempMsg);

            //}
            //catch (Exception ex)
            //{
            //    eventSB.AppendLine("Exception: " + ex.Message);
            //}
            //finally
            //{
            //    if (eventSB.Length > 0)
            //        SHM.QueueEvent.Enqueue(eventSB.ToString());
            //    if (logSB.Length > 0)
            //        SHM.QueueDatabase.Enqueue(logSB.ToString());
            //}
            #endregion
            if (string.IsNullOrEmpty(txbUserID.Text.Trim()) && string.IsNullOrEmpty(txbUserPw.Text.Trim()) && string.IsNullOrEmpty(txbUserName.Text.Trim()))
            {
                MessageBox.Show("職工編號、密碼、姓名不得為空");
                WriteLog.WriteLogByWebApi(this.Name, "Insert", EventLog_OperatorNotifyDto.LogType.ERROR);
            }
            else
            {

                MD5 md5 = MD5.Create();
                string pwMd5 = Convert.ToBase64String(md5.ComputeHash(Encoding.Default.GetBytes(txbUserPw.Text)));
                ComboBoxItem item = (ComboBoxItem)cmbUserPrivilege.SelectedItem;
                User userData = new User();
                userData.UserID = txbUserID.Text;
                userData.UserName = txbUserName.Text;
                userData.UserPw = pwMd5;

                //判斷使用者是否有要順便宣告部門
                if (cmbDept.SelectedIndex > 0)
                    userData.DeptID = cmbDept.SelectedValue as string;

                userData.Email = txbEmail.Text;
                userData.Phone = txbPhone.Text;
                userData.UserPrivilege = Convert.ToInt32(item.Value);
                userData.InsertUserID = SHM.user[0].UserID;
                userData.UpdateUserID = SHM.user[0].UserID;
                userData.InsertTime = DateTime.Now;
                userData.UpdateTime = DateTime.Now;
                userData.Description = txbDesc.Text;
                Insert(userData);
                clearInput();
                WriteLog.WriteLogByWebApi(this.Name, "Insert", EventLog_OperatorNotifyDto.LogType.INFO);
            }



        }
        private void Insert(User user)
        {
            var response = WebApiCaller.Srting_GetMessageByPost("http://localhost:48965/api/UserInfo", user);

            if (response.IsSuccessStatusCode)
            {
                getUserData(user.UserID);
            }

            MessageBox.Show(response.ContentBody);

        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {

            #region "舊方法"
            //StringBuilder logSB = new StringBuilder();
            //StringBuilder eventSB = new StringBuilder();
            //string tempMsg = string.Empty;
            //StringBuilder sqlSB = new StringBuilder();
            //string sqlCmd = string.Empty;

            //int sts = 0;
            //try
            //{

            //    if (SHM.IsAdmin)
            //    {

            //    }
            //    else
            //    {

            //    }
            //    sqlSB.Append("UPDATE Sys_User SET ");
            //    sqlSB.Append(" UserName = '" + txbUserName.Text + "' ,");
            //    sqlSB.Append(" UserPw = '" + pwMd5 + "' ,");
            //    sqlSB.Append(" Email = '" + txbEmail.Text + "' ,");
            //    sqlSB.Append(" Phone = '" + txbPhone.Text + "' ,");
            //    sqlSB.Append(" DeptID = '" + cmbDept.SelectedValue.ToString() + "' ,");
            //    sqlSB.Append(" UserPrivilege = '" + item.Value.ToString() + "', ");
            //    sqlSB.Append(" Description = '" + txbDesc.Text + "' ,");
            //    sqlSB.Append(" UpdateUserID = '" + SHM.Staff + "' ,");
            //    sqlSB.Append(" UpdateTime = '" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "'");

            //    sqlSB.Append(" WHERE UserID = '" + txbUserID.Text + "'");


            //    sqlCmd = sqlSB.ToString();

            //    sts = DbOp.ExecuteSql(SHM.DBIp, SHM.DBName, SHM.DBUser, SHM.DBPasswd, ref sqlCmd, SHM.LOG_LEVEL_DBDetail, ref tempMsg);

            //    if (sts > 0)
            //    {
            //        MessageBox.Show("修改成功");
            //        getData("");
            //        dataGridView1.DataSource = SHM.UserData;
            //    }

            //    logSB.AppendLine(tempMsg);



            //}
            //catch (Exception ex)
            //{
            //    eventSB.AppendLine("Exception: " + ex.Message);
            //}
            //finally
            //{
            //    if (eventSB.Length > 0)
            //        SHM.QueueEvent.Enqueue(eventSB.ToString());
            //    if (logSB.Length > 0)
            //        SHM.QueueDatabase.Enqueue(logSB.ToString());
            //}
            #endregion

            //判斷是否有權修改密碼
            if (this._isAdmin || SHM.user[0].UserID.Equals(txbUserID.Text))
            {
                ComboBoxItem item = (ComboBoxItem)cmbUserPrivilege.SelectedItem;
                User userData = new User();
                userData.UserID = txbUserID.Text;
                userData.UserName = txbUserName.Text;

                //依照使用者有沒有輸入密碼判斷其是否要修改密碼
                if (!string.IsNullOrEmpty(txbUserPw.Text))
                {
                    MD5 md5 = MD5.Create();
                    string pwMd5 = Convert.ToBase64String(md5.ComputeHash(Encoding.Default.GetBytes(txbUserPw.Text)));
                    userData.UserPw = pwMd5;
                }

                //判斷使用者是否要擁有部門關係
                if (cmbDept.SelectedIndex > 0)
                    userData.DeptID = cmbDept.SelectedValue as string;

                userData.Email = txbEmail.Text;
                userData.Phone = txbPhone.Text;
                userData.UserPrivilege = Convert.ToInt32(item.Value);
                userData.UpdateUserID = SHM.user[0].UserID;
                userData.UpdateTime = DateTime.Now;
                userData.Description = txbDesc.Text;
                UpdateData(userData, false);
                clearInput();
                WriteLog.WriteLogByWebApi(this.Name, "Update", EventLog_OperatorNotifyDto.LogType.INFO);
            }
            else
            {
                MessageBox.Show("您沒有此權限");
            }

        }
        private void UpdateData(User user, bool isAdm)
        {
            var response = WebApiCaller.Srting_GetMessageByPut("http://localhost:48965/api/UserInfo", user);

            if (response.IsSuccessStatusCode)
            {
                getUserData(user.UserID);

            }

            MessageBox.Show(response.ContentBody);

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {

            #region "舊"
            //StringBuilder logSB = new StringBuilder();
            //StringBuilder eventSB = new StringBuilder();
            //string tempMsg = string.Empty;
            //StringBuilder sqlSB = new StringBuilder();
            //string sqlCmd = string.Empty;

            //int sts = 0;
            //try
            //{

            //    sqlSB.Append("UPDATE Sys_User SET UserPrivilege = 2, ");
            //    sqlSB.Append(" RetireTime = '" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "' ,");                
            //    sqlSB.Append(" UpdateUserID = '" + SHM.Staff + "' ");
            //    sqlSB.Append(" WHERE UserID = '" + txbUserID.Text + "'");
            //    //sqlSB.Append("DELETE FROM Sys_User WHERE UserID = '" + txbUserID.Text + "'");

            //    sqlCmd = sqlSB.ToString();

            //    sts = DbOp.ExecuteSql(SHM.DBIp, SHM.DBName, SHM.DBUser, SHM.DBPasswd, ref sqlCmd, SHM.LOG_LEVEL_DBDetail, ref tempMsg);

            //    if (sts > 0)
            //    {
            //        MessageBox.Show("刪除成功");
            //        getData("");
            //        dataGridView1.DataSource = SHM.UserData;
            //    }

            //    logSB.AppendLine(tempMsg);

            //}
            //catch (Exception ex)
            //{
            //    eventSB.AppendLine("Exception: " + ex.Message);
            //}
            //finally
            //{
            //    if (eventSB.Length > 0)
            //        SHM.QueueEvent.Enqueue(eventSB.ToString());
            //    if (logSB.Length > 0)
            //        SHM.QueueDatabase.Enqueue(logSB.ToString());
            //}
            //clearInput();
            #endregion

            if (MessageBox.Show("是否刪除該使用者?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                User userData = new User();
                userData.UserID = txbUserID.Text;

                userData.UpdateUserID = SHM.user[0].UserID;
                userData.RetireTime = DateTime.Now;
                DeleteData(userData);
                txbUserID.Enabled = true;
                clearInput();
                WriteLog.WriteLogByWebApi(this.Name, string.Format("Delete user : {0}", userData.UserID), EventLog_OperatorNotifyDto.LogType.INFO);
            }
        }
        private void DeleteData(User user)
        {
            string url = @"http://localhost:48965/api/UserInfo?Userid={0}";
            var response = WebApiCaller.Srting_GetMessageByDelete(string.Format(url, user.UserID));

            if (response.IsSuccessStatusCode)
            {
                getUserData("");

            }

            MessageBox.Show(response.ContentBody);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (this._isAdmin)
            {
                clearInput();
                txbUserID.Enabled = true;
            }
            else
            {
                txbUserPw.Text = "";
                txbPhone.Text = "";
            }
     

        }

        private void UpdateDataGridViewDatasource(List<User> userList)
        {
            List<UserInfoViewModel> viewList = new List<UserInfoViewModel>();
            //取得部門資訊
            Dept[] deptInfos = cmbDept.DataSource as Dept[];

            foreach (User user in userList)
            {
                UserInfoViewModel viewModel = new UserInfoViewModel()
                {
                    UserID = user.UserID,
                    UserName = user.UserName,
                    UserPrivilege = user.UserPrivilege,
                    UserPw = user.UserPw,
                    DeptID = user.DeptID,
                    Description = user.Description,
                    Email = user.Email,
                    Phone = user.Phone,
                    InsertTime = user.InsertTime,
                    InsertUserID = user.InsertUserID,
                    UpdateTime = user.UpdateTime,
                    UpdateUserID = user.UpdateUserID,
                    RetireTime = user.RetireTime
                };
                foreach (Dept dept in deptInfos)
                {
                    if (dept.DeptID.Equals(viewModel.DeptID))
                    {
                        viewModel.DeptName = user.DeptID = dept.DeptID;
                        break;
                    }
                }

                if (viewModel.DeptName == null)
                    viewModel.DeptName = "無";

                viewList.Add(viewModel);
            }

            dataGridView1.DataSource = viewList;

        }

        public static string GetMD5(string original)
        {
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            byte[] b = md5.ComputeHash(Encoding.UTF8.GetBytes(original));
            return BitConverter.ToString(b).Replace("-", string.Empty);
        }

        private void btnUserMgtR_Click(object sender, EventArgs e)
        {
            string selectValue = null;

            //判斷使用者是否要設定部門條件進行查詢
            selectValue = cmbSDeptID.SelectedIndex == 0 ? "" : string.Format("{0}", cmbSDeptID.SelectedValue);

            getUserData(txbSUserID.Text, txbSUserName.Text, selectValue);

            WriteLog.WriteLogByWebApi(this.Name, "Query", EventLog_OperatorNotifyDto.LogType.INFO);
        }

 

        private async void getUserData(string UserID, string UserName, string DeptID)
        {
            string baseAddr = "http://localhost:48965/";
            List<User> userData = new List<User>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseAddr);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                string uriStr = "api/UserInfo?UserID=" + UserID + "&UserName=" + UserName + "&DeptID=" + DeptID;
                HttpResponseMessage response = await client.GetAsync(uriStr);
                if (response.IsSuccessStatusCode)
                {
                    userData = await response.Content.ReadAsAsync<List<User>>();
                }

                UpdateDataGridViewDatasource(userData);
                // dataGridView1.DataSource = userData;
                MessageBox.Show("查詢成功");
            }
        }

        private void getData(string staff)
        {
            StringBuilder logSB = new StringBuilder();
            StringBuilder eventSB = new StringBuilder();
            string tempMsg = string.Empty;
            StringBuilder sqlSB = new StringBuilder();
            string sqlCmd = string.Empty;
            DataTable dt = new DataTable();
            int sts = 0;
            try
            {

                sqlSB.Append("SELECT * FROM Sys_User where UserPrivilege <> 2");
                if (!string.IsNullOrEmpty(staff))
                    sqlSB.Append(" and USERID  = '" + staff + "' ");
                sqlCmd = sqlSB.ToString();

                sts = DbOp.QuerySql(SHM.DBIp, SHM.DBName, SHM.DBUser, SHM.DBPasswd, ref sqlCmd, ref dt, SHM.LOG_LEVEL_DBDetail, ref tempMsg);
                if (sts > 0)
                {

                    SHM.UserData = dt;
                }

                logSB.AppendLine(tempMsg);



            }
            catch (Exception ex)
            {
                eventSB.AppendLine("Exception: " + ex.Message);
            }
            finally
            {
                if (eventSB.Length > 0)
                    SHM.QueueEvent.Enqueue(eventSB.ToString());
                if (logSB.Length > 0)
                    SHM.QueueDatabase.Enqueue(logSB.ToString());
            }

        }
   
        private bool CheckAdmin()
        {
            bool isAdmin = false;
            foreach (GroupInfo gInfo in SHM.user[0].GroupInfo)
            {
                if (gInfo.GroupID == "1")
                {
                    isAdmin = true;
                    break;
                }
            }

            return isAdmin;
        }

        private void clearInput()
        {
            txbUserID.Text = "";
            txbUserName.Text = "";
            txbUserPw.Text = "";
            txbEmail.Text = "";
            txbPhone.Text = "";
            txbDesc.Text = "";
            cmbUserPrivilege.SelectedIndex = 1;
            //cmbDept.SelectedIndex = 0;
        }


        private void button_EnabledChanged(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            if (btn.Enabled)
                btn.FlatStyle = FlatStyle.Flat;
            else
                btn.FlatStyle = FlatStyle.Standard;
        }



        private bool _isAdmin = false;

 
    }

    public class UserInfoViewModel : User
    {
        public string DeptName { get; set; }
    }
}
