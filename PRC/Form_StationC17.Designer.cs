﻿namespace PRC
{
    partial class Form_StationC17
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.circleUC47 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.lineUC37 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC36 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC35 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC4 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.circleUC43 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.label15 = new System.Windows.Forms.Label();
            this.lineUC24 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC25 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC27 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC28 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC31 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC32 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC34 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label6 = new System.Windows.Forms.Label();
            this.circleUC37 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC38 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lineUC5 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC14 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC15 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.circleUC39 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC40 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.lineUC16 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC17 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC18 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC19 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC20 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC21 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC22 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.circleUC41 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.lineUC23 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.switch05UC2 = new iSCADA.Design.Utilities.Electrical.Switch05UC();
            this.retangleUC2 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label5 = new System.Windows.Forms.Label();
            this.circleUC36 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC4 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lineUC13 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC12 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC11 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.circleUC3 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC2 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.lineUC10 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC9 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC8 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC7 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC6 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC4 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC3 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.circleUC1 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.lineUC2 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.switch05UC1 = new iSCADA.Design.Utilities.Electrical.Switch05UC();
            this.lineUC1 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label110 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.label112 = new System.Windows.Forms.Label();
            this.label113 = new System.Windows.Forms.Label();
            this.label114 = new System.Windows.Forms.Label();
            this.label116 = new System.Windows.Forms.Label();
            this.label118 = new System.Windows.Forms.Label();
            this.label120 = new System.Windows.Forms.Label();
            this.label122 = new System.Windows.Forms.Label();
            this.label124 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.retangleUC14 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.retangleUC13 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC1 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC5 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.switch01UC1 = new iSCADA.Design.Utilities.Electrical.Switch01UC();
            this.retangleUC3 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.circleUC51 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC50 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC49 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC48 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC46 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC45 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC44 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC42 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC34 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC33 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC32 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC31 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC30 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC29 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC35 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC28 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC27 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.label262 = new System.Windows.Forms.Label();
            this.label198 = new System.Windows.Forms.Label();
            this.label201 = new System.Windows.Forms.Label();
            this.label202 = new System.Windows.Forms.Label();
            this.label203 = new System.Windows.Forms.Label();
            this.label204 = new System.Windows.Forms.Label();
            this.label164 = new System.Windows.Forms.Label();
            this.label165 = new System.Windows.Forms.Label();
            this.label166 = new System.Windows.Forms.Label();
            this.label167 = new System.Windows.Forms.Label();
            this.label168 = new System.Windows.Forms.Label();
            this.label169 = new System.Windows.Forms.Label();
            this.label170 = new System.Windows.Forms.Label();
            this.label171 = new System.Windows.Forms.Label();
            this.label172 = new System.Windows.Forms.Label();
            this.label173 = new System.Windows.Forms.Label();
            this.label180 = new System.Windows.Forms.Label();
            this.label181 = new System.Windows.Forms.Label();
            this.label162 = new System.Windows.Forms.Label();
            this.label161 = new System.Windows.Forms.Label();
            this.circleUC26 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC25 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC24 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.label160 = new System.Windows.Forms.Label();
            this.circleUC23 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.label159 = new System.Windows.Forms.Label();
            this.circleUC22 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.label156 = new System.Windows.Forms.Label();
            this.label158 = new System.Windows.Forms.Label();
            this.label157 = new System.Windows.Forms.Label();
            this.label137 = new System.Windows.Forms.Label();
            this.label136 = new System.Windows.Forms.Label();
            this.label135 = new System.Windows.Forms.Label();
            this.label134 = new System.Windows.Forms.Label();
            this.circleUC21 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC20 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC19 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC18 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC17 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC16 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC15 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.label133 = new System.Windows.Forms.Label();
            this.circleUC14 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.label132 = new System.Windows.Forms.Label();
            this.circleUC13 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.label131 = new System.Windows.Forms.Label();
            this.circleUC12 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.label130 = new System.Windows.Forms.Label();
            this.circleUC11 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.label129 = new System.Windows.Forms.Label();
            this.circleUC10 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.label128 = new System.Windows.Forms.Label();
            this.circleUC9 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.label127 = new System.Windows.Forms.Label();
            this.circleUC8 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.circleUC7 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.label126 = new System.Windows.Forms.Label();
            this.circleUC6 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.label38 = new System.Windows.Forms.Label();
            this.label163 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.circleUC5 = new iSCADA.Design.Utilities.Electrical.CircleUC();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1910, 900);
            this.tabControl1.TabIndex = 1822;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tabPage2.Controls.Add(this.label16);
            this.tabPage2.Controls.Add(this.label17);
            this.tabPage2.Controls.Add(this.label18);
            this.tabPage2.Controls.Add(this.label12);
            this.tabPage2.Controls.Add(this.label13);
            this.tabPage2.Controls.Add(this.label14);
            this.tabPage2.Controls.Add(this.button7);
            this.tabPage2.Controls.Add(this.button8);
            this.tabPage2.Controls.Add(this.button5);
            this.tabPage2.Controls.Add(this.button6);
            this.tabPage2.Controls.Add(this.button3);
            this.tabPage2.Controls.Add(this.button4);
            this.tabPage2.Controls.Add(this.button2);
            this.tabPage2.Controls.Add(this.button1);
            this.tabPage2.Controls.Add(this.label11);
            this.tabPage2.Controls.Add(this.circleUC47);
            this.tabPage2.Controls.Add(this.lineUC37);
            this.tabPage2.Controls.Add(this.lineUC36);
            this.tabPage2.Controls.Add(this.lineUC35);
            this.tabPage2.Controls.Add(this.retangleUC4);
            this.tabPage2.Controls.Add(this.circleUC43);
            this.tabPage2.Controls.Add(this.label15);
            this.tabPage2.Controls.Add(this.lineUC24);
            this.tabPage2.Controls.Add(this.lineUC25);
            this.tabPage2.Controls.Add(this.lineUC27);
            this.tabPage2.Controls.Add(this.lineUC28);
            this.tabPage2.Controls.Add(this.lineUC31);
            this.tabPage2.Controls.Add(this.lineUC32);
            this.tabPage2.Controls.Add(this.lineUC34);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.circleUC37);
            this.tabPage2.Controls.Add(this.circleUC38);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.lineUC5);
            this.tabPage2.Controls.Add(this.lineUC14);
            this.tabPage2.Controls.Add(this.lineUC15);
            this.tabPage2.Controls.Add(this.circleUC39);
            this.tabPage2.Controls.Add(this.circleUC40);
            this.tabPage2.Controls.Add(this.lineUC16);
            this.tabPage2.Controls.Add(this.lineUC17);
            this.tabPage2.Controls.Add(this.lineUC18);
            this.tabPage2.Controls.Add(this.lineUC19);
            this.tabPage2.Controls.Add(this.lineUC20);
            this.tabPage2.Controls.Add(this.lineUC21);
            this.tabPage2.Controls.Add(this.lineUC22);
            this.tabPage2.Controls.Add(this.circleUC41);
            this.tabPage2.Controls.Add(this.lineUC23);
            this.tabPage2.Controls.Add(this.switch05UC2);
            this.tabPage2.Controls.Add(this.retangleUC2);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.circleUC36);
            this.tabPage2.Controls.Add(this.circleUC4);
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.lineUC13);
            this.tabPage2.Controls.Add(this.lineUC12);
            this.tabPage2.Controls.Add(this.lineUC11);
            this.tabPage2.Controls.Add(this.circleUC3);
            this.tabPage2.Controls.Add(this.circleUC2);
            this.tabPage2.Controls.Add(this.lineUC10);
            this.tabPage2.Controls.Add(this.lineUC9);
            this.tabPage2.Controls.Add(this.lineUC8);
            this.tabPage2.Controls.Add(this.lineUC7);
            this.tabPage2.Controls.Add(this.lineUC6);
            this.tabPage2.Controls.Add(this.lineUC4);
            this.tabPage2.Controls.Add(this.lineUC3);
            this.tabPage2.Controls.Add(this.circleUC1);
            this.tabPage2.Controls.Add(this.lineUC2);
            this.tabPage2.Controls.Add(this.switch05UC1);
            this.tabPage2.Controls.Add(this.lineUC1);
            this.tabPage2.Controls.Add(this.label110);
            this.tabPage2.Controls.Add(this.label111);
            this.tabPage2.Controls.Add(this.label112);
            this.tabPage2.Controls.Add(this.label113);
            this.tabPage2.Controls.Add(this.label114);
            this.tabPage2.Controls.Add(this.label116);
            this.tabPage2.Controls.Add(this.label118);
            this.tabPage2.Controls.Add(this.label120);
            this.tabPage2.Controls.Add(this.label122);
            this.tabPage2.Controls.Add(this.label124);
            this.tabPage2.Controls.Add(this.label74);
            this.tabPage2.Controls.Add(this.label75);
            this.tabPage2.Controls.Add(this.label76);
            this.tabPage2.Controls.Add(this.label77);
            this.tabPage2.Controls.Add(this.label78);
            this.tabPage2.Controls.Add(this.label79);
            this.tabPage2.Controls.Add(this.label80);
            this.tabPage2.Controls.Add(this.label81);
            this.tabPage2.Controls.Add(this.label72);
            this.tabPage2.Controls.Add(this.label73);
            this.tabPage2.Controls.Add(this.label70);
            this.tabPage2.Controls.Add(this.label71);
            this.tabPage2.Controls.Add(this.label68);
            this.tabPage2.Controls.Add(this.label69);
            this.tabPage2.Controls.Add(this.label67);
            this.tabPage2.Controls.Add(this.label66);
            this.tabPage2.Controls.Add(this.retangleUC14);
            this.tabPage2.Controls.Add(this.label43);
            this.tabPage2.Controls.Add(this.label44);
            this.tabPage2.Controls.Add(this.label41);
            this.tabPage2.Controls.Add(this.label40);
            this.tabPage2.Controls.Add(this.label39);
            this.tabPage2.Controls.Add(this.retangleUC13);
            this.tabPage2.Controls.Add(this.retangleUC1);
            this.tabPage2.Controls.Add(this.retangleUC5);
            this.tabPage2.Controls.Add(this.switch01UC1);
            this.tabPage2.Controls.Add(this.retangleUC3);
            this.tabPage2.Font = new System.Drawing.Font("微軟正黑體", 16F);
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage2.Size = new System.Drawing.Size(1902, 867);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "C17";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label16.Location = new System.Drawing.Point(1354, 495);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(283, 24);
            this.label16.TabIndex = 2269;
            this.label16.Text = "MC63 PANEL TEMPERATURE：";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label17.Location = new System.Drawing.Point(1354, 466);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(335, 24);
            this.label17.TabIndex = 2268;
            this.label17.Text = "MC63 CURRENT CONTACTOR KF4：\r\n";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label18.Location = new System.Drawing.Point(1354, 437);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(335, 24);
            this.label18.TabIndex = 2267;
            this.label18.Text = "MC63 CURRENT CONTACTOR KF2：";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label12.Location = new System.Drawing.Point(1354, 406);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(248, 24);
            this.label12.TabIndex = 2266;
            this.label12.Text = "MC63 CURRENT DIODE3：";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label13.Location = new System.Drawing.Point(1354, 376);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(248, 24);
            this.label13.TabIndex = 2265;
            this.label13.Text = "MC63 CURRENT DIODE2：";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label14.Location = new System.Drawing.Point(1354, 347);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(248, 24);
            this.label14.TabIndex = 2264;
            this.label14.Text = "MC63 CURRENT DIODE1：";
            // 
            // button7
            // 
            this.button7.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.button7.Location = new System.Drawing.Point(1483, 169);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(100, 35);
            this.button7.TabIndex = 2263;
            this.button7.Text = "OFF";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // button8
            // 
            this.button8.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.button8.Location = new System.Drawing.Point(1350, 169);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(100, 35);
            this.button8.TabIndex = 2262;
            this.button8.Text = "ON";
            this.button8.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.button5.Location = new System.Drawing.Point(1483, 129);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(100, 35);
            this.button5.TabIndex = 2261;
            this.button5.Text = "OFF";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.button6.Location = new System.Drawing.Point(1350, 129);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(100, 35);
            this.button6.TabIndex = 2260;
            this.button6.Text = "ON";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.button3.Location = new System.Drawing.Point(1483, 89);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(100, 35);
            this.button3.TabIndex = 2259;
            this.button3.Text = "OFF";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.button4.Location = new System.Drawing.Point(1350, 89);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(100, 35);
            this.button4.TabIndex = 2258;
            this.button4.Text = "ON";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.button2.Location = new System.Drawing.Point(1483, 49);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(100, 35);
            this.button2.TabIndex = 2257;
            this.button2.Text = "OFF";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.button1.Location = new System.Drawing.Point(1350, 49);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 35);
            this.button1.TabIndex = 2256;
            this.button1.Text = "ON";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label11.Location = new System.Drawing.Point(581, 109);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(42, 21);
            this.label11.TabIndex = 2255;
            this.label11.Text = "鋼軌";
            // 
            // circleUC47
            // 
            this.circleUC47.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC47.ExtenderWidth = 2;
            this.circleUC47.Fill = false;
            this.circleUC47.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.circleUC47.GroupID = null;
            this.circleUC47.Location = new System.Drawing.Point(620, 365);
            this.circleUC47.Margin = new System.Windows.Forms.Padding(12, 16, 12, 16);
            this.circleUC47.Name = "circleUC47";
            this.circleUC47.Size = new System.Drawing.Size(10, 9);
            this.circleUC47.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC47.TabIndex = 2254;
            // 
            // lineUC37
            // 
            this.lineUC37.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC37.ExtenderWidth = 2;
            this.lineUC37.Fill = false;
            this.lineUC37.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lineUC37.GroupID = null;
            this.lineUC37.Location = new System.Drawing.Point(584, 247);
            this.lineUC37.Margin = new System.Windows.Forms.Padding(96, 182, 96, 182);
            this.lineUC37.Name = "lineUC37";
            this.lineUC37.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC37.Size = new System.Drawing.Size(2, 80);
            this.lineUC37.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC37.TabIndex = 2253;
            // 
            // lineUC36
            // 
            this.lineUC36.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC36.ExtenderWidth = 2;
            this.lineUC36.Fill = false;
            this.lineUC36.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lineUC36.GroupID = null;
            this.lineUC36.Location = new System.Drawing.Point(526, 368);
            this.lineUC36.Margin = new System.Windows.Forms.Padding(24, 36, 24, 36);
            this.lineUC36.Name = "lineUC36";
            this.lineUC36.Size = new System.Drawing.Size(100, 2);
            this.lineUC36.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC36.TabIndex = 2252;
            // 
            // lineUC35
            // 
            this.lineUC35.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC35.ExtenderWidth = 2;
            this.lineUC35.Fill = false;
            this.lineUC35.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lineUC35.GroupID = null;
            this.lineUC35.Location = new System.Drawing.Point(526, 319);
            this.lineUC35.Margin = new System.Windows.Forms.Padding(96, 182, 96, 182);
            this.lineUC35.Name = "lineUC35";
            this.lineUC35.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC35.Size = new System.Drawing.Size(2, 50);
            this.lineUC35.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC35.TabIndex = 2251;
            // 
            // retangleUC4
            // 
            this.retangleUC4.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC4.ExtenderWidth = 3;
            this.retangleUC4.Fill = false;
            this.retangleUC4.GroupID = null;
            this.retangleUC4.Location = new System.Drawing.Point(543, 107);
            this.retangleUC4.Name = "retangleUC4";
            this.retangleUC4.Size = new System.Drawing.Size(142, 26);
            this.retangleUC4.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC4.TabIndex = 2248;
            // 
            // circleUC43
            // 
            this.circleUC43.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC43.ExtenderWidth = 2;
            this.circleUC43.Fill = false;
            this.circleUC43.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.circleUC43.GroupID = null;
            this.circleUC43.Location = new System.Drawing.Point(620, 195);
            this.circleUC43.Margin = new System.Windows.Forms.Padding(12, 16, 12, 16);
            this.circleUC43.Name = "circleUC43";
            this.circleUC43.Size = new System.Drawing.Size(10, 9);
            this.circleUC43.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC43.TabIndex = 2244;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label15.Location = new System.Drawing.Point(503, 155);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 21);
            this.label15.TabIndex = 2240;
            this.label15.Text = "VLD";
            // 
            // lineUC24
            // 
            this.lineUC24.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC24.ExtenderWidth = 2;
            this.lineUC24.Fill = false;
            this.lineUC24.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lineUC24.GroupID = null;
            this.lineUC24.Location = new System.Drawing.Point(526, 198);
            this.lineUC24.Margin = new System.Windows.Forms.Padding(96, 182, 96, 182);
            this.lineUC24.Name = "lineUC24";
            this.lineUC24.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC24.Size = new System.Drawing.Size(2, 30);
            this.lineUC24.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC24.TabIndex = 2239;
            // 
            // lineUC25
            // 
            this.lineUC25.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC25.ExtenderWidth = 2;
            this.lineUC25.Fill = false;
            this.lineUC25.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lineUC25.GroupID = null;
            this.lineUC25.Location = new System.Drawing.Point(663, 247);
            this.lineUC25.Margin = new System.Windows.Forms.Padding(96, 182, 96, 182);
            this.lineUC25.Name = "lineUC25";
            this.lineUC25.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC25.Size = new System.Drawing.Size(2, 80);
            this.lineUC25.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC25.TabIndex = 2238;
            // 
            // lineUC27
            // 
            this.lineUC27.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC27.ExtenderWidth = 2;
            this.lineUC27.Fill = false;
            this.lineUC27.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lineUC27.GroupID = null;
            this.lineUC27.Location = new System.Drawing.Point(585, 325);
            this.lineUC27.Margin = new System.Windows.Forms.Padding(24, 36, 24, 36);
            this.lineUC27.Name = "lineUC27";
            this.lineUC27.Size = new System.Drawing.Size(80, 2);
            this.lineUC27.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC27.TabIndex = 2234;
            // 
            // lineUC28
            // 
            this.lineUC28.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC28.ExtenderWidth = 2;
            this.lineUC28.Fill = false;
            this.lineUC28.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lineUC28.GroupID = null;
            this.lineUC28.Location = new System.Drawing.Point(526, 198);
            this.lineUC28.Margin = new System.Windows.Forms.Padding(24, 36, 24, 36);
            this.lineUC28.Name = "lineUC28";
            this.lineUC28.Size = new System.Drawing.Size(100, 2);
            this.lineUC28.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC28.TabIndex = 2233;
            // 
            // lineUC31
            // 
            this.lineUC31.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC31.ExtenderWidth = 2;
            this.lineUC31.Fill = false;
            this.lineUC31.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lineUC31.GroupID = null;
            this.lineUC31.Location = new System.Drawing.Point(584, 247);
            this.lineUC31.Margin = new System.Windows.Forms.Padding(24, 36, 24, 36);
            this.lineUC31.Name = "lineUC31";
            this.lineUC31.Size = new System.Drawing.Size(80, 2);
            this.lineUC31.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC31.TabIndex = 2230;
            // 
            // lineUC32
            // 
            this.lineUC32.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC32.ExtenderWidth = 2;
            this.lineUC32.Fill = false;
            this.lineUC32.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lineUC32.GroupID = null;
            this.lineUC32.Location = new System.Drawing.Point(624, 127);
            this.lineUC32.Margin = new System.Windows.Forms.Padding(48, 81, 48, 81);
            this.lineUC32.Name = "lineUC32";
            this.lineUC32.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC32.Size = new System.Drawing.Size(2, 330);
            this.lineUC32.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC32.TabIndex = 2229;
            // 
            // lineUC34
            // 
            this.lineUC34.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC34.ExtenderWidth = 2;
            this.lineUC34.Fill = false;
            this.lineUC34.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lineUC34.GroupID = null;
            this.lineUC34.Location = new System.Drawing.Point(549, 455);
            this.lineUC34.Margin = new System.Windows.Forms.Padding(12, 16, 12, 16);
            this.lineUC34.Name = "lineUC34";
            this.lineUC34.Size = new System.Drawing.Size(136, 2);
            this.lineUC34.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC34.TabIndex = 2226;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label6.Location = new System.Drawing.Point(310, 477);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 21);
            this.label6.TabIndex = 2224;
            this.label6.Text = "下行";
            // 
            // circleUC37
            // 
            this.circleUC37.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC37.ExtenderWidth = 2;
            this.circleUC37.Fill = false;
            this.circleUC37.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.circleUC37.GroupID = null;
            this.circleUC37.Location = new System.Drawing.Point(406, 463);
            this.circleUC37.Margin = new System.Windows.Forms.Padding(12, 16, 12, 16);
            this.circleUC37.Name = "circleUC37";
            this.circleUC37.Size = new System.Drawing.Size(10, 9);
            this.circleUC37.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC37.TabIndex = 2222;
            // 
            // circleUC38
            // 
            this.circleUC38.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC38.ExtenderWidth = 2;
            this.circleUC38.Fill = false;
            this.circleUC38.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.circleUC38.GroupID = null;
            this.circleUC38.Location = new System.Drawing.Point(312, 463);
            this.circleUC38.Margin = new System.Windows.Forms.Padding(12, 16, 12, 16);
            this.circleUC38.Name = "circleUC38";
            this.circleUC38.Size = new System.Drawing.Size(10, 9);
            this.circleUC38.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC38.TabIndex = 2221;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label7.Location = new System.Drawing.Point(430, 350);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(24, 17);
            this.label7.TabIndex = 2220;
            this.label7.Text = "K4";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label8.Location = new System.Drawing.Point(279, 350);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(24, 17);
            this.label8.TabIndex = 2219;
            this.label8.Text = "K2";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label9.Location = new System.Drawing.Point(271, 236);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 42);
            this.label9.TabIndex = 2218;
            this.label9.Text = "Q2\r\n2600A";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label10.Location = new System.Drawing.Point(278, 167);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(71, 21);
            this.label10.TabIndex = 2217;
            this.label10.Text = "\"MC63\"";
            // 
            // lineUC5
            // 
            this.lineUC5.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC5.ExtenderWidth = 2;
            this.lineUC5.Fill = false;
            this.lineUC5.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lineUC5.GroupID = null;
            this.lineUC5.Location = new System.Drawing.Point(317, 299);
            this.lineUC5.Margin = new System.Windows.Forms.Padding(96, 182, 96, 182);
            this.lineUC5.Name = "lineUC5";
            this.lineUC5.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC5.Size = new System.Drawing.Size(2, 50);
            this.lineUC5.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC5.TabIndex = 2216;
            // 
            // lineUC14
            // 
            this.lineUC14.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC14.ExtenderWidth = 2;
            this.lineUC14.Fill = false;
            this.lineUC14.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lineUC14.GroupID = null;
            this.lineUC14.Location = new System.Drawing.Point(410, 368);
            this.lineUC14.Margin = new System.Windows.Forms.Padding(96, 182, 96, 182);
            this.lineUC14.Name = "lineUC14";
            this.lineUC14.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC14.Size = new System.Drawing.Size(2, 99);
            this.lineUC14.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC14.TabIndex = 2215;
            // 
            // lineUC15
            // 
            this.lineUC15.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC15.ExtenderWidth = 2;
            this.lineUC15.Fill = false;
            this.lineUC15.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lineUC15.GroupID = null;
            this.lineUC15.Location = new System.Drawing.Point(410, 300);
            this.lineUC15.Margin = new System.Windows.Forms.Padding(96, 182, 96, 182);
            this.lineUC15.Name = "lineUC15";
            this.lineUC15.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC15.Size = new System.Drawing.Size(2, 50);
            this.lineUC15.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC15.TabIndex = 2214;
            // 
            // circleUC39
            // 
            this.circleUC39.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC39.ExtenderWidth = 2;
            this.circleUC39.Fill = false;
            this.circleUC39.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.circleUC39.GroupID = null;
            this.circleUC39.Location = new System.Drawing.Point(406, 291);
            this.circleUC39.Margin = new System.Windows.Forms.Padding(12, 16, 12, 16);
            this.circleUC39.Name = "circleUC39";
            this.circleUC39.Size = new System.Drawing.Size(10, 9);
            this.circleUC39.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC39.TabIndex = 2213;
            // 
            // circleUC40
            // 
            this.circleUC40.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC40.ExtenderWidth = 2;
            this.circleUC40.Fill = false;
            this.circleUC40.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.circleUC40.GroupID = null;
            this.circleUC40.Location = new System.Drawing.Point(314, 291);
            this.circleUC40.Margin = new System.Windows.Forms.Padding(12, 16, 12, 16);
            this.circleUC40.Name = "circleUC40";
            this.circleUC40.Size = new System.Drawing.Size(10, 9);
            this.circleUC40.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC40.TabIndex = 2212;
            // 
            // lineUC16
            // 
            this.lineUC16.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC16.ExtenderWidth = 2;
            this.lineUC16.Fill = false;
            this.lineUC16.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lineUC16.GroupID = null;
            this.lineUC16.Location = new System.Drawing.Point(303, 365);
            this.lineUC16.Margin = new System.Windows.Forms.Padding(24, 36, 24, 36);
            this.lineUC16.Name = "lineUC16";
            this.lineUC16.Size = new System.Drawing.Size(30, 2);
            this.lineUC16.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC16.TabIndex = 2211;
            // 
            // lineUC17
            // 
            this.lineUC17.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC17.ExtenderWidth = 2;
            this.lineUC17.Fill = false;
            this.lineUC17.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lineUC17.GroupID = null;
            this.lineUC17.Location = new System.Drawing.Point(296, 466);
            this.lineUC17.Margin = new System.Windows.Forms.Padding(24, 36, 24, 36);
            this.lineUC17.Name = "lineUC17";
            this.lineUC17.Size = new System.Drawing.Size(136, 2);
            this.lineUC17.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC17.TabIndex = 2210;
            // 
            // lineUC18
            // 
            this.lineUC18.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC18.ExtenderWidth = 2;
            this.lineUC18.Fill = false;
            this.lineUC18.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lineUC18.GroupID = null;
            this.lineUC18.Location = new System.Drawing.Point(394, 365);
            this.lineUC18.Margin = new System.Windows.Forms.Padding(24, 36, 24, 36);
            this.lineUC18.Name = "lineUC18";
            this.lineUC18.Size = new System.Drawing.Size(30, 2);
            this.lineUC18.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC18.TabIndex = 2209;
            // 
            // lineUC19
            // 
            this.lineUC19.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC19.ExtenderWidth = 2;
            this.lineUC19.Fill = false;
            this.lineUC19.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lineUC19.GroupID = null;
            this.lineUC19.Location = new System.Drawing.Point(394, 350);
            this.lineUC19.Margin = new System.Windows.Forms.Padding(24, 36, 24, 36);
            this.lineUC19.Name = "lineUC19";
            this.lineUC19.Size = new System.Drawing.Size(30, 2);
            this.lineUC19.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC19.TabIndex = 2208;
            // 
            // lineUC20
            // 
            this.lineUC20.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC20.ExtenderWidth = 2;
            this.lineUC20.Fill = false;
            this.lineUC20.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lineUC20.GroupID = null;
            this.lineUC20.Location = new System.Drawing.Point(303, 350);
            this.lineUC20.Margin = new System.Windows.Forms.Padding(24, 36, 24, 36);
            this.lineUC20.Name = "lineUC20";
            this.lineUC20.Size = new System.Drawing.Size(30, 2);
            this.lineUC20.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC20.TabIndex = 2207;
            // 
            // lineUC21
            // 
            this.lineUC21.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC21.ExtenderWidth = 2;
            this.lineUC21.Fill = false;
            this.lineUC21.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lineUC21.GroupID = null;
            this.lineUC21.Location = new System.Drawing.Point(316, 368);
            this.lineUC21.Margin = new System.Windows.Forms.Padding(48, 81, 48, 81);
            this.lineUC21.Name = "lineUC21";
            this.lineUC21.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC21.Size = new System.Drawing.Size(2, 99);
            this.lineUC21.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC21.TabIndex = 2206;
            // 
            // lineUC22
            // 
            this.lineUC22.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC22.ExtenderWidth = 2;
            this.lineUC22.Fill = false;
            this.lineUC22.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lineUC22.GroupID = null;
            this.lineUC22.Location = new System.Drawing.Point(364, 95);
            this.lineUC22.Margin = new System.Windows.Forms.Padding(24, 36, 24, 36);
            this.lineUC22.Name = "lineUC22";
            this.lineUC22.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC22.Size = new System.Drawing.Size(2, 50);
            this.lineUC22.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC22.TabIndex = 2205;
            // 
            // circleUC41
            // 
            this.circleUC41.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC41.ExtenderWidth = 2;
            this.circleUC41.Fill = false;
            this.circleUC41.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.circleUC41.GroupID = null;
            this.circleUC41.Location = new System.Drawing.Point(360, 291);
            this.circleUC41.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.circleUC41.Name = "circleUC41";
            this.circleUC41.Size = new System.Drawing.Size(10, 9);
            this.circleUC41.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC41.TabIndex = 2204;
            // 
            // lineUC23
            // 
            this.lineUC23.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC23.ExtenderWidth = 2;
            this.lineUC23.Fill = false;
            this.lineUC23.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lineUC23.GroupID = null;
            this.lineUC23.Location = new System.Drawing.Point(296, 296);
            this.lineUC23.Margin = new System.Windows.Forms.Padding(12, 16, 12, 16);
            this.lineUC23.Name = "lineUC23";
            this.lineUC23.Size = new System.Drawing.Size(136, 2);
            this.lineUC23.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC23.TabIndex = 2203;
            // 
            // switch05UC2
            // 
            this.switch05UC2.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.switch05UC2.ExtenderWidth = 2;
            this.switch05UC2.Fill = false;
            this.switch05UC2.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.switch05UC2.GroupID = null;
            this.switch05UC2.Location = new System.Drawing.Point(315, 145);
            this.switch05UC2.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.switch05UC2.Name = "switch05UC2";
            this.switch05UC2.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.switch05UC2.Size = new System.Drawing.Size(111, 151);
            this.switch05UC2.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.switch05UC2.TabIndex = 2202;
            // 
            // retangleUC2
            // 
            this.retangleUC2.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Dash;
            this.retangleUC2.ExtenderWidth = 3;
            this.retangleUC2.Fill = false;
            this.retangleUC2.GroupID = null;
            this.retangleUC2.Location = new System.Drawing.Point(259, 145);
            this.retangleUC2.Name = "retangleUC2";
            this.retangleUC2.Size = new System.Drawing.Size(210, 282);
            this.retangleUC2.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC2.TabIndex = 2223;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label5.Location = new System.Drawing.Point(82, 477);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 21);
            this.label5.TabIndex = 2201;
            this.label5.Text = "上行";
            // 
            // circleUC36
            // 
            this.circleUC36.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC36.ExtenderWidth = 2;
            this.circleUC36.Fill = false;
            this.circleUC36.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.circleUC36.GroupID = null;
            this.circleUC36.Location = new System.Drawing.Point(178, 463);
            this.circleUC36.Margin = new System.Windows.Forms.Padding(12, 16, 12, 16);
            this.circleUC36.Name = "circleUC36";
            this.circleUC36.Size = new System.Drawing.Size(10, 9);
            this.circleUC36.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC36.TabIndex = 2199;
            // 
            // circleUC4
            // 
            this.circleUC4.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC4.ExtenderWidth = 2;
            this.circleUC4.Fill = false;
            this.circleUC4.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.circleUC4.GroupID = null;
            this.circleUC4.Location = new System.Drawing.Point(84, 463);
            this.circleUC4.Margin = new System.Windows.Forms.Padding(12, 16, 12, 16);
            this.circleUC4.Name = "circleUC4";
            this.circleUC4.Size = new System.Drawing.Size(10, 9);
            this.circleUC4.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC4.TabIndex = 2198;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(202, 350);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(24, 17);
            this.label1.TabIndex = 2197;
            this.label1.Text = "K3";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(51, 350);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(24, 17);
            this.label2.TabIndex = 2196;
            this.label2.Text = "K1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label3.Location = new System.Drawing.Point(43, 236);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 42);
            this.label3.TabIndex = 2195;
            this.label3.Text = "Q1\r\n2600A";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label4.Location = new System.Drawing.Point(50, 167);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 21);
            this.label4.TabIndex = 2194;
            this.label4.Text = "\"MC62\"";
            // 
            // lineUC13
            // 
            this.lineUC13.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC13.ExtenderWidth = 2;
            this.lineUC13.Fill = false;
            this.lineUC13.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lineUC13.GroupID = null;
            this.lineUC13.Location = new System.Drawing.Point(89, 299);
            this.lineUC13.Margin = new System.Windows.Forms.Padding(96, 182, 96, 182);
            this.lineUC13.Name = "lineUC13";
            this.lineUC13.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC13.Size = new System.Drawing.Size(2, 50);
            this.lineUC13.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC13.TabIndex = 2193;
            // 
            // lineUC12
            // 
            this.lineUC12.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC12.ExtenderWidth = 2;
            this.lineUC12.Fill = false;
            this.lineUC12.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lineUC12.GroupID = null;
            this.lineUC12.Location = new System.Drawing.Point(182, 368);
            this.lineUC12.Margin = new System.Windows.Forms.Padding(96, 182, 96, 182);
            this.lineUC12.Name = "lineUC12";
            this.lineUC12.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC12.Size = new System.Drawing.Size(2, 99);
            this.lineUC12.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC12.TabIndex = 2192;
            // 
            // lineUC11
            // 
            this.lineUC11.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC11.ExtenderWidth = 2;
            this.lineUC11.Fill = false;
            this.lineUC11.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lineUC11.GroupID = null;
            this.lineUC11.Location = new System.Drawing.Point(182, 300);
            this.lineUC11.Margin = new System.Windows.Forms.Padding(96, 182, 96, 182);
            this.lineUC11.Name = "lineUC11";
            this.lineUC11.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC11.Size = new System.Drawing.Size(2, 50);
            this.lineUC11.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC11.TabIndex = 2191;
            // 
            // circleUC3
            // 
            this.circleUC3.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC3.ExtenderWidth = 2;
            this.circleUC3.Fill = false;
            this.circleUC3.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.circleUC3.GroupID = null;
            this.circleUC3.Location = new System.Drawing.Point(178, 291);
            this.circleUC3.Margin = new System.Windows.Forms.Padding(12, 16, 12, 16);
            this.circleUC3.Name = "circleUC3";
            this.circleUC3.Size = new System.Drawing.Size(10, 9);
            this.circleUC3.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC3.TabIndex = 2190;
            // 
            // circleUC2
            // 
            this.circleUC2.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC2.ExtenderWidth = 2;
            this.circleUC2.Fill = false;
            this.circleUC2.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.circleUC2.GroupID = null;
            this.circleUC2.Location = new System.Drawing.Point(86, 291);
            this.circleUC2.Margin = new System.Windows.Forms.Padding(12, 16, 12, 16);
            this.circleUC2.Name = "circleUC2";
            this.circleUC2.Size = new System.Drawing.Size(10, 9);
            this.circleUC2.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC2.TabIndex = 2189;
            // 
            // lineUC10
            // 
            this.lineUC10.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC10.ExtenderWidth = 2;
            this.lineUC10.Fill = false;
            this.lineUC10.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lineUC10.GroupID = null;
            this.lineUC10.Location = new System.Drawing.Point(75, 365);
            this.lineUC10.Margin = new System.Windows.Forms.Padding(24, 36, 24, 36);
            this.lineUC10.Name = "lineUC10";
            this.lineUC10.Size = new System.Drawing.Size(30, 2);
            this.lineUC10.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC10.TabIndex = 2188;
            // 
            // lineUC9
            // 
            this.lineUC9.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC9.ExtenderWidth = 2;
            this.lineUC9.Fill = false;
            this.lineUC9.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lineUC9.GroupID = null;
            this.lineUC9.Location = new System.Drawing.Point(68, 466);
            this.lineUC9.Margin = new System.Windows.Forms.Padding(24, 36, 24, 36);
            this.lineUC9.Name = "lineUC9";
            this.lineUC9.Size = new System.Drawing.Size(136, 2);
            this.lineUC9.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC9.TabIndex = 2187;
            // 
            // lineUC8
            // 
            this.lineUC8.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC8.ExtenderWidth = 2;
            this.lineUC8.Fill = false;
            this.lineUC8.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lineUC8.GroupID = null;
            this.lineUC8.Location = new System.Drawing.Point(166, 365);
            this.lineUC8.Margin = new System.Windows.Forms.Padding(24, 36, 24, 36);
            this.lineUC8.Name = "lineUC8";
            this.lineUC8.Size = new System.Drawing.Size(30, 2);
            this.lineUC8.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC8.TabIndex = 2186;
            // 
            // lineUC7
            // 
            this.lineUC7.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC7.ExtenderWidth = 2;
            this.lineUC7.Fill = false;
            this.lineUC7.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lineUC7.GroupID = null;
            this.lineUC7.Location = new System.Drawing.Point(166, 350);
            this.lineUC7.Margin = new System.Windows.Forms.Padding(24, 36, 24, 36);
            this.lineUC7.Name = "lineUC7";
            this.lineUC7.Size = new System.Drawing.Size(30, 2);
            this.lineUC7.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC7.TabIndex = 2185;
            // 
            // lineUC6
            // 
            this.lineUC6.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC6.ExtenderWidth = 2;
            this.lineUC6.Fill = false;
            this.lineUC6.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lineUC6.GroupID = null;
            this.lineUC6.Location = new System.Drawing.Point(75, 350);
            this.lineUC6.Margin = new System.Windows.Forms.Padding(24, 36, 24, 36);
            this.lineUC6.Name = "lineUC6";
            this.lineUC6.Size = new System.Drawing.Size(30, 2);
            this.lineUC6.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC6.TabIndex = 2184;
            // 
            // lineUC4
            // 
            this.lineUC4.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC4.ExtenderWidth = 2;
            this.lineUC4.Fill = false;
            this.lineUC4.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lineUC4.GroupID = null;
            this.lineUC4.Location = new System.Drawing.Point(88, 368);
            this.lineUC4.Margin = new System.Windows.Forms.Padding(48, 81, 48, 81);
            this.lineUC4.Name = "lineUC4";
            this.lineUC4.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC4.Size = new System.Drawing.Size(2, 99);
            this.lineUC4.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC4.TabIndex = 2182;
            // 
            // lineUC3
            // 
            this.lineUC3.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC3.ExtenderWidth = 2;
            this.lineUC3.Fill = false;
            this.lineUC3.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lineUC3.GroupID = null;
            this.lineUC3.Location = new System.Drawing.Point(136, 95);
            this.lineUC3.Margin = new System.Windows.Forms.Padding(24, 36, 24, 36);
            this.lineUC3.Name = "lineUC3";
            this.lineUC3.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC3.Size = new System.Drawing.Size(2, 50);
            this.lineUC3.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC3.TabIndex = 2181;
            // 
            // circleUC1
            // 
            this.circleUC1.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC1.ExtenderWidth = 2;
            this.circleUC1.Fill = false;
            this.circleUC1.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.circleUC1.GroupID = null;
            this.circleUC1.Location = new System.Drawing.Point(132, 291);
            this.circleUC1.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.circleUC1.Name = "circleUC1";
            this.circleUC1.Size = new System.Drawing.Size(10, 9);
            this.circleUC1.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC1.TabIndex = 2180;
            // 
            // lineUC2
            // 
            this.lineUC2.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC2.ExtenderWidth = 2;
            this.lineUC2.Fill = false;
            this.lineUC2.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lineUC2.GroupID = null;
            this.lineUC2.Location = new System.Drawing.Point(68, 296);
            this.lineUC2.Margin = new System.Windows.Forms.Padding(12, 16, 12, 16);
            this.lineUC2.Name = "lineUC2";
            this.lineUC2.Size = new System.Drawing.Size(136, 2);
            this.lineUC2.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC2.TabIndex = 2179;
            // 
            // switch05UC1
            // 
            this.switch05UC1.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.switch05UC1.ExtenderWidth = 2;
            this.switch05UC1.Fill = false;
            this.switch05UC1.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.switch05UC1.GroupID = null;
            this.switch05UC1.Location = new System.Drawing.Point(87, 145);
            this.switch05UC1.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.switch05UC1.Name = "switch05UC1";
            this.switch05UC1.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.switch05UC1.Size = new System.Drawing.Size(111, 151);
            this.switch05UC1.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.switch05UC1.TabIndex = 2178;
            // 
            // lineUC1
            // 
            this.lineUC1.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC1.ExtenderWidth = 2;
            this.lineUC1.Fill = false;
            this.lineUC1.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lineUC1.GroupID = null;
            this.lineUC1.Location = new System.Drawing.Point(54, 95);
            this.lineUC1.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.lineUC1.Name = "lineUC1";
            this.lineUC1.Size = new System.Drawing.Size(650, 2);
            this.lineUC1.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC1.TabIndex = 2177;
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label110.Location = new System.Drawing.Point(1704, 556);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(61, 24);
            this.label110.TabIndex = 2144;
            this.label110.Text = "Value";
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label111.Location = new System.Drawing.Point(1354, 556);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(283, 24);
            this.label111.TabIndex = 2143;
            this.label111.Text = "MC64 PANEL TEMPERATURE：";
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label112.Location = new System.Drawing.Point(1704, 525);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(61, 24);
            this.label112.TabIndex = 2142;
            this.label112.Text = "Value";
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label113.Location = new System.Drawing.Point(1354, 525);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(287, 24);
            this.label113.TabIndex = 2141;
            this.label113.Text = "MF80 VOLTAGE MEASURING：";
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label114.Location = new System.Drawing.Point(1704, 495);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(61, 24);
            this.label114.TabIndex = 2140;
            this.label114.Text = "Value";
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label116.Location = new System.Drawing.Point(1704, 466);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(61, 24);
            this.label116.TabIndex = 2138;
            this.label116.Text = "Value";
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label118.Location = new System.Drawing.Point(1704, 437);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(61, 24);
            this.label118.TabIndex = 2136;
            this.label118.Text = "Value";
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label120.Location = new System.Drawing.Point(1704, 406);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(61, 24);
            this.label120.TabIndex = 2134;
            this.label120.Text = "Value";
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label122.Location = new System.Drawing.Point(1704, 376);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(61, 24);
            this.label122.TabIndex = 2132;
            this.label122.Text = "Value";
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label124.Location = new System.Drawing.Point(1704, 347);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(61, 24);
            this.label124.TabIndex = 2130;
            this.label124.Text = "Value";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label74.Location = new System.Drawing.Point(1232, 556);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(61, 24);
            this.label74.TabIndex = 2114;
            this.label74.Text = "Value";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label75.Location = new System.Drawing.Point(841, 556);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(345, 24);
            this.label75.TabIndex = 2113;
            this.label75.Text = "MC63 VOLTAGE CONDUCTOR RAIL：";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label76.Location = new System.Drawing.Point(1232, 525);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(61, 24);
            this.label76.TabIndex = 2112;
            this.label76.Text = "Value";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label77.Location = new System.Drawing.Point(841, 525);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(283, 24);
            this.label77.TabIndex = 2111;
            this.label77.Text = "MC62 PANEL TEMPERATURE：";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label78.Location = new System.Drawing.Point(1232, 495);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(61, 24);
            this.label78.TabIndex = 2110;
            this.label78.Text = "Value";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label79.Location = new System.Drawing.Point(841, 495);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(335, 24);
            this.label79.TabIndex = 2109;
            this.label79.Text = "MC62 CURRENT CONTACTOR KF3：\r\n";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label80.Location = new System.Drawing.Point(1232, 466);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(61, 24);
            this.label80.TabIndex = 2108;
            this.label80.Text = "Value";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label81.Location = new System.Drawing.Point(841, 466);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(335, 24);
            this.label81.TabIndex = 2107;
            this.label81.Text = "MC62 CURRENT CONTACTOR KF1：";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label72.Location = new System.Drawing.Point(1232, 437);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(61, 24);
            this.label72.TabIndex = 2106;
            this.label72.Text = "Value";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label73.Location = new System.Drawing.Point(841, 437);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(248, 24);
            this.label73.TabIndex = 2105;
            this.label73.Text = "MC62 CURRENT DIODE3：";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label70.Location = new System.Drawing.Point(1232, 406);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(61, 24);
            this.label70.TabIndex = 2104;
            this.label70.Text = "Value";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label71.Location = new System.Drawing.Point(841, 406);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(248, 24);
            this.label71.TabIndex = 2103;
            this.label71.Text = "MC62 CURRENT DIODE2：";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label68.Location = new System.Drawing.Point(1232, 376);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(61, 24);
            this.label68.TabIndex = 2102;
            this.label68.Text = "Value";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label69.Location = new System.Drawing.Point(841, 376);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(248, 24);
            this.label69.TabIndex = 2101;
            this.label69.Text = "MC62 CURRENT DIODE1：";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label67.Location = new System.Drawing.Point(1232, 347);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(61, 24);
            this.label67.TabIndex = 2100;
            this.label67.Text = "Value";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label66.Location = new System.Drawing.Point(841, 347);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(345, 24);
            this.label66.TabIndex = 2099;
            this.label66.Text = "MC62 VOLTAGE CONDUCTOR RAIL：";
            // 
            // retangleUC14
            // 
            this.retangleUC14.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Dash;
            this.retangleUC14.ExtenderWidth = 3;
            this.retangleUC14.Fill = false;
            this.retangleUC14.GroupID = null;
            this.retangleUC14.Location = new System.Drawing.Point(829, 326);
            this.retangleUC14.Name = "retangleUC14";
            this.retangleUC14.Size = new System.Drawing.Size(1034, 306);
            this.retangleUC14.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC14.TabIndex = 2098;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label43.Location = new System.Drawing.Point(842, 213);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(116, 24);
            this.label43.TabIndex = 1928;
            this.label43.Text = "COMMAND";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label44.Location = new System.Drawing.Point(842, 173);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(486, 24);
            this.label44.TabIndex = 1921;
            this.label44.Text = "MF80 REMOTE-COMMAND PROTECTION-ON DIRECT";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label41.Location = new System.Drawing.Point(841, 133);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(441, 24);
            this.label41.TabIndex = 1914;
            this.label41.Text = "MF80 REMOTE-COMMAND FUNCTION TEST ON";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label40.Location = new System.Drawing.Point(841, 93);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(328, 24);
            this.label40.TabIndex = 1907;
            this.label40.Text = "MC63 DISCONNECTOR COMMAND";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.label39.Location = new System.Drawing.Point(841, 53);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(328, 24);
            this.label39.TabIndex = 1901;
            this.label39.Text = "MC62 DISCONNECTOR COMMAND";
            // 
            // retangleUC13
            // 
            this.retangleUC13.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Dash;
            this.retangleUC13.ExtenderWidth = 3;
            this.retangleUC13.Fill = false;
            this.retangleUC13.GroupID = null;
            this.retangleUC13.Location = new System.Drawing.Point(829, 38);
            this.retangleUC13.Name = "retangleUC13";
            this.retangleUC13.Size = new System.Drawing.Size(837, 240);
            this.retangleUC13.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC13.TabIndex = 2097;
            // 
            // retangleUC1
            // 
            this.retangleUC1.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Dash;
            this.retangleUC1.ExtenderWidth = 3;
            this.retangleUC1.Fill = false;
            this.retangleUC1.GroupID = null;
            this.retangleUC1.Location = new System.Drawing.Point(31, 145);
            this.retangleUC1.Name = "retangleUC1";
            this.retangleUC1.Size = new System.Drawing.Size(210, 282);
            this.retangleUC1.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC1.TabIndex = 2200;
            // 
            // retangleUC5
            // 
            this.retangleUC5.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Dash;
            this.retangleUC5.ExtenderWidth = 3;
            this.retangleUC5.Fill = false;
            this.retangleUC5.GroupID = null;
            this.retangleUC5.Location = new System.Drawing.Point(565, 225);
            this.retangleUC5.Name = "retangleUC5";
            this.retangleUC5.Size = new System.Drawing.Size(120, 120);
            this.retangleUC5.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC5.TabIndex = 2249;
            // 
            // switch01UC1
            // 
            this.switch01UC1.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.switch01UC1.ExtenderWidth = 2;
            this.switch01UC1.Fill = false;
            this.switch01UC1.GroupID = null;
            this.switch01UC1.Location = new System.Drawing.Point(496, 227);
            this.switch01UC1.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.switch01UC1.Name = "switch01UC1";
            this.switch01UC1.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.switch01UC1.Size = new System.Drawing.Size(64, 94);
            this.switch01UC1.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.switch01UC1.TabIndex = 2250;
            // 
            // retangleUC3
            // 
            this.retangleUC3.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Dash;
            this.retangleUC3.ExtenderWidth = 3;
            this.retangleUC3.Fill = false;
            this.retangleUC3.GroupID = null;
            this.retangleUC3.Location = new System.Drawing.Point(494, 145);
            this.retangleUC3.Name = "retangleUC3";
            this.retangleUC3.Size = new System.Drawing.Size(210, 282);
            this.retangleUC3.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC3.TabIndex = 2246;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tabPage1.Controls.Add(this.circleUC51);
            this.tabPage1.Controls.Add(this.circleUC50);
            this.tabPage1.Controls.Add(this.circleUC49);
            this.tabPage1.Controls.Add(this.circleUC48);
            this.tabPage1.Controls.Add(this.circleUC46);
            this.tabPage1.Controls.Add(this.circleUC45);
            this.tabPage1.Controls.Add(this.circleUC44);
            this.tabPage1.Controls.Add(this.circleUC42);
            this.tabPage1.Controls.Add(this.circleUC34);
            this.tabPage1.Controls.Add(this.circleUC33);
            this.tabPage1.Controls.Add(this.circleUC32);
            this.tabPage1.Controls.Add(this.circleUC31);
            this.tabPage1.Controls.Add(this.circleUC30);
            this.tabPage1.Controls.Add(this.circleUC29);
            this.tabPage1.Controls.Add(this.circleUC35);
            this.tabPage1.Controls.Add(this.circleUC28);
            this.tabPage1.Controls.Add(this.circleUC27);
            this.tabPage1.Controls.Add(this.label262);
            this.tabPage1.Controls.Add(this.label198);
            this.tabPage1.Controls.Add(this.label201);
            this.tabPage1.Controls.Add(this.label202);
            this.tabPage1.Controls.Add(this.label203);
            this.tabPage1.Controls.Add(this.label204);
            this.tabPage1.Controls.Add(this.label164);
            this.tabPage1.Controls.Add(this.label165);
            this.tabPage1.Controls.Add(this.label166);
            this.tabPage1.Controls.Add(this.label167);
            this.tabPage1.Controls.Add(this.label168);
            this.tabPage1.Controls.Add(this.label169);
            this.tabPage1.Controls.Add(this.label170);
            this.tabPage1.Controls.Add(this.label171);
            this.tabPage1.Controls.Add(this.label172);
            this.tabPage1.Controls.Add(this.label173);
            this.tabPage1.Controls.Add(this.label180);
            this.tabPage1.Controls.Add(this.label181);
            this.tabPage1.Controls.Add(this.label162);
            this.tabPage1.Controls.Add(this.label161);
            this.tabPage1.Controls.Add(this.circleUC26);
            this.tabPage1.Controls.Add(this.circleUC25);
            this.tabPage1.Controls.Add(this.circleUC24);
            this.tabPage1.Controls.Add(this.label160);
            this.tabPage1.Controls.Add(this.circleUC23);
            this.tabPage1.Controls.Add(this.label159);
            this.tabPage1.Controls.Add(this.circleUC22);
            this.tabPage1.Controls.Add(this.label156);
            this.tabPage1.Controls.Add(this.label158);
            this.tabPage1.Controls.Add(this.label157);
            this.tabPage1.Controls.Add(this.label137);
            this.tabPage1.Controls.Add(this.label136);
            this.tabPage1.Controls.Add(this.label135);
            this.tabPage1.Controls.Add(this.label134);
            this.tabPage1.Controls.Add(this.circleUC21);
            this.tabPage1.Controls.Add(this.circleUC20);
            this.tabPage1.Controls.Add(this.circleUC19);
            this.tabPage1.Controls.Add(this.circleUC18);
            this.tabPage1.Controls.Add(this.circleUC17);
            this.tabPage1.Controls.Add(this.circleUC16);
            this.tabPage1.Controls.Add(this.circleUC15);
            this.tabPage1.Controls.Add(this.label133);
            this.tabPage1.Controls.Add(this.circleUC14);
            this.tabPage1.Controls.Add(this.label132);
            this.tabPage1.Controls.Add(this.circleUC13);
            this.tabPage1.Controls.Add(this.label131);
            this.tabPage1.Controls.Add(this.circleUC12);
            this.tabPage1.Controls.Add(this.label130);
            this.tabPage1.Controls.Add(this.circleUC11);
            this.tabPage1.Controls.Add(this.label129);
            this.tabPage1.Controls.Add(this.circleUC10);
            this.tabPage1.Controls.Add(this.label128);
            this.tabPage1.Controls.Add(this.circleUC9);
            this.tabPage1.Controls.Add(this.label127);
            this.tabPage1.Controls.Add(this.circleUC8);
            this.tabPage1.Controls.Add(this.circleUC7);
            this.tabPage1.Controls.Add(this.label126);
            this.tabPage1.Controls.Add(this.circleUC6);
            this.tabPage1.Controls.Add(this.label38);
            this.tabPage1.Controls.Add(this.label163);
            this.tabPage1.Controls.Add(this.label37);
            this.tabPage1.Controls.Add(this.circleUC5);
            this.tabPage1.Font = new System.Drawing.Font("微軟正黑體", 16F);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(1902, 871);
            this.tabPage1.TabIndex = 2;
            this.tabPage1.Text = "Status";
            // 
            // circleUC51
            // 
            this.circleUC51.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC51.ExtenderWidth = 3;
            this.circleUC51.Fill = false;
            this.circleUC51.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.circleUC51.GroupID = null;
            this.circleUC51.Location = new System.Drawing.Point(403, 390);
            this.circleUC51.Margin = new System.Windows.Forms.Padding(24, 36, 24, 36);
            this.circleUC51.Name = "circleUC51";
            this.circleUC51.Size = new System.Drawing.Size(15, 15);
            this.circleUC51.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC51.TabIndex = 174;
            // 
            // circleUC50
            // 
            this.circleUC50.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC50.ExtenderWidth = 3;
            this.circleUC50.Fill = false;
            this.circleUC50.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.circleUC50.GroupID = null;
            this.circleUC50.Location = new System.Drawing.Point(403, 365);
            this.circleUC50.Margin = new System.Windows.Forms.Padding(24, 36, 24, 36);
            this.circleUC50.Name = "circleUC50";
            this.circleUC50.Size = new System.Drawing.Size(15, 15);
            this.circleUC50.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC50.TabIndex = 173;
            // 
            // circleUC49
            // 
            this.circleUC49.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC49.ExtenderWidth = 3;
            this.circleUC49.Fill = false;
            this.circleUC49.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.circleUC49.GroupID = null;
            this.circleUC49.Location = new System.Drawing.Point(403, 340);
            this.circleUC49.Margin = new System.Windows.Forms.Padding(24, 36, 24, 36);
            this.circleUC49.Name = "circleUC49";
            this.circleUC49.Size = new System.Drawing.Size(15, 15);
            this.circleUC49.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC49.TabIndex = 172;
            // 
            // circleUC48
            // 
            this.circleUC48.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC48.ExtenderWidth = 3;
            this.circleUC48.Fill = false;
            this.circleUC48.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.circleUC48.GroupID = null;
            this.circleUC48.Location = new System.Drawing.Point(403, 315);
            this.circleUC48.Margin = new System.Windows.Forms.Padding(24, 36, 24, 36);
            this.circleUC48.Name = "circleUC48";
            this.circleUC48.Size = new System.Drawing.Size(15, 15);
            this.circleUC48.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC48.TabIndex = 171;
            // 
            // circleUC46
            // 
            this.circleUC46.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC46.ExtenderWidth = 3;
            this.circleUC46.Fill = false;
            this.circleUC46.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.circleUC46.GroupID = null;
            this.circleUC46.Location = new System.Drawing.Point(403, 290);
            this.circleUC46.Margin = new System.Windows.Forms.Padding(24, 36, 24, 36);
            this.circleUC46.Name = "circleUC46";
            this.circleUC46.Size = new System.Drawing.Size(15, 15);
            this.circleUC46.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC46.TabIndex = 170;
            // 
            // circleUC45
            // 
            this.circleUC45.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC45.ExtenderWidth = 3;
            this.circleUC45.Fill = false;
            this.circleUC45.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.circleUC45.GroupID = null;
            this.circleUC45.Location = new System.Drawing.Point(403, 265);
            this.circleUC45.Margin = new System.Windows.Forms.Padding(24, 36, 24, 36);
            this.circleUC45.Name = "circleUC45";
            this.circleUC45.Size = new System.Drawing.Size(15, 15);
            this.circleUC45.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC45.TabIndex = 169;
            // 
            // circleUC44
            // 
            this.circleUC44.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC44.ExtenderWidth = 3;
            this.circleUC44.Fill = false;
            this.circleUC44.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.circleUC44.GroupID = null;
            this.circleUC44.Location = new System.Drawing.Point(403, 240);
            this.circleUC44.Margin = new System.Windows.Forms.Padding(24, 36, 24, 36);
            this.circleUC44.Name = "circleUC44";
            this.circleUC44.Size = new System.Drawing.Size(15, 15);
            this.circleUC44.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC44.TabIndex = 168;
            // 
            // circleUC42
            // 
            this.circleUC42.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC42.ExtenderWidth = 3;
            this.circleUC42.Fill = false;
            this.circleUC42.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.circleUC42.GroupID = null;
            this.circleUC42.Location = new System.Drawing.Point(403, 218);
            this.circleUC42.Margin = new System.Windows.Forms.Padding(24, 36, 24, 36);
            this.circleUC42.Name = "circleUC42";
            this.circleUC42.Size = new System.Drawing.Size(15, 15);
            this.circleUC42.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC42.TabIndex = 167;
            // 
            // circleUC34
            // 
            this.circleUC34.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC34.ExtenderWidth = 3;
            this.circleUC34.Fill = false;
            this.circleUC34.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.circleUC34.GroupID = null;
            this.circleUC34.Location = new System.Drawing.Point(403, 190);
            this.circleUC34.Margin = new System.Windows.Forms.Padding(24, 36, 24, 36);
            this.circleUC34.Name = "circleUC34";
            this.circleUC34.Size = new System.Drawing.Size(15, 15);
            this.circleUC34.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC34.TabIndex = 166;
            // 
            // circleUC33
            // 
            this.circleUC33.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC33.ExtenderWidth = 3;
            this.circleUC33.Fill = false;
            this.circleUC33.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.circleUC33.GroupID = null;
            this.circleUC33.Location = new System.Drawing.Point(403, 165);
            this.circleUC33.Margin = new System.Windows.Forms.Padding(24, 36, 24, 36);
            this.circleUC33.Name = "circleUC33";
            this.circleUC33.Size = new System.Drawing.Size(15, 15);
            this.circleUC33.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC33.TabIndex = 165;
            // 
            // circleUC32
            // 
            this.circleUC32.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC32.ExtenderWidth = 3;
            this.circleUC32.Fill = false;
            this.circleUC32.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.circleUC32.GroupID = null;
            this.circleUC32.Location = new System.Drawing.Point(403, 140);
            this.circleUC32.Margin = new System.Windows.Forms.Padding(24, 36, 24, 36);
            this.circleUC32.Name = "circleUC32";
            this.circleUC32.Size = new System.Drawing.Size(15, 15);
            this.circleUC32.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC32.TabIndex = 164;
            // 
            // circleUC31
            // 
            this.circleUC31.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC31.ExtenderWidth = 3;
            this.circleUC31.Fill = false;
            this.circleUC31.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.circleUC31.GroupID = null;
            this.circleUC31.Location = new System.Drawing.Point(403, 90);
            this.circleUC31.Margin = new System.Windows.Forms.Padding(24, 36, 24, 36);
            this.circleUC31.Name = "circleUC31";
            this.circleUC31.Size = new System.Drawing.Size(15, 15);
            this.circleUC31.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC31.TabIndex = 163;
            // 
            // circleUC30
            // 
            this.circleUC30.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC30.ExtenderWidth = 3;
            this.circleUC30.Fill = false;
            this.circleUC30.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.circleUC30.GroupID = null;
            this.circleUC30.Location = new System.Drawing.Point(403, 65);
            this.circleUC30.Margin = new System.Windows.Forms.Padding(24, 36, 24, 36);
            this.circleUC30.Name = "circleUC30";
            this.circleUC30.Size = new System.Drawing.Size(15, 15);
            this.circleUC30.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC30.TabIndex = 162;
            // 
            // circleUC29
            // 
            this.circleUC29.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC29.ExtenderWidth = 3;
            this.circleUC29.Fill = false;
            this.circleUC29.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.circleUC29.GroupID = null;
            this.circleUC29.Location = new System.Drawing.Point(403, 115);
            this.circleUC29.Margin = new System.Windows.Forms.Padding(24, 36, 24, 36);
            this.circleUC29.Name = "circleUC29";
            this.circleUC29.Size = new System.Drawing.Size(15, 15);
            this.circleUC29.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC29.TabIndex = 161;
            // 
            // circleUC35
            // 
            this.circleUC35.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC35.ExtenderWidth = 3;
            this.circleUC35.Fill = false;
            this.circleUC35.GroupID = null;
            this.circleUC35.Location = new System.Drawing.Point(403, 40);
            this.circleUC35.Margin = new System.Windows.Forms.Padding(12, 16, 12, 16);
            this.circleUC35.Name = "circleUC35";
            this.circleUC35.Size = new System.Drawing.Size(15, 15);
            this.circleUC35.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC35.TabIndex = 160;
            // 
            // circleUC28
            // 
            this.circleUC28.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC28.ExtenderWidth = 3;
            this.circleUC28.Fill = false;
            this.circleUC28.GroupID = null;
            this.circleUC28.Location = new System.Drawing.Point(12, 615);
            this.circleUC28.Margin = new System.Windows.Forms.Padding(12582912, 176720532, 12582912, 176720532);
            this.circleUC28.Name = "circleUC28";
            this.circleUC28.Size = new System.Drawing.Size(15, 15);
            this.circleUC28.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC28.TabIndex = 153;
            // 
            // circleUC27
            // 
            this.circleUC27.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC27.ExtenderWidth = 3;
            this.circleUC27.Fill = false;
            this.circleUC27.GroupID = null;
            this.circleUC27.Location = new System.Drawing.Point(12, 590);
            this.circleUC27.Margin = new System.Windows.Forms.Padding(6291456, 78542448, 6291456, 78542448);
            this.circleUC27.Name = "circleUC27";
            this.circleUC27.Size = new System.Drawing.Size(15, 15);
            this.circleUC27.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC27.TabIndex = 152;
            // 
            // label262
            // 
            this.label262.AutoSize = true;
            this.label262.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label262.Location = new System.Drawing.Point(1693, 851);
            this.label262.Name = "label262";
            this.label262.Size = new System.Drawing.Size(0, 18);
            this.label262.TabIndex = 151;
            // 
            // label198
            // 
            this.label198.AutoSize = true;
            this.label198.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label198.Location = new System.Drawing.Point(431, 390);
            this.label198.Name = "label198";
            this.label198.Size = new System.Drawing.Size(196, 18);
            this.label198.TabIndex = 56;
            this.label198.Text = "COM-UPS COM UPS ALARM\r\n";
            // 
            // label201
            // 
            this.label201.AutoSize = true;
            this.label201.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label201.Location = new System.Drawing.Point(431, 365);
            this.label201.Name = "label201";
            this.label201.Size = new System.Drawing.Size(188, 18);
            this.label201.TabIndex = 53;
            this.label201.Text = "COM-UPS COM UPS FAULT\r\n";
            // 
            // label202
            // 
            this.label202.AutoSize = true;
            this.label202.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label202.Location = new System.Drawing.Point(431, 340);
            this.label202.Name = "label202";
            this.label202.Size = new System.Drawing.Size(182, 18);
            this.label202.TabIndex = 52;
            this.label202.Text = "ESN-UPS ESN UPS ALARM\r\n";
            // 
            // label203
            // 
            this.label203.AutoSize = true;
            this.label203.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label203.Location = new System.Drawing.Point(431, 315);
            this.label203.Name = "label203";
            this.label203.Size = new System.Drawing.Size(174, 18);
            this.label203.TabIndex = 51;
            this.label203.Text = "ESN-UPS ESN UPS FAULT\r\n";
            // 
            // label204
            // 
            this.label204.AutoSize = true;
            this.label204.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label204.Location = new System.Drawing.Point(431, 290);
            this.label204.Name = "label204";
            this.label204.Size = new System.Drawing.Size(223, 18);
            this.label204.TabIndex = 50;
            this.label204.Text = "ESN-UPS Air conditoning FAULT\r\n";
            // 
            // label164
            // 
            this.label164.AutoSize = true;
            this.label164.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label164.Location = new System.Drawing.Point(431, 265);
            this.label164.Name = "label164";
            this.label164.Size = new System.Drawing.Size(118, 18);
            this.label164.TabIndex = 49;
            this.label164.Text = "MPP AC220V OK\r\n";
            // 
            // label165
            // 
            this.label165.AutoSize = true;
            this.label165.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label165.Location = new System.Drawing.Point(431, 240);
            this.label165.Name = "label165";
            this.label165.Size = new System.Drawing.Size(225, 18);
            this.label165.TabIndex = 48;
            this.label165.Text = "MC61 VOLTAGE AVALIABLE OUT\r\n";
            // 
            // label166
            // 
            this.label166.AutoSize = true;
            this.label166.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label166.Location = new System.Drawing.Point(431, 215);
            this.label166.Name = "label166";
            this.label166.Size = new System.Drawing.Size(211, 18);
            this.label166.TabIndex = 47;
            this.label166.Text = "MC61 VOLTAGE AVALIABLE IN";
            // 
            // label167
            // 
            this.label167.AutoSize = true;
            this.label167.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label167.Location = new System.Drawing.Point(431, 190);
            this.label167.Name = "label167";
            this.label167.Size = new System.Drawing.Size(151, 18);
            this.label167.TabIndex = 46;
            this.label167.Text = "MC61 DOOR CLOSED";
            // 
            // label168
            // 
            this.label168.AutoSize = true;
            this.label168.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label168.Location = new System.Drawing.Point(431, 115);
            this.label168.Name = "label168";
            this.label168.Size = new System.Drawing.Size(116, 18);
            this.label168.TabIndex = 44;
            this.label168.Text = "MC61 MCB TRIP";
            // 
            // label169
            // 
            this.label169.AutoSize = true;
            this.label169.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label169.Location = new System.Drawing.Point(431, 165);
            this.label169.Name = "label169";
            this.label169.Size = new System.Drawing.Size(178, 18);
            this.label169.TabIndex = 43;
            this.label169.Text = "MC61 FRAME FAULT TRIP";
            // 
            // label170
            // 
            this.label170.AutoSize = true;
            this.label170.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label170.Location = new System.Drawing.Point(431, 140);
            this.label170.Name = "label170";
            this.label170.Size = new System.Drawing.Size(157, 18);
            this.label170.TabIndex = 42;
            this.label170.Text = "MC61 REMOTE MODE";
            // 
            // label171
            // 
            this.label171.AutoSize = true;
            this.label171.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label171.Location = new System.Drawing.Point(431, 90);
            this.label171.Name = "label171";
            this.label171.Size = new System.Drawing.Size(196, 18);
            this.label171.TabIndex = 41;
            this.label171.Text = "MF80 OVERLOAD THERMAL";
            // 
            // label172
            // 
            this.label172.AutoSize = true;
            this.label172.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label172.Location = new System.Drawing.Point(431, 65);
            this.label172.Name = "label172";
            this.label172.Size = new System.Drawing.Size(209, 18);
            this.label172.TabIndex = 45;
            this.label172.Text = "MF80 Umn(RL-BWE) TIMEOUT";
            // 
            // label173
            // 
            this.label173.AutoSize = true;
            this.label173.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label173.Location = new System.Drawing.Point(431, 40);
            this.label173.Name = "label173";
            this.label173.Size = new System.Drawing.Size(277, 18);
            this.label173.TabIndex = 40;
            this.label173.Text = "MF80 VOLTAGE LIMITING DEVICE FAULT";
            // 
            // label180
            // 
            this.label180.AutoSize = true;
            this.label180.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label180.Location = new System.Drawing.Point(40, 615);
            this.label180.Name = "label180";
            this.label180.Size = new System.Drawing.Size(211, 18);
            this.label180.TabIndex = 33;
            this.label180.Text = "MC63 VOLTAGE AVAILABLE IN";
            // 
            // label181
            // 
            this.label181.AutoSize = true;
            this.label181.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label181.Location = new System.Drawing.Point(40, 590);
            this.label181.Name = "label181";
            this.label181.Size = new System.Drawing.Size(210, 18);
            this.label181.TabIndex = 32;
            this.label181.Text = "MC63 HANDCRANK PLUGGED";
            // 
            // label162
            // 
            this.label162.AutoSize = true;
            this.label162.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label162.Location = new System.Drawing.Point(40, 540);
            this.label162.Name = "label162";
            this.label162.Size = new System.Drawing.Size(157, 18);
            this.label162.TabIndex = 28;
            this.label162.Text = "MC63 REMOTE MODE";
            // 
            // label161
            // 
            this.label161.AutoSize = true;
            this.label161.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label161.Location = new System.Drawing.Point(40, 515);
            this.label161.Name = "label161";
            this.label161.Size = new System.Drawing.Size(129, 18);
            this.label161.TabIndex = 27;
            this.label161.Text = "MC63 MCB FAULT\r\n";
            // 
            // circleUC26
            // 
            this.circleUC26.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC26.ExtenderWidth = 3;
            this.circleUC26.Fill = false;
            this.circleUC26.GroupID = null;
            this.circleUC26.Location = new System.Drawing.Point(12, 565);
            this.circleUC26.Margin = new System.Windows.Forms.Padding(3145728, 34907757, 3145728, 34907757);
            this.circleUC26.Name = "circleUC26";
            this.circleUC26.Size = new System.Drawing.Size(15, 15);
            this.circleUC26.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC26.TabIndex = 26;
            // 
            // circleUC25
            // 
            this.circleUC25.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC25.ExtenderWidth = 3;
            this.circleUC25.Fill = false;
            this.circleUC25.GroupID = null;
            this.circleUC25.Location = new System.Drawing.Point(12, 540);
            this.circleUC25.Margin = new System.Windows.Forms.Padding(1572864, 15514560, 1572864, 15514560);
            this.circleUC25.Name = "circleUC25";
            this.circleUC25.Size = new System.Drawing.Size(15, 15);
            this.circleUC25.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC25.TabIndex = 26;
            // 
            // circleUC24
            // 
            this.circleUC24.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC24.ExtenderWidth = 3;
            this.circleUC24.Fill = false;
            this.circleUC24.GroupID = null;
            this.circleUC24.Location = new System.Drawing.Point(12, 515);
            this.circleUC24.Margin = new System.Windows.Forms.Padding(786432, 6895359, 786432, 6895359);
            this.circleUC24.Name = "circleUC24";
            this.circleUC24.Size = new System.Drawing.Size(15, 15);
            this.circleUC24.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC24.TabIndex = 26;
            // 
            // label160
            // 
            this.label160.AutoSize = true;
            this.label160.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label160.Location = new System.Drawing.Point(40, 490);
            this.label160.Name = "label160";
            this.label160.Size = new System.Drawing.Size(168, 18);
            this.label160.TabIndex = 25;
            this.label160.Text = "MC64 FANS RUN START";
            // 
            // circleUC23
            // 
            this.circleUC23.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC23.ExtenderWidth = 3;
            this.circleUC23.Fill = false;
            this.circleUC23.GroupID = null;
            this.circleUC23.Location = new System.Drawing.Point(12, 490);
            this.circleUC23.Margin = new System.Windows.Forms.Padding(393216, 3064604, 393216, 3064604);
            this.circleUC23.Name = "circleUC23";
            this.circleUC23.Size = new System.Drawing.Size(15, 15);
            this.circleUC23.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC23.TabIndex = 24;
            // 
            // label159
            // 
            this.label159.AutoSize = true;
            this.label159.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label159.Location = new System.Drawing.Point(40, 465);
            this.label159.Name = "label159";
            this.label159.Size = new System.Drawing.Size(183, 18);
            this.label159.TabIndex = 23;
            this.label159.Text = "MC64 CONTACTOR FAULT\r\n";
            // 
            // circleUC22
            // 
            this.circleUC22.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC22.ExtenderWidth = 3;
            this.circleUC22.Fill = false;
            this.circleUC22.GroupID = null;
            this.circleUC22.Location = new System.Drawing.Point(12, 465);
            this.circleUC22.Margin = new System.Windows.Forms.Padding(196608, 1362046, 196608, 1362046);
            this.circleUC22.Name = "circleUC22";
            this.circleUC22.Size = new System.Drawing.Size(15, 15);
            this.circleUC22.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC22.TabIndex = 22;
            // 
            // label156
            // 
            this.label156.AutoSize = true;
            this.label156.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label156.Location = new System.Drawing.Point(40, 390);
            this.label156.Name = "label156";
            this.label156.Size = new System.Drawing.Size(214, 18);
            this.label156.TabIndex = 21;
            this.label156.Text = "G9 POWER SUPPLY DC OUT OK\r\n";
            // 
            // label158
            // 
            this.label158.AutoSize = true;
            this.label158.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label158.Location = new System.Drawing.Point(40, 440);
            this.label158.Name = "label158";
            this.label158.Size = new System.Drawing.Size(172, 18);
            this.label158.TabIndex = 21;
            this.label158.Text = "MC63 SAFETY RELAY ON\r\n";
            // 
            // label157
            // 
            this.label157.AutoSize = true;
            this.label157.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label157.Location = new System.Drawing.Point(40, 415);
            this.label157.Name = "label157";
            this.label157.Size = new System.Drawing.Size(172, 18);
            this.label157.TabIndex = 21;
            this.label157.Text = "MC62 SAFETY RELAY ON";
            // 
            // label137
            // 
            this.label137.AutoSize = true;
            this.label137.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label137.Location = new System.Drawing.Point(40, 365);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(92, 18);
            this.label137.TabIndex = 21;
            this.label137.Text = "DOOR OPEN";
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label136.Location = new System.Drawing.Point(40, 340);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(145, 18);
            this.label136.TabIndex = 21;
            this.label136.Text = "ENABLE SWITCH ON";
            // 
            // label135
            // 
            this.label135.AutoSize = true;
            this.label135.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label135.Location = new System.Drawing.Point(40, 315);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(179, 18);
            this.label135.TabIndex = 21;
            this.label135.Text = "EMERGENCY STOP PB ON";
            // 
            // label134
            // 
            this.label134.AutoSize = true;
            this.label134.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label134.Location = new System.Drawing.Point(40, 290);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(177, 18);
            this.label134.TabIndex = 21;
            this.label134.Text = "G6 CHARGER DC OUT OK\r\n";
            // 
            // circleUC21
            // 
            this.circleUC21.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC21.ExtenderWidth = 3;
            this.circleUC21.Fill = false;
            this.circleUC21.GroupID = null;
            this.circleUC21.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.circleUC21.Location = new System.Drawing.Point(12, 440);
            this.circleUC21.Margin = new System.Windows.Forms.Padding(98304, 605354, 98304, 605354);
            this.circleUC21.Name = "circleUC21";
            this.circleUC21.Size = new System.Drawing.Size(15, 15);
            this.circleUC21.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC21.TabIndex = 20;
            // 
            // circleUC20
            // 
            this.circleUC20.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC20.ExtenderWidth = 3;
            this.circleUC20.Fill = false;
            this.circleUC20.GroupID = null;
            this.circleUC20.Location = new System.Drawing.Point(12, 415);
            this.circleUC20.Margin = new System.Windows.Forms.Padding(49152, 269046, 49152, 269046);
            this.circleUC20.Name = "circleUC20";
            this.circleUC20.Size = new System.Drawing.Size(15, 15);
            this.circleUC20.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC20.TabIndex = 20;
            // 
            // circleUC19
            // 
            this.circleUC19.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC19.ExtenderWidth = 3;
            this.circleUC19.Fill = false;
            this.circleUC19.GroupID = null;
            this.circleUC19.Location = new System.Drawing.Point(12, 390);
            this.circleUC19.Margin = new System.Windows.Forms.Padding(24576, 119576, 24576, 119576);
            this.circleUC19.Name = "circleUC19";
            this.circleUC19.Size = new System.Drawing.Size(15, 15);
            this.circleUC19.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC19.TabIndex = 20;
            // 
            // circleUC18
            // 
            this.circleUC18.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC18.ExtenderWidth = 3;
            this.circleUC18.Fill = false;
            this.circleUC18.GroupID = null;
            this.circleUC18.Location = new System.Drawing.Point(12, 365);
            this.circleUC18.Margin = new System.Windows.Forms.Padding(12288, 53145, 12288, 53145);
            this.circleUC18.Name = "circleUC18";
            this.circleUC18.Size = new System.Drawing.Size(15, 15);
            this.circleUC18.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC18.TabIndex = 20;
            // 
            // circleUC17
            // 
            this.circleUC17.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC17.ExtenderWidth = 3;
            this.circleUC17.Fill = false;
            this.circleUC17.GroupID = null;
            this.circleUC17.Location = new System.Drawing.Point(12, 340);
            this.circleUC17.Margin = new System.Windows.Forms.Padding(6144, 23620, 6144, 23620);
            this.circleUC17.Name = "circleUC17";
            this.circleUC17.Size = new System.Drawing.Size(15, 15);
            this.circleUC17.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC17.TabIndex = 20;
            // 
            // circleUC16
            // 
            this.circleUC16.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC16.ExtenderWidth = 3;
            this.circleUC16.Fill = false;
            this.circleUC16.GroupID = null;
            this.circleUC16.Location = new System.Drawing.Point(12, 315);
            this.circleUC16.Margin = new System.Windows.Forms.Padding(3072, 10498, 3072, 10498);
            this.circleUC16.Name = "circleUC16";
            this.circleUC16.Size = new System.Drawing.Size(15, 15);
            this.circleUC16.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC16.TabIndex = 20;
            // 
            // circleUC15
            // 
            this.circleUC15.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC15.ExtenderWidth = 3;
            this.circleUC15.Fill = false;
            this.circleUC15.GroupID = null;
            this.circleUC15.Location = new System.Drawing.Point(12, 290);
            this.circleUC15.Margin = new System.Windows.Forms.Padding(1536, 4666, 1536, 4666);
            this.circleUC15.Name = "circleUC15";
            this.circleUC15.Size = new System.Drawing.Size(15, 15);
            this.circleUC15.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC15.TabIndex = 20;
            // 
            // label133
            // 
            this.label133.AutoSize = true;
            this.label133.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label133.Location = new System.Drawing.Point(40, 265);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(184, 18);
            this.label133.TabIndex = 19;
            this.label133.Text = "G6 CHARGER BATTERY OK\r\n";
            // 
            // circleUC14
            // 
            this.circleUC14.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC14.ExtenderWidth = 3;
            this.circleUC14.Fill = false;
            this.circleUC14.GroupID = null;
            this.circleUC14.Location = new System.Drawing.Point(12, 265);
            this.circleUC14.Margin = new System.Windows.Forms.Padding(768, 2074, 768, 2074);
            this.circleUC14.Name = "circleUC14";
            this.circleUC14.Size = new System.Drawing.Size(15, 15);
            this.circleUC14.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC14.TabIndex = 18;
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label132.Location = new System.Drawing.Point(40, 240);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(163, 18);
            this.label132.TabIndex = 17;
            this.label132.Text = "G6 CHARGER DC IN OK";
            // 
            // circleUC13
            // 
            this.circleUC13.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC13.ExtenderWidth = 3;
            this.circleUC13.Fill = false;
            this.circleUC13.GroupID = null;
            this.circleUC13.Location = new System.Drawing.Point(12, 240);
            this.circleUC13.Margin = new System.Windows.Forms.Padding(384, 922, 384, 922);
            this.circleUC13.Name = "circleUC13";
            this.circleUC13.Size = new System.Drawing.Size(15, 15);
            this.circleUC13.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC13.TabIndex = 16;
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label131.Location = new System.Drawing.Point(40, 215);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(177, 18);
            this.label131.TabIndex = 15;
            this.label131.Text = "G5 CHARGER DC OUT OK\r\n";
            // 
            // circleUC12
            // 
            this.circleUC12.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC12.ExtenderWidth = 3;
            this.circleUC12.Fill = false;
            this.circleUC12.GroupID = null;
            this.circleUC12.Location = new System.Drawing.Point(12, 215);
            this.circleUC12.Margin = new System.Windows.Forms.Padding(192, 410, 192, 410);
            this.circleUC12.Name = "circleUC12";
            this.circleUC12.Size = new System.Drawing.Size(15, 15);
            this.circleUC12.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC12.TabIndex = 14;
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label130.Location = new System.Drawing.Point(40, 190);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(184, 18);
            this.label130.TabIndex = 13;
            this.label130.Text = "G5 CHARGER BATTERY OK";
            // 
            // circleUC11
            // 
            this.circleUC11.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC11.ExtenderWidth = 3;
            this.circleUC11.Fill = false;
            this.circleUC11.GroupID = null;
            this.circleUC11.Location = new System.Drawing.Point(12, 190);
            this.circleUC11.Margin = new System.Windows.Forms.Padding(96, 182, 96, 182);
            this.circleUC11.Name = "circleUC11";
            this.circleUC11.Size = new System.Drawing.Size(15, 15);
            this.circleUC11.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC11.TabIndex = 12;
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label129.Location = new System.Drawing.Point(40, 165);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(163, 18);
            this.label129.TabIndex = 11;
            this.label129.Text = "G5 CHARGER DC IN OK";
            // 
            // circleUC10
            // 
            this.circleUC10.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC10.ExtenderWidth = 3;
            this.circleUC10.Fill = false;
            this.circleUC10.GroupID = null;
            this.circleUC10.Location = new System.Drawing.Point(12, 165);
            this.circleUC10.Margin = new System.Windows.Forms.Padding(48, 81, 48, 81);
            this.circleUC10.Name = "circleUC10";
            this.circleUC10.Size = new System.Drawing.Size(15, 15);
            this.circleUC10.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC10.TabIndex = 10;
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label128.Location = new System.Drawing.Point(40, 140);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(149, 18);
            this.label128.TabIndex = 9;
            this.label128.Text = "BCM-NG SYSTEM OK";
            // 
            // circleUC9
            // 
            this.circleUC9.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC9.ExtenderWidth = 3;
            this.circleUC9.Fill = false;
            this.circleUC9.GroupID = null;
            this.circleUC9.Location = new System.Drawing.Point(12, 140);
            this.circleUC9.Margin = new System.Windows.Forms.Padding(48, 81, 48, 81);
            this.circleUC9.Name = "circleUC9";
            this.circleUC9.Size = new System.Drawing.Size(15, 15);
            this.circleUC9.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC9.TabIndex = 8;
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label127.Location = new System.Drawing.Point(40, 115);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(162, 18);
            this.label127.TabIndex = 7;
            this.label127.Text = "DC24V MCCB OFF/TRIP";
            // 
            // circleUC8
            // 
            this.circleUC8.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC8.ExtenderWidth = 3;
            this.circleUC8.Fill = false;
            this.circleUC8.GroupID = null;
            this.circleUC8.Location = new System.Drawing.Point(12, 115);
            this.circleUC8.Margin = new System.Windows.Forms.Padding(24, 36, 24, 36);
            this.circleUC8.Name = "circleUC8";
            this.circleUC8.Size = new System.Drawing.Size(15, 15);
            this.circleUC8.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC8.TabIndex = 6;
            // 
            // circleUC7
            // 
            this.circleUC7.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC7.ExtenderWidth = 3;
            this.circleUC7.Fill = false;
            this.circleUC7.GroupID = null;
            this.circleUC7.Location = new System.Drawing.Point(12, 90);
            this.circleUC7.Margin = new System.Windows.Forms.Padding(12, 16, 12, 16);
            this.circleUC7.Name = "circleUC7";
            this.circleUC7.Size = new System.Drawing.Size(15, 15);
            this.circleUC7.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC7.TabIndex = 5;
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label126.Location = new System.Drawing.Point(40, 90);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(208, 18);
            this.label126.TabIndex = 4;
            this.label126.Text = "AC110V/220V MCCB OFF/TRIP";
            // 
            // circleUC6
            // 
            this.circleUC6.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC6.ExtenderWidth = 3;
            this.circleUC6.Fill = false;
            this.circleUC6.GroupID = null;
            this.circleUC6.Location = new System.Drawing.Point(12, 65);
            this.circleUC6.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.circleUC6.Name = "circleUC6";
            this.circleUC6.Size = new System.Drawing.Size(15, 15);
            this.circleUC6.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC6.TabIndex = 3;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label38.Location = new System.Drawing.Point(40, 65);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(124, 18);
            this.label38.TabIndex = 2;
            this.label38.Text = "台電 AC220V 異常";
            // 
            // label163
            // 
            this.label163.AutoSize = true;
            this.label163.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label163.Location = new System.Drawing.Point(40, 565);
            this.label163.Name = "label163";
            this.label163.Size = new System.Drawing.Size(145, 18);
            this.label163.TabIndex = 2;
            this.label163.Text = "MC63 FRAME FAULT";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("微軟正黑體", 10F);
            this.label37.Location = new System.Drawing.Point(40, 40);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(122, 18);
            this.label37.TabIndex = 2;
            this.label37.Text = "UPS AC220V 異常";
            // 
            // circleUC5
            // 
            this.circleUC5.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.circleUC5.ExtenderWidth = 3;
            this.circleUC5.Fill = false;
            this.circleUC5.GroupID = null;
            this.circleUC5.Location = new System.Drawing.Point(12, 40);
            this.circleUC5.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.circleUC5.Name = "circleUC5";
            this.circleUC5.Size = new System.Drawing.Size(15, 15);
            this.circleUC5.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.circleUC5.TabIndex = 1;
            // 
            // Form_StationC17
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1910, 656);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form_StationC17";
            this.Text = "Form_StationC17";
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label11;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC47;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC37;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC36;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC35;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC4;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC43;
        private System.Windows.Forms.Label label15;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC24;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC25;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC27;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC28;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC31;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC32;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC34;
        private System.Windows.Forms.Label label6;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC37;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC38;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC5;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC14;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC15;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC39;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC40;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC16;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC17;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC18;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC19;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC20;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC21;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC22;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC41;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC23;
        private iSCADA.Design.Utilities.Electrical.Switch05UC switch05UC2;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC2;
        private System.Windows.Forms.Label label5;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC36;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC13;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC12;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC11;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC3;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC2;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC10;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC9;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC8;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC7;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC6;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC4;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC3;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC1;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC2;
        private iSCADA.Design.Utilities.Electrical.Switch05UC switch05UC1;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC1;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.Label label120;
        private System.Windows.Forms.Label label122;
        private System.Windows.Forms.Label label124;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label66;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC14;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label39;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC13;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC1;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC5;
        private iSCADA.Design.Utilities.Electrical.Switch01UC switch01UC1;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC3;
        private System.Windows.Forms.TabPage tabPage1;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC51;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC50;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC49;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC48;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC46;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC45;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC44;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC42;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC34;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC33;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC32;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC31;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC30;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC29;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC35;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC28;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC27;
        private System.Windows.Forms.Label label262;
        private System.Windows.Forms.Label label198;
        private System.Windows.Forms.Label label201;
        private System.Windows.Forms.Label label202;
        private System.Windows.Forms.Label label203;
        private System.Windows.Forms.Label label204;
        private System.Windows.Forms.Label label164;
        private System.Windows.Forms.Label label165;
        private System.Windows.Forms.Label label166;
        private System.Windows.Forms.Label label167;
        private System.Windows.Forms.Label label168;
        private System.Windows.Forms.Label label169;
        private System.Windows.Forms.Label label170;
        private System.Windows.Forms.Label label171;
        private System.Windows.Forms.Label label172;
        private System.Windows.Forms.Label label173;
        private System.Windows.Forms.Label label180;
        private System.Windows.Forms.Label label181;
        private System.Windows.Forms.Label label162;
        private System.Windows.Forms.Label label161;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC26;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC25;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC24;
        private System.Windows.Forms.Label label160;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC23;
        private System.Windows.Forms.Label label159;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC22;
        private System.Windows.Forms.Label label156;
        private System.Windows.Forms.Label label158;
        private System.Windows.Forms.Label label157;
        private System.Windows.Forms.Label label137;
        private System.Windows.Forms.Label label136;
        private System.Windows.Forms.Label label135;
        private System.Windows.Forms.Label label134;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC21;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC20;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC19;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC18;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC17;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC16;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC15;
        private System.Windows.Forms.Label label133;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC14;
        private System.Windows.Forms.Label label132;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC13;
        private System.Windows.Forms.Label label131;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC12;
        private System.Windows.Forms.Label label130;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC11;
        private System.Windows.Forms.Label label129;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC10;
        private System.Windows.Forms.Label label128;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC9;
        private System.Windows.Forms.Label label127;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC8;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC7;
        private System.Windows.Forms.Label label126;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC6;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label163;
        private System.Windows.Forms.Label label37;
        private iSCADA.Design.Utilities.Electrical.CircleUC circleUC5;
    }
}