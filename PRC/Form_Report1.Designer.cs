﻿namespace PRC
{
    partial class Form_Report1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series7 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.comboBox8 = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.comboBox6 = new System.Windows.Forms.ComboBox();
            this.comboBox7 = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBox5 = new System.Windows.Forms.ComboBox();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.radioYear = new System.Windows.Forms.RadioButton();
            this.radioQuarter = new System.Windows.Forms.RadioButton();
            this.radioMonth = new System.Windows.Forms.RadioButton();
            this.radioDay = new System.Windows.Forms.RadioButton();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.button13 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.rpt1_2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rpt1_3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rpt1_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rpt1_4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rpt1_5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rpt1_6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rpt1_7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rpt1_8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rpt1_9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rpt1_10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.chart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.rpt2_2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rpt2_3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rpt2_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rpt2_4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rpt2_5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rpt2_6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rpt2_7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rpt2_8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.rpt3_2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rpt3_3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rpt3_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rpt3_4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rpt3_5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rpt3_6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rpt3_7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rpt3_8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart3 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart3)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.groupBox3);
            this.panel2.Controls.Add(this.comboBox3);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.comboBox2);
            this.panel2.Controls.Add(this.label20);
            this.panel2.Controls.Add(this.comboBox1);
            this.panel2.Controls.Add(this.button13);
            this.panel2.Controls.Add(this.button12);
            this.panel2.Controls.Add(this.label23);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1904, 199);
            this.panel2.TabIndex = 11;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1738, 116);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(42, 42);
            this.button1.TabIndex = 24;
            this.button1.Text = "列印";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.comboBox8);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.comboBox6);
            this.groupBox3.Controls.Add(this.comboBox7);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.comboBox5);
            this.groupBox3.Controls.Add(this.comboBox4);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.dateTimePicker2);
            this.groupBox3.Controls.Add(this.dateTimePicker1);
            this.groupBox3.Controls.Add(this.radioYear);
            this.groupBox3.Controls.Add(this.radioQuarter);
            this.groupBox3.Controls.Add(this.radioMonth);
            this.groupBox3.Controls.Add(this.radioDay);
            this.groupBox3.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.groupBox3.ForeColor = System.Drawing.Color.White;
            this.groupBox3.Location = new System.Drawing.Point(719, 1);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(700, 195);
            this.groupBox3.TabIndex = 23;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "時間";
            // 
            // comboBox8
            // 
            this.comboBox8.Enabled = false;
            this.comboBox8.Font = new System.Drawing.Font("微軟正黑體", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.comboBox8.FormattingEnabled = true;
            this.comboBox8.Items.AddRange(new object[] {
            "2016",
            "2017",
            "2018"});
            this.comboBox8.Location = new System.Drawing.Point(170, 150);
            this.comboBox8.Name = "comboBox8";
            this.comboBox8.Size = new System.Drawing.Size(200, 35);
            this.comboBox8.TabIndex = 30;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(98, 151);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(54, 27);
            this.label8.TabIndex = 29;
            this.label8.Text = "年度";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(401, 112);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 27);
            this.label6.TabIndex = 28;
            this.label6.Text = "季";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(98, 112);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 27);
            this.label7.TabIndex = 27;
            this.label7.Text = "年度";
            // 
            // comboBox6
            // 
            this.comboBox6.Enabled = false;
            this.comboBox6.Font = new System.Drawing.Font("微軟正黑體", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.comboBox6.FormattingEnabled = true;
            this.comboBox6.Items.AddRange(new object[] {
            "第一季",
            "第二季",
            "第三季",
            "第四季"});
            this.comboBox6.Location = new System.Drawing.Point(449, 108);
            this.comboBox6.Name = "comboBox6";
            this.comboBox6.Size = new System.Drawing.Size(200, 35);
            this.comboBox6.TabIndex = 26;
            // 
            // comboBox7
            // 
            this.comboBox7.Enabled = false;
            this.comboBox7.Font = new System.Drawing.Font("微軟正黑體", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.comboBox7.FormattingEnabled = true;
            this.comboBox7.Items.AddRange(new object[] {
            "2016",
            "2017",
            "2018"});
            this.comboBox7.Location = new System.Drawing.Point(170, 108);
            this.comboBox7.Name = "comboBox7";
            this.comboBox7.Size = new System.Drawing.Size(200, 35);
            this.comboBox7.TabIndex = 25;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(401, 68);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 27);
            this.label5.TabIndex = 24;
            this.label5.Text = "月";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(98, 68);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 27);
            this.label4.TabIndex = 23;
            this.label4.Text = "年度";
            // 
            // comboBox5
            // 
            this.comboBox5.Enabled = false;
            this.comboBox5.Font = new System.Drawing.Font("微軟正黑體", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.comboBox5.FormattingEnabled = true;
            this.comboBox5.Items.AddRange(new object[] {
            "1月",
            "2月",
            "3月",
            "4月",
            "5月",
            "6月",
            "7月",
            "8月",
            "9月",
            "10月",
            "11月",
            "12月",
            ""});
            this.comboBox5.Location = new System.Drawing.Point(449, 64);
            this.comboBox5.Name = "comboBox5";
            this.comboBox5.Size = new System.Drawing.Size(200, 35);
            this.comboBox5.TabIndex = 22;
            // 
            // comboBox4
            // 
            this.comboBox4.Enabled = false;
            this.comboBox4.Font = new System.Drawing.Font("微軟正黑體", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Items.AddRange(new object[] {
            "2016",
            "2017",
            "2018"});
            this.comboBox4.Location = new System.Drawing.Point(170, 64);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(200, 35);
            this.comboBox4.TabIndex = 21;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(386, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 27);
            this.label3.TabIndex = 7;
            this.label3.Text = "~迄";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(98, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 27);
            this.label2.TabIndex = 6;
            this.label2.Text = "起";
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Enabled = false;
            this.dateTimePicker2.Location = new System.Drawing.Point(449, 21);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(200, 35);
            this.dateTimePicker2.TabIndex = 5;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Enabled = false;
            this.dateTimePicker1.Location = new System.Drawing.Point(170, 21);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 35);
            this.dateTimePicker1.TabIndex = 4;
            // 
            // radioYear
            // 
            this.radioYear.AutoSize = true;
            this.radioYear.Location = new System.Drawing.Point(26, 151);
            this.radioYear.Name = "radioYear";
            this.radioYear.Size = new System.Drawing.Size(72, 31);
            this.radioYear.TabIndex = 3;
            this.radioYear.TabStop = true;
            this.radioYear.Text = "年度";
            this.radioYear.UseVisualStyleBackColor = true;
            // 
            // radioQuarter
            // 
            this.radioQuarter.AutoSize = true;
            this.radioQuarter.Location = new System.Drawing.Point(26, 109);
            this.radioQuarter.Name = "radioQuarter";
            this.radioQuarter.Size = new System.Drawing.Size(51, 31);
            this.radioQuarter.TabIndex = 2;
            this.radioQuarter.TabStop = true;
            this.radioQuarter.Text = "季";
            this.radioQuarter.UseVisualStyleBackColor = true;
            // 
            // radioMonth
            // 
            this.radioMonth.AutoSize = true;
            this.radioMonth.Location = new System.Drawing.Point(26, 67);
            this.radioMonth.Name = "radioMonth";
            this.radioMonth.Size = new System.Drawing.Size(51, 31);
            this.radioMonth.TabIndex = 1;
            this.radioMonth.TabStop = true;
            this.radioMonth.Text = "月";
            this.radioMonth.UseVisualStyleBackColor = true;
            // 
            // radioDay
            // 
            this.radioDay.AutoSize = true;
            this.radioDay.Location = new System.Drawing.Point(26, 25);
            this.radioDay.Name = "radioDay";
            this.radioDay.Size = new System.Drawing.Size(51, 31);
            this.radioDay.TabIndex = 0;
            this.radioDay.TabStop = true;
            this.radioDay.Text = "日";
            this.radioDay.UseVisualStyleBackColor = true;
            // 
            // comboBox3
            // 
            this.comboBox3.Font = new System.Drawing.Font("微軟正黑體", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(340, 117);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(351, 35);
            this.comboBox3.TabIndex = 22;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微軟正黑體", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(225, 120);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 28);
            this.label1.TabIndex = 21;
            this.label1.Text = "設備點：";
            // 
            // comboBox2
            // 
            this.comboBox2.Font = new System.Drawing.Font("微軟正黑體", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "TSS",
            "     -TSS7",
            "     -TSS8",
            "     -TSS9",
            "     -TSS10",
            "     -TSS11",
            "     -TSS12",
            "     -DTSS1",
            "Station",
            "     -C15",
            "     -C16",
            "     -C17",
            "     -C18",
            "     -C19",
            "     -C20",
            "     -C21",
            "     -C22",
            "     -C23",
            "     -C24",
            "     -C25",
            "     -C26",
            "     -C27",
            "     -C28",
            "     -C29",
            "     -C30",
            "     -C31",
            "     -C32",
            "     -C33",
            "     -C34",
            "     -C35",
            "     -C36",
            "     -C37"});
            this.comboBox2.Location = new System.Drawing.Point(340, 69);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(351, 35);
            this.comboBox2.TabIndex = 20;
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("微軟正黑體", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label20.ForeColor = System.Drawing.Color.White;
            this.label20.Location = new System.Drawing.Point(225, 72);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(78, 28);
            this.label20.TabIndex = 19;
            this.label20.Text = "位置：";
            // 
            // comboBox1
            // 
            this.comboBox1.Font = new System.Drawing.Font("微軟正黑體", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "請選擇",
            "電壓、電流之最大/最小值",
            "各迴路尖離峰及總用電量",
            "電力用量分配圖"});
            this.comboBox1.Location = new System.Drawing.Point(340, 16);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(351, 35);
            this.comboBox1.TabIndex = 6;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // button13
            // 
            this.button13.BackgroundImage = global::PRC.Properties.Resources.musica_searcher;
            this.button13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button13.Location = new System.Drawing.Point(1506, 113);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(42, 42);
            this.button13.TabIndex = 5;
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // button12
            // 
            this.button12.BackgroundImage = global::PRC.Properties.Resources.download_symbol;
            this.button12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button12.Location = new System.Drawing.Point(1660, 113);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(42, 42);
            this.button12.TabIndex = 5;
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("微軟正黑體", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label23.ForeColor = System.Drawing.Color.White;
            this.label23.Location = new System.Drawing.Point(1434, 117);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(56, 28);
            this.label23.TabIndex = 0;
            this.label23.Text = "查詢";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("微軟正黑體", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label22.ForeColor = System.Drawing.Color.White;
            this.label22.Location = new System.Drawing.Point(1584, 117);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(56, 28);
            this.label22.TabIndex = 0;
            this.label22.Text = "下載";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("微軟正黑體", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label17.ForeColor = System.Drawing.Color.White;
            this.label17.Location = new System.Drawing.Point(225, 19);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(122, 28);
            this.label17.TabIndex = 0;
            this.label17.Text = "報表類別：";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.rpt1_2,
            this.rpt1_3,
            this.rpt1_1,
            this.rpt1_4,
            this.rpt1_5,
            this.rpt1_6,
            this.rpt1_7,
            this.rpt1_8,
            this.rpt1_9,
            this.rpt1_10});
            this.dataGridView1.Location = new System.Drawing.Point(57, 42);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(898, 586);
            this.dataGridView1.TabIndex = 12;
            // 
            // rpt1_2
            // 
            this.rpt1_2.DataPropertyName = "rpt1_2";
            this.rpt1_2.HeaderText = "年份(西元年)";
            this.rpt1_2.Name = "rpt1_2";
            this.rpt1_2.ReadOnly = true;
            // 
            // rpt1_3
            // 
            this.rpt1_3.DataPropertyName = "rpt1_3";
            this.rpt1_3.HeaderText = "月份";
            this.rpt1_3.Name = "rpt1_3";
            this.rpt1_3.ReadOnly = true;
            // 
            // rpt1_1
            // 
            this.rpt1_1.DataPropertyName = "rpt1_1";
            this.rpt1_1.HeaderText = "日期";
            this.rpt1_1.Name = "rpt1_1";
            this.rpt1_1.ReadOnly = true;
            // 
            // rpt1_4
            // 
            this.rpt1_4.DataPropertyName = "rpt1_4";
            this.rpt1_4.HeaderText = "時間(每小時)";
            this.rpt1_4.Name = "rpt1_4";
            this.rpt1_4.ReadOnly = true;
            this.rpt1_4.Width = 120;
            // 
            // rpt1_5
            // 
            this.rpt1_5.DataPropertyName = "rpt1_5";
            this.rpt1_5.HeaderText = "位置";
            this.rpt1_5.Name = "rpt1_5";
            this.rpt1_5.ReadOnly = true;
            // 
            // rpt1_6
            // 
            this.rpt1_6.DataPropertyName = "rpt1_6";
            this.rpt1_6.HeaderText = "點位";
            this.rpt1_6.Name = "rpt1_6";
            this.rpt1_6.ReadOnly = true;
            // 
            // rpt1_7
            // 
            this.rpt1_7.DataPropertyName = "rpt1_7";
            this.rpt1_7.HeaderText = "電壓最大(V)";
            this.rpt1_7.Name = "rpt1_7";
            this.rpt1_7.ReadOnly = true;
            this.rpt1_7.Width = 150;
            // 
            // rpt1_8
            // 
            this.rpt1_8.DataPropertyName = "rpt1_8";
            this.rpt1_8.HeaderText = "電壓最小(V)";
            this.rpt1_8.Name = "rpt1_8";
            this.rpt1_8.ReadOnly = true;
            this.rpt1_8.Width = 150;
            // 
            // rpt1_9
            // 
            this.rpt1_9.DataPropertyName = "rpt1_9";
            this.rpt1_9.HeaderText = "電流最大(A)";
            this.rpt1_9.Name = "rpt1_9";
            this.rpt1_9.ReadOnly = true;
            this.rpt1_9.Width = 150;
            // 
            // rpt1_10
            // 
            this.rpt1_10.DataPropertyName = "rpt1_10";
            this.rpt1_10.HeaderText = "電流最小(A)";
            this.rpt1_10.Name = "rpt1_10";
            this.rpt1_10.ReadOnly = true;
            this.rpt1_10.Width = 150;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(67, 205);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1837, 683);
            this.tabControl1.TabIndex = 16;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.chart3);
            this.tabPage1.Controls.Add(this.chart1);
            this.tabPage1.Controls.Add(this.chart2);
            this.tabPage1.Controls.Add(this.dataGridView1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1829, 657);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // chart2
            // 
            chartArea3.AxisX.Title = "時間";
            chartArea3.AxisY.Title = "電壓(V)";
            chartArea3.AxisY2.Title = "電流(A)";
            chartArea3.Name = "ChartArea1";
            this.chart2.ChartAreas.Add(chartArea3);
            legend3.Name = "Legend1";
            this.chart2.Legends.Add(legend3);
            this.chart2.Location = new System.Drawing.Point(1097, 42);
            this.chart2.Name = "chart2";
            series4.ChartArea = "ChartArea1";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            series4.Legend = "Legend1";
            series4.LegendText = "最大電壓";
            series4.Name = "Series1";
            series5.ChartArea = "ChartArea1";
            series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            series5.Legend = "Legend1";
            series5.LegendText = "最小電壓";
            series5.Name = "Series2";
            series6.ChartArea = "ChartArea1";
            series6.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            series6.Legend = "Legend1";
            series6.LegendText = "最大電流";
            series6.Name = "Series3";
            series7.ChartArea = "ChartArea1";
            series7.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            series7.Legend = "Legend1";
            series7.LegendText = "最小電流";
            series7.Name = "Series4";
            this.chart2.Series.Add(series4);
            this.chart2.Series.Add(series5);
            this.chart2.Series.Add(series6);
            this.chart2.Series.Add(series7);
            this.chart2.Size = new System.Drawing.Size(612, 586);
            this.chart2.TabIndex = 21;
            this.chart2.Text = "chart2";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dataGridView2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1829, 657);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.rpt2_2,
            this.rpt2_3,
            this.rpt2_1,
            this.rpt2_4,
            this.rpt2_5,
            this.rpt2_6,
            this.rpt2_7,
            this.rpt2_8});
            this.dataGridView2.Location = new System.Drawing.Point(57, 42);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.RowTemplate.Height = 24;
            this.dataGridView2.Size = new System.Drawing.Size(821, 586);
            this.dataGridView2.TabIndex = 19;
            // 
            // rpt2_2
            // 
            this.rpt2_2.DataPropertyName = "rpt2_2";
            this.rpt2_2.HeaderText = "年份(西元年)";
            this.rpt2_2.Name = "rpt2_2";
            this.rpt2_2.ReadOnly = true;
            // 
            // rpt2_3
            // 
            this.rpt2_3.DataPropertyName = "rpt2_3";
            this.rpt2_3.HeaderText = "月份";
            this.rpt2_3.Name = "rpt2_3";
            this.rpt2_3.ReadOnly = true;
            // 
            // rpt2_1
            // 
            this.rpt2_1.DataPropertyName = "rpt2_1";
            this.rpt2_1.HeaderText = "日期";
            this.rpt2_1.Name = "rpt2_1";
            this.rpt2_1.ReadOnly = true;
            // 
            // rpt2_4
            // 
            this.rpt2_4.DataPropertyName = "rpt2_4";
            this.rpt2_4.HeaderText = "時間(每小時)";
            this.rpt2_4.Name = "rpt2_4";
            this.rpt2_4.ReadOnly = true;
            this.rpt2_4.Width = 120;
            // 
            // rpt2_5
            // 
            this.rpt2_5.DataPropertyName = "rpt2_5";
            this.rpt2_5.HeaderText = "位置";
            this.rpt2_5.Name = "rpt2_5";
            this.rpt2_5.ReadOnly = true;
            // 
            // rpt2_6
            // 
            this.rpt2_6.DataPropertyName = "rpt2_6";
            this.rpt2_6.HeaderText = "點位";
            this.rpt2_6.Name = "rpt2_6";
            this.rpt2_6.ReadOnly = true;
            // 
            // rpt2_7
            // 
            this.rpt2_7.DataPropertyName = "rpt2_7";
            this.rpt2_7.HeaderText = "用電量";
            this.rpt2_7.Name = "rpt2_7";
            this.rpt2_7.ReadOnly = true;
            this.rpt2_7.Width = 150;
            // 
            // rpt2_8
            // 
            this.rpt2_8.DataPropertyName = "rpt2_8";
            this.rpt2_8.HeaderText = "累積用電量";
            this.rpt2_8.Name = "rpt2_8";
            this.rpt2_8.ReadOnly = true;
            this.rpt2_8.Width = 150;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.dataGridView3);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1829, 657);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "tabPage3";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // dataGridView3
            // 
            this.dataGridView3.AllowUserToAddRows = false;
            this.dataGridView3.AllowUserToDeleteRows = false;
            this.dataGridView3.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.rpt3_2,
            this.rpt3_3,
            this.rpt3_1,
            this.rpt3_4,
            this.rpt3_5,
            this.rpt3_6,
            this.rpt3_7,
            this.rpt3_8});
            this.dataGridView3.Location = new System.Drawing.Point(57, 42);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.ReadOnly = true;
            this.dataGridView3.RowTemplate.Height = 24;
            this.dataGridView3.Size = new System.Drawing.Size(898, 586);
            this.dataGridView3.TabIndex = 21;
            // 
            // rpt3_2
            // 
            this.rpt3_2.DataPropertyName = "rpt3_2";
            this.rpt3_2.HeaderText = "年份(西元年)";
            this.rpt3_2.Name = "rpt3_2";
            this.rpt3_2.ReadOnly = true;
            // 
            // rpt3_3
            // 
            this.rpt3_3.DataPropertyName = "rpt3_3";
            this.rpt3_3.HeaderText = "月份";
            this.rpt3_3.Name = "rpt3_3";
            this.rpt3_3.ReadOnly = true;
            this.rpt3_3.Width = 150;
            // 
            // rpt3_1
            // 
            this.rpt3_1.DataPropertyName = "rpt3_1";
            this.rpt3_1.HeaderText = "日期";
            this.rpt3_1.Name = "rpt3_1";
            this.rpt3_1.ReadOnly = true;
            // 
            // rpt3_4
            // 
            this.rpt3_4.DataPropertyName = "rpt3_4";
            this.rpt3_4.HeaderText = "時間(每小時)";
            this.rpt3_4.Name = "rpt3_4";
            this.rpt3_4.ReadOnly = true;
            this.rpt3_4.Width = 120;
            // 
            // rpt3_5
            // 
            this.rpt3_5.DataPropertyName = "rpt3_5";
            this.rpt3_5.HeaderText = "位置";
            this.rpt3_5.Name = "rpt3_5";
            this.rpt3_5.ReadOnly = true;
            // 
            // rpt3_6
            // 
            this.rpt3_6.DataPropertyName = "rpt3_6";
            this.rpt3_6.HeaderText = "點位";
            this.rpt3_6.Name = "rpt3_6";
            this.rpt3_6.ReadOnly = true;
            this.rpt3_6.Visible = false;
            // 
            // rpt3_7
            // 
            this.rpt3_7.DataPropertyName = "rpt3_7";
            this.rpt3_7.HeaderText = "用電量";
            this.rpt3_7.Name = "rpt3_7";
            this.rpt3_7.ReadOnly = true;
            this.rpt3_7.Width = 150;
            // 
            // rpt3_8
            // 
            this.rpt3_8.DataPropertyName = "rpt3_8";
            this.rpt3_8.HeaderText = "總用電量";
            this.rpt3_8.Name = "rpt3_8";
            this.rpt3_8.ReadOnly = true;
            this.rpt3_8.Width = 150;
            // 
            // chart1
            // 
            chartArea2.AxisX.Title = "日期";
            chartArea2.AxisY.Title = "用電量(KW)";
            chartArea2.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.chart1.Legends.Add(legend2);
            this.chart1.Location = new System.Drawing.Point(1106, 42);
            this.chart1.Name = "chart1";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StackedColumn;
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StackedColumn;
            series3.Legend = "Legend1";
            series3.Name = "Series2";
            this.chart1.Series.Add(series2);
            this.chart1.Series.Add(series3);
            this.chart1.Size = new System.Drawing.Size(612, 586);
            this.chart1.TabIndex = 23;
            this.chart1.Text = "chart1";
            // 
            // chart3
            // 
            chartArea1.Area3DStyle.Enable3D = true;
            chartArea1.Area3DStyle.Inclination = 50;
            chartArea1.Area3DStyle.LightStyle = System.Windows.Forms.DataVisualization.Charting.LightStyle.Realistic;
            chartArea1.Area3DStyle.PointDepth = 90;
            chartArea1.Area3DStyle.PointGapDepth = 90;
            chartArea1.Area3DStyle.Rotation = 40;
            chartArea1.Area3DStyle.WallWidth = 5;
            chartArea1.Name = "ChartArea1";
            this.chart3.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart3.Legends.Add(legend1);
            this.chart3.Location = new System.Drawing.Point(1106, 42);
            this.chart3.Name = "chart3";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chart3.Series.Add(series1);
            this.chart3.Size = new System.Drawing.Size(612, 586);
            this.chart3.TabIndex = 24;
            this.chart3.Text = "chart3";
            // 
            // Form_Report1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1904, 900);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form_Report1";
            this.Load += new System.EventHandler(this.Form_Report1_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton radioYear;
        private System.Windows.Forms.RadioButton radioQuarter;
        private System.Windows.Forms.RadioButton radioMonth;
        private System.Windows.Forms.RadioButton radioDay;
        private System.Windows.Forms.ComboBox comboBox8;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboBox6;
        private System.Windows.Forms.ComboBox comboBox7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBox5;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridViewTextBoxColumn rpt1_2;
        private System.Windows.Forms.DataGridViewTextBoxColumn rpt1_3;
        private System.Windows.Forms.DataGridViewTextBoxColumn rpt1_1;
        private System.Windows.Forms.DataGridViewTextBoxColumn rpt1_4;
        private System.Windows.Forms.DataGridViewTextBoxColumn rpt1_5;
        private System.Windows.Forms.DataGridViewTextBoxColumn rpt1_6;
        private System.Windows.Forms.DataGridViewTextBoxColumn rpt1_7;
        private System.Windows.Forms.DataGridViewTextBoxColumn rpt1_8;
        private System.Windows.Forms.DataGridViewTextBoxColumn rpt1_9;
        private System.Windows.Forms.DataGridViewTextBoxColumn rpt1_10;
        private System.Windows.Forms.DataGridViewTextBoxColumn rpt2_2;
        private System.Windows.Forms.DataGridViewTextBoxColumn rpt2_3;
        private System.Windows.Forms.DataGridViewTextBoxColumn rpt2_1;
        private System.Windows.Forms.DataGridViewTextBoxColumn rpt2_4;
        private System.Windows.Forms.DataGridViewTextBoxColumn rpt2_5;
        private System.Windows.Forms.DataGridViewTextBoxColumn rpt2_6;
        private System.Windows.Forms.DataGridViewTextBoxColumn rpt2_7;
        private System.Windows.Forms.DataGridViewTextBoxColumn rpt2_8;
        private System.Windows.Forms.DataGridViewTextBoxColumn rpt3_2;
        private System.Windows.Forms.DataGridViewTextBoxColumn rpt3_3;
        private System.Windows.Forms.DataGridViewTextBoxColumn rpt3_1;
        private System.Windows.Forms.DataGridViewTextBoxColumn rpt3_4;
        private System.Windows.Forms.DataGridViewTextBoxColumn rpt3_5;
        private System.Windows.Forms.DataGridViewTextBoxColumn rpt3_6;
        private System.Windows.Forms.DataGridViewTextBoxColumn rpt3_7;
        private System.Windows.Forms.DataGridViewTextBoxColumn rpt3_8;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart3;
    }
}