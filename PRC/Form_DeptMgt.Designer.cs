﻿namespace PRC
{
    partial class Form_DeptMgt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.gridDept = new System.Windows.Forms.DataGridView();
            this.DeptID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeptPrivilege = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UpdateUserID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UpdateTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InsertUserID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InsertTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RetireTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblSerialNo = new System.Windows.Forms.Label();
            this.btnDeptC = new System.Windows.Forms.Button();
            this.btnDeptD = new System.Windows.Forms.Button();
            this.btnDeptU = new System.Windows.Forms.Button();
            this.btnDeptI = new System.Windows.Forms.Button();
            this.cmbDeptPrivilege = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txbDeptID = new System.Windows.Forms.TextBox();
            this.txbDesc = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.gridDept)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridDept
            // 
            this.gridDept.AllowUserToAddRows = false;
            this.gridDept.AllowUserToDeleteRows = false;
            this.gridDept.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.gridDept.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.Menu;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridDept.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.gridDept.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridDept.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DeptID,
            this.Description,
            this.DeptPrivilege,
            this.UpdateUserID,
            this.UpdateTime,
            this.InsertUserID,
            this.InsertTime,
            this.RetireTime});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridDept.DefaultCellStyle = dataGridViewCellStyle2;
            this.gridDept.EnableHeadersVisualStyles = false;
            this.gridDept.GridColor = System.Drawing.SystemColors.Info;
            this.gridDept.Location = new System.Drawing.Point(12, 180);
            this.gridDept.Name = "gridDept";
            this.gridDept.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridDept.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.gridDept.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.gridDept.RowTemplate.Height = 24;
            this.gridDept.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridDept.Size = new System.Drawing.Size(1850, 317);
            this.gridDept.TabIndex = 0;
            this.gridDept.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridDept_CellClick);
            // 
            // DeptID
            // 
            this.DeptID.DataPropertyName = "DeptID";
            this.DeptID.HeaderText = "部門名稱";
            this.DeptID.Name = "DeptID";
            this.DeptID.ReadOnly = true;
            this.DeptID.Width = 150;
            // 
            // Description
            // 
            this.Description.DataPropertyName = "Description";
            this.Description.HeaderText = "部門描述";
            this.Description.Name = "Description";
            this.Description.ReadOnly = true;
            this.Description.Width = 350;
            // 
            // DeptPrivilege
            // 
            this.DeptPrivilege.DataPropertyName = "DeptPrivilege";
            this.DeptPrivilege.HeaderText = "是否啟用";
            this.DeptPrivilege.Name = "DeptPrivilege";
            this.DeptPrivilege.ReadOnly = true;
            this.DeptPrivilege.Width = 150;
            // 
            // UpdateUserID
            // 
            this.UpdateUserID.DataPropertyName = "UpdateUserID";
            this.UpdateUserID.HeaderText = "修改者";
            this.UpdateUserID.Name = "UpdateUserID";
            this.UpdateUserID.ReadOnly = true;
            this.UpdateUserID.Width = 150;
            // 
            // UpdateTime
            // 
            this.UpdateTime.DataPropertyName = "UpdateTime";
            this.UpdateTime.HeaderText = "修改時間";
            this.UpdateTime.Name = "UpdateTime";
            this.UpdateTime.ReadOnly = true;
            this.UpdateTime.Width = 300;
            // 
            // InsertUserID
            // 
            this.InsertUserID.DataPropertyName = "InsertUserID";
            this.InsertUserID.HeaderText = "建立者";
            this.InsertUserID.Name = "InsertUserID";
            this.InsertUserID.ReadOnly = true;
            this.InsertUserID.Width = 150;
            // 
            // InsertTime
            // 
            this.InsertTime.DataPropertyName = "InsertTime";
            this.InsertTime.HeaderText = "建立時間";
            this.InsertTime.Name = "InsertTime";
            this.InsertTime.ReadOnly = true;
            this.InsertTime.Width = 300;
            // 
            // RetireTime
            // 
            this.RetireTime.DataPropertyName = "RetireTime";
            this.RetireTime.HeaderText = "RetireTime";
            this.RetireTime.Name = "RetireTime";
            this.RetireTime.ReadOnly = true;
            this.RetireTime.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(12, 34);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1850, 128);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "查詢條件";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblSerialNo);
            this.groupBox2.Controls.Add(this.btnDeptC);
            this.groupBox2.Controls.Add(this.btnDeptD);
            this.groupBox2.Controls.Add(this.btnDeptU);
            this.groupBox2.Controls.Add(this.btnDeptI);
            this.groupBox2.Controls.Add(this.cmbDeptPrivilege);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.txbDeptID);
            this.groupBox2.Controls.Add(this.txbDesc);
            this.groupBox2.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.groupBox2.ForeColor = System.Drawing.Color.White;
            this.groupBox2.Location = new System.Drawing.Point(12, 512);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1850, 219);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            // 
            // lblSerialNo
            // 
            this.lblSerialNo.AutoSize = true;
            this.lblSerialNo.Location = new System.Drawing.Point(1437, 49);
            this.lblSerialNo.Name = "lblSerialNo";
            this.lblSerialNo.Size = new System.Drawing.Size(71, 27);
            this.lblSerialNo.TabIndex = 10;
            this.lblSerialNo.Text = "label4";
            this.lblSerialNo.Visible = false;
            // 
            // btnDeptC
            // 
            this.btnDeptC.BackColor = System.Drawing.Color.Transparent;
            this.btnDeptC.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeptC.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnDeptC.Location = new System.Drawing.Point(1247, 119);
            this.btnDeptC.Name = "btnDeptC";
            this.btnDeptC.Size = new System.Drawing.Size(112, 40);
            this.btnDeptC.TabIndex = 9;
            this.btnDeptC.Text = "取消";
            this.btnDeptC.UseVisualStyleBackColor = false;
            this.btnDeptC.EnabledChanged += new System.EventHandler(this.button_EnabledChanged);
            this.btnDeptC.Click += new System.EventHandler(this.btnDeptC_Click);
            // 
            // btnDeptD
            // 
            this.btnDeptD.BackColor = System.Drawing.Color.Transparent;
            this.btnDeptD.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeptD.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnDeptD.Location = new System.Drawing.Point(1105, 119);
            this.btnDeptD.Name = "btnDeptD";
            this.btnDeptD.Size = new System.Drawing.Size(112, 40);
            this.btnDeptD.TabIndex = 8;
            this.btnDeptD.Text = "刪除";
            this.btnDeptD.UseVisualStyleBackColor = false;
            this.btnDeptD.EnabledChanged += new System.EventHandler(this.button_EnabledChanged);
            this.btnDeptD.Click += new System.EventHandler(this.btnDeptD_Click);
            // 
            // btnDeptU
            // 
            this.btnDeptU.BackColor = System.Drawing.Color.Transparent;
            this.btnDeptU.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeptU.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnDeptU.Location = new System.Drawing.Point(963, 119);
            this.btnDeptU.Name = "btnDeptU";
            this.btnDeptU.Size = new System.Drawing.Size(112, 40);
            this.btnDeptU.TabIndex = 7;
            this.btnDeptU.Text = "修改";
            this.btnDeptU.UseVisualStyleBackColor = false;
            this.btnDeptU.EnabledChanged += new System.EventHandler(this.button_EnabledChanged);
            this.btnDeptU.Click += new System.EventHandler(this.btnDeptU_Click);
            // 
            // btnDeptI
            // 
            this.btnDeptI.BackColor = System.Drawing.Color.Transparent;
            this.btnDeptI.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeptI.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnDeptI.Location = new System.Drawing.Point(821, 119);
            this.btnDeptI.Name = "btnDeptI";
            this.btnDeptI.Size = new System.Drawing.Size(112, 40);
            this.btnDeptI.TabIndex = 6;
            this.btnDeptI.Text = "新增";
            this.btnDeptI.UseVisualStyleBackColor = false;
            this.btnDeptI.EnabledChanged += new System.EventHandler(this.button_EnabledChanged);
            this.btnDeptI.Click += new System.EventHandler(this.btnDeptI_Click);
            // 
            // cmbDeptPrivilege
            // 
            this.cmbDeptPrivilege.BackColor = System.Drawing.SystemColors.ControlDark;
            this.cmbDeptPrivilege.FormattingEnabled = true;
            this.cmbDeptPrivilege.Location = new System.Drawing.Point(939, 46);
            this.cmbDeptPrivilege.Name = "cmbDeptPrivilege";
            this.cmbDeptPrivilege.Size = new System.Drawing.Size(372, 35);
            this.cmbDeptPrivilege.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(816, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(117, 27);
            this.label3.TabIndex = 4;
            this.label3.Text = "是否啟用：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(172, 105);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 27);
            this.label2.TabIndex = 3;
            this.label2.Text = "描述：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(172, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 27);
            this.label1.TabIndex = 2;
            this.label1.Text = "部門名稱：";
            // 
            // txbDeptID
            // 
            this.txbDeptID.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txbDeptID.Location = new System.Drawing.Point(352, 46);
            this.txbDeptID.Name = "txbDeptID";
            this.txbDeptID.Size = new System.Drawing.Size(300, 35);
            this.txbDeptID.TabIndex = 1;
            // 
            // txbDesc
            // 
            this.txbDesc.BackColor = System.Drawing.SystemColors.ControlDark;
            this.txbDesc.Location = new System.Drawing.Point(352, 102);
            this.txbDesc.Multiline = true;
            this.txbDesc.Name = "txbDesc";
            this.txbDesc.Size = new System.Drawing.Size(300, 105);
            this.txbDesc.TabIndex = 0;
            // 
            // Form_DeptMgt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(1910, 800);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gridDept);
            this.ForeColor = System.Drawing.SystemColors.Info;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form_DeptMgt";
            this.Text = "Form_DeptMgt";
            this.Load += new System.EventHandler(this.Form_Dept_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridDept)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView gridDept;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txbDeptID;
        private System.Windows.Forms.TextBox txbDesc;
        private System.Windows.Forms.Button btnDeptC;
        private System.Windows.Forms.Button btnDeptD;
        private System.Windows.Forms.Button btnDeptU;
        private System.Windows.Forms.Button btnDeptI;
        private System.Windows.Forms.ComboBox cmbDeptPrivilege;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblSerialNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn DeptID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Description;
        private System.Windows.Forms.DataGridViewTextBoxColumn DeptPrivilege;
        private System.Windows.Forms.DataGridViewTextBoxColumn UpdateUserID;
        private System.Windows.Forms.DataGridViewTextBoxColumn UpdateTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn InsertUserID;
        private System.Windows.Forms.DataGridViewTextBoxColumn InsertTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn RetireTime;
    }
}