﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Net.Http;
using System.Windows.Forms.VisualStyles;
using Excel = Microsoft.Office.Interop.Excel;
using System.IO;
using PRC.MyClass;
using PRC.Tools;

namespace PRC
{

    public partial class Form_AlarmSystem : Form, IPermissionForm
    {
        Calendar abbt;
        string Chkpurview = "";
        private PermissionObject _itmsPermission;
        private PermissionObject _pssPermission;
        private PermissionObject _alarmPermission;
        public Form_AlarmSystem(string purview)
        {
            this.Chkpurview = purview;
            InitializeComponent();
        }
        //public Form_AlarmSystem()
        //{
        //    this.Chkpurview = purview;
        //    InitializeComponent();
        //}

        private void Form_AlarmSystem_Load(object sender, EventArgs e)
        {
            PermissionManager.InitForm(this,new PERMISSION_FORM_ID[] {  PERMISSION_FORM_ID.Elec, PERMISSION_FORM_ID.Signal,PERMISSION_FORM_ID.Alarm});
            //string sdfsdfds = SHM.loginRec[0].UserID.ToString();
            //button1.BackColor = Color.DimGray;
            //button2.BackColor = Color.Black;
            //button3.BackColor = Color.Black;
            button8.Enabled = false;
            button9.Enabled = false;
            button10.Enabled = false;
            string[] sid = this.Chkpurview.Split('_');
            panel8.Dock = DockStyle.None;
            panel8.Visible = false;
            panel8.Location = new Point(0, 0);
            panel5.Dock = DockStyle.None;
            panel5.Visible = false;
            panel5.Location = new Point(0, 0);
            panel2.Dock = DockStyle.Fill;
            panel2.Visible = true;
            panel2.Location = new Point(0, 0);

            List<string> cmb_SearchSource = new List<string>();
            foreach (string systemID in sid)
            {
                if ("ITMS".Equals(systemID) && this._itmsPermission.CanDisplay)
                    cmb_SearchSource.Add(systemID);
                else if ("PSS".Equals(systemID) && this._pssPermission.CanDisplay)
                    cmb_SearchSource.Add(systemID);
            }

            if (cmb_SearchSource.Count() > 0)
            {
                string rowsid = "";
                for (int i = 0; i < cmb_SearchSource.Count(); i++)
                {
                    this.comboBox3.Items.Add(cmb_SearchSource[i]);
                    this.comboBox6.Items.Add(cmb_SearchSource[i]);
                    this.comboBox7.Items.Add(cmb_SearchSource[i]);

                    rowsid += cmb_SearchSource[i] + ",";
                }
                rowsid = rowsid.Substring(0, rowsid.Length - 1);
                GetAlarm_Setting_Cob(rowsid);
            }
            this.comboBox5.Enabled = false;
            this.comboBox8.Enabled = false;
            DataGridViewDisableButtonColumn ActTime = new DataGridViewDisableButtonColumn();
            ActTime.HeaderText = "確認";

            ActTime.Name = "ActTime";
            ActTime.DataPropertyName = "ActTime";
            ActTime.Width = 100;

            this.dataGridView1.Columns.Add(ActTime);

            dateTimePicker1.BackColor = Color.Red;
            dateTimePicker1.Invalidate();

            comboBox1.SelectedIndex = 0;
            comboBox2.SelectedIndex = Convert.ToInt32(DateTime.Now.ToString("HH"));



        }

        private void checkBox()
        {
            if (checkBox1.Checked)
            {
                checkBox2.Checked = true;
                checkBox3.Checked = true;
            }
            else
            {
                checkBox2.Checked = false;
                checkBox3.Checked = false;
            }
        }

        private void checkBox1_Click(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                checkBox2.Checked = true;
                checkBox3.Checked = true;
            }
            else
            {
                checkBox2.Checked = false;
                checkBox3.Checked = false;
            }
        }

        private void checkBox3_Click(object sender, EventArgs e)
        {
            if (checkBox2.Checked && checkBox3.Checked)
            {
                checkBox1.Checked = true;
            }
            else
            {
                checkBox1.Checked = false;
            }
        }

        private void checkBox2_Click(object sender, EventArgs e)
        {
            if (checkBox2.Checked && checkBox3.Checked)
            {
                checkBox1.Checked = true;
            }
            else
            {
                checkBox1.Checked = false;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (comboBox3.Text.Trim() == "")
            {
                MessageBox.Show("系統別必須選取", "提示訊息");
                return;
            }
            if (!checkBox1.Checked && !checkBox2.Checked && !checkBox3.Checked)
            {
                MessageBox.Show("處理狀況必須擇其一", "提示訊息");
                return;
            }
            string rtCK = "1";
            if (checkBox1.Checked)
            {
                rtCK = "1";
            }
            else if (checkBox2.Checked)
            {
                rtCK = "3";
            }
            else if (checkBox3.Checked)
            {
                rtCK = "2";
            }
            string sfsddf = dateTimePicker1.Value.ToString("yyyyMMdd");
            //this.Alarm_LogSC = new DataTable();
            GetAlarm_Log(dateTimePicker1.Value.ToString("yyyyMMdd") + "_" + comboBox1.Text + "_" + dateTimePicker2.Value.ToString("yyyyMMdd") + "_" + comboBox2.Text + "_" + rtCK + "_" + comboBox3.Text);
            WriteLog.WriteLogByWebApi(this.Name, "Query", EventLog_OperatorNotifyDto.LogType.INFO);
        }


        /// //////////////////////////////////////////////////////////////////
        void dataGridView1_CurrentCellDirtyStateChanged(object sender,
        EventArgs e)
        {
            //if (dataGridView1.IsCurrentCellDirty)
            //{
            //    dataGridView1.CommitEdit(DataGridViewDataErrorContexts.Commit);
            //}

        }

        // If a check box cell is clicked, this event handler disables  
        // or enables the button in the same row as the clicked cell.
        public void dataGridView1_CellValueChanged(object sender,
            DataGridViewCellEventArgs e)
        {
            if (dataGridView1.Columns[e.ColumnIndex].Name == "CheckBoxes")
            {
                DataGridViewDisableButtonCell buttonCell =
                    (DataGridViewDisableButtonCell)dataGridView1.
                    Rows[e.RowIndex].Cells["Buttons"];

                DataGridViewCheckBoxCell checkCell =
                    (DataGridViewCheckBoxCell)dataGridView1.
                    Rows[e.RowIndex].Cells["CheckBoxes"];
                buttonCell.Enabled = !(Boolean)checkCell.Value;

                dataGridView1.Invalidate();
            }




        }


        // If the user clicks on an enabled button cell, this event handler  
        // reports that the button is enabled.
        void dataGridView1_CellClick(object sender,
            DataGridViewCellEventArgs e)
        {
            if (dataGridView1.Columns[e.ColumnIndex].Name == "Buttons")
            {
                DataGridViewDisableButtonCell buttonCell =
                    (DataGridViewDisableButtonCell)dataGridView1.
                    Rows[e.RowIndex].Cells["Buttons"];

                if (buttonCell.Enabled)
                {
                    //MessageBox.Show(dataGridView1.Rows[e.RowIndex].
                    //    Cells[e.ColumnIndex].Value.ToString() +
                    //    " is enabled");
                    buttonCell.Enabled = false;
                }



            }
        }

        void dataGridView_CellSateChanged(object sender, DataGridViewCellStateChangedEventArgs e)
        {
            DataGridViewDisableButtonCell buttonCell = (DataGridViewDisableButtonCell)dataGridView1.Rows[e.Cell.ColumnIndex].Cells["ActTime"];
            //if (buttonCell.Value.ToString()=="已確認")
            //{
            //    buttonCell.Enabled = false;
            //}
            switch (buttonCell.Value.ToString().Trim())
            {
                case "已確認":
                    buttonCell.Enabled = false;
                    break;
                case "待確認":

                    break;
            }
        }
        public void Write(string path, string wit)
        {
            //FileStream fs = new FileStream(path, FileMode.Create);
            StreamWriter sws = new StreamWriter(@".\LogSet.txt", true);
            //開始寫入
            sws.WriteLine(wit);
            //清空緩衝區
            sws.Flush();
            //關閉流
            sws.Close();
            //fs.Close();
        }
        /// </summary>
        DataTable Alarm_LogSC = new DataTable();
        private async void GetAlarm_Log(string wherest)
        {
            try
            {
                HttpClient client = new HttpClient();
                string httpet = "http://localhost:48965/api/GetAlarmLogs";
                HttpResponseMessage response = await client.GetAsync(httpet + @"/" + wherest);
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();

                DataTable source = JsonConvert.DeserializeObject<DataTable>(responseBody.Trim());

                if (source == null || source.Rows.Count <= 0)
                    this.Alarm_LogSC.Rows.Clear();
                else
                    this.Alarm_LogSC = source;

                dataGridView1.DataSource = this.Alarm_LogSC;
                Write(@".\LogSet.txt", "[" + Convert.ToString(DateTime.Now.ToString("yyyyMMdd HH:mm:ss")) + "_" + SHM.loginRec[0].UserID.ToString() + "] GetAlarm_Log select");
                //dataGridView_CellSateChanged()
                if (dataGridView1.Rows.Count > 0)
                {
                    for (int i = 0; i < dataGridView1.Rows.Count; i++)
                    {
                        if (dataGridView1.Rows[i].Cells["AlarmLevel_cht"].Value.ToString() == "警報(輕度)")
                        {
                            dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.Yellow;
                            //dataGridView1.Rows[i].DefaultCellStyle.ForeColor = Color.White;
                        }
                        else if (dataGridView1.Rows[i].Cells["AlarmLevel_cht"].Value.ToString() == "故障")
                        {
                            dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.Red;
                        }
                        else if (dataGridView1.Rows[i].Cells["AlarmLevel_cht"].Value.ToString() == "警報(重度)")
                        {
                            dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.Orange;
                        }
                    }
                }
                else
                {
                    MessageBox.Show("查無資料!");

                }

            }
            catch (Exception e)
            {
                MessageBox.Show("無資料", "提示訊息");
                Write(@".\LogSet.txt", "[" + Convert.ToString(DateTime.Now.ToString("yyyyMMdd HH:mm:ss")) + "_" + SHM.loginRec[0].UserID.ToString() + "] GetAlarm_Log ERR" + e.Message);
                this.Alarm_LogSC = new DataTable();
                while (dataGridView1.Rows.Count > 0)

                {

                    dataGridView1.Rows.RemoveAt(0);

                }
                return;
            }
        }
        DataTable GetAlarmUpdateChk = new DataTable();
        private async void GetAlarmUpdate_Log(string wherest)
        {


            try
            {
                DataTable GetAlarmUpdateChk2 = new DataTable();
                HttpClient client = new HttpClient();
                string httpet = "http://localhost:48965/api/GetAlarmLogsUpdate";
                HttpResponseMessage response = await client.GetAsync(httpet + @"/" + wherest);
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                //this.GetAlarmUpdateChk = new DataTable();
                GetAlarmUpdateChk2 = JsonConvert.DeserializeObject<DataTable>(responseBody.Trim());
                if (GetAlarmUpdateChk2.Rows.Count == 1)
                {
                    MessageBox.Show("確認成功[" + GetAlarmUpdateChk2.Rows[0][0].ToString() + "_" + GetAlarmUpdateChk2.Rows[0][1].ToString() + "_" + GetAlarmUpdateChk2.Rows[0][2].ToString(), "提示訊息");
                }
                else
                {
                    MessageBox.Show("確認失敗", "提示訊息");
                }
                string rtCK = "1";
                if (checkBox1.Checked)
                {
                    rtCK = "1";
                }
                else if (checkBox2.Checked)
                {
                    rtCK = "3";
                }
                else if (checkBox3.Checked)
                {
                    rtCK = "2";
                }
                GetAlarm_Log(dateTimePicker1.Value.ToString("yyyyMMdd") + "_" + comboBox1.Text + "_" + dateTimePicker2.Value.ToString("yyyyMMdd") + "_" + comboBox2.Text + "_" + rtCK + "_" + comboBox3.Text);
                //dataGridView_CellSateChanged()
                Write(@".\LogSet.txt", "[" + Convert.ToString(DateTime.Now.ToString("yyyyMMdd HH:mm:ss")) + "_" + SHM.loginRec[0].UserID.ToString() + "] GetAlarmUpdate_Log select");
            }
            catch (Exception e)
            {
                MessageBox.Show("無資料", "提示訊息");
                Write(@".\LogSet.txt", "[" + Convert.ToString(DateTime.Now.ToString("yyyyMMdd HH:mm:ss")) + "_" + SHM.loginRec[0].UserID.ToString() + "] GetAlarmUpdate_Log ERR" + e.Message);
                return;
            }
        }
        DataTable Alarm_Setting_Cob = new DataTable();
        private async void GetAlarm_Setting_Cob(string wherest)
        {
            try
            {
                HttpClient client = new HttpClient();
                string httpet = "http://localhost:48965/api/GetAlarmSetting";
                HttpResponseMessage response = await client.GetAsync(httpet + @"/" + wherest);
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();

                this.Alarm_Setting_Cob = JsonConvert.DeserializeObject<DataTable>(responseBody.Trim());
                //dataGridView1.DataSource = this.Alarm_LogSC;
                //dataGridView_CellSateChanged()
                this.comboBox4.Items.Clear();
                if (this.Alarm_Setting_Cob.Rows.Count > 0)
                {
                    DataTable myDT = this.Alarm_Setting_Cob.DefaultView.ToTable(true, new string[] { "Location" });
                    for (int i = 0; i < myDT.Rows.Count; i++)
                    {
                        this.comboBox4.Items.Add(myDT.Rows[i]["Location"].ToString());
                    }
                }
                Write(@".\LogSet.txt", "[" + Convert.ToString(DateTime.Now.ToString("yyyyMMdd HH:mm:ss")) + "_" + SHM.loginRec[0].UserID.ToString() + "] GetAlarm_Setting_Cob select");
            }
            catch (Exception e)
            {
                //MessageBox.Show("無資料", "提示訊息");
                this.Alarm_Setting_Cob = new DataTable();
                Write(@".\LogSet.txt", "[" + Convert.ToString(DateTime.Now.ToString("yyyyMMdd HH:mm:ss")) + "_" + SHM.loginRec[0].UserID.ToString() + "] GetAlarm_Setting_Cob ERR" + e.Message);
                return;
            }
        }
        DataTable Alarm_Setting_tab = new DataTable();
        private async void GetAlarm_Setting_tab(string wherest)
        {
            try
            {
                HttpClient client = new HttpClient();
                string httpet = "http://localhost:48965/api/GetAlarmSetting";
                HttpResponseMessage response = await client.GetAsync(httpet + @"/" + wherest);
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();

                this.Alarm_Setting_tab = JsonConvert.DeserializeObject<DataTable>(responseBody.Trim());
                dataGridView2.DataSource = this.Alarm_Setting_tab;
                //dataGridView_CellSateChanged()
                Write(@".\LogSet.txt", "[" + Convert.ToString(DateTime.Now.ToString("yyyyMMdd HH:mm:ss")) + "_" + SHM.loginRec[0].UserID.ToString() + "] GetAlarm_Setting_tab select");

            }
            catch (Exception e)
            {
                MessageBox.Show("無資料", "提示訊息");
                this.Alarm_Setting_tab = new DataTable();
                Write(@".\LogSet.txt", "[" + Convert.ToString(DateTime.Now.ToString("yyyyMMdd HH:mm:ss")) + "_" + SHM.loginRec[0].UserID.ToString() + "] GetAlarm_Setting_Cob ERR" + e.Message);
                return;
            }
        }
        private int retoviod(string ei)
        {
            if (ei.Trim() == "")
            {
                return 0;
            }
            else
            {
                return Convert.ToInt16(ei);
            }
        }
        private async void GetAlarm_Setting_edit(string wherest)
        {
            try
            {
                HttpClient client = new HttpClient();
                string httpet = "http://localhost:48965/api/GetAlarmSettingEdit";
                HttpResponseMessage response = await client.GetAsync(httpet + @"/" + wherest);
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                DataTable Alarm_Setting_edit = new DataTable();

                Alarm_Setting_edit = JsonConvert.DeserializeObject<DataTable>(responseBody.Trim());
                //DataTable myDT = Alarm_Setting_edit.DefaultView.ToTable(true, new string[] { "Location" });
                string[] sid = wherest.Split('_');
                if (sid.Length > 0)
                {
                    if (sid[0].ToString() == "s1")
                    {
                        if (Alarm_Setting_edit.Rows.Count > 0)
                        {
                            this.comboBox8.Items.Clear();
                            this.comboBox8.Enabled = false;
                            this.comboBox5.Items.Clear();
                            this.comboBox5.Enabled = true;
                            DataTable myDT = Alarm_Setting_edit.DefaultView.ToTable(true, new string[] { "Location" });
                            for (int i = 0; i < myDT.Rows.Count; i++)
                            {
                                this.comboBox5.Items.Add(myDT.Rows[i]["Location"].ToString());
                            }
                        }
                    }
                    else if (sid[0].ToString() == "s2")
                    {
                        if (Alarm_Setting_edit.Rows.Count > 0)
                        {
                            this.comboBox8.Items.Clear();
                            this.comboBox8.Enabled = true;
                            DataTable myDT = Alarm_Setting_edit.DefaultView.ToTable(true, new string[] { "EquipID" });
                            for (int i = 0; i < myDT.Rows.Count; i++)
                            {
                                this.comboBox8.Items.Add(myDT.Rows[i]["EquipID"].ToString());
                            }
                        }
                    }
                    else if (sid[0].ToString() == "s3")
                    {
                        if (Alarm_Setting_edit.Rows.Count > 0)
                        {
                            if (Alarm_Setting_edit.Rows[0]["EquipID_setting"].ToString().Trim() == "")
                            {
                                this.button8.Enabled = this._alarmPermission.CanCreate;
                                this.button9.Enabled = false;
                                this.button10.Enabled = false;
                            }
                            else
                            {
                                this.button8.Enabled = false;
                                this.button9.Enabled = this._alarmPermission.CanUpdate;
                                this.button10.Enabled = this._alarmPermission.CanDelete;
                            }
                            textBox1.Text = Alarm_Setting_edit.Rows[0]["SystemID"].ToString();
                            textBox2.Text = Alarm_Setting_edit.Rows[0]["Location"].ToString();
                            textBox3.Text = Alarm_Setting_edit.Rows[0]["EquipID"].ToString();
                            textBox4.Text = Alarm_Setting_edit.Rows[0]["Attribute"].ToString();
                            textBox5.Text = Alarm_Setting_edit.Rows[0]["DataType"].ToString();
                            textBox14.Text = Alarm_Setting_edit.Rows[0]["SubsystemID"].ToString();
                            if (Alarm_Setting_edit.Rows[0]["DataType"].ToString() == "DI" || Alarm_Setting_edit.Rows[0]["DataType"].ToString() == "DO")
                            {
                                textBox13.Visible = false;
                                comboBox13.Visible = true;
                                textBox6.Enabled = false;
                                textBox7.Enabled = false;
                                textBox8.Enabled = false;
                                textBox9.Enabled = false;
                                textBox10.Enabled = false;
                                textBox11.Enabled = false;
                                comboBox9.Visible = false;
                                comboBox10.Visible = false;
                                comboBox11.Visible = false;
                            }
                            else
                            {
                                textBox13.Visible = true;
                                comboBox13.Visible = false;
                                textBox6.Enabled = true;
                                textBox7.Enabled = true;
                                textBox8.Enabled = true;
                                textBox9.Enabled = true;
                                textBox10.Enabled = true;
                                textBox11.Enabled = true;
                                comboBox9.Visible = true;
                                comboBox10.Visible = true;
                                comboBox11.Visible = true;
                            }
                            textBox6.Text = Alarm_Setting_edit.Rows[0]["LowStop"].ToString();
                            comboBox9.SelectedIndex = retoviod(Alarm_Setting_edit.Rows[0]["LowStopAlarm"].ToString());
                            textBox7.Text = Alarm_Setting_edit.Rows[0]["LowStopMsg"].ToString();

                            textBox9.Text = Alarm_Setting_edit.Rows[0]["LowLimit"].ToString();
                            comboBox10.SelectedIndex = retoviod(Alarm_Setting_edit.Rows[0]["LowLimitAlarm"].ToString());
                            textBox8.Text = Alarm_Setting_edit.Rows[0]["LowLimitMsg"].ToString();

                            textBox11.Text = Alarm_Setting_edit.Rows[0]["HighLimit"].ToString();
                            comboBox11.SelectedIndex = retoviod(Alarm_Setting_edit.Rows[0]["HighLimitAlarm"].ToString());
                            textBox10.Text = Alarm_Setting_edit.Rows[0]["HighLimitMsg"].ToString();

                            if (Alarm_Setting_edit.Rows[0]["DataType"].ToString() == "DI" || Alarm_Setting_edit.Rows[0]["DataType"].ToString() == "DO")
                            {
                                if (Alarm_Setting_edit.Rows[0]["HighStop"].ToString() == "0")
                                {
                                    comboBox13.SelectedIndex = 1;
                                }
                                else if (Alarm_Setting_edit.Rows[0]["HighStop"].ToString() == "1")
                                {
                                    comboBox13.SelectedIndex = 2;
                                }
                                else
                                {
                                    comboBox13.SelectedIndex = 0;
                                }

                            }
                            else
                            {
                                textBox13.Text = Alarm_Setting_edit.Rows[0]["HighStop"].ToString();
                            }
                            comboBox12.SelectedIndex = retoviod(Alarm_Setting_edit.Rows[0]["HighStopAlarm"].ToString());
                            textBox12.Text = Alarm_Setting_edit.Rows[0]["HighStopMsg"].ToString();

                        }
                    }
                    else if (sid[0].ToString() == "i")
                    {

                    }
                }

                //dataGridView_CellSateChanged()
                Write(@".\LogSet.txt", "[" + Convert.ToString(DateTime.Now.ToString("yyyyMMdd HH:mm:ss")) + "_" + SHM.loginRec[0].UserID.ToString() + "] GetAlarm_Setting_edit select");
            }
            catch (Exception e)
            {
                MessageBox.Show("無資料", "提示訊息");
                this.Alarm_Setting_tab = new DataTable();
                Write(@".\LogSet.txt", "[" + Convert.ToString(DateTime.Now.ToString("yyyyMMdd HH:mm:ss")) + "_" + SHM.loginRec[0].UserID.ToString() + "] GetAlarm_Setting_tab ERR" + e.Message);
                return;
            }
        }
        private void dataGridView1_DataBindingComplete_1(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            //DataGridViewDisableButtonCell buttonCell = (DataGridViewDisableButtonCell)dataGridView1.Rows[e.Equals.].Cells["ActTime"];
            for (int i = 0; i < dataGridView1.RowCount; i++)
            {
                if (dataGridView1.Rows[i].Cells["ActTime"].Value.ToString() == "已確認")
                {
                    //dataGridView1.Rows[i].Cells["ActTime"].DataGridView.Enabled = false;
                    //((DataGridViewDisableButtonCell)row.Cells[3]).Enabled = enabled;
                    DataGridViewDisableButtonCell buttonCell = (DataGridViewDisableButtonCell)dataGridView1.Rows[i].Cells["ActTime"];
                    buttonCell.Enabled = false;
                }
                else
                {
                    //dataGridView1.Rows[i].Cells["ActTime"].DataGridView.Enabled = true;
                    DataGridViewDisableButtonCell buttonCell = (DataGridViewDisableButtonCell)dataGridView1.Rows[i].Cells["ActTime"];
                    buttonCell.Enabled = true;
                }
            }
            dataGridView1.Invalidate();
            dataGridView1.Refresh();
        }

        private void dataGridView1_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.RowCount > 0)
            {
                string system = string.Format("{0}", dataGridView1.Rows[e.RowIndex].Cells["SystemID"]?.Value);

                if (this._alarmPermission.CanUpdate)
                {
                    DataGridViewDisableButtonCell buttonCell = (DataGridViewDisableButtonCell)dataGridView1.Rows[e.RowIndex].Cells["ActTime"];
                    if (dataGridView1.Columns[e.ColumnIndex].Name == "ActTime" && buttonCell.Enabled)
                    {
                        GetAlarmUpdate_Log(dataGridView1.Rows[e.RowIndex].Cells["edatetime"].Value.ToString() + "_" + dataGridView1.Rows[e.RowIndex].Cells["Location"].Value.ToString() + "_" + dataGridView1.Rows[e.RowIndex].Cells["EquipID"].Value.ToString());
                        //if(this.GetAlarmUpdateChk.Rows.Count ==1)
                        //{
                        //    MessageBox.Show("確認成功["+ this.GetAlarmUpdateChk.Rows[0][0].ToString()+"_"+ this.GetAlarmUpdateChk.Rows[0][1].ToString()+"_"+ this.GetAlarmUpdateChk.Rows[0][2].ToString(), "提示訊息");
                        //    this.GetAlarmUpdateChk = new DataTable();
                        //}
                        //else
                        //{
                        //    MessageBox.Show("確認失敗", "提示訊息");
                        //}
                        //string rtCK = "1";
                        //if (checkBox1.Checked)
                        //{
                        //    rtCK = "1";
                        //}
                        //else if (checkBox2.Checked)
                        //{
                        //    rtCK = "3";
                        //}
                        //else if (checkBox3.Checked)
                        //{
                        //    rtCK = "2";
                        //}
                        //GetAlarm_Log(dateTimePicker1.Value.ToString("yyyyMMdd") + "_" + comboBox1.Text + "_" + dateTimePicker2.Value.ToString("yyyyMMdd") + "_" + comboBox2.Text + "_" + rtCK + "_" + comboBox3.Text);
                    }
                }
                else
                {
                    MessageBox.Show("您沒有此權限");
                }

            }
        }

        public void btn_AlarmSearch_Click(object sender, EventArgs e)
        {
            //button1.BackColor = Color.DimGray;
            //button2.BackColor = Color.Black;
            //button3.BackColor = Color.Black;
            panel8.Dock = DockStyle.None;
            panel8.Visible = false;
            panel5.Dock = DockStyle.None;
            panel5.Visible = false;
            panel2.Dock = DockStyle.Fill;
            panel2.Visible = true;
        }

        public void btn_AlarmSettingSearch_Click(object sender, EventArgs e)
        {
            //button1.BackColor = Color.Black;
            //button2.BackColor = Color.DimGray;
            //button3.BackColor = Color.Black;
            panel8.Dock = DockStyle.None;
            panel8.Visible = false;
            panel2.Dock = DockStyle.None;
            panel2.Visible = false;
            panel5.Dock = DockStyle.Fill;
            panel5.Visible = true;
        }
        public void btnAlarmSetting_Click(object sender, EventArgs e)
        {
            //button1.BackColor = Color.Black;
            //button2.BackColor = Color.Black;
            //button3.BackColor = Color.DimGray;
            panel8.Dock = DockStyle.Fill;
            panel8.Visible = true;
            panel2.Dock = DockStyle.None;
            panel2.Visible = false;
            panel5.Dock = DockStyle.None;
            panel5.Visible = false;
        }

        private void comboBox6_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.comboBox6.Text.Trim() != "")
            {
                GetAlarm_Setting_Cob(this.comboBox6.Text);
            }
        }


        private void button7_Click(object sender, EventArgs e)
        {
            if (comboBox6.Text.Trim() != "")
            {

                if (comboBox4.Text.Trim() != "")
                {
                    GetAlarm_Setting_tab(comboBox6.Text + "_" + comboBox4.Text);
                }
                else
                {
                    GetAlarm_Setting_tab(comboBox6.Text);
                }
            }
            else
            {
                MessageBox.Show("系統選項必須選擇", "提示訊息");
                return;
            }

        }

        private void comboBox7_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetAlarm_Setting_edit("s1_" + comboBox7.Text.ToString());
        }

        private void comboBox5_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetAlarm_Setting_edit("s2_" + comboBox7.Text.ToString() + "_" + comboBox5.Text.ToString());
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (comboBox8.Text.ToString().Trim() == "" || comboBox7.Text.ToString().Trim() == "" || comboBox5.Text.ToString().Trim() == "")
            {
                MessageBox.Show("[系統][位置][設備]皆必須選擇", "提示訊息");
                return;
            }
            else
            {
                textBox6.Text = "";
                textBox7.Text = "";
                textBox8.Text = "";
                textBox9.Text = "";
                textBox10.Text = "";
                textBox11.Text = "";
                textBox12.Text = "";
                textBox13.Text = "";
                comboBox9.SelectedIndex = 0;
                comboBox10.SelectedIndex = 0;
                comboBox11.SelectedIndex = 0;
                comboBox12.SelectedIndex = 0;
                comboBox13.SelectedIndex = 0;
                GetAlarm_Setting_edit("s3_" + comboBox7.Text.ToString() + "_" + comboBox5.Text.ToString() + "_" + comboBox8.Text.ToString());
            }
        }

        private void textBox6_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (Char)48 || e.KeyChar == (Char)49 ||
               e.KeyChar == (Char)50 || e.KeyChar == (Char)51 ||
               e.KeyChar == (Char)52 || e.KeyChar == (Char)53 ||
               e.KeyChar == (Char)54 || e.KeyChar == (Char)55 ||
               e.KeyChar == (Char)56 || e.KeyChar == (Char)57 ||
               e.KeyChar == (Char)13 || e.KeyChar == (Char)8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void textBox7_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void textBox9_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (Char)48 || e.KeyChar == (Char)49 ||
               e.KeyChar == (Char)50 || e.KeyChar == (Char)51 ||
               e.KeyChar == (Char)52 || e.KeyChar == (Char)53 ||
               e.KeyChar == (Char)54 || e.KeyChar == (Char)55 ||
               e.KeyChar == (Char)56 || e.KeyChar == (Char)57 ||
               e.KeyChar == (Char)13 || e.KeyChar == (Char)8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void textBox8_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void textBox11_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (Char)48 || e.KeyChar == (Char)49 ||
               e.KeyChar == (Char)50 || e.KeyChar == (Char)51 ||
               e.KeyChar == (Char)52 || e.KeyChar == (Char)53 ||
               e.KeyChar == (Char)54 || e.KeyChar == (Char)55 ||
               e.KeyChar == (Char)56 || e.KeyChar == (Char)57 ||
               e.KeyChar == (Char)13 || e.KeyChar == (Char)8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void textBox10_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void textBox13_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (Char)48 || e.KeyChar == (Char)49 ||
               e.KeyChar == (Char)50 || e.KeyChar == (Char)51 ||
               e.KeyChar == (Char)52 || e.KeyChar == (Char)53 ||
               e.KeyChar == (Char)54 || e.KeyChar == (Char)55 ||
               e.KeyChar == (Char)56 || e.KeyChar == (Char)57 ||
               e.KeyChar == (Char)13 || e.KeyChar == (Char)8)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void textBox12_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void button8_Click(object sender, EventArgs e)
        {
            string LowStop = renull(textBox6.Text.ToString().Trim());
            string LowStopAlarm = renullcombox(comboBox9.Text.ToString().Trim());
            string LowStopMsg = renull(textBox7.Text.ToString().Trim());

            string LowLimit = renull(textBox9.Text.ToString().Trim());
            string LowLimitAlarm = renullcombox(comboBox10.Text.ToString().Trim());
            string LowLimitMsg = renull(textBox8.Text.ToString().Trim());

            string HighLimit = renull(textBox11.Text.ToString().Trim());
            string HighLimitAlarm = renullcombox(comboBox11.Text.ToString().Trim());
            string HighLimitMsg = renull(textBox10.Text.ToString().Trim());

            string HighStop = "";
            if (textBox5.Text.Trim() == "DI" || textBox5.Text.Trim() == "DO")
            {
                HighStop = renullcombox(comboBox13.Text.ToString().Trim());
            }
            else
            {
                HighStop = renull(textBox13.Text.ToString().Trim());
            }

            string HighStopAlarm = renullcombox(comboBox12.Text.ToString().Trim());
            string HighStopMsg = renull(textBox12.Text.ToString().Trim());

            DialogResult myResult = MessageBox.Show("是否確認新增?", "系統提示", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (myResult == DialogResult.Yes)
            {
                GetAlarm_Setting_edit("i_" + textBox1.Text.ToString() + "_"
                + textBox14.Text.ToString() + "_"
                + textBox2.Text.ToString() + "_"
                + textBox3.Text.ToString() + "_"
                + textBox5.Text.ToString() + "_"
                + LowStop + "_"
            + LowStopAlarm + "_"
            + LowStopMsg + "_"
            + LowLimit + "_"
            + LowLimitAlarm + "_"
            + LowLimitMsg + "_"
            + HighLimit + "_"
            + HighLimitAlarm + "_"
            + HighLimitMsg + "_"
            + HighStop + "_"
            + HighStopAlarm + "_"
            + HighStopMsg);
                MessageBox.Show("新增成功", "系統提示");
                textBox6.Text = "";
                textBox7.Text = "";
                textBox8.Text = "";
                textBox9.Text = "";
                textBox10.Text = "";
                textBox11.Text = "";
                textBox12.Text = "";
                textBox13.Text = "";
                comboBox9.SelectedIndex = 0;
                comboBox10.SelectedIndex = 0;
                comboBox11.SelectedIndex = 0;
                comboBox12.SelectedIndex = 0;
                comboBox13.SelectedIndex = 0;
                WriteLog.WriteLogByWebApi(this.Name, "Insert", EventLog_OperatorNotifyDto.LogType.INFO);
                GetAlarm_Setting_edit("s3_" + comboBox7.Text.ToString() + "_" + comboBox5.Text.ToString() + "_" + comboBox8.Text.ToString());

            }
            else if (myResult == DialogResult.No)
            {
                MessageBox.Show("新增已取消", "系統提示");
            }

        }

        private string renull(string reix)
        {
            if (reix.Trim() == "")
            {
                return "NULL";
            }
            else
            {
                return reix;
            }
        }
        private string renullcombox(string reix)
        {

            if (reix.Trim() == "")
            {
                return "NULL";
            }
            else
            {
                string[] sid = reix.Split('_');

                return sid[0].ToString();

            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            string LowStop = renull(textBox6.Text.ToString().Trim());
            string LowStopAlarm = renullcombox(comboBox9.Text.ToString().Trim());
            string LowStopMsg = renull(textBox7.Text.ToString().Trim());

            string LowLimit = renull(textBox9.Text.ToString().Trim());
            string LowLimitAlarm = renullcombox(comboBox10.Text.ToString().Trim());
            string LowLimitMsg = renull(textBox8.Text.ToString().Trim());

            string HighLimit = renull(textBox11.Text.ToString().Trim());
            string HighLimitAlarm = renullcombox(comboBox11.Text.ToString().Trim());
            string HighLimitMsg = renull(textBox10.Text.ToString().Trim());

            string HighStop = "";
            if (textBox5.Text.Trim() == "DI" || textBox5.Text.Trim() == "DO")
            {
                HighStop = renullcombox(comboBox13.Text.ToString().Trim());
            }
            else
            {
                HighStop = renull(textBox13.Text.ToString().Trim());
            }

            string HighStopAlarm = renullcombox(comboBox12.Text.ToString().Trim());
            string HighStopMsg = renull(textBox12.Text.ToString().Trim());

            DialogResult myResult = MessageBox.Show("是否確認修改?", "系統提示", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (myResult == DialogResult.Yes)
            {
                GetAlarm_Setting_edit("u_" + textBox1.Text.ToString() + "_"

                + textBox2.Text.ToString() + "_"
                + textBox3.Text.ToString() + "_"

                + LowStop + "_"
            + LowStopAlarm + "_"
            + LowStopMsg + "_"
            + LowLimit + "_"
            + LowLimitAlarm + "_"
            + LowLimitMsg + "_"
            + HighLimit + "_"
            + HighLimitAlarm + "_"
            + HighLimitMsg + "_"
            + HighStop + "_"
            + HighStopAlarm + "_"
            + HighStopMsg);
                MessageBox.Show("修改成功", "系統提示");
                textBox6.Text = "";
                textBox7.Text = "";
                textBox8.Text = "";
                textBox9.Text = "";
                textBox10.Text = "";
                textBox11.Text = "";
                textBox12.Text = "";
                textBox13.Text = "";
                comboBox9.SelectedIndex = 0;
                comboBox10.SelectedIndex = 0;
                comboBox11.SelectedIndex = 0;
                comboBox12.SelectedIndex = 0;
                comboBox13.SelectedIndex = 0;
                WriteLog.WriteLogByWebApi(this.Name, "Edit", EventLog_OperatorNotifyDto.LogType.INFO);
                GetAlarm_Setting_edit("s3_" + comboBox7.Text.ToString() + "_" + comboBox5.Text.ToString() + "_" + comboBox8.Text.ToString());

            }
            else if (myResult == DialogResult.No)
            {
                MessageBox.Show("修改已取消", "系統提示");
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {


            DialogResult myResult = MessageBox.Show("是否確認刪除?", "系統提示", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (myResult == DialogResult.Yes)
            {
                GetAlarm_Setting_edit("d_" + textBox1.Text.ToString() + "_"

                + textBox2.Text.ToString() + "_"
                + textBox3.Text.ToString() + "_"

                );
                MessageBox.Show("刪除成功", "系統提示");
                button8.Enabled = false;
                button9.Enabled = false;
                button10.Enabled = false;
                textBox1.Text = "";
                textBox2.Text = "";
                textBox3.Text = "";
                textBox4.Text = "";
                textBox5.Text = "";
                textBox14.Text = "";

                textBox6.Text = "";
                textBox7.Text = "";
                textBox8.Text = "";
                textBox9.Text = "";
                textBox10.Text = "";
                textBox11.Text = "";
                textBox12.Text = "";
                textBox13.Text = "";
                comboBox9.SelectedIndex = 0;
                comboBox10.SelectedIndex = 0;
                comboBox11.SelectedIndex = 0;
                comboBox12.SelectedIndex = 0;
                comboBox13.SelectedIndex = 0;
                WriteLog.WriteLogByWebApi(this.Name, "Delete", EventLog_OperatorNotifyDto.LogType.INFO);
            }
            else if (myResult == DialogResult.No)
            {
                MessageBox.Show("刪除已取消", "系統提示");
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            string saveFileName = "";
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Execl files (*.xls)|*.xls";
            saveFileDialog.FilterIndex = 0;
            saveFileDialog.RestoreDirectory = true;
            saveFileDialog.CreatePrompt = true;
            saveFileDialog.Title = "Export Excel File To";
            saveFileDialog.ShowDialog();
            saveFileName = saveFileDialog.FileName;

            if (saveFileName.IndexOf(":") < 0)
                return;
            Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
            if (xlApp == null)
            {
                MessageBox.Show("請確定是否有安裝Excel?!");
                return;
            }

            Microsoft.Office.Interop.Excel.Workbooks workbooks = xlApp.Workbooks;
            Microsoft.Office.Interop.Excel.Workbook workbook = workbooks.Add(Microsoft.Office.Interop.Excel.XlWBATemplate.xlWBATWorksheet);
            Microsoft.Office.Interop.Excel.Worksheet worksheet = (Microsoft.Office.Interop.Excel.Worksheet)workbook.Worksheets[1];//取得sheet1 
                                                                                                                                  //下面只是匯出到excel的簡單格式設定 可自行變化 
                                                                                                                                  //worksheet.get_Range(worksheet.Cells[1, 1], worksheet.Cells[1, 10]).MergeCells = true; //左右合併 
                                                                                                                                  //            worksheet.get_Range(worksheet.Cells[2, 1], worksheet.Cells[2, 10]).MergeCells = true; //左右合併 
                                                                                                                                  //            worksheet.get_Range(worksheet.Cells[1, 1], worksheet.Cells[2, 10]).MergeCells = true; //上下合併 1.2列合併 
                                                                                                                                  //            worksheet.get_Range(worksheet.Cells[3, 1], worksheet.Cells[3, 10]).MergeCells = true; //左右合併 
                                                                                                                                  //            worksheet.get_Range(worksheet.Cells[4, 1], worksheet.Cells[4, 10]).MergeCells = true; //左右合併 
                                                                                                                                  //            worksheet.get_Range(worksheet.Cells[3, 1], worksheet.Cells[4, 10]).MergeCells = true; //上下合併 3.4列合併 
                                                                                                                                  //            worksheet.get_Range(worksheet.Cells[1, 1], worksheet.Cells[1, 10]).Cells.Font.Size = 20; //標題字型大小 
                                                                                                                                  //            worksheet.get_Range(worksheet.Cells[1, 1], worksheet.Cells[1, 10]).HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                                                                                                                                  //            worksheet.get_Range(worksheet.Cells[3, 1], worksheet.Cells[3, 10]).HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;



            DateTime dt = DateTime.Now;
            //worksheet.Cells[1, 1] = "你想要出現的標題";
            //worksheet.Cells[3, 1] = "時間(看個人需不需要)" + dt.Year + dt.Month.ToString().PadLeft(2, '0') + dt.Day.ToString().PadLeft(2, '0');


            for (int i = 0; i < dataGridView1.ColumnCount; i++)
            {
                worksheet.Cells[1, i + 1] = dataGridView1.Columns[i].HeaderText; // 依照上面合併使用過的列數(最大值+1) 前面四行我拿作標題跟時間了所以重第五列開始 
            }
            for (int r = 0; r < dataGridView1.Rows.Count; r++)
            {
                for (int i = 0; i < dataGridView1.ColumnCount; i++)
                {
                    worksheet.Cells[r + 2, i + 1] = dataGridView1.Rows[r].Cells[i].Value;
                }
                System.Windows.Forms.Application.DoEvents();

                worksheet.Columns.EntireColumn.AutoFit();//自動調整欄位 



                if (saveFileName != "")
                {
                    try
                    {
                        workbook.Saved = true;
                        workbook.SaveCopyAs(saveFileName);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("匯出文件時出錯,EXCEL文件可能正在使用！\n" + ex.Message);
                    }

                }
            }
            WriteLog.WriteLogByWebApi(this.Name, "Download", EventLog_OperatorNotifyDto.LogType.INFO);
            MessageBox.Show("匯出完成", "系統訊息");
        }

        private void button_EnabledChanged(object sender, EventArgs e)
        {

            Button btn = (Button)sender;
            if (btn.Enabled)
            {
                btn.FlatStyle = FlatStyle.Flat;
                btn.ForeColor = Color.FromArgb(100, 128, 255, 255);
                btn.BackColor = Color.Transparent;
                btn.FlatAppearance.BorderColor = Color.FromArgb(100, 128, 255, 255);

            }
            else
            {
                btn.FlatStyle = FlatStyle.Standard;
            }

        }


        #region Init with Permission

        public void InitByPermission(List<PermissionObject> permissionObjects)
        {
            foreach (PermissionObject managePermission in permissionObjects)
            {
                switch (managePermission.PermissionCode)
                {
                    case PERMISSION_FORM_ID.Elec:
                        this._pssPermission = managePermission;

                        break;
                    case  PERMISSION_FORM_ID.Signal:
                        this._itmsPermission = managePermission;
                        break;
                    case   PERMISSION_FORM_ID.Alarm:
                        this._alarmPermission = managePermission;
                        button4.Enabled = managePermission.CanRead;
                        button5.Enabled = managePermission.CanRead;
                        button6.Enabled = managePermission.CanRead;
                        button7.Enabled = managePermission.CanRead;
                        break;
                }
            }
        }


        #endregion


    }
    public class DataGridViewDisableButtonColumn : DataGridViewButtonColumn
    {
        public DataGridViewDisableButtonColumn()
        {
            this.CellTemplate = new DataGridViewDisableButtonCell();
        }
    }

    public class DataGridViewDisableButtonCell : DataGridViewButtonCell
    {
        private bool enabledValue;
        public bool Enabled
        {
            get
            {
                return enabledValue;
            }
            set
            {
                enabledValue = value;
            }
        }

        // Override the Clone method so that the Enabled property is copied.
        public override object Clone()
        {
            DataGridViewDisableButtonCell cell =
                (DataGridViewDisableButtonCell)base.Clone();
            cell.Enabled = this.Enabled;
            return cell;
        }

        // By default, enable the button cell.
        public DataGridViewDisableButtonCell()
        {
            this.enabledValue = true;
        }

        protected override void Paint(Graphics graphics,
            Rectangle clipBounds, Rectangle cellBounds, int rowIndex,
            DataGridViewElementStates elementState, object value,
            object formattedValue, string errorText,
            DataGridViewCellStyle cellStyle,
            DataGridViewAdvancedBorderStyle advancedBorderStyle,
            DataGridViewPaintParts paintParts)
        {
            // The button cell is disabled, so paint the border,  
            // background, and disabled button for the cell.
            if (!this.enabledValue)
            {
                // Draw the cell background, if specified.
                if ((paintParts & DataGridViewPaintParts.Background) ==
                    DataGridViewPaintParts.Background)
                {
                    SolidBrush cellBackground =
                        new SolidBrush(cellStyle.BackColor);
                    graphics.FillRectangle(cellBackground, cellBounds);
                    cellBackground.Dispose();
                }

                // Draw the cell borders, if specified.
                if ((paintParts & DataGridViewPaintParts.Border) ==
                    DataGridViewPaintParts.Border)
                {
                    PaintBorder(graphics, clipBounds, cellBounds, cellStyle,
                        advancedBorderStyle);
                }

                // Calculate the area in which to draw the button.
                Rectangle buttonArea = cellBounds;
                Rectangle buttonAdjustment =
                    this.BorderWidths(advancedBorderStyle);
                buttonArea.X += buttonAdjustment.X;
                buttonArea.Y += buttonAdjustment.Y;
                buttonArea.Height -= buttonAdjustment.Height;
                buttonArea.Width -= buttonAdjustment.Width;

                // Draw the disabled button.                
                ButtonRenderer.DrawButton(graphics, buttonArea,
                    PushButtonState.Disabled);

                // Draw the disabled button text. 
                if (this.FormattedValue is String)
                {
                    TextRenderer.DrawText(graphics,
                        (string)this.FormattedValue,
                        this.DataGridView.Font,
                        buttonArea, SystemColors.GrayText);
                }
            }
            else
            {
                // The button cell is enabled, so let the base class 
                // handle the painting.
                base.Paint(graphics, clipBounds, cellBounds, rowIndex,
                    elementState, value, formattedValue, errorText,
                    cellStyle, advancedBorderStyle, paintParts);
            }
        }
    }
}
