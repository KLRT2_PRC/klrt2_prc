﻿using ICSCNetwork;
using iSCADA.Design.Utilities.Electrical;
//using log4net.Config;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PRC
{
    public partial class Form_StationC15 : Form
    {
        protected Socket _socket;
        private ICommClient _portClient;
        private ModbusClient _modbusclient;
        System.Threading.Timer _ThreadingTimer = null;
        System.Timers.Timer _TimersTimer = null;
        private bool _ControlFlag = false;
        private bool _Connected = false;
        private int _transactionId;
        private C15 pck;

        //private static readonly log4net.ILog _log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        //TCPServer _tcpsvr = new TCPServer();
        MessageFormat.TSS7 _tss7;
        MessageFormat.C15 _c15;

        public Form_StationC15()
        {
            InitializeComponent();
            //_tss7 = tss7;
        }

        private void Form_StationC15_Load(object sender, EventArgs e)
        {
            //this._TimersTimer = new System.Timers.Timer();
            //this._TimersTimer.Interval = 500;
            //this._TimersTimer.Enabled = true;
            //this._TimersTimer.Elapsed += new System.Timers.ElapsedEventHandler(_TimersTimer_Elapsed);
            //UpdateControls();
            //var initask = Task.Run(() => NetConnect());
            //initask.Wait(10000);

            //XmlConfigurator.Configure(new System.IO.FileInfo("./log4net.config"));
            //_log.Info("PRC 啟動;");
            //_tcpsvr.LocalPort = 8080;
            //_tcpsvr.Listening += tcpsvr_Listening;
            //_tcpsvr.Accepted += tcpsvr_Accepted;
            //_tcpsvr.Stoped += tcpsvr_Stoped;
            //_tcpsvr.Received += tcpsvr_Received;
            //_tcpsvr.Disconnected += tcpsvr_Disconnected;
            //_tcpsvr.Listen();
            //UpdateControls();
            MainFrame._udpserver.RecievedData += _udpserver_RecievedData;
        }

        private void _udpserver_RecievedData(object sender, RecieveDataEventArgs e)
        {
            UpdateControls();
            //throw new NotImplementedException();
        }

        public void UpdateControls()
        {
            if (InvokeRequired)
            {
                BeginInvoke(new Action(UpdateControls));
                return;
            }
            else
            {
                UpdateCTStatus();
            }
            //switch (_Connected)
            //{
            //    case true:
            //        UpdateCTStatus();
            //        break;
            //    case false:
            //        foreach (Control c in this.Controls)
            //        {
            //            if (c.CompanyName == "iSCADA.Design.Utilities.Electrical")
            //            {
            //                ((iSCADA.Design.Utilities.Electrical.ElectricalSymbol)c).State = SymbolState.Disconnected;
            //            }
            //        }
            //        break;
            //}
        }
        private void btn_click(object sender, EventArgs e)
        {
            switch (((Button)sender).Name)
            {
                case "btnMC62On":
                    btnMC62On.BackColor = Color.Red;
                    btnMC62Off.BackColor = SystemColors.ControlDark;
                    ExecuteWriteCommand(44, 9, 256);
                    break;
                case "btnMC62Off":
                    btnMC62On.BackColor = Color.Red;
                    btnMC62Off.BackColor = SystemColors.ControlDark;
                    ExecuteWriteCommand(44, 10, 512);
                    break;
                case "btnMC63On":
                    btnMC63On.BackColor = Color.Red;
                    btnMC63Off.BackColor = SystemColors.ControlDark;
                    ExecuteWriteCommand(44, 11, 1024);
                    break;
                case "btnMC63Off":
                    btnMC63On.BackColor = Color.Red;
                    btnMC63Off.BackColor = SystemColors.ControlDark;
                    ExecuteWriteCommand(44, 12, 2048);
                    break;
                case "btnMC80FunOn":
                    btnMC80FunOn.BackColor = Color.Red;
                    ExecuteWriteCommand(44, 13, 4096);
                    break;
                case "btnMC80ProOn":
                    btnMC80ProOn.BackColor = Color.Red;
                    ExecuteWriteCommand(44, 14, 8192);
                    break;
            }
        }
        private void UpdateCTStatus()
        {

            //label36.Text = MainFrame._mc15.SOE.ToString("yyyy/MM/dd HH:mm:ss");
            ////if (Form_ElecTSS7._c15.MC62DISCONNECTOROpened == true)
            ////{
            ////    switch05UC1.State = SymbolState.Error;
            ////}
            ////if (Form_ElecTSS7._c15.MC62DISCONNECTORClosed == true)
            ////{
            ////    switch05UC1.State = SymbolState.On;
            ////}
            //switch05UC1.State = MainFrame._mc15.MC62DISCONNECTOROpened == true ? SymbolState.On : SymbolState.Error;

            ////if (Form_ElecTSS7._c15.MC63DISCONNECTOROpened == true)
            ////{
            ////    switch05UC2.State = SymbolState.Error;
            ////}
            ////if (Form_ElecTSS7._c15.MC63DISCONNECTORClosed == true)
            ////{
            ////    switch05UC2.State = SymbolState.On;
            ////}
            ////switch05UC2.State= switch05UC2.State
            //if (MainFrame._mc15.MC62K1CONTACTOROpened == true)
            //{
            //    lineUC13.State = SymbolState.Error;
            //    lineUC6.State = SymbolState.Error;
            //    lineUC10.State = SymbolState.Error;
            //    lineUC4.State = SymbolState.Error;
            //}
            //if (MainFrame._mc15.MC62K1CONTACTORClosed == true)
            //{
            //    lineUC13.State = SymbolState.On;
            //    lineUC6.State = SymbolState.On;
            //    lineUC10.State = SymbolState.On;
            //    lineUC4.State = SymbolState.On;
            //}

            //if (MainFrame._mc15.MC62K3CONTACTOROpened == true)
            //{
            //    lineUC11.State = SymbolState.Error;
            //    lineUC7.State = SymbolState.Error;
            //    lineUC8.State = SymbolState.Error;
            //    lineUC12.State = SymbolState.Error;
            //}
            //if (MainFrame._mc15.MC62K3CONTACTORClosed == true)
            //{
            //    lineUC11.State = SymbolState.On;
            //    lineUC7.State = SymbolState.On;
            //    lineUC8.State = SymbolState.On;
            //    lineUC12.State = SymbolState.On;
            //}

            //switch (MainFrame._mc15.MC64G9POWERSUPPLYDCOUTOk)
            //{
            //    case true:
            //        circleUC19.State = SymbolState.Error;
            //        circleUC19.Fill = true;
            //        break;
            //    case false:
            //        circleUC19.State = SymbolState.On;
            //        circleUC19.Fill = true;
            //        break;
            //    default:
            //        circleUC19.State = SymbolState.Off;
            //        circleUC19.Fill = true;
            //        break;
            //}

            //label67.Text = MainFrame._mc15.MC62VOLTAGECONDUCTORRail.ToString();
            //label110.Text = MainFrame._mc15.MC64PANELTemperature.ToString();
        }

        private void ExecuteWriteCommand(int StartAddress, int num, int value)
        {
            try
            {
                var command = new ModBusCommand(ModBusCommand.FuncWriteSingleRegister)
                {
                    Offset = StartAddress,
                    Count = num,
                    TransId = _transactionId++,
                    Data = new ushort[1]
                };
                command.Data[0] = (ushort)(value);
                var result = _modbusclient.ExecuteGeneric(_portClient, command);
            }
            catch
            { }
        }
    }

    public partial class C15
    {
        public C15(ushort[] data)
        {
            SOE = Convert.ToDateTime(data[0].ToString() + "-" + BitConverter.GetBytes(data[1])[1].ToString() + "-" + BitConverter.GetBytes(data[1])[0].ToString() + "  " + BitConverter.GetBytes(data[2])[0].ToString() + ":" + BitConverter.GetBytes(data[3])[1].ToString() + ":" + BitConverter.GetBytes(data[3])[0].ToString() + "." + data[4].ToString() + data[5].ToString());
            byte[] dibyte = new byte[DILength - 12];
            byte[] aibyte = new byte[AILength];
            byte[] dobyte = new byte[DOLength];

            Buffer.BlockCopy(data, 12, dibyte, 0, dibyte.Length);
            Buffer.BlockCopy(data, 24, aibyte, 0, aibyte.Length);
            Buffer.BlockCopy(data, 88, dobyte, 0, dobyte.Length);

            //DI
            BitArray bits = new BitArray(dibyte);
            CPURunning = bits[8];
            CPUError = bits[9];
            CPUCommFault = bits[10];
            INTERLOCKCommFault = bits[11];
            CPUBFlag = bits[12];

            MC64UPSAC220VOn =bits[0];
            MC64TWELEAC220VOn = bits[1];
            MC64AC110V220VMCCBNormal = bits[2];
            MC64DC24VMCCBNormal = bits[3];
            MC64BCMNGSYSTEMOk = bits[4];
            MC64G5CHARGERDCINOk = bits[5];
            MC64G5CHARGERBATTERYOk = bits[6];
            MC64G5CHARGERDCOUTOk = bits[7];
            MC64G6CHARGERDCINOk = bits[24];
            MC64G6CHARGERBATTERYOk = bits[25];
            MC64G6CHARGERDCOUTOk = bits[26];
            MC64EMERGENCYSTOPPBRelease = bits[27];
            MC64ENABLESWITCHOn = bits[28];
            MC64DOOROpen = bits[29];
            MC64G9POWERSUPPLYDCOUTOk = bits[30];
            MC64MC62SAFETYRELAYOn = bits[16];
            MC64MC63SAFETYRELAYOn = bits[17];
            MC64CONTACTOROFFFault = bits[18];
            MC64FANSRUNStart = bits[19];
            MC62MCBFault = bits[40];
            MC62REMOTEMode = bits[41];
            MC62DISCONNECTOROpened = bits[42];
            MC62DISCONNECTORClosed = bits[43];
            MC62K1CONTACTOROpened = bits[44];
            MC62K1CONTACTORClosed = bits[45];
            MC62K3CONTACTOROpened = bits[46];
            MC62K3CONTACTORClosed = bits[47];
            MC62FRAMEFault = bits[32];
            MC62HANDCRANKPlugged = bits[33];
            MC62VOLTAGEAVAILABLEIn = bits[34];

            MC63MCBFault = bits[56];
            MC63REMOTEMode = bits[57];
            MC63DISCONNECTOROpened = bits[58];
            MC63DISCONNECTORClosed = bits[59];
            MC63K2CONTACTOROpened = bits[60];
            MC63K2CONTACTORClosed = bits[61];
            MC63K4CONTACTOROpened = bits[62];
            MC63K4CONTACTORClosed = bits[63];
            MC63FRAMEFault = bits[48];
            MC63HANDCRANKPlugged = bits[49];
            MC63VOLTAGEAVAILABLEIn = bits[50];

            MF80VOLTAGELIMITINGDEVICEBlocked = bits[72];
            MF80VOLTAGELIMITINGDEVICEOpened = bits[73];
            MF80VOLTAGELIMITINGDEVICEClosed = bits[74];
            MF80VOLTAGELIMITINGDEVICEFault = bits[75];
            MF80UminRLBWETIMEOut = bits[76];
            MF80OVERLOADThermal = bits[77];

            MPPAC220VOk = bits[64];
            ESNUPSAIRCONDITIONINGFault = bits[65];
            ESNUPSFault = bits[66];
            ESNUPSAlarm = bits[67];
            COMUPSCOMUPSFault = bits[68];
            COMUPSCOMUPSAlarm = bits[69];
            //AI
            MC62VOLTAGECONDUCTORRail = BitConverter.ToSingle(Swapped(aibyte, 0), 0);
            MC62CURRENTDIODE1 = BitConverter.ToSingle(Swapped(aibyte, 4), 0);
            MC62CURRENTDIODE2 = BitConverter.ToSingle(Swapped(aibyte, 8), 0);
            MC62CURRENTDIODE3 = BitConverter.ToSingle(Swapped(aibyte, 12), 0);
            MC62CURRENTCONTACTORKF1 = BitConverter.ToSingle(Swapped(aibyte, 16), 0);
            MC62CURRENTCONTACTORKF3 = BitConverter.ToSingle(Swapped(aibyte, 20), 0);
            MC62PANELTemperature = BitConverter.ToSingle(Swapped(aibyte, 24), 0);

            MC63VOLTAGECONDUCTORRail = BitConverter.ToSingle(Swapped(aibyte, 28), 0);
            MC63CURRENTDIODE1 = BitConverter.ToSingle(Swapped(aibyte, 32), 0);
            MC63CURRENTDIODE2 = BitConverter.ToSingle(Swapped(aibyte, 36), 0);
            MC63CURRENTDIODE3 = BitConverter.ToSingle(Swapped(aibyte, 40), 0);
            MC63CURRENTCONTACTORKF2 = BitConverter.ToSingle(Swapped(aibyte, 44), 0);
            MC63CURRENTCONTACTORKF4 = BitConverter.ToSingle(Swapped(aibyte, 48), 0);
            MC63PANELTemperature = BitConverter.ToSingle(Swapped(aibyte, 52), 0);

            MF80VOLTAGEMeasuring = BitConverter.ToSingle(Swapped(aibyte, 56), 0);
            MC64PANELTemperature = BitConverter.ToSingle(Swapped(aibyte, 60), 0);
            //DO
            BitArray dobits = new BitArray(dobyte);
            MC62DISCONNECTORONCommand = dobits[8];
            MC62DISCONNECTOROFFCommand = dobits[9];
            MC63DISCONNECTORONCommand = dobits[10];
            MC63DISCONNECTOROFFCommand = dobits[11];
            MF80REMOTECOMMANDFUNCTIONTESTOn = dobits[12];
            MF80REMOTECOMMANDPROTECTIONONDirect = dobits[13];
            LTSLIGHTINGOn = dobits[0];
            CPUDATATRANSFERACCEPTConfirm = dobits[7];
        }

        private byte[] Swapped(byte[] source, int start)
        {
            byte[] dtarray = new byte[4];
            byte[] result = new byte[4];

            Array.Copy(source, start, dtarray, 0, 4);
            Array.Copy(dtarray, 0, result, 2, 2);
            Array.Copy(dtarray, 2, result, 0, 2);
            return result;
        }

        public readonly int Length = 90;
        private readonly int DILength = 22;
        private readonly int AILength = 66;
        private readonly int DOLength = 2;
        public DateTime SOE { get; private set; }
        public readonly string SystemID = "PSS";
        public readonly string Subsystem = "TSS";
        public readonly string Location = "C15";
        //DI
        public bool CPURunning { get; private set; }
        public bool CPUError { get; private set; }
        public bool CPUCommFault { get; private set; }
        public bool INTERLOCKCommFault { get; private set; }
        public bool CPUBFlag { get; private set; }

        public bool MC64UPSAC220VOn { get; private set; }
        public bool MC64TWELEAC220VOn { get; private set; }
        public bool MC64AC110V220VMCCBNormal { get; private set; }
        public bool MC64DC24VMCCBNormal { get; private set; }
        public bool MC64BCMNGSYSTEMOk { get; private set; }
        public bool MC64G5CHARGERDCINOk { get; private set; }
        public bool MC64G5CHARGERBATTERYOk { get; private set; }
        public bool MC64G5CHARGERDCOUTOk { get; private set; }
        public bool MC64G6CHARGERDCINOk { get; private set; }
        public bool MC64G6CHARGERBATTERYOk { get; private set; }
        public bool MC64G6CHARGERDCOUTOk { get; private set; }
        public bool MC64EMERGENCYSTOPPBRelease { get; private set; }
        public bool MC64ENABLESWITCHOn { get; private set; }
        public bool MC64DOOROpen { get; private set; }
        public bool MC64G9POWERSUPPLYDCOUTOk { get; private set; }
        public bool MC64MC62SAFETYRELAYOn { get; private set; }
        public bool MC64MC63SAFETYRELAYOn { get; private set; }
        public bool MC64CONTACTOROFFFault { get; private set; }
        public bool MC64FANSRUNStart { get; private set; }
        public bool MC62MCBFault { get; private set; }
        public bool MC62REMOTEMode { get; private set; }
        public bool MC62DISCONNECTOROpened { get; private set; }
        public bool MC62DISCONNECTORClosed { get; private set; }
        public bool MC62K1CONTACTOROpened { get; private set; }
        public bool MC62K1CONTACTORClosed { get; private set; }
        public bool MC62K3CONTACTOROpened { get; private set; }
        public bool MC62K3CONTACTORClosed { get; private set; }
        public bool MC62FRAMEFault { get; private set; }
        public bool MC62HANDCRANKPlugged { get; private set; }
        public bool MC62VOLTAGEAVAILABLEIn { get; private set; }

        public bool MC63MCBFault { get; private set; }
        public bool MC63REMOTEMode { get; private set; }
        public bool MC63DISCONNECTOROpened { get; private set; }
        public bool MC63DISCONNECTORClosed { get; private set; }
        public bool MC63K2CONTACTOROpened { get; private set; }
        public bool MC63K2CONTACTORClosed { get; private set; }
        public bool MC63K4CONTACTOROpened { get; private set; }
        public bool MC63K4CONTACTORClosed { get; private set; }
        public bool MC63FRAMEFault { get; private set; }
        public bool MC63HANDCRANKPlugged { get; private set; }
        public bool MC63VOLTAGEAVAILABLEIn { get; private set; }

        public bool MF80VOLTAGELIMITINGDEVICEBlocked { get; private set; }
        public bool MF80VOLTAGELIMITINGDEVICEOpened { get; private set; }
        public bool MF80VOLTAGELIMITINGDEVICEClosed { get; private set; }
        public bool MF80VOLTAGELIMITINGDEVICEFault { get; private set; }
        public bool MF80UminRLBWETIMEOut { get; private set; }
        public bool MF80OVERLOADThermal { get; private set; }

        public bool MPPAC220VOk { get; private set; }
        public bool ESNUPSAIRCONDITIONINGFault { get; private set; }
        public bool ESNUPSFault { get; private set; }
        public bool ESNUPSAlarm { get; private set; }
        public bool COMUPSCOMUPSFault { get; private set; }
        public bool COMUPSCOMUPSAlarm { get; private set; }
        //AI
        public float MC62VOLTAGECONDUCTORRail { get; private set; }
        public float MC62CURRENTDIODE1 { get; private set; }
        public float MC62CURRENTDIODE2 { get; private set; }
        public float MC62CURRENTDIODE3 { get; private set; }
        public float MC62CURRENTCONTACTORKF1 { get; private set; }
        public float MC62CURRENTCONTACTORKF3 { get; private set; }
        public float MC62PANELTemperature { get; private set; }

        public float MC63VOLTAGECONDUCTORRail { get; private set; }
        public float MC63CURRENTDIODE1 { get; private set; }
        public float MC63CURRENTDIODE2 { get; private set; }
        public float MC63CURRENTDIODE3 { get; private set; }
        public float MC63CURRENTCONTACTORKF2 { get; private set; }
        public float MC63CURRENTCONTACTORKF4 { get; private set; }
        public float MC63PANELTemperature { get; private set; }

        public float MF80VOLTAGEMeasuring { get; private set; }
        public float MC64PANELTemperature { get; private set; }
        //DO
        public bool MC62DISCONNECTORONCommand { get; private set; }
        public bool MC62DISCONNECTOROFFCommand { get; private set; }
        public bool MC63DISCONNECTORONCommand { get; private set; }
        public bool MC63DISCONNECTOROFFCommand { get; private set; }
        public bool MF80REMOTECOMMANDFUNCTIONTESTOn { get; private set; }
        public bool MF80REMOTECOMMANDPROTECTIONONDirect { get; private set; }
        public bool LTSLIGHTINGOn { get; private set; }
        public bool CPUDATATRANSFERACCEPTConfirm { get; private set; }

    }
}
