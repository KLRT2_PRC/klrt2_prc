﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PRC.MyClass;
using System.Windows.Forms.DataVisualization.Charting;
using System.Net.Http;
using System.Net.Http.Headers;

namespace PRC
{
    public partial class Form_Report1 : Form
    {

        private static DataTable dt1 = new DataTable();
        private static DataTable dt2 = new DataTable();
        private static DataTable dt3 = new DataTable();
        public Form_Report1()
        {
            InitializeComponent();
            radioDay.CheckedChanged += new EventHandler(radioButton_CheckedChanged);
            radioMonth.CheckedChanged += new EventHandler(radioButton_CheckedChanged);
            radioQuarter.CheckedChanged += new EventHandler(radioButton_CheckedChanged);
            radioYear.CheckedChanged += new EventHandler(radioButton_CheckedChanged);
        }
        private void initGridData()
        {
            DataGridViewRowCollection rows = dataGridView1.Rows;
            rows.Add(new object[] { "2018/03/01", "2018", "3", "00:10:00", "TSS14", "H02 R-S PHASE VOLTAGE/CURRENT", "213.49", "210", "274.08", "213" });
            rows.Add(new object[] { "2018/03/01", "2018", "4", "01:10:00", "TSS14", "H02 R-S PHASE VOLTAGE/CURRENT", "212.69", "210", "281.76", "213" });
            rows.Add(new object[] { "2018/03/01", "2018", "6", "02:10:00", "TSS14", "H02 R-S PHASE VOLTAGE/CURRENT", "213.13", "210", "213.6", "213" });
            rows.Add(new object[] { "2018/03/01", "2018", "9", "03:10:00", "TSS14", "H02 R-S PHASE VOLTAGE/CURRENT", "213.49", "210", "274.08", "213" });
            rows.Add(new object[] { "2018/03/01", "2018", "5", "04:10:00", "TSS14", "H02 R-S PHASE VOLTAGE/CURRENT", "212.69", "210", "281.76", "213" });
            rows.Add(new object[] { "2018/03/01", "2018", "6", "05:10:00", "TSS14", "H02 R-S PHASE VOLTAGE/CURRENT", "213.13", "210", "213.6", "213" });
            rows.Add(new object[] { "2018/03/01", "2018", "6", "06:10:00", "TSS14", "H02 R-S PHASE VOLTAGE/CURRENT", "213.49", "210", "274.08", "213" });
            rows.Add(new object[] { "2018/03/01", "2018", "4", "07:10:00", "TSS14", "H02 R-S PHASE VOLTAGE/CURRENT", "212.69", "210", "281.76", "213" });
            rows.Add(new object[] { "2018/03/01", "2018", "7", "08:10:00", "TSS14", "H02 R-S PHASE VOLTAGE/CURRENT", "213.13", "210", "213.6", "213" });
            rows.Add(new object[] { "2018/03/01", "2018", "6", "09:10:00", "TSS14", "H02 R-S PHASE VOLTAGE/CURRENT", "212.69", "210", "281.76", "213" });
            rows.Add(new object[] { "2018/03/01", "2018", "3", "10:10:00", "TSS14", "H02 R-S PHASE VOLTAGE/CURRENT", "213.49", "210", "213.6", "213" });
        }
        
        private void initGrid1()
        {
            //DataTable dt1 = new DataTable();
            dt1.Columns.Add("rpt1_1");
            dt1.Columns.Add("rpt1_2");
            dt1.Columns.Add("rpt1_3");
            dt1.Columns.Add("rpt1_4");
            dt1.Columns.Add("rpt1_5");
            dt1.Columns.Add("rpt1_6");
            dt1.Columns.Add("rpt1_7");
            dt1.Columns.Add("rpt1_8");
            dt1.Columns.Add("rpt1_9");
            dt1.Columns.Add("rpt1_10");

            DataRow dr = dt1.NewRow();
            dr["rpt1_1"] = "2018/03/11";
            dr["rpt1_2"] = "2018";
            dr["rpt1_3"] = "03";
            dr["rpt1_4"] = "01:01:05";
            dr["rpt1_5"] = "TSS7";
            dr["rpt1_6"] = "H02 R-S PHASE VOLTAGE/CURRENT";
            dr["rpt1_7"] = "221.54";
            dr["rpt1_8"] = "211.2";
            dr["rpt1_9"] = "101.21";
            dr["rpt1_10"] = "96.33";
            dt1.Rows.Add(dr);

            dr = dt1.NewRow();
            dr["rpt1_1"] = "2018/03/11";
            dr["rpt1_2"] = "2018";
            dr["rpt1_3"] = "03";
            dr["rpt1_4"] = "02:01:05";
            dr["rpt1_5"] = "TSS7";
            dr["rpt1_6"] = "H02 R-S PHASE VOLTAGE/CURRENT";
            dr["rpt1_7"] = "223.04";
            dr["rpt1_8"] = "215.2";
            dr["rpt1_9"] = "105.6";
            dr["rpt1_10"] = "97.03";
            dt1.Rows.Add(dr);

            dr = dt1.NewRow();
            dr["rpt1_1"] = "2018/03/11";
            dr["rpt1_2"] = "2018";
            dr["rpt1_3"] = "03";
            dr["rpt1_4"] = "03:01:05";
            dr["rpt1_5"] = "TSS7";
            dr["rpt1_6"] = "H02 R-S PHASE VOLTAGE/CURRENT";
            dr["rpt1_7"] = "225.11";
            dr["rpt1_8"] = "213.9";
            dr["rpt1_9"] = "102.91";
            dr["rpt1_10"] = "95.96";
            dt1.Rows.Add(dr);

            dr = dt1.NewRow();
            dr["rpt1_1"] = "2018/03/11";
            dr["rpt1_2"] = "2018";
            dr["rpt1_3"] = "03";
            dr["rpt1_4"] = "04:01:05";
            dr["rpt1_5"] = "TSS7";
            dr["rpt1_6"] = "H02 R-S PHASE VOLTAGE/CURRENT";
            dr["rpt1_7"] = "224.36";
            dr["rpt1_8"] = "211.49";
            dr["rpt1_9"] = "101.21";
            dr["rpt1_10"] = "97.24";
            dt1.Rows.Add(dr);

            dr = dt1.NewRow();
            dr["rpt1_1"] = "2018/03/11";
            dr["rpt1_2"] = "2018";
            dr["rpt1_3"] = "03";
            dr["rpt1_4"] = "05:01:05";
            dr["rpt1_5"] = "TSS7";
            dr["rpt1_6"] = "H02 R-S PHASE VOLTAGE/CURRENT";
            dr["rpt1_7"] = "226.12";
            dr["rpt1_8"] = "209.95";
            dr["rpt1_9"] = "106.5";
            dr["rpt1_10"] = "93.8";
            dt1.Rows.Add(dr);

            dr = dt1.NewRow();
            dr["rpt1_1"] = "2018/03/11";
            dr["rpt1_2"] = "2018";
            dr["rpt1_3"] = "03";
            dr["rpt1_4"] = "06:01:05";
            dr["rpt1_5"] = "TSS7";
            dr["rpt1_6"] = "H02 R-S PHASE VOLTAGE/CURRENT";
            dr["rpt1_7"] = "222.6";
            dr["rpt1_8"] = "209.2";
            dr["rpt1_9"] = "103.39";
            dr["rpt1_10"] = "95.6";
            dt1.Rows.Add(dr);

            dr = dt1.NewRow();
            dr["rpt1_1"] = "2018/03/11";
            dr["rpt1_2"] = "2018";
            dr["rpt1_3"] = "03";
            dr["rpt1_4"] = "07:01:05";
            dr["rpt1_5"] = "TSS7";
            dr["rpt1_6"] = "H02 R-S PHASE VOLTAGE/CURRENT";
            dr["rpt1_7"] = "220.38";
            dr["rpt1_8"] = "215.2";
            dr["rpt1_9"] = "101.39";
            dr["rpt1_10"] = "97.4";
            dt1.Rows.Add(dr);

            dr = dt1.NewRow();
            dr["rpt1_1"] = "2018/03/11";
            dr["rpt1_2"] = "2018";
            dr["rpt1_3"] = "03";
            dr["rpt1_4"] = "08:01:05";
            dr["rpt1_5"] = "TSS7";
            dr["rpt1_6"] = "H02 R-S PHASE VOLTAGE/CURRENT";
            dr["rpt1_7"] = "225.3";
            dr["rpt1_8"] = "212.2";
            dr["rpt1_9"] = "103.9";
            dr["rpt1_10"] = "95.56";
            dt1.Rows.Add(dr);

            dr = dt1.NewRow();
            dr["rpt1_1"] = "2018/03/11";
            dr["rpt1_2"] = "2018";
            dr["rpt1_3"] = "03";
            dr["rpt1_4"] = "09:01:05";
            dr["rpt1_5"] = "TSS7";
            dr["rpt1_6"] = "H02 R-S PHASE VOLTAGE/CURRENT";
            dr["rpt1_7"] = "230.98";
            dr["rpt1_8"] = "208.3";
            dr["rpt1_9"] = "105.4";
            dr["rpt1_10"] = "94.83";
            dt1.Rows.Add(dr);

            dr = dt1.NewRow();
            dr["rpt1_1"] = "2018/03/11";
            dr["rpt1_2"] = "2018";
            dr["rpt1_3"] = "03";
            dr["rpt1_4"] = "10:01:05";
            dr["rpt1_5"] = "TSS7";
            dr["rpt1_6"] = "H02 R-S PHASE VOLTAGE/CURRENT";
            dr["rpt1_7"] = "228.64";
            dr["rpt1_8"] = "213.75";
            dr["rpt1_9"] = "104.92";
            dr["rpt1_10"] = "96.47";
            dt1.Rows.Add(dr);

            dataGridView1.DataSource = dt1;
            
        }

        private void initGrid12()
        {
            //DataTable dt1 = new DataTable();
            //dt1.Columns.Clear()
            dt1.Columns.Add("rpt1_1");
            dt1.Columns.Add("rpt1_2");
            dt1.Columns.Add("rpt1_3");
            dt1.Columns.Add("rpt1_4");
            dt1.Columns.Add("rpt1_5");
            dt1.Columns.Add("rpt1_6");
            dt1.Columns.Add("rpt1_7");
            dt1.Columns.Add("rpt1_8");
            dt1.Columns.Add("rpt1_9");
            dt1.Columns.Add("rpt1_10");

            DataRow dr = dt1.NewRow();
            dr["rpt1_1"] = "2018/03/11";
            dr["rpt1_2"] = "2018";
            dr["rpt1_3"] = "03";
            dr["rpt1_4"] = "01:01:05";
            dr["rpt1_5"] = "TSS7";
            dr["rpt1_6"] = "H02 R-S PHASE VOLTAGE/CURRENT";
            dr["rpt1_7"] = "230";
            dr["rpt1_8"] = "215";
            dr["rpt1_9"] = "105";
            dr["rpt1_10"] = "96";
            dt1.Rows.Add(dr);

            dr = dt1.NewRow();
            dr["rpt1_1"] = "2018/03/12";
            dr["rpt1_2"] = "2018";
            dr["rpt1_3"] = "03";
            dr["rpt1_4"] = "02:01:05";
            dr["rpt1_5"] = "TSS7";
            dr["rpt1_6"] = "H02 R-S PHASE VOLTAGE/CURRENT";
            dr["rpt1_7"] = "235";
            dr["rpt1_8"] = "213";
            dr["rpt1_9"] = "107";
            dr["rpt1_10"] = "97";
            dt1.Rows.Add(dr);

            dr = dt1.NewRow();
            dr["rpt1_1"] = "2018/03/13";
            dr["rpt1_2"] = "2018";
            dr["rpt1_3"] = "03";
            dr["rpt1_4"] = "03:01:05";
            dr["rpt1_5"] = "TSS7";
            dr["rpt1_6"] = "H02 R-S PHASE VOLTAGE/CURRENT";
            dr["rpt1_7"] = "228";
            dr["rpt1_8"] = "216";
            dr["rpt1_9"] = "102";
            dr["rpt1_10"] = "94";
            dt1.Rows.Add(dr);

            dr = dt1.NewRow();
            dr["rpt1_1"] = "2018/03/14";
            dr["rpt1_2"] = "2018";
            dr["rpt1_3"] = "03";
            dr["rpt1_4"] = "04:01:05";
            dr["rpt1_5"] = "TSS7";
            dr["rpt1_6"] = "H02 R-S PHASE VOLTAGE/CURRENT";
            dr["rpt1_7"] = "220";
            dr["rpt1_8"] = "218";
            dr["rpt1_9"] = "109";
            dr["rpt1_10"] = "99";
            dt1.Rows.Add(dr);

            dr = dt1.NewRow();
            dr["rpt1_1"] = "2018/03/15";
            dr["rpt1_2"] = "2018";
            dr["rpt1_3"] = "03";
            dr["rpt1_4"] = "05:01:05";
            dr["rpt1_5"] = "TSS7";
            dr["rpt1_6"] = "H02 R-S PHASE VOLTAGE/CURRENT";
            dr["rpt1_7"] = "236";
            dr["rpt1_8"] = "209";
            dr["rpt1_9"] = "103";
            dr["rpt1_10"] = "93";
            dt1.Rows.Add(dr);

            dr = dt1.NewRow();
            dr["rpt1_1"] = "2018/03/16";
            dr["rpt1_2"] = "2018";
            dr["rpt1_3"] = "03";
            dr["rpt1_4"] = "06:01:05";
            dr["rpt1_5"] = "TSS7";
            dr["rpt1_6"] = "H02 R-S PHASE VOLTAGE/CURRENT";
            dr["rpt1_7"] = "232";
            dr["rpt1_8"] = "215";
            dr["rpt1_9"] = "103";
            dr["rpt1_10"] = "97";
            dt1.Rows.Add(dr);

            dr = dt1.NewRow();
            dr["rpt1_1"] = "2018/03/17";
            dr["rpt1_2"] = "2018";
            dr["rpt1_3"] = "03";
            dr["rpt1_4"] = "07:01:05";
            dr["rpt1_5"] = "TSS7";
            dr["rpt1_6"] = "H02 R-S PHASE VOLTAGE/CURRENT";
            dr["rpt1_7"] = "226";
            dr["rpt1_8"] = "217";
            dr["rpt1_9"] = "101";
            dr["rpt1_10"] = "93";
            dt1.Rows.Add(dr);

            dr = dt1.NewRow();
            dr["rpt1_1"] = "2018/03/18";
            dr["rpt1_2"] = "2018";
            dr["rpt1_3"] = "03";
            dr["rpt1_4"] = "08:01:05";
            dr["rpt1_5"] = "TSS7";
            dr["rpt1_6"] = "H02 R-S PHASE VOLTAGE/CURRENT";
            dr["rpt1_7"] = "234";
            dr["rpt1_8"] = "212";
            dr["rpt1_9"] = "103";
            dr["rpt1_10"] = "95";
            dt1.Rows.Add(dr);

            dr = dt1.NewRow();
            dr["rpt1_1"] = "2018/03/19";
            dr["rpt1_2"] = "2018";
            dr["rpt1_3"] = "03";
            dr["rpt1_4"] = "09:01:05";
            dr["rpt1_5"] = "TSS7";
            dr["rpt1_6"] = "H02 R-S PHASE VOLTAGE/CURRENT";
            dr["rpt1_7"] = "230";
            dr["rpt1_8"] = "208";
            dr["rpt1_9"] = "105";
            dr["rpt1_10"] = "94";
            dt1.Rows.Add(dr);

            dr = dt1.NewRow();
            dr["rpt1_1"] = "2018/03/20";
            dr["rpt1_2"] = "2018";
            dr["rpt1_3"] = "03";
            dr["rpt1_4"] = "10:01:05";
            dr["rpt1_5"] = "TSS7";
            dr["rpt1_6"] = "H02 R-S PHASE VOLTAGE/CURRENT";
            dr["rpt1_7"] = "228";
            dr["rpt1_8"] = "213";
            dr["rpt1_9"] = "106";
            dr["rpt1_10"] = "96";
            dt1.Rows.Add(dr);

            //dataGridView1.DataSource = dt1;

        }

        private void initGrid2()
        {
            //DataTable dt1 = new DataTable();
            dt2.Columns.Add("rpt2_1");
            dt2.Columns.Add("rpt2_2");
            dt2.Columns.Add("rpt2_3");
            dt2.Columns.Add("rpt2_4");
            dt2.Columns.Add("rpt2_5");
            dt2.Columns.Add("rpt2_6");
            dt2.Columns.Add("rpt2_7");
            dt2.Columns.Add("rpt2_8");
         

            DataRow dr = dt2.NewRow();
            dr["rpt2_1"] = "2018/03/11";
            dr["rpt2_2"] = "2018";
            dr["rpt2_3"] = "03";
            dr["rpt2_4"] = "01:01:05";
            dr["rpt2_5"] = "TSS7";
            dr["rpt2_6"] = "H02 POWER (KWH)";
            dr["rpt2_7"] = "22";
            dr["rpt2_8"] = "522";
          
            dt2.Rows.Add(dr);

            dr = dt2.NewRow();
            dr["rpt2_1"] = "2018/03/11";
            dr["rpt2_2"] = "2018";
            dr["rpt2_3"] = "03";
            dr["rpt2_4"] = "02:01:05";
            dr["rpt2_5"] = "TSS7";
            dr["rpt2_6"] = "H02 POWER (KWH)";
            dr["rpt2_7"] = "24";
            dr["rpt2_8"] = "546";
           
            dt2.Rows.Add(dr);

            dr = dt2.NewRow();
            dr["rpt2_1"] = "2018/03/11";
            dr["rpt2_2"] = "2018";
            dr["rpt2_3"] = "03";
            dr["rpt2_4"] = "03:01:05";
            dr["rpt2_5"] = "TSS7";
            dr["rpt2_6"] = "H02 POWER (KWH)";
            dr["rpt2_7"] = "26";
            dr["rpt2_8"] = "572";
            
            dt2.Rows.Add(dr);

            dr = dt2.NewRow();
            dr["rpt2_1"] = "2018/03/11";
            dr["rpt2_2"] = "2018";
            dr["rpt2_3"] = "03";
            dr["rpt2_4"] = "04:01:05";
            dr["rpt2_5"] = "TSS7";
            dr["rpt2_6"] = "H02 POWER (KWH)";
            dr["rpt2_7"] = "28";
            dr["rpt2_8"] = "600";
           
            dt2.Rows.Add(dr);

            dr = dt2.NewRow();
            dr["rpt2_1"] = "2018/03/11";
            dr["rpt2_2"] = "2018";
            dr["rpt2_3"] = "03";
            dr["rpt2_4"] = "05:01:05";
            dr["rpt2_5"] = "TSS7";
            dr["rpt2_6"] = "H02 POWER (KWH)";
            dr["rpt2_7"] = "24";
            dr["rpt2_8"] = "624";
          
            dt2.Rows.Add(dr);

            dr = dt2.NewRow();
            dr["rpt2_1"] = "2018/03/11";
            dr["rpt2_2"] = "2018";
            dr["rpt2_3"] = "03";
            dr["rpt2_4"] = "06:01:05";
            dr["rpt2_5"] = "TSS7";
            dr["rpt2_6"] = "H02 POWER (KWH)";
            dr["rpt2_7"] = "26";
            dr["rpt2_8"] = "650";
           
            dt2.Rows.Add(dr);

            dr = dt2.NewRow();
            dr["rpt2_1"] = "2018/03/11";
            dr["rpt2_2"] = "2018";
            dr["rpt2_3"] = "03";
            dr["rpt2_4"] = "07:01:05";
            dr["rpt2_5"] = "TSS7";
            dr["rpt2_6"] = "H02 POWER (KWH)";
            dr["rpt2_7"] = "30";
            dr["rpt2_8"] = "680";
            
            dt2.Rows.Add(dr);

            dr = dt2.NewRow();
            dr["rpt2_1"] = "2018/03/11";
            dr["rpt2_2"] = "2018";
            dr["rpt2_3"] = "03";
            dr["rpt2_4"] = "08:01:05";
            dr["rpt2_5"] = "TSS7";
            dr["rpt2_6"] = "H02 POWER (KWH)";
            dr["rpt2_7"] = "32";
            dr["rpt2_8"] = "712";
           
            dt2.Rows.Add(dr);

            dr = dt2.NewRow();
            dr["rpt2_1"] = "2018/03/11";
            dr["rpt2_2"] = "2018";
            dr["rpt2_3"] = "03";
            dr["rpt2_4"] = "09:01:05";
            dr["rpt2_5"] = "TSS7";
            dr["rpt2_6"] = "H02 POWER (KWH)";
            dr["rpt2_7"] = "28";
            dr["rpt2_8"] = "740";
           
            dt2.Rows.Add(dr);

            dr = dt2.NewRow();
            dr["rpt2_1"] = "2018/03/11";
            dr["rpt2_2"] = "2018";
            dr["rpt2_3"] = "03";
            dr["rpt2_4"] = "10:01:05";
            dr["rpt2_5"] = "TSS7";
            dr["rpt2_6"] = "H02 POWER (KWH)";
            dr["rpt2_7"] = "30";
            dr["rpt2_8"] = "770";
            
            dt2.Rows.Add(dr);

            dataGridView2.DataSource = dt2;

        }

        private void initGrid3()
        {
            dt3.Columns.Add("rpt3_1");
            dt3.Columns.Add("rpt3_2");
            dt3.Columns.Add("rpt3_3");
            dt3.Columns.Add("rpt3_4");
            dt3.Columns.Add("rpt3_5");
            dt3.Columns.Add("rpt3_6");
            dt3.Columns.Add("rpt3_7");
            dt3.Columns.Add("rpt3_8");

            DataRow dr = dt3.NewRow();
            dr["rpt3_1"] = "2018/03/11";
            dr["rpt3_2"] = "2018";
            dr["rpt3_3"] = "03";
            dr["rpt3_4"] = "01:01:05";
            dr["rpt3_5"] = "TSS7";
            dr["rpt3_6"] = "H02 POWER (KWH)";
            dr["rpt3_7"] = "35";
            dr["rpt3_8"] = "101";
            dt3.Rows.Add(dr);

            dr = dt3.NewRow();
            dr["rpt3_1"] = "2018/03/11";
            dr["rpt3_2"] = "2018";
            dr["rpt3_3"] = "03";
            dr["rpt3_4"] = "01:01:05";
            dr["rpt3_5"] = "TSS7";
            dr["rpt3_6"] = "H03 POWER (KWH)";
            dr["rpt3_7"] = "26";
            dr["rpt3_8"] = "101";
            dt3.Rows.Add(dr);

            dr = dt3.NewRow();
            dr["rpt3_1"] = "2018/03/11";
            dr["rpt3_2"] = "2018";
            dr["rpt3_3"] = "03";
            dr["rpt3_4"] = "01:01:05";
            dr["rpt3_5"] = "TSS7";
            dr["rpt3_6"] = "H04 POWER (KWH)";
            dr["rpt3_7"] = "40";
            dr["rpt3_8"] = "101";
            dt3.Rows.Add(dr);

            dr = dt3.NewRow();
            dr["rpt3_1"] = "2018/03/11";
            dr["rpt3_2"] = "2018";
            dr["rpt3_3"] = "03";
            dr["rpt3_4"] = "02:01:05";
            dr["rpt3_5"] = "TSS7";
            dr["rpt3_6"] = "H02 POWER (KWH)";
            dr["rpt3_7"] = "50";
            dr["rpt3_8"] = "255";
            dt3.Rows.Add(dr);

            dr = dt3.NewRow();
            dr["rpt3_1"] = "2018/03/11";
            dr["rpt3_2"] = "2018";
            dr["rpt3_3"] = "03";
            dr["rpt3_4"] = "02:01:05";
            dr["rpt3_5"] = "TSS7";
            dr["rpt3_6"] = "H03 POWER (KWH)";
            dr["rpt3_7"] = "41";
            dr["rpt3_8"] = "255";
            dt3.Rows.Add(dr);

            dr = dt3.NewRow();
            dr["rpt3_1"] = "2018/03/11";
            dr["rpt3_2"] = "2018";
            dr["rpt3_3"] = "03";
            dr["rpt3_4"] = "02:01:05";
            dr["rpt3_5"] = "TSS7";
            dr["rpt3_6"] = "H04 POWER (KWH)";
            dr["rpt3_7"] = "63";
            dr["rpt3_8"] = "255";
            dt3.Rows.Add(dr);

            dr = dt3.NewRow();
            dr["rpt3_1"] = "2018/03/11";
            dr["rpt3_2"] = "2018";
            dr["rpt3_3"] = "03";
            dr["rpt3_4"] = "03:01:05";
            dr["rpt3_5"] = "TSS7";
            dr["rpt3_6"] = "H02 POWER (KWH)";
            dr["rpt3_7"] = "42";
            dr["rpt3_8"] = "415";
            dt3.Rows.Add(dr);

            dr = dt3.NewRow();
            dr["rpt3_1"] = "2018/03/11";
            dr["rpt3_2"] = "2018";
            dr["rpt3_3"] = "03";
            dr["rpt3_4"] = "03:01:05";
            dr["rpt3_5"] = "TSS7";
            dr["rpt3_6"] = "H03 POWER (KWH)";
            dr["rpt3_7"] = "52";
            dr["rpt3_8"] = "415";
            dt3.Rows.Add(dr);

            dr = dt3.NewRow();
            dr["rpt3_1"] = "2018/03/11";
            dr["rpt3_2"] = "2018";
            dr["rpt3_3"] = "03";
            dr["rpt3_4"] = "03:01:05";
            dr["rpt3_5"] = "TSS7";
            dr["rpt3_6"] = "H04 POWER (KWH)";
            dr["rpt3_7"] = "63";
            dr["rpt3_8"] = "412";
            dt3.Rows.Add(dr);

            dr = dt3.NewRow();
            dr["rpt3_1"] = "2018/03/11";
            dr["rpt3_2"] = "2018";
            dr["rpt3_3"] = "03";
            dr["rpt3_4"] = "04:01:05";
            dr["rpt3_5"] = "TSS7";
            dr["rpt3_6"] = "H02 POWER (KWH)";
            dr["rpt3_7"] = "93";
            dr["rpt3_8"] = "657";
            dt3.Rows.Add(dr);

            dr = dt3.NewRow();
            dr["rpt3_1"] = "2018/03/11";
            dr["rpt3_2"] = "2018";
            dr["rpt3_3"] = "03";
            dr["rpt3_4"] = "04:01:05";
            dr["rpt3_5"] = "TSS7";
            dr["rpt3_6"] = "H03 POWER (KWH)";
            dr["rpt3_7"] = "67";
            dr["rpt3_8"] = "657";
            dt3.Rows.Add(dr);

            dr = dt3.NewRow();
            dr["rpt3_1"] = "2018/03/11";
            dr["rpt3_2"] = "2018";
            dr["rpt3_3"] = "03";
            dr["rpt3_4"] = "04:01:05";
            dr["rpt3_5"] = "TSS7";
            dr["rpt3_6"] = "H04 POWER (KWH)";
            dr["rpt3_7"] = "85";
            dr["rpt3_8"] = "657";
            dt3.Rows.Add(dr);

            dataGridView3.DataSource = dt3;
        }
        void setChart()
        {
            string[] xValues = { "PSS", "BMS", "ITEMS" };
            string[] titleArr = { "1月", "2月", "3月" };
            double[] yValues = { 4, 2, 2 };
            double[] yValues2 = { 2, 3.5, 2 };
            double[] yValues3 = { 3.5, 1, 3 };

            //chart1.ChartAreas["ChartArea1"].AxisY.LabelStyle.Format = "#.##";//設定小數點
            chart1.Series["Series1"].Points.DataBindXY(xValues, yValues);
            chart1.Series["Series1"].LegendText = titleArr[0];
            chart1.Series["Series1"].IsValueShownAsLabel = true;

            chart1.Series["Series2"].Points.DataBindXY(xValues, yValues2);
            chart1.Series["Series2"].LegendText = titleArr[1];
            chart1.Series["Series2"].IsValueShownAsLabel = true;

            chart1.Series["Series3"].Points.DataBindXY(xValues, yValues3);
            chart1.Series["Series3"].LegendText = titleArr[2];
            chart1.Series["Series3"].IsValueShownAsLabel = true;

            chart1.Series["Series1"]["DrawingStyle"] = "Cylinder";
            chart1.Series["Series1"]["PointWidth"] = "0.6";

            chart1.Series["Series2"]["DrawingStyle"] = "Cylinder";
            chart1.Series["Series2"]["PointWidth"] = "0.6";

            chart1.Series["Series3"]["DrawingStyle"] = "Cylinder";
            chart1.Series["Series3"]["PointWidth"] = "0.6";


        }

        void setChart11(object obj)
        {
            List<string> xValues = new List<string>();
            List<double> yValues1 = new List<double>();
            List<double> yValues2 = new List<double>();
            if (radioDay.Checked)
            {
                List<Rpt2Day> list = (List<Rpt2Day>)obj;
                foreach (var data in list)
                {
                    xValues.Add(data.rptDate.ToString("yyyy/MM/dd HH:mm:ss"));
                    yValues1.Add(Convert.ToDouble(data.elec));
                    yValues2.Add(Convert.ToDouble(data.totalElec));
                   
                }
            }
            else if (radioMonth.Checked)
            {
                List<Rpt2Month> list = (List<Rpt2Month>)obj;
                foreach (var data in list)
                {
                    xValues.Add(data.rptDate.ToString("yyyy/MM/dd"));
                    yValues1.Add(Convert.ToDouble(data.elec));
                    yValues2.Add(Convert.ToDouble(data.totalElec));

                }
            }
            else if (radioQuarter.Checked)
            {
                List<Rpt2Quarter> list = (List<Rpt2Quarter>)obj;
                foreach (var data in list)
                {
                    xValues.Add(data.rptDate.ToString("yyyy/MM"));
                    yValues1.Add(Convert.ToDouble(data.elec));
                    yValues2.Add(Convert.ToDouble(data.totalElec));

                }
            }
            else if (radioYear.Checked)
            {
                List<Rpt2Year> list = (List<Rpt2Year>)obj;
                foreach (var data in list)
                {
                    xValues.Add(data.rptDate.ToString("yyyy/MM"));
                    yValues1.Add(Convert.ToDouble(data.elec));
                    yValues2.Add(Convert.ToDouble(data.totalElec));

                }
            }
           
            chart1.Series["Series1"].Points.DataBindXY(xValues, yValues1);
            chart1.Series["Series1"].LegendText = "用電量";
            chart1.Series["Series1"]["StackedGroupName"] = "Series1";
            chart1.Series["Series1"].IsValueShownAsLabel = true;
            chart1.Series["Series2"].Points.DataBindXY(xValues, yValues2);
            chart1.Series["Series2"].LegendText = "累積用電量";
            chart1.Series["Series2"]["StackedGroupName"] = "Series1";
            chart1.Series["Series2"].IsValueShownAsLabel = true;
        }
        void setChart22(object obj)
        {
            List<string> xValues = new List<string>();
            List<double> yValues1 = new List<double>();
            List<double> yValues2 = new List<double>();
            List<double> yValues3 = new List<double>();
            List<double> yValues4 = new List<double>();
            if (radioDay.Checked)
            {
                List<Rpt1Day> list = (List<Rpt1Day>)obj;
                foreach(var data in list)
                {
                    xValues.Add(data.rptDate.ToString("yyyy/MM/dd HH:mm:ss"));
                    yValues1.Add(Convert.ToDouble(data.maxV));
                    yValues2.Add(Convert.ToDouble(data.minV));
                    yValues3.Add(Convert.ToDouble(data.maxI));
                    yValues4.Add(Convert.ToDouble(data.minI));
                }
            }
            else if (radioMonth.Checked)
            {
                List<Rpt1Month> list = (List<Rpt1Month>)obj;
                foreach (var data in list)
                {
                    xValues.Add(data.rptDate.ToString("yyyy/MM/dd"));
                    yValues1.Add(Convert.ToDouble(data.maxV));
                    yValues2.Add(Convert.ToDouble(data.minV));
                    yValues3.Add(Convert.ToDouble(data.maxI));
                    yValues4.Add(Convert.ToDouble(data.minI));
                }
            }
            else if (radioQuarter.Checked)
            {
                List<Rpt1Quarter> list = (List<Rpt1Quarter>)obj;
                foreach (var data in list)
                {
                    xValues.Add(data.rptDate.ToString("yyyy/MM"));
                    yValues1.Add(Convert.ToDouble(data.maxV));
                    yValues2.Add(Convert.ToDouble(data.minV));
                    yValues3.Add(Convert.ToDouble(data.maxI));
                    yValues4.Add(Convert.ToDouble(data.minI));
                }
            }
            else if (radioYear.Checked)
            {
                List<Rpt1Year> list = (List<Rpt1Year>)obj;
                foreach (var data in list)
                {
                    xValues.Add(data.rptDate.ToString("yyyy/MM"));
                    yValues1.Add(Convert.ToDouble(data.maxV));
                    yValues2.Add(Convert.ToDouble(data.minV));
                    yValues3.Add(Convert.ToDouble(data.maxI));
                    yValues4.Add(Convert.ToDouble(data.minI));
                }
            }

            chart2.Series["Series1"].Points.DataBindXY(xValues, yValues1);
            chart2.Series["Series1"].YAxisType = AxisType.Primary;
            chart2.Series["Series1"].XAxisType = AxisType.Primary;

            chart2.Series["Series2"].Points.DataBindXY(xValues, yValues2);
            chart2.Series["Series2"].YAxisType = AxisType.Primary;
            chart2.Series["Series2"].XAxisType = AxisType.Primary;

            chart2.Series["Series3"].Points.DataBindXY(xValues, yValues3);
            chart2.Series["Series3"].YAxisType = AxisType.Secondary;
            chart2.Series["Series3"].XAxisType = AxisType.Primary;

            chart2.Series["Series4"].Points.DataBindXY(xValues, yValues4);
            chart2.Series["Series4"].YAxisType = AxisType.Secondary;
            chart2.Series["Series4"].XAxisType = AxisType.Primary;
        }
        void setChart2()
        {
            DateTime a = DateTime.Now;
            DateTime b = a.AddHours(2);
            DateTime c = b.AddHours(2);

            List<string> xValues = new List<string>();
            xValues.Add(a.ToString("yyyy-mm-dd HH:mm:ss"));
            xValues.Add(a.AddHours(2).ToString("yyyy-mm-dd HH:mm:ss"));
            xValues.Add(a.AddHours(4).ToString("yyyy-mm-dd HH:mm:ss"));
            xValues.Add(a.AddHours(6).ToString("yyyy-mm-dd HH:mm:ss"));
            xValues.Add(a.AddHours(8).ToString("yyyy-mm-dd HH:mm:ss"));
            xValues.Add(a.AddHours(10).ToString("yyyy-mm-dd HH:mm:ss"));
            xValues.Add(a.AddHours(12).ToString("yyyy-mm-dd HH:mm:ss"));
            xValues.Add(a.AddHours(14).ToString("yyyy-mm-dd HH:mm:ss"));
            xValues.Add(a.AddHours(16).ToString("yyyy-mm-dd HH:mm:ss"));

            double[] yValues = { 788, 1125, 1233, 950, 1100, 866, 1251, 1066, 999 };
            double[] yValues2 = { 1212, 1200, 986, 976, 1111, 1053, 966, 1321, 1254 };

            //chart2.ChartAreas[0].AxisX.Interval = 3;
            
            chart2.Series["Series1"].Points.DataBindXY(xValues, yValues);
            chart2.Series["Series1"].YAxisType = AxisType.Primary;
            chart2.Series["Series1"].XAxisType = AxisType.Primary;

            chart2.Series["Series2"].Points.DataBindXY(xValues, yValues2);
            chart2.Series["Series2"].YAxisType = AxisType.Primary;
            chart2.Series["Series2"].XAxisType = AxisType.Primary;


        }
        void setChart33(object obj)
        {
            List<string> xValues = new List<string>();
            List<double> yValues = new List<double>();
            Dictionary<string, double> dic = new Dictionary<string, double>();
            double sum = 0;
            if (radioDay.Checked)
            {
                List<Rpt2Day> list = (List<Rpt2Day>)obj;
                foreach (var data in list)
                {
                    if (dic.ContainsKey(data.point))
                    {
                        dic[data.point] += Convert.ToDouble(data.elec);
                        sum += Convert.ToDouble(data.elec);
                    }
                    else
                    {
                        dic.Add(data.point, Convert.ToDouble(data.elec));
                        sum += Convert.ToDouble(data.elec);
                    }
                }
            }
            else if (radioMonth.Checked)
            {
                List<Rpt2Month> list = (List<Rpt2Month>)obj;
                foreach (var data in list)
                {
                    if (dic.ContainsKey(data.point))
                    {
                        dic[data.point] += Convert.ToDouble(data.elec);
                        sum += Convert.ToDouble(data.elec);
                    }
                    else
                    {
                        dic.Add(data.point, Convert.ToDouble(data.elec));
                        sum += Convert.ToDouble(data.elec);
                    }
                }
            }
            else if (radioQuarter.Checked)
            {
                List<Rpt2Quarter> list = (List<Rpt2Quarter>)obj;
                foreach (var data in list)
                {
                    if (dic.ContainsKey(data.point))
                    {
                        dic[data.point] += Convert.ToDouble(data.elec);
                        sum += Convert.ToDouble(data.elec);
                    }
                    else
                    {
                        dic.Add(data.point, Convert.ToDouble(data.elec));
                        sum += Convert.ToDouble(data.elec);
                    }
                }
            }
            else if (radioYear.Checked)
            {
                List<Rpt2Year> list = (List<Rpt2Year>)obj;
                foreach (var data in list)
                {
                    if (dic.ContainsKey(data.point))
                    {
                        dic[data.point] += Convert.ToDouble(data.elec);
                        sum += Convert.ToDouble(data.elec);
                    }
                    else
                    {
                        dic.Add(data.point, Convert.ToDouble(data.elec));
                        sum += Convert.ToDouble(data.elec);
                    }
                }
            }
          
            foreach(var item in dic)
            {
                xValues.Add(item.Key + " 用電量");
                double result = 0;
                result = Math.Round(item.Value / sum, 4) * 100;
                yValues.Add(result);
            }

            chart3.Series["Series1"].Points.DataBindXY(xValues, yValues);
            chart3.Series["Series1"]["PieLabelStyle"] = "Outside";
         
            chart3.Series["Series1"].IsValueShownAsLabel = true;
        }
        void setChart3()
        {
            string[] xValues = { "空調用電量(度)", "插座用電量(度)", "照明用電量(度)" };

            double[] yValues = { 51.5, 13.3, 35.3 };
            chart3.Series["Series1"].Points.DataBindXY(xValues, yValues);
            chart3.Series["Series1"]["PieLabelStyle"] = "Outside";
            //chart3.Series["Series1"]["PieDrawingStyle"] = "SoftEdge";

            chart3.Series["Series1"].IsValueShownAsLabel = true;
        }

        

        private void Form_Report1_Load(object sender, EventArgs e)
        {
            comboBox1.SelectedIndex = 1;
            radioDay.Checked = true;
           

            //DateTime startDate = new DateTime(2018, 3, 11);
            //DateTime endDate = new DateTime(2018, 3, 12);
            //getRpt1DayData("TSS7", "", startDate, endDate);

            //DateTime startDate1 = new DateTime(2018, 4, 1);
            //DateTime endDate1 = new DateTime(2018, 4, 1);
            //getRpt1MonthData("TSS7", "", startDate1, endDate1);


            //DateTime startDate2 = new DateTime(2018, 1, 1);
            //DateTime endDate2 = new DateTime(2018, 3, 1);
            //getRpt1YearData("TSS7", "", startDate2, endDate2);
            //DateTime t = new DateTime(2017, 4, 1);
            //getRpt1QuarterData("TSS7", "", t);
        }

        private async void getRpt1DayData(string loc,string point,DateTime startDate, DateTime endDate)
        {
            string baseAddr = "http://localhost:48965/";
            List<Rpt1Day> rptData = new List<Rpt1Day>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseAddr);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                string uriStr = "api/Rpt1Day?loc=" + loc + "&point=" + point + "&startDate=" + startDate.ToString("yyyy/MM/dd") + "&endDate=" + endDate.ToString("yyyy/MM/dd");
                HttpResponseMessage response = await client.GetAsync(uriStr);
                if (response.IsSuccessStatusCode)
                {
                    rptData = await response.Content.ReadAsAsync<List<Rpt1Day>>();
                }
            }

            if(rptData.Count()>0)
            {
                dataGridView1.Columns.Clear();

                AddColumn("日期", "rptDate", "rptDate");
                AddColumn("時間(每小時)", "rptTime", "rptTime");
                AddColumn("位置", "loc","loc");
                AddColumn("點位", "point","point");
                AddColumn("電壓最大(V)", "maxV","maxV");
                AddColumn("電壓最小(V)", "minV","minV");
                AddColumn("電流最大(I)", "maxI","maxI");
                AddColumn("電流最小(I)", "minI","minI");
                BindingSource bs = new BindingSource();
                bs.DataSource = rptData;
                dataGridView1.DataSource = bs;
                dataGridView1.Columns["SerialNo"].Visible = false;
                dataGridView1.Columns["insertTime"].Visible = false;
                dataGridView1.Columns["rptDate"].DefaultCellStyle.Format = "yyyy/MM/dd";
                setChart22(rptData);
            }
            else
            {
                dataGridView1.DataSource = null;
                chart2.Series.Clear();
            }

        }

        private async void getRpt1MonthData(string loc, string point, DateTime startDate)
        {
            string baseAddr = "http://localhost:48965/";
            List<Rpt1Month> rptData = new List<Rpt1Month>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseAddr);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                string uriStr = "api/Rpt1Month?loc=" + loc + "&point=" + point + "&startDate=" + startDate.ToString("yyyy/MM/dd") ;
                HttpResponseMessage response = await client.GetAsync(uriStr);
                if (response.IsSuccessStatusCode)
                {
                    rptData = await response.Content.ReadAsAsync<List<Rpt1Month>>();
                }
            }

            if (rptData.Count() > 0)
            {
                dataGridView1.Columns.Clear();

                AddColumn("日期", "rptDate", "rptDate");
                AddColumn("月份", "rptDate", "rptDate1");
                AddColumn("位置", "loc", "loc");
                AddColumn("點位", "point", "point");
                AddColumn("電壓最大(V)", "maxV", "maxV");
                AddColumn("電壓最小(V)", "minV", "minV");
                AddColumn("電流最大(I)", "maxI", "maxI");
                AddColumn("電流最小(I)", "minI", "minI");
                dataGridView1.Columns["rptDate"].DefaultCellStyle.Format = "yyyy/MM/dd";
                dataGridView1.Columns["rptDate1"].DefaultCellStyle.Format = "MM";
                BindingSource bs = new BindingSource();
                bs.DataSource = rptData;
                dataGridView1.DataSource = bs;
                dataGridView1.Columns["SerialNo"].Visible = false;
                dataGridView1.Columns["insertTime"].Visible = false;

                setChart22(rptData);
            }
            else
            {
                dataGridView1.DataSource = null;
            }

        }

        private async void getRpt1QuarterData(string loc, string point, DateTime sDate)
        {
            string baseAddr = "http://localhost:48965/";
            List<Rpt1Quarter> rptData = new List<Rpt1Quarter>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseAddr);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                string uriStr = "api/Rpt1Year?loc=" + loc + "&point=" + point + "&sDate=" + sDate.ToString("yyyy/MM/dd");
                HttpResponseMessage response = await client.GetAsync(uriStr);
                if (response.IsSuccessStatusCode)
                {
                    rptData = await response.Content.ReadAsAsync<List<Rpt1Quarter>>();
                }
            }

            if (rptData.Count() > 0)
            {
                dataGridView1.Columns.Clear();

                AddColumn("年度", "rptDate", "rptDate");
                AddColumn("月份", "rptDate", "rptDate1");
                AddColumn("位置", "loc", "loc");
                AddColumn("點位", "point", "point");
                AddColumn("電壓最大(V)", "maxV", "maxV");
                AddColumn("電壓最小(V)", "minV", "minV");
                AddColumn("電流最大(I)", "maxI", "maxI");
                AddColumn("電流最小(I)", "minI", "minI");
                dataGridView1.Columns["rptDate"].DefaultCellStyle.Format = "yyyy";
                dataGridView1.Columns["rptDate1"].DefaultCellStyle.Format = "MM";
                BindingSource bs = new BindingSource();
                bs.DataSource = rptData;
                dataGridView1.DataSource = bs;
                dataGridView1.Columns["SerialNo"].Visible = false;
                dataGridView1.Columns["insertTime"].Visible = false;

                setChart22(rptData);
            }
            else
            {
                dataGridView1.DataSource = null;
            }

        }

        private async void getRpt1YearData(string loc, string point, int year)
        {
            string baseAddr = "http://localhost:48965/";
            List<Rpt1Year> rptData = new List<Rpt1Year>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseAddr);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                string uriStr = "api/Rpt1Year?loc=" + loc + "&point=" + point + "&year=" + year;
                HttpResponseMessage response = await client.GetAsync(uriStr);
                if (response.IsSuccessStatusCode)
                {
                    rptData = await response.Content.ReadAsAsync<List<Rpt1Year>>();
                }

                if (rptData.Count() > 0)
                {
                    dataGridView1.Columns.Clear();

                    AddColumn("年度", "rptDate", "rptDate");
                    AddColumn("月份", "rptDate", "rptDate1");
                    AddColumn("位置", "loc", "loc");
                    AddColumn("點位", "point", "point");
                    AddColumn("電壓最大(V)", "maxV", "maxV");
                    AddColumn("電壓最小(V)", "minV", "minV");
                    AddColumn("電流最大(I)", "maxI", "maxI");
                    AddColumn("電流最小(I)", "minI", "minI");
                    dataGridView1.Columns["rptDate"].DefaultCellStyle.Format = "yyyy";
                    dataGridView1.Columns["rptDate1"].DefaultCellStyle.Format = "MM";
                    BindingSource bs = new BindingSource();
                    bs.DataSource = rptData;
                    dataGridView1.DataSource = bs;
                    dataGridView1.Columns["SerialNo"].Visible = false;
                    dataGridView1.Columns["insertTime"].Visible = false;

                    setChart22(rptData);
                }
                else
                {
                    dataGridView1.DataSource = null;
                }

            }

        }

        private async void getRpt2DayData(string loc, string point, DateTime startDate, DateTime endDate,int chart)
        {
            string baseAddr = "http://localhost:48965/";
            List<Rpt2Day> rptData = new List<Rpt2Day>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseAddr);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                string uriStr = "api/Rpt2Day?loc=" + loc + "&point=" + point + "&sDate=" + startDate.ToString("yyyy/MM/dd") + "&eDate=" + endDate.ToString("yyyy/MM/dd");
                HttpResponseMessage response = await client.GetAsync(uriStr);
                if (response.IsSuccessStatusCode)
                {
                    rptData = await response.Content.ReadAsAsync<List<Rpt2Day>>();
                }
            }

            if (rptData.Count() > 0)
            {
                dataGridView1.Columns.Clear();

                AddColumn("日期", "rptDate", "rptDate");
                AddColumn("時間(每小時)", "rptTime", "rptTime");
                AddColumn("位置", "loc", "loc");               
                AddColumn("點位", "point", "point");
                AddColumn("用電量", "elec", "elec");
                AddColumn("總用電量", "totalElec", "totalElec");
              
                BindingSource bs = new BindingSource();
                bs.DataSource = rptData;
                dataGridView1.DataSource = bs;
                dataGridView1.Columns["SerialNo"].Visible = false;
                dataGridView1.Columns["insertTime"].Visible = false;               
                dataGridView1.Columns["rptDate"].DefaultCellStyle.Format = "yyyy/MM/dd";
                if (chart == 2)
                    setChart11(rptData);
                else if(chart == 3)
                {
                    dataGridView1.Columns["point"].Visible = false;
                    setChart33(rptData);
                }
            }
            else
            {
                dataGridView1.DataSource = null;
                //chart2.Series.Clear();
            }

        }

        private async void getRpt2MonthData(string loc, string point, DateTime startDate,int chart)
        {
            string baseAddr = "http://localhost:48965/";
            List<Rpt2Month> rptData = new List<Rpt2Month>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseAddr);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                string uriStr = "api/Rpt2Month?loc=" + loc + "&point=" + point + "&sDate=" + startDate.ToString("yyyy/MM/dd");
                HttpResponseMessage response = await client.GetAsync(uriStr);
                if (response.IsSuccessStatusCode)
                {
                    rptData = await response.Content.ReadAsAsync<List<Rpt2Month>>();
                }
            }

            if (rptData.Count() > 0)
            {
                dataGridView1.Columns.Clear();

                AddColumn("日期", "rptDate", "rptDate");
                AddColumn("月份", "rptDate", "rptDate1");
                AddColumn("位置", "loc", "loc");
                AddColumn("點位", "point", "point");
                AddColumn("用電量", "elec", "elec");
                AddColumn("總用電量", "totalElec", "totalElec");

                dataGridView1.Columns["rptDate"].DefaultCellStyle.Format = "yyyy/MM/dd";
                dataGridView1.Columns["rptDate1"].DefaultCellStyle.Format = "MM";
                BindingSource bs = new BindingSource();
                bs.DataSource = rptData;
                dataGridView1.DataSource = bs;
                dataGridView1.Columns["SerialNo"].Visible = false;
                dataGridView1.Columns["insertTime"].Visible = false;
                if (chart == 2)
                    setChart11(rptData);
                else if (chart == 3)
                {
                    dataGridView1.Columns["point"].Visible = false;
                    setChart33(rptData);
                }
            }
            else
            {
                dataGridView1.DataSource = null;
            }

        }

        private async void getRpt2QuarterData(string loc, string point, DateTime sDate,int chart)
        {
            string baseAddr = "http://localhost:48965/";
            List<Rpt2Quarter> rptData = new List<Rpt2Quarter>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseAddr);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                string uriStr = "api/Rpt2Year?loc=" + loc + "&point=" + point + "&sDate=" + sDate.ToString("yyyy/MM/dd");
                HttpResponseMessage response = await client.GetAsync(uriStr);
                if (response.IsSuccessStatusCode)
                {
                    rptData = await response.Content.ReadAsAsync<List<Rpt2Quarter>>();
                }
            }

            if (rptData.Count() > 0)
            {
                dataGridView1.Columns.Clear();

                AddColumn("年度", "rptDate", "rptDate");
                AddColumn("月份", "rptDate", "rptDate1");
                AddColumn("位置", "loc", "loc");
                AddColumn("點位", "point", "point");
                AddColumn("用電量", "elec", "elec");
                AddColumn("總用電量", "totalElec", "totalElec");
                dataGridView1.Columns["rptDate"].DefaultCellStyle.Format = "yyyy";
                dataGridView1.Columns["rptDate1"].DefaultCellStyle.Format = "MM";
                BindingSource bs = new BindingSource();
                bs.DataSource = rptData;
                dataGridView1.DataSource = bs;
                dataGridView1.Columns["SerialNo"].Visible = false;
                dataGridView1.Columns["insertTime"].Visible = false;
                if (chart == 2)
                    setChart11(rptData);
                else if (chart == 3)
                {
                    dataGridView1.Columns["point"].Visible = false;
                    setChart33(rptData);
                }
            }
            else
            {
                dataGridView1.DataSource = null;
            }

        }

        private async void getRpt2YearData(string loc, string point, int year,int chart)
        {
            string baseAddr = "http://localhost:48965/";
            List<Rpt2Year> rptData = new List<Rpt2Year>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseAddr);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                string uriStr = "api/Rpt2Year?loc=" + loc + "&point=" + point + "&year=" + year;
                HttpResponseMessage response = await client.GetAsync(uriStr);
                if (response.IsSuccessStatusCode)
                {
                    rptData = await response.Content.ReadAsAsync<List<Rpt2Year>>();
                }

                if (rptData.Count() > 0)
                {
                    dataGridView1.Columns.Clear();

                    AddColumn("年度", "rptDate", "rptDate");
                    AddColumn("月份", "rptDate", "rptDate1");
                    AddColumn("位置", "loc", "loc");
                    AddColumn("點位", "point", "point");
                    AddColumn("用電量", "elec", "elec");
                    AddColumn("總用電量", "totalElec", "totalElec");
                    dataGridView1.Columns["rptDate"].DefaultCellStyle.Format = "yyyy";
                    dataGridView1.Columns["rptDate1"].DefaultCellStyle.Format = "MM";
                    BindingSource bs = new BindingSource();
                    bs.DataSource = rptData;
                    dataGridView1.DataSource = bs;
                    dataGridView1.Columns["SerialNo"].Visible = false;
                    dataGridView1.Columns["insertTime"].Visible = false;
                    if (chart == 2)
                        setChart11(rptData);
                    else if (chart == 3)
                    {
                        dataGridView1.Columns["point"].Visible = false;
                        setChart33(rptData);
                    }
                }
                else
                {
                    dataGridView1.DataSource = null;
                }

            }

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBox2.SelectedIndex = 0;
            comboBox3.Items.Clear();
            comboBox3.Text = "";
            if (comboBox1.SelectedIndex == 1)
            {
                chart1.Visible = false;
                chart2.Visible = true;
                chart3.Visible = false;
       
            }
            else if (comboBox1.SelectedIndex == 2)
            {
                chart1.Visible = true;
                chart2.Visible = false;
                chart3.Visible = false;
            }
            else if (comboBox1.SelectedIndex == 3)
            {
                chart1.Visible = false;
                chart2.Visible = false;
                chart3.Visible = true;
            }
        
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(comboBox2.SelectedItem.ToString().Equals("     -TSS7") && comboBox1.SelectedItem.ToString().Equals("電壓、電流之最大/最小值"))
            {
                comboBox3.Items.Clear();
                ComboBoxItem item = new ComboBoxItem();
                item.Text = "H02 R-S PHASE VOLTAGE/CURRENT";
                item.Value = 0;
                comboBox3.Items.Add(item);
                item = new ComboBoxItem();
                item.Text = "H02 S-T PHASE VOLTAGE/CURRENT";
                item.Value = 1;
                comboBox3.Items.Add(item);
                item = new ComboBoxItem();
                item.Text = "H02 T-R PHASE VOLTAGE/CURRENT";
                item.Value = 2;
                comboBox3.Items.Add(item);
                item = new ComboBoxItem();
                item.Text = "H03 R-S PHASE VOLTAGE/CURRENT";
                item.Value = 3;
                comboBox3.Items.Add(item);
                item = new ComboBoxItem();
                item.Text = "H03 S-T PHASE VOLTAGE/CURRENT";
                item.Value = 4;
                comboBox3.Items.Add(item);
                item = new ComboBoxItem();
                item.Text = "H03 T-R PHASE VOLTAGE/CURRENT";
                item.Value = 5;
                comboBox3.Items.Add(item);
                item = new ComboBoxItem();
                item.Text = "H04 R-S PHASE VOLTAGE/CURRENT";
                item.Value = 6;
                comboBox3.Items.Add(item);
                item = new ComboBoxItem();
                item.Text = "H04 S-T PHASE VOLTAGE/CURRENT";
                item.Value = 7;
                comboBox3.Items.Add(item);
                item = new ComboBoxItem();
                item.Text = "H04 T-R PHASE VOLTAGE/CURRENT";
                item.Value = 8;
                comboBox3.Items.Add(item);
                item = new ComboBoxItem();
                item.Text = "MC21 DC VOLTAGE/CURRENT";
                item.Value = 9;
                comboBox3.Items.Add(item);
                item = new ComboBoxItem();
                item.Text = "MC22 DC VOLTAGE/CURRENT";
                item.Value = 10;
                comboBox3.Items.Add(item);
                item = new ComboBoxItem();
                item.Text = "MC81 DC VOLTAGE/CURRENT";
                item.Value = 11;
                comboBox3.Items.Add(item);

                comboBox3.SelectedIndex = 1;
            }
            if (comboBox2.SelectedItem.ToString().Equals("     -TSS7") && comboBox1.SelectedItem.ToString().Equals("各迴路尖離峰及總用電量"))
            {
                comboBox3.Items.Clear();
                ComboBoxItem item = new ComboBoxItem();
                item.Text = "H02 POWER (KWH)";
                item.Value = 0;                
                comboBox3.Items.Add(item);
                item = new ComboBoxItem();
                item.Text = "H03 POWER (KWH)";
                item.Value = 1;               
                comboBox3.Items.Add(item);
                item = new ComboBoxItem();
                item.Text = "H04 POWER (KWH)";
                item.Value = 2;
                comboBox3.Items.Add(item);
                comboBox3.SelectedIndex = 0;
            }
        }


        private void radioButton_CheckedChanged(object sender, EventArgs e)

        {

            if (sender is RadioButton)

            {
               
                RadioButton radio = (RadioButton)sender;
                if(radio.Text.Equals("日"))
                {
                    dateTimePicker1.Enabled = true;
                    dateTimePicker2.Enabled = true;
                    comboBox4.Enabled = false;
                    comboBox5.Enabled = false;
                    comboBox6.Enabled = false;
                    comboBox7.Enabled = false;
                    comboBox8.Enabled = false;

                }
                if(radio.Text.Equals("月"))
                {
                    //initGrid12();
                    dateTimePicker1.Enabled = false;
                    dateTimePicker2.Enabled = false;
                    comboBox4.Enabled = true;
                    comboBox5.Enabled = true;
                    comboBox6.Enabled = false;
                    comboBox7.Enabled = false;
                    comboBox8.Enabled = false;

                }
                if (radio.Text.Equals("季"))
                {
                    dateTimePicker1.Enabled = false;
                    dateTimePicker2.Enabled = false;
                    comboBox4.Enabled = false;
                    comboBox5.Enabled = false;
                    comboBox6.Enabled = true;
                    comboBox7.Enabled = true;
                    comboBox8.Enabled = false;

                }
                if (radio.Text.Equals("年度"))
                {
                    dateTimePicker1.Enabled = false;
                    dateTimePicker2.Enabled = false;
                    comboBox4.Enabled = false;
                    comboBox5.Enabled = false;
                    comboBox6.Enabled = false;
                    comboBox7.Enabled = false;
                    comboBox8.Enabled = true;

                }
                //setChart11();
                //setChart22();
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            System.Drawing.Printing.PrintDocument doc = new System.Drawing.Printing.PrintDocument();

            doc.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(doc_PrintPage);

            printArea(tabControl1.SelectedTab);
            doc.Print();
        }
        Bitmap bmp;

        void printArea(TabPage tp)
        {
            bmp = new Bitmap(tp.Width, tp.Height, tp.CreateGraphics());

            tp.DrawToBitmap(bmp, new Rectangle(0, 0, tp.Width, tp.Height));
        }
        private void doc_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)

        {
            
            //Panel grd = new Panel();
            
            //Bitmap bmp = new Bitmap(tabPage1.Width, tabPage1.Height, tabPage1.CreateGraphics());

            //tabPage1.DrawToBitmap(bmp, new Rectangle(0, 0, tabPage1.Width, tabPage1.Height));

            RectangleF bounds = e.PageSettings.PrintableArea;

            float factor = ((float)bmp.Height / (float)bmp.Width);

            e.Graphics.DrawImage(bmp, bounds.Left, bounds.Top, bounds.Width, factor * bounds.Width);

        }


        private void ExportToExcel(DataGridView grid)

        {

            // Creating a Excel object. 

            Microsoft.Office.Interop.Excel._Application excel = new Microsoft.Office.Interop.Excel.Application();

            Microsoft.Office.Interop.Excel._Workbook workbook = excel.Workbooks.Add(Type.Missing);

            Microsoft.Office.Interop.Excel._Worksheet worksheet = null;



            try

            {



                worksheet = workbook.ActiveSheet;



                worksheet.Name = "ExportedFromDatGrid";



                int cellRowIndex = 1;

                int cellColumnIndex = 1;



                //Loop through each row and read value from each column. 

                for (int i = 0; i < grid.Rows.Count - 1; i++)

                {

                    for (int j = 0; j < grid.Columns.Count; j++)

                    {

                        // Excel index starts from 1,1. As first Row would have the Column headers, adding a condition check. 

                        if (cellRowIndex == 1)

                        {

                            worksheet.Cells[cellRowIndex, cellColumnIndex] = grid.Columns[j].HeaderText;

                        }

                        else

                        {

                            worksheet.Cells[cellRowIndex, cellColumnIndex] = grid.Rows[i].Cells[j].Value.ToString();

                        }

                        cellColumnIndex++;

                    }

                    cellColumnIndex = 1;

                    cellRowIndex++;

                }



                //Getting the location and file name of the excel to save from user. 

                SaveFileDialog saveDialog = new SaveFileDialog();

                saveDialog.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";

                saveDialog.FilterIndex = 2;



                if (saveDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)

                {

                    workbook.SaveAs(saveDialog.FileName);

                    MessageBox.Show("Export Successful");

                }

            }

            catch (System.Exception ex)

            {

                MessageBox.Show(ex.Message);

            }

            finally

            {

                excel.Quit();

                workbook = null;

                excel = null;

            }



        }

        private void button12_Click(object sender, EventArgs e)
        {
            if(tabControl1.SelectedIndex ==0)
            {
                ExportToExcel(dataGridView1);
            }
            if (tabControl1.SelectedIndex == 1)
            {
                ExportToExcel(dataGridView2);
            }
            if (tabControl1.SelectedIndex == 2)
            {
                ExportToExcel(dataGridView3);
            }
        }

        private void button13_Click(object sender, EventArgs e)
        {

            
            string loc = "";
            string point = "";
            if (radioDay.Checked)
            {
                loc = comboBox2.SelectedItem.ToString().Trim().Substring(1);
                if (comboBox3.SelectedItem != null)
                    point = comboBox3.SelectedItem.ToString();
                DateTime sDate = dateTimePicker1.Value;
                DateTime eDate = dateTimePicker2.Value;
                
                if (comboBox1.SelectedIndex == 1)
                {
                    getRpt1DayData(loc, point, sDate, eDate);
                 
                }
                else if(comboBox1.SelectedIndex == 2)
                {
                    getRpt2DayData(loc, point, sDate, eDate, 2);
                }
                else if(comboBox1.SelectedIndex == 3)
                {
                    getRpt2DayData(loc, point, sDate, eDate, 3);
                }

              
            }
            else if(radioMonth.Checked)
            {

                loc = comboBox2.SelectedItem.ToString().Trim().Substring(1);
                if (comboBox3.SelectedItem != null)
                    point = comboBox3.SelectedItem.ToString();
                string year = comboBox4.SelectedItem.ToString();
                string month = comboBox5.SelectedItem.ToString().Remove(comboBox5.SelectedItem.ToString().Length - 1);
                DateTime sDate = new DateTime(Convert.ToInt32(year), Convert.ToInt32(month), 1);
                if (comboBox1.SelectedIndex == 1)
                {
                    getRpt1MonthData(loc, point, sDate);

                }
                else if (comboBox1.SelectedIndex == 2)
                {
                    getRpt2MonthData(loc, point, sDate, 2);
                }
                else if (comboBox1.SelectedIndex == 3)
                {
                    getRpt2MonthData(loc, point, sDate, 3);
                }

            }
            else if (radioQuarter.Checked)
            {
                loc = comboBox2.SelectedItem.ToString().Trim().Substring(1);
                if (comboBox3.SelectedItem != null)
                    point = comboBox3.SelectedItem.ToString();
                int year = Convert.ToInt32(comboBox7.SelectedItem);
                int month = 1;
                switch (comboBox6.SelectedItem.ToString())
                {
                    case "第一季":
                        month = 1;
                        break;
                    case "第二季":
                        month = 4;
                        break;
                    case "第三季":
                        month = 7;
                        break;
                    case "第四季":
                        month = 10;
                        break;
                }
                DateTime sDate = new DateTime(year, month, 1);

                if (comboBox1.SelectedIndex == 1)
                {
                 
                    getRpt1QuarterData(loc, point, sDate);

                }
                else if (comboBox1.SelectedIndex == 2)
                {
                    getRpt2QuarterData(loc, point, sDate, 2);
                }
                else if (comboBox1.SelectedIndex == 3)
                {
                    getRpt2QuarterData(loc, point, sDate, 3);
                }

            }
            else if(radioYear.Checked)
            {
                loc = comboBox2.SelectedItem.ToString().Trim().Substring(1);
                if (comboBox3.SelectedItem != null)
                    point = comboBox3.SelectedItem.ToString();
                int year = Convert.ToInt32(comboBox8.SelectedItem);
                if (comboBox1.SelectedIndex == 1)
                {
                    getRpt1YearData(loc, point, year);

                }
                else if (comboBox1.SelectedIndex == 2)
                {
                    getRpt2YearData(loc, point, year, 2);
                }
                else if (comboBox1.SelectedIndex == 3)
                {
                    getRpt2YearData(loc, point, year, 3);
                }

            }

            //dt1.Select(expression);
            //dataGridView1.DataSource = dt1.Select(expression);
            //setChart11();
            //setChart22();
        }


        public void AddColumn(string strHeader,string bind,string name)
        {
            DataGridViewColumn column = new DataGridViewTextBoxColumn();
            column.HeaderText = strHeader;
            column.DataPropertyName = bind;
            column.Name = name;
            dataGridView1.Columns.Add(column);
        }

      
    }
}
