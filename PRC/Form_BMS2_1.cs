﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PRC
{
    public partial class Form_BMS2_1 : Form
    {
        public Form_BMS2_1()
        {
            InitializeComponent();
        }

        private void tableLayoutPanel1_CellPaint(object sender, TableLayoutCellPaintEventArgs e)
        {
            Pen pen = new Pen(Color.Black);
            pen.Width = 2;

            e.Graphics.DrawRectangle(pen, e.CellBounds.X, e.CellBounds.Y, e.CellBounds.X + this.Width, e.CellBounds.Y + this.Height);

        }

        private void tableLayoutPanel2_CellPaint(object sender, TableLayoutCellPaintEventArgs e)
        {
            Pen pen = new Pen(Color.Black);
            pen.Width = 2;

            e.Graphics.DrawRectangle(pen, e.CellBounds.X, e.CellBounds.Y, e.CellBounds.X + this.Width, e.CellBounds.Y + this.Height);

        }

        private void tableLayoutPanel3_CellPaint(object sender, TableLayoutCellPaintEventArgs e)
        {
            Pen pen = new Pen(Color.Black);
            pen.Width = 2;

            e.Graphics.DrawRectangle(pen, e.CellBounds.X, e.CellBounds.Y, e.CellBounds.X + this.Width, e.CellBounds.Y + this.Height);

        }
    }
}
