﻿using PRC.MyClass;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PRC
{
    public partial class Form_SignalStationBase : Form
    {
        public Form_SignalStationBase()
        {
            InitializeComponent();
            this._c16 = new Form_SignalC16();
            this._c16.TopLevel = false;
            ITMSMessageFormat.RegistItmsForm("C16", this._c16);
        }

        private void btn_C16Station_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rb = sender as RadioButton;
            if (rb.Checked)
            {
                pnl_Container.Controls.Clear();
                pnl_Container.Controls.Add(this._c16);
                this._c16.Show();
            }
            else
            {
                this._c16.Hide();
            }
        }


        private Form_SignalC16 _c16;
    }
}
