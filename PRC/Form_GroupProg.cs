﻿using ICSC.Database.MSSQL;
using PRC.MyClass;
using PRC.Tools;
using PRC.WebApiResults;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PRC
{

    public partial class Form_GroupProg : Form
    {
        public Form_GroupProg()
        {
            InitializeComponent();
        }

        public Form_GroupProg(PermissionObject permission):this()
        {
            button3.Enabled = permission.CanUpdate;
        }

        #region Init

        #region Init dgv

        private void InitDgv_Permission()
        {
            dgv_Permission.Font = new Font("微軟正黑體", 18, GraphicsUnit.Pixel);
            dgv_Permission.Columns["DisplayFun"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter ;
            dgv_Permission.Columns["SearchFun"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgv_Permission.Columns["InsertFun"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgv_Permission.Columns["UpdateFun"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgv_Permission.Columns["DeleteFun"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgv_Permission.CellPainting += dgv_Permission_CellPainting;
        }

     
        private void dgv_Permission_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.RowIndex > 0)
            {
                DataGridViewRow row = dgv_Permission.Rows[e.RowIndex];
                string progName = string.Format("{0}", row.Cells["ProgID_En"].Value);
                if (e.ColumnIndex > 0 && this._programInfoDict.Keys.Contains(progName))
                {
                    Programs prog = this._programInfoDict[progName];

                    if (dgv_Permission.Columns[e.ColumnIndex].Name.Equals("DisplayFun"))
                        UnPaintCell(prog.DisplayFun, e);

                    else if (dgv_Permission.Columns[e.ColumnIndex].Name.Equals("SearchFun"))
                        UnPaintCell(prog.SearchFun, e);

                    else if (dgv_Permission.Columns[e.ColumnIndex].Name.Equals("InsertFun"))
                        UnPaintCell(prog.InsertFun, e);

                    else if (dgv_Permission.Columns[e.ColumnIndex].Name.Equals("UpdateFun"))
                        UnPaintCell(prog.UpdateFun, e);

                    else if (dgv_Permission.Columns[e.ColumnIndex].Name.Equals("DeleteFun"))
                        UnPaintCell(prog.DeleteFun, e);
                }
            }

        }

        /// <summary>
        /// 判斷是否取消繪製Cell
        /// </summary>
        /// <param name="paint"></param>
        /// <param name="e"></param>
        private void UnPaintCell(bool paint, DataGridViewCellPaintingEventArgs e)
        {
            if (!paint)
            {
                e.CellStyle.BackColor = Color.FromArgb(234, 232, 232);
                e.CellStyle.SelectionBackColor = Color.FromArgb(234, 232, 232);
                e.PaintBackground(e.ClipBounds, true);
                e.Handled = true;
            }else
            {
      
            }
        }

        #endregion

        #region Init Cmb

        private void InitGroupData()
        {
            string baseAddr = "http://localhost:48965/";
            List<Group> groupData = new List<Group>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseAddr);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                string uriStr = "api/Group?isOpen=" + 1;
                HttpResponseMessage response = client.GetAsync(uriStr).Result;
                if (response.IsSuccessStatusCode)
                {
                    groupData = response.Content.ReadAsAsync<List<Group>>().Result;
                }
            }

            cmbGroup.DisplayMember = "GroupName_Cht";
            cmbGroup.ValueMember = "GroupID";
            cmbGroup.DataSource = groupData;
            cmbGroup.SelectedIndex = 0;
            cmbGroup.SelectedIndexChanged += cmbGroup_SelectedIndexChanged;
            cmbGroup_SelectedIndexChanged(cmbGroup, new EventArgs());
        }

        private void cmbGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            getGroupProg(cmbGroup.SelectedValue.ToString());
        }

        #endregion

        #endregion

        #region Bind Event

        private void Form_GroupProg_Load(object sender, EventArgs e)
        {
            //cmbGroup.DisplayMember = "GroupName_Cht";
            //cmbGroup.ValueMember = "GroupID";
            //cmbGroup.DataSource = getGroupData();
            //cmbGroup.SelectedIndex = 0;

            //先把表單資訊載入記憶體
            this._programInfoDict = WebApiCaller.Json_GetModeByGet<List<Programs>>(@"http://localhost:48965/api/Prog/GetAll").ContentBody.ToDictionary(x => x.ProgID_En, y => y);

            InitDgv_Permission();
            InitGroupData();

        }

        private void button3_Click(object sender, EventArgs e)
        {

            string groupID = string.Format("{0}", cmbGroup.SelectedValue);
            List<GroupInfoSource> editSource = (List<GroupInfoSource>)dgv_Permission.DataSource;
            InsertGroupToProg(groupID, editSource);
        }

        private void button3_EnabledChanged(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            if (btn.Enabled)
                btn.FlatStyle = FlatStyle.Flat;
            else
                btn.FlatStyle = FlatStyle.Standard;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                getGroupData("1");
            }
            else
            {
                getGroupData("0");
            }
        }
        #endregion

        #region GetData By WebApi

        private void getGroupData(string isOpen)
        {
            string baseAddr = "http://localhost:48965/";
            List<Group> groupData = new List<Group>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseAddr);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                string uriStr = "api/Group?isOpen=" + isOpen;
                HttpResponseMessage response = client.GetAsync(uriStr).Result;
                if (response.IsSuccessStatusCode)
                {
                    groupData = response.Content.ReadAsAsync<List<Group>>().Result;
                }
            }
            //gridGroup.DataSource = groupData;
            cmbGroup.DisplayMember = "GroupName_Cht";
            cmbGroup.ValueMember = "GroupID";
            cmbGroup.DataSource = groupData;
            cmbGroup.SelectedIndex = 0;
        }

        private void getGroupProg(string id)
        {

            List<GroupInfoSource> groupProg = WebApiCaller.Json_GetModeByGet<List<GroupInfoSource>>(@"http://localhost:48965/api/ProgInfo?GroupID=" + id).ContentBody;

            if (groupProg == null)
                groupProg = new List<GroupInfoSource>();

            dgv_Permission.DataSource = groupProg;

        }

        #endregion

        #region Method

        private void InsertGroupToProg(string id, List<GroupInfoSource> list)
        {
            GroupToProgram[] transSource = new GroupToProgram[list.Count()];

            for (int index = 0; index < list.Count(); index++)
            {
                GroupInfoSource infoSource = list[index];
                GroupToProgram tSourceItem = new GroupToProgram()
                {
                    GroupID = id,
                    ProgID_En = infoSource.ProgID_En,
                    DisplayFun = infoSource.DisplayFun,
                    SearchFun = infoSource.SearchFun,
                    InsertFun = infoSource.InsertFun,
                    DeleteFun = infoSource.DeleteFun,
                    UpdateFun = infoSource.UpdateFun,
                    UpdateTime = DateTime.Now,
                    UpdateUserID = SHM.user[0].UserID,
                    InsertTime = DateTime.Now,
                    InsertUserID = SHM.user[0].UserID
                };

                transSource[index] = tSourceItem;
            }


            ApiCallResult<string> result = WebApiCaller.Srting_GetMessageByPut(@"http://localhost:48965/api/ProgInfo", transSource);

            if (result.IsSuccessStatusCode)
            {
                getGroupProg(cmbGroup.SelectedValue.ToString());
                MessageBox.Show("權限變更成功");
            }
            else
            {
                MessageBox.Show(result.ContentBody);
            }
        }

        #endregion

        #region Field

        private Dictionary<string, Programs> _programInfoDict;

        #endregion

   
    }

    #region GroupInfoSource Class

    public class GroupInfoSource
    {
        public string ProgID_En { get; set; }
        public string ProgID_Cht { get; set; }
        public bool DisplayFun { get; set; }
        public bool SearchFun { get; set; }
        public bool InsertFun { get; set; }
        public bool DeleteFun { get; set; }
        public bool UpdateFun { get; set; }
    }

    #endregion

}
