﻿namespace PRC
{
    partial class Form_ElecOverAllBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnl_Header = new System.Windows.Forms.Panel();
            this.btn_C16Station = new System.Windows.Forms.RadioButton();
            this.btnIndex = new System.Windows.Forms.RadioButton();
            this.pnl_Container = new System.Windows.Forms.Panel();
            this.pnl_Header.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnl_Header
            // 
            this.pnl_Header.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.pnl_Header.Controls.Add(this.btn_C16Station);
            this.pnl_Header.Controls.Add(this.btnIndex);
            this.pnl_Header.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnl_Header.Location = new System.Drawing.Point(0, 0);
            this.pnl_Header.Name = "pnl_Header";
            this.pnl_Header.Size = new System.Drawing.Size(1878, 97);
            this.pnl_Header.TabIndex = 0;
            // 
            // btn_C16Station
            // 
            this.btn_C16Station.Appearance = System.Windows.Forms.Appearance.Button;
            this.btn_C16Station.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.btn_C16Station.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_C16Station.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btn_C16Station.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_C16Station.Font = new System.Drawing.Font("新細明體", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btn_C16Station.Location = new System.Drawing.Point(97, 12);
            this.btn_C16Station.Name = "btn_C16Station";
            this.btn_C16Station.Size = new System.Drawing.Size(80, 30);
            this.btn_C16Station.TabIndex = 5;
            this.btn_C16Station.TabStop = true;
            this.btn_C16Station.Text = "整體圖02";
            this.btn_C16Station.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn_C16Station.UseVisualStyleBackColor = false;
            this.btn_C16Station.CheckedChanged += new System.EventHandler(this.btnCheckedChanged);
            // 
            // btnIndex
            // 
            this.btnIndex.Appearance = System.Windows.Forms.Appearance.Button;
            this.btnIndex.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.btnIndex.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnIndex.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnIndex.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIndex.Font = new System.Drawing.Font("新細明體", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnIndex.Location = new System.Drawing.Point(12, 12);
            this.btnIndex.Name = "btnIndex";
            this.btnIndex.Size = new System.Drawing.Size(80, 30);
            this.btnIndex.TabIndex = 4;
            this.btnIndex.TabStop = true;
            this.btnIndex.Text = "整體圖01";
            this.btnIndex.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnIndex.UseVisualStyleBackColor = false;
            this.btnIndex.CheckedChanged += new System.EventHandler(this.btnCheckedChanged);
            // 
            // pnl_Container
            // 
            this.pnl_Container.AutoScroll = true;
            this.pnl_Container.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnl_Container.Location = new System.Drawing.Point(0, 97);
            this.pnl_Container.Name = "pnl_Container";
            this.pnl_Container.Size = new System.Drawing.Size(1878, 764);
            this.pnl_Container.TabIndex = 1;
            // 
            // Form_ElecOverAllBase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1878, 861);
            this.Controls.Add(this.pnl_Container);
            this.Controls.Add(this.pnl_Header);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_ElecOverAllBase";
            this.Text = "Form_SignaBase";
            this.pnl_Header.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnl_Header;
        private System.Windows.Forms.Panel pnl_Container;
        private System.Windows.Forms.RadioButton btn_C16Station;
        private System.Windows.Forms.RadioButton btnIndex;
    }
}