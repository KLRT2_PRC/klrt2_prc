﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PRC.MyClass;

namespace PRC
{
    public partial class Form_Total1_backup : Form
    {
        MessageFormat.TSS7 _tss7;
        MessageFormat.C15 _c15;

        public Form_Total1_backup()
        {
            InitializeComponent();
        }

        private void Form_Total1_Load(object sender, EventArgs e)
        {
            // UpdateControls();
            MainFrame._udpserver.RecievedData += _udpserver_RecievedData;
        }

        private void _udpserver_RecievedData(object sender, RecieveDataEventArgs e)
        {
            UpdateControls();
            //throw new NotImplementedException();
        }

        public void UpdateControls()
        {
            if (InvokeRequired)
            {
                BeginInvoke(new Action(UpdateControls));
                return;
            }
            else
            {
               UpdateCTStatus();
            }
        }
        private void UpdateCTStatus()
        {
            retangleUC1.State = MainFrame._mtss7.MG01DIODETRIP == true ? iSCADA.Design.Utilities.Electrical.SymbolState.Error : iSCADA.Design.Utilities.Electrical.SymbolState.On;
            retangleUC1.Fill = true;// MainFrame._mtss7.MG01DIODETRIP;

            rhombusUC4.State=MainFrame._mtss7.MC81MCBTRIP==true? iSCADA.Design.Utilities.Electrical.SymbolState.Error : iSCADA.Design.Utilities.Electrical.SymbolState.On;
            rhombusUC4.Fill = true;// MainFrame._mtss7.MC81MCBTRIP;

            rhombusUC3.State = MainFrame._mtss7.MC21COMMUNICATIONFAULT == true ? iSCADA.Design.Utilities.Electrical.SymbolState.Error : iSCADA.Design.Utilities.Electrical.SymbolState.On;
            rhombusUC3.Fill = true;// MainFrame._mtss7.MC21COMMUNICATIONFAULT;

            rhombusUC2.State = MainFrame._mtss7.MC22COMMUNICATIONFAULT == true ? iSCADA.Design.Utilities.Electrical.SymbolState.Error : iSCADA.Design.Utilities.Electrical.SymbolState.On;
            rhombusUC2.Fill = true;// MainFrame._mtss7.MC22COMMUNICATIONFAULT;

            rhombusUC1.State = MainFrame._mtss7.MC41COMMUNICATIONFAULT == true ? iSCADA.Design.Utilities.Electrical.SymbolState.Error : iSCADA.Design.Utilities.Electrical.SymbolState.On;
            rhombusUC1.Fill = true;// MainFrame._mtss7.MC41COMMUNICATIONFAULT;

            //rhombusUC5.State = MainFrame._mc15.MC62MCBFault == true ? iSCADA.Design.Utilities.Electrical.SymbolState.Error : iSCADA.Design.Utilities.Electrical.SymbolState.On;
            //rhombusUC5.Fill = true;// MainFrame._mc15.MC62MCBFault;

            //rhombusUC6.State = MainFrame._mc15.MC63MCBFault == true ? iSCADA.Design.Utilities.Electrical.SymbolState.Error : iSCADA.Design.Utilities.Electrical.SymbolState.On;
            //rhombusUC6.Fill = true;// MainFrame._mc15.MC63MCBFault;

            //retangleUC5.State = MainFrame._mc15.MC62K1CONTACTOROpened == true ? iSCADA.Design.Utilities.Electrical.SymbolState.Error : iSCADA.Design.Utilities.Electrical.SymbolState.On;
            //retangleUC5.Fill = true;// MainFrame._mc15.MC62K1CONTACTOROpened;

            //retangleUC4.State = MainFrame._mc15.MC62K3CONTACTOROpened == true ? iSCADA.Design.Utilities.Electrical.SymbolState.Error : iSCADA.Design.Utilities.Electrical.SymbolState.On;
            //retangleUC4.Fill = true;// MainFrame._mc15.MC62K3CONTACTOROpened;

            //retangleUC3.State = MainFrame._mc15.MC63K2CONTACTOROpened == true ? iSCADA.Design.Utilities.Electrical.SymbolState.Error : iSCADA.Design.Utilities.Electrical.SymbolState.On;
            //retangleUC3.Fill = true;//  MainFrame._mc15.MC63K2CONTACTOROpened;

            //retangleUC2.State = MainFrame._mc15.MC63K4CONTACTOROpened == true ? iSCADA.Design.Utilities.Electrical.SymbolState.Error : iSCADA.Design.Utilities.Electrical.SymbolState.On;
            //retangleUC2.Fill = true;//  MainFrame._mc15.MC63K4CONTACTOROpened;
            //if (Form_ElecTSS7._tss7.MG01DIODETRIP == true)
            //{
            //    retangleUC1.State = iSCADA.Design.Utilities.Electrical.SymbolState.Error;
            //}
            //else
            //{
            //    retangleUC1.State = iSCADA.Design.Utilities.Electrical.SymbolState.On;
            //}

            //if (Form_ElecTSS7._tss7.MC21COMMUNICATIONFAULT == true)
            //{
            //    rhombusUC3.State = iSCADA.Design.Utilities.Electrical.SymbolState.Error;
            //}
            //else
            //{
            //    rhombusUC3.State = iSCADA.Design.Utilities.Electrical.SymbolState.On;
            //}

        }
    }
}
