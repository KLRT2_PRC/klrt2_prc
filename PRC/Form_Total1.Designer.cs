﻿namespace PRC
{
    partial class Form_Total1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.rhombusUC1 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.rhombusUC2 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.rhombusUC3 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC2 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.rhombusUC4 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC3 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC4 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC5 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC6 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC1 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.rhombusUC6 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC8 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC2 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC3 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC9 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC10 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC13 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC10 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC23 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC11 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC1 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.rhombusUC5 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC7 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC4 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC5 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC12 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC14 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC15 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.rhombusUC7 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC16 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC6 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC7 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC17 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC18 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC19 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.rhombusUC8 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC20 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC8 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC9 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC21 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC22 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC24 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.rhombusUC9 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC25 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC11 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC12 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC26 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC27 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC28 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.rhombusUC10 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC29 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC13 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC14 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC30 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC31 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC32 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.rhombusUC11 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC33 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC15 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC16 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC39 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC40 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC41 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.rhombusUC12 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC42 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC17 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC19 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC43 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC44 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC45 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.rhombusUC13 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC46 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC20 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC21 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC47 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC48 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC49 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.rhombusUC14 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC50 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC22 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC23 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC51 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC52 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC53 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.rhombusUC15 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC54 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC24 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC25 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC55 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC56 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC57 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.rhombusUC16 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC58 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC26 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC27 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC59 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC60 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC61 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.rhombusUC17 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.rhombusUC18 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.rhombusUC19 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC62 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.rhombusUC24 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC63 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC64 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC65 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC66 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC28 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label46 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.rhombusUC26 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC71 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC31 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC32 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC72 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC73 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC74 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.rhombusUC27 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC75 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC33 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC34 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC76 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC77 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC78 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.rhombusUC28 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC79 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC35 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC36 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC80 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC81 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC82 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.rhombusUC29 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC83 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC37 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC38 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC84 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC85 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC86 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.rhombusUC30 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC87 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC39 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC40 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC88 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC89 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC90 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.rhombusUC25 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.rhombusUC31 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.rhombusUC32 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC67 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.rhombusUC33 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC68 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC69 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC70 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC91 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC29 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label66 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.rhombusUC34 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC92 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC30 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC41 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC93 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC94 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC95 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label70 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.rhombusUC35 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC96 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC42 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC43 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC97 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC98 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC99 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label79 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.rhombusUC22 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.rhombusUC23 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.rhombusUC36 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC103 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.rhombusUC37 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC104 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC105 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC106 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC107 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC47 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label83 = new System.Windows.Forms.Label();
            this.label118 = new System.Windows.Forms.Label();
            this.label119 = new System.Windows.Forms.Label();
            this.label120 = new System.Windows.Forms.Label();
            this.label121 = new System.Windows.Forms.Label();
            this.rhombusUC49 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.rhombusUC50 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.rhombusUC51 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC152 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.rhombusUC52 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC153 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC154 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC155 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC156 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC70 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label122 = new System.Windows.Forms.Label();
            this.retangleUC73 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC161 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC162 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC163 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label71 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.rhombusUC20 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC34 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC18 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC44 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC35 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC36 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC37 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label74 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.rhombusUC21 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC38 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC45 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC46 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC100 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC101 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC102 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label84 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.rhombusUC38 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC108 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC48 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC49 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC109 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC110 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC111 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label87 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.rhombusUC39 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC112 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC50 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC51 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC113 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC114 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC115 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label90 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.rhombusUC40 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC116 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC52 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC53 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC117 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC118 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC119 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label93 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.rhombusUC41 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC120 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC54 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC55 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC121 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC122 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC123 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label96 = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.rhombusUC42 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC124 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC56 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC57 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC125 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC126 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC127 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label99 = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.rhombusUC43 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC128 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC58 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC59 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC129 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC130 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC131 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label102 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.rhombusUC44 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC132 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC60 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC61 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC133 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC134 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC135 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label105 = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.rhombusUC45 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC136 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC62 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC63 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC137 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC138 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC139 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label108 = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.label110 = new System.Windows.Forms.Label();
            this.rhombusUC46 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC140 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC64 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC65 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC141 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC142 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC143 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label111 = new System.Windows.Forms.Label();
            this.label112 = new System.Windows.Forms.Label();
            this.label113 = new System.Windows.Forms.Label();
            this.rhombusUC47 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC144 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC66 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC67 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC145 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC146 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC147 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label114 = new System.Windows.Forms.Label();
            this.label115 = new System.Windows.Forms.Label();
            this.label116 = new System.Windows.Forms.Label();
            this.rhombusUC48 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC148 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC68 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC69 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC149 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC150 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC151 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label117 = new System.Windows.Forms.Label();
            this.label123 = new System.Windows.Forms.Label();
            this.label124 = new System.Windows.Forms.Label();
            this.rhombusUC53 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC157 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC71 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC72 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC158 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC159 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC160 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label125 = new System.Windows.Forms.Label();
            this.label126 = new System.Windows.Forms.Label();
            this.label127 = new System.Windows.Forms.Label();
            this.rhombusUC54 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC164 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC74 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC75 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC165 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC166 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC167 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label128 = new System.Windows.Forms.Label();
            this.label129 = new System.Windows.Forms.Label();
            this.label130 = new System.Windows.Forms.Label();
            this.rhombusUC55 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC168 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC76 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC77 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC169 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC170 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC171 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label131 = new System.Windows.Forms.Label();
            this.label132 = new System.Windows.Forms.Label();
            this.label133 = new System.Windows.Forms.Label();
            this.rhombusUC56 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC172 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC78 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC79 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC173 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC174 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC175 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label134 = new System.Windows.Forms.Label();
            this.label135 = new System.Windows.Forms.Label();
            this.label136 = new System.Windows.Forms.Label();
            this.rhombusUC57 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC176 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC80 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC81 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC177 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC178 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC179 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label137 = new System.Windows.Forms.Label();
            this.label138 = new System.Windows.Forms.Label();
            this.label139 = new System.Windows.Forms.Label();
            this.rhombusUC58 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC180 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC82 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC83 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC181 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC182 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC183 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label197 = new System.Windows.Forms.Label();
            this.label198 = new System.Windows.Forms.Label();
            this.label199 = new System.Windows.Forms.Label();
            this.label200 = new System.Windows.Forms.Label();
            this.rhombusUC78 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.rhombusUC79 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.rhombusUC80 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC260 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.rhombusUC81 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC261 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC262 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC263 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC264 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC122 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label201 = new System.Windows.Forms.Label();
            this.label202 = new System.Windows.Forms.Label();
            this.label203 = new System.Windows.Forms.Label();
            this.label204 = new System.Windows.Forms.Label();
            this.label205 = new System.Windows.Forms.Label();
            this.rhombusUC82 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.rhombusUC83 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.rhombusUC84 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC265 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.rhombusUC85 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC266 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC267 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC268 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC269 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC123 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label206 = new System.Windows.Forms.Label();
            this.retangleUC124 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC270 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC271 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC272 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label140 = new System.Windows.Forms.Label();
            this.label141 = new System.Windows.Forms.Label();
            this.label142 = new System.Windows.Forms.Label();
            this.rhombusUC59 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC184 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC84 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC85 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC185 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC186 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC187 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label143 = new System.Windows.Forms.Label();
            this.label144 = new System.Windows.Forms.Label();
            this.label145 = new System.Windows.Forms.Label();
            this.rhombusUC60 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC188 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC86 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC87 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC189 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC190 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC191 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label146 = new System.Windows.Forms.Label();
            this.label147 = new System.Windows.Forms.Label();
            this.label148 = new System.Windows.Forms.Label();
            this.rhombusUC61 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC192 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC88 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC89 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC193 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC194 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC195 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label149 = new System.Windows.Forms.Label();
            this.label150 = new System.Windows.Forms.Label();
            this.label151 = new System.Windows.Forms.Label();
            this.rhombusUC62 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC196 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC90 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC91 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC197 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC198 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC199 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label152 = new System.Windows.Forms.Label();
            this.label153 = new System.Windows.Forms.Label();
            this.label154 = new System.Windows.Forms.Label();
            this.rhombusUC63 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC200 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC92 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC93 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC201 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC202 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC203 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label155 = new System.Windows.Forms.Label();
            this.label156 = new System.Windows.Forms.Label();
            this.label157 = new System.Windows.Forms.Label();
            this.rhombusUC64 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC204 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC94 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC95 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC205 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC206 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC207 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label158 = new System.Windows.Forms.Label();
            this.label159 = new System.Windows.Forms.Label();
            this.label160 = new System.Windows.Forms.Label();
            this.rhombusUC65 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC208 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC96 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC97 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC209 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC210 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC211 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label161 = new System.Windows.Forms.Label();
            this.label162 = new System.Windows.Forms.Label();
            this.label163 = new System.Windows.Forms.Label();
            this.rhombusUC66 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC212 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC98 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC99 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC213 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC214 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC215 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label164 = new System.Windows.Forms.Label();
            this.label165 = new System.Windows.Forms.Label();
            this.label166 = new System.Windows.Forms.Label();
            this.rhombusUC67 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC216 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC100 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC101 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC217 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC218 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC219 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label167 = new System.Windows.Forms.Label();
            this.label168 = new System.Windows.Forms.Label();
            this.label169 = new System.Windows.Forms.Label();
            this.rhombusUC68 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC220 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC102 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC103 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC221 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC222 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC223 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label170 = new System.Windows.Forms.Label();
            this.label171 = new System.Windows.Forms.Label();
            this.label172 = new System.Windows.Forms.Label();
            this.rhombusUC69 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC224 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC104 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC105 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC225 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC226 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC227 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label173 = new System.Windows.Forms.Label();
            this.label174 = new System.Windows.Forms.Label();
            this.label175 = new System.Windows.Forms.Label();
            this.rhombusUC70 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC228 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC106 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC107 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC229 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC230 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC231 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label176 = new System.Windows.Forms.Label();
            this.label177 = new System.Windows.Forms.Label();
            this.label178 = new System.Windows.Forms.Label();
            this.rhombusUC71 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC232 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC108 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC109 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC233 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC234 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC235 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label179 = new System.Windows.Forms.Label();
            this.label180 = new System.Windows.Forms.Label();
            this.label181 = new System.Windows.Forms.Label();
            this.rhombusUC72 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC236 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC110 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC111 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC237 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC238 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC239 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label182 = new System.Windows.Forms.Label();
            this.label183 = new System.Windows.Forms.Label();
            this.label184 = new System.Windows.Forms.Label();
            this.rhombusUC73 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC240 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC112 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC113 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC241 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC242 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC243 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label185 = new System.Windows.Forms.Label();
            this.label186 = new System.Windows.Forms.Label();
            this.label187 = new System.Windows.Forms.Label();
            this.rhombusUC74 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC244 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC114 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC115 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC245 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC246 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC247 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label188 = new System.Windows.Forms.Label();
            this.label189 = new System.Windows.Forms.Label();
            this.label190 = new System.Windows.Forms.Label();
            this.rhombusUC75 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC248 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC116 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC117 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.lineUC249 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC250 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC251 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.retangleUC118 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC119 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC120 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC121 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC125 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC126 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC127 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC128 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC129 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC130 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC131 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC132 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC133 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC134 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC135 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC136 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC137 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC138 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC139 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC140 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC141 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC142 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC143 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC144 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC145 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC146 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC147 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC148 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC149 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC150 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC151 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC152 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC153 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC154 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC155 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC156 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC157 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC158 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC159 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC160 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC161 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC162 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC163 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC164 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC165 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC166 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC167 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC168 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC169 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC170 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC171 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.retangleUC172 = new iSCADA.Design.Utilities.Electrical.RetangleUC();
            this.label191 = new System.Windows.Forms.Label();
            this.label192 = new System.Windows.Forms.Label();
            this.label193 = new System.Windows.Forms.Label();
            this.lineUC252 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC253 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC254 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC255 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC256 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC257 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.lineUC258 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label194 = new System.Windows.Forms.Label();
            this.label195 = new System.Windows.Forms.Label();
            this.label196 = new System.Windows.Forms.Label();
            this.label207 = new System.Windows.Forms.Label();
            this.label208 = new System.Windows.Forms.Label();
            this.label209 = new System.Windows.Forms.Label();
            this.label210 = new System.Windows.Forms.Label();
            this.label211 = new System.Windows.Forms.Label();
            this.label212 = new System.Windows.Forms.Label();
            this.label213 = new System.Windows.Forms.Label();
            this.label214 = new System.Windows.Forms.Label();
            this.label215 = new System.Windows.Forms.Label();
            this.label216 = new System.Windows.Forms.Label();
            this.label217 = new System.Windows.Forms.Label();
            this.label218 = new System.Windows.Forms.Label();
            this.label219 = new System.Windows.Forms.Label();
            this.label220 = new System.Windows.Forms.Label();
            this.label221 = new System.Windows.Forms.Label();
            this.label222 = new System.Windows.Forms.Label();
            this.label223 = new System.Windows.Forms.Label();
            this.label224 = new System.Windows.Forms.Label();
            this.label225 = new System.Windows.Forms.Label();
            this.label226 = new System.Windows.Forms.Label();
            this.label227 = new System.Windows.Forms.Label();
            this.label228 = new System.Windows.Forms.Label();
            this.label229 = new System.Windows.Forms.Label();
            this.label230 = new System.Windows.Forms.Label();
            this.label231 = new System.Windows.Forms.Label();
            this.label232 = new System.Windows.Forms.Label();
            this.label233 = new System.Windows.Forms.Label();
            this.label234 = new System.Windows.Forms.Label();
            this.label235 = new System.Windows.Forms.Label();
            this.label236 = new System.Windows.Forms.Label();
            this.label237 = new System.Windows.Forms.Label();
            this.rhombusUC76 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.label238 = new System.Windows.Forms.Label();
            this.rhombusUC77 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC273 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label239 = new System.Windows.Forms.Label();
            this.rhombusUC86 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.label240 = new System.Windows.Forms.Label();
            this.rhombusUC87 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.label241 = new System.Windows.Forms.Label();
            this.rhombusUC88 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC276 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label242 = new System.Windows.Forms.Label();
            this.rhombusUC89 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.label243 = new System.Windows.Forms.Label();
            this.rhombusUC90 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.label244 = new System.Windows.Forms.Label();
            this.rhombusUC91 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.label245 = new System.Windows.Forms.Label();
            this.rhombusUC92 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.lineUC280 = new iSCADA.Design.Utilities.Electrical.LineUC();
            this.label246 = new System.Windows.Forms.Label();
            this.rhombusUC93 = new iSCADA.Design.Utilities.Electrical.RhombusUC();
            this.label247 = new System.Windows.Forms.Label();
            this.label248 = new System.Windows.Forms.Label();
            this.label249 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label21.Location = new System.Drawing.Point(174, 159);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(23, 15);
            this.label21.TabIndex = 2583;
            this.label21.Text = "Q2";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label22.Location = new System.Drawing.Point(200, 208);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(21, 15);
            this.label22.TabIndex = 2582;
            this.label22.Text = "K4";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(598, 120);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 15);
            this.label1.TabIndex = 2598;
            this.label1.Text = "MC41";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(657, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 15);
            this.label2.TabIndex = 2597;
            this.label2.Text = "MC22";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label3.Location = new System.Drawing.Point(541, 83);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 15);
            this.label3.TabIndex = 2596;
            this.label3.Text = "MC21";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label4.Location = new System.Drawing.Point(623, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 15);
            this.label4.TabIndex = 2595;
            this.label4.Text = "MG01";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label5.Location = new System.Drawing.Point(625, 44);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 15);
            this.label5.TabIndex = 2584;
            this.label5.Text = "MC81";
            // 
            // rhombusUC1
            // 
            this.rhombusUC1.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC1.ExtenderWidth = 2;
            this.rhombusUC1.Fill = false;
            this.rhombusUC1.GroupID = null;
            this.rhombusUC1.Location = new System.Drawing.Point(607, 101);
            this.rhombusUC1.Name = "rhombusUC1";
            this.rhombusUC1.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC1.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC1.TabIndex = 2594;
            // 
            // rhombusUC2
            // 
            this.rhombusUC2.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC2.ExtenderWidth = 2;
            this.rhombusUC2.Fill = false;
            this.rhombusUC2.GroupID = null;
            this.rhombusUC2.Location = new System.Drawing.Point(639, 82);
            this.rhombusUC2.Name = "rhombusUC2";
            this.rhombusUC2.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC2.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC2.TabIndex = 2593;
            // 
            // rhombusUC3
            // 
            this.rhombusUC3.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC3.ExtenderWidth = 2;
            this.rhombusUC3.Fill = false;
            this.rhombusUC3.GroupID = null;
            this.rhombusUC3.Location = new System.Drawing.Point(580, 82);
            this.rhombusUC3.Name = "rhombusUC3";
            this.rhombusUC3.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC3.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC3.TabIndex = 2592;
            // 
            // lineUC2
            // 
            this.lineUC2.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC2.ExtenderWidth = 3;
            this.lineUC2.Fill = false;
            this.lineUC2.GroupID = null;
            this.lineUC2.Location = new System.Drawing.Point(587, 110);
            this.lineUC2.Name = "lineUC2";
            this.lineUC2.Size = new System.Drawing.Size(60, 3);
            this.lineUC2.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC2.TabIndex = 2591;
            // 
            // rhombusUC4
            // 
            this.rhombusUC4.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC4.ExtenderWidth = 2;
            this.rhombusUC4.Fill = false;
            this.rhombusUC4.GroupID = null;
            this.rhombusUC4.Location = new System.Drawing.Point(607, 43);
            this.rhombusUC4.Name = "rhombusUC4";
            this.rhombusUC4.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC4.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC4.TabIndex = 2586;
            // 
            // lineUC3
            // 
            this.lineUC3.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC3.ExtenderWidth = 3;
            this.lineUC3.Fill = false;
            this.lineUC3.GroupID = null;
            this.lineUC3.Location = new System.Drawing.Point(614, 33);
            this.lineUC3.Name = "lineUC3";
            this.lineUC3.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC3.Size = new System.Drawing.Size(3, 40);
            this.lineUC3.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC3.TabIndex = 2590;
            // 
            // lineUC4
            // 
            this.lineUC4.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC4.ExtenderWidth = 3;
            this.lineUC4.Fill = false;
            this.lineUC4.GroupID = null;
            this.lineUC4.Location = new System.Drawing.Point(645, 70);
            this.lineUC4.Name = "lineUC4";
            this.lineUC4.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC4.Size = new System.Drawing.Size(3, 70);
            this.lineUC4.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC4.TabIndex = 2589;
            // 
            // lineUC5
            // 
            this.lineUC5.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC5.ExtenderWidth = 3;
            this.lineUC5.Fill = false;
            this.lineUC5.GroupID = null;
            this.lineUC5.Location = new System.Drawing.Point(587, 70);
            this.lineUC5.Name = "lineUC5";
            this.lineUC5.Size = new System.Drawing.Size(60, 3);
            this.lineUC5.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC5.TabIndex = 2588;
            // 
            // lineUC6
            // 
            this.lineUC6.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC6.ExtenderWidth = 3;
            this.lineUC6.Fill = false;
            this.lineUC6.GroupID = null;
            this.lineUC6.Location = new System.Drawing.Point(587, 70);
            this.lineUC6.Name = "lineUC6";
            this.lineUC6.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC6.Size = new System.Drawing.Size(3, 70);
            this.lineUC6.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC6.TabIndex = 2587;
            // 
            // retangleUC1
            // 
            this.retangleUC1.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC1.ExtenderWidth = 3;
            this.retangleUC1.Fill = false;
            this.retangleUC1.GroupID = null;
            this.retangleUC1.Location = new System.Drawing.Point(608, 18);
            this.retangleUC1.Name = "retangleUC1";
            this.retangleUC1.Size = new System.Drawing.Size(15, 15);
            this.retangleUC1.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC1.TabIndex = 2585;
            // 
            // rhombusUC6
            // 
            this.rhombusUC6.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC6.ExtenderWidth = 2;
            this.rhombusUC6.Fill = false;
            this.rhombusUC6.GroupID = null;
            this.rhombusUC6.Location = new System.Drawing.Point(196, 159);
            this.rhombusUC6.Name = "rhombusUC6";
            this.rhombusUC6.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC6.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC6.TabIndex = 2575;
            // 
            // lineUC8
            // 
            this.lineUC8.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC8.ExtenderWidth = 3;
            this.lineUC8.Fill = false;
            this.lineUC8.GroupID = null;
            this.lineUC8.Location = new System.Drawing.Point(203, 139);
            this.lineUC8.Name = "lineUC8";
            this.lineUC8.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC8.Size = new System.Drawing.Size(3, 50);
            this.lineUC8.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC8.TabIndex = 2581;
            // 
            // retangleUC2
            // 
            this.retangleUC2.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC2.ExtenderWidth = 3;
            this.retangleUC2.Fill = false;
            this.retangleUC2.GroupID = null;
            this.retangleUC2.Location = new System.Drawing.Point(223, 205);
            this.retangleUC2.Name = "retangleUC2";
            this.retangleUC2.Size = new System.Drawing.Size(15, 15);
            this.retangleUC2.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC2.TabIndex = 2580;
            // 
            // retangleUC3
            // 
            this.retangleUC3.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC3.ExtenderWidth = 3;
            this.retangleUC3.Fill = false;
            this.retangleUC3.GroupID = null;
            this.retangleUC3.Location = new System.Drawing.Point(173, 205);
            this.retangleUC3.Name = "retangleUC3";
            this.retangleUC3.Size = new System.Drawing.Size(15, 15);
            this.retangleUC3.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC3.TabIndex = 2579;
            // 
            // lineUC9
            // 
            this.lineUC9.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC9.ExtenderWidth = 3;
            this.lineUC9.Fill = false;
            this.lineUC9.GroupID = null;
            this.lineUC9.Location = new System.Drawing.Point(229, 189);
            this.lineUC9.Name = "lineUC9";
            this.lineUC9.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC9.Size = new System.Drawing.Size(3, 60);
            this.lineUC9.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC9.TabIndex = 2578;
            // 
            // lineUC10
            // 
            this.lineUC10.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC10.ExtenderWidth = 3;
            this.lineUC10.Fill = false;
            this.lineUC10.GroupID = null;
            this.lineUC10.Location = new System.Drawing.Point(179, 189);
            this.lineUC10.Name = "lineUC10";
            this.lineUC10.Size = new System.Drawing.Size(50, 3);
            this.lineUC10.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC10.TabIndex = 2577;
            // 
            // lineUC13
            // 
            this.lineUC13.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC13.ExtenderWidth = 3;
            this.lineUC13.Fill = false;
            this.lineUC13.GroupID = null;
            this.lineUC13.Location = new System.Drawing.Point(179, 189);
            this.lineUC13.Name = "lineUC13";
            this.lineUC13.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC13.Size = new System.Drawing.Size(3, 60);
            this.lineUC13.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC13.TabIndex = 2576;
            // 
            // retangleUC10
            // 
            this.retangleUC10.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC10.ExtenderWidth = 3;
            this.retangleUC10.Fill = false;
            this.retangleUC10.GroupID = null;
            this.retangleUC10.Location = new System.Drawing.Point(86, 285);
            this.retangleUC10.Name = "retangleUC10";
            this.retangleUC10.Size = new System.Drawing.Size(65, 16);
            this.retangleUC10.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC10.TabIndex = 2574;
            // 
            // lineUC23
            // 
            this.lineUC23.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC23.ExtenderWidth = 3;
            this.lineUC23.Fill = false;
            this.lineUC23.GroupID = null;
            this.lineUC23.Location = new System.Drawing.Point(0, 298);
            this.lineUC23.Name = "lineUC23";
            this.lineUC23.Size = new System.Drawing.Size(1900, 3);
            this.lineUC23.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC23.TabIndex = 2573;
            // 
            // lineUC11
            // 
            this.lineUC11.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC11.ExtenderWidth = 3;
            this.lineUC11.Fill = false;
            this.lineUC11.GroupID = null;
            this.lineUC11.Location = new System.Drawing.Point(1, 262);
            this.lineUC11.Name = "lineUC11";
            this.lineUC11.Size = new System.Drawing.Size(1900, 3);
            this.lineUC11.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC11.TabIndex = 2572;
            // 
            // lineUC1
            // 
            this.lineUC1.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC1.ExtenderWidth = 3;
            this.lineUC1.Fill = false;
            this.lineUC1.GroupID = null;
            this.lineUC1.Location = new System.Drawing.Point(0, 137);
            this.lineUC1.Name = "lineUC1";
            this.lineUC1.Size = new System.Drawing.Size(590, 3);
            this.lineUC1.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC1.TabIndex = 2571;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label6.Location = new System.Drawing.Point(152, 205);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(21, 15);
            this.label6.TabIndex = 2599;
            this.label6.Text = "K2";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label7.Location = new System.Drawing.Point(65, 205);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(21, 15);
            this.label7.TabIndex = 2608;
            this.label7.Text = "K1";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label8.Location = new System.Drawing.Point(113, 208);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(21, 15);
            this.label8.TabIndex = 2607;
            this.label8.Text = "K3";
            // 
            // rhombusUC5
            // 
            this.rhombusUC5.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC5.ExtenderWidth = 2;
            this.rhombusUC5.Fill = false;
            this.rhombusUC5.GroupID = null;
            this.rhombusUC5.Location = new System.Drawing.Point(109, 159);
            this.rhombusUC5.Name = "rhombusUC5";
            this.rhombusUC5.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC5.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC5.TabIndex = 2600;
            // 
            // lineUC7
            // 
            this.lineUC7.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC7.ExtenderWidth = 3;
            this.lineUC7.Fill = false;
            this.lineUC7.GroupID = null;
            this.lineUC7.Location = new System.Drawing.Point(116, 139);
            this.lineUC7.Name = "lineUC7";
            this.lineUC7.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC7.Size = new System.Drawing.Size(3, 50);
            this.lineUC7.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC7.TabIndex = 2606;
            // 
            // retangleUC4
            // 
            this.retangleUC4.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC4.ExtenderWidth = 3;
            this.retangleUC4.Fill = false;
            this.retangleUC4.GroupID = null;
            this.retangleUC4.Location = new System.Drawing.Point(136, 205);
            this.retangleUC4.Name = "retangleUC4";
            this.retangleUC4.Size = new System.Drawing.Size(15, 15);
            this.retangleUC4.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC4.TabIndex = 2605;
            // 
            // retangleUC5
            // 
            this.retangleUC5.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC5.ExtenderWidth = 3;
            this.retangleUC5.Fill = false;
            this.retangleUC5.GroupID = null;
            this.retangleUC5.Location = new System.Drawing.Point(86, 205);
            this.retangleUC5.Name = "retangleUC5";
            this.retangleUC5.Size = new System.Drawing.Size(15, 15);
            this.retangleUC5.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC5.TabIndex = 2604;
            // 
            // lineUC12
            // 
            this.lineUC12.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC12.ExtenderWidth = 3;
            this.lineUC12.Fill = false;
            this.lineUC12.GroupID = null;
            this.lineUC12.Location = new System.Drawing.Point(142, 189);
            this.lineUC12.Name = "lineUC12";
            this.lineUC12.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC12.Size = new System.Drawing.Size(3, 96);
            this.lineUC12.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC12.TabIndex = 2603;
            // 
            // lineUC14
            // 
            this.lineUC14.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC14.ExtenderWidth = 3;
            this.lineUC14.Fill = false;
            this.lineUC14.GroupID = null;
            this.lineUC14.Location = new System.Drawing.Point(92, 189);
            this.lineUC14.Name = "lineUC14";
            this.lineUC14.Size = new System.Drawing.Size(50, 3);
            this.lineUC14.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC14.TabIndex = 2602;
            // 
            // lineUC15
            // 
            this.lineUC15.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC15.ExtenderWidth = 3;
            this.lineUC15.Fill = false;
            this.lineUC15.GroupID = null;
            this.lineUC15.Location = new System.Drawing.Point(92, 189);
            this.lineUC15.Name = "lineUC15";
            this.lineUC15.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC15.Size = new System.Drawing.Size(3, 96);
            this.lineUC15.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC15.TabIndex = 2601;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label9.Location = new System.Drawing.Point(87, 159);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(23, 15);
            this.label9.TabIndex = 2609;
            this.label9.Text = "Q1";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label10.Location = new System.Drawing.Point(263, 159);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(23, 15);
            this.label10.TabIndex = 2629;
            this.label10.Text = "Q1";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label11.Location = new System.Drawing.Point(241, 205);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(21, 15);
            this.label11.TabIndex = 2628;
            this.label11.Text = "K1";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label12.Location = new System.Drawing.Point(289, 208);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(21, 15);
            this.label12.TabIndex = 2627;
            this.label12.Text = "K3";
            // 
            // rhombusUC7
            // 
            this.rhombusUC7.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC7.ExtenderWidth = 2;
            this.rhombusUC7.Fill = false;
            this.rhombusUC7.GroupID = null;
            this.rhombusUC7.Location = new System.Drawing.Point(285, 159);
            this.rhombusUC7.Name = "rhombusUC7";
            this.rhombusUC7.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC7.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC7.TabIndex = 2620;
            // 
            // lineUC16
            // 
            this.lineUC16.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC16.ExtenderWidth = 3;
            this.lineUC16.Fill = false;
            this.lineUC16.GroupID = null;
            this.lineUC16.Location = new System.Drawing.Point(292, 139);
            this.lineUC16.Name = "lineUC16";
            this.lineUC16.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC16.Size = new System.Drawing.Size(3, 50);
            this.lineUC16.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC16.TabIndex = 2626;
            // 
            // retangleUC6
            // 
            this.retangleUC6.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC6.ExtenderWidth = 3;
            this.retangleUC6.Fill = false;
            this.retangleUC6.GroupID = null;
            this.retangleUC6.Location = new System.Drawing.Point(312, 205);
            this.retangleUC6.Name = "retangleUC6";
            this.retangleUC6.Size = new System.Drawing.Size(15, 15);
            this.retangleUC6.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC6.TabIndex = 2625;
            // 
            // retangleUC7
            // 
            this.retangleUC7.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC7.ExtenderWidth = 3;
            this.retangleUC7.Fill = false;
            this.retangleUC7.GroupID = null;
            this.retangleUC7.Location = new System.Drawing.Point(262, 205);
            this.retangleUC7.Name = "retangleUC7";
            this.retangleUC7.Size = new System.Drawing.Size(15, 15);
            this.retangleUC7.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC7.TabIndex = 2624;
            // 
            // lineUC17
            // 
            this.lineUC17.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC17.ExtenderWidth = 3;
            this.lineUC17.Fill = false;
            this.lineUC17.GroupID = null;
            this.lineUC17.Location = new System.Drawing.Point(318, 189);
            this.lineUC17.Name = "lineUC17";
            this.lineUC17.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC17.Size = new System.Drawing.Size(3, 96);
            this.lineUC17.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC17.TabIndex = 2623;
            // 
            // lineUC18
            // 
            this.lineUC18.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC18.ExtenderWidth = 3;
            this.lineUC18.Fill = false;
            this.lineUC18.GroupID = null;
            this.lineUC18.Location = new System.Drawing.Point(268, 189);
            this.lineUC18.Name = "lineUC18";
            this.lineUC18.Size = new System.Drawing.Size(50, 3);
            this.lineUC18.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC18.TabIndex = 2622;
            // 
            // lineUC19
            // 
            this.lineUC19.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC19.ExtenderWidth = 3;
            this.lineUC19.Fill = false;
            this.lineUC19.GroupID = null;
            this.lineUC19.Location = new System.Drawing.Point(268, 189);
            this.lineUC19.Name = "lineUC19";
            this.lineUC19.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC19.Size = new System.Drawing.Size(3, 96);
            this.lineUC19.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC19.TabIndex = 2621;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label13.Location = new System.Drawing.Point(328, 205);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(21, 15);
            this.label13.TabIndex = 2619;
            this.label13.Text = "K2";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label14.Location = new System.Drawing.Point(350, 159);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(23, 15);
            this.label14.TabIndex = 2618;
            this.label14.Text = "Q2";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label15.Location = new System.Drawing.Point(376, 208);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(21, 15);
            this.label15.TabIndex = 2617;
            this.label15.Text = "K4";
            // 
            // rhombusUC8
            // 
            this.rhombusUC8.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC8.ExtenderWidth = 2;
            this.rhombusUC8.Fill = false;
            this.rhombusUC8.GroupID = null;
            this.rhombusUC8.Location = new System.Drawing.Point(372, 159);
            this.rhombusUC8.Name = "rhombusUC8";
            this.rhombusUC8.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC8.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC8.TabIndex = 2610;
            // 
            // lineUC20
            // 
            this.lineUC20.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC20.ExtenderWidth = 3;
            this.lineUC20.Fill = false;
            this.lineUC20.GroupID = null;
            this.lineUC20.Location = new System.Drawing.Point(379, 139);
            this.lineUC20.Name = "lineUC20";
            this.lineUC20.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC20.Size = new System.Drawing.Size(3, 50);
            this.lineUC20.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC20.TabIndex = 2616;
            // 
            // retangleUC8
            // 
            this.retangleUC8.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC8.ExtenderWidth = 3;
            this.retangleUC8.Fill = false;
            this.retangleUC8.GroupID = null;
            this.retangleUC8.Location = new System.Drawing.Point(399, 205);
            this.retangleUC8.Name = "retangleUC8";
            this.retangleUC8.Size = new System.Drawing.Size(15, 15);
            this.retangleUC8.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC8.TabIndex = 2615;
            // 
            // retangleUC9
            // 
            this.retangleUC9.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC9.ExtenderWidth = 3;
            this.retangleUC9.Fill = false;
            this.retangleUC9.GroupID = null;
            this.retangleUC9.Location = new System.Drawing.Point(349, 205);
            this.retangleUC9.Name = "retangleUC9";
            this.retangleUC9.Size = new System.Drawing.Size(15, 15);
            this.retangleUC9.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC9.TabIndex = 2614;
            // 
            // lineUC21
            // 
            this.lineUC21.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC21.ExtenderWidth = 3;
            this.lineUC21.Fill = false;
            this.lineUC21.GroupID = null;
            this.lineUC21.Location = new System.Drawing.Point(405, 189);
            this.lineUC21.Name = "lineUC21";
            this.lineUC21.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC21.Size = new System.Drawing.Size(3, 60);
            this.lineUC21.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC21.TabIndex = 2613;
            // 
            // lineUC22
            // 
            this.lineUC22.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC22.ExtenderWidth = 3;
            this.lineUC22.Fill = false;
            this.lineUC22.GroupID = null;
            this.lineUC22.Location = new System.Drawing.Point(355, 189);
            this.lineUC22.Name = "lineUC22";
            this.lineUC22.Size = new System.Drawing.Size(50, 3);
            this.lineUC22.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC22.TabIndex = 2612;
            // 
            // lineUC24
            // 
            this.lineUC24.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC24.ExtenderWidth = 3;
            this.lineUC24.Fill = false;
            this.lineUC24.GroupID = null;
            this.lineUC24.Location = new System.Drawing.Point(355, 189);
            this.lineUC24.Name = "lineUC24";
            this.lineUC24.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC24.Size = new System.Drawing.Size(3, 60);
            this.lineUC24.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC24.TabIndex = 2611;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label16.Location = new System.Drawing.Point(439, 159);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(23, 15);
            this.label16.TabIndex = 2649;
            this.label16.Text = "Q1";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label17.Location = new System.Drawing.Point(417, 205);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(21, 15);
            this.label17.TabIndex = 2648;
            this.label17.Text = "K1";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label18.Location = new System.Drawing.Point(465, 208);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(21, 15);
            this.label18.TabIndex = 2647;
            this.label18.Text = "K3";
            // 
            // rhombusUC9
            // 
            this.rhombusUC9.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC9.ExtenderWidth = 2;
            this.rhombusUC9.Fill = false;
            this.rhombusUC9.GroupID = null;
            this.rhombusUC9.Location = new System.Drawing.Point(461, 159);
            this.rhombusUC9.Name = "rhombusUC9";
            this.rhombusUC9.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC9.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC9.TabIndex = 2640;
            // 
            // lineUC25
            // 
            this.lineUC25.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC25.ExtenderWidth = 3;
            this.lineUC25.Fill = false;
            this.lineUC25.GroupID = null;
            this.lineUC25.Location = new System.Drawing.Point(468, 139);
            this.lineUC25.Name = "lineUC25";
            this.lineUC25.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC25.Size = new System.Drawing.Size(3, 50);
            this.lineUC25.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC25.TabIndex = 2646;
            // 
            // retangleUC11
            // 
            this.retangleUC11.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC11.ExtenderWidth = 3;
            this.retangleUC11.Fill = false;
            this.retangleUC11.GroupID = null;
            this.retangleUC11.Location = new System.Drawing.Point(488, 205);
            this.retangleUC11.Name = "retangleUC11";
            this.retangleUC11.Size = new System.Drawing.Size(15, 15);
            this.retangleUC11.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC11.TabIndex = 2645;
            // 
            // retangleUC12
            // 
            this.retangleUC12.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC12.ExtenderWidth = 3;
            this.retangleUC12.Fill = false;
            this.retangleUC12.GroupID = null;
            this.retangleUC12.Location = new System.Drawing.Point(438, 205);
            this.retangleUC12.Name = "retangleUC12";
            this.retangleUC12.Size = new System.Drawing.Size(15, 15);
            this.retangleUC12.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC12.TabIndex = 2644;
            // 
            // lineUC26
            // 
            this.lineUC26.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC26.ExtenderWidth = 3;
            this.lineUC26.Fill = false;
            this.lineUC26.GroupID = null;
            this.lineUC26.Location = new System.Drawing.Point(494, 189);
            this.lineUC26.Name = "lineUC26";
            this.lineUC26.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC26.Size = new System.Drawing.Size(3, 96);
            this.lineUC26.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC26.TabIndex = 2643;
            // 
            // lineUC27
            // 
            this.lineUC27.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC27.ExtenderWidth = 3;
            this.lineUC27.Fill = false;
            this.lineUC27.GroupID = null;
            this.lineUC27.Location = new System.Drawing.Point(444, 189);
            this.lineUC27.Name = "lineUC27";
            this.lineUC27.Size = new System.Drawing.Size(50, 3);
            this.lineUC27.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC27.TabIndex = 2642;
            // 
            // lineUC28
            // 
            this.lineUC28.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC28.ExtenderWidth = 3;
            this.lineUC28.Fill = false;
            this.lineUC28.GroupID = null;
            this.lineUC28.Location = new System.Drawing.Point(444, 189);
            this.lineUC28.Name = "lineUC28";
            this.lineUC28.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC28.Size = new System.Drawing.Size(3, 96);
            this.lineUC28.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC28.TabIndex = 2641;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label19.Location = new System.Drawing.Point(504, 205);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(21, 15);
            this.label19.TabIndex = 2639;
            this.label19.Text = "K2";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label20.Location = new System.Drawing.Point(526, 159);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(23, 15);
            this.label20.TabIndex = 2638;
            this.label20.Text = "Q2";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label23.Location = new System.Drawing.Point(552, 208);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(21, 15);
            this.label23.TabIndex = 2637;
            this.label23.Text = "K4";
            // 
            // rhombusUC10
            // 
            this.rhombusUC10.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC10.ExtenderWidth = 2;
            this.rhombusUC10.Fill = false;
            this.rhombusUC10.GroupID = null;
            this.rhombusUC10.Location = new System.Drawing.Point(548, 159);
            this.rhombusUC10.Name = "rhombusUC10";
            this.rhombusUC10.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC10.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC10.TabIndex = 2630;
            // 
            // lineUC29
            // 
            this.lineUC29.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC29.ExtenderWidth = 3;
            this.lineUC29.Fill = false;
            this.lineUC29.GroupID = null;
            this.lineUC29.Location = new System.Drawing.Point(555, 139);
            this.lineUC29.Name = "lineUC29";
            this.lineUC29.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC29.Size = new System.Drawing.Size(3, 50);
            this.lineUC29.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC29.TabIndex = 2636;
            // 
            // retangleUC13
            // 
            this.retangleUC13.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC13.ExtenderWidth = 3;
            this.retangleUC13.Fill = false;
            this.retangleUC13.GroupID = null;
            this.retangleUC13.Location = new System.Drawing.Point(575, 205);
            this.retangleUC13.Name = "retangleUC13";
            this.retangleUC13.Size = new System.Drawing.Size(15, 15);
            this.retangleUC13.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC13.TabIndex = 2635;
            // 
            // retangleUC14
            // 
            this.retangleUC14.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC14.ExtenderWidth = 3;
            this.retangleUC14.Fill = false;
            this.retangleUC14.GroupID = null;
            this.retangleUC14.Location = new System.Drawing.Point(525, 205);
            this.retangleUC14.Name = "retangleUC14";
            this.retangleUC14.Size = new System.Drawing.Size(15, 15);
            this.retangleUC14.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC14.TabIndex = 2634;
            // 
            // lineUC30
            // 
            this.lineUC30.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC30.ExtenderWidth = 3;
            this.lineUC30.Fill = false;
            this.lineUC30.GroupID = null;
            this.lineUC30.Location = new System.Drawing.Point(581, 189);
            this.lineUC30.Name = "lineUC30";
            this.lineUC30.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC30.Size = new System.Drawing.Size(3, 60);
            this.lineUC30.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC30.TabIndex = 2633;
            // 
            // lineUC31
            // 
            this.lineUC31.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC31.ExtenderWidth = 3;
            this.lineUC31.Fill = false;
            this.lineUC31.GroupID = null;
            this.lineUC31.Location = new System.Drawing.Point(531, 189);
            this.lineUC31.Name = "lineUC31";
            this.lineUC31.Size = new System.Drawing.Size(50, 3);
            this.lineUC31.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC31.TabIndex = 2632;
            // 
            // lineUC32
            // 
            this.lineUC32.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC32.ExtenderWidth = 3;
            this.lineUC32.Fill = false;
            this.lineUC32.GroupID = null;
            this.lineUC32.Location = new System.Drawing.Point(531, 189);
            this.lineUC32.Name = "lineUC32";
            this.lineUC32.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC32.Size = new System.Drawing.Size(3, 60);
            this.lineUC32.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC32.TabIndex = 2631;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label24.Location = new System.Drawing.Point(1035, 159);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(23, 15);
            this.label24.TabIndex = 2709;
            this.label24.Text = "Q1";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label25.Location = new System.Drawing.Point(1013, 205);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(21, 15);
            this.label25.TabIndex = 2708;
            this.label25.Text = "K1";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label26.Location = new System.Drawing.Point(1061, 208);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(21, 15);
            this.label26.TabIndex = 2707;
            this.label26.Text = "K3";
            // 
            // rhombusUC11
            // 
            this.rhombusUC11.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC11.ExtenderWidth = 2;
            this.rhombusUC11.Fill = false;
            this.rhombusUC11.GroupID = null;
            this.rhombusUC11.Location = new System.Drawing.Point(1057, 159);
            this.rhombusUC11.Name = "rhombusUC11";
            this.rhombusUC11.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC11.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC11.TabIndex = 2700;
            // 
            // lineUC33
            // 
            this.lineUC33.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC33.ExtenderWidth = 3;
            this.lineUC33.Fill = false;
            this.lineUC33.GroupID = null;
            this.lineUC33.Location = new System.Drawing.Point(1064, 139);
            this.lineUC33.Name = "lineUC33";
            this.lineUC33.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC33.Size = new System.Drawing.Size(3, 50);
            this.lineUC33.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC33.TabIndex = 2706;
            // 
            // retangleUC15
            // 
            this.retangleUC15.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC15.ExtenderWidth = 3;
            this.retangleUC15.Fill = false;
            this.retangleUC15.GroupID = null;
            this.retangleUC15.Location = new System.Drawing.Point(1084, 205);
            this.retangleUC15.Name = "retangleUC15";
            this.retangleUC15.Size = new System.Drawing.Size(15, 15);
            this.retangleUC15.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC15.TabIndex = 2705;
            // 
            // retangleUC16
            // 
            this.retangleUC16.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC16.ExtenderWidth = 3;
            this.retangleUC16.Fill = false;
            this.retangleUC16.GroupID = null;
            this.retangleUC16.Location = new System.Drawing.Point(1034, 205);
            this.retangleUC16.Name = "retangleUC16";
            this.retangleUC16.Size = new System.Drawing.Size(15, 15);
            this.retangleUC16.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC16.TabIndex = 2704;
            // 
            // lineUC39
            // 
            this.lineUC39.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC39.ExtenderWidth = 3;
            this.lineUC39.Fill = false;
            this.lineUC39.GroupID = null;
            this.lineUC39.Location = new System.Drawing.Point(1090, 189);
            this.lineUC39.Name = "lineUC39";
            this.lineUC39.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC39.Size = new System.Drawing.Size(3, 96);
            this.lineUC39.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC39.TabIndex = 2703;
            // 
            // lineUC40
            // 
            this.lineUC40.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC40.ExtenderWidth = 3;
            this.lineUC40.Fill = false;
            this.lineUC40.GroupID = null;
            this.lineUC40.Location = new System.Drawing.Point(1040, 189);
            this.lineUC40.Name = "lineUC40";
            this.lineUC40.Size = new System.Drawing.Size(50, 3);
            this.lineUC40.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC40.TabIndex = 2702;
            // 
            // lineUC41
            // 
            this.lineUC41.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC41.ExtenderWidth = 3;
            this.lineUC41.Fill = false;
            this.lineUC41.GroupID = null;
            this.lineUC41.Location = new System.Drawing.Point(1040, 189);
            this.lineUC41.Name = "lineUC41";
            this.lineUC41.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC41.Size = new System.Drawing.Size(3, 96);
            this.lineUC41.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC41.TabIndex = 2701;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label27.Location = new System.Drawing.Point(1100, 205);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(21, 15);
            this.label27.TabIndex = 2699;
            this.label27.Text = "K2";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label28.Location = new System.Drawing.Point(1122, 159);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(23, 15);
            this.label28.TabIndex = 2698;
            this.label28.Text = "Q2";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label29.Location = new System.Drawing.Point(1148, 208);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(21, 15);
            this.label29.TabIndex = 2697;
            this.label29.Text = "K4";
            // 
            // rhombusUC12
            // 
            this.rhombusUC12.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC12.ExtenderWidth = 2;
            this.rhombusUC12.Fill = false;
            this.rhombusUC12.GroupID = null;
            this.rhombusUC12.Location = new System.Drawing.Point(1144, 159);
            this.rhombusUC12.Name = "rhombusUC12";
            this.rhombusUC12.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC12.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC12.TabIndex = 2690;
            // 
            // lineUC42
            // 
            this.lineUC42.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC42.ExtenderWidth = 3;
            this.lineUC42.Fill = false;
            this.lineUC42.GroupID = null;
            this.lineUC42.Location = new System.Drawing.Point(1151, 139);
            this.lineUC42.Name = "lineUC42";
            this.lineUC42.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC42.Size = new System.Drawing.Size(3, 50);
            this.lineUC42.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC42.TabIndex = 2696;
            // 
            // retangleUC17
            // 
            this.retangleUC17.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC17.ExtenderWidth = 3;
            this.retangleUC17.Fill = false;
            this.retangleUC17.GroupID = null;
            this.retangleUC17.Location = new System.Drawing.Point(1171, 205);
            this.retangleUC17.Name = "retangleUC17";
            this.retangleUC17.Size = new System.Drawing.Size(15, 15);
            this.retangleUC17.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC17.TabIndex = 2695;
            // 
            // retangleUC19
            // 
            this.retangleUC19.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC19.ExtenderWidth = 3;
            this.retangleUC19.Fill = false;
            this.retangleUC19.GroupID = null;
            this.retangleUC19.Location = new System.Drawing.Point(1121, 205);
            this.retangleUC19.Name = "retangleUC19";
            this.retangleUC19.Size = new System.Drawing.Size(15, 15);
            this.retangleUC19.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC19.TabIndex = 2694;
            // 
            // lineUC43
            // 
            this.lineUC43.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC43.ExtenderWidth = 3;
            this.lineUC43.Fill = false;
            this.lineUC43.GroupID = null;
            this.lineUC43.Location = new System.Drawing.Point(1177, 189);
            this.lineUC43.Name = "lineUC43";
            this.lineUC43.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC43.Size = new System.Drawing.Size(3, 60);
            this.lineUC43.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC43.TabIndex = 2693;
            // 
            // lineUC44
            // 
            this.lineUC44.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC44.ExtenderWidth = 3;
            this.lineUC44.Fill = false;
            this.lineUC44.GroupID = null;
            this.lineUC44.Location = new System.Drawing.Point(1127, 189);
            this.lineUC44.Name = "lineUC44";
            this.lineUC44.Size = new System.Drawing.Size(50, 3);
            this.lineUC44.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC44.TabIndex = 2692;
            // 
            // lineUC45
            // 
            this.lineUC45.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC45.ExtenderWidth = 3;
            this.lineUC45.Fill = false;
            this.lineUC45.GroupID = null;
            this.lineUC45.Location = new System.Drawing.Point(1127, 189);
            this.lineUC45.Name = "lineUC45";
            this.lineUC45.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC45.Size = new System.Drawing.Size(3, 60);
            this.lineUC45.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC45.TabIndex = 2691;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label30.Location = new System.Drawing.Point(859, 159);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(23, 15);
            this.label30.TabIndex = 2689;
            this.label30.Text = "Q1";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label31.Location = new System.Drawing.Point(837, 205);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(21, 15);
            this.label31.TabIndex = 2688;
            this.label31.Text = "K1";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label32.Location = new System.Drawing.Point(885, 208);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(21, 15);
            this.label32.TabIndex = 2687;
            this.label32.Text = "K3";
            // 
            // rhombusUC13
            // 
            this.rhombusUC13.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC13.ExtenderWidth = 2;
            this.rhombusUC13.Fill = false;
            this.rhombusUC13.GroupID = null;
            this.rhombusUC13.Location = new System.Drawing.Point(881, 159);
            this.rhombusUC13.Name = "rhombusUC13";
            this.rhombusUC13.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC13.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC13.TabIndex = 2680;
            // 
            // lineUC46
            // 
            this.lineUC46.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC46.ExtenderWidth = 3;
            this.lineUC46.Fill = false;
            this.lineUC46.GroupID = null;
            this.lineUC46.Location = new System.Drawing.Point(888, 139);
            this.lineUC46.Name = "lineUC46";
            this.lineUC46.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC46.Size = new System.Drawing.Size(3, 50);
            this.lineUC46.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC46.TabIndex = 2686;
            // 
            // retangleUC20
            // 
            this.retangleUC20.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC20.ExtenderWidth = 3;
            this.retangleUC20.Fill = false;
            this.retangleUC20.GroupID = null;
            this.retangleUC20.Location = new System.Drawing.Point(908, 205);
            this.retangleUC20.Name = "retangleUC20";
            this.retangleUC20.Size = new System.Drawing.Size(15, 15);
            this.retangleUC20.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC20.TabIndex = 2685;
            // 
            // retangleUC21
            // 
            this.retangleUC21.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC21.ExtenderWidth = 3;
            this.retangleUC21.Fill = false;
            this.retangleUC21.GroupID = null;
            this.retangleUC21.Location = new System.Drawing.Point(858, 205);
            this.retangleUC21.Name = "retangleUC21";
            this.retangleUC21.Size = new System.Drawing.Size(15, 15);
            this.retangleUC21.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC21.TabIndex = 2684;
            // 
            // lineUC47
            // 
            this.lineUC47.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC47.ExtenderWidth = 3;
            this.lineUC47.Fill = false;
            this.lineUC47.GroupID = null;
            this.lineUC47.Location = new System.Drawing.Point(914, 189);
            this.lineUC47.Name = "lineUC47";
            this.lineUC47.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC47.Size = new System.Drawing.Size(3, 96);
            this.lineUC47.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC47.TabIndex = 2683;
            // 
            // lineUC48
            // 
            this.lineUC48.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC48.ExtenderWidth = 3;
            this.lineUC48.Fill = false;
            this.lineUC48.GroupID = null;
            this.lineUC48.Location = new System.Drawing.Point(864, 189);
            this.lineUC48.Name = "lineUC48";
            this.lineUC48.Size = new System.Drawing.Size(50, 3);
            this.lineUC48.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC48.TabIndex = 2682;
            // 
            // lineUC49
            // 
            this.lineUC49.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC49.ExtenderWidth = 3;
            this.lineUC49.Fill = false;
            this.lineUC49.GroupID = null;
            this.lineUC49.Location = new System.Drawing.Point(864, 189);
            this.lineUC49.Name = "lineUC49";
            this.lineUC49.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC49.Size = new System.Drawing.Size(3, 96);
            this.lineUC49.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC49.TabIndex = 2681;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label33.Location = new System.Drawing.Point(924, 205);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(21, 15);
            this.label33.TabIndex = 2679;
            this.label33.Text = "K2";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label34.Location = new System.Drawing.Point(946, 159);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(23, 15);
            this.label34.TabIndex = 2678;
            this.label34.Text = "Q2";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label35.Location = new System.Drawing.Point(972, 208);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(21, 15);
            this.label35.TabIndex = 2677;
            this.label35.Text = "K4";
            // 
            // rhombusUC14
            // 
            this.rhombusUC14.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC14.ExtenderWidth = 2;
            this.rhombusUC14.Fill = false;
            this.rhombusUC14.GroupID = null;
            this.rhombusUC14.Location = new System.Drawing.Point(968, 159);
            this.rhombusUC14.Name = "rhombusUC14";
            this.rhombusUC14.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC14.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC14.TabIndex = 2670;
            // 
            // lineUC50
            // 
            this.lineUC50.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC50.ExtenderWidth = 3;
            this.lineUC50.Fill = false;
            this.lineUC50.GroupID = null;
            this.lineUC50.Location = new System.Drawing.Point(975, 139);
            this.lineUC50.Name = "lineUC50";
            this.lineUC50.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC50.Size = new System.Drawing.Size(3, 50);
            this.lineUC50.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC50.TabIndex = 2676;
            // 
            // retangleUC22
            // 
            this.retangleUC22.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC22.ExtenderWidth = 3;
            this.retangleUC22.Fill = false;
            this.retangleUC22.GroupID = null;
            this.retangleUC22.Location = new System.Drawing.Point(995, 205);
            this.retangleUC22.Name = "retangleUC22";
            this.retangleUC22.Size = new System.Drawing.Size(15, 15);
            this.retangleUC22.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC22.TabIndex = 2675;
            // 
            // retangleUC23
            // 
            this.retangleUC23.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC23.ExtenderWidth = 3;
            this.retangleUC23.Fill = false;
            this.retangleUC23.GroupID = null;
            this.retangleUC23.Location = new System.Drawing.Point(945, 205);
            this.retangleUC23.Name = "retangleUC23";
            this.retangleUC23.Size = new System.Drawing.Size(15, 15);
            this.retangleUC23.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC23.TabIndex = 2674;
            // 
            // lineUC51
            // 
            this.lineUC51.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC51.ExtenderWidth = 3;
            this.lineUC51.Fill = false;
            this.lineUC51.GroupID = null;
            this.lineUC51.Location = new System.Drawing.Point(1001, 189);
            this.lineUC51.Name = "lineUC51";
            this.lineUC51.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC51.Size = new System.Drawing.Size(3, 60);
            this.lineUC51.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC51.TabIndex = 2673;
            // 
            // lineUC52
            // 
            this.lineUC52.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC52.ExtenderWidth = 3;
            this.lineUC52.Fill = false;
            this.lineUC52.GroupID = null;
            this.lineUC52.Location = new System.Drawing.Point(951, 189);
            this.lineUC52.Name = "lineUC52";
            this.lineUC52.Size = new System.Drawing.Size(50, 3);
            this.lineUC52.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC52.TabIndex = 2672;
            // 
            // lineUC53
            // 
            this.lineUC53.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC53.ExtenderWidth = 3;
            this.lineUC53.Fill = false;
            this.lineUC53.GroupID = null;
            this.lineUC53.Location = new System.Drawing.Point(951, 189);
            this.lineUC53.Name = "lineUC53";
            this.lineUC53.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC53.Size = new System.Drawing.Size(3, 60);
            this.lineUC53.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC53.TabIndex = 2671;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label36.Location = new System.Drawing.Point(645, 159);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(23, 15);
            this.label36.TabIndex = 2669;
            this.label36.Text = "Q1";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label37.Location = new System.Drawing.Point(623, 205);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(21, 15);
            this.label37.TabIndex = 2668;
            this.label37.Text = "K1";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label38.Location = new System.Drawing.Point(671, 208);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(21, 15);
            this.label38.TabIndex = 2667;
            this.label38.Text = "K3";
            // 
            // rhombusUC15
            // 
            this.rhombusUC15.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC15.ExtenderWidth = 2;
            this.rhombusUC15.Fill = false;
            this.rhombusUC15.GroupID = null;
            this.rhombusUC15.Location = new System.Drawing.Point(667, 159);
            this.rhombusUC15.Name = "rhombusUC15";
            this.rhombusUC15.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC15.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC15.TabIndex = 2660;
            // 
            // lineUC54
            // 
            this.lineUC54.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC54.ExtenderWidth = 3;
            this.lineUC54.Fill = false;
            this.lineUC54.GroupID = null;
            this.lineUC54.Location = new System.Drawing.Point(674, 139);
            this.lineUC54.Name = "lineUC54";
            this.lineUC54.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC54.Size = new System.Drawing.Size(3, 50);
            this.lineUC54.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC54.TabIndex = 2666;
            // 
            // retangleUC24
            // 
            this.retangleUC24.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC24.ExtenderWidth = 3;
            this.retangleUC24.Fill = false;
            this.retangleUC24.GroupID = null;
            this.retangleUC24.Location = new System.Drawing.Point(694, 205);
            this.retangleUC24.Name = "retangleUC24";
            this.retangleUC24.Size = new System.Drawing.Size(15, 15);
            this.retangleUC24.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC24.TabIndex = 2665;
            // 
            // retangleUC25
            // 
            this.retangleUC25.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC25.ExtenderWidth = 3;
            this.retangleUC25.Fill = false;
            this.retangleUC25.GroupID = null;
            this.retangleUC25.Location = new System.Drawing.Point(644, 205);
            this.retangleUC25.Name = "retangleUC25";
            this.retangleUC25.Size = new System.Drawing.Size(15, 15);
            this.retangleUC25.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC25.TabIndex = 2664;
            // 
            // lineUC55
            // 
            this.lineUC55.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC55.ExtenderWidth = 3;
            this.lineUC55.Fill = false;
            this.lineUC55.GroupID = null;
            this.lineUC55.Location = new System.Drawing.Point(700, 189);
            this.lineUC55.Name = "lineUC55";
            this.lineUC55.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC55.Size = new System.Drawing.Size(3, 96);
            this.lineUC55.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC55.TabIndex = 2663;
            // 
            // lineUC56
            // 
            this.lineUC56.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC56.ExtenderWidth = 3;
            this.lineUC56.Fill = false;
            this.lineUC56.GroupID = null;
            this.lineUC56.Location = new System.Drawing.Point(650, 189);
            this.lineUC56.Name = "lineUC56";
            this.lineUC56.Size = new System.Drawing.Size(50, 3);
            this.lineUC56.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC56.TabIndex = 2662;
            // 
            // lineUC57
            // 
            this.lineUC57.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC57.ExtenderWidth = 3;
            this.lineUC57.Fill = false;
            this.lineUC57.GroupID = null;
            this.lineUC57.Location = new System.Drawing.Point(650, 189);
            this.lineUC57.Name = "lineUC57";
            this.lineUC57.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC57.Size = new System.Drawing.Size(3, 96);
            this.lineUC57.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC57.TabIndex = 2661;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label39.Location = new System.Drawing.Point(710, 205);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(21, 15);
            this.label39.TabIndex = 2659;
            this.label39.Text = "K2";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label40.Location = new System.Drawing.Point(732, 159);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(23, 15);
            this.label40.TabIndex = 2658;
            this.label40.Text = "Q2";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label41.Location = new System.Drawing.Point(758, 208);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(21, 15);
            this.label41.TabIndex = 2657;
            this.label41.Text = "K4";
            // 
            // rhombusUC16
            // 
            this.rhombusUC16.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC16.ExtenderWidth = 2;
            this.rhombusUC16.Fill = false;
            this.rhombusUC16.GroupID = null;
            this.rhombusUC16.Location = new System.Drawing.Point(754, 159);
            this.rhombusUC16.Name = "rhombusUC16";
            this.rhombusUC16.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC16.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC16.TabIndex = 2650;
            // 
            // lineUC58
            // 
            this.lineUC58.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC58.ExtenderWidth = 3;
            this.lineUC58.Fill = false;
            this.lineUC58.GroupID = null;
            this.lineUC58.Location = new System.Drawing.Point(761, 139);
            this.lineUC58.Name = "lineUC58";
            this.lineUC58.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC58.Size = new System.Drawing.Size(3, 50);
            this.lineUC58.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC58.TabIndex = 2656;
            // 
            // retangleUC26
            // 
            this.retangleUC26.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC26.ExtenderWidth = 3;
            this.retangleUC26.Fill = false;
            this.retangleUC26.GroupID = null;
            this.retangleUC26.Location = new System.Drawing.Point(781, 205);
            this.retangleUC26.Name = "retangleUC26";
            this.retangleUC26.Size = new System.Drawing.Size(15, 15);
            this.retangleUC26.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC26.TabIndex = 2655;
            // 
            // retangleUC27
            // 
            this.retangleUC27.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC27.ExtenderWidth = 3;
            this.retangleUC27.Fill = false;
            this.retangleUC27.GroupID = null;
            this.retangleUC27.Location = new System.Drawing.Point(731, 205);
            this.retangleUC27.Name = "retangleUC27";
            this.retangleUC27.Size = new System.Drawing.Size(15, 15);
            this.retangleUC27.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC27.TabIndex = 2654;
            // 
            // lineUC59
            // 
            this.lineUC59.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC59.ExtenderWidth = 3;
            this.lineUC59.Fill = false;
            this.lineUC59.GroupID = null;
            this.lineUC59.Location = new System.Drawing.Point(787, 189);
            this.lineUC59.Name = "lineUC59";
            this.lineUC59.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC59.Size = new System.Drawing.Size(3, 60);
            this.lineUC59.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC59.TabIndex = 2653;
            // 
            // lineUC60
            // 
            this.lineUC60.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC60.ExtenderWidth = 3;
            this.lineUC60.Fill = false;
            this.lineUC60.GroupID = null;
            this.lineUC60.Location = new System.Drawing.Point(737, 189);
            this.lineUC60.Name = "lineUC60";
            this.lineUC60.Size = new System.Drawing.Size(50, 3);
            this.lineUC60.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC60.TabIndex = 2652;
            // 
            // lineUC61
            // 
            this.lineUC61.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC61.ExtenderWidth = 3;
            this.lineUC61.Fill = false;
            this.lineUC61.GroupID = null;
            this.lineUC61.Location = new System.Drawing.Point(737, 189);
            this.lineUC61.Name = "lineUC61";
            this.lineUC61.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC61.Size = new System.Drawing.Size(3, 60);
            this.lineUC61.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC61.TabIndex = 2651;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label42.Location = new System.Drawing.Point(1193, 120);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(39, 15);
            this.label42.TabIndex = 2724;
            this.label42.Text = "MC41";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label43.Location = new System.Drawing.Point(1252, 83);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(39, 15);
            this.label43.TabIndex = 2723;
            this.label43.Text = "MC22";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label44.Location = new System.Drawing.Point(1136, 83);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(39, 15);
            this.label44.TabIndex = 2722;
            this.label44.Text = "MC21";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label45.Location = new System.Drawing.Point(1218, 18);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(40, 15);
            this.label45.TabIndex = 2721;
            this.label45.Text = "MG01";
            // 
            // rhombusUC17
            // 
            this.rhombusUC17.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC17.ExtenderWidth = 2;
            this.rhombusUC17.Fill = false;
            this.rhombusUC17.GroupID = null;
            this.rhombusUC17.Location = new System.Drawing.Point(1202, 101);
            this.rhombusUC17.Name = "rhombusUC17";
            this.rhombusUC17.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC17.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC17.TabIndex = 2720;
            // 
            // rhombusUC18
            // 
            this.rhombusUC18.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC18.ExtenderWidth = 2;
            this.rhombusUC18.Fill = false;
            this.rhombusUC18.GroupID = null;
            this.rhombusUC18.Location = new System.Drawing.Point(1234, 82);
            this.rhombusUC18.Name = "rhombusUC18";
            this.rhombusUC18.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC18.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC18.TabIndex = 2719;
            // 
            // rhombusUC19
            // 
            this.rhombusUC19.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC19.ExtenderWidth = 2;
            this.rhombusUC19.Fill = false;
            this.rhombusUC19.GroupID = null;
            this.rhombusUC19.Location = new System.Drawing.Point(1175, 82);
            this.rhombusUC19.Name = "rhombusUC19";
            this.rhombusUC19.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC19.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC19.TabIndex = 2718;
            // 
            // lineUC62
            // 
            this.lineUC62.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC62.ExtenderWidth = 3;
            this.lineUC62.Fill = false;
            this.lineUC62.GroupID = null;
            this.lineUC62.Location = new System.Drawing.Point(1182, 110);
            this.lineUC62.Name = "lineUC62";
            this.lineUC62.Size = new System.Drawing.Size(60, 3);
            this.lineUC62.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC62.TabIndex = 2717;
            // 
            // rhombusUC24
            // 
            this.rhombusUC24.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC24.ExtenderWidth = 2;
            this.rhombusUC24.Fill = false;
            this.rhombusUC24.GroupID = null;
            this.rhombusUC24.Location = new System.Drawing.Point(1202, 43);
            this.rhombusUC24.Name = "rhombusUC24";
            this.rhombusUC24.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC24.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC24.TabIndex = 2712;
            // 
            // lineUC63
            // 
            this.lineUC63.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC63.ExtenderWidth = 3;
            this.lineUC63.Fill = false;
            this.lineUC63.GroupID = null;
            this.lineUC63.Location = new System.Drawing.Point(1209, 33);
            this.lineUC63.Name = "lineUC63";
            this.lineUC63.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC63.Size = new System.Drawing.Size(3, 40);
            this.lineUC63.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC63.TabIndex = 2716;
            // 
            // lineUC64
            // 
            this.lineUC64.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC64.ExtenderWidth = 3;
            this.lineUC64.Fill = false;
            this.lineUC64.GroupID = null;
            this.lineUC64.Location = new System.Drawing.Point(1240, 70);
            this.lineUC64.Name = "lineUC64";
            this.lineUC64.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC64.Size = new System.Drawing.Size(3, 70);
            this.lineUC64.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC64.TabIndex = 2715;
            // 
            // lineUC65
            // 
            this.lineUC65.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC65.ExtenderWidth = 3;
            this.lineUC65.Fill = false;
            this.lineUC65.GroupID = null;
            this.lineUC65.Location = new System.Drawing.Point(1182, 70);
            this.lineUC65.Name = "lineUC65";
            this.lineUC65.Size = new System.Drawing.Size(60, 3);
            this.lineUC65.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC65.TabIndex = 2714;
            // 
            // lineUC66
            // 
            this.lineUC66.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC66.ExtenderWidth = 3;
            this.lineUC66.Fill = false;
            this.lineUC66.GroupID = null;
            this.lineUC66.Location = new System.Drawing.Point(1182, 70);
            this.lineUC66.Name = "lineUC66";
            this.lineUC66.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC66.Size = new System.Drawing.Size(3, 70);
            this.lineUC66.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC66.TabIndex = 2713;
            // 
            // retangleUC28
            // 
            this.retangleUC28.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC28.ExtenderWidth = 3;
            this.retangleUC28.Fill = false;
            this.retangleUC28.GroupID = null;
            this.retangleUC28.Location = new System.Drawing.Point(1203, 18);
            this.retangleUC28.Name = "retangleUC28";
            this.retangleUC28.Size = new System.Drawing.Size(15, 15);
            this.retangleUC28.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC28.TabIndex = 2711;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label46.Location = new System.Drawing.Point(1220, 44);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(39, 15);
            this.label46.TabIndex = 2710;
            this.label46.Text = "MC81";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label50.Location = new System.Drawing.Point(1571, 205);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(21, 15);
            this.label50.TabIndex = 2774;
            this.label50.Text = "K2";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label51.Location = new System.Drawing.Point(1593, 159);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(23, 15);
            this.label51.TabIndex = 2773;
            this.label51.Text = "Q2";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label52.Location = new System.Drawing.Point(1619, 208);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(21, 15);
            this.label52.TabIndex = 2772;
            this.label52.Text = "K4";
            // 
            // rhombusUC26
            // 
            this.rhombusUC26.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC26.ExtenderWidth = 2;
            this.rhombusUC26.Fill = false;
            this.rhombusUC26.GroupID = null;
            this.rhombusUC26.Location = new System.Drawing.Point(1615, 159);
            this.rhombusUC26.Name = "rhombusUC26";
            this.rhombusUC26.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC26.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC26.TabIndex = 2765;
            // 
            // lineUC71
            // 
            this.lineUC71.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC71.ExtenderWidth = 3;
            this.lineUC71.Fill = false;
            this.lineUC71.GroupID = null;
            this.lineUC71.Location = new System.Drawing.Point(1622, 139);
            this.lineUC71.Name = "lineUC71";
            this.lineUC71.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC71.Size = new System.Drawing.Size(3, 50);
            this.lineUC71.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC71.TabIndex = 2771;
            // 
            // retangleUC31
            // 
            this.retangleUC31.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC31.ExtenderWidth = 3;
            this.retangleUC31.Fill = false;
            this.retangleUC31.GroupID = null;
            this.retangleUC31.Location = new System.Drawing.Point(1642, 205);
            this.retangleUC31.Name = "retangleUC31";
            this.retangleUC31.Size = new System.Drawing.Size(15, 15);
            this.retangleUC31.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC31.TabIndex = 2770;
            // 
            // retangleUC32
            // 
            this.retangleUC32.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC32.ExtenderWidth = 3;
            this.retangleUC32.Fill = false;
            this.retangleUC32.GroupID = null;
            this.retangleUC32.Location = new System.Drawing.Point(1592, 205);
            this.retangleUC32.Name = "retangleUC32";
            this.retangleUC32.Size = new System.Drawing.Size(15, 15);
            this.retangleUC32.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC32.TabIndex = 2769;
            // 
            // lineUC72
            // 
            this.lineUC72.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC72.ExtenderWidth = 3;
            this.lineUC72.Fill = false;
            this.lineUC72.GroupID = null;
            this.lineUC72.Location = new System.Drawing.Point(1648, 189);
            this.lineUC72.Name = "lineUC72";
            this.lineUC72.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC72.Size = new System.Drawing.Size(3, 60);
            this.lineUC72.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC72.TabIndex = 2768;
            // 
            // lineUC73
            // 
            this.lineUC73.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC73.ExtenderWidth = 3;
            this.lineUC73.Fill = false;
            this.lineUC73.GroupID = null;
            this.lineUC73.Location = new System.Drawing.Point(1598, 189);
            this.lineUC73.Name = "lineUC73";
            this.lineUC73.Size = new System.Drawing.Size(50, 3);
            this.lineUC73.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC73.TabIndex = 2767;
            // 
            // lineUC74
            // 
            this.lineUC74.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC74.ExtenderWidth = 3;
            this.lineUC74.Fill = false;
            this.lineUC74.GroupID = null;
            this.lineUC74.Location = new System.Drawing.Point(1598, 189);
            this.lineUC74.Name = "lineUC74";
            this.lineUC74.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC74.Size = new System.Drawing.Size(3, 60);
            this.lineUC74.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC74.TabIndex = 2766;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label53.Location = new System.Drawing.Point(1420, 159);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(23, 15);
            this.label53.TabIndex = 2764;
            this.label53.Text = "Q1";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label54.Location = new System.Drawing.Point(1398, 205);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(21, 15);
            this.label54.TabIndex = 2763;
            this.label54.Text = "K1";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label55.Location = new System.Drawing.Point(1446, 208);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(21, 15);
            this.label55.TabIndex = 2762;
            this.label55.Text = "K3";
            // 
            // rhombusUC27
            // 
            this.rhombusUC27.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC27.ExtenderWidth = 2;
            this.rhombusUC27.Fill = false;
            this.rhombusUC27.GroupID = null;
            this.rhombusUC27.Location = new System.Drawing.Point(1442, 159);
            this.rhombusUC27.Name = "rhombusUC27";
            this.rhombusUC27.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC27.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC27.TabIndex = 2755;
            // 
            // lineUC75
            // 
            this.lineUC75.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC75.ExtenderWidth = 3;
            this.lineUC75.Fill = false;
            this.lineUC75.GroupID = null;
            this.lineUC75.Location = new System.Drawing.Point(1449, 139);
            this.lineUC75.Name = "lineUC75";
            this.lineUC75.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC75.Size = new System.Drawing.Size(3, 50);
            this.lineUC75.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC75.TabIndex = 2761;
            // 
            // retangleUC33
            // 
            this.retangleUC33.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC33.ExtenderWidth = 3;
            this.retangleUC33.Fill = false;
            this.retangleUC33.GroupID = null;
            this.retangleUC33.Location = new System.Drawing.Point(1469, 205);
            this.retangleUC33.Name = "retangleUC33";
            this.retangleUC33.Size = new System.Drawing.Size(15, 15);
            this.retangleUC33.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC33.TabIndex = 2760;
            // 
            // retangleUC34
            // 
            this.retangleUC34.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC34.ExtenderWidth = 3;
            this.retangleUC34.Fill = false;
            this.retangleUC34.GroupID = null;
            this.retangleUC34.Location = new System.Drawing.Point(1419, 205);
            this.retangleUC34.Name = "retangleUC34";
            this.retangleUC34.Size = new System.Drawing.Size(15, 15);
            this.retangleUC34.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC34.TabIndex = 2759;
            // 
            // lineUC76
            // 
            this.lineUC76.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC76.ExtenderWidth = 3;
            this.lineUC76.Fill = false;
            this.lineUC76.GroupID = null;
            this.lineUC76.Location = new System.Drawing.Point(1475, 189);
            this.lineUC76.Name = "lineUC76";
            this.lineUC76.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC76.Size = new System.Drawing.Size(3, 96);
            this.lineUC76.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC76.TabIndex = 2758;
            // 
            // lineUC77
            // 
            this.lineUC77.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC77.ExtenderWidth = 3;
            this.lineUC77.Fill = false;
            this.lineUC77.GroupID = null;
            this.lineUC77.Location = new System.Drawing.Point(1425, 189);
            this.lineUC77.Name = "lineUC77";
            this.lineUC77.Size = new System.Drawing.Size(50, 3);
            this.lineUC77.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC77.TabIndex = 2757;
            // 
            // lineUC78
            // 
            this.lineUC78.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC78.ExtenderWidth = 3;
            this.lineUC78.Fill = false;
            this.lineUC78.GroupID = null;
            this.lineUC78.Location = new System.Drawing.Point(1425, 189);
            this.lineUC78.Name = "lineUC78";
            this.lineUC78.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC78.Size = new System.Drawing.Size(3, 96);
            this.lineUC78.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC78.TabIndex = 2756;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label56.Location = new System.Drawing.Point(1485, 205);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(21, 15);
            this.label56.TabIndex = 2754;
            this.label56.Text = "K2";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label57.Location = new System.Drawing.Point(1507, 159);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(23, 15);
            this.label57.TabIndex = 2753;
            this.label57.Text = "Q2";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label58.Location = new System.Drawing.Point(1533, 208);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(21, 15);
            this.label58.TabIndex = 2752;
            this.label58.Text = "K4";
            // 
            // rhombusUC28
            // 
            this.rhombusUC28.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC28.ExtenderWidth = 2;
            this.rhombusUC28.Fill = false;
            this.rhombusUC28.GroupID = null;
            this.rhombusUC28.Location = new System.Drawing.Point(1529, 159);
            this.rhombusUC28.Name = "rhombusUC28";
            this.rhombusUC28.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC28.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC28.TabIndex = 2745;
            // 
            // lineUC79
            // 
            this.lineUC79.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC79.ExtenderWidth = 3;
            this.lineUC79.Fill = false;
            this.lineUC79.GroupID = null;
            this.lineUC79.Location = new System.Drawing.Point(1536, 139);
            this.lineUC79.Name = "lineUC79";
            this.lineUC79.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC79.Size = new System.Drawing.Size(3, 50);
            this.lineUC79.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC79.TabIndex = 2751;
            // 
            // retangleUC35
            // 
            this.retangleUC35.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC35.ExtenderWidth = 3;
            this.retangleUC35.Fill = false;
            this.retangleUC35.GroupID = null;
            this.retangleUC35.Location = new System.Drawing.Point(1556, 205);
            this.retangleUC35.Name = "retangleUC35";
            this.retangleUC35.Size = new System.Drawing.Size(15, 15);
            this.retangleUC35.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC35.TabIndex = 2750;
            // 
            // retangleUC36
            // 
            this.retangleUC36.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC36.ExtenderWidth = 3;
            this.retangleUC36.Fill = false;
            this.retangleUC36.GroupID = null;
            this.retangleUC36.Location = new System.Drawing.Point(1506, 205);
            this.retangleUC36.Name = "retangleUC36";
            this.retangleUC36.Size = new System.Drawing.Size(15, 15);
            this.retangleUC36.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC36.TabIndex = 2749;
            // 
            // lineUC80
            // 
            this.lineUC80.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC80.ExtenderWidth = 3;
            this.lineUC80.Fill = false;
            this.lineUC80.GroupID = null;
            this.lineUC80.Location = new System.Drawing.Point(1562, 189);
            this.lineUC80.Name = "lineUC80";
            this.lineUC80.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC80.Size = new System.Drawing.Size(3, 60);
            this.lineUC80.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC80.TabIndex = 2748;
            // 
            // lineUC81
            // 
            this.lineUC81.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC81.ExtenderWidth = 3;
            this.lineUC81.Fill = false;
            this.lineUC81.GroupID = null;
            this.lineUC81.Location = new System.Drawing.Point(1512, 189);
            this.lineUC81.Name = "lineUC81";
            this.lineUC81.Size = new System.Drawing.Size(50, 3);
            this.lineUC81.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC81.TabIndex = 2747;
            // 
            // lineUC82
            // 
            this.lineUC82.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC82.ExtenderWidth = 3;
            this.lineUC82.Fill = false;
            this.lineUC82.GroupID = null;
            this.lineUC82.Location = new System.Drawing.Point(1512, 189);
            this.lineUC82.Name = "lineUC82";
            this.lineUC82.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC82.Size = new System.Drawing.Size(3, 60);
            this.lineUC82.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC82.TabIndex = 2746;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label59.Location = new System.Drawing.Point(1224, 159);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(23, 15);
            this.label59.TabIndex = 2744;
            this.label59.Text = "Q1";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label60.Location = new System.Drawing.Point(1202, 205);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(21, 15);
            this.label60.TabIndex = 2743;
            this.label60.Text = "K1";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label61.Location = new System.Drawing.Point(1250, 208);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(21, 15);
            this.label61.TabIndex = 2742;
            this.label61.Text = "K3";
            // 
            // rhombusUC29
            // 
            this.rhombusUC29.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC29.ExtenderWidth = 2;
            this.rhombusUC29.Fill = false;
            this.rhombusUC29.GroupID = null;
            this.rhombusUC29.Location = new System.Drawing.Point(1246, 159);
            this.rhombusUC29.Name = "rhombusUC29";
            this.rhombusUC29.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC29.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC29.TabIndex = 2735;
            // 
            // lineUC83
            // 
            this.lineUC83.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC83.ExtenderWidth = 3;
            this.lineUC83.Fill = false;
            this.lineUC83.GroupID = null;
            this.lineUC83.Location = new System.Drawing.Point(1253, 139);
            this.lineUC83.Name = "lineUC83";
            this.lineUC83.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC83.Size = new System.Drawing.Size(3, 50);
            this.lineUC83.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC83.TabIndex = 2741;
            // 
            // retangleUC37
            // 
            this.retangleUC37.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC37.ExtenderWidth = 3;
            this.retangleUC37.Fill = false;
            this.retangleUC37.GroupID = null;
            this.retangleUC37.Location = new System.Drawing.Point(1273, 205);
            this.retangleUC37.Name = "retangleUC37";
            this.retangleUC37.Size = new System.Drawing.Size(15, 15);
            this.retangleUC37.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC37.TabIndex = 2740;
            // 
            // retangleUC38
            // 
            this.retangleUC38.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC38.ExtenderWidth = 3;
            this.retangleUC38.Fill = false;
            this.retangleUC38.GroupID = null;
            this.retangleUC38.Location = new System.Drawing.Point(1223, 205);
            this.retangleUC38.Name = "retangleUC38";
            this.retangleUC38.Size = new System.Drawing.Size(15, 15);
            this.retangleUC38.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC38.TabIndex = 2739;
            // 
            // lineUC84
            // 
            this.lineUC84.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC84.ExtenderWidth = 3;
            this.lineUC84.Fill = false;
            this.lineUC84.GroupID = null;
            this.lineUC84.Location = new System.Drawing.Point(1279, 189);
            this.lineUC84.Name = "lineUC84";
            this.lineUC84.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC84.Size = new System.Drawing.Size(3, 96);
            this.lineUC84.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC84.TabIndex = 2738;
            // 
            // lineUC85
            // 
            this.lineUC85.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC85.ExtenderWidth = 3;
            this.lineUC85.Fill = false;
            this.lineUC85.GroupID = null;
            this.lineUC85.Location = new System.Drawing.Point(1229, 189);
            this.lineUC85.Name = "lineUC85";
            this.lineUC85.Size = new System.Drawing.Size(50, 3);
            this.lineUC85.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC85.TabIndex = 2737;
            // 
            // lineUC86
            // 
            this.lineUC86.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC86.ExtenderWidth = 3;
            this.lineUC86.Fill = false;
            this.lineUC86.GroupID = null;
            this.lineUC86.Location = new System.Drawing.Point(1229, 189);
            this.lineUC86.Name = "lineUC86";
            this.lineUC86.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC86.Size = new System.Drawing.Size(3, 96);
            this.lineUC86.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC86.TabIndex = 2736;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label62.Location = new System.Drawing.Point(1289, 205);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(21, 15);
            this.label62.TabIndex = 2734;
            this.label62.Text = "K2";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label63.Location = new System.Drawing.Point(1311, 159);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(23, 15);
            this.label63.TabIndex = 2733;
            this.label63.Text = "Q2";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label64.Location = new System.Drawing.Point(1337, 208);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(21, 15);
            this.label64.TabIndex = 2732;
            this.label64.Text = "K4";
            // 
            // rhombusUC30
            // 
            this.rhombusUC30.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC30.ExtenderWidth = 2;
            this.rhombusUC30.Fill = false;
            this.rhombusUC30.GroupID = null;
            this.rhombusUC30.Location = new System.Drawing.Point(1333, 159);
            this.rhombusUC30.Name = "rhombusUC30";
            this.rhombusUC30.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC30.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC30.TabIndex = 2725;
            // 
            // lineUC87
            // 
            this.lineUC87.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC87.ExtenderWidth = 3;
            this.lineUC87.Fill = false;
            this.lineUC87.GroupID = null;
            this.lineUC87.Location = new System.Drawing.Point(1340, 139);
            this.lineUC87.Name = "lineUC87";
            this.lineUC87.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC87.Size = new System.Drawing.Size(3, 50);
            this.lineUC87.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC87.TabIndex = 2731;
            // 
            // retangleUC39
            // 
            this.retangleUC39.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC39.ExtenderWidth = 3;
            this.retangleUC39.Fill = false;
            this.retangleUC39.GroupID = null;
            this.retangleUC39.Location = new System.Drawing.Point(1360, 205);
            this.retangleUC39.Name = "retangleUC39";
            this.retangleUC39.Size = new System.Drawing.Size(15, 15);
            this.retangleUC39.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC39.TabIndex = 2730;
            // 
            // retangleUC40
            // 
            this.retangleUC40.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC40.ExtenderWidth = 3;
            this.retangleUC40.Fill = false;
            this.retangleUC40.GroupID = null;
            this.retangleUC40.Location = new System.Drawing.Point(1310, 205);
            this.retangleUC40.Name = "retangleUC40";
            this.retangleUC40.Size = new System.Drawing.Size(15, 15);
            this.retangleUC40.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC40.TabIndex = 2729;
            // 
            // lineUC88
            // 
            this.lineUC88.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC88.ExtenderWidth = 3;
            this.lineUC88.Fill = false;
            this.lineUC88.GroupID = null;
            this.lineUC88.Location = new System.Drawing.Point(1366, 189);
            this.lineUC88.Name = "lineUC88";
            this.lineUC88.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC88.Size = new System.Drawing.Size(3, 60);
            this.lineUC88.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC88.TabIndex = 2728;
            // 
            // lineUC89
            // 
            this.lineUC89.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC89.ExtenderWidth = 3;
            this.lineUC89.Fill = false;
            this.lineUC89.GroupID = null;
            this.lineUC89.Location = new System.Drawing.Point(1316, 189);
            this.lineUC89.Name = "lineUC89";
            this.lineUC89.Size = new System.Drawing.Size(50, 3);
            this.lineUC89.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC89.TabIndex = 2727;
            // 
            // lineUC90
            // 
            this.lineUC90.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC90.ExtenderWidth = 3;
            this.lineUC90.Fill = false;
            this.lineUC90.GroupID = null;
            this.lineUC90.Location = new System.Drawing.Point(1316, 189);
            this.lineUC90.Name = "lineUC90";
            this.lineUC90.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC90.Size = new System.Drawing.Size(3, 60);
            this.lineUC90.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC90.TabIndex = 2726;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label47.Location = new System.Drawing.Point(1666, 120);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(39, 15);
            this.label47.TabIndex = 2789;
            this.label47.Text = "MC41";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label48.Location = new System.Drawing.Point(1725, 83);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(39, 15);
            this.label48.TabIndex = 2788;
            this.label48.Text = "MC22";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label49.Location = new System.Drawing.Point(1609, 83);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(39, 15);
            this.label49.TabIndex = 2787;
            this.label49.Text = "MC21";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label65.Location = new System.Drawing.Point(1691, 18);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(40, 15);
            this.label65.TabIndex = 2786;
            this.label65.Text = "MG01";
            // 
            // rhombusUC25
            // 
            this.rhombusUC25.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC25.ExtenderWidth = 2;
            this.rhombusUC25.Fill = false;
            this.rhombusUC25.GroupID = null;
            this.rhombusUC25.Location = new System.Drawing.Point(1675, 101);
            this.rhombusUC25.Name = "rhombusUC25";
            this.rhombusUC25.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC25.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC25.TabIndex = 2785;
            // 
            // rhombusUC31
            // 
            this.rhombusUC31.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC31.ExtenderWidth = 2;
            this.rhombusUC31.Fill = false;
            this.rhombusUC31.GroupID = null;
            this.rhombusUC31.Location = new System.Drawing.Point(1707, 82);
            this.rhombusUC31.Name = "rhombusUC31";
            this.rhombusUC31.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC31.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC31.TabIndex = 2784;
            // 
            // rhombusUC32
            // 
            this.rhombusUC32.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC32.ExtenderWidth = 2;
            this.rhombusUC32.Fill = false;
            this.rhombusUC32.GroupID = null;
            this.rhombusUC32.Location = new System.Drawing.Point(1648, 82);
            this.rhombusUC32.Name = "rhombusUC32";
            this.rhombusUC32.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC32.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC32.TabIndex = 2783;
            // 
            // lineUC67
            // 
            this.lineUC67.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC67.ExtenderWidth = 3;
            this.lineUC67.Fill = false;
            this.lineUC67.GroupID = null;
            this.lineUC67.Location = new System.Drawing.Point(1655, 110);
            this.lineUC67.Name = "lineUC67";
            this.lineUC67.Size = new System.Drawing.Size(60, 3);
            this.lineUC67.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC67.TabIndex = 2782;
            // 
            // rhombusUC33
            // 
            this.rhombusUC33.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC33.ExtenderWidth = 2;
            this.rhombusUC33.Fill = false;
            this.rhombusUC33.GroupID = null;
            this.rhombusUC33.Location = new System.Drawing.Point(1675, 43);
            this.rhombusUC33.Name = "rhombusUC33";
            this.rhombusUC33.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC33.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC33.TabIndex = 2777;
            // 
            // lineUC68
            // 
            this.lineUC68.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC68.ExtenderWidth = 3;
            this.lineUC68.Fill = false;
            this.lineUC68.GroupID = null;
            this.lineUC68.Location = new System.Drawing.Point(1682, 33);
            this.lineUC68.Name = "lineUC68";
            this.lineUC68.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC68.Size = new System.Drawing.Size(3, 40);
            this.lineUC68.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC68.TabIndex = 2781;
            // 
            // lineUC69
            // 
            this.lineUC69.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC69.ExtenderWidth = 3;
            this.lineUC69.Fill = false;
            this.lineUC69.GroupID = null;
            this.lineUC69.Location = new System.Drawing.Point(1713, 70);
            this.lineUC69.Name = "lineUC69";
            this.lineUC69.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC69.Size = new System.Drawing.Size(3, 70);
            this.lineUC69.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC69.TabIndex = 2780;
            // 
            // lineUC70
            // 
            this.lineUC70.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC70.ExtenderWidth = 3;
            this.lineUC70.Fill = false;
            this.lineUC70.GroupID = null;
            this.lineUC70.Location = new System.Drawing.Point(1655, 70);
            this.lineUC70.Name = "lineUC70";
            this.lineUC70.Size = new System.Drawing.Size(60, 3);
            this.lineUC70.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC70.TabIndex = 2779;
            // 
            // lineUC91
            // 
            this.lineUC91.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC91.ExtenderWidth = 3;
            this.lineUC91.Fill = false;
            this.lineUC91.GroupID = null;
            this.lineUC91.Location = new System.Drawing.Point(1655, 70);
            this.lineUC91.Name = "lineUC91";
            this.lineUC91.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC91.Size = new System.Drawing.Size(3, 70);
            this.lineUC91.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC91.TabIndex = 2778;
            // 
            // retangleUC29
            // 
            this.retangleUC29.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC29.ExtenderWidth = 3;
            this.retangleUC29.Fill = false;
            this.retangleUC29.GroupID = null;
            this.retangleUC29.Location = new System.Drawing.Point(1676, 18);
            this.retangleUC29.Name = "retangleUC29";
            this.retangleUC29.Size = new System.Drawing.Size(15, 15);
            this.retangleUC29.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC29.TabIndex = 2776;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label66.Location = new System.Drawing.Point(1693, 44);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(39, 15);
            this.label66.TabIndex = 2775;
            this.label66.Text = "MC81";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label67.Location = new System.Drawing.Point(1745, 159);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(23, 15);
            this.label67.TabIndex = 2809;
            this.label67.Text = "Q1";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label68.Location = new System.Drawing.Point(1723, 205);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(21, 15);
            this.label68.TabIndex = 2808;
            this.label68.Text = "K1";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label69.Location = new System.Drawing.Point(1771, 208);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(21, 15);
            this.label69.TabIndex = 2807;
            this.label69.Text = "K3";
            // 
            // rhombusUC34
            // 
            this.rhombusUC34.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC34.ExtenderWidth = 2;
            this.rhombusUC34.Fill = false;
            this.rhombusUC34.GroupID = null;
            this.rhombusUC34.Location = new System.Drawing.Point(1767, 159);
            this.rhombusUC34.Name = "rhombusUC34";
            this.rhombusUC34.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC34.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC34.TabIndex = 2800;
            // 
            // lineUC92
            // 
            this.lineUC92.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC92.ExtenderWidth = 3;
            this.lineUC92.Fill = false;
            this.lineUC92.GroupID = null;
            this.lineUC92.Location = new System.Drawing.Point(1774, 139);
            this.lineUC92.Name = "lineUC92";
            this.lineUC92.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC92.Size = new System.Drawing.Size(3, 50);
            this.lineUC92.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC92.TabIndex = 2806;
            // 
            // retangleUC30
            // 
            this.retangleUC30.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC30.ExtenderWidth = 3;
            this.retangleUC30.Fill = false;
            this.retangleUC30.GroupID = null;
            this.retangleUC30.Location = new System.Drawing.Point(1794, 205);
            this.retangleUC30.Name = "retangleUC30";
            this.retangleUC30.Size = new System.Drawing.Size(15, 15);
            this.retangleUC30.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC30.TabIndex = 2805;
            // 
            // retangleUC41
            // 
            this.retangleUC41.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC41.ExtenderWidth = 3;
            this.retangleUC41.Fill = false;
            this.retangleUC41.GroupID = null;
            this.retangleUC41.Location = new System.Drawing.Point(1744, 205);
            this.retangleUC41.Name = "retangleUC41";
            this.retangleUC41.Size = new System.Drawing.Size(15, 15);
            this.retangleUC41.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC41.TabIndex = 2804;
            // 
            // lineUC93
            // 
            this.lineUC93.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC93.ExtenderWidth = 3;
            this.lineUC93.Fill = false;
            this.lineUC93.GroupID = null;
            this.lineUC93.Location = new System.Drawing.Point(1800, 189);
            this.lineUC93.Name = "lineUC93";
            this.lineUC93.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC93.Size = new System.Drawing.Size(3, 96);
            this.lineUC93.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC93.TabIndex = 2803;
            // 
            // lineUC94
            // 
            this.lineUC94.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC94.ExtenderWidth = 3;
            this.lineUC94.Fill = false;
            this.lineUC94.GroupID = null;
            this.lineUC94.Location = new System.Drawing.Point(1750, 189);
            this.lineUC94.Name = "lineUC94";
            this.lineUC94.Size = new System.Drawing.Size(50, 3);
            this.lineUC94.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC94.TabIndex = 2802;
            // 
            // lineUC95
            // 
            this.lineUC95.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC95.ExtenderWidth = 3;
            this.lineUC95.Fill = false;
            this.lineUC95.GroupID = null;
            this.lineUC95.Location = new System.Drawing.Point(1750, 189);
            this.lineUC95.Name = "lineUC95";
            this.lineUC95.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC95.Size = new System.Drawing.Size(3, 96);
            this.lineUC95.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC95.TabIndex = 2801;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label70.Location = new System.Drawing.Point(1810, 205);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(21, 15);
            this.label70.TabIndex = 2799;
            this.label70.Text = "K2";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label76.Location = new System.Drawing.Point(1832, 159);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(23, 15);
            this.label76.TabIndex = 2798;
            this.label76.Text = "Q2";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label77.Location = new System.Drawing.Point(1858, 208);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(21, 15);
            this.label77.TabIndex = 2797;
            this.label77.Text = "K4";
            // 
            // rhombusUC35
            // 
            this.rhombusUC35.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC35.ExtenderWidth = 2;
            this.rhombusUC35.Fill = false;
            this.rhombusUC35.GroupID = null;
            this.rhombusUC35.Location = new System.Drawing.Point(1854, 159);
            this.rhombusUC35.Name = "rhombusUC35";
            this.rhombusUC35.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC35.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC35.TabIndex = 2790;
            // 
            // lineUC96
            // 
            this.lineUC96.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC96.ExtenderWidth = 3;
            this.lineUC96.Fill = false;
            this.lineUC96.GroupID = null;
            this.lineUC96.Location = new System.Drawing.Point(1861, 139);
            this.lineUC96.Name = "lineUC96";
            this.lineUC96.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC96.Size = new System.Drawing.Size(3, 50);
            this.lineUC96.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC96.TabIndex = 2796;
            // 
            // retangleUC42
            // 
            this.retangleUC42.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC42.ExtenderWidth = 3;
            this.retangleUC42.Fill = false;
            this.retangleUC42.GroupID = null;
            this.retangleUC42.Location = new System.Drawing.Point(1881, 205);
            this.retangleUC42.Name = "retangleUC42";
            this.retangleUC42.Size = new System.Drawing.Size(15, 15);
            this.retangleUC42.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC42.TabIndex = 2795;
            // 
            // retangleUC43
            // 
            this.retangleUC43.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC43.ExtenderWidth = 3;
            this.retangleUC43.Fill = false;
            this.retangleUC43.GroupID = null;
            this.retangleUC43.Location = new System.Drawing.Point(1831, 205);
            this.retangleUC43.Name = "retangleUC43";
            this.retangleUC43.Size = new System.Drawing.Size(15, 15);
            this.retangleUC43.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC43.TabIndex = 2794;
            // 
            // lineUC97
            // 
            this.lineUC97.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC97.ExtenderWidth = 3;
            this.lineUC97.Fill = false;
            this.lineUC97.GroupID = null;
            this.lineUC97.Location = new System.Drawing.Point(1887, 189);
            this.lineUC97.Name = "lineUC97";
            this.lineUC97.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC97.Size = new System.Drawing.Size(3, 60);
            this.lineUC97.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC97.TabIndex = 2793;
            // 
            // lineUC98
            // 
            this.lineUC98.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC98.ExtenderWidth = 3;
            this.lineUC98.Fill = false;
            this.lineUC98.GroupID = null;
            this.lineUC98.Location = new System.Drawing.Point(1837, 189);
            this.lineUC98.Name = "lineUC98";
            this.lineUC98.Size = new System.Drawing.Size(50, 3);
            this.lineUC98.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC98.TabIndex = 2792;
            // 
            // lineUC99
            // 
            this.lineUC99.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC99.ExtenderWidth = 3;
            this.lineUC99.Fill = false;
            this.lineUC99.GroupID = null;
            this.lineUC99.Location = new System.Drawing.Point(1837, 189);
            this.lineUC99.Name = "lineUC99";
            this.lineUC99.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC99.Size = new System.Drawing.Size(3, 60);
            this.lineUC99.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC99.TabIndex = 2791;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label79.Location = new System.Drawing.Point(1321, 418);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(39, 15);
            this.label79.TabIndex = 2963;
            this.label79.Text = "MC41";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label80.Location = new System.Drawing.Point(1380, 381);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(39, 15);
            this.label80.TabIndex = 2962;
            this.label80.Text = "MC22";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label81.Location = new System.Drawing.Point(1264, 381);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(39, 15);
            this.label81.TabIndex = 2961;
            this.label81.Text = "MC21";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label82.Location = new System.Drawing.Point(1347, 316);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(40, 15);
            this.label82.TabIndex = 2960;
            this.label82.Text = "MG01";
            // 
            // rhombusUC22
            // 
            this.rhombusUC22.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC22.ExtenderWidth = 2;
            this.rhombusUC22.Fill = false;
            this.rhombusUC22.GroupID = null;
            this.rhombusUC22.Location = new System.Drawing.Point(1331, 399);
            this.rhombusUC22.Name = "rhombusUC22";
            this.rhombusUC22.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC22.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC22.TabIndex = 2959;
            // 
            // rhombusUC23
            // 
            this.rhombusUC23.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC23.ExtenderWidth = 2;
            this.rhombusUC23.Fill = false;
            this.rhombusUC23.GroupID = null;
            this.rhombusUC23.Location = new System.Drawing.Point(1363, 380);
            this.rhombusUC23.Name = "rhombusUC23";
            this.rhombusUC23.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC23.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC23.TabIndex = 2958;
            // 
            // rhombusUC36
            // 
            this.rhombusUC36.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC36.ExtenderWidth = 2;
            this.rhombusUC36.Fill = false;
            this.rhombusUC36.GroupID = null;
            this.rhombusUC36.Location = new System.Drawing.Point(1303, 380);
            this.rhombusUC36.Name = "rhombusUC36";
            this.rhombusUC36.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC36.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC36.TabIndex = 2957;
            // 
            // lineUC103
            // 
            this.lineUC103.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC103.ExtenderWidth = 3;
            this.lineUC103.Fill = false;
            this.lineUC103.GroupID = null;
            this.lineUC103.Location = new System.Drawing.Point(1311, 408);
            this.lineUC103.Name = "lineUC103";
            this.lineUC103.Size = new System.Drawing.Size(60, 3);
            this.lineUC103.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC103.TabIndex = 2956;
            // 
            // rhombusUC37
            // 
            this.rhombusUC37.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC37.ExtenderWidth = 2;
            this.rhombusUC37.Fill = false;
            this.rhombusUC37.GroupID = null;
            this.rhombusUC37.Location = new System.Drawing.Point(1331, 341);
            this.rhombusUC37.Name = "rhombusUC37";
            this.rhombusUC37.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC37.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC37.TabIndex = 2951;
            // 
            // lineUC104
            // 
            this.lineUC104.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC104.ExtenderWidth = 3;
            this.lineUC104.Fill = false;
            this.lineUC104.GroupID = null;
            this.lineUC104.Location = new System.Drawing.Point(1337, 331);
            this.lineUC104.Name = "lineUC104";
            this.lineUC104.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC104.Size = new System.Drawing.Size(3, 40);
            this.lineUC104.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC104.TabIndex = 2955;
            // 
            // lineUC105
            // 
            this.lineUC105.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC105.ExtenderWidth = 3;
            this.lineUC105.Fill = false;
            this.lineUC105.GroupID = null;
            this.lineUC105.Location = new System.Drawing.Point(1368, 368);
            this.lineUC105.Name = "lineUC105";
            this.lineUC105.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC105.Size = new System.Drawing.Size(3, 70);
            this.lineUC105.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC105.TabIndex = 2954;
            // 
            // lineUC106
            // 
            this.lineUC106.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC106.ExtenderWidth = 3;
            this.lineUC106.Fill = false;
            this.lineUC106.GroupID = null;
            this.lineUC106.Location = new System.Drawing.Point(1311, 368);
            this.lineUC106.Name = "lineUC106";
            this.lineUC106.Size = new System.Drawing.Size(60, 3);
            this.lineUC106.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC106.TabIndex = 2953;
            // 
            // lineUC107
            // 
            this.lineUC107.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC107.ExtenderWidth = 3;
            this.lineUC107.Fill = false;
            this.lineUC107.GroupID = null;
            this.lineUC107.Location = new System.Drawing.Point(1311, 368);
            this.lineUC107.Name = "lineUC107";
            this.lineUC107.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC107.Size = new System.Drawing.Size(3, 70);
            this.lineUC107.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC107.TabIndex = 2952;
            // 
            // retangleUC47
            // 
            this.retangleUC47.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC47.ExtenderWidth = 3;
            this.retangleUC47.Fill = false;
            this.retangleUC47.GroupID = null;
            this.retangleUC47.Location = new System.Drawing.Point(1331, 316);
            this.retangleUC47.Name = "retangleUC47";
            this.retangleUC47.Size = new System.Drawing.Size(15, 15);
            this.retangleUC47.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC47.TabIndex = 2950;
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label83.Location = new System.Drawing.Point(1348, 342);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(39, 15);
            this.label83.TabIndex = 2949;
            this.label83.Text = "MC81";
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label118.Location = new System.Drawing.Point(579, 418);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(39, 15);
            this.label118.TabIndex = 2837;
            this.label118.Text = "MC41";
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label119.Location = new System.Drawing.Point(638, 381);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(39, 15);
            this.label119.TabIndex = 2836;
            this.label119.Text = "MC22";
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label120.Location = new System.Drawing.Point(522, 381);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(39, 15);
            this.label120.TabIndex = 2835;
            this.label120.Text = "MC21";
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label121.Location = new System.Drawing.Point(603, 316);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(40, 15);
            this.label121.TabIndex = 2834;
            this.label121.Text = "MG01";
            // 
            // rhombusUC49
            // 
            this.rhombusUC49.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC49.ExtenderWidth = 2;
            this.rhombusUC49.Fill = false;
            this.rhombusUC49.GroupID = null;
            this.rhombusUC49.Location = new System.Drawing.Point(587, 399);
            this.rhombusUC49.Name = "rhombusUC49";
            this.rhombusUC49.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC49.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC49.TabIndex = 2833;
            // 
            // rhombusUC50
            // 
            this.rhombusUC50.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC50.ExtenderWidth = 2;
            this.rhombusUC50.Fill = false;
            this.rhombusUC50.GroupID = null;
            this.rhombusUC50.Location = new System.Drawing.Point(619, 380);
            this.rhombusUC50.Name = "rhombusUC50";
            this.rhombusUC50.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC50.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC50.TabIndex = 2832;
            // 
            // rhombusUC51
            // 
            this.rhombusUC51.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC51.ExtenderWidth = 2;
            this.rhombusUC51.Fill = false;
            this.rhombusUC51.GroupID = null;
            this.rhombusUC51.Location = new System.Drawing.Point(561, 380);
            this.rhombusUC51.Name = "rhombusUC51";
            this.rhombusUC51.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC51.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC51.TabIndex = 2831;
            // 
            // lineUC152
            // 
            this.lineUC152.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC152.ExtenderWidth = 3;
            this.lineUC152.Fill = false;
            this.lineUC152.GroupID = null;
            this.lineUC152.Location = new System.Drawing.Point(567, 408);
            this.lineUC152.Name = "lineUC152";
            this.lineUC152.Size = new System.Drawing.Size(60, 3);
            this.lineUC152.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC152.TabIndex = 2830;
            // 
            // rhombusUC52
            // 
            this.rhombusUC52.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC52.ExtenderWidth = 2;
            this.rhombusUC52.Fill = false;
            this.rhombusUC52.GroupID = null;
            this.rhombusUC52.Location = new System.Drawing.Point(587, 341);
            this.rhombusUC52.Name = "rhombusUC52";
            this.rhombusUC52.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC52.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC52.TabIndex = 2825;
            // 
            // lineUC153
            // 
            this.lineUC153.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC153.ExtenderWidth = 3;
            this.lineUC153.Fill = false;
            this.lineUC153.GroupID = null;
            this.lineUC153.Location = new System.Drawing.Point(595, 331);
            this.lineUC153.Name = "lineUC153";
            this.lineUC153.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC153.Size = new System.Drawing.Size(3, 40);
            this.lineUC153.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC153.TabIndex = 2829;
            // 
            // lineUC154
            // 
            this.lineUC154.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC154.ExtenderWidth = 3;
            this.lineUC154.Fill = false;
            this.lineUC154.GroupID = null;
            this.lineUC154.Location = new System.Drawing.Point(626, 368);
            this.lineUC154.Name = "lineUC154";
            this.lineUC154.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC154.Size = new System.Drawing.Size(3, 70);
            this.lineUC154.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC154.TabIndex = 2828;
            // 
            // lineUC155
            // 
            this.lineUC155.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC155.ExtenderWidth = 3;
            this.lineUC155.Fill = false;
            this.lineUC155.GroupID = null;
            this.lineUC155.Location = new System.Drawing.Point(567, 368);
            this.lineUC155.Name = "lineUC155";
            this.lineUC155.Size = new System.Drawing.Size(60, 3);
            this.lineUC155.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC155.TabIndex = 2827;
            // 
            // lineUC156
            // 
            this.lineUC156.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC156.ExtenderWidth = 3;
            this.lineUC156.Fill = false;
            this.lineUC156.GroupID = null;
            this.lineUC156.Location = new System.Drawing.Point(567, 368);
            this.lineUC156.Name = "lineUC156";
            this.lineUC156.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC156.Size = new System.Drawing.Size(3, 70);
            this.lineUC156.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC156.TabIndex = 2826;
            // 
            // retangleUC70
            // 
            this.retangleUC70.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC70.ExtenderWidth = 3;
            this.retangleUC70.Fill = false;
            this.retangleUC70.GroupID = null;
            this.retangleUC70.Location = new System.Drawing.Point(589, 316);
            this.retangleUC70.Name = "retangleUC70";
            this.retangleUC70.Size = new System.Drawing.Size(15, 15);
            this.retangleUC70.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC70.TabIndex = 2824;
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label122.Location = new System.Drawing.Point(606, 342);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(39, 15);
            this.label122.TabIndex = 2823;
            this.label122.Text = "MC81";
            // 
            // retangleUC73
            // 
            this.retangleUC73.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC73.ExtenderWidth = 3;
            this.retangleUC73.Fill = false;
            this.retangleUC73.GroupID = null;
            this.retangleUC73.Location = new System.Drawing.Point(44, 583);
            this.retangleUC73.Name = "retangleUC73";
            this.retangleUC73.Size = new System.Drawing.Size(65, 16);
            this.retangleUC73.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC73.TabIndex = 2813;
            // 
            // lineUC161
            // 
            this.lineUC161.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC161.ExtenderWidth = 3;
            this.lineUC161.Fill = false;
            this.lineUC161.GroupID = null;
            this.lineUC161.Location = new System.Drawing.Point(0, 596);
            this.lineUC161.Name = "lineUC161";
            this.lineUC161.Size = new System.Drawing.Size(1900, 3);
            this.lineUC161.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC161.TabIndex = 2812;
            // 
            // lineUC162
            // 
            this.lineUC162.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC162.ExtenderWidth = 3;
            this.lineUC162.Fill = false;
            this.lineUC162.GroupID = null;
            this.lineUC162.Location = new System.Drawing.Point(1, 560);
            this.lineUC162.Name = "lineUC162";
            this.lineUC162.Size = new System.Drawing.Size(1900, 3);
            this.lineUC162.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC162.TabIndex = 2811;
            // 
            // lineUC163
            // 
            this.lineUC163.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC163.ExtenderWidth = 3;
            this.lineUC163.Fill = false;
            this.lineUC163.GroupID = null;
            this.lineUC163.Location = new System.Drawing.Point(12, 435);
            this.lineUC163.Name = "lineUC163";
            this.lineUC163.Size = new System.Drawing.Size(557, 3);
            this.lineUC163.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC163.TabIndex = 2810;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label71.Location = new System.Drawing.Point(196, 503);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(21, 15);
            this.label71.TabIndex = 2993;
            this.label71.Text = "K2";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label72.Location = new System.Drawing.Point(218, 457);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(23, 15);
            this.label72.TabIndex = 2992;
            this.label72.Text = "Q2";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label73.Location = new System.Drawing.Point(244, 506);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(21, 15);
            this.label73.TabIndex = 2991;
            this.label73.Text = "K4";
            // 
            // rhombusUC20
            // 
            this.rhombusUC20.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC20.ExtenderWidth = 2;
            this.rhombusUC20.Fill = false;
            this.rhombusUC20.GroupID = null;
            this.rhombusUC20.Location = new System.Drawing.Point(240, 457);
            this.rhombusUC20.Name = "rhombusUC20";
            this.rhombusUC20.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC20.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC20.TabIndex = 2984;
            // 
            // lineUC34
            // 
            this.lineUC34.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC34.ExtenderWidth = 3;
            this.lineUC34.Fill = false;
            this.lineUC34.GroupID = null;
            this.lineUC34.Location = new System.Drawing.Point(247, 437);
            this.lineUC34.Name = "lineUC34";
            this.lineUC34.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC34.Size = new System.Drawing.Size(3, 50);
            this.lineUC34.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC34.TabIndex = 2990;
            // 
            // retangleUC18
            // 
            this.retangleUC18.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC18.ExtenderWidth = 3;
            this.retangleUC18.Fill = false;
            this.retangleUC18.GroupID = null;
            this.retangleUC18.Location = new System.Drawing.Point(267, 503);
            this.retangleUC18.Name = "retangleUC18";
            this.retangleUC18.Size = new System.Drawing.Size(15, 15);
            this.retangleUC18.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC18.TabIndex = 2989;
            // 
            // retangleUC44
            // 
            this.retangleUC44.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC44.ExtenderWidth = 3;
            this.retangleUC44.Fill = false;
            this.retangleUC44.GroupID = null;
            this.retangleUC44.Location = new System.Drawing.Point(217, 503);
            this.retangleUC44.Name = "retangleUC44";
            this.retangleUC44.Size = new System.Drawing.Size(15, 15);
            this.retangleUC44.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC44.TabIndex = 2988;
            // 
            // lineUC35
            // 
            this.lineUC35.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC35.ExtenderWidth = 3;
            this.lineUC35.Fill = false;
            this.lineUC35.GroupID = null;
            this.lineUC35.Location = new System.Drawing.Point(273, 487);
            this.lineUC35.Name = "lineUC35";
            this.lineUC35.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC35.Size = new System.Drawing.Size(3, 60);
            this.lineUC35.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC35.TabIndex = 2987;
            // 
            // lineUC36
            // 
            this.lineUC36.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC36.ExtenderWidth = 3;
            this.lineUC36.Fill = false;
            this.lineUC36.GroupID = null;
            this.lineUC36.Location = new System.Drawing.Point(223, 487);
            this.lineUC36.Name = "lineUC36";
            this.lineUC36.Size = new System.Drawing.Size(50, 3);
            this.lineUC36.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC36.TabIndex = 2986;
            // 
            // lineUC37
            // 
            this.lineUC37.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC37.ExtenderWidth = 3;
            this.lineUC37.Fill = false;
            this.lineUC37.GroupID = null;
            this.lineUC37.Location = new System.Drawing.Point(223, 487);
            this.lineUC37.Name = "lineUC37";
            this.lineUC37.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC37.Size = new System.Drawing.Size(3, 60);
            this.lineUC37.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC37.TabIndex = 2985;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label74.Location = new System.Drawing.Point(45, 457);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(23, 15);
            this.label74.TabIndex = 2983;
            this.label74.Text = "Q1";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label75.Location = new System.Drawing.Point(23, 503);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(21, 15);
            this.label75.TabIndex = 2982;
            this.label75.Text = "K1";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label78.Location = new System.Drawing.Point(71, 506);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(21, 15);
            this.label78.TabIndex = 2981;
            this.label78.Text = "K3";
            // 
            // rhombusUC21
            // 
            this.rhombusUC21.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC21.ExtenderWidth = 2;
            this.rhombusUC21.Fill = false;
            this.rhombusUC21.GroupID = null;
            this.rhombusUC21.Location = new System.Drawing.Point(67, 457);
            this.rhombusUC21.Name = "rhombusUC21";
            this.rhombusUC21.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC21.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC21.TabIndex = 2974;
            // 
            // lineUC38
            // 
            this.lineUC38.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC38.ExtenderWidth = 3;
            this.lineUC38.Fill = false;
            this.lineUC38.GroupID = null;
            this.lineUC38.Location = new System.Drawing.Point(74, 437);
            this.lineUC38.Name = "lineUC38";
            this.lineUC38.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC38.Size = new System.Drawing.Size(3, 50);
            this.lineUC38.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC38.TabIndex = 2980;
            // 
            // retangleUC45
            // 
            this.retangleUC45.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC45.ExtenderWidth = 3;
            this.retangleUC45.Fill = false;
            this.retangleUC45.GroupID = null;
            this.retangleUC45.Location = new System.Drawing.Point(94, 503);
            this.retangleUC45.Name = "retangleUC45";
            this.retangleUC45.Size = new System.Drawing.Size(15, 15);
            this.retangleUC45.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC45.TabIndex = 2979;
            // 
            // retangleUC46
            // 
            this.retangleUC46.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC46.ExtenderWidth = 3;
            this.retangleUC46.Fill = false;
            this.retangleUC46.GroupID = null;
            this.retangleUC46.Location = new System.Drawing.Point(44, 503);
            this.retangleUC46.Name = "retangleUC46";
            this.retangleUC46.Size = new System.Drawing.Size(15, 15);
            this.retangleUC46.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC46.TabIndex = 2978;
            // 
            // lineUC100
            // 
            this.lineUC100.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC100.ExtenderWidth = 3;
            this.lineUC100.Fill = false;
            this.lineUC100.GroupID = null;
            this.lineUC100.Location = new System.Drawing.Point(100, 487);
            this.lineUC100.Name = "lineUC100";
            this.lineUC100.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC100.Size = new System.Drawing.Size(3, 96);
            this.lineUC100.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC100.TabIndex = 2977;
            // 
            // lineUC101
            // 
            this.lineUC101.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC101.ExtenderWidth = 3;
            this.lineUC101.Fill = false;
            this.lineUC101.GroupID = null;
            this.lineUC101.Location = new System.Drawing.Point(50, 487);
            this.lineUC101.Name = "lineUC101";
            this.lineUC101.Size = new System.Drawing.Size(50, 3);
            this.lineUC101.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC101.TabIndex = 2976;
            // 
            // lineUC102
            // 
            this.lineUC102.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC102.ExtenderWidth = 3;
            this.lineUC102.Fill = false;
            this.lineUC102.GroupID = null;
            this.lineUC102.Location = new System.Drawing.Point(50, 487);
            this.lineUC102.Name = "lineUC102";
            this.lineUC102.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC102.Size = new System.Drawing.Size(3, 96);
            this.lineUC102.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC102.TabIndex = 2975;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label84.Location = new System.Drawing.Point(110, 503);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(21, 15);
            this.label84.TabIndex = 2973;
            this.label84.Text = "K2";
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label85.Location = new System.Drawing.Point(132, 457);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(23, 15);
            this.label85.TabIndex = 2972;
            this.label85.Text = "Q2";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label86.Location = new System.Drawing.Point(158, 506);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(21, 15);
            this.label86.TabIndex = 2971;
            this.label86.Text = "K4";
            // 
            // rhombusUC38
            // 
            this.rhombusUC38.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC38.ExtenderWidth = 2;
            this.rhombusUC38.Fill = false;
            this.rhombusUC38.GroupID = null;
            this.rhombusUC38.Location = new System.Drawing.Point(154, 457);
            this.rhombusUC38.Name = "rhombusUC38";
            this.rhombusUC38.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC38.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC38.TabIndex = 2964;
            // 
            // lineUC108
            // 
            this.lineUC108.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC108.ExtenderWidth = 3;
            this.lineUC108.Fill = false;
            this.lineUC108.GroupID = null;
            this.lineUC108.Location = new System.Drawing.Point(161, 437);
            this.lineUC108.Name = "lineUC108";
            this.lineUC108.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC108.Size = new System.Drawing.Size(3, 50);
            this.lineUC108.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC108.TabIndex = 2970;
            // 
            // retangleUC48
            // 
            this.retangleUC48.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC48.ExtenderWidth = 3;
            this.retangleUC48.Fill = false;
            this.retangleUC48.GroupID = null;
            this.retangleUC48.Location = new System.Drawing.Point(181, 503);
            this.retangleUC48.Name = "retangleUC48";
            this.retangleUC48.Size = new System.Drawing.Size(15, 15);
            this.retangleUC48.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC48.TabIndex = 2969;
            // 
            // retangleUC49
            // 
            this.retangleUC49.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC49.ExtenderWidth = 3;
            this.retangleUC49.Fill = false;
            this.retangleUC49.GroupID = null;
            this.retangleUC49.Location = new System.Drawing.Point(131, 503);
            this.retangleUC49.Name = "retangleUC49";
            this.retangleUC49.Size = new System.Drawing.Size(15, 15);
            this.retangleUC49.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC49.TabIndex = 2968;
            // 
            // lineUC109
            // 
            this.lineUC109.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC109.ExtenderWidth = 3;
            this.lineUC109.Fill = false;
            this.lineUC109.GroupID = null;
            this.lineUC109.Location = new System.Drawing.Point(187, 487);
            this.lineUC109.Name = "lineUC109";
            this.lineUC109.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC109.Size = new System.Drawing.Size(3, 60);
            this.lineUC109.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC109.TabIndex = 2967;
            // 
            // lineUC110
            // 
            this.lineUC110.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC110.ExtenderWidth = 3;
            this.lineUC110.Fill = false;
            this.lineUC110.GroupID = null;
            this.lineUC110.Location = new System.Drawing.Point(137, 487);
            this.lineUC110.Name = "lineUC110";
            this.lineUC110.Size = new System.Drawing.Size(50, 3);
            this.lineUC110.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC110.TabIndex = 2966;
            // 
            // lineUC111
            // 
            this.lineUC111.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC111.ExtenderWidth = 3;
            this.lineUC111.Fill = false;
            this.lineUC111.GroupID = null;
            this.lineUC111.Location = new System.Drawing.Point(137, 487);
            this.lineUC111.Name = "lineUC111";
            this.lineUC111.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC111.Size = new System.Drawing.Size(3, 60);
            this.lineUC111.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC111.TabIndex = 2965;
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label87.Location = new System.Drawing.Point(477, 503);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(21, 15);
            this.label87.TabIndex = 3023;
            this.label87.Text = "K2";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label88.Location = new System.Drawing.Point(499, 457);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(23, 15);
            this.label88.TabIndex = 3022;
            this.label88.Text = "Q1";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label89.Location = new System.Drawing.Point(525, 506);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(21, 15);
            this.label89.TabIndex = 3021;
            this.label89.Text = "K4";
            // 
            // rhombusUC39
            // 
            this.rhombusUC39.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC39.ExtenderWidth = 2;
            this.rhombusUC39.Fill = false;
            this.rhombusUC39.GroupID = null;
            this.rhombusUC39.Location = new System.Drawing.Point(521, 457);
            this.rhombusUC39.Name = "rhombusUC39";
            this.rhombusUC39.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC39.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC39.TabIndex = 3014;
            // 
            // lineUC112
            // 
            this.lineUC112.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC112.ExtenderWidth = 3;
            this.lineUC112.Fill = false;
            this.lineUC112.GroupID = null;
            this.lineUC112.Location = new System.Drawing.Point(528, 437);
            this.lineUC112.Name = "lineUC112";
            this.lineUC112.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC112.Size = new System.Drawing.Size(3, 50);
            this.lineUC112.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC112.TabIndex = 3020;
            // 
            // retangleUC50
            // 
            this.retangleUC50.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC50.ExtenderWidth = 3;
            this.retangleUC50.Fill = false;
            this.retangleUC50.GroupID = null;
            this.retangleUC50.Location = new System.Drawing.Point(548, 503);
            this.retangleUC50.Name = "retangleUC50";
            this.retangleUC50.Size = new System.Drawing.Size(15, 15);
            this.retangleUC50.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC50.TabIndex = 3019;
            // 
            // retangleUC51
            // 
            this.retangleUC51.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC51.ExtenderWidth = 3;
            this.retangleUC51.Fill = false;
            this.retangleUC51.GroupID = null;
            this.retangleUC51.Location = new System.Drawing.Point(498, 503);
            this.retangleUC51.Name = "retangleUC51";
            this.retangleUC51.Size = new System.Drawing.Size(15, 15);
            this.retangleUC51.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC51.TabIndex = 3018;
            // 
            // lineUC113
            // 
            this.lineUC113.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC113.ExtenderWidth = 3;
            this.lineUC113.Fill = false;
            this.lineUC113.GroupID = null;
            this.lineUC113.Location = new System.Drawing.Point(554, 487);
            this.lineUC113.Name = "lineUC113";
            this.lineUC113.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC113.Size = new System.Drawing.Size(3, 96);
            this.lineUC113.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC113.TabIndex = 3017;
            // 
            // lineUC114
            // 
            this.lineUC114.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC114.ExtenderWidth = 3;
            this.lineUC114.Fill = false;
            this.lineUC114.GroupID = null;
            this.lineUC114.Location = new System.Drawing.Point(504, 487);
            this.lineUC114.Name = "lineUC114";
            this.lineUC114.Size = new System.Drawing.Size(50, 3);
            this.lineUC114.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC114.TabIndex = 3016;
            // 
            // lineUC115
            // 
            this.lineUC115.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC115.ExtenderWidth = 3;
            this.lineUC115.Fill = false;
            this.lineUC115.GroupID = null;
            this.lineUC115.Location = new System.Drawing.Point(504, 487);
            this.lineUC115.Name = "lineUC115";
            this.lineUC115.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC115.Size = new System.Drawing.Size(3, 96);
            this.lineUC115.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC115.TabIndex = 3015;
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label90.Location = new System.Drawing.Point(326, 457);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(23, 15);
            this.label90.TabIndex = 3013;
            this.label90.Text = "Q1";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label91.Location = new System.Drawing.Point(304, 503);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(21, 15);
            this.label91.TabIndex = 3012;
            this.label91.Text = "K1";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label92.Location = new System.Drawing.Point(352, 506);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(21, 15);
            this.label92.TabIndex = 3011;
            this.label92.Text = "K3";
            // 
            // rhombusUC40
            // 
            this.rhombusUC40.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC40.ExtenderWidth = 2;
            this.rhombusUC40.Fill = false;
            this.rhombusUC40.GroupID = null;
            this.rhombusUC40.Location = new System.Drawing.Point(348, 457);
            this.rhombusUC40.Name = "rhombusUC40";
            this.rhombusUC40.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC40.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC40.TabIndex = 3004;
            // 
            // lineUC116
            // 
            this.lineUC116.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC116.ExtenderWidth = 3;
            this.lineUC116.Fill = false;
            this.lineUC116.GroupID = null;
            this.lineUC116.Location = new System.Drawing.Point(355, 437);
            this.lineUC116.Name = "lineUC116";
            this.lineUC116.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC116.Size = new System.Drawing.Size(3, 50);
            this.lineUC116.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC116.TabIndex = 3010;
            // 
            // retangleUC52
            // 
            this.retangleUC52.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC52.ExtenderWidth = 3;
            this.retangleUC52.Fill = false;
            this.retangleUC52.GroupID = null;
            this.retangleUC52.Location = new System.Drawing.Point(375, 503);
            this.retangleUC52.Name = "retangleUC52";
            this.retangleUC52.Size = new System.Drawing.Size(15, 15);
            this.retangleUC52.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC52.TabIndex = 3009;
            // 
            // retangleUC53
            // 
            this.retangleUC53.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC53.ExtenderWidth = 3;
            this.retangleUC53.Fill = false;
            this.retangleUC53.GroupID = null;
            this.retangleUC53.Location = new System.Drawing.Point(325, 503);
            this.retangleUC53.Name = "retangleUC53";
            this.retangleUC53.Size = new System.Drawing.Size(15, 15);
            this.retangleUC53.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC53.TabIndex = 3008;
            // 
            // lineUC117
            // 
            this.lineUC117.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC117.ExtenderWidth = 3;
            this.lineUC117.Fill = false;
            this.lineUC117.GroupID = null;
            this.lineUC117.Location = new System.Drawing.Point(381, 487);
            this.lineUC117.Name = "lineUC117";
            this.lineUC117.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC117.Size = new System.Drawing.Size(3, 96);
            this.lineUC117.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC117.TabIndex = 3007;
            // 
            // lineUC118
            // 
            this.lineUC118.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC118.ExtenderWidth = 3;
            this.lineUC118.Fill = false;
            this.lineUC118.GroupID = null;
            this.lineUC118.Location = new System.Drawing.Point(331, 487);
            this.lineUC118.Name = "lineUC118";
            this.lineUC118.Size = new System.Drawing.Size(50, 3);
            this.lineUC118.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC118.TabIndex = 3006;
            // 
            // lineUC119
            // 
            this.lineUC119.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC119.ExtenderWidth = 3;
            this.lineUC119.Fill = false;
            this.lineUC119.GroupID = null;
            this.lineUC119.Location = new System.Drawing.Point(331, 487);
            this.lineUC119.Name = "lineUC119";
            this.lineUC119.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC119.Size = new System.Drawing.Size(3, 96);
            this.lineUC119.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC119.TabIndex = 3005;
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label93.Location = new System.Drawing.Point(391, 503);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(21, 15);
            this.label93.TabIndex = 3003;
            this.label93.Text = "K2";
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label94.Location = new System.Drawing.Point(413, 457);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(23, 15);
            this.label94.TabIndex = 3002;
            this.label94.Text = "Q2";
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label95.Location = new System.Drawing.Point(439, 506);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(21, 15);
            this.label95.TabIndex = 3001;
            this.label95.Text = "K4";
            // 
            // rhombusUC41
            // 
            this.rhombusUC41.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC41.ExtenderWidth = 2;
            this.rhombusUC41.Fill = false;
            this.rhombusUC41.GroupID = null;
            this.rhombusUC41.Location = new System.Drawing.Point(435, 457);
            this.rhombusUC41.Name = "rhombusUC41";
            this.rhombusUC41.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC41.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC41.TabIndex = 2994;
            // 
            // lineUC120
            // 
            this.lineUC120.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC120.ExtenderWidth = 3;
            this.lineUC120.Fill = false;
            this.lineUC120.GroupID = null;
            this.lineUC120.Location = new System.Drawing.Point(442, 437);
            this.lineUC120.Name = "lineUC120";
            this.lineUC120.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC120.Size = new System.Drawing.Size(3, 50);
            this.lineUC120.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC120.TabIndex = 3000;
            // 
            // retangleUC54
            // 
            this.retangleUC54.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC54.ExtenderWidth = 3;
            this.retangleUC54.Fill = false;
            this.retangleUC54.GroupID = null;
            this.retangleUC54.Location = new System.Drawing.Point(462, 503);
            this.retangleUC54.Name = "retangleUC54";
            this.retangleUC54.Size = new System.Drawing.Size(15, 15);
            this.retangleUC54.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC54.TabIndex = 2999;
            // 
            // retangleUC55
            // 
            this.retangleUC55.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC55.ExtenderWidth = 3;
            this.retangleUC55.Fill = false;
            this.retangleUC55.GroupID = null;
            this.retangleUC55.Location = new System.Drawing.Point(412, 503);
            this.retangleUC55.Name = "retangleUC55";
            this.retangleUC55.Size = new System.Drawing.Size(15, 15);
            this.retangleUC55.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC55.TabIndex = 2998;
            // 
            // lineUC121
            // 
            this.lineUC121.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC121.ExtenderWidth = 3;
            this.lineUC121.Fill = false;
            this.lineUC121.GroupID = null;
            this.lineUC121.Location = new System.Drawing.Point(468, 487);
            this.lineUC121.Name = "lineUC121";
            this.lineUC121.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC121.Size = new System.Drawing.Size(3, 60);
            this.lineUC121.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC121.TabIndex = 2997;
            // 
            // lineUC122
            // 
            this.lineUC122.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC122.ExtenderWidth = 3;
            this.lineUC122.Fill = false;
            this.lineUC122.GroupID = null;
            this.lineUC122.Location = new System.Drawing.Point(418, 487);
            this.lineUC122.Name = "lineUC122";
            this.lineUC122.Size = new System.Drawing.Size(50, 3);
            this.lineUC122.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC122.TabIndex = 2996;
            // 
            // lineUC123
            // 
            this.lineUC123.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC123.ExtenderWidth = 3;
            this.lineUC123.Fill = false;
            this.lineUC123.GroupID = null;
            this.lineUC123.Location = new System.Drawing.Point(418, 487);
            this.lineUC123.Name = "lineUC123";
            this.lineUC123.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC123.Size = new System.Drawing.Size(3, 60);
            this.lineUC123.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC123.TabIndex = 2995;
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label96.Location = new System.Drawing.Point(779, 503);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(21, 15);
            this.label96.TabIndex = 3053;
            this.label96.Text = "K2";
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label97.Location = new System.Drawing.Point(801, 457);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(23, 15);
            this.label97.TabIndex = 3052;
            this.label97.Text = "Q2";
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label98.Location = new System.Drawing.Point(827, 506);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(21, 15);
            this.label98.TabIndex = 3051;
            this.label98.Text = "K4";
            // 
            // rhombusUC42
            // 
            this.rhombusUC42.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC42.ExtenderWidth = 2;
            this.rhombusUC42.Fill = false;
            this.rhombusUC42.GroupID = null;
            this.rhombusUC42.Location = new System.Drawing.Point(823, 457);
            this.rhombusUC42.Name = "rhombusUC42";
            this.rhombusUC42.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC42.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC42.TabIndex = 3044;
            // 
            // lineUC124
            // 
            this.lineUC124.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC124.ExtenderWidth = 3;
            this.lineUC124.Fill = false;
            this.lineUC124.GroupID = null;
            this.lineUC124.Location = new System.Drawing.Point(830, 437);
            this.lineUC124.Name = "lineUC124";
            this.lineUC124.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC124.Size = new System.Drawing.Size(3, 50);
            this.lineUC124.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC124.TabIndex = 3050;
            // 
            // retangleUC56
            // 
            this.retangleUC56.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC56.ExtenderWidth = 3;
            this.retangleUC56.Fill = false;
            this.retangleUC56.GroupID = null;
            this.retangleUC56.Location = new System.Drawing.Point(850, 503);
            this.retangleUC56.Name = "retangleUC56";
            this.retangleUC56.Size = new System.Drawing.Size(15, 15);
            this.retangleUC56.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC56.TabIndex = 3049;
            // 
            // retangleUC57
            // 
            this.retangleUC57.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC57.ExtenderWidth = 3;
            this.retangleUC57.Fill = false;
            this.retangleUC57.GroupID = null;
            this.retangleUC57.Location = new System.Drawing.Point(800, 503);
            this.retangleUC57.Name = "retangleUC57";
            this.retangleUC57.Size = new System.Drawing.Size(15, 15);
            this.retangleUC57.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC57.TabIndex = 3048;
            // 
            // lineUC125
            // 
            this.lineUC125.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC125.ExtenderWidth = 3;
            this.lineUC125.Fill = false;
            this.lineUC125.GroupID = null;
            this.lineUC125.Location = new System.Drawing.Point(856, 487);
            this.lineUC125.Name = "lineUC125";
            this.lineUC125.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC125.Size = new System.Drawing.Size(3, 60);
            this.lineUC125.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC125.TabIndex = 3047;
            // 
            // lineUC126
            // 
            this.lineUC126.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC126.ExtenderWidth = 3;
            this.lineUC126.Fill = false;
            this.lineUC126.GroupID = null;
            this.lineUC126.Location = new System.Drawing.Point(806, 487);
            this.lineUC126.Name = "lineUC126";
            this.lineUC126.Size = new System.Drawing.Size(50, 3);
            this.lineUC126.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC126.TabIndex = 3046;
            // 
            // lineUC127
            // 
            this.lineUC127.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC127.ExtenderWidth = 3;
            this.lineUC127.Fill = false;
            this.lineUC127.GroupID = null;
            this.lineUC127.Location = new System.Drawing.Point(806, 487);
            this.lineUC127.Name = "lineUC127";
            this.lineUC127.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC127.Size = new System.Drawing.Size(3, 60);
            this.lineUC127.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC127.TabIndex = 3045;
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label99.Location = new System.Drawing.Point(628, 457);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(23, 15);
            this.label99.TabIndex = 3043;
            this.label99.Text = "Q1";
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label100.Location = new System.Drawing.Point(606, 503);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(21, 15);
            this.label100.TabIndex = 3042;
            this.label100.Text = "K1";
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label101.Location = new System.Drawing.Point(654, 506);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(21, 15);
            this.label101.TabIndex = 3041;
            this.label101.Text = "K3";
            // 
            // rhombusUC43
            // 
            this.rhombusUC43.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC43.ExtenderWidth = 2;
            this.rhombusUC43.Fill = false;
            this.rhombusUC43.GroupID = null;
            this.rhombusUC43.Location = new System.Drawing.Point(650, 457);
            this.rhombusUC43.Name = "rhombusUC43";
            this.rhombusUC43.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC43.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC43.TabIndex = 3034;
            // 
            // lineUC128
            // 
            this.lineUC128.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC128.ExtenderWidth = 3;
            this.lineUC128.Fill = false;
            this.lineUC128.GroupID = null;
            this.lineUC128.Location = new System.Drawing.Point(657, 437);
            this.lineUC128.Name = "lineUC128";
            this.lineUC128.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC128.Size = new System.Drawing.Size(3, 50);
            this.lineUC128.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC128.TabIndex = 3040;
            // 
            // retangleUC58
            // 
            this.retangleUC58.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC58.ExtenderWidth = 3;
            this.retangleUC58.Fill = false;
            this.retangleUC58.GroupID = null;
            this.retangleUC58.Location = new System.Drawing.Point(677, 503);
            this.retangleUC58.Name = "retangleUC58";
            this.retangleUC58.Size = new System.Drawing.Size(15, 15);
            this.retangleUC58.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC58.TabIndex = 3039;
            // 
            // retangleUC59
            // 
            this.retangleUC59.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC59.ExtenderWidth = 3;
            this.retangleUC59.Fill = false;
            this.retangleUC59.GroupID = null;
            this.retangleUC59.Location = new System.Drawing.Point(627, 503);
            this.retangleUC59.Name = "retangleUC59";
            this.retangleUC59.Size = new System.Drawing.Size(15, 15);
            this.retangleUC59.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC59.TabIndex = 3038;
            // 
            // lineUC129
            // 
            this.lineUC129.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC129.ExtenderWidth = 3;
            this.lineUC129.Fill = false;
            this.lineUC129.GroupID = null;
            this.lineUC129.Location = new System.Drawing.Point(683, 487);
            this.lineUC129.Name = "lineUC129";
            this.lineUC129.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC129.Size = new System.Drawing.Size(3, 96);
            this.lineUC129.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC129.TabIndex = 3037;
            // 
            // lineUC130
            // 
            this.lineUC130.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC130.ExtenderWidth = 3;
            this.lineUC130.Fill = false;
            this.lineUC130.GroupID = null;
            this.lineUC130.Location = new System.Drawing.Point(633, 487);
            this.lineUC130.Name = "lineUC130";
            this.lineUC130.Size = new System.Drawing.Size(50, 3);
            this.lineUC130.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC130.TabIndex = 3036;
            // 
            // lineUC131
            // 
            this.lineUC131.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC131.ExtenderWidth = 3;
            this.lineUC131.Fill = false;
            this.lineUC131.GroupID = null;
            this.lineUC131.Location = new System.Drawing.Point(633, 487);
            this.lineUC131.Name = "lineUC131";
            this.lineUC131.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC131.Size = new System.Drawing.Size(3, 96);
            this.lineUC131.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC131.TabIndex = 3035;
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label102.Location = new System.Drawing.Point(693, 503);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(21, 15);
            this.label102.TabIndex = 3033;
            this.label102.Text = "K2";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label103.Location = new System.Drawing.Point(715, 457);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(23, 15);
            this.label103.TabIndex = 3032;
            this.label103.Text = "Q2";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label104.Location = new System.Drawing.Point(741, 506);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(21, 15);
            this.label104.TabIndex = 3031;
            this.label104.Text = "K4";
            // 
            // rhombusUC44
            // 
            this.rhombusUC44.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC44.ExtenderWidth = 2;
            this.rhombusUC44.Fill = false;
            this.rhombusUC44.GroupID = null;
            this.rhombusUC44.Location = new System.Drawing.Point(737, 457);
            this.rhombusUC44.Name = "rhombusUC44";
            this.rhombusUC44.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC44.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC44.TabIndex = 3024;
            // 
            // lineUC132
            // 
            this.lineUC132.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC132.ExtenderWidth = 3;
            this.lineUC132.Fill = false;
            this.lineUC132.GroupID = null;
            this.lineUC132.Location = new System.Drawing.Point(744, 437);
            this.lineUC132.Name = "lineUC132";
            this.lineUC132.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC132.Size = new System.Drawing.Size(3, 50);
            this.lineUC132.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC132.TabIndex = 3030;
            // 
            // retangleUC60
            // 
            this.retangleUC60.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC60.ExtenderWidth = 3;
            this.retangleUC60.Fill = false;
            this.retangleUC60.GroupID = null;
            this.retangleUC60.Location = new System.Drawing.Point(764, 503);
            this.retangleUC60.Name = "retangleUC60";
            this.retangleUC60.Size = new System.Drawing.Size(15, 15);
            this.retangleUC60.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC60.TabIndex = 3029;
            // 
            // retangleUC61
            // 
            this.retangleUC61.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC61.ExtenderWidth = 3;
            this.retangleUC61.Fill = false;
            this.retangleUC61.GroupID = null;
            this.retangleUC61.Location = new System.Drawing.Point(714, 503);
            this.retangleUC61.Name = "retangleUC61";
            this.retangleUC61.Size = new System.Drawing.Size(15, 15);
            this.retangleUC61.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC61.TabIndex = 3028;
            // 
            // lineUC133
            // 
            this.lineUC133.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC133.ExtenderWidth = 3;
            this.lineUC133.Fill = false;
            this.lineUC133.GroupID = null;
            this.lineUC133.Location = new System.Drawing.Point(770, 487);
            this.lineUC133.Name = "lineUC133";
            this.lineUC133.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC133.Size = new System.Drawing.Size(3, 60);
            this.lineUC133.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC133.TabIndex = 3027;
            // 
            // lineUC134
            // 
            this.lineUC134.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC134.ExtenderWidth = 3;
            this.lineUC134.Fill = false;
            this.lineUC134.GroupID = null;
            this.lineUC134.Location = new System.Drawing.Point(720, 487);
            this.lineUC134.Name = "lineUC134";
            this.lineUC134.Size = new System.Drawing.Size(50, 3);
            this.lineUC134.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC134.TabIndex = 3026;
            // 
            // lineUC135
            // 
            this.lineUC135.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC135.ExtenderWidth = 3;
            this.lineUC135.Fill = false;
            this.lineUC135.GroupID = null;
            this.lineUC135.Location = new System.Drawing.Point(720, 487);
            this.lineUC135.Name = "lineUC135";
            this.lineUC135.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC135.Size = new System.Drawing.Size(3, 60);
            this.lineUC135.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC135.TabIndex = 3025;
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label105.Location = new System.Drawing.Point(908, 457);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(23, 15);
            this.label105.TabIndex = 3073;
            this.label105.Text = "Q1";
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label106.Location = new System.Drawing.Point(886, 503);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(21, 15);
            this.label106.TabIndex = 3072;
            this.label106.Text = "K1";
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label107.Location = new System.Drawing.Point(934, 506);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(21, 15);
            this.label107.TabIndex = 3071;
            this.label107.Text = "K3";
            // 
            // rhombusUC45
            // 
            this.rhombusUC45.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC45.ExtenderWidth = 2;
            this.rhombusUC45.Fill = false;
            this.rhombusUC45.GroupID = null;
            this.rhombusUC45.Location = new System.Drawing.Point(930, 457);
            this.rhombusUC45.Name = "rhombusUC45";
            this.rhombusUC45.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC45.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC45.TabIndex = 3064;
            // 
            // lineUC136
            // 
            this.lineUC136.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC136.ExtenderWidth = 3;
            this.lineUC136.Fill = false;
            this.lineUC136.GroupID = null;
            this.lineUC136.Location = new System.Drawing.Point(937, 437);
            this.lineUC136.Name = "lineUC136";
            this.lineUC136.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC136.Size = new System.Drawing.Size(3, 50);
            this.lineUC136.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC136.TabIndex = 3070;
            // 
            // retangleUC62
            // 
            this.retangleUC62.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC62.ExtenderWidth = 3;
            this.retangleUC62.Fill = false;
            this.retangleUC62.GroupID = null;
            this.retangleUC62.Location = new System.Drawing.Point(957, 503);
            this.retangleUC62.Name = "retangleUC62";
            this.retangleUC62.Size = new System.Drawing.Size(15, 15);
            this.retangleUC62.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC62.TabIndex = 3069;
            // 
            // retangleUC63
            // 
            this.retangleUC63.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC63.ExtenderWidth = 3;
            this.retangleUC63.Fill = false;
            this.retangleUC63.GroupID = null;
            this.retangleUC63.Location = new System.Drawing.Point(907, 503);
            this.retangleUC63.Name = "retangleUC63";
            this.retangleUC63.Size = new System.Drawing.Size(15, 15);
            this.retangleUC63.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC63.TabIndex = 3068;
            // 
            // lineUC137
            // 
            this.lineUC137.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC137.ExtenderWidth = 3;
            this.lineUC137.Fill = false;
            this.lineUC137.GroupID = null;
            this.lineUC137.Location = new System.Drawing.Point(963, 487);
            this.lineUC137.Name = "lineUC137";
            this.lineUC137.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC137.Size = new System.Drawing.Size(3, 96);
            this.lineUC137.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC137.TabIndex = 3067;
            // 
            // lineUC138
            // 
            this.lineUC138.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC138.ExtenderWidth = 3;
            this.lineUC138.Fill = false;
            this.lineUC138.GroupID = null;
            this.lineUC138.Location = new System.Drawing.Point(913, 487);
            this.lineUC138.Name = "lineUC138";
            this.lineUC138.Size = new System.Drawing.Size(50, 3);
            this.lineUC138.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC138.TabIndex = 3066;
            // 
            // lineUC139
            // 
            this.lineUC139.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC139.ExtenderWidth = 3;
            this.lineUC139.Fill = false;
            this.lineUC139.GroupID = null;
            this.lineUC139.Location = new System.Drawing.Point(913, 487);
            this.lineUC139.Name = "lineUC139";
            this.lineUC139.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC139.Size = new System.Drawing.Size(3, 96);
            this.lineUC139.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC139.TabIndex = 3065;
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label108.Location = new System.Drawing.Point(973, 503);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(21, 15);
            this.label108.TabIndex = 3063;
            this.label108.Text = "K2";
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label109.Location = new System.Drawing.Point(995, 457);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(23, 15);
            this.label109.TabIndex = 3062;
            this.label109.Text = "Q2";
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label110.Location = new System.Drawing.Point(1021, 506);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(21, 15);
            this.label110.TabIndex = 3061;
            this.label110.Text = "K4";
            // 
            // rhombusUC46
            // 
            this.rhombusUC46.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC46.ExtenderWidth = 2;
            this.rhombusUC46.Fill = false;
            this.rhombusUC46.GroupID = null;
            this.rhombusUC46.Location = new System.Drawing.Point(1017, 457);
            this.rhombusUC46.Name = "rhombusUC46";
            this.rhombusUC46.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC46.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC46.TabIndex = 3054;
            // 
            // lineUC140
            // 
            this.lineUC140.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC140.ExtenderWidth = 3;
            this.lineUC140.Fill = false;
            this.lineUC140.GroupID = null;
            this.lineUC140.Location = new System.Drawing.Point(1024, 437);
            this.lineUC140.Name = "lineUC140";
            this.lineUC140.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC140.Size = new System.Drawing.Size(3, 50);
            this.lineUC140.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC140.TabIndex = 3060;
            // 
            // retangleUC64
            // 
            this.retangleUC64.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC64.ExtenderWidth = 3;
            this.retangleUC64.Fill = false;
            this.retangleUC64.GroupID = null;
            this.retangleUC64.Location = new System.Drawing.Point(1044, 503);
            this.retangleUC64.Name = "retangleUC64";
            this.retangleUC64.Size = new System.Drawing.Size(15, 15);
            this.retangleUC64.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC64.TabIndex = 3059;
            // 
            // retangleUC65
            // 
            this.retangleUC65.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC65.ExtenderWidth = 3;
            this.retangleUC65.Fill = false;
            this.retangleUC65.GroupID = null;
            this.retangleUC65.Location = new System.Drawing.Point(994, 503);
            this.retangleUC65.Name = "retangleUC65";
            this.retangleUC65.Size = new System.Drawing.Size(15, 15);
            this.retangleUC65.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC65.TabIndex = 3058;
            // 
            // lineUC141
            // 
            this.lineUC141.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC141.ExtenderWidth = 3;
            this.lineUC141.Fill = false;
            this.lineUC141.GroupID = null;
            this.lineUC141.Location = new System.Drawing.Point(1050, 487);
            this.lineUC141.Name = "lineUC141";
            this.lineUC141.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC141.Size = new System.Drawing.Size(3, 60);
            this.lineUC141.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC141.TabIndex = 3057;
            // 
            // lineUC142
            // 
            this.lineUC142.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC142.ExtenderWidth = 3;
            this.lineUC142.Fill = false;
            this.lineUC142.GroupID = null;
            this.lineUC142.Location = new System.Drawing.Point(1000, 487);
            this.lineUC142.Name = "lineUC142";
            this.lineUC142.Size = new System.Drawing.Size(50, 3);
            this.lineUC142.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC142.TabIndex = 3056;
            // 
            // lineUC143
            // 
            this.lineUC143.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC143.ExtenderWidth = 3;
            this.lineUC143.Fill = false;
            this.lineUC143.GroupID = null;
            this.lineUC143.Location = new System.Drawing.Point(1000, 487);
            this.lineUC143.Name = "lineUC143";
            this.lineUC143.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC143.Size = new System.Drawing.Size(3, 60);
            this.lineUC143.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC143.TabIndex = 3055;
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label111.Location = new System.Drawing.Point(1233, 503);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(21, 15);
            this.label111.TabIndex = 3103;
            this.label111.Text = "K2";
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label112.Location = new System.Drawing.Point(1255, 457);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(23, 15);
            this.label112.TabIndex = 3102;
            this.label112.Text = "Q2";
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label113.Location = new System.Drawing.Point(1281, 506);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(21, 15);
            this.label113.TabIndex = 3101;
            this.label113.Text = "K4";
            // 
            // rhombusUC47
            // 
            this.rhombusUC47.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC47.ExtenderWidth = 2;
            this.rhombusUC47.Fill = false;
            this.rhombusUC47.GroupID = null;
            this.rhombusUC47.Location = new System.Drawing.Point(1277, 457);
            this.rhombusUC47.Name = "rhombusUC47";
            this.rhombusUC47.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC47.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC47.TabIndex = 3094;
            // 
            // lineUC144
            // 
            this.lineUC144.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC144.ExtenderWidth = 3;
            this.lineUC144.Fill = false;
            this.lineUC144.GroupID = null;
            this.lineUC144.Location = new System.Drawing.Point(1284, 437);
            this.lineUC144.Name = "lineUC144";
            this.lineUC144.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC144.Size = new System.Drawing.Size(3, 50);
            this.lineUC144.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC144.TabIndex = 3100;
            // 
            // retangleUC66
            // 
            this.retangleUC66.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC66.ExtenderWidth = 3;
            this.retangleUC66.Fill = false;
            this.retangleUC66.GroupID = null;
            this.retangleUC66.Location = new System.Drawing.Point(1304, 503);
            this.retangleUC66.Name = "retangleUC66";
            this.retangleUC66.Size = new System.Drawing.Size(15, 15);
            this.retangleUC66.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC66.TabIndex = 3099;
            // 
            // retangleUC67
            // 
            this.retangleUC67.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC67.ExtenderWidth = 3;
            this.retangleUC67.Fill = false;
            this.retangleUC67.GroupID = null;
            this.retangleUC67.Location = new System.Drawing.Point(1254, 503);
            this.retangleUC67.Name = "retangleUC67";
            this.retangleUC67.Size = new System.Drawing.Size(15, 15);
            this.retangleUC67.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC67.TabIndex = 3098;
            // 
            // lineUC145
            // 
            this.lineUC145.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC145.ExtenderWidth = 3;
            this.lineUC145.Fill = false;
            this.lineUC145.GroupID = null;
            this.lineUC145.Location = new System.Drawing.Point(1310, 487);
            this.lineUC145.Name = "lineUC145";
            this.lineUC145.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC145.Size = new System.Drawing.Size(3, 60);
            this.lineUC145.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC145.TabIndex = 3097;
            // 
            // lineUC146
            // 
            this.lineUC146.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC146.ExtenderWidth = 3;
            this.lineUC146.Fill = false;
            this.lineUC146.GroupID = null;
            this.lineUC146.Location = new System.Drawing.Point(1260, 487);
            this.lineUC146.Name = "lineUC146";
            this.lineUC146.Size = new System.Drawing.Size(50, 3);
            this.lineUC146.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC146.TabIndex = 3096;
            // 
            // lineUC147
            // 
            this.lineUC147.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC147.ExtenderWidth = 3;
            this.lineUC147.Fill = false;
            this.lineUC147.GroupID = null;
            this.lineUC147.Location = new System.Drawing.Point(1260, 487);
            this.lineUC147.Name = "lineUC147";
            this.lineUC147.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC147.Size = new System.Drawing.Size(3, 60);
            this.lineUC147.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC147.TabIndex = 3095;
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label114.Location = new System.Drawing.Point(1082, 457);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(23, 15);
            this.label114.TabIndex = 3093;
            this.label114.Text = "Q1";
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label115.Location = new System.Drawing.Point(1060, 503);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(21, 15);
            this.label115.TabIndex = 3092;
            this.label115.Text = "K1";
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label116.Location = new System.Drawing.Point(1108, 506);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(21, 15);
            this.label116.TabIndex = 3091;
            this.label116.Text = "K3";
            // 
            // rhombusUC48
            // 
            this.rhombusUC48.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC48.ExtenderWidth = 2;
            this.rhombusUC48.Fill = false;
            this.rhombusUC48.GroupID = null;
            this.rhombusUC48.Location = new System.Drawing.Point(1104, 457);
            this.rhombusUC48.Name = "rhombusUC48";
            this.rhombusUC48.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC48.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC48.TabIndex = 3084;
            // 
            // lineUC148
            // 
            this.lineUC148.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC148.ExtenderWidth = 3;
            this.lineUC148.Fill = false;
            this.lineUC148.GroupID = null;
            this.lineUC148.Location = new System.Drawing.Point(1111, 437);
            this.lineUC148.Name = "lineUC148";
            this.lineUC148.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC148.Size = new System.Drawing.Size(3, 50);
            this.lineUC148.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC148.TabIndex = 3090;
            // 
            // retangleUC68
            // 
            this.retangleUC68.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC68.ExtenderWidth = 3;
            this.retangleUC68.Fill = false;
            this.retangleUC68.GroupID = null;
            this.retangleUC68.Location = new System.Drawing.Point(1131, 503);
            this.retangleUC68.Name = "retangleUC68";
            this.retangleUC68.Size = new System.Drawing.Size(15, 15);
            this.retangleUC68.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC68.TabIndex = 3089;
            // 
            // retangleUC69
            // 
            this.retangleUC69.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC69.ExtenderWidth = 3;
            this.retangleUC69.Fill = false;
            this.retangleUC69.GroupID = null;
            this.retangleUC69.Location = new System.Drawing.Point(1081, 503);
            this.retangleUC69.Name = "retangleUC69";
            this.retangleUC69.Size = new System.Drawing.Size(15, 15);
            this.retangleUC69.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC69.TabIndex = 3088;
            // 
            // lineUC149
            // 
            this.lineUC149.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC149.ExtenderWidth = 3;
            this.lineUC149.Fill = false;
            this.lineUC149.GroupID = null;
            this.lineUC149.Location = new System.Drawing.Point(1137, 487);
            this.lineUC149.Name = "lineUC149";
            this.lineUC149.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC149.Size = new System.Drawing.Size(3, 96);
            this.lineUC149.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC149.TabIndex = 3087;
            // 
            // lineUC150
            // 
            this.lineUC150.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC150.ExtenderWidth = 3;
            this.lineUC150.Fill = false;
            this.lineUC150.GroupID = null;
            this.lineUC150.Location = new System.Drawing.Point(1087, 487);
            this.lineUC150.Name = "lineUC150";
            this.lineUC150.Size = new System.Drawing.Size(50, 3);
            this.lineUC150.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC150.TabIndex = 3086;
            // 
            // lineUC151
            // 
            this.lineUC151.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC151.ExtenderWidth = 3;
            this.lineUC151.Fill = false;
            this.lineUC151.GroupID = null;
            this.lineUC151.Location = new System.Drawing.Point(1087, 487);
            this.lineUC151.Name = "lineUC151";
            this.lineUC151.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC151.Size = new System.Drawing.Size(3, 96);
            this.lineUC151.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC151.TabIndex = 3085;
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label117.Location = new System.Drawing.Point(1147, 503);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(21, 15);
            this.label117.TabIndex = 3083;
            this.label117.Text = "K2";
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label123.Location = new System.Drawing.Point(1169, 457);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(23, 15);
            this.label123.TabIndex = 3082;
            this.label123.Text = "Q2";
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label124.Location = new System.Drawing.Point(1195, 506);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(21, 15);
            this.label124.TabIndex = 3081;
            this.label124.Text = "K4";
            // 
            // rhombusUC53
            // 
            this.rhombusUC53.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC53.ExtenderWidth = 2;
            this.rhombusUC53.Fill = false;
            this.rhombusUC53.GroupID = null;
            this.rhombusUC53.Location = new System.Drawing.Point(1191, 457);
            this.rhombusUC53.Name = "rhombusUC53";
            this.rhombusUC53.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC53.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC53.TabIndex = 3074;
            // 
            // lineUC157
            // 
            this.lineUC157.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC157.ExtenderWidth = 3;
            this.lineUC157.Fill = false;
            this.lineUC157.GroupID = null;
            this.lineUC157.Location = new System.Drawing.Point(1198, 437);
            this.lineUC157.Name = "lineUC157";
            this.lineUC157.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC157.Size = new System.Drawing.Size(3, 50);
            this.lineUC157.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC157.TabIndex = 3080;
            // 
            // retangleUC71
            // 
            this.retangleUC71.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC71.ExtenderWidth = 3;
            this.retangleUC71.Fill = false;
            this.retangleUC71.GroupID = null;
            this.retangleUC71.Location = new System.Drawing.Point(1218, 503);
            this.retangleUC71.Name = "retangleUC71";
            this.retangleUC71.Size = new System.Drawing.Size(15, 15);
            this.retangleUC71.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC71.TabIndex = 3079;
            // 
            // retangleUC72
            // 
            this.retangleUC72.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC72.ExtenderWidth = 3;
            this.retangleUC72.Fill = false;
            this.retangleUC72.GroupID = null;
            this.retangleUC72.Location = new System.Drawing.Point(1168, 503);
            this.retangleUC72.Name = "retangleUC72";
            this.retangleUC72.Size = new System.Drawing.Size(15, 15);
            this.retangleUC72.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC72.TabIndex = 3078;
            // 
            // lineUC158
            // 
            this.lineUC158.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC158.ExtenderWidth = 3;
            this.lineUC158.Fill = false;
            this.lineUC158.GroupID = null;
            this.lineUC158.Location = new System.Drawing.Point(1224, 487);
            this.lineUC158.Name = "lineUC158";
            this.lineUC158.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC158.Size = new System.Drawing.Size(3, 60);
            this.lineUC158.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC158.TabIndex = 3077;
            // 
            // lineUC159
            // 
            this.lineUC159.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC159.ExtenderWidth = 3;
            this.lineUC159.Fill = false;
            this.lineUC159.GroupID = null;
            this.lineUC159.Location = new System.Drawing.Point(1174, 487);
            this.lineUC159.Name = "lineUC159";
            this.lineUC159.Size = new System.Drawing.Size(50, 3);
            this.lineUC159.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC159.TabIndex = 3076;
            // 
            // lineUC160
            // 
            this.lineUC160.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC160.ExtenderWidth = 3;
            this.lineUC160.Fill = false;
            this.lineUC160.GroupID = null;
            this.lineUC160.Location = new System.Drawing.Point(1174, 487);
            this.lineUC160.Name = "lineUC160";
            this.lineUC160.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC160.Size = new System.Drawing.Size(3, 60);
            this.lineUC160.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC160.TabIndex = 3075;
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label125.Location = new System.Drawing.Point(1369, 457);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(23, 15);
            this.label125.TabIndex = 3123;
            this.label125.Text = "Q1";
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label126.Location = new System.Drawing.Point(1347, 503);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(21, 15);
            this.label126.TabIndex = 3122;
            this.label126.Text = "K1";
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label127.Location = new System.Drawing.Point(1395, 506);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(21, 15);
            this.label127.TabIndex = 3121;
            this.label127.Text = "K3";
            // 
            // rhombusUC54
            // 
            this.rhombusUC54.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC54.ExtenderWidth = 2;
            this.rhombusUC54.Fill = false;
            this.rhombusUC54.GroupID = null;
            this.rhombusUC54.Location = new System.Drawing.Point(1391, 457);
            this.rhombusUC54.Name = "rhombusUC54";
            this.rhombusUC54.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC54.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC54.TabIndex = 3114;
            // 
            // lineUC164
            // 
            this.lineUC164.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC164.ExtenderWidth = 3;
            this.lineUC164.Fill = false;
            this.lineUC164.GroupID = null;
            this.lineUC164.Location = new System.Drawing.Point(1398, 437);
            this.lineUC164.Name = "lineUC164";
            this.lineUC164.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC164.Size = new System.Drawing.Size(3, 50);
            this.lineUC164.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC164.TabIndex = 3120;
            // 
            // retangleUC74
            // 
            this.retangleUC74.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC74.ExtenderWidth = 3;
            this.retangleUC74.Fill = false;
            this.retangleUC74.GroupID = null;
            this.retangleUC74.Location = new System.Drawing.Point(1418, 503);
            this.retangleUC74.Name = "retangleUC74";
            this.retangleUC74.Size = new System.Drawing.Size(15, 15);
            this.retangleUC74.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC74.TabIndex = 3119;
            // 
            // retangleUC75
            // 
            this.retangleUC75.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC75.ExtenderWidth = 3;
            this.retangleUC75.Fill = false;
            this.retangleUC75.GroupID = null;
            this.retangleUC75.Location = new System.Drawing.Point(1368, 503);
            this.retangleUC75.Name = "retangleUC75";
            this.retangleUC75.Size = new System.Drawing.Size(15, 15);
            this.retangleUC75.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC75.TabIndex = 3118;
            // 
            // lineUC165
            // 
            this.lineUC165.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC165.ExtenderWidth = 3;
            this.lineUC165.Fill = false;
            this.lineUC165.GroupID = null;
            this.lineUC165.Location = new System.Drawing.Point(1424, 487);
            this.lineUC165.Name = "lineUC165";
            this.lineUC165.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC165.Size = new System.Drawing.Size(3, 96);
            this.lineUC165.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC165.TabIndex = 3117;
            // 
            // lineUC166
            // 
            this.lineUC166.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC166.ExtenderWidth = 3;
            this.lineUC166.Fill = false;
            this.lineUC166.GroupID = null;
            this.lineUC166.Location = new System.Drawing.Point(1374, 487);
            this.lineUC166.Name = "lineUC166";
            this.lineUC166.Size = new System.Drawing.Size(50, 3);
            this.lineUC166.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC166.TabIndex = 3116;
            // 
            // lineUC167
            // 
            this.lineUC167.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC167.ExtenderWidth = 3;
            this.lineUC167.Fill = false;
            this.lineUC167.GroupID = null;
            this.lineUC167.Location = new System.Drawing.Point(1374, 487);
            this.lineUC167.Name = "lineUC167";
            this.lineUC167.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC167.Size = new System.Drawing.Size(3, 96);
            this.lineUC167.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC167.TabIndex = 3115;
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label128.Location = new System.Drawing.Point(1434, 503);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(21, 15);
            this.label128.TabIndex = 3113;
            this.label128.Text = "K2";
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label129.Location = new System.Drawing.Point(1456, 457);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(23, 15);
            this.label129.TabIndex = 3112;
            this.label129.Text = "Q2";
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label130.Location = new System.Drawing.Point(1482, 506);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(21, 15);
            this.label130.TabIndex = 3111;
            this.label130.Text = "K4";
            // 
            // rhombusUC55
            // 
            this.rhombusUC55.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC55.ExtenderWidth = 2;
            this.rhombusUC55.Fill = false;
            this.rhombusUC55.GroupID = null;
            this.rhombusUC55.Location = new System.Drawing.Point(1478, 457);
            this.rhombusUC55.Name = "rhombusUC55";
            this.rhombusUC55.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC55.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC55.TabIndex = 3104;
            // 
            // lineUC168
            // 
            this.lineUC168.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC168.ExtenderWidth = 3;
            this.lineUC168.Fill = false;
            this.lineUC168.GroupID = null;
            this.lineUC168.Location = new System.Drawing.Point(1485, 437);
            this.lineUC168.Name = "lineUC168";
            this.lineUC168.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC168.Size = new System.Drawing.Size(3, 50);
            this.lineUC168.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC168.TabIndex = 3110;
            // 
            // retangleUC76
            // 
            this.retangleUC76.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC76.ExtenderWidth = 3;
            this.retangleUC76.Fill = false;
            this.retangleUC76.GroupID = null;
            this.retangleUC76.Location = new System.Drawing.Point(1505, 503);
            this.retangleUC76.Name = "retangleUC76";
            this.retangleUC76.Size = new System.Drawing.Size(15, 15);
            this.retangleUC76.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC76.TabIndex = 3109;
            // 
            // retangleUC77
            // 
            this.retangleUC77.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC77.ExtenderWidth = 3;
            this.retangleUC77.Fill = false;
            this.retangleUC77.GroupID = null;
            this.retangleUC77.Location = new System.Drawing.Point(1455, 503);
            this.retangleUC77.Name = "retangleUC77";
            this.retangleUC77.Size = new System.Drawing.Size(15, 15);
            this.retangleUC77.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC77.TabIndex = 3108;
            // 
            // lineUC169
            // 
            this.lineUC169.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC169.ExtenderWidth = 3;
            this.lineUC169.Fill = false;
            this.lineUC169.GroupID = null;
            this.lineUC169.Location = new System.Drawing.Point(1511, 487);
            this.lineUC169.Name = "lineUC169";
            this.lineUC169.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC169.Size = new System.Drawing.Size(3, 60);
            this.lineUC169.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC169.TabIndex = 3107;
            // 
            // lineUC170
            // 
            this.lineUC170.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC170.ExtenderWidth = 3;
            this.lineUC170.Fill = false;
            this.lineUC170.GroupID = null;
            this.lineUC170.Location = new System.Drawing.Point(1461, 487);
            this.lineUC170.Name = "lineUC170";
            this.lineUC170.Size = new System.Drawing.Size(50, 3);
            this.lineUC170.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC170.TabIndex = 3106;
            // 
            // lineUC171
            // 
            this.lineUC171.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC171.ExtenderWidth = 3;
            this.lineUC171.Fill = false;
            this.lineUC171.GroupID = null;
            this.lineUC171.Location = new System.Drawing.Point(1461, 487);
            this.lineUC171.Name = "lineUC171";
            this.lineUC171.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC171.Size = new System.Drawing.Size(3, 60);
            this.lineUC171.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC171.TabIndex = 3105;
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label131.Location = new System.Drawing.Point(1717, 457);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(23, 15);
            this.label131.TabIndex = 3153;
            this.label131.Text = "Q1";
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label132.Location = new System.Drawing.Point(1695, 503);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(21, 15);
            this.label132.TabIndex = 3152;
            this.label132.Text = "K1";
            // 
            // label133
            // 
            this.label133.AutoSize = true;
            this.label133.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label133.Location = new System.Drawing.Point(1743, 506);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(21, 15);
            this.label133.TabIndex = 3151;
            this.label133.Text = "K3";
            // 
            // rhombusUC56
            // 
            this.rhombusUC56.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC56.ExtenderWidth = 2;
            this.rhombusUC56.Fill = false;
            this.rhombusUC56.GroupID = null;
            this.rhombusUC56.Location = new System.Drawing.Point(1739, 457);
            this.rhombusUC56.Name = "rhombusUC56";
            this.rhombusUC56.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC56.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC56.TabIndex = 3144;
            // 
            // lineUC172
            // 
            this.lineUC172.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC172.ExtenderWidth = 3;
            this.lineUC172.Fill = false;
            this.lineUC172.GroupID = null;
            this.lineUC172.Location = new System.Drawing.Point(1746, 437);
            this.lineUC172.Name = "lineUC172";
            this.lineUC172.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC172.Size = new System.Drawing.Size(3, 50);
            this.lineUC172.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC172.TabIndex = 3150;
            // 
            // retangleUC78
            // 
            this.retangleUC78.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC78.ExtenderWidth = 3;
            this.retangleUC78.Fill = false;
            this.retangleUC78.GroupID = null;
            this.retangleUC78.Location = new System.Drawing.Point(1766, 503);
            this.retangleUC78.Name = "retangleUC78";
            this.retangleUC78.Size = new System.Drawing.Size(15, 15);
            this.retangleUC78.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC78.TabIndex = 3149;
            // 
            // retangleUC79
            // 
            this.retangleUC79.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC79.ExtenderWidth = 3;
            this.retangleUC79.Fill = false;
            this.retangleUC79.GroupID = null;
            this.retangleUC79.Location = new System.Drawing.Point(1716, 503);
            this.retangleUC79.Name = "retangleUC79";
            this.retangleUC79.Size = new System.Drawing.Size(15, 15);
            this.retangleUC79.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC79.TabIndex = 3148;
            // 
            // lineUC173
            // 
            this.lineUC173.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC173.ExtenderWidth = 3;
            this.lineUC173.Fill = false;
            this.lineUC173.GroupID = null;
            this.lineUC173.Location = new System.Drawing.Point(1772, 487);
            this.lineUC173.Name = "lineUC173";
            this.lineUC173.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC173.Size = new System.Drawing.Size(3, 96);
            this.lineUC173.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC173.TabIndex = 3147;
            // 
            // lineUC174
            // 
            this.lineUC174.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC174.ExtenderWidth = 3;
            this.lineUC174.Fill = false;
            this.lineUC174.GroupID = null;
            this.lineUC174.Location = new System.Drawing.Point(1722, 487);
            this.lineUC174.Name = "lineUC174";
            this.lineUC174.Size = new System.Drawing.Size(50, 3);
            this.lineUC174.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC174.TabIndex = 3146;
            // 
            // lineUC175
            // 
            this.lineUC175.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC175.ExtenderWidth = 3;
            this.lineUC175.Fill = false;
            this.lineUC175.GroupID = null;
            this.lineUC175.Location = new System.Drawing.Point(1722, 487);
            this.lineUC175.Name = "lineUC175";
            this.lineUC175.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC175.Size = new System.Drawing.Size(3, 96);
            this.lineUC175.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC175.TabIndex = 3145;
            // 
            // label134
            // 
            this.label134.AutoSize = true;
            this.label134.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label134.Location = new System.Drawing.Point(1543, 457);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(23, 15);
            this.label134.TabIndex = 3143;
            this.label134.Text = "Q1";
            // 
            // label135
            // 
            this.label135.AutoSize = true;
            this.label135.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label135.Location = new System.Drawing.Point(1521, 503);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(21, 15);
            this.label135.TabIndex = 3142;
            this.label135.Text = "K1";
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label136.Location = new System.Drawing.Point(1569, 506);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(21, 15);
            this.label136.TabIndex = 3141;
            this.label136.Text = "K3";
            // 
            // rhombusUC57
            // 
            this.rhombusUC57.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC57.ExtenderWidth = 2;
            this.rhombusUC57.Fill = false;
            this.rhombusUC57.GroupID = null;
            this.rhombusUC57.Location = new System.Drawing.Point(1565, 457);
            this.rhombusUC57.Name = "rhombusUC57";
            this.rhombusUC57.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC57.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC57.TabIndex = 3134;
            // 
            // lineUC176
            // 
            this.lineUC176.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC176.ExtenderWidth = 3;
            this.lineUC176.Fill = false;
            this.lineUC176.GroupID = null;
            this.lineUC176.Location = new System.Drawing.Point(1572, 437);
            this.lineUC176.Name = "lineUC176";
            this.lineUC176.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC176.Size = new System.Drawing.Size(3, 50);
            this.lineUC176.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC176.TabIndex = 3140;
            // 
            // retangleUC80
            // 
            this.retangleUC80.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC80.ExtenderWidth = 3;
            this.retangleUC80.Fill = false;
            this.retangleUC80.GroupID = null;
            this.retangleUC80.Location = new System.Drawing.Point(1592, 503);
            this.retangleUC80.Name = "retangleUC80";
            this.retangleUC80.Size = new System.Drawing.Size(15, 15);
            this.retangleUC80.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC80.TabIndex = 3139;
            // 
            // retangleUC81
            // 
            this.retangleUC81.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC81.ExtenderWidth = 3;
            this.retangleUC81.Fill = false;
            this.retangleUC81.GroupID = null;
            this.retangleUC81.Location = new System.Drawing.Point(1542, 503);
            this.retangleUC81.Name = "retangleUC81";
            this.retangleUC81.Size = new System.Drawing.Size(15, 15);
            this.retangleUC81.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC81.TabIndex = 3138;
            // 
            // lineUC177
            // 
            this.lineUC177.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC177.ExtenderWidth = 3;
            this.lineUC177.Fill = false;
            this.lineUC177.GroupID = null;
            this.lineUC177.Location = new System.Drawing.Point(1598, 487);
            this.lineUC177.Name = "lineUC177";
            this.lineUC177.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC177.Size = new System.Drawing.Size(3, 96);
            this.lineUC177.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC177.TabIndex = 3137;
            // 
            // lineUC178
            // 
            this.lineUC178.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC178.ExtenderWidth = 3;
            this.lineUC178.Fill = false;
            this.lineUC178.GroupID = null;
            this.lineUC178.Location = new System.Drawing.Point(1548, 487);
            this.lineUC178.Name = "lineUC178";
            this.lineUC178.Size = new System.Drawing.Size(50, 3);
            this.lineUC178.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC178.TabIndex = 3136;
            // 
            // lineUC179
            // 
            this.lineUC179.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC179.ExtenderWidth = 3;
            this.lineUC179.Fill = false;
            this.lineUC179.GroupID = null;
            this.lineUC179.Location = new System.Drawing.Point(1548, 487);
            this.lineUC179.Name = "lineUC179";
            this.lineUC179.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC179.Size = new System.Drawing.Size(3, 96);
            this.lineUC179.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC179.TabIndex = 3135;
            // 
            // label137
            // 
            this.label137.AutoSize = true;
            this.label137.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label137.Location = new System.Drawing.Point(1608, 503);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(21, 15);
            this.label137.TabIndex = 3133;
            this.label137.Text = "K2";
            // 
            // label138
            // 
            this.label138.AutoSize = true;
            this.label138.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label138.Location = new System.Drawing.Point(1630, 457);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(23, 15);
            this.label138.TabIndex = 3132;
            this.label138.Text = "Q2";
            // 
            // label139
            // 
            this.label139.AutoSize = true;
            this.label139.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label139.Location = new System.Drawing.Point(1656, 506);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(21, 15);
            this.label139.TabIndex = 3131;
            this.label139.Text = "K4";
            // 
            // rhombusUC58
            // 
            this.rhombusUC58.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC58.ExtenderWidth = 2;
            this.rhombusUC58.Fill = false;
            this.rhombusUC58.GroupID = null;
            this.rhombusUC58.Location = new System.Drawing.Point(1652, 457);
            this.rhombusUC58.Name = "rhombusUC58";
            this.rhombusUC58.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC58.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC58.TabIndex = 3124;
            // 
            // lineUC180
            // 
            this.lineUC180.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC180.ExtenderWidth = 3;
            this.lineUC180.Fill = false;
            this.lineUC180.GroupID = null;
            this.lineUC180.Location = new System.Drawing.Point(1659, 437);
            this.lineUC180.Name = "lineUC180";
            this.lineUC180.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC180.Size = new System.Drawing.Size(3, 50);
            this.lineUC180.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC180.TabIndex = 3130;
            // 
            // retangleUC82
            // 
            this.retangleUC82.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC82.ExtenderWidth = 3;
            this.retangleUC82.Fill = false;
            this.retangleUC82.GroupID = null;
            this.retangleUC82.Location = new System.Drawing.Point(1679, 503);
            this.retangleUC82.Name = "retangleUC82";
            this.retangleUC82.Size = new System.Drawing.Size(15, 15);
            this.retangleUC82.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC82.TabIndex = 3129;
            // 
            // retangleUC83
            // 
            this.retangleUC83.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC83.ExtenderWidth = 3;
            this.retangleUC83.Fill = false;
            this.retangleUC83.GroupID = null;
            this.retangleUC83.Location = new System.Drawing.Point(1629, 503);
            this.retangleUC83.Name = "retangleUC83";
            this.retangleUC83.Size = new System.Drawing.Size(15, 15);
            this.retangleUC83.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC83.TabIndex = 3128;
            // 
            // lineUC181
            // 
            this.lineUC181.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC181.ExtenderWidth = 3;
            this.lineUC181.Fill = false;
            this.lineUC181.GroupID = null;
            this.lineUC181.Location = new System.Drawing.Point(1685, 487);
            this.lineUC181.Name = "lineUC181";
            this.lineUC181.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC181.Size = new System.Drawing.Size(3, 60);
            this.lineUC181.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC181.TabIndex = 3127;
            // 
            // lineUC182
            // 
            this.lineUC182.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC182.ExtenderWidth = 3;
            this.lineUC182.Fill = false;
            this.lineUC182.GroupID = null;
            this.lineUC182.Location = new System.Drawing.Point(1635, 487);
            this.lineUC182.Name = "lineUC182";
            this.lineUC182.Size = new System.Drawing.Size(50, 3);
            this.lineUC182.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC182.TabIndex = 3126;
            // 
            // lineUC183
            // 
            this.lineUC183.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC183.ExtenderWidth = 3;
            this.lineUC183.Fill = false;
            this.lineUC183.GroupID = null;
            this.lineUC183.Location = new System.Drawing.Point(1635, 487);
            this.lineUC183.Name = "lineUC183";
            this.lineUC183.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC183.Size = new System.Drawing.Size(3, 60);
            this.lineUC183.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC183.TabIndex = 3125;
            // 
            // label197
            // 
            this.label197.AutoSize = true;
            this.label197.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label197.Location = new System.Drawing.Point(1600, 715);
            this.label197.Name = "label197";
            this.label197.Size = new System.Drawing.Size(39, 15);
            this.label197.TabIndex = 3187;
            this.label197.Text = "MC41";
            // 
            // label198
            // 
            this.label198.AutoSize = true;
            this.label198.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label198.Location = new System.Drawing.Point(1659, 678);
            this.label198.Name = "label198";
            this.label198.Size = new System.Drawing.Size(39, 15);
            this.label198.TabIndex = 3186;
            this.label198.Text = "MC22";
            // 
            // label199
            // 
            this.label199.AutoSize = true;
            this.label199.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label199.Location = new System.Drawing.Point(1543, 678);
            this.label199.Name = "label199";
            this.label199.Size = new System.Drawing.Size(39, 15);
            this.label199.TabIndex = 3185;
            this.label199.Text = "MC21";
            // 
            // label200
            // 
            this.label200.AutoSize = true;
            this.label200.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label200.Location = new System.Drawing.Point(1625, 613);
            this.label200.Name = "label200";
            this.label200.Size = new System.Drawing.Size(40, 15);
            this.label200.TabIndex = 3184;
            this.label200.Text = "MG01";
            // 
            // rhombusUC78
            // 
            this.rhombusUC78.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC78.ExtenderWidth = 2;
            this.rhombusUC78.Fill = false;
            this.rhombusUC78.GroupID = null;
            this.rhombusUC78.Location = new System.Drawing.Point(1609, 696);
            this.rhombusUC78.Name = "rhombusUC78";
            this.rhombusUC78.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC78.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC78.TabIndex = 3183;
            // 
            // rhombusUC79
            // 
            this.rhombusUC79.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC79.ExtenderWidth = 2;
            this.rhombusUC79.Fill = false;
            this.rhombusUC79.GroupID = null;
            this.rhombusUC79.Location = new System.Drawing.Point(1641, 677);
            this.rhombusUC79.Name = "rhombusUC79";
            this.rhombusUC79.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC79.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC79.TabIndex = 3182;
            // 
            // rhombusUC80
            // 
            this.rhombusUC80.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC80.ExtenderWidth = 2;
            this.rhombusUC80.Fill = false;
            this.rhombusUC80.GroupID = null;
            this.rhombusUC80.Location = new System.Drawing.Point(1582, 677);
            this.rhombusUC80.Name = "rhombusUC80";
            this.rhombusUC80.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC80.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC80.TabIndex = 3181;
            // 
            // lineUC260
            // 
            this.lineUC260.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC260.ExtenderWidth = 3;
            this.lineUC260.Fill = false;
            this.lineUC260.GroupID = null;
            this.lineUC260.Location = new System.Drawing.Point(1589, 705);
            this.lineUC260.Name = "lineUC260";
            this.lineUC260.Size = new System.Drawing.Size(60, 3);
            this.lineUC260.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC260.TabIndex = 3180;
            // 
            // rhombusUC81
            // 
            this.rhombusUC81.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC81.ExtenderWidth = 2;
            this.rhombusUC81.Fill = false;
            this.rhombusUC81.GroupID = null;
            this.rhombusUC81.Location = new System.Drawing.Point(1609, 638);
            this.rhombusUC81.Name = "rhombusUC81";
            this.rhombusUC81.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC81.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC81.TabIndex = 3175;
            // 
            // lineUC261
            // 
            this.lineUC261.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC261.ExtenderWidth = 3;
            this.lineUC261.Fill = false;
            this.lineUC261.GroupID = null;
            this.lineUC261.Location = new System.Drawing.Point(1616, 628);
            this.lineUC261.Name = "lineUC261";
            this.lineUC261.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC261.Size = new System.Drawing.Size(3, 40);
            this.lineUC261.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC261.TabIndex = 3179;
            // 
            // lineUC262
            // 
            this.lineUC262.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC262.ExtenderWidth = 3;
            this.lineUC262.Fill = false;
            this.lineUC262.GroupID = null;
            this.lineUC262.Location = new System.Drawing.Point(1647, 665);
            this.lineUC262.Name = "lineUC262";
            this.lineUC262.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC262.Size = new System.Drawing.Size(3, 70);
            this.lineUC262.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC262.TabIndex = 3178;
            // 
            // lineUC263
            // 
            this.lineUC263.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC263.ExtenderWidth = 3;
            this.lineUC263.Fill = false;
            this.lineUC263.GroupID = null;
            this.lineUC263.Location = new System.Drawing.Point(1589, 665);
            this.lineUC263.Name = "lineUC263";
            this.lineUC263.Size = new System.Drawing.Size(60, 3);
            this.lineUC263.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC263.TabIndex = 3177;
            // 
            // lineUC264
            // 
            this.lineUC264.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC264.ExtenderWidth = 3;
            this.lineUC264.Fill = false;
            this.lineUC264.GroupID = null;
            this.lineUC264.Location = new System.Drawing.Point(1589, 665);
            this.lineUC264.Name = "lineUC264";
            this.lineUC264.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC264.Size = new System.Drawing.Size(3, 70);
            this.lineUC264.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC264.TabIndex = 3176;
            // 
            // retangleUC122
            // 
            this.retangleUC122.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC122.ExtenderWidth = 3;
            this.retangleUC122.Fill = false;
            this.retangleUC122.GroupID = null;
            this.retangleUC122.Location = new System.Drawing.Point(1610, 613);
            this.retangleUC122.Name = "retangleUC122";
            this.retangleUC122.Size = new System.Drawing.Size(15, 15);
            this.retangleUC122.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC122.TabIndex = 3174;
            // 
            // label201
            // 
            this.label201.AutoSize = true;
            this.label201.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label201.Location = new System.Drawing.Point(1627, 639);
            this.label201.Name = "label201";
            this.label201.Size = new System.Drawing.Size(39, 15);
            this.label201.TabIndex = 3173;
            this.label201.Text = "MC81";
            // 
            // label202
            // 
            this.label202.AutoSize = true;
            this.label202.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label202.Location = new System.Drawing.Point(585, 715);
            this.label202.Name = "label202";
            this.label202.Size = new System.Drawing.Size(39, 15);
            this.label202.TabIndex = 3172;
            this.label202.Text = "MC41";
            // 
            // label203
            // 
            this.label203.AutoSize = true;
            this.label203.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label203.Location = new System.Drawing.Point(644, 678);
            this.label203.Name = "label203";
            this.label203.Size = new System.Drawing.Size(39, 15);
            this.label203.TabIndex = 3171;
            this.label203.Text = "MC22";
            // 
            // label204
            // 
            this.label204.AutoSize = true;
            this.label204.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label204.Location = new System.Drawing.Point(528, 678);
            this.label204.Name = "label204";
            this.label204.Size = new System.Drawing.Size(39, 15);
            this.label204.TabIndex = 3170;
            this.label204.Text = "MC21";
            // 
            // label205
            // 
            this.label205.AutoSize = true;
            this.label205.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label205.Location = new System.Drawing.Point(610, 613);
            this.label205.Name = "label205";
            this.label205.Size = new System.Drawing.Size(40, 15);
            this.label205.TabIndex = 3169;
            this.label205.Text = "MG01";
            // 
            // rhombusUC82
            // 
            this.rhombusUC82.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC82.ExtenderWidth = 2;
            this.rhombusUC82.Fill = false;
            this.rhombusUC82.GroupID = null;
            this.rhombusUC82.Location = new System.Drawing.Point(594, 696);
            this.rhombusUC82.Name = "rhombusUC82";
            this.rhombusUC82.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC82.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC82.TabIndex = 3168;
            // 
            // rhombusUC83
            // 
            this.rhombusUC83.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC83.ExtenderWidth = 2;
            this.rhombusUC83.Fill = false;
            this.rhombusUC83.GroupID = null;
            this.rhombusUC83.Location = new System.Drawing.Point(626, 677);
            this.rhombusUC83.Name = "rhombusUC83";
            this.rhombusUC83.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC83.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC83.TabIndex = 3167;
            // 
            // rhombusUC84
            // 
            this.rhombusUC84.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC84.ExtenderWidth = 2;
            this.rhombusUC84.Fill = false;
            this.rhombusUC84.GroupID = null;
            this.rhombusUC84.Location = new System.Drawing.Point(567, 677);
            this.rhombusUC84.Name = "rhombusUC84";
            this.rhombusUC84.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC84.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC84.TabIndex = 3166;
            // 
            // lineUC265
            // 
            this.lineUC265.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC265.ExtenderWidth = 3;
            this.lineUC265.Fill = false;
            this.lineUC265.GroupID = null;
            this.lineUC265.Location = new System.Drawing.Point(574, 705);
            this.lineUC265.Name = "lineUC265";
            this.lineUC265.Size = new System.Drawing.Size(60, 3);
            this.lineUC265.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC265.TabIndex = 3165;
            // 
            // rhombusUC85
            // 
            this.rhombusUC85.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC85.ExtenderWidth = 2;
            this.rhombusUC85.Fill = false;
            this.rhombusUC85.GroupID = null;
            this.rhombusUC85.Location = new System.Drawing.Point(594, 638);
            this.rhombusUC85.Name = "rhombusUC85";
            this.rhombusUC85.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC85.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC85.TabIndex = 3160;
            // 
            // lineUC266
            // 
            this.lineUC266.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC266.ExtenderWidth = 3;
            this.lineUC266.Fill = false;
            this.lineUC266.GroupID = null;
            this.lineUC266.Location = new System.Drawing.Point(601, 628);
            this.lineUC266.Name = "lineUC266";
            this.lineUC266.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC266.Size = new System.Drawing.Size(3, 40);
            this.lineUC266.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC266.TabIndex = 3164;
            // 
            // lineUC267
            // 
            this.lineUC267.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC267.ExtenderWidth = 3;
            this.lineUC267.Fill = false;
            this.lineUC267.GroupID = null;
            this.lineUC267.Location = new System.Drawing.Point(632, 665);
            this.lineUC267.Name = "lineUC267";
            this.lineUC267.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC267.Size = new System.Drawing.Size(3, 70);
            this.lineUC267.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC267.TabIndex = 3163;
            // 
            // lineUC268
            // 
            this.lineUC268.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC268.ExtenderWidth = 3;
            this.lineUC268.Fill = false;
            this.lineUC268.GroupID = null;
            this.lineUC268.Location = new System.Drawing.Point(574, 665);
            this.lineUC268.Name = "lineUC268";
            this.lineUC268.Size = new System.Drawing.Size(60, 3);
            this.lineUC268.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC268.TabIndex = 3162;
            // 
            // lineUC269
            // 
            this.lineUC269.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC269.ExtenderWidth = 3;
            this.lineUC269.Fill = false;
            this.lineUC269.GroupID = null;
            this.lineUC269.Location = new System.Drawing.Point(574, 665);
            this.lineUC269.Name = "lineUC269";
            this.lineUC269.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC269.Size = new System.Drawing.Size(3, 70);
            this.lineUC269.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC269.TabIndex = 3161;
            // 
            // retangleUC123
            // 
            this.retangleUC123.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC123.ExtenderWidth = 3;
            this.retangleUC123.Fill = false;
            this.retangleUC123.GroupID = null;
            this.retangleUC123.Location = new System.Drawing.Point(595, 613);
            this.retangleUC123.Name = "retangleUC123";
            this.retangleUC123.Size = new System.Drawing.Size(15, 15);
            this.retangleUC123.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC123.TabIndex = 3159;
            // 
            // label206
            // 
            this.label206.AutoSize = true;
            this.label206.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label206.Location = new System.Drawing.Point(612, 639);
            this.label206.Name = "label206";
            this.label206.Size = new System.Drawing.Size(39, 15);
            this.label206.TabIndex = 3158;
            this.label206.Text = "MC81";
            // 
            // retangleUC124
            // 
            this.retangleUC124.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC124.ExtenderWidth = 3;
            this.retangleUC124.Fill = false;
            this.retangleUC124.GroupID = null;
            this.retangleUC124.Location = new System.Drawing.Point(36, 880);
            this.retangleUC124.Name = "retangleUC124";
            this.retangleUC124.Size = new System.Drawing.Size(65, 16);
            this.retangleUC124.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC124.TabIndex = 3157;
            // 
            // lineUC270
            // 
            this.lineUC270.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC270.ExtenderWidth = 3;
            this.lineUC270.Fill = false;
            this.lineUC270.GroupID = null;
            this.lineUC270.Location = new System.Drawing.Point(0, 893);
            this.lineUC270.Name = "lineUC270";
            this.lineUC270.Size = new System.Drawing.Size(1900, 3);
            this.lineUC270.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC270.TabIndex = 3156;
            // 
            // lineUC271
            // 
            this.lineUC271.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC271.ExtenderWidth = 3;
            this.lineUC271.Fill = false;
            this.lineUC271.GroupID = null;
            this.lineUC271.Location = new System.Drawing.Point(29, 857);
            this.lineUC271.Name = "lineUC271";
            this.lineUC271.Size = new System.Drawing.Size(1900, 3);
            this.lineUC271.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC271.TabIndex = 3155;
            // 
            // lineUC272
            // 
            this.lineUC272.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC272.ExtenderWidth = 3;
            this.lineUC272.Fill = false;
            this.lineUC272.GroupID = null;
            this.lineUC272.Location = new System.Drawing.Point(14, 732);
            this.lineUC272.Name = "lineUC272";
            this.lineUC272.Size = new System.Drawing.Size(560, 3);
            this.lineUC272.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC272.TabIndex = 3154;
            // 
            // label140
            // 
            this.label140.AutoSize = true;
            this.label140.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label140.Location = new System.Drawing.Point(210, 754);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(23, 15);
            this.label140.TabIndex = 3217;
            this.label140.Text = "Q1";
            // 
            // label141
            // 
            this.label141.AutoSize = true;
            this.label141.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label141.Location = new System.Drawing.Point(188, 800);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(21, 15);
            this.label141.TabIndex = 3216;
            this.label141.Text = "K1";
            // 
            // label142
            // 
            this.label142.AutoSize = true;
            this.label142.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label142.Location = new System.Drawing.Point(236, 803);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(21, 15);
            this.label142.TabIndex = 3215;
            this.label142.Text = "K3";
            // 
            // rhombusUC59
            // 
            this.rhombusUC59.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC59.ExtenderWidth = 2;
            this.rhombusUC59.Fill = false;
            this.rhombusUC59.GroupID = null;
            this.rhombusUC59.Location = new System.Drawing.Point(232, 754);
            this.rhombusUC59.Name = "rhombusUC59";
            this.rhombusUC59.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC59.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC59.TabIndex = 3208;
            // 
            // lineUC184
            // 
            this.lineUC184.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC184.ExtenderWidth = 3;
            this.lineUC184.Fill = false;
            this.lineUC184.GroupID = null;
            this.lineUC184.Location = new System.Drawing.Point(239, 734);
            this.lineUC184.Name = "lineUC184";
            this.lineUC184.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC184.Size = new System.Drawing.Size(3, 50);
            this.lineUC184.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC184.TabIndex = 3214;
            // 
            // retangleUC84
            // 
            this.retangleUC84.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC84.ExtenderWidth = 3;
            this.retangleUC84.Fill = false;
            this.retangleUC84.GroupID = null;
            this.retangleUC84.Location = new System.Drawing.Point(259, 800);
            this.retangleUC84.Name = "retangleUC84";
            this.retangleUC84.Size = new System.Drawing.Size(15, 15);
            this.retangleUC84.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC84.TabIndex = 3213;
            // 
            // retangleUC85
            // 
            this.retangleUC85.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC85.ExtenderWidth = 3;
            this.retangleUC85.Fill = false;
            this.retangleUC85.GroupID = null;
            this.retangleUC85.Location = new System.Drawing.Point(209, 800);
            this.retangleUC85.Name = "retangleUC85";
            this.retangleUC85.Size = new System.Drawing.Size(15, 15);
            this.retangleUC85.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC85.TabIndex = 3212;
            // 
            // lineUC185
            // 
            this.lineUC185.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC185.ExtenderWidth = 3;
            this.lineUC185.Fill = false;
            this.lineUC185.GroupID = null;
            this.lineUC185.Location = new System.Drawing.Point(265, 784);
            this.lineUC185.Name = "lineUC185";
            this.lineUC185.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC185.Size = new System.Drawing.Size(3, 96);
            this.lineUC185.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC185.TabIndex = 3211;
            // 
            // lineUC186
            // 
            this.lineUC186.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC186.ExtenderWidth = 3;
            this.lineUC186.Fill = false;
            this.lineUC186.GroupID = null;
            this.lineUC186.Location = new System.Drawing.Point(215, 784);
            this.lineUC186.Name = "lineUC186";
            this.lineUC186.Size = new System.Drawing.Size(50, 3);
            this.lineUC186.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC186.TabIndex = 3210;
            // 
            // lineUC187
            // 
            this.lineUC187.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC187.ExtenderWidth = 3;
            this.lineUC187.Fill = false;
            this.lineUC187.GroupID = null;
            this.lineUC187.Location = new System.Drawing.Point(215, 784);
            this.lineUC187.Name = "lineUC187";
            this.lineUC187.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC187.Size = new System.Drawing.Size(3, 96);
            this.lineUC187.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC187.TabIndex = 3209;
            // 
            // label143
            // 
            this.label143.AutoSize = true;
            this.label143.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label143.Location = new System.Drawing.Point(36, 754);
            this.label143.Name = "label143";
            this.label143.Size = new System.Drawing.Size(23, 15);
            this.label143.TabIndex = 3207;
            this.label143.Text = "Q1";
            // 
            // label144
            // 
            this.label144.AutoSize = true;
            this.label144.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label144.Location = new System.Drawing.Point(14, 800);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(21, 15);
            this.label144.TabIndex = 3206;
            this.label144.Text = "K1";
            // 
            // label145
            // 
            this.label145.AutoSize = true;
            this.label145.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label145.Location = new System.Drawing.Point(62, 803);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(21, 15);
            this.label145.TabIndex = 3205;
            this.label145.Text = "K3";
            // 
            // rhombusUC60
            // 
            this.rhombusUC60.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC60.ExtenderWidth = 2;
            this.rhombusUC60.Fill = false;
            this.rhombusUC60.GroupID = null;
            this.rhombusUC60.Location = new System.Drawing.Point(58, 754);
            this.rhombusUC60.Name = "rhombusUC60";
            this.rhombusUC60.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC60.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC60.TabIndex = 3198;
            // 
            // lineUC188
            // 
            this.lineUC188.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC188.ExtenderWidth = 3;
            this.lineUC188.Fill = false;
            this.lineUC188.GroupID = null;
            this.lineUC188.Location = new System.Drawing.Point(65, 734);
            this.lineUC188.Name = "lineUC188";
            this.lineUC188.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC188.Size = new System.Drawing.Size(3, 50);
            this.lineUC188.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC188.TabIndex = 3204;
            // 
            // retangleUC86
            // 
            this.retangleUC86.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC86.ExtenderWidth = 3;
            this.retangleUC86.Fill = false;
            this.retangleUC86.GroupID = null;
            this.retangleUC86.Location = new System.Drawing.Point(85, 800);
            this.retangleUC86.Name = "retangleUC86";
            this.retangleUC86.Size = new System.Drawing.Size(15, 15);
            this.retangleUC86.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC86.TabIndex = 3203;
            // 
            // retangleUC87
            // 
            this.retangleUC87.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC87.ExtenderWidth = 3;
            this.retangleUC87.Fill = false;
            this.retangleUC87.GroupID = null;
            this.retangleUC87.Location = new System.Drawing.Point(35, 800);
            this.retangleUC87.Name = "retangleUC87";
            this.retangleUC87.Size = new System.Drawing.Size(15, 15);
            this.retangleUC87.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC87.TabIndex = 3202;
            // 
            // lineUC189
            // 
            this.lineUC189.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC189.ExtenderWidth = 3;
            this.lineUC189.Fill = false;
            this.lineUC189.GroupID = null;
            this.lineUC189.Location = new System.Drawing.Point(91, 784);
            this.lineUC189.Name = "lineUC189";
            this.lineUC189.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC189.Size = new System.Drawing.Size(3, 96);
            this.lineUC189.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC189.TabIndex = 3201;
            // 
            // lineUC190
            // 
            this.lineUC190.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC190.ExtenderWidth = 3;
            this.lineUC190.Fill = false;
            this.lineUC190.GroupID = null;
            this.lineUC190.Location = new System.Drawing.Point(41, 784);
            this.lineUC190.Name = "lineUC190";
            this.lineUC190.Size = new System.Drawing.Size(50, 3);
            this.lineUC190.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC190.TabIndex = 3200;
            // 
            // lineUC191
            // 
            this.lineUC191.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC191.ExtenderWidth = 3;
            this.lineUC191.Fill = false;
            this.lineUC191.GroupID = null;
            this.lineUC191.Location = new System.Drawing.Point(41, 784);
            this.lineUC191.Name = "lineUC191";
            this.lineUC191.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC191.Size = new System.Drawing.Size(3, 96);
            this.lineUC191.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC191.TabIndex = 3199;
            // 
            // label146
            // 
            this.label146.AutoSize = true;
            this.label146.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label146.Location = new System.Drawing.Point(101, 800);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(21, 15);
            this.label146.TabIndex = 3197;
            this.label146.Text = "K2";
            // 
            // label147
            // 
            this.label147.AutoSize = true;
            this.label147.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label147.Location = new System.Drawing.Point(123, 754);
            this.label147.Name = "label147";
            this.label147.Size = new System.Drawing.Size(23, 15);
            this.label147.TabIndex = 3196;
            this.label147.Text = "Q2";
            // 
            // label148
            // 
            this.label148.AutoSize = true;
            this.label148.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label148.Location = new System.Drawing.Point(149, 803);
            this.label148.Name = "label148";
            this.label148.Size = new System.Drawing.Size(21, 15);
            this.label148.TabIndex = 3195;
            this.label148.Text = "K4";
            // 
            // rhombusUC61
            // 
            this.rhombusUC61.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC61.ExtenderWidth = 2;
            this.rhombusUC61.Fill = false;
            this.rhombusUC61.GroupID = null;
            this.rhombusUC61.Location = new System.Drawing.Point(145, 754);
            this.rhombusUC61.Name = "rhombusUC61";
            this.rhombusUC61.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC61.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC61.TabIndex = 3188;
            // 
            // lineUC192
            // 
            this.lineUC192.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC192.ExtenderWidth = 3;
            this.lineUC192.Fill = false;
            this.lineUC192.GroupID = null;
            this.lineUC192.Location = new System.Drawing.Point(152, 734);
            this.lineUC192.Name = "lineUC192";
            this.lineUC192.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC192.Size = new System.Drawing.Size(3, 50);
            this.lineUC192.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC192.TabIndex = 3194;
            // 
            // retangleUC88
            // 
            this.retangleUC88.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC88.ExtenderWidth = 3;
            this.retangleUC88.Fill = false;
            this.retangleUC88.GroupID = null;
            this.retangleUC88.Location = new System.Drawing.Point(172, 800);
            this.retangleUC88.Name = "retangleUC88";
            this.retangleUC88.Size = new System.Drawing.Size(15, 15);
            this.retangleUC88.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC88.TabIndex = 3193;
            // 
            // retangleUC89
            // 
            this.retangleUC89.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC89.ExtenderWidth = 3;
            this.retangleUC89.Fill = false;
            this.retangleUC89.GroupID = null;
            this.retangleUC89.Location = new System.Drawing.Point(122, 800);
            this.retangleUC89.Name = "retangleUC89";
            this.retangleUC89.Size = new System.Drawing.Size(15, 15);
            this.retangleUC89.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC89.TabIndex = 3192;
            // 
            // lineUC193
            // 
            this.lineUC193.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC193.ExtenderWidth = 3;
            this.lineUC193.Fill = false;
            this.lineUC193.GroupID = null;
            this.lineUC193.Location = new System.Drawing.Point(178, 784);
            this.lineUC193.Name = "lineUC193";
            this.lineUC193.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC193.Size = new System.Drawing.Size(3, 60);
            this.lineUC193.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC193.TabIndex = 3191;
            // 
            // lineUC194
            // 
            this.lineUC194.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC194.ExtenderWidth = 3;
            this.lineUC194.Fill = false;
            this.lineUC194.GroupID = null;
            this.lineUC194.Location = new System.Drawing.Point(128, 784);
            this.lineUC194.Name = "lineUC194";
            this.lineUC194.Size = new System.Drawing.Size(50, 3);
            this.lineUC194.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC194.TabIndex = 3190;
            // 
            // lineUC195
            // 
            this.lineUC195.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC195.ExtenderWidth = 3;
            this.lineUC195.Fill = false;
            this.lineUC195.GroupID = null;
            this.lineUC195.Location = new System.Drawing.Point(128, 784);
            this.lineUC195.Name = "lineUC195";
            this.lineUC195.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC195.Size = new System.Drawing.Size(3, 60);
            this.lineUC195.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC195.TabIndex = 3189;
            // 
            // label149
            // 
            this.label149.AutoSize = true;
            this.label149.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label149.Location = new System.Drawing.Point(472, 754);
            this.label149.Name = "label149";
            this.label149.Size = new System.Drawing.Size(23, 15);
            this.label149.TabIndex = 3247;
            this.label149.Text = "Q1";
            // 
            // label150
            // 
            this.label150.AutoSize = true;
            this.label150.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label150.Location = new System.Drawing.Point(450, 800);
            this.label150.Name = "label150";
            this.label150.Size = new System.Drawing.Size(21, 15);
            this.label150.TabIndex = 3246;
            this.label150.Text = "K1";
            // 
            // label151
            // 
            this.label151.AutoSize = true;
            this.label151.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label151.Location = new System.Drawing.Point(498, 803);
            this.label151.Name = "label151";
            this.label151.Size = new System.Drawing.Size(21, 15);
            this.label151.TabIndex = 3245;
            this.label151.Text = "K3";
            // 
            // rhombusUC62
            // 
            this.rhombusUC62.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC62.ExtenderWidth = 2;
            this.rhombusUC62.Fill = false;
            this.rhombusUC62.GroupID = null;
            this.rhombusUC62.Location = new System.Drawing.Point(494, 754);
            this.rhombusUC62.Name = "rhombusUC62";
            this.rhombusUC62.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC62.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC62.TabIndex = 3238;
            // 
            // lineUC196
            // 
            this.lineUC196.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC196.ExtenderWidth = 3;
            this.lineUC196.Fill = false;
            this.lineUC196.GroupID = null;
            this.lineUC196.Location = new System.Drawing.Point(501, 734);
            this.lineUC196.Name = "lineUC196";
            this.lineUC196.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC196.Size = new System.Drawing.Size(3, 50);
            this.lineUC196.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC196.TabIndex = 3244;
            // 
            // retangleUC90
            // 
            this.retangleUC90.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC90.ExtenderWidth = 3;
            this.retangleUC90.Fill = false;
            this.retangleUC90.GroupID = null;
            this.retangleUC90.Location = new System.Drawing.Point(521, 800);
            this.retangleUC90.Name = "retangleUC90";
            this.retangleUC90.Size = new System.Drawing.Size(15, 15);
            this.retangleUC90.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC90.TabIndex = 3243;
            // 
            // retangleUC91
            // 
            this.retangleUC91.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC91.ExtenderWidth = 3;
            this.retangleUC91.Fill = false;
            this.retangleUC91.GroupID = null;
            this.retangleUC91.Location = new System.Drawing.Point(471, 800);
            this.retangleUC91.Name = "retangleUC91";
            this.retangleUC91.Size = new System.Drawing.Size(15, 15);
            this.retangleUC91.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC91.TabIndex = 3242;
            // 
            // lineUC197
            // 
            this.lineUC197.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC197.ExtenderWidth = 3;
            this.lineUC197.Fill = false;
            this.lineUC197.GroupID = null;
            this.lineUC197.Location = new System.Drawing.Point(527, 784);
            this.lineUC197.Name = "lineUC197";
            this.lineUC197.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC197.Size = new System.Drawing.Size(3, 96);
            this.lineUC197.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC197.TabIndex = 3241;
            // 
            // lineUC198
            // 
            this.lineUC198.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC198.ExtenderWidth = 3;
            this.lineUC198.Fill = false;
            this.lineUC198.GroupID = null;
            this.lineUC198.Location = new System.Drawing.Point(477, 784);
            this.lineUC198.Name = "lineUC198";
            this.lineUC198.Size = new System.Drawing.Size(50, 3);
            this.lineUC198.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC198.TabIndex = 3240;
            // 
            // lineUC199
            // 
            this.lineUC199.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC199.ExtenderWidth = 3;
            this.lineUC199.Fill = false;
            this.lineUC199.GroupID = null;
            this.lineUC199.Location = new System.Drawing.Point(477, 784);
            this.lineUC199.Name = "lineUC199";
            this.lineUC199.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC199.Size = new System.Drawing.Size(3, 96);
            this.lineUC199.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC199.TabIndex = 3239;
            // 
            // label152
            // 
            this.label152.AutoSize = true;
            this.label152.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label152.Location = new System.Drawing.Point(298, 754);
            this.label152.Name = "label152";
            this.label152.Size = new System.Drawing.Size(23, 15);
            this.label152.TabIndex = 3237;
            this.label152.Text = "Q1";
            // 
            // label153
            // 
            this.label153.AutoSize = true;
            this.label153.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label153.Location = new System.Drawing.Point(276, 800);
            this.label153.Name = "label153";
            this.label153.Size = new System.Drawing.Size(21, 15);
            this.label153.TabIndex = 3236;
            this.label153.Text = "K1";
            // 
            // label154
            // 
            this.label154.AutoSize = true;
            this.label154.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label154.Location = new System.Drawing.Point(324, 803);
            this.label154.Name = "label154";
            this.label154.Size = new System.Drawing.Size(21, 15);
            this.label154.TabIndex = 3235;
            this.label154.Text = "K3";
            // 
            // rhombusUC63
            // 
            this.rhombusUC63.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC63.ExtenderWidth = 2;
            this.rhombusUC63.Fill = false;
            this.rhombusUC63.GroupID = null;
            this.rhombusUC63.Location = new System.Drawing.Point(320, 754);
            this.rhombusUC63.Name = "rhombusUC63";
            this.rhombusUC63.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC63.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC63.TabIndex = 3228;
            // 
            // lineUC200
            // 
            this.lineUC200.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC200.ExtenderWidth = 3;
            this.lineUC200.Fill = false;
            this.lineUC200.GroupID = null;
            this.lineUC200.Location = new System.Drawing.Point(327, 734);
            this.lineUC200.Name = "lineUC200";
            this.lineUC200.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC200.Size = new System.Drawing.Size(3, 50);
            this.lineUC200.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC200.TabIndex = 3234;
            // 
            // retangleUC92
            // 
            this.retangleUC92.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC92.ExtenderWidth = 3;
            this.retangleUC92.Fill = false;
            this.retangleUC92.GroupID = null;
            this.retangleUC92.Location = new System.Drawing.Point(347, 800);
            this.retangleUC92.Name = "retangleUC92";
            this.retangleUC92.Size = new System.Drawing.Size(15, 15);
            this.retangleUC92.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC92.TabIndex = 3233;
            // 
            // retangleUC93
            // 
            this.retangleUC93.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC93.ExtenderWidth = 3;
            this.retangleUC93.Fill = false;
            this.retangleUC93.GroupID = null;
            this.retangleUC93.Location = new System.Drawing.Point(297, 800);
            this.retangleUC93.Name = "retangleUC93";
            this.retangleUC93.Size = new System.Drawing.Size(15, 15);
            this.retangleUC93.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC93.TabIndex = 3232;
            // 
            // lineUC201
            // 
            this.lineUC201.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC201.ExtenderWidth = 3;
            this.lineUC201.Fill = false;
            this.lineUC201.GroupID = null;
            this.lineUC201.Location = new System.Drawing.Point(353, 784);
            this.lineUC201.Name = "lineUC201";
            this.lineUC201.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC201.Size = new System.Drawing.Size(3, 96);
            this.lineUC201.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC201.TabIndex = 3231;
            // 
            // lineUC202
            // 
            this.lineUC202.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC202.ExtenderWidth = 3;
            this.lineUC202.Fill = false;
            this.lineUC202.GroupID = null;
            this.lineUC202.Location = new System.Drawing.Point(303, 784);
            this.lineUC202.Name = "lineUC202";
            this.lineUC202.Size = new System.Drawing.Size(50, 3);
            this.lineUC202.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC202.TabIndex = 3230;
            // 
            // lineUC203
            // 
            this.lineUC203.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC203.ExtenderWidth = 3;
            this.lineUC203.Fill = false;
            this.lineUC203.GroupID = null;
            this.lineUC203.Location = new System.Drawing.Point(303, 784);
            this.lineUC203.Name = "lineUC203";
            this.lineUC203.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC203.Size = new System.Drawing.Size(3, 96);
            this.lineUC203.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC203.TabIndex = 3229;
            // 
            // label155
            // 
            this.label155.AutoSize = true;
            this.label155.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label155.Location = new System.Drawing.Point(363, 800);
            this.label155.Name = "label155";
            this.label155.Size = new System.Drawing.Size(21, 15);
            this.label155.TabIndex = 3227;
            this.label155.Text = "K2";
            // 
            // label156
            // 
            this.label156.AutoSize = true;
            this.label156.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label156.Location = new System.Drawing.Point(385, 754);
            this.label156.Name = "label156";
            this.label156.Size = new System.Drawing.Size(23, 15);
            this.label156.TabIndex = 3226;
            this.label156.Text = "Q2";
            // 
            // label157
            // 
            this.label157.AutoSize = true;
            this.label157.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label157.Location = new System.Drawing.Point(411, 803);
            this.label157.Name = "label157";
            this.label157.Size = new System.Drawing.Size(21, 15);
            this.label157.TabIndex = 3225;
            this.label157.Text = "K4";
            // 
            // rhombusUC64
            // 
            this.rhombusUC64.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC64.ExtenderWidth = 2;
            this.rhombusUC64.Fill = false;
            this.rhombusUC64.GroupID = null;
            this.rhombusUC64.Location = new System.Drawing.Point(407, 754);
            this.rhombusUC64.Name = "rhombusUC64";
            this.rhombusUC64.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC64.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC64.TabIndex = 3218;
            // 
            // lineUC204
            // 
            this.lineUC204.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC204.ExtenderWidth = 3;
            this.lineUC204.Fill = false;
            this.lineUC204.GroupID = null;
            this.lineUC204.Location = new System.Drawing.Point(414, 734);
            this.lineUC204.Name = "lineUC204";
            this.lineUC204.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC204.Size = new System.Drawing.Size(3, 50);
            this.lineUC204.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC204.TabIndex = 3224;
            // 
            // retangleUC94
            // 
            this.retangleUC94.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC94.ExtenderWidth = 3;
            this.retangleUC94.Fill = false;
            this.retangleUC94.GroupID = null;
            this.retangleUC94.Location = new System.Drawing.Point(434, 800);
            this.retangleUC94.Name = "retangleUC94";
            this.retangleUC94.Size = new System.Drawing.Size(15, 15);
            this.retangleUC94.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC94.TabIndex = 3223;
            // 
            // retangleUC95
            // 
            this.retangleUC95.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC95.ExtenderWidth = 3;
            this.retangleUC95.Fill = false;
            this.retangleUC95.GroupID = null;
            this.retangleUC95.Location = new System.Drawing.Point(384, 800);
            this.retangleUC95.Name = "retangleUC95";
            this.retangleUC95.Size = new System.Drawing.Size(15, 15);
            this.retangleUC95.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC95.TabIndex = 3222;
            // 
            // lineUC205
            // 
            this.lineUC205.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC205.ExtenderWidth = 3;
            this.lineUC205.Fill = false;
            this.lineUC205.GroupID = null;
            this.lineUC205.Location = new System.Drawing.Point(440, 784);
            this.lineUC205.Name = "lineUC205";
            this.lineUC205.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC205.Size = new System.Drawing.Size(3, 60);
            this.lineUC205.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC205.TabIndex = 3221;
            // 
            // lineUC206
            // 
            this.lineUC206.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC206.ExtenderWidth = 3;
            this.lineUC206.Fill = false;
            this.lineUC206.GroupID = null;
            this.lineUC206.Location = new System.Drawing.Point(390, 784);
            this.lineUC206.Name = "lineUC206";
            this.lineUC206.Size = new System.Drawing.Size(50, 3);
            this.lineUC206.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC206.TabIndex = 3220;
            // 
            // lineUC207
            // 
            this.lineUC207.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC207.ExtenderWidth = 3;
            this.lineUC207.Fill = false;
            this.lineUC207.GroupID = null;
            this.lineUC207.Location = new System.Drawing.Point(390, 784);
            this.lineUC207.Name = "lineUC207";
            this.lineUC207.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC207.Size = new System.Drawing.Size(3, 60);
            this.lineUC207.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC207.TabIndex = 3219;
            // 
            // label158
            // 
            this.label158.AutoSize = true;
            this.label158.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label158.Location = new System.Drawing.Point(627, 754);
            this.label158.Name = "label158";
            this.label158.Size = new System.Drawing.Size(23, 15);
            this.label158.TabIndex = 3267;
            this.label158.Text = "Q1";
            // 
            // label159
            // 
            this.label159.AutoSize = true;
            this.label159.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label159.Location = new System.Drawing.Point(606, 800);
            this.label159.Name = "label159";
            this.label159.Size = new System.Drawing.Size(21, 15);
            this.label159.TabIndex = 3266;
            this.label159.Text = "K1";
            // 
            // label160
            // 
            this.label160.AutoSize = true;
            this.label160.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label160.Location = new System.Drawing.Point(654, 803);
            this.label160.Name = "label160";
            this.label160.Size = new System.Drawing.Size(21, 15);
            this.label160.TabIndex = 3265;
            this.label160.Text = "K3";
            // 
            // rhombusUC65
            // 
            this.rhombusUC65.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC65.ExtenderWidth = 2;
            this.rhombusUC65.Fill = false;
            this.rhombusUC65.GroupID = null;
            this.rhombusUC65.Location = new System.Drawing.Point(650, 754);
            this.rhombusUC65.Name = "rhombusUC65";
            this.rhombusUC65.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC65.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC65.TabIndex = 3258;
            // 
            // lineUC208
            // 
            this.lineUC208.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC208.ExtenderWidth = 3;
            this.lineUC208.Fill = false;
            this.lineUC208.GroupID = null;
            this.lineUC208.Location = new System.Drawing.Point(657, 734);
            this.lineUC208.Name = "lineUC208";
            this.lineUC208.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC208.Size = new System.Drawing.Size(3, 50);
            this.lineUC208.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC208.TabIndex = 3264;
            // 
            // retangleUC96
            // 
            this.retangleUC96.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC96.ExtenderWidth = 3;
            this.retangleUC96.Fill = false;
            this.retangleUC96.GroupID = null;
            this.retangleUC96.Location = new System.Drawing.Point(677, 800);
            this.retangleUC96.Name = "retangleUC96";
            this.retangleUC96.Size = new System.Drawing.Size(15, 15);
            this.retangleUC96.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC96.TabIndex = 3263;
            // 
            // retangleUC97
            // 
            this.retangleUC97.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC97.ExtenderWidth = 3;
            this.retangleUC97.Fill = false;
            this.retangleUC97.GroupID = null;
            this.retangleUC97.Location = new System.Drawing.Point(627, 800);
            this.retangleUC97.Name = "retangleUC97";
            this.retangleUC97.Size = new System.Drawing.Size(15, 15);
            this.retangleUC97.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC97.TabIndex = 3262;
            // 
            // lineUC209
            // 
            this.lineUC209.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC209.ExtenderWidth = 3;
            this.lineUC209.Fill = false;
            this.lineUC209.GroupID = null;
            this.lineUC209.Location = new System.Drawing.Point(683, 784);
            this.lineUC209.Name = "lineUC209";
            this.lineUC209.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC209.Size = new System.Drawing.Size(3, 96);
            this.lineUC209.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC209.TabIndex = 3261;
            // 
            // lineUC210
            // 
            this.lineUC210.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC210.ExtenderWidth = 3;
            this.lineUC210.Fill = false;
            this.lineUC210.GroupID = null;
            this.lineUC210.Location = new System.Drawing.Point(633, 784);
            this.lineUC210.Name = "lineUC210";
            this.lineUC210.Size = new System.Drawing.Size(50, 3);
            this.lineUC210.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC210.TabIndex = 3260;
            // 
            // lineUC211
            // 
            this.lineUC211.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC211.ExtenderWidth = 3;
            this.lineUC211.Fill = false;
            this.lineUC211.GroupID = null;
            this.lineUC211.Location = new System.Drawing.Point(633, 784);
            this.lineUC211.Name = "lineUC211";
            this.lineUC211.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC211.Size = new System.Drawing.Size(3, 96);
            this.lineUC211.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC211.TabIndex = 3259;
            // 
            // label161
            // 
            this.label161.AutoSize = true;
            this.label161.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label161.Location = new System.Drawing.Point(693, 800);
            this.label161.Name = "label161";
            this.label161.Size = new System.Drawing.Size(21, 15);
            this.label161.TabIndex = 3257;
            this.label161.Text = "K2";
            // 
            // label162
            // 
            this.label162.AutoSize = true;
            this.label162.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label162.Location = new System.Drawing.Point(715, 754);
            this.label162.Name = "label162";
            this.label162.Size = new System.Drawing.Size(23, 15);
            this.label162.TabIndex = 3256;
            this.label162.Text = "Q2";
            // 
            // label163
            // 
            this.label163.AutoSize = true;
            this.label163.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label163.Location = new System.Drawing.Point(741, 803);
            this.label163.Name = "label163";
            this.label163.Size = new System.Drawing.Size(21, 15);
            this.label163.TabIndex = 3255;
            this.label163.Text = "K4";
            // 
            // rhombusUC66
            // 
            this.rhombusUC66.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC66.ExtenderWidth = 2;
            this.rhombusUC66.Fill = false;
            this.rhombusUC66.GroupID = null;
            this.rhombusUC66.Location = new System.Drawing.Point(737, 754);
            this.rhombusUC66.Name = "rhombusUC66";
            this.rhombusUC66.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC66.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC66.TabIndex = 3248;
            // 
            // lineUC212
            // 
            this.lineUC212.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC212.ExtenderWidth = 3;
            this.lineUC212.Fill = false;
            this.lineUC212.GroupID = null;
            this.lineUC212.Location = new System.Drawing.Point(743, 734);
            this.lineUC212.Name = "lineUC212";
            this.lineUC212.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC212.Size = new System.Drawing.Size(3, 50);
            this.lineUC212.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC212.TabIndex = 3254;
            // 
            // retangleUC98
            // 
            this.retangleUC98.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC98.ExtenderWidth = 3;
            this.retangleUC98.Fill = false;
            this.retangleUC98.GroupID = null;
            this.retangleUC98.Location = new System.Drawing.Point(763, 800);
            this.retangleUC98.Name = "retangleUC98";
            this.retangleUC98.Size = new System.Drawing.Size(15, 15);
            this.retangleUC98.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC98.TabIndex = 3253;
            // 
            // retangleUC99
            // 
            this.retangleUC99.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC99.ExtenderWidth = 3;
            this.retangleUC99.Fill = false;
            this.retangleUC99.GroupID = null;
            this.retangleUC99.Location = new System.Drawing.Point(714, 800);
            this.retangleUC99.Name = "retangleUC99";
            this.retangleUC99.Size = new System.Drawing.Size(15, 15);
            this.retangleUC99.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC99.TabIndex = 3252;
            // 
            // lineUC213
            // 
            this.lineUC213.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC213.ExtenderWidth = 3;
            this.lineUC213.Fill = false;
            this.lineUC213.GroupID = null;
            this.lineUC213.Location = new System.Drawing.Point(770, 784);
            this.lineUC213.Name = "lineUC213";
            this.lineUC213.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC213.Size = new System.Drawing.Size(3, 60);
            this.lineUC213.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC213.TabIndex = 3251;
            // 
            // lineUC214
            // 
            this.lineUC214.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC214.ExtenderWidth = 3;
            this.lineUC214.Fill = false;
            this.lineUC214.GroupID = null;
            this.lineUC214.Location = new System.Drawing.Point(719, 784);
            this.lineUC214.Name = "lineUC214";
            this.lineUC214.Size = new System.Drawing.Size(50, 3);
            this.lineUC214.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC214.TabIndex = 3250;
            // 
            // lineUC215
            // 
            this.lineUC215.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC215.ExtenderWidth = 3;
            this.lineUC215.Fill = false;
            this.lineUC215.GroupID = null;
            this.lineUC215.Location = new System.Drawing.Point(719, 784);
            this.lineUC215.Name = "lineUC215";
            this.lineUC215.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC215.Size = new System.Drawing.Size(3, 60);
            this.lineUC215.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC215.TabIndex = 3249;
            // 
            // label164
            // 
            this.label164.AutoSize = true;
            this.label164.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label164.Location = new System.Drawing.Point(1089, 754);
            this.label164.Name = "label164";
            this.label164.Size = new System.Drawing.Size(23, 15);
            this.label164.TabIndex = 3317;
            this.label164.Text = "Q1";
            // 
            // label165
            // 
            this.label165.AutoSize = true;
            this.label165.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label165.Location = new System.Drawing.Point(1039, 800);
            this.label165.Name = "label165";
            this.label165.Size = new System.Drawing.Size(21, 15);
            this.label165.TabIndex = 3316;
            this.label165.Text = "K1";
            // 
            // label166
            // 
            this.label166.AutoSize = true;
            this.label166.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label166.Location = new System.Drawing.Point(1115, 803);
            this.label166.Name = "label166";
            this.label166.Size = new System.Drawing.Size(21, 15);
            this.label166.TabIndex = 3315;
            this.label166.Text = "K3";
            // 
            // rhombusUC67
            // 
            this.rhombusUC67.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC67.ExtenderWidth = 2;
            this.rhombusUC67.Fill = false;
            this.rhombusUC67.GroupID = null;
            this.rhombusUC67.Location = new System.Drawing.Point(1111, 754);
            this.rhombusUC67.Name = "rhombusUC67";
            this.rhombusUC67.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC67.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC67.TabIndex = 3308;
            // 
            // lineUC216
            // 
            this.lineUC216.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC216.ExtenderWidth = 3;
            this.lineUC216.Fill = false;
            this.lineUC216.GroupID = null;
            this.lineUC216.Location = new System.Drawing.Point(1118, 734);
            this.lineUC216.Name = "lineUC216";
            this.lineUC216.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC216.Size = new System.Drawing.Size(3, 50);
            this.lineUC216.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC216.TabIndex = 3314;
            // 
            // retangleUC100
            // 
            this.retangleUC100.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC100.ExtenderWidth = 3;
            this.retangleUC100.Fill = false;
            this.retangleUC100.GroupID = null;
            this.retangleUC100.Location = new System.Drawing.Point(1138, 800);
            this.retangleUC100.Name = "retangleUC100";
            this.retangleUC100.Size = new System.Drawing.Size(15, 15);
            this.retangleUC100.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC100.TabIndex = 3313;
            // 
            // retangleUC101
            // 
            this.retangleUC101.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC101.ExtenderWidth = 3;
            this.retangleUC101.Fill = false;
            this.retangleUC101.GroupID = null;
            this.retangleUC101.Location = new System.Drawing.Point(1088, 800);
            this.retangleUC101.Name = "retangleUC101";
            this.retangleUC101.Size = new System.Drawing.Size(15, 15);
            this.retangleUC101.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC101.TabIndex = 3312;
            // 
            // lineUC217
            // 
            this.lineUC217.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC217.ExtenderWidth = 3;
            this.lineUC217.Fill = false;
            this.lineUC217.GroupID = null;
            this.lineUC217.Location = new System.Drawing.Point(1144, 784);
            this.lineUC217.Name = "lineUC217";
            this.lineUC217.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC217.Size = new System.Drawing.Size(3, 96);
            this.lineUC217.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC217.TabIndex = 3311;
            // 
            // lineUC218
            // 
            this.lineUC218.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC218.ExtenderWidth = 3;
            this.lineUC218.Fill = false;
            this.lineUC218.GroupID = null;
            this.lineUC218.Location = new System.Drawing.Point(1094, 784);
            this.lineUC218.Name = "lineUC218";
            this.lineUC218.Size = new System.Drawing.Size(50, 3);
            this.lineUC218.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC218.TabIndex = 3310;
            // 
            // lineUC219
            // 
            this.lineUC219.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC219.ExtenderWidth = 3;
            this.lineUC219.Fill = false;
            this.lineUC219.GroupID = null;
            this.lineUC219.Location = new System.Drawing.Point(1094, 784);
            this.lineUC219.Name = "lineUC219";
            this.lineUC219.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC219.Size = new System.Drawing.Size(3, 96);
            this.lineUC219.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC219.TabIndex = 3309;
            // 
            // label167
            // 
            this.label167.AutoSize = true;
            this.label167.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label167.Location = new System.Drawing.Point(1154, 800);
            this.label167.Name = "label167";
            this.label167.Size = new System.Drawing.Size(21, 15);
            this.label167.TabIndex = 3307;
            this.label167.Text = "K2";
            // 
            // label168
            // 
            this.label168.AutoSize = true;
            this.label168.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label168.Location = new System.Drawing.Point(1176, 754);
            this.label168.Name = "label168";
            this.label168.Size = new System.Drawing.Size(23, 15);
            this.label168.TabIndex = 3306;
            this.label168.Text = "Q2";
            // 
            // label169
            // 
            this.label169.AutoSize = true;
            this.label169.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label169.Location = new System.Drawing.Point(1202, 803);
            this.label169.Name = "label169";
            this.label169.Size = new System.Drawing.Size(21, 15);
            this.label169.TabIndex = 3305;
            this.label169.Text = "K4";
            // 
            // rhombusUC68
            // 
            this.rhombusUC68.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC68.ExtenderWidth = 2;
            this.rhombusUC68.Fill = false;
            this.rhombusUC68.GroupID = null;
            this.rhombusUC68.Location = new System.Drawing.Point(1198, 754);
            this.rhombusUC68.Name = "rhombusUC68";
            this.rhombusUC68.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC68.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC68.TabIndex = 3298;
            // 
            // lineUC220
            // 
            this.lineUC220.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC220.ExtenderWidth = 3;
            this.lineUC220.Fill = false;
            this.lineUC220.GroupID = null;
            this.lineUC220.Location = new System.Drawing.Point(1205, 734);
            this.lineUC220.Name = "lineUC220";
            this.lineUC220.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC220.Size = new System.Drawing.Size(3, 50);
            this.lineUC220.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC220.TabIndex = 3304;
            // 
            // retangleUC102
            // 
            this.retangleUC102.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC102.ExtenderWidth = 3;
            this.retangleUC102.Fill = false;
            this.retangleUC102.GroupID = null;
            this.retangleUC102.Location = new System.Drawing.Point(1225, 800);
            this.retangleUC102.Name = "retangleUC102";
            this.retangleUC102.Size = new System.Drawing.Size(15, 15);
            this.retangleUC102.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC102.TabIndex = 3303;
            // 
            // retangleUC103
            // 
            this.retangleUC103.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC103.ExtenderWidth = 3;
            this.retangleUC103.Fill = false;
            this.retangleUC103.GroupID = null;
            this.retangleUC103.Location = new System.Drawing.Point(1175, 800);
            this.retangleUC103.Name = "retangleUC103";
            this.retangleUC103.Size = new System.Drawing.Size(15, 15);
            this.retangleUC103.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC103.TabIndex = 3302;
            // 
            // lineUC221
            // 
            this.lineUC221.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC221.ExtenderWidth = 3;
            this.lineUC221.Fill = false;
            this.lineUC221.GroupID = null;
            this.lineUC221.Location = new System.Drawing.Point(1231, 784);
            this.lineUC221.Name = "lineUC221";
            this.lineUC221.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC221.Size = new System.Drawing.Size(3, 60);
            this.lineUC221.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC221.TabIndex = 3301;
            // 
            // lineUC222
            // 
            this.lineUC222.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC222.ExtenderWidth = 3;
            this.lineUC222.Fill = false;
            this.lineUC222.GroupID = null;
            this.lineUC222.Location = new System.Drawing.Point(1181, 784);
            this.lineUC222.Name = "lineUC222";
            this.lineUC222.Size = new System.Drawing.Size(50, 3);
            this.lineUC222.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC222.TabIndex = 3300;
            // 
            // lineUC223
            // 
            this.lineUC223.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC223.ExtenderWidth = 3;
            this.lineUC223.Fill = false;
            this.lineUC223.GroupID = null;
            this.lineUC223.Location = new System.Drawing.Point(1181, 784);
            this.lineUC223.Name = "lineUC223";
            this.lineUC223.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC223.Size = new System.Drawing.Size(3, 60);
            this.lineUC223.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC223.TabIndex = 3299;
            // 
            // label170
            // 
            this.label170.AutoSize = true;
            this.label170.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label170.Location = new System.Drawing.Point(953, 800);
            this.label170.Name = "label170";
            this.label170.Size = new System.Drawing.Size(21, 15);
            this.label170.TabIndex = 3297;
            this.label170.Text = "K2";
            // 
            // label171
            // 
            this.label171.AutoSize = true;
            this.label171.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label171.Location = new System.Drawing.Point(975, 754);
            this.label171.Name = "label171";
            this.label171.Size = new System.Drawing.Size(23, 15);
            this.label171.TabIndex = 3296;
            this.label171.Text = "Q2";
            // 
            // label172
            // 
            this.label172.AutoSize = true;
            this.label172.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label172.Location = new System.Drawing.Point(1001, 803);
            this.label172.Name = "label172";
            this.label172.Size = new System.Drawing.Size(21, 15);
            this.label172.TabIndex = 3295;
            this.label172.Text = "K4";
            // 
            // rhombusUC69
            // 
            this.rhombusUC69.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC69.ExtenderWidth = 2;
            this.rhombusUC69.Fill = false;
            this.rhombusUC69.GroupID = null;
            this.rhombusUC69.Location = new System.Drawing.Point(997, 754);
            this.rhombusUC69.Name = "rhombusUC69";
            this.rhombusUC69.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC69.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC69.TabIndex = 3288;
            // 
            // lineUC224
            // 
            this.lineUC224.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC224.ExtenderWidth = 3;
            this.lineUC224.Fill = false;
            this.lineUC224.GroupID = null;
            this.lineUC224.Location = new System.Drawing.Point(1003, 734);
            this.lineUC224.Name = "lineUC224";
            this.lineUC224.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC224.Size = new System.Drawing.Size(3, 50);
            this.lineUC224.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC224.TabIndex = 3294;
            // 
            // retangleUC104
            // 
            this.retangleUC104.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC104.ExtenderWidth = 3;
            this.retangleUC104.Fill = false;
            this.retangleUC104.GroupID = null;
            this.retangleUC104.Location = new System.Drawing.Point(1023, 800);
            this.retangleUC104.Name = "retangleUC104";
            this.retangleUC104.Size = new System.Drawing.Size(15, 15);
            this.retangleUC104.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC104.TabIndex = 3293;
            // 
            // retangleUC105
            // 
            this.retangleUC105.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC105.ExtenderWidth = 3;
            this.retangleUC105.Fill = false;
            this.retangleUC105.GroupID = null;
            this.retangleUC105.Location = new System.Drawing.Point(974, 800);
            this.retangleUC105.Name = "retangleUC105";
            this.retangleUC105.Size = new System.Drawing.Size(15, 15);
            this.retangleUC105.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC105.TabIndex = 3292;
            // 
            // lineUC225
            // 
            this.lineUC225.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC225.ExtenderWidth = 3;
            this.lineUC225.Fill = false;
            this.lineUC225.GroupID = null;
            this.lineUC225.Location = new System.Drawing.Point(1030, 784);
            this.lineUC225.Name = "lineUC225";
            this.lineUC225.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC225.Size = new System.Drawing.Size(3, 60);
            this.lineUC225.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC225.TabIndex = 3291;
            // 
            // lineUC226
            // 
            this.lineUC226.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC226.ExtenderWidth = 3;
            this.lineUC226.Fill = false;
            this.lineUC226.GroupID = null;
            this.lineUC226.Location = new System.Drawing.Point(979, 784);
            this.lineUC226.Name = "lineUC226";
            this.lineUC226.Size = new System.Drawing.Size(50, 3);
            this.lineUC226.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC226.TabIndex = 3290;
            // 
            // lineUC227
            // 
            this.lineUC227.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC227.ExtenderWidth = 3;
            this.lineUC227.Fill = false;
            this.lineUC227.GroupID = null;
            this.lineUC227.Location = new System.Drawing.Point(979, 784);
            this.lineUC227.Name = "lineUC227";
            this.lineUC227.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC227.Size = new System.Drawing.Size(3, 60);
            this.lineUC227.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC227.TabIndex = 3289;
            // 
            // label173
            // 
            this.label173.AutoSize = true;
            this.label173.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label173.Location = new System.Drawing.Point(802, 754);
            this.label173.Name = "label173";
            this.label173.Size = new System.Drawing.Size(23, 15);
            this.label173.TabIndex = 3287;
            this.label173.Text = "Q1";
            // 
            // label174
            // 
            this.label174.AutoSize = true;
            this.label174.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label174.Location = new System.Drawing.Point(779, 800);
            this.label174.Name = "label174";
            this.label174.Size = new System.Drawing.Size(21, 15);
            this.label174.TabIndex = 3286;
            this.label174.Text = "K1";
            // 
            // label175
            // 
            this.label175.AutoSize = true;
            this.label175.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label175.Location = new System.Drawing.Point(827, 803);
            this.label175.Name = "label175";
            this.label175.Size = new System.Drawing.Size(21, 15);
            this.label175.TabIndex = 3285;
            this.label175.Text = "K3";
            // 
            // rhombusUC70
            // 
            this.rhombusUC70.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC70.ExtenderWidth = 2;
            this.rhombusUC70.Fill = false;
            this.rhombusUC70.GroupID = null;
            this.rhombusUC70.Location = new System.Drawing.Point(823, 754);
            this.rhombusUC70.Name = "rhombusUC70";
            this.rhombusUC70.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC70.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC70.TabIndex = 3278;
            // 
            // lineUC228
            // 
            this.lineUC228.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC228.ExtenderWidth = 3;
            this.lineUC228.Fill = false;
            this.lineUC228.GroupID = null;
            this.lineUC228.Location = new System.Drawing.Point(831, 734);
            this.lineUC228.Name = "lineUC228";
            this.lineUC228.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC228.Size = new System.Drawing.Size(3, 50);
            this.lineUC228.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC228.TabIndex = 3284;
            // 
            // retangleUC106
            // 
            this.retangleUC106.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC106.ExtenderWidth = 3;
            this.retangleUC106.Fill = false;
            this.retangleUC106.GroupID = null;
            this.retangleUC106.Location = new System.Drawing.Point(851, 800);
            this.retangleUC106.Name = "retangleUC106";
            this.retangleUC106.Size = new System.Drawing.Size(15, 15);
            this.retangleUC106.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC106.TabIndex = 3283;
            // 
            // retangleUC107
            // 
            this.retangleUC107.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC107.ExtenderWidth = 3;
            this.retangleUC107.Fill = false;
            this.retangleUC107.GroupID = null;
            this.retangleUC107.Location = new System.Drawing.Point(801, 800);
            this.retangleUC107.Name = "retangleUC107";
            this.retangleUC107.Size = new System.Drawing.Size(15, 15);
            this.retangleUC107.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC107.TabIndex = 3282;
            // 
            // lineUC229
            // 
            this.lineUC229.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC229.ExtenderWidth = 3;
            this.lineUC229.Fill = false;
            this.lineUC229.GroupID = null;
            this.lineUC229.Location = new System.Drawing.Point(857, 784);
            this.lineUC229.Name = "lineUC229";
            this.lineUC229.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC229.Size = new System.Drawing.Size(3, 96);
            this.lineUC229.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC229.TabIndex = 3281;
            // 
            // lineUC230
            // 
            this.lineUC230.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC230.ExtenderWidth = 3;
            this.lineUC230.Fill = false;
            this.lineUC230.GroupID = null;
            this.lineUC230.Location = new System.Drawing.Point(807, 784);
            this.lineUC230.Name = "lineUC230";
            this.lineUC230.Size = new System.Drawing.Size(50, 3);
            this.lineUC230.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC230.TabIndex = 3280;
            // 
            // lineUC231
            // 
            this.lineUC231.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC231.ExtenderWidth = 3;
            this.lineUC231.Fill = false;
            this.lineUC231.GroupID = null;
            this.lineUC231.Location = new System.Drawing.Point(807, 784);
            this.lineUC231.Name = "lineUC231";
            this.lineUC231.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC231.Size = new System.Drawing.Size(3, 96);
            this.lineUC231.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC231.TabIndex = 3279;
            // 
            // label176
            // 
            this.label176.AutoSize = true;
            this.label176.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label176.Location = new System.Drawing.Point(867, 800);
            this.label176.Name = "label176";
            this.label176.Size = new System.Drawing.Size(21, 15);
            this.label176.TabIndex = 3277;
            this.label176.Text = "K2";
            // 
            // label177
            // 
            this.label177.AutoSize = true;
            this.label177.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label177.Location = new System.Drawing.Point(889, 754);
            this.label177.Name = "label177";
            this.label177.Size = new System.Drawing.Size(23, 15);
            this.label177.TabIndex = 3276;
            this.label177.Text = "Q2";
            // 
            // label178
            // 
            this.label178.AutoSize = true;
            this.label178.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label178.Location = new System.Drawing.Point(915, 803);
            this.label178.Name = "label178";
            this.label178.Size = new System.Drawing.Size(21, 15);
            this.label178.TabIndex = 3275;
            this.label178.Text = "K4";
            // 
            // rhombusUC71
            // 
            this.rhombusUC71.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC71.ExtenderWidth = 2;
            this.rhombusUC71.Fill = false;
            this.rhombusUC71.GroupID = null;
            this.rhombusUC71.Location = new System.Drawing.Point(911, 754);
            this.rhombusUC71.Name = "rhombusUC71";
            this.rhombusUC71.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC71.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC71.TabIndex = 3268;
            // 
            // lineUC232
            // 
            this.lineUC232.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC232.ExtenderWidth = 3;
            this.lineUC232.Fill = false;
            this.lineUC232.GroupID = null;
            this.lineUC232.Location = new System.Drawing.Point(918, 734);
            this.lineUC232.Name = "lineUC232";
            this.lineUC232.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC232.Size = new System.Drawing.Size(3, 50);
            this.lineUC232.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC232.TabIndex = 3274;
            // 
            // retangleUC108
            // 
            this.retangleUC108.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC108.ExtenderWidth = 3;
            this.retangleUC108.Fill = false;
            this.retangleUC108.GroupID = null;
            this.retangleUC108.Location = new System.Drawing.Point(938, 800);
            this.retangleUC108.Name = "retangleUC108";
            this.retangleUC108.Size = new System.Drawing.Size(15, 15);
            this.retangleUC108.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC108.TabIndex = 3273;
            // 
            // retangleUC109
            // 
            this.retangleUC109.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC109.ExtenderWidth = 3;
            this.retangleUC109.Fill = false;
            this.retangleUC109.GroupID = null;
            this.retangleUC109.Location = new System.Drawing.Point(887, 800);
            this.retangleUC109.Name = "retangleUC109";
            this.retangleUC109.Size = new System.Drawing.Size(15, 15);
            this.retangleUC109.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC109.TabIndex = 3272;
            // 
            // lineUC233
            // 
            this.lineUC233.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC233.ExtenderWidth = 3;
            this.lineUC233.Fill = false;
            this.lineUC233.GroupID = null;
            this.lineUC233.Location = new System.Drawing.Point(943, 784);
            this.lineUC233.Name = "lineUC233";
            this.lineUC233.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC233.Size = new System.Drawing.Size(3, 60);
            this.lineUC233.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC233.TabIndex = 3271;
            // 
            // lineUC234
            // 
            this.lineUC234.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC234.ExtenderWidth = 3;
            this.lineUC234.Fill = false;
            this.lineUC234.GroupID = null;
            this.lineUC234.Location = new System.Drawing.Point(894, 784);
            this.lineUC234.Name = "lineUC234";
            this.lineUC234.Size = new System.Drawing.Size(50, 3);
            this.lineUC234.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC234.TabIndex = 3270;
            // 
            // lineUC235
            // 
            this.lineUC235.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC235.ExtenderWidth = 3;
            this.lineUC235.Fill = false;
            this.lineUC235.GroupID = null;
            this.lineUC235.Location = new System.Drawing.Point(894, 784);
            this.lineUC235.Name = "lineUC235";
            this.lineUC235.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC235.Size = new System.Drawing.Size(3, 60);
            this.lineUC235.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC235.TabIndex = 3269;
            // 
            // label179
            // 
            this.label179.AutoSize = true;
            this.label179.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label179.Location = new System.Drawing.Point(1440, 754);
            this.label179.Name = "label179";
            this.label179.Size = new System.Drawing.Size(23, 15);
            this.label179.TabIndex = 3357;
            this.label179.Text = "Q1";
            // 
            // label180
            // 
            this.label180.AutoSize = true;
            this.label180.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label180.Location = new System.Drawing.Point(1418, 800);
            this.label180.Name = "label180";
            this.label180.Size = new System.Drawing.Size(21, 15);
            this.label180.TabIndex = 3356;
            this.label180.Text = "K1";
            // 
            // label181
            // 
            this.label181.AutoSize = true;
            this.label181.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label181.Location = new System.Drawing.Point(1466, 803);
            this.label181.Name = "label181";
            this.label181.Size = new System.Drawing.Size(21, 15);
            this.label181.TabIndex = 3355;
            this.label181.Text = "K3";
            // 
            // rhombusUC72
            // 
            this.rhombusUC72.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC72.ExtenderWidth = 2;
            this.rhombusUC72.Fill = false;
            this.rhombusUC72.GroupID = null;
            this.rhombusUC72.Location = new System.Drawing.Point(1462, 754);
            this.rhombusUC72.Name = "rhombusUC72";
            this.rhombusUC72.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC72.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC72.TabIndex = 3348;
            // 
            // lineUC236
            // 
            this.lineUC236.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC236.ExtenderWidth = 3;
            this.lineUC236.Fill = false;
            this.lineUC236.GroupID = null;
            this.lineUC236.Location = new System.Drawing.Point(1469, 734);
            this.lineUC236.Name = "lineUC236";
            this.lineUC236.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC236.Size = new System.Drawing.Size(3, 50);
            this.lineUC236.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC236.TabIndex = 3354;
            // 
            // retangleUC110
            // 
            this.retangleUC110.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC110.ExtenderWidth = 3;
            this.retangleUC110.Fill = false;
            this.retangleUC110.GroupID = null;
            this.retangleUC110.Location = new System.Drawing.Point(1489, 800);
            this.retangleUC110.Name = "retangleUC110";
            this.retangleUC110.Size = new System.Drawing.Size(15, 15);
            this.retangleUC110.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC110.TabIndex = 3353;
            // 
            // retangleUC111
            // 
            this.retangleUC111.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC111.ExtenderWidth = 3;
            this.retangleUC111.Fill = false;
            this.retangleUC111.GroupID = null;
            this.retangleUC111.Location = new System.Drawing.Point(1439, 800);
            this.retangleUC111.Name = "retangleUC111";
            this.retangleUC111.Size = new System.Drawing.Size(15, 15);
            this.retangleUC111.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC111.TabIndex = 3352;
            // 
            // lineUC237
            // 
            this.lineUC237.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC237.ExtenderWidth = 3;
            this.lineUC237.Fill = false;
            this.lineUC237.GroupID = null;
            this.lineUC237.Location = new System.Drawing.Point(1495, 784);
            this.lineUC237.Name = "lineUC237";
            this.lineUC237.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC237.Size = new System.Drawing.Size(3, 96);
            this.lineUC237.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC237.TabIndex = 3351;
            // 
            // lineUC238
            // 
            this.lineUC238.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC238.ExtenderWidth = 3;
            this.lineUC238.Fill = false;
            this.lineUC238.GroupID = null;
            this.lineUC238.Location = new System.Drawing.Point(1445, 784);
            this.lineUC238.Name = "lineUC238";
            this.lineUC238.Size = new System.Drawing.Size(50, 3);
            this.lineUC238.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC238.TabIndex = 3350;
            // 
            // lineUC239
            // 
            this.lineUC239.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC239.ExtenderWidth = 3;
            this.lineUC239.Fill = false;
            this.lineUC239.GroupID = null;
            this.lineUC239.Location = new System.Drawing.Point(1445, 784);
            this.lineUC239.Name = "lineUC239";
            this.lineUC239.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC239.Size = new System.Drawing.Size(3, 96);
            this.lineUC239.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC239.TabIndex = 3349;
            // 
            // label182
            // 
            this.label182.AutoSize = true;
            this.label182.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label182.Location = new System.Drawing.Point(1505, 800);
            this.label182.Name = "label182";
            this.label182.Size = new System.Drawing.Size(21, 15);
            this.label182.TabIndex = 3347;
            this.label182.Text = "K2";
            // 
            // label183
            // 
            this.label183.AutoSize = true;
            this.label183.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label183.Location = new System.Drawing.Point(1527, 754);
            this.label183.Name = "label183";
            this.label183.Size = new System.Drawing.Size(23, 15);
            this.label183.TabIndex = 3346;
            this.label183.Text = "Q2";
            // 
            // label184
            // 
            this.label184.AutoSize = true;
            this.label184.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label184.Location = new System.Drawing.Point(1553, 803);
            this.label184.Name = "label184";
            this.label184.Size = new System.Drawing.Size(21, 15);
            this.label184.TabIndex = 3345;
            this.label184.Text = "K4";
            // 
            // rhombusUC73
            // 
            this.rhombusUC73.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC73.ExtenderWidth = 2;
            this.rhombusUC73.Fill = false;
            this.rhombusUC73.GroupID = null;
            this.rhombusUC73.Location = new System.Drawing.Point(1549, 754);
            this.rhombusUC73.Name = "rhombusUC73";
            this.rhombusUC73.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC73.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC73.TabIndex = 3338;
            // 
            // lineUC240
            // 
            this.lineUC240.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC240.ExtenderWidth = 3;
            this.lineUC240.Fill = false;
            this.lineUC240.GroupID = null;
            this.lineUC240.Location = new System.Drawing.Point(1556, 734);
            this.lineUC240.Name = "lineUC240";
            this.lineUC240.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC240.Size = new System.Drawing.Size(3, 50);
            this.lineUC240.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC240.TabIndex = 3344;
            // 
            // retangleUC112
            // 
            this.retangleUC112.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC112.ExtenderWidth = 3;
            this.retangleUC112.Fill = false;
            this.retangleUC112.GroupID = null;
            this.retangleUC112.Location = new System.Drawing.Point(1576, 800);
            this.retangleUC112.Name = "retangleUC112";
            this.retangleUC112.Size = new System.Drawing.Size(15, 15);
            this.retangleUC112.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC112.TabIndex = 3343;
            // 
            // retangleUC113
            // 
            this.retangleUC113.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC113.ExtenderWidth = 3;
            this.retangleUC113.Fill = false;
            this.retangleUC113.GroupID = null;
            this.retangleUC113.Location = new System.Drawing.Point(1526, 800);
            this.retangleUC113.Name = "retangleUC113";
            this.retangleUC113.Size = new System.Drawing.Size(15, 15);
            this.retangleUC113.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC113.TabIndex = 3342;
            // 
            // lineUC241
            // 
            this.lineUC241.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC241.ExtenderWidth = 3;
            this.lineUC241.Fill = false;
            this.lineUC241.GroupID = null;
            this.lineUC241.Location = new System.Drawing.Point(1582, 784);
            this.lineUC241.Name = "lineUC241";
            this.lineUC241.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC241.Size = new System.Drawing.Size(3, 60);
            this.lineUC241.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC241.TabIndex = 3341;
            // 
            // lineUC242
            // 
            this.lineUC242.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC242.ExtenderWidth = 3;
            this.lineUC242.Fill = false;
            this.lineUC242.GroupID = null;
            this.lineUC242.Location = new System.Drawing.Point(1532, 784);
            this.lineUC242.Name = "lineUC242";
            this.lineUC242.Size = new System.Drawing.Size(50, 3);
            this.lineUC242.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC242.TabIndex = 3340;
            // 
            // lineUC243
            // 
            this.lineUC243.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC243.ExtenderWidth = 3;
            this.lineUC243.Fill = false;
            this.lineUC243.GroupID = null;
            this.lineUC243.Location = new System.Drawing.Point(1532, 784);
            this.lineUC243.Name = "lineUC243";
            this.lineUC243.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC243.Size = new System.Drawing.Size(3, 60);
            this.lineUC243.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC243.TabIndex = 3339;
            // 
            // label185
            // 
            this.label185.AutoSize = true;
            this.label185.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label185.Location = new System.Drawing.Point(1264, 754);
            this.label185.Name = "label185";
            this.label185.Size = new System.Drawing.Size(23, 15);
            this.label185.TabIndex = 3337;
            this.label185.Text = "Q1";
            // 
            // label186
            // 
            this.label186.AutoSize = true;
            this.label186.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label186.Location = new System.Drawing.Point(1242, 800);
            this.label186.Name = "label186";
            this.label186.Size = new System.Drawing.Size(21, 15);
            this.label186.TabIndex = 3336;
            this.label186.Text = "K1";
            // 
            // label187
            // 
            this.label187.AutoSize = true;
            this.label187.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label187.Location = new System.Drawing.Point(1290, 803);
            this.label187.Name = "label187";
            this.label187.Size = new System.Drawing.Size(21, 15);
            this.label187.TabIndex = 3335;
            this.label187.Text = "K3";
            // 
            // rhombusUC74
            // 
            this.rhombusUC74.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC74.ExtenderWidth = 2;
            this.rhombusUC74.Fill = false;
            this.rhombusUC74.GroupID = null;
            this.rhombusUC74.Location = new System.Drawing.Point(1286, 754);
            this.rhombusUC74.Name = "rhombusUC74";
            this.rhombusUC74.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC74.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC74.TabIndex = 3328;
            // 
            // lineUC244
            // 
            this.lineUC244.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC244.ExtenderWidth = 3;
            this.lineUC244.Fill = false;
            this.lineUC244.GroupID = null;
            this.lineUC244.Location = new System.Drawing.Point(1293, 734);
            this.lineUC244.Name = "lineUC244";
            this.lineUC244.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC244.Size = new System.Drawing.Size(3, 50);
            this.lineUC244.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC244.TabIndex = 3334;
            // 
            // retangleUC114
            // 
            this.retangleUC114.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC114.ExtenderWidth = 3;
            this.retangleUC114.Fill = false;
            this.retangleUC114.GroupID = null;
            this.retangleUC114.Location = new System.Drawing.Point(1313, 800);
            this.retangleUC114.Name = "retangleUC114";
            this.retangleUC114.Size = new System.Drawing.Size(15, 15);
            this.retangleUC114.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC114.TabIndex = 3333;
            // 
            // retangleUC115
            // 
            this.retangleUC115.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC115.ExtenderWidth = 3;
            this.retangleUC115.Fill = false;
            this.retangleUC115.GroupID = null;
            this.retangleUC115.Location = new System.Drawing.Point(1263, 800);
            this.retangleUC115.Name = "retangleUC115";
            this.retangleUC115.Size = new System.Drawing.Size(15, 15);
            this.retangleUC115.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC115.TabIndex = 3332;
            // 
            // lineUC245
            // 
            this.lineUC245.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC245.ExtenderWidth = 3;
            this.lineUC245.Fill = false;
            this.lineUC245.GroupID = null;
            this.lineUC245.Location = new System.Drawing.Point(1319, 784);
            this.lineUC245.Name = "lineUC245";
            this.lineUC245.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC245.Size = new System.Drawing.Size(3, 96);
            this.lineUC245.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC245.TabIndex = 3331;
            // 
            // lineUC246
            // 
            this.lineUC246.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC246.ExtenderWidth = 3;
            this.lineUC246.Fill = false;
            this.lineUC246.GroupID = null;
            this.lineUC246.Location = new System.Drawing.Point(1269, 784);
            this.lineUC246.Name = "lineUC246";
            this.lineUC246.Size = new System.Drawing.Size(50, 3);
            this.lineUC246.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC246.TabIndex = 3330;
            // 
            // lineUC247
            // 
            this.lineUC247.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC247.ExtenderWidth = 3;
            this.lineUC247.Fill = false;
            this.lineUC247.GroupID = null;
            this.lineUC247.Location = new System.Drawing.Point(1269, 784);
            this.lineUC247.Name = "lineUC247";
            this.lineUC247.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC247.Size = new System.Drawing.Size(3, 96);
            this.lineUC247.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC247.TabIndex = 3329;
            // 
            // label188
            // 
            this.label188.AutoSize = true;
            this.label188.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label188.Location = new System.Drawing.Point(1329, 800);
            this.label188.Name = "label188";
            this.label188.Size = new System.Drawing.Size(21, 15);
            this.label188.TabIndex = 3327;
            this.label188.Text = "K2";
            // 
            // label189
            // 
            this.label189.AutoSize = true;
            this.label189.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label189.Location = new System.Drawing.Point(1351, 754);
            this.label189.Name = "label189";
            this.label189.Size = new System.Drawing.Size(23, 15);
            this.label189.TabIndex = 3326;
            this.label189.Text = "Q2";
            // 
            // label190
            // 
            this.label190.AutoSize = true;
            this.label190.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label190.Location = new System.Drawing.Point(1377, 803);
            this.label190.Name = "label190";
            this.label190.Size = new System.Drawing.Size(21, 15);
            this.label190.TabIndex = 3325;
            this.label190.Text = "K4";
            // 
            // rhombusUC75
            // 
            this.rhombusUC75.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC75.ExtenderWidth = 2;
            this.rhombusUC75.Fill = false;
            this.rhombusUC75.GroupID = null;
            this.rhombusUC75.Location = new System.Drawing.Point(1373, 754);
            this.rhombusUC75.Name = "rhombusUC75";
            this.rhombusUC75.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC75.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC75.TabIndex = 3318;
            // 
            // lineUC248
            // 
            this.lineUC248.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC248.ExtenderWidth = 3;
            this.lineUC248.Fill = false;
            this.lineUC248.GroupID = null;
            this.lineUC248.Location = new System.Drawing.Point(1380, 734);
            this.lineUC248.Name = "lineUC248";
            this.lineUC248.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC248.Size = new System.Drawing.Size(3, 50);
            this.lineUC248.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC248.TabIndex = 3324;
            // 
            // retangleUC116
            // 
            this.retangleUC116.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC116.ExtenderWidth = 3;
            this.retangleUC116.Fill = false;
            this.retangleUC116.GroupID = null;
            this.retangleUC116.Location = new System.Drawing.Point(1400, 800);
            this.retangleUC116.Name = "retangleUC116";
            this.retangleUC116.Size = new System.Drawing.Size(15, 15);
            this.retangleUC116.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC116.TabIndex = 3323;
            // 
            // retangleUC117
            // 
            this.retangleUC117.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC117.ExtenderWidth = 3;
            this.retangleUC117.Fill = false;
            this.retangleUC117.GroupID = null;
            this.retangleUC117.Location = new System.Drawing.Point(1350, 800);
            this.retangleUC117.Name = "retangleUC117";
            this.retangleUC117.Size = new System.Drawing.Size(15, 15);
            this.retangleUC117.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC117.TabIndex = 3322;
            // 
            // lineUC249
            // 
            this.lineUC249.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC249.ExtenderWidth = 3;
            this.lineUC249.Fill = false;
            this.lineUC249.GroupID = null;
            this.lineUC249.Location = new System.Drawing.Point(1406, 784);
            this.lineUC249.Name = "lineUC249";
            this.lineUC249.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC249.Size = new System.Drawing.Size(3, 60);
            this.lineUC249.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC249.TabIndex = 3321;
            // 
            // lineUC250
            // 
            this.lineUC250.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC250.ExtenderWidth = 3;
            this.lineUC250.Fill = false;
            this.lineUC250.GroupID = null;
            this.lineUC250.Location = new System.Drawing.Point(1356, 784);
            this.lineUC250.Name = "lineUC250";
            this.lineUC250.Size = new System.Drawing.Size(50, 3);
            this.lineUC250.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC250.TabIndex = 3320;
            // 
            // lineUC251
            // 
            this.lineUC251.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC251.ExtenderWidth = 3;
            this.lineUC251.Fill = false;
            this.lineUC251.GroupID = null;
            this.lineUC251.Location = new System.Drawing.Point(1356, 784);
            this.lineUC251.Name = "lineUC251";
            this.lineUC251.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC251.Size = new System.Drawing.Size(3, 60);
            this.lineUC251.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC251.TabIndex = 3319;
            // 
            // retangleUC118
            // 
            this.retangleUC118.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC118.ExtenderWidth = 3;
            this.retangleUC118.Fill = false;
            this.retangleUC118.GroupID = null;
            this.retangleUC118.Location = new System.Drawing.Point(173, 249);
            this.retangleUC118.Name = "retangleUC118";
            this.retangleUC118.Size = new System.Drawing.Size(65, 16);
            this.retangleUC118.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC118.TabIndex = 3358;
            // 
            // retangleUC119
            // 
            this.retangleUC119.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC119.ExtenderWidth = 3;
            this.retangleUC119.Fill = false;
            this.retangleUC119.GroupID = null;
            this.retangleUC119.Location = new System.Drawing.Point(262, 285);
            this.retangleUC119.Name = "retangleUC119";
            this.retangleUC119.Size = new System.Drawing.Size(65, 16);
            this.retangleUC119.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC119.TabIndex = 3359;
            // 
            // retangleUC120
            // 
            this.retangleUC120.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC120.ExtenderWidth = 3;
            this.retangleUC120.Fill = false;
            this.retangleUC120.GroupID = null;
            this.retangleUC120.Location = new System.Drawing.Point(349, 249);
            this.retangleUC120.Name = "retangleUC120";
            this.retangleUC120.Size = new System.Drawing.Size(65, 16);
            this.retangleUC120.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC120.TabIndex = 3360;
            // 
            // retangleUC121
            // 
            this.retangleUC121.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC121.ExtenderWidth = 3;
            this.retangleUC121.Fill = false;
            this.retangleUC121.GroupID = null;
            this.retangleUC121.Location = new System.Drawing.Point(438, 285);
            this.retangleUC121.Name = "retangleUC121";
            this.retangleUC121.Size = new System.Drawing.Size(65, 16);
            this.retangleUC121.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC121.TabIndex = 3361;
            // 
            // retangleUC125
            // 
            this.retangleUC125.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC125.ExtenderWidth = 3;
            this.retangleUC125.Fill = false;
            this.retangleUC125.GroupID = null;
            this.retangleUC125.Location = new System.Drawing.Point(525, 249);
            this.retangleUC125.Name = "retangleUC125";
            this.retangleUC125.Size = new System.Drawing.Size(65, 16);
            this.retangleUC125.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC125.TabIndex = 3362;
            // 
            // retangleUC126
            // 
            this.retangleUC126.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC126.ExtenderWidth = 3;
            this.retangleUC126.Fill = false;
            this.retangleUC126.GroupID = null;
            this.retangleUC126.Location = new System.Drawing.Point(644, 285);
            this.retangleUC126.Name = "retangleUC126";
            this.retangleUC126.Size = new System.Drawing.Size(65, 16);
            this.retangleUC126.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC126.TabIndex = 3363;
            // 
            // retangleUC127
            // 
            this.retangleUC127.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC127.ExtenderWidth = 3;
            this.retangleUC127.Fill = false;
            this.retangleUC127.GroupID = null;
            this.retangleUC127.Location = new System.Drawing.Point(731, 249);
            this.retangleUC127.Name = "retangleUC127";
            this.retangleUC127.Size = new System.Drawing.Size(65, 16);
            this.retangleUC127.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC127.TabIndex = 3364;
            // 
            // retangleUC128
            // 
            this.retangleUC128.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC128.ExtenderWidth = 3;
            this.retangleUC128.Fill = false;
            this.retangleUC128.GroupID = null;
            this.retangleUC128.Location = new System.Drawing.Point(858, 285);
            this.retangleUC128.Name = "retangleUC128";
            this.retangleUC128.Size = new System.Drawing.Size(65, 16);
            this.retangleUC128.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC128.TabIndex = 3365;
            // 
            // retangleUC129
            // 
            this.retangleUC129.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC129.ExtenderWidth = 3;
            this.retangleUC129.Fill = false;
            this.retangleUC129.GroupID = null;
            this.retangleUC129.Location = new System.Drawing.Point(945, 249);
            this.retangleUC129.Name = "retangleUC129";
            this.retangleUC129.Size = new System.Drawing.Size(65, 16);
            this.retangleUC129.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC129.TabIndex = 3366;
            // 
            // retangleUC130
            // 
            this.retangleUC130.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC130.ExtenderWidth = 3;
            this.retangleUC130.Fill = false;
            this.retangleUC130.GroupID = null;
            this.retangleUC130.Location = new System.Drawing.Point(1034, 285);
            this.retangleUC130.Name = "retangleUC130";
            this.retangleUC130.Size = new System.Drawing.Size(65, 16);
            this.retangleUC130.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC130.TabIndex = 3367;
            // 
            // retangleUC131
            // 
            this.retangleUC131.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC131.ExtenderWidth = 3;
            this.retangleUC131.Fill = false;
            this.retangleUC131.GroupID = null;
            this.retangleUC131.Location = new System.Drawing.Point(1121, 249);
            this.retangleUC131.Name = "retangleUC131";
            this.retangleUC131.Size = new System.Drawing.Size(65, 16);
            this.retangleUC131.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC131.TabIndex = 3368;
            // 
            // retangleUC132
            // 
            this.retangleUC132.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC132.ExtenderWidth = 3;
            this.retangleUC132.Fill = false;
            this.retangleUC132.GroupID = null;
            this.retangleUC132.Location = new System.Drawing.Point(1223, 285);
            this.retangleUC132.Name = "retangleUC132";
            this.retangleUC132.Size = new System.Drawing.Size(65, 16);
            this.retangleUC132.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC132.TabIndex = 3369;
            // 
            // retangleUC133
            // 
            this.retangleUC133.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC133.ExtenderWidth = 3;
            this.retangleUC133.Fill = false;
            this.retangleUC133.GroupID = null;
            this.retangleUC133.Location = new System.Drawing.Point(1310, 249);
            this.retangleUC133.Name = "retangleUC133";
            this.retangleUC133.Size = new System.Drawing.Size(65, 16);
            this.retangleUC133.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC133.TabIndex = 3370;
            // 
            // retangleUC134
            // 
            this.retangleUC134.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC134.ExtenderWidth = 3;
            this.retangleUC134.Fill = false;
            this.retangleUC134.GroupID = null;
            this.retangleUC134.Location = new System.Drawing.Point(1419, 285);
            this.retangleUC134.Name = "retangleUC134";
            this.retangleUC134.Size = new System.Drawing.Size(65, 16);
            this.retangleUC134.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC134.TabIndex = 3371;
            // 
            // retangleUC135
            // 
            this.retangleUC135.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC135.ExtenderWidth = 3;
            this.retangleUC135.Fill = false;
            this.retangleUC135.GroupID = null;
            this.retangleUC135.Location = new System.Drawing.Point(1506, 249);
            this.retangleUC135.Name = "retangleUC135";
            this.retangleUC135.Size = new System.Drawing.Size(65, 16);
            this.retangleUC135.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC135.TabIndex = 3372;
            // 
            // retangleUC136
            // 
            this.retangleUC136.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC136.ExtenderWidth = 3;
            this.retangleUC136.Fill = false;
            this.retangleUC136.GroupID = null;
            this.retangleUC136.Location = new System.Drawing.Point(1592, 249);
            this.retangleUC136.Name = "retangleUC136";
            this.retangleUC136.Size = new System.Drawing.Size(65, 16);
            this.retangleUC136.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC136.TabIndex = 3373;
            // 
            // retangleUC137
            // 
            this.retangleUC137.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC137.ExtenderWidth = 3;
            this.retangleUC137.Fill = false;
            this.retangleUC137.GroupID = null;
            this.retangleUC137.Location = new System.Drawing.Point(1744, 285);
            this.retangleUC137.Name = "retangleUC137";
            this.retangleUC137.Size = new System.Drawing.Size(65, 16);
            this.retangleUC137.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC137.TabIndex = 3374;
            // 
            // retangleUC138
            // 
            this.retangleUC138.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC138.ExtenderWidth = 3;
            this.retangleUC138.Fill = false;
            this.retangleUC138.GroupID = null;
            this.retangleUC138.Location = new System.Drawing.Point(1831, 249);
            this.retangleUC138.Name = "retangleUC138";
            this.retangleUC138.Size = new System.Drawing.Size(65, 16);
            this.retangleUC138.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC138.TabIndex = 3375;
            // 
            // retangleUC139
            // 
            this.retangleUC139.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC139.ExtenderWidth = 3;
            this.retangleUC139.Fill = false;
            this.retangleUC139.GroupID = null;
            this.retangleUC139.Location = new System.Drawing.Point(131, 547);
            this.retangleUC139.Name = "retangleUC139";
            this.retangleUC139.Size = new System.Drawing.Size(65, 16);
            this.retangleUC139.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC139.TabIndex = 3376;
            // 
            // retangleUC140
            // 
            this.retangleUC140.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC140.ExtenderWidth = 3;
            this.retangleUC140.Fill = false;
            this.retangleUC140.GroupID = null;
            this.retangleUC140.Location = new System.Drawing.Point(217, 547);
            this.retangleUC140.Name = "retangleUC140";
            this.retangleUC140.Size = new System.Drawing.Size(65, 16);
            this.retangleUC140.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC140.TabIndex = 3377;
            // 
            // retangleUC141
            // 
            this.retangleUC141.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC141.ExtenderWidth = 3;
            this.retangleUC141.Fill = false;
            this.retangleUC141.GroupID = null;
            this.retangleUC141.Location = new System.Drawing.Point(325, 583);
            this.retangleUC141.Name = "retangleUC141";
            this.retangleUC141.Size = new System.Drawing.Size(65, 16);
            this.retangleUC141.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC141.TabIndex = 3378;
            // 
            // retangleUC142
            // 
            this.retangleUC142.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC142.ExtenderWidth = 3;
            this.retangleUC142.Fill = false;
            this.retangleUC142.GroupID = null;
            this.retangleUC142.Location = new System.Drawing.Point(412, 547);
            this.retangleUC142.Name = "retangleUC142";
            this.retangleUC142.Size = new System.Drawing.Size(65, 16);
            this.retangleUC142.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC142.TabIndex = 3379;
            // 
            // retangleUC143
            // 
            this.retangleUC143.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC143.ExtenderWidth = 3;
            this.retangleUC143.Fill = false;
            this.retangleUC143.GroupID = null;
            this.retangleUC143.Location = new System.Drawing.Point(498, 583);
            this.retangleUC143.Name = "retangleUC143";
            this.retangleUC143.Size = new System.Drawing.Size(65, 16);
            this.retangleUC143.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC143.TabIndex = 3380;
            // 
            // retangleUC144
            // 
            this.retangleUC144.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC144.ExtenderWidth = 3;
            this.retangleUC144.Fill = false;
            this.retangleUC144.GroupID = null;
            this.retangleUC144.Location = new System.Drawing.Point(627, 583);
            this.retangleUC144.Name = "retangleUC144";
            this.retangleUC144.Size = new System.Drawing.Size(65, 16);
            this.retangleUC144.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC144.TabIndex = 3381;
            // 
            // retangleUC145
            // 
            this.retangleUC145.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC145.ExtenderWidth = 3;
            this.retangleUC145.Fill = false;
            this.retangleUC145.GroupID = null;
            this.retangleUC145.Location = new System.Drawing.Point(714, 547);
            this.retangleUC145.Name = "retangleUC145";
            this.retangleUC145.Size = new System.Drawing.Size(65, 16);
            this.retangleUC145.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC145.TabIndex = 3382;
            // 
            // retangleUC146
            // 
            this.retangleUC146.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC146.ExtenderWidth = 3;
            this.retangleUC146.Fill = false;
            this.retangleUC146.GroupID = null;
            this.retangleUC146.Location = new System.Drawing.Point(800, 547);
            this.retangleUC146.Name = "retangleUC146";
            this.retangleUC146.Size = new System.Drawing.Size(65, 16);
            this.retangleUC146.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC146.TabIndex = 3383;
            // 
            // retangleUC147
            // 
            this.retangleUC147.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC147.ExtenderWidth = 3;
            this.retangleUC147.Fill = false;
            this.retangleUC147.GroupID = null;
            this.retangleUC147.Location = new System.Drawing.Point(907, 583);
            this.retangleUC147.Name = "retangleUC147";
            this.retangleUC147.Size = new System.Drawing.Size(65, 16);
            this.retangleUC147.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC147.TabIndex = 3384;
            // 
            // retangleUC148
            // 
            this.retangleUC148.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC148.ExtenderWidth = 3;
            this.retangleUC148.Fill = false;
            this.retangleUC148.GroupID = null;
            this.retangleUC148.Location = new System.Drawing.Point(994, 547);
            this.retangleUC148.Name = "retangleUC148";
            this.retangleUC148.Size = new System.Drawing.Size(65, 16);
            this.retangleUC148.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC148.TabIndex = 3385;
            // 
            // retangleUC149
            // 
            this.retangleUC149.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC149.ExtenderWidth = 3;
            this.retangleUC149.Fill = false;
            this.retangleUC149.GroupID = null;
            this.retangleUC149.Location = new System.Drawing.Point(1081, 583);
            this.retangleUC149.Name = "retangleUC149";
            this.retangleUC149.Size = new System.Drawing.Size(65, 16);
            this.retangleUC149.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC149.TabIndex = 3386;
            // 
            // retangleUC150
            // 
            this.retangleUC150.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC150.ExtenderWidth = 3;
            this.retangleUC150.Fill = false;
            this.retangleUC150.GroupID = null;
            this.retangleUC150.Location = new System.Drawing.Point(1168, 547);
            this.retangleUC150.Name = "retangleUC150";
            this.retangleUC150.Size = new System.Drawing.Size(65, 16);
            this.retangleUC150.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC150.TabIndex = 3387;
            // 
            // retangleUC151
            // 
            this.retangleUC151.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC151.ExtenderWidth = 3;
            this.retangleUC151.Fill = false;
            this.retangleUC151.GroupID = null;
            this.retangleUC151.Location = new System.Drawing.Point(1254, 547);
            this.retangleUC151.Name = "retangleUC151";
            this.retangleUC151.Size = new System.Drawing.Size(65, 16);
            this.retangleUC151.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC151.TabIndex = 3388;
            // 
            // retangleUC152
            // 
            this.retangleUC152.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC152.ExtenderWidth = 3;
            this.retangleUC152.Fill = false;
            this.retangleUC152.GroupID = null;
            this.retangleUC152.Location = new System.Drawing.Point(1368, 583);
            this.retangleUC152.Name = "retangleUC152";
            this.retangleUC152.Size = new System.Drawing.Size(65, 16);
            this.retangleUC152.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC152.TabIndex = 3389;
            // 
            // retangleUC153
            // 
            this.retangleUC153.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC153.ExtenderWidth = 3;
            this.retangleUC153.Fill = false;
            this.retangleUC153.GroupID = null;
            this.retangleUC153.Location = new System.Drawing.Point(1455, 547);
            this.retangleUC153.Name = "retangleUC153";
            this.retangleUC153.Size = new System.Drawing.Size(65, 16);
            this.retangleUC153.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC153.TabIndex = 3390;
            // 
            // retangleUC154
            // 
            this.retangleUC154.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC154.ExtenderWidth = 3;
            this.retangleUC154.Fill = false;
            this.retangleUC154.GroupID = null;
            this.retangleUC154.Location = new System.Drawing.Point(1542, 583);
            this.retangleUC154.Name = "retangleUC154";
            this.retangleUC154.Size = new System.Drawing.Size(65, 16);
            this.retangleUC154.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC154.TabIndex = 3391;
            // 
            // retangleUC155
            // 
            this.retangleUC155.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC155.ExtenderWidth = 3;
            this.retangleUC155.Fill = false;
            this.retangleUC155.GroupID = null;
            this.retangleUC155.Location = new System.Drawing.Point(1629, 547);
            this.retangleUC155.Name = "retangleUC155";
            this.retangleUC155.Size = new System.Drawing.Size(65, 16);
            this.retangleUC155.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC155.TabIndex = 3392;
            // 
            // retangleUC156
            // 
            this.retangleUC156.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC156.ExtenderWidth = 3;
            this.retangleUC156.Fill = false;
            this.retangleUC156.GroupID = null;
            this.retangleUC156.Location = new System.Drawing.Point(1716, 583);
            this.retangleUC156.Name = "retangleUC156";
            this.retangleUC156.Size = new System.Drawing.Size(65, 16);
            this.retangleUC156.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC156.TabIndex = 3393;
            // 
            // retangleUC157
            // 
            this.retangleUC157.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC157.ExtenderWidth = 3;
            this.retangleUC157.Fill = false;
            this.retangleUC157.GroupID = null;
            this.retangleUC157.Location = new System.Drawing.Point(122, 844);
            this.retangleUC157.Name = "retangleUC157";
            this.retangleUC157.Size = new System.Drawing.Size(65, 16);
            this.retangleUC157.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC157.TabIndex = 3394;
            // 
            // retangleUC158
            // 
            this.retangleUC158.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC158.ExtenderWidth = 3;
            this.retangleUC158.Fill = false;
            this.retangleUC158.GroupID = null;
            this.retangleUC158.Location = new System.Drawing.Point(209, 880);
            this.retangleUC158.Name = "retangleUC158";
            this.retangleUC158.Size = new System.Drawing.Size(65, 16);
            this.retangleUC158.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC158.TabIndex = 3395;
            // 
            // retangleUC159
            // 
            this.retangleUC159.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC159.ExtenderWidth = 3;
            this.retangleUC159.Fill = false;
            this.retangleUC159.GroupID = null;
            this.retangleUC159.Location = new System.Drawing.Point(297, 880);
            this.retangleUC159.Name = "retangleUC159";
            this.retangleUC159.Size = new System.Drawing.Size(65, 16);
            this.retangleUC159.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC159.TabIndex = 3396;
            // 
            // retangleUC160
            // 
            this.retangleUC160.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC160.ExtenderWidth = 3;
            this.retangleUC160.Fill = false;
            this.retangleUC160.GroupID = null;
            this.retangleUC160.Location = new System.Drawing.Point(384, 844);
            this.retangleUC160.Name = "retangleUC160";
            this.retangleUC160.Size = new System.Drawing.Size(65, 16);
            this.retangleUC160.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC160.TabIndex = 3397;
            // 
            // retangleUC161
            // 
            this.retangleUC161.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC161.ExtenderWidth = 3;
            this.retangleUC161.Fill = false;
            this.retangleUC161.GroupID = null;
            this.retangleUC161.Location = new System.Drawing.Point(471, 880);
            this.retangleUC161.Name = "retangleUC161";
            this.retangleUC161.Size = new System.Drawing.Size(65, 16);
            this.retangleUC161.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC161.TabIndex = 3398;
            // 
            // retangleUC162
            // 
            this.retangleUC162.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC162.ExtenderWidth = 3;
            this.retangleUC162.Fill = false;
            this.retangleUC162.GroupID = null;
            this.retangleUC162.Location = new System.Drawing.Point(627, 880);
            this.retangleUC162.Name = "retangleUC162";
            this.retangleUC162.Size = new System.Drawing.Size(65, 16);
            this.retangleUC162.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC162.TabIndex = 3399;
            // 
            // retangleUC163
            // 
            this.retangleUC163.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC163.ExtenderWidth = 3;
            this.retangleUC163.Fill = false;
            this.retangleUC163.GroupID = null;
            this.retangleUC163.Location = new System.Drawing.Point(714, 844);
            this.retangleUC163.Name = "retangleUC163";
            this.retangleUC163.Size = new System.Drawing.Size(65, 16);
            this.retangleUC163.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC163.TabIndex = 3400;
            // 
            // retangleUC164
            // 
            this.retangleUC164.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC164.ExtenderWidth = 3;
            this.retangleUC164.Fill = false;
            this.retangleUC164.GroupID = null;
            this.retangleUC164.Location = new System.Drawing.Point(801, 880);
            this.retangleUC164.Name = "retangleUC164";
            this.retangleUC164.Size = new System.Drawing.Size(65, 16);
            this.retangleUC164.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC164.TabIndex = 3401;
            // 
            // retangleUC165
            // 
            this.retangleUC165.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC165.ExtenderWidth = 3;
            this.retangleUC165.Fill = false;
            this.retangleUC165.GroupID = null;
            this.retangleUC165.Location = new System.Drawing.Point(887, 844);
            this.retangleUC165.Name = "retangleUC165";
            this.retangleUC165.Size = new System.Drawing.Size(65, 16);
            this.retangleUC165.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC165.TabIndex = 3402;
            // 
            // retangleUC166
            // 
            this.retangleUC166.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC166.ExtenderWidth = 3;
            this.retangleUC166.Fill = false;
            this.retangleUC166.GroupID = null;
            this.retangleUC166.Location = new System.Drawing.Point(974, 844);
            this.retangleUC166.Name = "retangleUC166";
            this.retangleUC166.Size = new System.Drawing.Size(65, 16);
            this.retangleUC166.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC166.TabIndex = 3403;
            // 
            // retangleUC167
            // 
            this.retangleUC167.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC167.ExtenderWidth = 3;
            this.retangleUC167.Fill = false;
            this.retangleUC167.GroupID = null;
            this.retangleUC167.Location = new System.Drawing.Point(1088, 880);
            this.retangleUC167.Name = "retangleUC167";
            this.retangleUC167.Size = new System.Drawing.Size(65, 16);
            this.retangleUC167.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC167.TabIndex = 3404;
            // 
            // retangleUC168
            // 
            this.retangleUC168.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC168.ExtenderWidth = 3;
            this.retangleUC168.Fill = false;
            this.retangleUC168.GroupID = null;
            this.retangleUC168.Location = new System.Drawing.Point(1175, 844);
            this.retangleUC168.Name = "retangleUC168";
            this.retangleUC168.Size = new System.Drawing.Size(65, 16);
            this.retangleUC168.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC168.TabIndex = 3405;
            // 
            // retangleUC169
            // 
            this.retangleUC169.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC169.ExtenderWidth = 3;
            this.retangleUC169.Fill = false;
            this.retangleUC169.GroupID = null;
            this.retangleUC169.Location = new System.Drawing.Point(1263, 880);
            this.retangleUC169.Name = "retangleUC169";
            this.retangleUC169.Size = new System.Drawing.Size(65, 16);
            this.retangleUC169.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC169.TabIndex = 3406;
            // 
            // retangleUC170
            // 
            this.retangleUC170.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC170.ExtenderWidth = 3;
            this.retangleUC170.Fill = false;
            this.retangleUC170.GroupID = null;
            this.retangleUC170.Location = new System.Drawing.Point(1350, 844);
            this.retangleUC170.Name = "retangleUC170";
            this.retangleUC170.Size = new System.Drawing.Size(65, 16);
            this.retangleUC170.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC170.TabIndex = 3407;
            // 
            // retangleUC171
            // 
            this.retangleUC171.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC171.ExtenderWidth = 3;
            this.retangleUC171.Fill = false;
            this.retangleUC171.GroupID = null;
            this.retangleUC171.Location = new System.Drawing.Point(1526, 844);
            this.retangleUC171.Name = "retangleUC171";
            this.retangleUC171.Size = new System.Drawing.Size(65, 16);
            this.retangleUC171.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC171.TabIndex = 3408;
            // 
            // retangleUC172
            // 
            this.retangleUC172.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.retangleUC172.ExtenderWidth = 3;
            this.retangleUC172.Fill = false;
            this.retangleUC172.GroupID = null;
            this.retangleUC172.Location = new System.Drawing.Point(1439, 880);
            this.retangleUC172.Name = "retangleUC172";
            this.retangleUC172.Size = new System.Drawing.Size(65, 16);
            this.retangleUC172.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.retangleUC172.TabIndex = 3409;
            // 
            // label191
            // 
            this.label191.AutoSize = true;
            this.label191.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label191.Location = new System.Drawing.Point(119, 110);
            this.label191.Name = "label191";
            this.label191.Size = new System.Drawing.Size(95, 17);
            this.label191.TabIndex = 3410;
            this.label191.Text = "STATION C15";
            // 
            // label192
            // 
            this.label192.AutoSize = true;
            this.label192.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label192.Location = new System.Drawing.Point(296, 110);
            this.label192.Name = "label192";
            this.label192.Size = new System.Drawing.Size(95, 17);
            this.label192.TabIndex = 3411;
            this.label192.Text = "STATION C16";
            // 
            // label193
            // 
            this.label193.AutoSize = true;
            this.label193.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label193.Location = new System.Drawing.Point(466, 110);
            this.label193.Name = "label193";
            this.label193.Size = new System.Drawing.Size(95, 17);
            this.label193.TabIndex = 3412;
            this.label193.Text = "STATION C17";
            // 
            // lineUC252
            // 
            this.lineUC252.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC252.ExtenderWidth = 3;
            this.lineUC252.Fill = false;
            this.lineUC252.GroupID = null;
            this.lineUC252.Location = new System.Drawing.Point(645, 137);
            this.lineUC252.Name = "lineUC252";
            this.lineUC252.Size = new System.Drawing.Size(540, 3);
            this.lineUC252.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC252.TabIndex = 3413;
            // 
            // lineUC253
            // 
            this.lineUC253.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC253.ExtenderWidth = 3;
            this.lineUC253.Fill = false;
            this.lineUC253.GroupID = null;
            this.lineUC253.Location = new System.Drawing.Point(1241, 137);
            this.lineUC253.Name = "lineUC253";
            this.lineUC253.Size = new System.Drawing.Size(415, 3);
            this.lineUC253.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC253.TabIndex = 3414;
            // 
            // lineUC254
            // 
            this.lineUC254.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC254.ExtenderWidth = 3;
            this.lineUC254.Fill = false;
            this.lineUC254.GroupID = null;
            this.lineUC254.Location = new System.Drawing.Point(1715, 137);
            this.lineUC254.Name = "lineUC254";
            this.lineUC254.Size = new System.Drawing.Size(160, 3);
            this.lineUC254.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC254.TabIndex = 3415;
            // 
            // lineUC255
            // 
            this.lineUC255.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC255.ExtenderWidth = 3;
            this.lineUC255.Fill = false;
            this.lineUC255.GroupID = null;
            this.lineUC255.Location = new System.Drawing.Point(627, 435);
            this.lineUC255.Name = "lineUC255";
            this.lineUC255.Size = new System.Drawing.Size(687, 3);
            this.lineUC255.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC255.TabIndex = 3416;
            // 
            // lineUC256
            // 
            this.lineUC256.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC256.ExtenderWidth = 3;
            this.lineUC256.Fill = false;
            this.lineUC256.GroupID = null;
            this.lineUC256.Location = new System.Drawing.Point(1369, 435);
            this.lineUC256.Name = "lineUC256";
            this.lineUC256.Size = new System.Drawing.Size(500, 3);
            this.lineUC256.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC256.TabIndex = 3417;
            // 
            // lineUC257
            // 
            this.lineUC257.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC257.ExtenderWidth = 3;
            this.lineUC257.Fill = false;
            this.lineUC257.GroupID = null;
            this.lineUC257.Location = new System.Drawing.Point(633, 732);
            this.lineUC257.Name = "lineUC257";
            this.lineUC257.Size = new System.Drawing.Size(957, 3);
            this.lineUC257.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC257.TabIndex = 3418;
            // 
            // lineUC258
            // 
            this.lineUC258.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC258.ExtenderWidth = 3;
            this.lineUC258.Fill = false;
            this.lineUC258.GroupID = null;
            this.lineUC258.Location = new System.Drawing.Point(1647, 732);
            this.lineUC258.Name = "lineUC258";
            this.lineUC258.Size = new System.Drawing.Size(100, 3);
            this.lineUC258.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC258.TabIndex = 3419;
            // 
            // label194
            // 
            this.label194.AutoSize = true;
            this.label194.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label194.Location = new System.Drawing.Point(675, 110);
            this.label194.Name = "label194";
            this.label194.Size = new System.Drawing.Size(95, 17);
            this.label194.TabIndex = 3420;
            this.label194.Text = "STATION C18";
            // 
            // label195
            // 
            this.label195.AutoSize = true;
            this.label195.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label195.Location = new System.Drawing.Point(886, 110);
            this.label195.Name = "label195";
            this.label195.Size = new System.Drawing.Size(95, 17);
            this.label195.TabIndex = 3421;
            this.label195.Text = "STATION C19";
            // 
            // label196
            // 
            this.label196.AutoSize = true;
            this.label196.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label196.Location = new System.Drawing.Point(1062, 110);
            this.label196.Name = "label196";
            this.label196.Size = new System.Drawing.Size(95, 17);
            this.label196.TabIndex = 3422;
            this.label196.Text = "STATION C20";
            // 
            // label207
            // 
            this.label207.AutoSize = true;
            this.label207.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label207.Location = new System.Drawing.Point(1254, 110);
            this.label207.Name = "label207";
            this.label207.Size = new System.Drawing.Size(95, 17);
            this.label207.TabIndex = 3423;
            this.label207.Text = "STATION C21";
            // 
            // label208
            // 
            this.label208.AutoSize = true;
            this.label208.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label208.Location = new System.Drawing.Point(1481, 110);
            this.label208.Name = "label208";
            this.label208.Size = new System.Drawing.Size(95, 17);
            this.label208.TabIndex = 3424;
            this.label208.Text = "STATION C22";
            // 
            // label209
            // 
            this.label209.AutoSize = true;
            this.label209.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label209.Location = new System.Drawing.Point(1771, 110);
            this.label209.Name = "label209";
            this.label209.Size = new System.Drawing.Size(95, 17);
            this.label209.TabIndex = 3425;
            this.label209.Text = "STATION C23";
            // 
            // label210
            // 
            this.label210.AutoSize = true;
            this.label210.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label210.Location = new System.Drawing.Point(119, 408);
            this.label210.Name = "label210";
            this.label210.Size = new System.Drawing.Size(95, 17);
            this.label210.TabIndex = 3426;
            this.label210.Text = "STATION C24";
            // 
            // label211
            // 
            this.label211.AutoSize = true;
            this.label211.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label211.Location = new System.Drawing.Point(393, 408);
            this.label211.Name = "label211";
            this.label211.Size = new System.Drawing.Size(95, 17);
            this.label211.TabIndex = 3427;
            this.label211.Text = "STATION C25";
            // 
            // label212
            // 
            this.label212.AutoSize = true;
            this.label212.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label212.Location = new System.Drawing.Point(701, 408);
            this.label212.Name = "label212";
            this.label212.Size = new System.Drawing.Size(95, 17);
            this.label212.TabIndex = 3428;
            this.label212.Text = "STATION C26";
            // 
            // label213
            // 
            this.label213.AutoSize = true;
            this.label213.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label213.Location = new System.Drawing.Point(935, 408);
            this.label213.Name = "label213";
            this.label213.Size = new System.Drawing.Size(95, 17);
            this.label213.TabIndex = 3429;
            this.label213.Text = "STATION C27";
            // 
            // label214
            // 
            this.label214.AutoSize = true;
            this.label214.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label214.Location = new System.Drawing.Point(1149, 408);
            this.label214.Name = "label214";
            this.label214.Size = new System.Drawing.Size(95, 17);
            this.label214.TabIndex = 3430;
            this.label214.Text = "STATION C28";
            // 
            // label215
            // 
            this.label215.AutoSize = true;
            this.label215.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label215.Location = new System.Drawing.Point(1391, 408);
            this.label215.Name = "label215";
            this.label215.Size = new System.Drawing.Size(95, 17);
            this.label215.TabIndex = 3431;
            this.label215.Text = "STATION C29";
            // 
            // label216
            // 
            this.label216.AutoSize = true;
            this.label216.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label216.Location = new System.Drawing.Point(1609, 408);
            this.label216.Name = "label216";
            this.label216.Size = new System.Drawing.Size(95, 17);
            this.label216.TabIndex = 3432;
            this.label216.Text = "STATION C30";
            // 
            // label217
            // 
            this.label217.AutoSize = true;
            this.label217.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label217.Location = new System.Drawing.Point(112, 705);
            this.label217.Name = "label217";
            this.label217.Size = new System.Drawing.Size(95, 17);
            this.label217.TabIndex = 3433;
            this.label217.Text = "STATION C31";
            // 
            // label218
            // 
            this.label218.AutoSize = true;
            this.label218.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label218.Location = new System.Drawing.Point(371, 705);
            this.label218.Name = "label218";
            this.label218.Size = new System.Drawing.Size(95, 17);
            this.label218.TabIndex = 3434;
            this.label218.Text = "STATION C32";
            // 
            // label219
            // 
            this.label219.AutoSize = true;
            this.label219.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label219.Location = new System.Drawing.Point(651, 705);
            this.label219.Name = "label219";
            this.label219.Size = new System.Drawing.Size(95, 17);
            this.label219.TabIndex = 3435;
            this.label219.Text = "STATION C33";
            // 
            // label220
            // 
            this.label220.AutoSize = true;
            this.label220.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label220.Location = new System.Drawing.Point(873, 705);
            this.label220.Name = "label220";
            this.label220.Size = new System.Drawing.Size(95, 17);
            this.label220.TabIndex = 3436;
            this.label220.Text = "STATION C34";
            // 
            // label221
            // 
            this.label221.AutoSize = true;
            this.label221.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label221.Location = new System.Drawing.Point(1117, 705);
            this.label221.Name = "label221";
            this.label221.Size = new System.Drawing.Size(95, 17);
            this.label221.TabIndex = 3437;
            this.label221.Text = "STATION C35";
            // 
            // label222
            // 
            this.label222.AutoSize = true;
            this.label222.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label222.Location = new System.Drawing.Point(1293, 705);
            this.label222.Name = "label222";
            this.label222.Size = new System.Drawing.Size(95, 17);
            this.label222.TabIndex = 3438;
            this.label222.Text = "STATION C36";
            // 
            // label223
            // 
            this.label223.AutoSize = true;
            this.label223.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label223.Location = new System.Drawing.Point(1467, 705);
            this.label223.Name = "label223";
            this.label223.Size = new System.Drawing.Size(95, 17);
            this.label223.TabIndex = 3439;
            this.label223.Text = "STATION C37";
            // 
            // label224
            // 
            this.label224.AutoSize = true;
            this.label224.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label224.Location = new System.Drawing.Point(565, 16);
            this.label224.Name = "label224";
            this.label224.Size = new System.Drawing.Size(40, 17);
            this.label224.TabIndex = 3440;
            this.label224.Text = "TSS7";
            // 
            // label225
            // 
            this.label225.AutoSize = true;
            this.label225.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label225.Location = new System.Drawing.Point(1160, 16);
            this.label225.Name = "label225";
            this.label225.Size = new System.Drawing.Size(40, 17);
            this.label225.TabIndex = 3441;
            this.label225.Text = "TSS8";
            // 
            // label226
            // 
            this.label226.AutoSize = true;
            this.label226.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label226.Location = new System.Drawing.Point(1633, 16);
            this.label226.Name = "label226";
            this.label226.Size = new System.Drawing.Size(40, 17);
            this.label226.TabIndex = 3442;
            this.label226.Text = "TSS9";
            // 
            // label227
            // 
            this.label227.AutoSize = true;
            this.label227.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label227.Location = new System.Drawing.Point(538, 314);
            this.label227.Name = "label227";
            this.label227.Size = new System.Drawing.Size(48, 17);
            this.label227.TabIndex = 3443;
            this.label227.Text = "TSS10";
            // 
            // label228
            // 
            this.label228.AutoSize = true;
            this.label228.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label228.Location = new System.Drawing.Point(1281, 314);
            this.label228.Name = "label228";
            this.label228.Size = new System.Drawing.Size(48, 17);
            this.label228.TabIndex = 3444;
            this.label228.Text = "TSS11";
            // 
            // label229
            // 
            this.label229.AutoSize = true;
            this.label229.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label229.Location = new System.Drawing.Point(544, 611);
            this.label229.Name = "label229";
            this.label229.Size = new System.Drawing.Size(48, 17);
            this.label229.TabIndex = 3445;
            this.label229.Text = "TSS12";
            // 
            // label230
            // 
            this.label230.AutoSize = true;
            this.label230.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label230.Location = new System.Drawing.Point(1527, 611);
            this.label230.Name = "label230";
            this.label230.Size = new System.Drawing.Size(80, 17);
            this.label230.TabIndex = 3446;
            this.label230.Text = "DTSS-TSS1";
            // 
            // label231
            // 
            this.label231.AutoSize = true;
            this.label231.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label231.Location = new System.Drawing.Point(-1, 265);
            this.label231.Name = "label231";
            this.label231.Size = new System.Drawing.Size(82, 15);
            this.label231.TabIndex = 3447;
            this.label231.Text = "DOWN TRACK";
            // 
            // label232
            // 
            this.label232.AutoSize = true;
            this.label232.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label232.Location = new System.Drawing.Point(-1, 283);
            this.label232.Name = "label232";
            this.label232.Size = new System.Drawing.Size(60, 15);
            this.label232.TabIndex = 3448;
            this.label232.Text = "UP TRACK";
            // 
            // label233
            // 
            this.label233.AutoSize = true;
            this.label233.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label233.Location = new System.Drawing.Point(138, 581);
            this.label233.Name = "label233";
            this.label233.Size = new System.Drawing.Size(60, 15);
            this.label233.TabIndex = 3450;
            this.label233.Text = "UP TRACK";
            // 
            // label234
            // 
            this.label234.AutoSize = true;
            this.label234.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label234.Location = new System.Drawing.Point(138, 563);
            this.label234.Name = "label234";
            this.label234.Size = new System.Drawing.Size(82, 15);
            this.label234.TabIndex = 3449;
            this.label234.Text = "DOWN TRACK";
            // 
            // label235
            // 
            this.label235.AutoSize = true;
            this.label235.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label235.Location = new System.Drawing.Point(1638, 878);
            this.label235.Name = "label235";
            this.label235.Size = new System.Drawing.Size(60, 15);
            this.label235.TabIndex = 3452;
            this.label235.Text = "UP TRACK";
            // 
            // label236
            // 
            this.label236.AutoSize = true;
            this.label236.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label236.Location = new System.Drawing.Point(1638, 860);
            this.label236.Name = "label236";
            this.label236.Size = new System.Drawing.Size(82, 15);
            this.label236.TabIndex = 3451;
            this.label236.Text = "DOWN TRACK";
            // 
            // label237
            // 
            this.label237.AutoSize = true;
            this.label237.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label237.Location = new System.Drawing.Point(237, 150);
            this.label237.Name = "label237";
            this.label237.Size = new System.Drawing.Size(20, 15);
            this.label237.TabIndex = 3455;
            this.label237.Text = "S7";
            // 
            // rhombusUC76
            // 
            this.rhombusUC76.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC76.ExtenderWidth = 2;
            this.rhombusUC76.Fill = false;
            this.rhombusUC76.GroupID = null;
            this.rhombusUC76.Location = new System.Drawing.Point(241, 131);
            this.rhombusUC76.Name = "rhombusUC76";
            this.rhombusUC76.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC76.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC76.TabIndex = 3453;
            // 
            // label238
            // 
            this.label238.AutoSize = true;
            this.label238.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label238.Location = new System.Drawing.Point(777, 159);
            this.label238.Name = "label238";
            this.label238.Size = new System.Drawing.Size(26, 15);
            this.label238.TabIndex = 3458;
            this.label238.Text = "S8S";
            // 
            // rhombusUC77
            // 
            this.rhombusUC77.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC77.ExtenderWidth = 2;
            this.rhombusUC77.Fill = false;
            this.rhombusUC77.GroupID = null;
            this.rhombusUC77.Location = new System.Drawing.Point(800, 159);
            this.rhombusUC77.Name = "rhombusUC77";
            this.rhombusUC77.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC77.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC77.TabIndex = 3456;
            // 
            // lineUC273
            // 
            this.lineUC273.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC273.ExtenderWidth = 3;
            this.lineUC273.Fill = false;
            this.lineUC273.GroupID = null;
            this.lineUC273.Location = new System.Drawing.Point(807, 139);
            this.lineUC273.Name = "lineUC273";
            this.lineUC273.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC273.Size = new System.Drawing.Size(3, 33);
            this.lineUC273.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC273.TabIndex = 3457;
            // 
            // label239
            // 
            this.label239.AutoSize = true;
            this.label239.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label239.Location = new System.Drawing.Point(837, 150);
            this.label239.Name = "label239";
            this.label239.Size = new System.Drawing.Size(20, 15);
            this.label239.TabIndex = 3461;
            this.label239.Text = "S8";
            // 
            // rhombusUC86
            // 
            this.rhombusUC86.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC86.ExtenderWidth = 2;
            this.rhombusUC86.Fill = false;
            this.rhombusUC86.GroupID = null;
            this.rhombusUC86.Location = new System.Drawing.Point(845, 131);
            this.rhombusUC86.Name = "rhombusUC86";
            this.rhombusUC86.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC86.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC86.TabIndex = 3459;
            // 
            // label240
            // 
            this.label240.AutoSize = true;
            this.label240.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label240.Location = new System.Drawing.Point(1391, 150);
            this.label240.Name = "label240";
            this.label240.Size = new System.Drawing.Size(20, 15);
            this.label240.TabIndex = 3464;
            this.label240.Text = "S9";
            // 
            // rhombusUC87
            // 
            this.rhombusUC87.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC87.ExtenderWidth = 2;
            this.rhombusUC87.Fill = false;
            this.rhombusUC87.GroupID = null;
            this.rhombusUC87.Location = new System.Drawing.Point(1401, 131);
            this.rhombusUC87.Name = "rhombusUC87";
            this.rhombusUC87.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC87.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC87.TabIndex = 3462;
            // 
            // label241
            // 
            this.label241.AutoSize = true;
            this.label241.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label241.Location = new System.Drawing.Point(1697, 160);
            this.label241.Name = "label241";
            this.label241.Size = new System.Drawing.Size(26, 15);
            this.label241.TabIndex = 3467;
            this.label241.Text = "S9S";
            // 
            // rhombusUC88
            // 
            this.rhombusUC88.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC88.ExtenderWidth = 2;
            this.rhombusUC88.Fill = false;
            this.rhombusUC88.GroupID = null;
            this.rhombusUC88.Location = new System.Drawing.Point(1720, 160);
            this.rhombusUC88.Name = "rhombusUC88";
            this.rhombusUC88.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC88.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC88.TabIndex = 3465;
            // 
            // lineUC276
            // 
            this.lineUC276.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC276.ExtenderWidth = 3;
            this.lineUC276.Fill = false;
            this.lineUC276.GroupID = null;
            this.lineUC276.Location = new System.Drawing.Point(1727, 140);
            this.lineUC276.Name = "lineUC276";
            this.lineUC276.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC276.Size = new System.Drawing.Size(3, 33);
            this.lineUC276.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC276.TabIndex = 3466;
            // 
            // label242
            // 
            this.label242.AutoSize = true;
            this.label242.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label242.Location = new System.Drawing.Point(300, 447);
            this.label242.Name = "label242";
            this.label242.Size = new System.Drawing.Size(27, 15);
            this.label242.TabIndex = 3470;
            this.label242.Text = "S10";
            // 
            // rhombusUC89
            // 
            this.rhombusUC89.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC89.ExtenderWidth = 2;
            this.rhombusUC89.Fill = false;
            this.rhombusUC89.GroupID = null;
            this.rhombusUC89.Location = new System.Drawing.Point(314, 428);
            this.rhombusUC89.Name = "rhombusUC89";
            this.rhombusUC89.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC89.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC89.TabIndex = 3468;
            // 
            // label243
            // 
            this.label243.AutoSize = true;
            this.label243.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label243.Location = new System.Drawing.Point(873, 447);
            this.label243.Name = "label243";
            this.label243.Size = new System.Drawing.Size(27, 15);
            this.label243.TabIndex = 3473;
            this.label243.Text = "S11";
            // 
            // rhombusUC90
            // 
            this.rhombusUC90.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC90.ExtenderWidth = 2;
            this.rhombusUC90.Fill = false;
            this.rhombusUC90.GroupID = null;
            this.rhombusUC90.Location = new System.Drawing.Point(891, 428);
            this.rhombusUC90.Name = "rhombusUC90";
            this.rhombusUC90.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC90.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC90.TabIndex = 3471;
            // 
            // label244
            // 
            this.label244.AutoSize = true;
            this.label244.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label244.Location = new System.Drawing.Point(17, 745);
            this.label244.Name = "label244";
            this.label244.Size = new System.Drawing.Size(27, 15);
            this.label244.TabIndex = 3476;
            this.label244.Text = "S12";
            // 
            // rhombusUC91
            // 
            this.rhombusUC91.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC91.ExtenderWidth = 2;
            this.rhombusUC91.Fill = false;
            this.rhombusUC91.GroupID = null;
            this.rhombusUC91.Location = new System.Drawing.Point(25, 726);
            this.rhombusUC91.Name = "rhombusUC91";
            this.rhombusUC91.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC91.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC91.TabIndex = 3474;
            // 
            // label245
            // 
            this.label245.AutoSize = true;
            this.label245.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label245.Location = new System.Drawing.Point(521, 754);
            this.label245.Name = "label245";
            this.label245.Size = new System.Drawing.Size(33, 15);
            this.label245.TabIndex = 3479;
            this.label245.Text = "S12S";
            // 
            // rhombusUC92
            // 
            this.rhombusUC92.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC92.ExtenderWidth = 2;
            this.rhombusUC92.Fill = false;
            this.rhombusUC92.GroupID = null;
            this.rhombusUC92.Location = new System.Drawing.Point(552, 754);
            this.rhombusUC92.Name = "rhombusUC92";
            this.rhombusUC92.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC92.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC92.TabIndex = 3477;
            // 
            // lineUC280
            // 
            this.lineUC280.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.lineUC280.ExtenderWidth = 3;
            this.lineUC280.Fill = false;
            this.lineUC280.GroupID = null;
            this.lineUC280.Location = new System.Drawing.Point(559, 734);
            this.lineUC280.Name = "lineUC280";
            this.lineUC280.Orientation = iSCADA.Design.Utilities.Electrical.SymbolOrientation.Verticle;
            this.lineUC280.Size = new System.Drawing.Size(3, 33);
            this.lineUC280.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.lineUC280.TabIndex = 3478;
            // 
            // label246
            // 
            this.label246.AutoSize = true;
            this.label246.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label246.Location = new System.Drawing.Point(1054, 745);
            this.label246.Name = "label246";
            this.label246.Size = new System.Drawing.Size(27, 15);
            this.label246.TabIndex = 3482;
            this.label246.Text = "S13";
            // 
            // rhombusUC93
            // 
            this.rhombusUC93.dashstyle = iSCADA.Design.Utilities.Electrical.DashStyle.Solid;
            this.rhombusUC93.ExtenderWidth = 2;
            this.rhombusUC93.Fill = false;
            this.rhombusUC93.GroupID = null;
            this.rhombusUC93.Location = new System.Drawing.Point(1073, 726);
            this.rhombusUC93.Name = "rhombusUC93";
            this.rhombusUC93.Size = new System.Drawing.Size(16, 16);
            this.rhombusUC93.Style = iSCADA.Design.Utilities.Electrical.SymbolStyle.Default;
            this.rhombusUC93.TabIndex = 3480;
            // 
            // label247
            // 
            this.label247.AutoSize = true;
            this.label247.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label247.Location = new System.Drawing.Point(785, 110);
            this.label247.Name = "label247";
            this.label247.Size = new System.Drawing.Size(47, 17);
            this.label247.TabIndex = 3483;
            this.label247.Text = "駐車軌";
            // 
            // label248
            // 
            this.label248.AutoSize = true;
            this.label248.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label248.Location = new System.Drawing.Point(1721, 118);
            this.label248.Name = "label248";
            this.label248.Size = new System.Drawing.Size(47, 17);
            this.label248.TabIndex = 3484;
            this.label248.Text = "避車軌";
            // 
            // label249
            // 
            this.label249.AutoSize = true;
            this.label249.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label249.Location = new System.Drawing.Point(527, 711);
            this.label249.Name = "label249";
            this.label249.Size = new System.Drawing.Size(47, 17);
            this.label249.TabIndex = 3485;
            this.label249.Text = "避車軌";
            // 
            // Form_Total1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1910, 900);
            this.Controls.Add(this.label249);
            this.Controls.Add(this.label248);
            this.Controls.Add(this.label247);
            this.Controls.Add(this.label246);
            this.Controls.Add(this.rhombusUC93);
            this.Controls.Add(this.label245);
            this.Controls.Add(this.rhombusUC92);
            this.Controls.Add(this.lineUC280);
            this.Controls.Add(this.label244);
            this.Controls.Add(this.rhombusUC91);
            this.Controls.Add(this.label243);
            this.Controls.Add(this.rhombusUC90);
            this.Controls.Add(this.label242);
            this.Controls.Add(this.rhombusUC89);
            this.Controls.Add(this.label241);
            this.Controls.Add(this.rhombusUC88);
            this.Controls.Add(this.lineUC276);
            this.Controls.Add(this.label240);
            this.Controls.Add(this.rhombusUC87);
            this.Controls.Add(this.label239);
            this.Controls.Add(this.rhombusUC86);
            this.Controls.Add(this.label238);
            this.Controls.Add(this.rhombusUC77);
            this.Controls.Add(this.lineUC273);
            this.Controls.Add(this.label237);
            this.Controls.Add(this.rhombusUC76);
            this.Controls.Add(this.label235);
            this.Controls.Add(this.label236);
            this.Controls.Add(this.label233);
            this.Controls.Add(this.label234);
            this.Controls.Add(this.label232);
            this.Controls.Add(this.label231);
            this.Controls.Add(this.label230);
            this.Controls.Add(this.label229);
            this.Controls.Add(this.label228);
            this.Controls.Add(this.label227);
            this.Controls.Add(this.label226);
            this.Controls.Add(this.label225);
            this.Controls.Add(this.label224);
            this.Controls.Add(this.label223);
            this.Controls.Add(this.label222);
            this.Controls.Add(this.label221);
            this.Controls.Add(this.label220);
            this.Controls.Add(this.label219);
            this.Controls.Add(this.label218);
            this.Controls.Add(this.label217);
            this.Controls.Add(this.label216);
            this.Controls.Add(this.label215);
            this.Controls.Add(this.label214);
            this.Controls.Add(this.label213);
            this.Controls.Add(this.label212);
            this.Controls.Add(this.label211);
            this.Controls.Add(this.label210);
            this.Controls.Add(this.label209);
            this.Controls.Add(this.label208);
            this.Controls.Add(this.label207);
            this.Controls.Add(this.label196);
            this.Controls.Add(this.label195);
            this.Controls.Add(this.label194);
            this.Controls.Add(this.lineUC258);
            this.Controls.Add(this.lineUC257);
            this.Controls.Add(this.lineUC256);
            this.Controls.Add(this.lineUC255);
            this.Controls.Add(this.lineUC254);
            this.Controls.Add(this.lineUC253);
            this.Controls.Add(this.lineUC252);
            this.Controls.Add(this.label193);
            this.Controls.Add(this.label192);
            this.Controls.Add(this.label191);
            this.Controls.Add(this.retangleUC172);
            this.Controls.Add(this.retangleUC171);
            this.Controls.Add(this.retangleUC170);
            this.Controls.Add(this.retangleUC169);
            this.Controls.Add(this.retangleUC168);
            this.Controls.Add(this.retangleUC167);
            this.Controls.Add(this.retangleUC166);
            this.Controls.Add(this.retangleUC165);
            this.Controls.Add(this.retangleUC164);
            this.Controls.Add(this.retangleUC163);
            this.Controls.Add(this.retangleUC162);
            this.Controls.Add(this.retangleUC161);
            this.Controls.Add(this.retangleUC160);
            this.Controls.Add(this.retangleUC159);
            this.Controls.Add(this.retangleUC158);
            this.Controls.Add(this.retangleUC157);
            this.Controls.Add(this.retangleUC156);
            this.Controls.Add(this.retangleUC155);
            this.Controls.Add(this.retangleUC154);
            this.Controls.Add(this.retangleUC153);
            this.Controls.Add(this.retangleUC152);
            this.Controls.Add(this.retangleUC151);
            this.Controls.Add(this.retangleUC150);
            this.Controls.Add(this.retangleUC149);
            this.Controls.Add(this.retangleUC148);
            this.Controls.Add(this.retangleUC147);
            this.Controls.Add(this.retangleUC146);
            this.Controls.Add(this.retangleUC145);
            this.Controls.Add(this.retangleUC144);
            this.Controls.Add(this.retangleUC143);
            this.Controls.Add(this.retangleUC142);
            this.Controls.Add(this.retangleUC141);
            this.Controls.Add(this.retangleUC140);
            this.Controls.Add(this.retangleUC139);
            this.Controls.Add(this.retangleUC138);
            this.Controls.Add(this.retangleUC137);
            this.Controls.Add(this.retangleUC136);
            this.Controls.Add(this.retangleUC135);
            this.Controls.Add(this.retangleUC134);
            this.Controls.Add(this.retangleUC133);
            this.Controls.Add(this.retangleUC132);
            this.Controls.Add(this.retangleUC131);
            this.Controls.Add(this.retangleUC130);
            this.Controls.Add(this.retangleUC129);
            this.Controls.Add(this.retangleUC128);
            this.Controls.Add(this.retangleUC127);
            this.Controls.Add(this.retangleUC126);
            this.Controls.Add(this.retangleUC125);
            this.Controls.Add(this.retangleUC121);
            this.Controls.Add(this.retangleUC120);
            this.Controls.Add(this.retangleUC119);
            this.Controls.Add(this.retangleUC118);
            this.Controls.Add(this.label179);
            this.Controls.Add(this.label180);
            this.Controls.Add(this.label181);
            this.Controls.Add(this.rhombusUC72);
            this.Controls.Add(this.lineUC236);
            this.Controls.Add(this.retangleUC110);
            this.Controls.Add(this.retangleUC111);
            this.Controls.Add(this.lineUC237);
            this.Controls.Add(this.lineUC238);
            this.Controls.Add(this.lineUC239);
            this.Controls.Add(this.label182);
            this.Controls.Add(this.label183);
            this.Controls.Add(this.label184);
            this.Controls.Add(this.rhombusUC73);
            this.Controls.Add(this.lineUC240);
            this.Controls.Add(this.retangleUC112);
            this.Controls.Add(this.retangleUC113);
            this.Controls.Add(this.lineUC241);
            this.Controls.Add(this.lineUC242);
            this.Controls.Add(this.lineUC243);
            this.Controls.Add(this.label185);
            this.Controls.Add(this.label186);
            this.Controls.Add(this.label187);
            this.Controls.Add(this.rhombusUC74);
            this.Controls.Add(this.lineUC244);
            this.Controls.Add(this.retangleUC114);
            this.Controls.Add(this.retangleUC115);
            this.Controls.Add(this.lineUC245);
            this.Controls.Add(this.lineUC246);
            this.Controls.Add(this.lineUC247);
            this.Controls.Add(this.label188);
            this.Controls.Add(this.label189);
            this.Controls.Add(this.label190);
            this.Controls.Add(this.rhombusUC75);
            this.Controls.Add(this.lineUC248);
            this.Controls.Add(this.retangleUC116);
            this.Controls.Add(this.retangleUC117);
            this.Controls.Add(this.lineUC249);
            this.Controls.Add(this.lineUC250);
            this.Controls.Add(this.lineUC251);
            this.Controls.Add(this.label164);
            this.Controls.Add(this.label165);
            this.Controls.Add(this.label166);
            this.Controls.Add(this.rhombusUC67);
            this.Controls.Add(this.lineUC216);
            this.Controls.Add(this.retangleUC100);
            this.Controls.Add(this.retangleUC101);
            this.Controls.Add(this.lineUC217);
            this.Controls.Add(this.lineUC218);
            this.Controls.Add(this.lineUC219);
            this.Controls.Add(this.label167);
            this.Controls.Add(this.label168);
            this.Controls.Add(this.label169);
            this.Controls.Add(this.rhombusUC68);
            this.Controls.Add(this.lineUC220);
            this.Controls.Add(this.retangleUC102);
            this.Controls.Add(this.retangleUC103);
            this.Controls.Add(this.lineUC221);
            this.Controls.Add(this.lineUC222);
            this.Controls.Add(this.lineUC223);
            this.Controls.Add(this.label170);
            this.Controls.Add(this.label171);
            this.Controls.Add(this.label172);
            this.Controls.Add(this.rhombusUC69);
            this.Controls.Add(this.lineUC224);
            this.Controls.Add(this.retangleUC104);
            this.Controls.Add(this.retangleUC105);
            this.Controls.Add(this.lineUC225);
            this.Controls.Add(this.lineUC226);
            this.Controls.Add(this.lineUC227);
            this.Controls.Add(this.label173);
            this.Controls.Add(this.label174);
            this.Controls.Add(this.label175);
            this.Controls.Add(this.rhombusUC70);
            this.Controls.Add(this.lineUC228);
            this.Controls.Add(this.retangleUC106);
            this.Controls.Add(this.retangleUC107);
            this.Controls.Add(this.lineUC229);
            this.Controls.Add(this.lineUC230);
            this.Controls.Add(this.lineUC231);
            this.Controls.Add(this.label176);
            this.Controls.Add(this.label177);
            this.Controls.Add(this.label178);
            this.Controls.Add(this.rhombusUC71);
            this.Controls.Add(this.lineUC232);
            this.Controls.Add(this.retangleUC108);
            this.Controls.Add(this.retangleUC109);
            this.Controls.Add(this.lineUC233);
            this.Controls.Add(this.lineUC234);
            this.Controls.Add(this.lineUC235);
            this.Controls.Add(this.label158);
            this.Controls.Add(this.label159);
            this.Controls.Add(this.label160);
            this.Controls.Add(this.rhombusUC65);
            this.Controls.Add(this.lineUC208);
            this.Controls.Add(this.retangleUC96);
            this.Controls.Add(this.retangleUC97);
            this.Controls.Add(this.lineUC209);
            this.Controls.Add(this.lineUC210);
            this.Controls.Add(this.lineUC211);
            this.Controls.Add(this.label161);
            this.Controls.Add(this.label162);
            this.Controls.Add(this.label163);
            this.Controls.Add(this.rhombusUC66);
            this.Controls.Add(this.lineUC212);
            this.Controls.Add(this.retangleUC98);
            this.Controls.Add(this.retangleUC99);
            this.Controls.Add(this.lineUC213);
            this.Controls.Add(this.lineUC214);
            this.Controls.Add(this.lineUC215);
            this.Controls.Add(this.label149);
            this.Controls.Add(this.label150);
            this.Controls.Add(this.label151);
            this.Controls.Add(this.rhombusUC62);
            this.Controls.Add(this.lineUC196);
            this.Controls.Add(this.retangleUC90);
            this.Controls.Add(this.retangleUC91);
            this.Controls.Add(this.lineUC197);
            this.Controls.Add(this.lineUC198);
            this.Controls.Add(this.lineUC199);
            this.Controls.Add(this.label152);
            this.Controls.Add(this.label153);
            this.Controls.Add(this.label154);
            this.Controls.Add(this.rhombusUC63);
            this.Controls.Add(this.lineUC200);
            this.Controls.Add(this.retangleUC92);
            this.Controls.Add(this.retangleUC93);
            this.Controls.Add(this.lineUC201);
            this.Controls.Add(this.lineUC202);
            this.Controls.Add(this.lineUC203);
            this.Controls.Add(this.label155);
            this.Controls.Add(this.label156);
            this.Controls.Add(this.label157);
            this.Controls.Add(this.rhombusUC64);
            this.Controls.Add(this.lineUC204);
            this.Controls.Add(this.retangleUC94);
            this.Controls.Add(this.retangleUC95);
            this.Controls.Add(this.lineUC205);
            this.Controls.Add(this.lineUC206);
            this.Controls.Add(this.lineUC207);
            this.Controls.Add(this.label140);
            this.Controls.Add(this.label141);
            this.Controls.Add(this.label142);
            this.Controls.Add(this.rhombusUC59);
            this.Controls.Add(this.lineUC184);
            this.Controls.Add(this.retangleUC84);
            this.Controls.Add(this.retangleUC85);
            this.Controls.Add(this.lineUC185);
            this.Controls.Add(this.lineUC186);
            this.Controls.Add(this.lineUC187);
            this.Controls.Add(this.label143);
            this.Controls.Add(this.label144);
            this.Controls.Add(this.label145);
            this.Controls.Add(this.rhombusUC60);
            this.Controls.Add(this.lineUC188);
            this.Controls.Add(this.retangleUC86);
            this.Controls.Add(this.retangleUC87);
            this.Controls.Add(this.lineUC189);
            this.Controls.Add(this.lineUC190);
            this.Controls.Add(this.lineUC191);
            this.Controls.Add(this.label146);
            this.Controls.Add(this.label147);
            this.Controls.Add(this.label148);
            this.Controls.Add(this.rhombusUC61);
            this.Controls.Add(this.lineUC192);
            this.Controls.Add(this.retangleUC88);
            this.Controls.Add(this.retangleUC89);
            this.Controls.Add(this.lineUC193);
            this.Controls.Add(this.lineUC194);
            this.Controls.Add(this.lineUC195);
            this.Controls.Add(this.label197);
            this.Controls.Add(this.label198);
            this.Controls.Add(this.label199);
            this.Controls.Add(this.label200);
            this.Controls.Add(this.rhombusUC78);
            this.Controls.Add(this.rhombusUC79);
            this.Controls.Add(this.rhombusUC80);
            this.Controls.Add(this.lineUC260);
            this.Controls.Add(this.rhombusUC81);
            this.Controls.Add(this.lineUC261);
            this.Controls.Add(this.lineUC262);
            this.Controls.Add(this.lineUC263);
            this.Controls.Add(this.lineUC264);
            this.Controls.Add(this.retangleUC122);
            this.Controls.Add(this.label201);
            this.Controls.Add(this.label202);
            this.Controls.Add(this.label203);
            this.Controls.Add(this.label204);
            this.Controls.Add(this.label205);
            this.Controls.Add(this.rhombusUC82);
            this.Controls.Add(this.rhombusUC83);
            this.Controls.Add(this.rhombusUC84);
            this.Controls.Add(this.lineUC265);
            this.Controls.Add(this.rhombusUC85);
            this.Controls.Add(this.lineUC266);
            this.Controls.Add(this.lineUC267);
            this.Controls.Add(this.lineUC268);
            this.Controls.Add(this.lineUC269);
            this.Controls.Add(this.retangleUC123);
            this.Controls.Add(this.label206);
            this.Controls.Add(this.retangleUC124);
            this.Controls.Add(this.lineUC270);
            this.Controls.Add(this.lineUC271);
            this.Controls.Add(this.lineUC272);
            this.Controls.Add(this.label131);
            this.Controls.Add(this.label132);
            this.Controls.Add(this.label133);
            this.Controls.Add(this.rhombusUC56);
            this.Controls.Add(this.lineUC172);
            this.Controls.Add(this.retangleUC78);
            this.Controls.Add(this.retangleUC79);
            this.Controls.Add(this.lineUC173);
            this.Controls.Add(this.lineUC174);
            this.Controls.Add(this.lineUC175);
            this.Controls.Add(this.label134);
            this.Controls.Add(this.label135);
            this.Controls.Add(this.label136);
            this.Controls.Add(this.rhombusUC57);
            this.Controls.Add(this.lineUC176);
            this.Controls.Add(this.retangleUC80);
            this.Controls.Add(this.retangleUC81);
            this.Controls.Add(this.lineUC177);
            this.Controls.Add(this.lineUC178);
            this.Controls.Add(this.lineUC179);
            this.Controls.Add(this.label137);
            this.Controls.Add(this.label138);
            this.Controls.Add(this.label139);
            this.Controls.Add(this.rhombusUC58);
            this.Controls.Add(this.lineUC180);
            this.Controls.Add(this.retangleUC82);
            this.Controls.Add(this.retangleUC83);
            this.Controls.Add(this.lineUC181);
            this.Controls.Add(this.lineUC182);
            this.Controls.Add(this.lineUC183);
            this.Controls.Add(this.label125);
            this.Controls.Add(this.label126);
            this.Controls.Add(this.label127);
            this.Controls.Add(this.rhombusUC54);
            this.Controls.Add(this.lineUC164);
            this.Controls.Add(this.retangleUC74);
            this.Controls.Add(this.retangleUC75);
            this.Controls.Add(this.lineUC165);
            this.Controls.Add(this.lineUC166);
            this.Controls.Add(this.lineUC167);
            this.Controls.Add(this.label128);
            this.Controls.Add(this.label129);
            this.Controls.Add(this.label130);
            this.Controls.Add(this.rhombusUC55);
            this.Controls.Add(this.lineUC168);
            this.Controls.Add(this.retangleUC76);
            this.Controls.Add(this.retangleUC77);
            this.Controls.Add(this.lineUC169);
            this.Controls.Add(this.lineUC170);
            this.Controls.Add(this.lineUC171);
            this.Controls.Add(this.label111);
            this.Controls.Add(this.label112);
            this.Controls.Add(this.label113);
            this.Controls.Add(this.rhombusUC47);
            this.Controls.Add(this.lineUC144);
            this.Controls.Add(this.retangleUC66);
            this.Controls.Add(this.retangleUC67);
            this.Controls.Add(this.lineUC145);
            this.Controls.Add(this.lineUC146);
            this.Controls.Add(this.lineUC147);
            this.Controls.Add(this.label114);
            this.Controls.Add(this.label115);
            this.Controls.Add(this.label116);
            this.Controls.Add(this.rhombusUC48);
            this.Controls.Add(this.lineUC148);
            this.Controls.Add(this.retangleUC68);
            this.Controls.Add(this.retangleUC69);
            this.Controls.Add(this.lineUC149);
            this.Controls.Add(this.lineUC150);
            this.Controls.Add(this.lineUC151);
            this.Controls.Add(this.label117);
            this.Controls.Add(this.label123);
            this.Controls.Add(this.label124);
            this.Controls.Add(this.rhombusUC53);
            this.Controls.Add(this.lineUC157);
            this.Controls.Add(this.retangleUC71);
            this.Controls.Add(this.retangleUC72);
            this.Controls.Add(this.lineUC158);
            this.Controls.Add(this.lineUC159);
            this.Controls.Add(this.lineUC160);
            this.Controls.Add(this.label105);
            this.Controls.Add(this.label106);
            this.Controls.Add(this.label107);
            this.Controls.Add(this.rhombusUC45);
            this.Controls.Add(this.lineUC136);
            this.Controls.Add(this.retangleUC62);
            this.Controls.Add(this.retangleUC63);
            this.Controls.Add(this.lineUC137);
            this.Controls.Add(this.lineUC138);
            this.Controls.Add(this.lineUC139);
            this.Controls.Add(this.label108);
            this.Controls.Add(this.label109);
            this.Controls.Add(this.label110);
            this.Controls.Add(this.rhombusUC46);
            this.Controls.Add(this.lineUC140);
            this.Controls.Add(this.retangleUC64);
            this.Controls.Add(this.retangleUC65);
            this.Controls.Add(this.lineUC141);
            this.Controls.Add(this.lineUC142);
            this.Controls.Add(this.lineUC143);
            this.Controls.Add(this.label96);
            this.Controls.Add(this.label97);
            this.Controls.Add(this.label98);
            this.Controls.Add(this.rhombusUC42);
            this.Controls.Add(this.lineUC124);
            this.Controls.Add(this.retangleUC56);
            this.Controls.Add(this.retangleUC57);
            this.Controls.Add(this.lineUC125);
            this.Controls.Add(this.lineUC126);
            this.Controls.Add(this.lineUC127);
            this.Controls.Add(this.label99);
            this.Controls.Add(this.label100);
            this.Controls.Add(this.label101);
            this.Controls.Add(this.rhombusUC43);
            this.Controls.Add(this.lineUC128);
            this.Controls.Add(this.retangleUC58);
            this.Controls.Add(this.retangleUC59);
            this.Controls.Add(this.lineUC129);
            this.Controls.Add(this.lineUC130);
            this.Controls.Add(this.lineUC131);
            this.Controls.Add(this.label102);
            this.Controls.Add(this.label103);
            this.Controls.Add(this.label104);
            this.Controls.Add(this.rhombusUC44);
            this.Controls.Add(this.lineUC132);
            this.Controls.Add(this.retangleUC60);
            this.Controls.Add(this.retangleUC61);
            this.Controls.Add(this.lineUC133);
            this.Controls.Add(this.lineUC134);
            this.Controls.Add(this.lineUC135);
            this.Controls.Add(this.label87);
            this.Controls.Add(this.label88);
            this.Controls.Add(this.label89);
            this.Controls.Add(this.rhombusUC39);
            this.Controls.Add(this.lineUC112);
            this.Controls.Add(this.retangleUC50);
            this.Controls.Add(this.retangleUC51);
            this.Controls.Add(this.lineUC113);
            this.Controls.Add(this.lineUC114);
            this.Controls.Add(this.lineUC115);
            this.Controls.Add(this.label90);
            this.Controls.Add(this.label91);
            this.Controls.Add(this.label92);
            this.Controls.Add(this.rhombusUC40);
            this.Controls.Add(this.lineUC116);
            this.Controls.Add(this.retangleUC52);
            this.Controls.Add(this.retangleUC53);
            this.Controls.Add(this.lineUC117);
            this.Controls.Add(this.lineUC118);
            this.Controls.Add(this.lineUC119);
            this.Controls.Add(this.label93);
            this.Controls.Add(this.label94);
            this.Controls.Add(this.label95);
            this.Controls.Add(this.rhombusUC41);
            this.Controls.Add(this.lineUC120);
            this.Controls.Add(this.retangleUC54);
            this.Controls.Add(this.retangleUC55);
            this.Controls.Add(this.lineUC121);
            this.Controls.Add(this.lineUC122);
            this.Controls.Add(this.lineUC123);
            this.Controls.Add(this.label71);
            this.Controls.Add(this.label72);
            this.Controls.Add(this.label73);
            this.Controls.Add(this.rhombusUC20);
            this.Controls.Add(this.lineUC34);
            this.Controls.Add(this.retangleUC18);
            this.Controls.Add(this.retangleUC44);
            this.Controls.Add(this.lineUC35);
            this.Controls.Add(this.lineUC36);
            this.Controls.Add(this.lineUC37);
            this.Controls.Add(this.label74);
            this.Controls.Add(this.label75);
            this.Controls.Add(this.label78);
            this.Controls.Add(this.rhombusUC21);
            this.Controls.Add(this.lineUC38);
            this.Controls.Add(this.retangleUC45);
            this.Controls.Add(this.retangleUC46);
            this.Controls.Add(this.lineUC100);
            this.Controls.Add(this.lineUC101);
            this.Controls.Add(this.lineUC102);
            this.Controls.Add(this.label84);
            this.Controls.Add(this.label85);
            this.Controls.Add(this.label86);
            this.Controls.Add(this.rhombusUC38);
            this.Controls.Add(this.lineUC108);
            this.Controls.Add(this.retangleUC48);
            this.Controls.Add(this.retangleUC49);
            this.Controls.Add(this.lineUC109);
            this.Controls.Add(this.lineUC110);
            this.Controls.Add(this.lineUC111);
            this.Controls.Add(this.label79);
            this.Controls.Add(this.label80);
            this.Controls.Add(this.label81);
            this.Controls.Add(this.label82);
            this.Controls.Add(this.rhombusUC22);
            this.Controls.Add(this.rhombusUC23);
            this.Controls.Add(this.rhombusUC36);
            this.Controls.Add(this.lineUC103);
            this.Controls.Add(this.rhombusUC37);
            this.Controls.Add(this.lineUC104);
            this.Controls.Add(this.lineUC105);
            this.Controls.Add(this.lineUC106);
            this.Controls.Add(this.lineUC107);
            this.Controls.Add(this.retangleUC47);
            this.Controls.Add(this.label83);
            this.Controls.Add(this.label118);
            this.Controls.Add(this.label119);
            this.Controls.Add(this.label120);
            this.Controls.Add(this.label121);
            this.Controls.Add(this.rhombusUC49);
            this.Controls.Add(this.rhombusUC50);
            this.Controls.Add(this.rhombusUC51);
            this.Controls.Add(this.lineUC152);
            this.Controls.Add(this.rhombusUC52);
            this.Controls.Add(this.lineUC153);
            this.Controls.Add(this.lineUC154);
            this.Controls.Add(this.lineUC155);
            this.Controls.Add(this.lineUC156);
            this.Controls.Add(this.retangleUC70);
            this.Controls.Add(this.label122);
            this.Controls.Add(this.retangleUC73);
            this.Controls.Add(this.lineUC161);
            this.Controls.Add(this.lineUC162);
            this.Controls.Add(this.lineUC163);
            this.Controls.Add(this.label67);
            this.Controls.Add(this.label68);
            this.Controls.Add(this.label69);
            this.Controls.Add(this.rhombusUC34);
            this.Controls.Add(this.lineUC92);
            this.Controls.Add(this.retangleUC30);
            this.Controls.Add(this.retangleUC41);
            this.Controls.Add(this.lineUC93);
            this.Controls.Add(this.lineUC94);
            this.Controls.Add(this.lineUC95);
            this.Controls.Add(this.label70);
            this.Controls.Add(this.label76);
            this.Controls.Add(this.label77);
            this.Controls.Add(this.rhombusUC35);
            this.Controls.Add(this.lineUC96);
            this.Controls.Add(this.retangleUC42);
            this.Controls.Add(this.retangleUC43);
            this.Controls.Add(this.lineUC97);
            this.Controls.Add(this.lineUC98);
            this.Controls.Add(this.lineUC99);
            this.Controls.Add(this.label47);
            this.Controls.Add(this.label48);
            this.Controls.Add(this.label49);
            this.Controls.Add(this.label65);
            this.Controls.Add(this.rhombusUC25);
            this.Controls.Add(this.rhombusUC31);
            this.Controls.Add(this.rhombusUC32);
            this.Controls.Add(this.lineUC67);
            this.Controls.Add(this.rhombusUC33);
            this.Controls.Add(this.lineUC68);
            this.Controls.Add(this.lineUC69);
            this.Controls.Add(this.lineUC70);
            this.Controls.Add(this.lineUC91);
            this.Controls.Add(this.retangleUC29);
            this.Controls.Add(this.label66);
            this.Controls.Add(this.label50);
            this.Controls.Add(this.label51);
            this.Controls.Add(this.label52);
            this.Controls.Add(this.rhombusUC26);
            this.Controls.Add(this.lineUC71);
            this.Controls.Add(this.retangleUC31);
            this.Controls.Add(this.retangleUC32);
            this.Controls.Add(this.lineUC72);
            this.Controls.Add(this.lineUC73);
            this.Controls.Add(this.lineUC74);
            this.Controls.Add(this.label53);
            this.Controls.Add(this.label54);
            this.Controls.Add(this.label55);
            this.Controls.Add(this.rhombusUC27);
            this.Controls.Add(this.lineUC75);
            this.Controls.Add(this.retangleUC33);
            this.Controls.Add(this.retangleUC34);
            this.Controls.Add(this.lineUC76);
            this.Controls.Add(this.lineUC77);
            this.Controls.Add(this.lineUC78);
            this.Controls.Add(this.label56);
            this.Controls.Add(this.label57);
            this.Controls.Add(this.label58);
            this.Controls.Add(this.rhombusUC28);
            this.Controls.Add(this.lineUC79);
            this.Controls.Add(this.retangleUC35);
            this.Controls.Add(this.retangleUC36);
            this.Controls.Add(this.lineUC80);
            this.Controls.Add(this.lineUC81);
            this.Controls.Add(this.lineUC82);
            this.Controls.Add(this.label59);
            this.Controls.Add(this.label60);
            this.Controls.Add(this.label61);
            this.Controls.Add(this.rhombusUC29);
            this.Controls.Add(this.lineUC83);
            this.Controls.Add(this.retangleUC37);
            this.Controls.Add(this.retangleUC38);
            this.Controls.Add(this.lineUC84);
            this.Controls.Add(this.lineUC85);
            this.Controls.Add(this.lineUC86);
            this.Controls.Add(this.label62);
            this.Controls.Add(this.label63);
            this.Controls.Add(this.label64);
            this.Controls.Add(this.rhombusUC30);
            this.Controls.Add(this.lineUC87);
            this.Controls.Add(this.retangleUC39);
            this.Controls.Add(this.retangleUC40);
            this.Controls.Add(this.lineUC88);
            this.Controls.Add(this.lineUC89);
            this.Controls.Add(this.lineUC90);
            this.Controls.Add(this.label42);
            this.Controls.Add(this.label43);
            this.Controls.Add(this.label44);
            this.Controls.Add(this.label45);
            this.Controls.Add(this.rhombusUC17);
            this.Controls.Add(this.rhombusUC18);
            this.Controls.Add(this.rhombusUC19);
            this.Controls.Add(this.lineUC62);
            this.Controls.Add(this.rhombusUC24);
            this.Controls.Add(this.lineUC63);
            this.Controls.Add(this.lineUC64);
            this.Controls.Add(this.lineUC65);
            this.Controls.Add(this.lineUC66);
            this.Controls.Add(this.retangleUC28);
            this.Controls.Add(this.label46);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.rhombusUC11);
            this.Controls.Add(this.lineUC33);
            this.Controls.Add(this.retangleUC15);
            this.Controls.Add(this.retangleUC16);
            this.Controls.Add(this.lineUC39);
            this.Controls.Add(this.lineUC40);
            this.Controls.Add(this.lineUC41);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.rhombusUC12);
            this.Controls.Add(this.lineUC42);
            this.Controls.Add(this.retangleUC17);
            this.Controls.Add(this.retangleUC19);
            this.Controls.Add(this.lineUC43);
            this.Controls.Add(this.lineUC44);
            this.Controls.Add(this.lineUC45);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.rhombusUC13);
            this.Controls.Add(this.lineUC46);
            this.Controls.Add(this.retangleUC20);
            this.Controls.Add(this.retangleUC21);
            this.Controls.Add(this.lineUC47);
            this.Controls.Add(this.lineUC48);
            this.Controls.Add(this.lineUC49);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.rhombusUC14);
            this.Controls.Add(this.lineUC50);
            this.Controls.Add(this.retangleUC22);
            this.Controls.Add(this.retangleUC23);
            this.Controls.Add(this.lineUC51);
            this.Controls.Add(this.lineUC52);
            this.Controls.Add(this.lineUC53);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.rhombusUC15);
            this.Controls.Add(this.lineUC54);
            this.Controls.Add(this.retangleUC24);
            this.Controls.Add(this.retangleUC25);
            this.Controls.Add(this.lineUC55);
            this.Controls.Add(this.lineUC56);
            this.Controls.Add(this.lineUC57);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.label40);
            this.Controls.Add(this.label41);
            this.Controls.Add(this.rhombusUC16);
            this.Controls.Add(this.lineUC58);
            this.Controls.Add(this.retangleUC26);
            this.Controls.Add(this.retangleUC27);
            this.Controls.Add(this.lineUC59);
            this.Controls.Add(this.lineUC60);
            this.Controls.Add(this.lineUC61);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.rhombusUC9);
            this.Controls.Add(this.lineUC25);
            this.Controls.Add(this.retangleUC11);
            this.Controls.Add(this.retangleUC12);
            this.Controls.Add(this.lineUC26);
            this.Controls.Add(this.lineUC27);
            this.Controls.Add(this.lineUC28);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.rhombusUC10);
            this.Controls.Add(this.lineUC29);
            this.Controls.Add(this.retangleUC13);
            this.Controls.Add(this.retangleUC14);
            this.Controls.Add(this.lineUC30);
            this.Controls.Add(this.lineUC31);
            this.Controls.Add(this.lineUC32);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.rhombusUC7);
            this.Controls.Add(this.lineUC16);
            this.Controls.Add(this.retangleUC6);
            this.Controls.Add(this.retangleUC7);
            this.Controls.Add(this.lineUC17);
            this.Controls.Add(this.lineUC18);
            this.Controls.Add(this.lineUC19);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.rhombusUC8);
            this.Controls.Add(this.lineUC20);
            this.Controls.Add(this.retangleUC8);
            this.Controls.Add(this.retangleUC9);
            this.Controls.Add(this.lineUC21);
            this.Controls.Add(this.lineUC22);
            this.Controls.Add(this.lineUC24);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.rhombusUC5);
            this.Controls.Add(this.lineUC7);
            this.Controls.Add(this.retangleUC4);
            this.Controls.Add(this.retangleUC5);
            this.Controls.Add(this.lineUC12);
            this.Controls.Add(this.lineUC14);
            this.Controls.Add(this.lineUC15);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.rhombusUC1);
            this.Controls.Add(this.rhombusUC2);
            this.Controls.Add(this.rhombusUC3);
            this.Controls.Add(this.lineUC2);
            this.Controls.Add(this.rhombusUC4);
            this.Controls.Add(this.lineUC3);
            this.Controls.Add(this.lineUC4);
            this.Controls.Add(this.lineUC5);
            this.Controls.Add(this.lineUC6);
            this.Controls.Add(this.retangleUC1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.rhombusUC6);
            this.Controls.Add(this.lineUC8);
            this.Controls.Add(this.retangleUC2);
            this.Controls.Add(this.retangleUC3);
            this.Controls.Add(this.lineUC9);
            this.Controls.Add(this.lineUC10);
            this.Controls.Add(this.lineUC13);
            this.Controls.Add(this.retangleUC10);
            this.Controls.Add(this.lineUC23);
            this.Controls.Add(this.lineUC11);
            this.Controls.Add(this.lineUC1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form_Total1";
            this.Text = "Form_Total1";
            this.Load += new System.EventHandler(this.Form_Total1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC6;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC8;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC2;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC3;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC9;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC10;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC13;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC10;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC23;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC11;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC1;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC2;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC3;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC2;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC4;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC3;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC4;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC5;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC6;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC5;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC7;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC4;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC5;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC12;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC14;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC15;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC7;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC16;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC6;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC7;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC17;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC18;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC19;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC8;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC20;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC8;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC9;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC21;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC22;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC24;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC9;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC25;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC11;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC12;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC26;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC27;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC28;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label23;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC10;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC29;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC13;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC14;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC30;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC31;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC32;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC11;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC33;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC15;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC16;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC39;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC40;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC41;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC12;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC42;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC17;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC19;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC43;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC44;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC45;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC13;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC46;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC20;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC21;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC47;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC48;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC49;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC14;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC50;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC22;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC23;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC51;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC52;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC53;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC15;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC54;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC24;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC25;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC55;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC56;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC57;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC16;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC58;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC26;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC27;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC59;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC60;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC61;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC17;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC18;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC19;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC62;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC24;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC63;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC64;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC65;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC66;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC28;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC26;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC71;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC31;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC32;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC72;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC73;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC74;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC27;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC75;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC33;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC34;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC76;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC77;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC78;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC28;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC79;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC35;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC36;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC80;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC81;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC82;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label61;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC29;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC83;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC37;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC38;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC84;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC85;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC86;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label64;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC30;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC87;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC39;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC40;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC88;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC89;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC90;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label65;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC25;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC31;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC32;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC67;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC33;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC68;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC69;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC70;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC91;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC29;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label69;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC34;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC92;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC30;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC41;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC93;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC94;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC95;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label77;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC35;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC96;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC42;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC43;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC97;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC98;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC99;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label82;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC22;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC23;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC36;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC103;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC37;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC104;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC105;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC106;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC107;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC47;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.Label label119;
        private System.Windows.Forms.Label label120;
        private System.Windows.Forms.Label label121;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC49;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC50;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC51;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC152;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC52;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC153;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC154;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC155;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC156;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC70;
        private System.Windows.Forms.Label label122;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC73;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC161;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC162;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC163;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label73;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC20;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC34;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC18;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC44;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC35;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC36;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC37;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label78;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC21;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC38;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC45;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC46;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC100;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC101;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC102;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label86;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC38;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC108;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC48;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC49;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC109;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC110;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC111;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label label89;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC39;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC112;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC50;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC51;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC113;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC114;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC115;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label label92;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC40;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC116;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC52;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC53;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC117;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC118;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC119;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label95;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC41;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC120;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC54;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC55;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC121;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC122;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC123;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label98;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC42;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC124;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC56;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC57;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC125;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC126;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC127;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Label label101;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC43;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC128;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC58;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC59;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC129;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC130;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC131;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label104;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC44;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC132;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC60;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC61;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC133;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC134;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC135;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label label107;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC45;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC136;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC62;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC63;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC137;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC138;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC139;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.Label label110;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC46;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC140;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC64;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC65;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC141;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC142;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC143;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.Label label113;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC47;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC144;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC66;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC67;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC145;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC146;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC147;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.Label label116;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC48;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC148;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC68;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC69;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC149;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC150;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC151;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.Label label123;
        private System.Windows.Forms.Label label124;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC53;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC157;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC71;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC72;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC158;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC159;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC160;
        private System.Windows.Forms.Label label125;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.Label label127;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC54;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC164;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC74;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC75;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC165;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC166;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC167;
        private System.Windows.Forms.Label label128;
        private System.Windows.Forms.Label label129;
        private System.Windows.Forms.Label label130;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC55;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC168;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC76;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC77;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC169;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC170;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC171;
        private System.Windows.Forms.Label label131;
        private System.Windows.Forms.Label label132;
        private System.Windows.Forms.Label label133;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC56;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC172;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC78;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC79;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC173;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC174;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC175;
        private System.Windows.Forms.Label label134;
        private System.Windows.Forms.Label label135;
        private System.Windows.Forms.Label label136;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC57;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC176;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC80;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC81;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC177;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC178;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC179;
        private System.Windows.Forms.Label label137;
        private System.Windows.Forms.Label label138;
        private System.Windows.Forms.Label label139;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC58;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC180;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC82;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC83;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC181;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC182;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC183;
        private System.Windows.Forms.Label label197;
        private System.Windows.Forms.Label label198;
        private System.Windows.Forms.Label label199;
        private System.Windows.Forms.Label label200;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC78;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC79;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC80;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC260;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC81;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC261;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC262;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC263;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC264;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC122;
        private System.Windows.Forms.Label label201;
        private System.Windows.Forms.Label label202;
        private System.Windows.Forms.Label label203;
        private System.Windows.Forms.Label label204;
        private System.Windows.Forms.Label label205;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC82;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC83;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC84;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC265;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC85;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC266;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC267;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC268;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC269;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC123;
        private System.Windows.Forms.Label label206;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC124;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC270;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC271;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC272;
        private System.Windows.Forms.Label label140;
        private System.Windows.Forms.Label label141;
        private System.Windows.Forms.Label label142;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC59;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC184;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC84;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC85;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC185;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC186;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC187;
        private System.Windows.Forms.Label label143;
        private System.Windows.Forms.Label label144;
        private System.Windows.Forms.Label label145;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC60;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC188;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC86;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC87;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC189;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC190;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC191;
        private System.Windows.Forms.Label label146;
        private System.Windows.Forms.Label label147;
        private System.Windows.Forms.Label label148;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC61;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC192;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC88;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC89;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC193;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC194;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC195;
        private System.Windows.Forms.Label label149;
        private System.Windows.Forms.Label label150;
        private System.Windows.Forms.Label label151;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC62;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC196;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC90;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC91;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC197;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC198;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC199;
        private System.Windows.Forms.Label label152;
        private System.Windows.Forms.Label label153;
        private System.Windows.Forms.Label label154;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC63;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC200;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC92;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC93;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC201;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC202;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC203;
        private System.Windows.Forms.Label label155;
        private System.Windows.Forms.Label label156;
        private System.Windows.Forms.Label label157;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC64;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC204;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC94;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC95;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC205;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC206;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC207;
        private System.Windows.Forms.Label label158;
        private System.Windows.Forms.Label label159;
        private System.Windows.Forms.Label label160;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC65;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC208;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC96;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC97;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC209;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC210;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC211;
        private System.Windows.Forms.Label label161;
        private System.Windows.Forms.Label label162;
        private System.Windows.Forms.Label label163;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC66;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC212;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC98;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC99;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC213;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC214;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC215;
        private System.Windows.Forms.Label label164;
        private System.Windows.Forms.Label label165;
        private System.Windows.Forms.Label label166;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC67;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC216;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC100;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC101;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC217;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC218;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC219;
        private System.Windows.Forms.Label label167;
        private System.Windows.Forms.Label label168;
        private System.Windows.Forms.Label label169;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC68;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC220;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC102;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC103;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC221;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC222;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC223;
        private System.Windows.Forms.Label label170;
        private System.Windows.Forms.Label label171;
        private System.Windows.Forms.Label label172;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC69;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC224;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC104;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC105;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC225;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC226;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC227;
        private System.Windows.Forms.Label label173;
        private System.Windows.Forms.Label label174;
        private System.Windows.Forms.Label label175;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC70;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC228;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC106;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC107;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC229;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC230;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC231;
        private System.Windows.Forms.Label label176;
        private System.Windows.Forms.Label label177;
        private System.Windows.Forms.Label label178;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC71;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC232;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC108;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC109;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC233;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC234;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC235;
        private System.Windows.Forms.Label label179;
        private System.Windows.Forms.Label label180;
        private System.Windows.Forms.Label label181;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC72;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC236;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC110;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC111;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC237;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC238;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC239;
        private System.Windows.Forms.Label label182;
        private System.Windows.Forms.Label label183;
        private System.Windows.Forms.Label label184;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC73;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC240;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC112;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC113;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC241;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC242;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC243;
        private System.Windows.Forms.Label label185;
        private System.Windows.Forms.Label label186;
        private System.Windows.Forms.Label label187;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC74;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC244;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC114;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC115;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC245;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC246;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC247;
        private System.Windows.Forms.Label label188;
        private System.Windows.Forms.Label label189;
        private System.Windows.Forms.Label label190;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC75;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC248;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC116;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC117;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC249;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC250;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC251;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC118;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC119;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC120;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC121;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC125;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC126;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC127;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC128;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC129;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC130;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC131;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC132;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC133;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC134;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC135;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC136;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC137;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC138;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC139;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC140;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC141;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC142;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC143;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC144;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC145;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC146;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC147;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC148;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC149;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC150;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC151;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC152;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC153;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC154;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC155;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC156;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC157;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC158;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC159;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC160;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC161;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC162;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC163;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC164;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC165;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC166;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC167;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC168;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC169;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC170;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC171;
        private iSCADA.Design.Utilities.Electrical.RetangleUC retangleUC172;
        private System.Windows.Forms.Label label191;
        private System.Windows.Forms.Label label192;
        private System.Windows.Forms.Label label193;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC252;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC253;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC254;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC255;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC256;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC257;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC258;
        private System.Windows.Forms.Label label194;
        private System.Windows.Forms.Label label195;
        private System.Windows.Forms.Label label196;
        private System.Windows.Forms.Label label207;
        private System.Windows.Forms.Label label208;
        private System.Windows.Forms.Label label209;
        private System.Windows.Forms.Label label210;
        private System.Windows.Forms.Label label211;
        private System.Windows.Forms.Label label212;
        private System.Windows.Forms.Label label213;
        private System.Windows.Forms.Label label214;
        private System.Windows.Forms.Label label215;
        private System.Windows.Forms.Label label216;
        private System.Windows.Forms.Label label217;
        private System.Windows.Forms.Label label218;
        private System.Windows.Forms.Label label219;
        private System.Windows.Forms.Label label220;
        private System.Windows.Forms.Label label221;
        private System.Windows.Forms.Label label222;
        private System.Windows.Forms.Label label223;
        private System.Windows.Forms.Label label224;
        private System.Windows.Forms.Label label225;
        private System.Windows.Forms.Label label226;
        private System.Windows.Forms.Label label227;
        private System.Windows.Forms.Label label228;
        private System.Windows.Forms.Label label229;
        private System.Windows.Forms.Label label230;
        private System.Windows.Forms.Label label231;
        private System.Windows.Forms.Label label232;
        private System.Windows.Forms.Label label233;
        private System.Windows.Forms.Label label234;
        private System.Windows.Forms.Label label235;
        private System.Windows.Forms.Label label236;
        private System.Windows.Forms.Label label237;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC76;
        private System.Windows.Forms.Label label238;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC77;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC273;
        private System.Windows.Forms.Label label239;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC86;
        private System.Windows.Forms.Label label240;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC87;
        private System.Windows.Forms.Label label241;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC88;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC276;
        private System.Windows.Forms.Label label242;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC89;
        private System.Windows.Forms.Label label243;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC90;
        private System.Windows.Forms.Label label244;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC91;
        private System.Windows.Forms.Label label245;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC92;
        private iSCADA.Design.Utilities.Electrical.LineUC lineUC280;
        private System.Windows.Forms.Label label246;
        private iSCADA.Design.Utilities.Electrical.RhombusUC rhombusUC93;
        private System.Windows.Forms.Label label247;
        private System.Windows.Forms.Label label248;
        private System.Windows.Forms.Label label249;
    }
}