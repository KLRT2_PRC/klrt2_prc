﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PRC.MyClass;
using System.Security.Cryptography;
using ICSC.Database.MSSQL;
using System.Net.Http;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Threading;

namespace PRC
{
    public partial class Form_Login_backup : Form
    {
        public Form_Login_backup()
        {
            InitializeComponent();
        }
        
        private int MaxCnt = 5;
        private void Form_Login_Load(object sender, EventArgs e)
        {

        }
        
        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txbUserID.Text) && !string.IsNullOrEmpty(txbUserPw.Text))
            {
                //getLoginInfo(txbUserID.Text, txbUserPw.Text);
                if(!string.IsNullOrEmpty(UserInfo.UserID))
                {
                    UserInfo.UserID = "";
                    UserInfo.GroupName.Clear();
                    UserInfo.Permissions.Clear();
                    initControl(SHM.Frm_mf);

                }
                getUser(txbUserID.Text, txbUserPw.Text); 
            }
            else
            {
                txbUserID.Text = "";
                txbUserPw.Text = "";
                MessageBox.Show("請輸入正確帳號密碼");
            }
        }
        void oldGetUser()
        {
            bool checkUser = false;
            checkUser = CheckPermissions.CheckLogin(txbUserID.Text, txbUserPw.Text);
         
            if (checkUser)
            {
                UserInfo.UserID = txbUserID.Text;

                Dictionary<string, Dictionary<string, bool>> test = new Dictionary<string, Dictionary<string, bool>>();
                DataTable dt = new DataTable();
                dt = CheckPermissions.VaildPermissions(txbUserID.Text);
                Dictionary<string, bool> DicProg = new Dictionary<string, bool>();
                string groupid = "";
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string temp = dt.Rows[i]["GroupID"].ToString();
                    if (!temp.Equals(groupid))
                    {
                        if (!string.IsNullOrEmpty(groupid))
                        {
                            UserInfo.Permissions.Add(groupid, DicProg);
                            test.Add(groupid, DicProg);
                            DicProg = new Dictionary<string, bool>();
                        }
                        groupid = temp;

                    }
                    string prog = dt.Rows[i]["ProgID_En"].ToString();
                    bool isOpen = Convert.ToBoolean(dt.Rows[i]["DisplayFun"]);
                    //if (DicProg.ContainsKey(prog))
                    //{
                    //    isOpen = DicProg[prog] && isOpen;
                    //}
                    DicProg.Add(prog, isOpen);
                }
                test.Add(groupid, DicProg);
                UserInfo.Permissions.Add(groupid, DicProg);
                Dictionary<string, bool> right = new Dictionary<string, bool>();
                foreach (var value in UserInfo.Permissions.Values)
                {

                    foreach (KeyValuePair<string, bool> val in value)
                    {
                        bool flag = val.Value;
                        if (right.ContainsKey(val.Key))
                        {
                            flag = flag || right[val.Key];
                            right[val.Key] = flag;
                        }
                        else
                        {
                            right.Add(val.Key, flag);
                        }

                    }
                }


                foreach (Control ctl in SHM.Frm_mf.Controls)
                {
                    FindSubControl(ctl, right);
                }
                txbUserID.Text = "";
                txbUserPw.Text = "";
                ((MainFrame)ParentForm).setUserID();
            }
            else
            {
                txbUserID.Text = "";
                txbUserPw.Text = "";
                MessageBox.Show("請輸入正確帳號密碼");
            }

        }
        public void FindSubControl(Control Ctl,Dictionary<string,bool> progStatus)
        {
            //判斷是否有子控制項
            if (Ctl.Controls.Count > 0)
            {

                foreach (Control Ctl1 in Ctl.Controls)
                {
                    FindSubControl(Ctl1, progStatus);
                }

            }
            else
            {
                //若沒有就在這裡判斷控制項並結束遞迴
                if (Ctl is Button)
                {
                    string func = Ctl.Name.Substring(3);
                    if (progStatus.ContainsKey(func))
                    {
                        Ctl.Enabled = progStatus[func];
                    }
                    //MessageBox.Show(Ctl.Name);
                }
            }
        }

        public void initControl(Control ctls)
        {
            foreach (Control ctl in ctls.Controls)
            {
                if (ctl.Controls.Count > 0)
                {
                    initControl(ctl);
                }
                else
                {
                    if (ctl is Button)
                    {
                        if(!ctl.Name.Equals("btnLogin"))
                            ctl.Enabled = false;
                    }
                }
            }
        }
        public static void getLoginInfo(string UserID,string UserPw)
        {
            StringBuilder logSB = new StringBuilder();
            StringBuilder eventSB = new StringBuilder();
            string tempMsg = string.Empty;
            StringBuilder sqlSB = new StringBuilder();
            string sqlCmd = string.Empty;
            DataTable dt = new DataTable();
            int sts = 0;
            try
            {
                MD5 md5 = MD5.Create();//建立一個MD5
                byte[] source = Encoding.Default.GetBytes(UserPw);//將字串轉為Byte[]
                byte[] crypto = md5.ComputeHash(source);//進行MD5加密
                string resultPw = Convert.ToBase64String(crypto);//把加密後的字串從Byte[]轉為字串

                sqlSB.Append("SELECT * FROM Sys_User WHERE UserID = '" + UserID + "' AND UserPw ='" + resultPw + "'");
              
                sqlCmd = sqlSB.ToString();

                sts = DbOp.QuerySql(SHM.DBIp, SHM.DBName, SHM.DBUser, SHM.DBPasswd, ref sqlCmd, ref dt, SHM.LOG_LEVEL_DBDetail, ref tempMsg);
                if (sts > 0)
                {
                    SHM.Staff = dt.Rows[0]["UserID"].ToString();
                }

                logSB.AppendLine(tempMsg);



            }
            catch (Exception ex)
            {
                eventSB.AppendLine("Exception: " + ex.Message);
            }
            finally
            {
                if (eventSB.Length > 0)
                    SHM.QueueEvent.Enqueue(eventSB.ToString());
                if (logSB.Length > 0)
                    SHM.QueueDatabase.Enqueue(logSB.ToString());
            }

        }

 
        private async void getUser(string userid,string userpw)
        {
           
            HttpClient client = new HttpClient();
            string uritmp = "http://localhost:48965/api/UserInfo?UserID=" + userid + "&UserPw=" + userpw;
            Uri uri = new Uri(uritmp);

            HttpResponseMessage response = await client.GetAsync(uri);
            response.EnsureSuccessStatusCode();
            string responseBody = await response.Content.ReadAsStringAsync();
            ///List<UserInfos> temp = JsonConvert.DeserializeObject<List<UserInfos>>(responseBody);
            SHM.user = JsonConvert.DeserializeObject<List<UserInfos>>(responseBody);
            bool checkUser = false;
            checkUser = SHM.user[0].isUser;
            if (checkUser)
            {

                //紀錄登入時間
                LoginRecord login = new LoginRecord();
                login.UserID = userid;
                login.LoginInTime = DateTime.Now;
                login.LoginOutTime = new DateTime(2000, 01, 01);
                login.errRecord.Clear();
                SHM.loginRec.Add(login);
                InsertLoginRec(login.UserID, login.LoginInTime, login.LoginOutTime);


                UserInfo.UserID = txbUserID.Text;

                Dictionary<string, Dictionary<string, bool>> test = new Dictionary<string, Dictionary<string, bool>>();             
                Dictionary<string, bool> DicProg = new Dictionary<string, bool>();
                string groupid = "";
                for(int i =0;i<SHM.user[0].GroupInfo.Count;i++)
                {
                    GroupInfo gi = SHM.user[0].GroupInfo[i];
                    string temp = gi.GroupID;
                    if(!temp.Equals(groupid))
                    {
                        if (!string.IsNullOrEmpty(groupid))
                        {
                            SHM.user[0].Permissions.Add(groupid, DicProg);
                            //test.Add(groupid, DicProg);
                            DicProg = new Dictionary<string, bool>();
                        }
                        groupid = temp;
                    }
                    string prog = gi.ProgID_En;
                    bool isOpen = gi.DisplayFun;
                    DicProg.Add(prog, isOpen);
                }
                SHM.user[0].Permissions.Add(groupid, DicProg);
                
               
                Dictionary<string, bool> right = new Dictionary<string, bool>();
                foreach (var value in SHM.user[0].Permissions.Values)
                {

                    foreach (KeyValuePair<string, bool> val in value)
                    {
                        bool flag = val.Value;
                        if (right.ContainsKey(val.Key))
                        {
                            flag = flag || right[val.Key];
                            right[val.Key] = flag;
                        }
                        else
                        {
                            right.Add(val.Key, flag);
                        }

                    }
                }


                foreach (Control ctl in SHM.Frm_mf.Controls)
                {
                    FindSubControl(ctl, right);
                }
                txbUserID.Text = "";
                txbUserPw.Text = "";
                ((MainFrame)ParentForm).setUserID();
            }
            else
            {
                txbUserID.Text = "";
                txbUserPw.Text = "";
                
                //MessageBox.Show("帳密錯誤" + logCnt + "次，還剩下" + (MaxCnt - logCnt) + "次");
              

                if(SHM.loginRec.Count > 0)
                {
                    var find = from data in SHM.loginRec
                               where data.UserID == userid
                               select data;
                    if(find.Count() > 0)
                    {
                        foreach (var item in find)
                        {
                            ErrRecord err = new ErrRecord();
                            item.logCnt++;
                            err.ErrCnt = item.logCnt;
                            err.ErrTime = DateTime.Now;
                            item.errRecord.Add(err);
                            InsertErrRec(userid, err.ErrCnt, err.ErrTime);
                           if(item.logCnt >= MaxCnt)
                            {
                                MessageBox.Show("你已經失敗" + MaxCnt + "次，帳號鎖定請洽管理員!");
                                UpdateErrorUser(userid);
                            }
                            else
                            {
                                MessageBox.Show("密碼輸入錯誤次數：" + err.ErrCnt + "次，還剩下 " + (MaxCnt - err.ErrCnt) + "次");
                                
                            }
                        }
                        
                    }
                    else
                    {
                        LoginRecord errLogin = new LoginRecord();
                        errLogin.logCnt++;
                        errLogin.UserID = userid;
                        ErrRecord err = new ErrRecord();
                        err.ErrCnt = errLogin.logCnt;
                        err.ErrTime = DateTime.Now;
                        errLogin.errRecord.Add(err);
                        SHM.loginRec.Add(errLogin);
                        InsertErrRec(userid, err.ErrCnt, err.ErrTime);
                        MessageBox.Show("密碼輸入錯誤次數：" + err.ErrCnt + "次，還剩下 " + (MaxCnt - err.ErrCnt) + "次");
                    }
                   
                }
                else
                {
                    LoginRecord errLogin = new LoginRecord();
                    errLogin.logCnt++;
                    errLogin.UserID = userid;
                    ErrRecord err = new ErrRecord();
                    err.ErrCnt = errLogin.logCnt;
                    err.ErrTime = DateTime.Now;
                    errLogin.errRecord.Add(err);
                    SHM.loginRec.Add(errLogin);
                    InsertErrRec(userid, err.ErrCnt, err.ErrTime);
                    MessageBox.Show("密碼輸入錯誤次數：" + err.ErrCnt + "次，還剩下 " + (MaxCnt - err.ErrCnt) + "次");
                }
               
            }

        }

        private void InsertErrRec(string userid,int errCnt,DateTime errTime)
        {
           
            string tempMsg = string.Empty;
            StringBuilder sqlSB = new StringBuilder();
            string sqlCmd = string.Empty;
            int sts = 0;
            try
            {

                sqlSB.Append("INSERT INTO ErrorLoginRecord (UserID, ErrCnt, ErrTime ) ");
                sqlSB.Append(" VALUES (");
                sqlSB.Append("'" + userid + "',");
                sqlSB.Append("'" + errCnt + "',");
               
                sqlSB.Append("'" + errTime.ToString("yyyy/MM/dd HH:mm:ss") + "') ;");

                sqlCmd = sqlSB.ToString();

                sts = DbOp.ExecuteSql(SHM.DBIp, SHM.DBName, SHM.DBUser, SHM.DBPasswd, ref sqlCmd, SHM.LOG_LEVEL_DBDetail, ref tempMsg);

            }
            catch (Exception ex)
            {
             
            }
            finally
            {
              
            }
        }


        private void InsertLoginRec(string userid, DateTime LoginIn , DateTime LoginOut)
        {

            string tempMsg = string.Empty;
            StringBuilder sqlSB = new StringBuilder();
            string sqlCmd = string.Empty;
            int sts = 0;
            try
            {

                sqlSB.Append("INSERT INTO Sys_LoginRecord (UserID, LoginInTime, LoginOutTime ) ");
                sqlSB.Append(" VALUES (");
                sqlSB.Append("'" + userid + "',");
                sqlSB.Append("'" + LoginIn.ToString("yyyy/MM/dd HH:mm:ss") + "',");
                
                sqlSB.Append("'" + LoginOut.ToString("yyyy/MM/dd HH:mm:ss") + "') ;");

                sqlCmd = sqlSB.ToString();

                sts = DbOp.ExecuteSql(SHM.DBIp, SHM.DBName, SHM.DBUser, SHM.DBPasswd, ref sqlCmd, SHM.LOG_LEVEL_DBDetail, ref tempMsg);

            }
            catch (Exception ex)
            {

            }
            finally
            {

            }
        }

        private void UpdateErrorUser(string userid)
        {

            string tempMsg = string.Empty;
            StringBuilder sqlSB = new StringBuilder();
            string sqlCmd = string.Empty;
            int sts = 0;
            try
            {

                StringBuilder sb = new StringBuilder();
                sb.Append("UPDATE Sys_User SET ");
             
                sb.Append(" UserPrivilege = '0', ");
                sb.Append(" Description = '帳號鎖定' ,");
               
                sb.Append(" UpdateTime = '" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "'");

                sb.Append(" WHERE UserID = '" + userid + "'");
                sqlCmd = sb.ToString();

                sts = DbOp.ExecuteSql(SHM.DBIp, SHM.DBName, SHM.DBUser, SHM.DBPasswd, ref sqlCmd, SHM.LOG_LEVEL_DBDetail, ref tempMsg);

            }
            catch (Exception ex)
            {

            }
            finally
            {

            }
        }

    }
}
