﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Imaging;

namespace iSCADA.Design.Utilities.Electrical
{
    public partial class ButtonStatus : ElectricalSymbol
    {

        private Button btn = new Button();
        public ButtonStatus()
        {
            InitializeComponent();

            this.Size = new Size(140, 40);
            
            btn.Font = new Font("微軟正黑體", 12, FontStyle.Bold);
            btn.FlatStyle = FlatStyle.Flat;
            btn.FlatAppearance.BorderSize = this.ExtenderWidth;
            btn.Size = new Size(100, 40);
            btn.Location = new Point(20, 0);
            
            btn.Click += new EventHandler(this.btn_Click);
            this.Controls.Add(btn);

        }

        private void btn_Click(object sender, EventArgs e)
        {
            
        }
  

        protected override void OnPaint(System.Windows.Forms.PaintEventArgs e)
        {
            base.OnPaint(e);
            this.Paint(e);
        }
        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            ResizeNow(e);
        }

        private void ResizeNow(EventArgs ea)
        {
            this.Refresh();
        }
        private new void Paint(System.Windows.Forms.PaintEventArgs e)
        {

            int btnW = 0, btnH = 0;
            int btnX = 0, btnY = 0;
            btnW = (int)Math.Ceiling(this.Width * 0.8);
            btnH = this.Height;
            btn.Size = new Size(btnW, btnH);
            btnX = btnW = (int)Math.Ceiling(this.Width * 0.1);
            btn.Location = new Point(btnX, btnY);
            Rectangle rec = this.DisplayRectangle;
            //Rectangle newrec = new Rectangle(rec.Left, rec.Top, rec.Width - 1, rec.Height - 1);
            Rectangle newrec = new Rectangle(rec.X, rec.Y, rec.Width - 1, rec.Height - 1);
           
                using (Pen newpen = new Pen(this.Brush, this.ExtenderWidth))
                {
                    //e.Graphics.DrawEllipse(newpen, newrec);
                    e.Graphics.DrawRectangle(newpen, newrec);
                    //e.Graphics.DrawEllipse(this.Pen, newrec);
                }
         

        }

        private string _btnText = "";
        [Category("Electrical Symbols"), Description("BtnText")]
        public string BtnText
        {
            get { return _btnText; }
            set
            {
                _btnText = value;
                btn.Text = value;
                this.Refresh();
            }
        }
        private Color col = new Color();

        [Category("Electrical Symbols"), Description("BtnBackColor")]
        public Color BtnBackColor
        {
            get { return col; }
            set
            {
                col = value;
                btn.BackColor = value;
                this.Refresh();
            }
        }

        public void setBtnColor(Color color)
        {
            btn.BackColor = color;
            this.Refresh();
        }
    }
}
