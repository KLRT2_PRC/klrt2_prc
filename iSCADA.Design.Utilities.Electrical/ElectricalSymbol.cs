﻿using System;
using System.Text;
using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Drawing;
using System.Windows.Forms;

namespace iSCADA.Design.Utilities.Electrical
{
    public abstract class ElectricalSymbol : UserControl, ISerializable
    {
        public event EventHandler<EventArgs> OnToolStripItemClick;

        private bool _fillGradiant = false;
        private SymbolState _state = SymbolState.Off;
        private SymbolStyle _style = SymbolStyle.Default;
        //private System.Drawing.Pen _pen = System.Drawing.Pens.Lime;
        //private System.Drawing.Pen _pen = new Pen(Brushes.Lime);
        private System.Drawing.Pen _pen = new Pen(Brushes.Black);
        private DashStyle _dashstyle = DashStyle.Solid;
        private LineCap _startcap = LineCap.NoAnchor;
        private LineCap _endcap = LineCap.NoAnchor;
        private int _extenderWidth = 3;
        private System.Drawing.Brush _brush = System.Drawing.Brushes.Black;
        //private System.Drawing.Brush _brush = new Brush();
        //private int _borderWidth = 3;


        private ToolStripButton toolStripButton1;
        private ToolStripButton toolStripButton2;
        private ToolStripButton toolStripButton3;
        private ContextMenuStrip contextMenuStrip1;
        private IContainer components;
        private ToolStripMenuItem toolStripMenuItem1;
        private ToolStripMenuItem toolStripMenuItem2;
        private ContextMenuStrip contextMenuStrip2;
        private ToolStripMenuItem rearrangeButtonsToolStripMenuItem;
        private ToolStripMenuItem selectIconsToolStripMenuItem;
        private ToolStrip toolStrip1;

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            throw new NotImplementedException();
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // ElectricalSymbol
            // 
            this.BackColor = System.Drawing.Color.Transparent;
            this.SetStyle(System.Windows.Forms.ControlStyles.SupportsTransparentBackColor, true);
            this.Name = "ElectricalSymbol";
            //this._pen.DashStyle =(System.Drawing.Drawing2D.DashStyle) this._dashstyle;
            this.ResumeLayout(false);
        }

        //[Category("Electrical Symbols"), Description("Get the value of drawing brush"), DefaultValueAttribute(true)]
        [Category("Electrical Symbols"), Description("Get the value of drawing brush")]
        public virtual System.Drawing.Brush Brush
        {
            get { return this._brush; }
            set
            {
                this._brush = value;
                this.Refresh();
            }
        }

        //[Category("Electrical Symbols"), Description("Get the value of drawing pen"), DefaultValueAttribute(true)]
        [Category("Electrical Symbols"), Description("Get the value of drawing pen")]
        //public virtual System.Drawing.Pen Pen
        public virtual System.Drawing.Pen Pen
        {
            get { return this._pen; }
            set
            {
                //System.Drawing.Pen p = new Pen(this._brush);
                //p = value;
                //p.Width = this._extenderWidth;
                this._pen = value;
                //this._pen.Width = this.ExtenderWidth;
                //this._pen.Width = this._extenderWidth;
                //this._pen.Width = this.ExtenderWidth;
                //switch (this._dashstyle)
                //{
                //    case DashStyle.Dash:
                //        this._pen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash;
                //        break;
                //    case DashStyle.DashDot:
                //        this._pen.DashStyle = System.Drawing.Drawing2D.DashStyle.DashDot;
                //        break;
                //    case DashStyle.DashDotDot:
                //        this._pen.DashStyle = System.Drawing.Drawing2D.DashStyle.DashDotDot;
                //        break;
                //    case DashStyle.Dot:
                //        this._pen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dot;
                //        break;
                //    case DashStyle.Solid:
                //        this._pen.DashStyle = System.Drawing.Drawing2D.DashStyle.Solid;
                //        break;
                //}
                this.Refresh();
            }
        }

        ////[Category("Electrical Symbols"), Description("Get or set the DashStyle"), DefaultValueAttribute(DashStyle.Solid)]
        [Category("Electrical Symbols"), Description("Get or set the DashStyle")]
        public DashStyle dashstyle
        {
            get { return this._dashstyle; }
            set
            {
                this._dashstyle = value;
                switch (value)
                {
                    case DashStyle.Dash:
                        this._pen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash;
                        break;
                    case DashStyle.DashDot:
                        this._pen.DashStyle = System.Drawing.Drawing2D.DashStyle.DashDot;
                        break;
                    case DashStyle.DashDotDot:
                        this._pen.DashStyle = System.Drawing.Drawing2D.DashStyle.DashDotDot;
                        break;
                    case DashStyle.Dot:
                        this._pen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dot;
                        break;
                    case DashStyle.Solid:
                        this._pen.DashStyle = System.Drawing.Drawing2D.DashStyle.Solid;
                        break;
                }
                this.Refresh();
            }
        }

        [Category("Electrical Symbols"), Description("Get or set the StartCap"), DefaultValueAttribute(LineCap.NoAnchor)]
        public LineCap StartCap
        {
            get { return this._startcap; }
            set
            {
                this._startcap = value;
                switch (value)
                {
                    case LineCap.ArrowAnchor:
                        this._pen.StartCap = System.Drawing.Drawing2D.LineCap.ArrowAnchor;
                        break;
                    case LineCap.DiamondAnchor:
                        this._pen.StartCap = System.Drawing.Drawing2D.LineCap.DiamondAnchor;
                        break;
                    case LineCap.Flat:
                        this._pen.StartCap = System.Drawing.Drawing2D.LineCap.Flat;
                        break;
                    case LineCap.Round:
                        this._pen.StartCap = System.Drawing.Drawing2D.LineCap.Round;
                        break;
                    case LineCap.RoundAnchor:
                        this._pen.StartCap = System.Drawing.Drawing2D.LineCap.RoundAnchor;
                        break;
                    case LineCap.Square:
                        this._pen.StartCap = System.Drawing.Drawing2D.LineCap.Square;
                        break;
                    case LineCap.SquareAnchor:
                        this._pen.StartCap = System.Drawing.Drawing2D.LineCap.SquareAnchor;
                        break;
                    case LineCap.Triangle:
                        this._pen.StartCap = System.Drawing.Drawing2D.LineCap.Triangle;
                        break;
                    default:
                        this._pen.StartCap = System.Drawing.Drawing2D.LineCap.NoAnchor;
                        break;
                }
                this.Refresh();
            }
        }

        [Category("Electrical Symbols"), Description("Get or set the EndCap"), DefaultValueAttribute(LineCap.NoAnchor)]
        public LineCap EndCap
        {
            get { return this._endcap; }
            set
            {
                this._endcap = value;
                switch (value)
                {
                    case LineCap.ArrowAnchor:
                        this._pen.EndCap = System.Drawing.Drawing2D.LineCap.ArrowAnchor;
                        break;
                    case LineCap.DiamondAnchor:
                        this._pen.EndCap = System.Drawing.Drawing2D.LineCap.DiamondAnchor;
                        break;
                    case LineCap.Flat:
                        this._pen.EndCap = System.Drawing.Drawing2D.LineCap.Flat;
                        break;
                    case LineCap.Round:
                        this._pen.EndCap = System.Drawing.Drawing2D.LineCap.Round;
                        break;
                    case LineCap.RoundAnchor:
                        this._pen.EndCap = System.Drawing.Drawing2D.LineCap.RoundAnchor;
                        break;
                    case LineCap.Square:
                        this._pen.EndCap = System.Drawing.Drawing2D.LineCap.Square;
                        break;
                    case LineCap.SquareAnchor:
                        this._pen.EndCap = System.Drawing.Drawing2D.LineCap.SquareAnchor;
                        break;
                    case LineCap.Triangle:
                        this._pen.EndCap = System.Drawing.Drawing2D.LineCap.Triangle;
                        break;
                    default:
                        this._pen.EndCap = System.Drawing.Drawing2D.LineCap.NoAnchor;
                        break;
                }
                this.Refresh();
            }
        }


        [Category("Electrical Symbols"), Description("Get or set the Width of the Line"),DefaultValueAttribute(true)]
        public virtual int ExtenderWidth
        {
            get { return this._extenderWidth; }
            set
            {
                if (value <= 0) return;
                this._extenderWidth = value;
                //this._pen.Width = this._extenderWidth;
                //this._pen.Width = value;
                this.Refresh();
            }
        }

        [Category("Electrical Symbols"), Description("Get or set the value of Gradiant Fill"), DefaultValueAttribute(true)]
        public virtual bool Fill
        {
            get { return this._fillGradiant; }
            set
            {
                this._fillGradiant = value;
                this.Refresh();
            }
        }

        [Category("Electrical Symbols"), Description("Get or set the style of the symbol"), DefaultValueAttribute(true)]
        public virtual SymbolStyle Style
        {
            get { return this._style; }
            set
            {
                this._style = value;
                this.Refresh();
            }
        }


        [Category("Electrical Symbols"), Description("Get or set the State"), DefaultValueAttribute(SymbolState.Off)]
        public virtual SymbolState State
        {
            get { return this._state; }
            set
            {
                this._state = value;
                switch (this._state)
                {
                    case SymbolState.Disconnected:
                        this._brush = System.Drawing.Brushes.Blue;
                        this._pen = System.Drawing.Pens.Blue;
                        break;
                    case SymbolState.On:
                        //this._brush = System.Drawing.Brushes.Lime;
                        //this._pen = System.Drawing.Pens.Lime;
                        this._brush = System.Drawing.Brushes.Red;
                        this._pen = System.Drawing.Pens.Red;
                        break;
                    case SymbolState.Off:
                        this._brush = System.Drawing.Brushes.Black;
                        this._pen = System.Drawing.Pens.Black;
                        break;
                    case SymbolState.Error:
                        //this._brush = System.Drawing.Brushes.Red;
                        //this._pen = System.Drawing.Pens.Red;
                        this._brush = System.Drawing.Brushes.Lime;
                        this._pen = System.Drawing.Pens.Lime;
                        break;
                    case SymbolState.Level1_Alarm:
                        this._brush = System.Drawing.Brushes.Gold;
                        this._pen = System.Drawing.Pens.Gold;
                        break;
                    case SymbolState.Level2_Alarm:
                        this._brush = System.Drawing.Brushes.Orange;
                        this._pen = System.Drawing.Pens.Orange;
                        break;
                }
                //this._brush = this._state == SymbolState.On ? System.Drawing.Brushes.Red : System.Drawing.Brushes.Lime;
                //this._pen = this._state == SymbolState.On ? System.Drawing.Pens.Red : System.Drawing.Pens.Lime;
                this.Refresh();
            }
        }

        //[Category("Electrical Symbols"), Description("Get or set the size"), DefaultValueAttribute(30)]
        //public virtual int SymbolSize
        //{
        //    get { return this._symbolSize; }
        //    set
        //    {
        //        this._symbolSize = value;
        //        this.Refresh();
        //    }
        //}

        //[Category("Electrical Symbols"), Description("Get or set the border width"), DefaultValueAttribute(1)]
        //public virtual int BorderWidth
        //{
        //    get { return this._borderWidth; }
        //    set
        //    {
        //        this._borderWidth = value;
        //        this._pen.Width = value;
        //        this.Refresh();
        //    }
        //}

        [Category("Electrical Symbols"), Description("Get or set the Group ID"), DefaultValueAttribute(1)]
        public virtual System.String GroupID
        {
            get; set;
        }

        //protected override void OnMouseEnter(EventArgs e)
        //{
        //    base.OnMouseEnter(e);
        //    ToolTip tooltip = new ToolTip();
        //    tooltip.SetToolTip(this, this.Name);
        //}

        //protected override void OnMouseClick(MouseEventArgs e)
        //{
        //    base.OnMouseClick(e);
        //    if (e.Button == MouseButtons.Right)
        //    {
        //        ContextMenuStrip cms = new ContextMenuStrip();
        //        ToolStripMenuItem tsm1 = new ToolStripMenuItem();
        //        tsm1.Name = "test01";
        //        tsm1.Size = new System.Drawing.Size(184, 36);
        //        tsm1.Text = "指令";
        //        ToolStripMenuItem tsm1_1 = new ToolStripMenuItem();
        //        tsm1_1.Name = "test01_1";
        //        tsm1_1.Size = new System.Drawing.Size(184, 36);
        //        tsm1_1.Text = "資料更新";
        //        ToolStripMenuItem tsm1_2 = new ToolStripMenuItem();
        //        tsm1_2.Name = "test01_2";
        //        tsm1_2.Size = new System.Drawing.Size(184, 36);
        //        tsm1_2.Text = "發送控制訊號";
        //        tsm1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
        //            tsm1_1,tsm1_2
        //        });
        //        tsm1_1.Click += new EventHandler(ToolStripItem_Click);
        //        tsm1_2.Click += new EventHandler(ToolStripItem_Click);
        //        cms.Items.Add(tsm1);
        //        cms.Show(this, new Point(e.X, e.Y));
        //    }
        //}

        protected virtual void ToolStripItem_Click(object sender, EventArgs e)
        {
            // _RaiseEvent( sender, e);
            if (OnToolStripItemClick != null)
            {
                OnToolStripItemClick.Invoke(sender, e);
            }
        }

        //private void _RaiseEvent(object sender, EventArgs e)
        //{
        //    if (OnToolStripItemClick != null)
        //    {
        //        OnToolStripItemClick.Invoke(sender, e);
        //    }
        //}
    }
    #region Enums
    public enum SymbolStyle
    {
        Default = 0,
        Advance = 1
    }

    public enum SymbolDirection
    {
        Left = 0,
        Top = 1,
        Right = 2,
        Bottom = 3
    }

    public enum SymbolOrientation
    {
        Horizontal,
        Verticle
    }

    public enum ConnectHands
    {
        None = 0,
        LefOrToptHand = 1,
        RightOrBottomHand = 2,
        Both = 3
    }

    public enum SymbolState
    {
        Disconnected,//未連線/Gray
        On,//正常/上腺/Lime
        Off,//關機/下線/Black
        Error,//故障/Red
        Level1_Alarm,//警報1/
        Level2_Alarm,//警報2/
        Default //預設第二種ON模式
    }

    public enum DashStyle
    {
        Dash,
        DashDot,
        DashDotDot,
        Dot,
        Solid
    }
    public enum LineCap
    {
        ArrowAnchor,
        DiamondAnchor,
        Flat,
        NoAnchor,
        Round,
        RoundAnchor,
        Square,
        SquareAnchor,
        Triangle
    }
    //public enum TrasformerType
    //{
    //    TwoWindingTrasformer = 1,
    //    ThreeWindingTrasformer = 2,
    //    AutoTrasformer = 3,
    //    CurrentTrasformer = 4,
    //    PotentialTrasformer = 5
    //}

    #endregion Enums

}
