﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iSCADA.Design.Utilities.Electrical
{
    public partial class Transformer02UC : ElectricalSymbol
    {
        private SymbolOrientation _orientation = SymbolOrientation.Verticle;
        private string drawString = string.Empty;
        public Transformer02UC()
        {
            InitializeComponent();
        }
        [Category("Electrical Symbols"), Description("Get or set the Orientation"), DefaultValueAttribute(SymbolOrientation.Horizontal)]
        public SymbolOrientation Orientation
        {
            get { return this._orientation; }
            set
            {
                this._orientation = value;
                this.Refresh();
            }
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            this.Paint(e);
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            ResizeNow(e);
        }

        private void ResizeNow(EventArgs ea)
        {
            this.Refresh();
        }

        private new void Paint(PaintEventArgs e)
        {
            System.Drawing.Drawing2D.PathGradientBrush _gBrush = null;
            System.Drawing.Drawing2D.GraphicsPath _gpath = new System.Drawing.Drawing2D.GraphicsPath();
            drawString = "Y";
            //定義圖形大小，可任意縮放
            Double defaultsize = (this.Orientation == SymbolOrientation.Horizontal ? (this.Width / 4) : (this.Height / 4));
            float switchsize = (int)Math.Ceiling(defaultsize);
            float circlesize = switchsize;// * .1f;
            //////////////////////////////////////////////////////
            //兩個圓
            float C1X1 = 0, C1Y1 = 0;
            float C2X1 = 0, C2Y1 = 0;
            //兩條線
            float L1X1 = 0, L1Y1 = 0, L1X2 = 0, L1Y2 = 0;
            float L2X1 = 0, L2Y1 = 0, L2X2 = 0, L2Y2 = 0;
            //三角形
            PointF pointA = new PointF();
            PointF pointB = new PointF();
            PointF pointC = new PointF();
            //顯示字型
            FontFamily family = new FontFamily("Times New Roman");
            int fontStyle = (int)FontStyle.Regular;
            StringFormat format = StringFormat.GenericDefault;
            //字型位置
            float SX1 = 0, SY1 = 0;
            //////////////////////////////////////////////////////
            if (this.Orientation == SymbolOrientation.Verticle)
            {
                L1Y1 = 0;
                L1X2 = L1X1;
                L1Y2 = L1Y1 + switchsize;
                C1X1 = 0;
                L1X1 = C1X1 + (circlesize / 2);
                L1X2 = L1X1;
                C1Y1 = L1Y2;
                C2X1 = C1X1;
                C2Y1 = C1Y1 + (3 * circlesize / 4);
                L2X1 = L1X2;
                L2Y1 = C2Y1 + circlesize;
                L2X2 = L2X1;
                L2Y2 = L2Y1 + switchsize;

                pointA.X = C1X1 + (circlesize / 4);
                pointA.Y = L1Y2 + (circlesize * 5 / 8);
                pointB.X = L1X2;
                pointB.Y = L1Y2 + (circlesize / 8);
                pointC.X = (float)(C1X1 + circlesize * 3 / 4);
                pointC.Y = pointA.Y;

                SX1 = C2X1 + (circlesize / 4);
                SY1 = C2Y1 + (circlesize / 4);

                this.Width = (int)Math.Ceiling(circlesize + this.ExtenderWidth);
                this.Height = (int)Math.Ceiling(switchsize * 4);
            }
            else
            {
                L1X1 = 0;
                C1Y1 = 0;// L1Y2 - (circlesize / 2);
                L1Y1 = C1Y1+(circlesize/2);
                L1X2 = L1X1 + switchsize;
                L1Y2 = L1Y1 ;
                C1X1 = L1X2;
                
                C2X1 = C1X1 + (3 * circlesize / 4);
                C2Y1 = C1Y1 ;
                L2X1 = C2X1+(circlesize);
                L2Y1 = L1Y2;
                L2X2 = L2X1+switchsize;
                L2Y2 = L2Y1;

                pointA.X = C1X1 + (circlesize / 8);
                pointA.Y = L1Y2 + (circlesize/3);
                pointB.X = L1X2+(circlesize*3/7);
                pointB.Y = C1Y1 + (circlesize/ 4);
                pointC.X = (float)(C1X1 + circlesize * 3 / 4);
                pointC.Y = pointA.Y;

                SX1 = C2X1 + (circlesize / 4);
                SY1 = C2Y1 + (circlesize / 4);

                this.Width = (int)Math.Ceiling(switchsize * 4);
                this.Height = (int)Math.Ceiling(circlesize + this.ExtenderWidth);
            }

            //////////////////////////////////////////////////////
            _gpath.AddArc(C1X1, C1Y1, circlesize, circlesize, 0, 360);
            _gpath.AddArc(C2X1, C2Y1, circlesize, circlesize, 0, 360);
            _gpath.CloseFigure();
            _gpath.AddLine(L1X1, L1Y1, L1X2, L1Y2);
            _gpath.CloseFigure();
            _gpath.AddLine(L2X1, L2Y1, L2X2, L2Y2);
            _gpath.CloseFigure();
            _gpath.AddLines(new PointF[] { pointA, pointB, pointC, pointA });
            _gpath.CloseFigure();
            _gpath.AddString(drawString, family, fontStyle, circlesize / 2, new Point((int)SX1, (int)SY1), format);
            _gpath.CloseFigure();
            //e.Graphics.DrawPath(this.Pen, _gpath);
            using (Pen newpen = new Pen(this.Brush, this.ExtenderWidth))
            {
                e.Graphics.DrawPath(newpen, _gpath);
            }
        }
    }
}
