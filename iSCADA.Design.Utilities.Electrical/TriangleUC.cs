﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iSCADA.Design.Utilities.Electrical
{
    public partial class TriangleUC : ElectricalSymbol
    {
        private SymbolOrientation _orientation = SymbolOrientation.Horizontal;
        private SymbolState _state = SymbolState.Disconnected;
        private DashStyle _dashstyle = DashStyle.Solid;

        public TriangleUC()
        {
            InitializeComponent();
        }

        [Category("Electrical Symbols"), Description("Get or set the Orientation"), DefaultValueAttribute(SymbolOrientation.Horizontal)]
        public SymbolOrientation Orientation
        {
            get { return this._orientation; }
            set
            {
                this._orientation = value;
                this.Refresh();
            }
        }
        protected override void OnPaint(System.Windows.Forms.PaintEventArgs e)
        {
            base.OnPaint(e);
            this.Paint(e);
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            ResizeNow(e);
        }

        private void ResizeNow(EventArgs ea)
        {
            this.Refresh();
        }
        private new void Paint(System.Windows.Forms.PaintEventArgs e)
        {
            Double defaultsize = (this.Orientation == SymbolOrientation.Horizontal ? (this.Width / 3) : (this.Height / 3));
            int switchsize = (int)Math.Ceiling(defaultsize);
            int start_x = 0, start_y = 0, end_x = 0, end_y = 0,third_x=0,third_y=0;
            int lines_x = 0, lines_y = 0, linee_x = 0, linee_y = 0;
            Point[] pnt = new Point[3];
            /////////////////////////////////////////////////////////////////
            if (this._orientation == SymbolOrientation.Horizontal)
            {
                //start_x = switchsize;
                start_y = this.DisplayRectangle.Height-1;
                pnt[0].X = start_x;
                pnt[0].Y = start_y ;
                

                end_x = start_x + this.DisplayRectangle.Width;
                end_y = start_y;
                pnt[1].X = end_x;
                pnt[1].Y = end_y;

                third_x = start_x + this.DisplayRectangle.Width/2;
                third_y = start_y - this.DisplayRectangle.Height;
                pnt[2].X = third_x;
                pnt[2].Y = third_y;

                lines_x = start_x;
                lines_y = third_y+1;

                linee_x = end_x;
                linee_y = third_y+1;
            }
            else
            {
                pnt[0].X = start_x;
                pnt[0].Y = start_y;


                end_x = start_x + this.DisplayRectangle.Width;
                end_y = start_y;
                pnt[1].X = end_x;
                pnt[1].Y = end_y;

                third_x = start_x + this.DisplayRectangle.Width / 2;
                third_y = start_y +this.DisplayRectangle.Height-1;
                pnt[2].X = third_x;
                pnt[2].Y = third_y;

                lines_x = start_x;
                lines_y = third_y - 1;

                linee_x = end_x;
                linee_y = third_y - 1;
            }

            switch (_state)
            {
                case SymbolState.On:
                    using (Pen newpen = new Pen(Color.Red, this.ExtenderWidth))
                    {
                        e.Graphics.FillPolygon(this.Brush, pnt);
                        e.Graphics.DrawLine(newpen, lines_x, lines_y, linee_x, linee_y);
                        //e.Graphics.DrawPolygon(newpen, pnt);
                        //e.Graphics.DrawLine(newpen, start_x, start_y, end_x, end_y);
                    }
                    break;
                case SymbolState.Error:
                    using (Pen newpen = new Pen(Color.Lime, this.ExtenderWidth))
                    {
                        //e.Graphics.DrawLine(newpen, start_x, start_y, end_x, end_y);
                        //e.Graphics.DrawPolygon(newpen, pnt);
                        e.Graphics.FillPolygon(this.Brush, pnt);
                        e.Graphics.DrawLine(newpen, lines_x, lines_y, linee_x, linee_y);
                    }
                    break;
                case SymbolState.Off:
                    using (Pen newpen = new Pen(Color.Black, this.ExtenderWidth))
                    {
                        //e.Graphics.DrawLine(newpen, start_x, start_y, end_x, end_y);
                        //e.Graphics.DrawPolygon(newpen, pnt);
                        e.Graphics.FillPolygon(this.Brush, pnt);
                        e.Graphics.DrawLine(newpen, lines_x, lines_y, linee_x, linee_y);
                    }
                    break;
                case SymbolState.Level1_Alarm:
                    using (Pen newpen = new Pen(Color.Gold, this.ExtenderWidth))
                    {
                        //e.Graphics.DrawLine(newpen, start_x, start_y, end_x, end_y);
                        //e.Graphics.DrawPolygon(newpen, pnt);
                        e.Graphics.FillPolygon(this.Brush, pnt);
                        e.Graphics.DrawLine(newpen, lines_x, lines_y, linee_x, linee_y);
                    }
                    break;
                case SymbolState.Level2_Alarm:
                    using (Pen newpen = new Pen(Color.Orange, this.ExtenderWidth))
                    {
                        //e.Graphics.DrawLine(newpen, start_x, start_y, end_x, end_y);
                        //e.Graphics.DrawPolygon(newpen, pnt);
                        e.Graphics.FillPolygon(this.Brush, pnt);
                        e.Graphics.DrawLine(newpen, lines_x, lines_y, linee_x, linee_y);
                    }
                    break;
                case SymbolState.Disconnected:
                    using (Pen newpen = new Pen(Color.Black, this.ExtenderWidth))
                    {
                        //e.Graphics.DrawLine(newpen, start_x, start_y, end_x, end_y);
                        //e.Graphics.DrawPolygon(newpen, pnt);
                        e.Graphics.FillPolygon(this.Brush, pnt);
                        e.Graphics.DrawLine(newpen, lines_x, lines_y, linee_x, linee_y);
                    }
                    break;
                case SymbolState.Default:
                    using (Pen newpen = new Pen(Color.Blue, this.ExtenderWidth))
                    {
                        //e.Graphics.DrawLine(newpen, start_x, start_y, end_x, end_y);
                        //e.Graphics.DrawPolygon(newpen, pnt);
                        e.Graphics.FillPolygon(this.Brush, pnt);
                        e.Graphics.DrawLine(newpen, lines_x, lines_y, linee_x, linee_y);
                    }
                    break;
            }
        }
    }
}
