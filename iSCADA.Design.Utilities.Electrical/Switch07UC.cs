﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iSCADA.Design.Utilities.Electrical
{
    public class Switch07UC : ElectricalSymbol
    {
        private SymbolOrientation _orientation = SymbolOrientation.Horizontal;


        //[Category("Graphical Symbols for Single Line Diagrams"), Description("Get or set the element orientation of the Isolator")]
        [Category("Electrical Symbols"), Description("Get or set the Orientation"), DefaultValueAttribute(SymbolOrientation.Horizontal)]
        public SymbolOrientation Orientation
        {
            get { return this._orientation; }
            set
            {
                this._orientation = value;
                this.Refresh();
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            this.Paint(e);
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            this.ResizeNow(e);
        }




        private void ResizeNow(EventArgs e)
        {
            this.Refresh();
        }

        private new void Paint(PaintEventArgs e)
        {

            System.Drawing.Drawing2D.PathGradientBrush _gBrush = null;
            System.Drawing.Drawing2D.GraphicsPath _gpath = new System.Drawing.Drawing2D.GraphicsPath();

            //定義圖形大小，可任意縮放
            Double defaultsize = (this.Orientation == SymbolOrientation.Horizontal ? (this.Width / 3) : (this.Height / 3));
            float switchsize = (int)Math.Ceiling(defaultsize);


            //有三個圓，大小為圖形的0.1
            float circlesize = switchsize * .3f;

            //共有8條線，每條線有兩個點，起點終點(從左至右)

            //主線有4條
            float L1X1 = 0, L1Y1 = 0, L1X2 = 0, L1Y2 = 0;
            float L2X1 = 0, L2Y1 = 0, L2X1_1 = 0, L2Y1_1 = 0, L2X2 = 0, L2Y2 = 0;
            float L3X1 = 0, L3Y1 = 0, L3X2 = 0, L3Y2 = 0;
            float L4X1 = 0, L4Y1 = 0, L4X2 = 0, L4Y2 = 0;

            //分支有4條
            float L5X1 = 0, L5Y1 = 0, L5X2 = 0, L5Y2 = 0;
            float L6X1 = 0, L6Y1 = 0, L6X2 = 0, L6Y2 = 0;
            float L7X1 = 0, L7Y1 = 0, L7X2 = 0, L7Y2 = 0;
            float L8X1 = 0, L8Y1 = 0, L8X2 = 0, L8Y2 = 0;

            //3個圓的起點跟終點
            float C1X1 = 0, C1Y1 = 0;
            float C2X1 = 0, C2Y1 = 0;
            float C3X1 = 0, C3Y1 = 0;

            //水平的
            if (this.Orientation == SymbolOrientation.Horizontal)
            {
                //定義元件大小
                this.Height = (int)Math.Ceiling((switchsize * 3) + 1);
                this.Width = (int)Math.Ceiling((switchsize * 3) + 1);


                L1X1 = (switchsize * 3) * 0.75f;
                L1Y1 = 0;
                L1X2 = L1X1;
                L1Y2 = switchsize - circlesize;

                L2X1 = (switchsize * 3) * 0.75f;
                L2Y1 = switchsize;
                L2X2 = L1X1;
                L2Y2 = switchsize * 2;
                L2X1_1 = L2X1 - L2Y1 * 0.5f;
                L2Y1_1 = L2Y1 + (L2Y1 * 0.12f);

                L3X1 = L1X1;
                L3Y1 = switchsize * 2 + circlesize;
                L3X2 = L3X1;
                L3Y2 = switchsize * 3;

                L4X1 = switchsize * 0.3f;
                L4Y1 = (switchsize * 3) / 2; ;
                L4X2 = switchsize * 1.25f;
                L4Y2 = L4Y1;

                L5X1 = L4X1;
                L5Y1 = L4Y1 - L4Y1 * 0.3f;
                L5X2 = L5X1;
                L5Y2 = L4Y1 + L4Y1 * 0.3f;

                L6X1 = L4X1 - switchsize * 0.1f;
                L6Y1 = L4Y1 - L4Y1 * 0.2f;
                L6X2 = L6X1;
                L6Y2 = L4Y1 + L4Y1 * 0.2f;

                L7X1 = L4X1 - switchsize * 0.2f;
                L7Y1 = L4Y1 - L4Y1 * 0.1f;
                L7X2 = L7X1;
                L7Y2 = L4Y1 + L4Y1 * 0.1f;

                L8X1 = (L2X1_1 + L2X2) / 2;
                L8Y1 = (L2Y1_1 + L2Y2) / 2;
                L8X2 = L2X2;
                L8Y2 = ((L2X1_1 - L2X2) * (L8X2 - L8X1) / (L2Y1_1 - L2Y2) * -1) + L8Y1;
                double L8Length = Math.Sqrt((Math.Pow(L8X2 - L8X1, 2) + Math.Pow(L8Y2 - L8Y1, 2)));


                C1X1 = L1X1 - (circlesize / 2);
                C1Y1 = L1Y2;

                C2X1 = C1X1;
                C2Y1 = L3Y1 - circlesize;

                C3X1 = L4X2;
                C3Y1 = L4Y1 - (circlesize / 2);


                if (this.State == SymbolState.On)
                {
                    L2X1_1 = L2X1;
                    L2Y1_1 = L2Y1;

                    L8X1 = (L2X1_1 + L2X2) / 2;
                    L8Y1 = (L2Y1_1 + L2Y2) / 2;
                    L8Y2 = L8Y1;
                    L8X2 = (float)(L8X1 + L8Length);
                }
                else if (this.State == SymbolState.Default)
                {
                    L2X1_1 = L4X2 + circlesize;
                    L2Y1_1 = L4Y2;

                    float L8Slope = (-1) * (L2X1_1 - L2X2) / (L2Y1_1 - L2Y2);

                    L8X1 = (L2X1_1 + L2X2) / 2;
                    L8Y1 = (L2Y1_1 + L2Y2) / 2;

                    //取得增量值 ，設L8X2之增量為x L8Y2之增量為 L8的斜率 * x
                    double insertRange = L8Length / Math.Sqrt(Math.Pow(L8Slope, 2) + 1);

                    L8X2 = (float)(L8X1 + insertRange);
                    L8Y2 = (float)(L8Y1 + (insertRange * L8Slope));
                }
            }
            //垂直
            else
            {
                //定義元件大小
                this.Height = (int)Math.Ceiling((switchsize * 3) + 1);
                this.Width = (int)Math.Ceiling((switchsize * 3) + 1);


                L1X1 = 0;
                L1Y1 = (switchsize * 3) * 0.25f;
                L1X2 = switchsize - circlesize;
                L1Y2 = L1Y1;


                L2X1 = switchsize;
                L2Y1 = L1Y1;
                L2X2 = switchsize * 2;
                L2Y2 = L2Y1;
                L2X1_1 = L2X1 + (L2X1 * 0.12f);
                L2Y1_1 = L2Y1 + L2Y1 * 0.5f;

                L3X1 = switchsize * 2 + circlesize;
                L3Y1 = L1Y1;
                L3X2 = switchsize * 3;
                L3Y2 = L3Y1;

                L4X1 = (switchsize * 3) / 2;
                L4Y1 = (switchsize * 2) * 0.8f;
                L4X2 = L4X1;
                L4Y2 = (switchsize * 3) - (switchsize * 0.3f);

                L5X1 = L4X1 - L4X1 * 0.3f;
                L5Y1 = L4Y2;
                L5X2 = L4X1 + L4X1 * 0.3f;
                L5Y2 = L5Y1;

                L6X1 = L4X1 - L4X1 * 0.2f;
                L6Y1 = L4Y2 + (switchsize * 0.1f);
                L6X2 = L4X1 + L4X1 * 0.2f;
                L6Y2 = L6Y1;

                L7X1 = L4X1 - L4X1 * 0.1f;
                L7Y1 = L4Y2 + (switchsize * 0.2f);
                L7X2 = L4X1 + L4X1 * 0.1f;
                L7Y2 = L7Y1;

                L8X1 = (L2X1_1 + L2X2) / 2;
                L8Y1 = (L2Y1_1 + L2Y2) / 2;
                //L8X2 = L2X2;
                //L8Y2 = ((L2X1_1 - L2X2) * (L8X2 - L8X1) / (L2Y1_1 - L2Y2) * -1) + L8Y1
                L8Y2 = L2Y2;
                L8X2 = ((L2Y1_1 - L2Y2) * (L8Y2 - L8Y1) / (L2X1_1 - L2X2) * -1) + L8X1;
                double L8Length = Math.Sqrt((Math.Pow(L8X2 - L8X1, 2) + Math.Pow(L8Y2 - L8Y1, 2)));

                C1X1 = L1X2;
                C1Y1 = L1Y2 - (circlesize / 2);

                C2X1 = L3X1 - (circlesize);
                C2Y1 = C1Y1;

                C3X1 = L4X2 - (circlesize / 2);
                C3Y1 = L4Y1 - (circlesize);

                if (this.State == SymbolState.On)
                {
                    L2X1_1 = L2X1;
                    L2Y1_1 = L2Y1;

                    L8X1 = (L2X1_1 + L2X2) / 2;
                    L8Y1 = (L2Y1_1 + L2Y2) / 2;
                    L8Y2 = (float)(L8Y1 -L8Length) ;
                    L8X2 = L8X1 ;
                }
                else if (this.State == SymbolState.Default)
                {
                    L2X1_1 = L4X1 ;
                    L2Y1_1 = L4Y1 - circlesize;

                    float L8Slope = (-1) * (L2X1_1 - L2X2) / (L2Y1_1 - L2Y2);

                    L8X1 = (L2X1_1 + L2X2) / 2;
                    L8Y1 = (L2Y1_1 + L2Y2) / 2;

                    //取得增量值 ，設L8X2之增量為x L8Y2之增量為 L8的斜率 * x
                    double insertRange = L8Length / Math.Sqrt(Math.Pow(L8Slope, 2) + 1);

                    L8X2 = (float)(L8X1 - insertRange);
                    L8Y2 = (float)(L8Y1 - (insertRange * L8Slope));
                }
            }


            if (this.Fill == false)
            {
                _gpath.AddArc(C1X1, C1Y1, circlesize, circlesize, 0, 360);
                _gpath.AddArc(C2X1, C2Y1, circlesize, circlesize, 0, 360);
                _gpath.AddArc(C3X1, C3Y1, circlesize, circlesize, 0, 360);
                _gpath.CloseFigure();
            }
            else
            {
                _gpath.CloseAllFigures();
                _gBrush = new System.Drawing.Drawing2D.PathGradientBrush(_gpath);

                switch (this.State)
                {
                    case SymbolState.Disconnected:
                        _gBrush.CenterColor = Color.DeepSkyBlue;
                        _gBrush.SurroundColors = new Color[] { Color.DeepSkyBlue };
                        break;
                    case SymbolState.On:
                        _gBrush.CenterColor = Color.Lime;
                        _gBrush.SurroundColors = new Color[] { Color.Lime };
                        break;
                    case SymbolState.Default:
                        _gBrush.CenterColor = Color.Lime;
                        _gBrush.SurroundColors = new Color[] { Color.Lime };
                        break;
                    case SymbolState.Off:
                        _gBrush.CenterColor = Color.Black;
                        _gBrush.SurroundColors = new Color[] { Color.Black };
                        break;
                    case SymbolState.Error:
                        _gBrush.CenterColor = Color.Red;
                        _gBrush.SurroundColors = new Color[] { Color.Red };
                        break;
                    case SymbolState.Level1_Alarm:
                        _gBrush.CenterColor = Color.Gold;
                        _gBrush.SurroundColors = new Color[] { Color.Gold };
                        break;
                    case SymbolState.Level2_Alarm:
                        _gBrush.CenterColor = Color.Orange;
                        _gBrush.SurroundColors = new Color[] { Color.Orange };
                        break;
                }

                _gpath.CloseFigure();
                _gpath.AddArc(C1X1, C1Y1, circlesize, circlesize, 0, 360);
                _gBrush = new System.Drawing.Drawing2D.PathGradientBrush(_gpath);
                _gBrush.WrapMode = WrapMode.TileFlipY;
                e.Graphics.FillPath(_gBrush, _gpath);



                _gpath.CloseFigure();
                _gpath.AddArc(C2X1, C2Y1, circlesize, circlesize, 0, 360);
                _gBrush = new System.Drawing.Drawing2D.PathGradientBrush(_gpath);
                _gBrush.WrapMode = WrapMode.TileFlipY;
                e.Graphics.FillPath(_gBrush, _gpath);


                _gpath.CloseFigure();
                _gpath.AddArc(C3X1, C3Y1, circlesize, circlesize, 0, 360);
                _gBrush = new System.Drawing.Drawing2D.PathGradientBrush(_gpath);
                _gBrush.WrapMode = WrapMode.TileFlipY;
                e.Graphics.FillPath(_gBrush, _gpath);
            }


            _gpath.CloseFigure();
            _gpath.AddLine(L1X1, L1Y1, L1X2, L1Y2);
            _gpath.CloseFigure();
            _gpath.AddLine(L2X1_1, L2Y1_1, L2X2, L2Y2);
            _gpath.CloseFigure();
            _gpath.AddLine(L3X1, L3Y1, L3X2, L3Y2);
            _gpath.CloseFigure();
            _gpath.AddLine(L4X1, L4Y1, L4X2, L4Y2);
            _gpath.CloseFigure();
            _gpath.AddLine(L5X1, L5Y1, L5X2, L5Y2);
            _gpath.CloseFigure();
            _gpath.AddLine(L6X1, L6Y1, L6X2, L6Y2);
            _gpath.CloseFigure();
            _gpath.AddLine(L7X1, L7Y1, L7X2, L7Y2);
            _gpath.CloseFigure();
            _gpath.AddLine(L8X1, L8Y1, L8X2, L8Y2);
            _gpath.CloseFigure();
            //e.Graphics.DrawPath(this.Pen, _gpath);
            using (Pen newpen = new Pen(this.Brush, this.ExtenderWidth))
            {
                e.Graphics.DrawPath(newpen, _gpath);
            }
        }

    }
}
