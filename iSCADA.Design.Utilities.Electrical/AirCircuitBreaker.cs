﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iSCADA.Design.Utilities.Electrical
{
    public partial class AirCircuitBreaker : ElectricalSymbol
    {
        private SymbolOrientation _orientation = SymbolOrientation.Verticle;
        public AirCircuitBreaker()
        {
            InitializeComponent();
        }

        [Category("Graphical Symbols for Single Line Diagrams"), Description("Get or set the element orientation of the Isolator")]
        public SymbolOrientation Orientation
        {
            get { return this._orientation; }
            set
            {
                this._orientation = value;
                this.Refresh();
            }
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            this.Paint(e);
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            ResizeNow(e);
        }

        private void ResizeNow(EventArgs ea)
        {
            this.Refresh();
        }
        private new void Paint(PaintEventArgs e)
        {
            System.Drawing.Drawing2D.PathGradientBrush _gBrush = null;
            System.Drawing.Drawing2D.GraphicsPath _gpath = new System.Drawing.Drawing2D.GraphicsPath();

            Double defaultsize = (this.Orientation == SymbolOrientation.Horizontal ? (this.Width/3) : (this.Height/3));
            float switchsize = (int)Math.Ceiling(defaultsize);
            int circlesize = (int)Math.Ceiling(defaultsize * .3f); // (int)Math.Ceiling(defaultsize);


            float C1X1 = 0, C1Y1 = 0;
            float C2X1 = 0, C2Y1 = 0;
            float L1X1 = 0, L1Y1 = 0;
            float L2X1 = 0, L2Y1 = 0;
            float L3X1 = 0, L3Y1 = 0;
            float L4X1 = 0, L4Y1 = 0, L4X2 = 0, L4Y2 = 0;
            float L5X1 = 0, L5Y1 = 0, L5X2 = 0, L5Y2 = 0;

            if (this.Orientation == SymbolOrientation.Verticle)
            {
                C1X1 = 0;// (float)switchsize + this.ExtenderWidth;
                C1Y1 = (float)switchsize;
                C2X1 = C1X1;
                C2Y1 = (float)(switchsize * 2 - circlesize - this.ExtenderWidth);

                L1X1 = C1X1 + circlesize;
                L1Y1 = C1Y1 + circlesize / 2;
                L2X1 = C2X1 + circlesize;
                L2Y1 = C2Y1 + circlesize / 2;
                L3X1 = L1X1 + (float)(switchsize / 8);
                L3Y1 = L1Y1 + (float)(switchsize / 3);

                L4X1 = 0 + (circlesize / 2);
                L4Y1 = 0;
                L4X2 = L4X1;
                L4Y2 = C1Y1;
                L5X1 = C2X1 + (circlesize / 2);
                L5Y1 = C2Y1 + circlesize;
                L5X2 = L5X1;
                L5Y2 = (float)(switchsize * 3);

                this.Width = (int)Math.Ceiling(L3X1) + this.ExtenderWidth;
                this.Height = (int)Math.Ceiling(switchsize * 3 + 1);
            }
            else
            {
                L4X1 = 0;
                L4Y1 = (float)(switchsize/6)+(circlesize/2);                
                C1X1 =L4X1+(float)switchsize;
                C1Y1 = (float)(switchsize /6);
                C2X1 = (float)(switchsize * 2 - circlesize - this.ExtenderWidth); 
                C2Y1 = C1Y1;

                L1X1 = C1X1 + (circlesize/2);
                L1Y1 = C1Y1 ;
                L2X1 = C2X1 + (circlesize/2);
                L2Y1 = C2Y1 ;
                L3X1 = L1X1 + (float)(switchsize / 3);
                L3Y1 =  L1Y1 - (float)(switchsize / 8);

                //L4X1 = 0 ;
                //L4Y1 = C1Y1+(circlesize/2) + this.ExtenderWidth;// (float)switchsize + (circlesize / 2)+this.ExtenderWidth ;
                L4X2 = C1X1;
                L4Y2 = L4Y1;// C1Y1+(circlesize/2) + this.ExtenderWidth;
                L5X1 = C2X1 + (circlesize);
                L5Y1 = C2Y1 + (circlesize/2);
                L5X2 = (float)(switchsize * 3); 
                L5Y2 = C2Y1 + (circlesize / 2);

                this.Width = (int)Math.Ceiling(switchsize * 3 + 1); 
                this.Height = (int)Math.Ceiling(L4Y1) + (circlesize/2) + this.ExtenderWidth;
            }
            //float C1X1 = (float)defaultsize + this.ExtenderWidth, C1Y1 = (float)defaultsize;
            
            _gpath.AddArc(C1X1, C1Y1, circlesize, circlesize, 0, 360);
            _gpath.AddArc(C2X1, C2Y1, circlesize, circlesize, 0, 360);
            
            _gpath.AddLine(L4X1, L4Y1, L4X2, L4Y2);
            _gpath.CloseFigure();
            _gpath.AddLine(L5X1, L5Y1, L5X2, L5Y2);
            _gpath.CloseFigure();
            Point[] pt = { new Point((int)Math.Ceiling(L1X1), (int)Math.Ceiling(L1Y1)), new Point((int)Math.Ceiling(L3X1), (int)Math.Ceiling(L3Y1)), new Point((int)Math.Ceiling(L2X1), (int)Math.Ceiling(L2Y1)) };
            _gpath.AddCurve(pt);

            using (Pen newpen = new Pen(this.Brush, this.ExtenderWidth))
            {
                e.Graphics.DrawPath(newpen, _gpath);
            }
            //e.Graphics.DrawPath(this.Pen, _gpath);

            //Point[] pt1 = { new Point((int)Math.Ceiling(L1X1), (int)Math.Ceiling(L1Y1)), new Point((int)Math.Ceiling(L3X1), (int)Math.Ceiling(L3Y1)) };
            //_gpath.AddCurve(pt1,.9f);
            //_gpath.CloseFigure();
            //Point[] pt2= {new Point((int)Math.Ceiling(L3X1), (int)Math.Ceiling(L3Y1)), new Point((int)Math.Ceiling(L2X1), (int)Math.Ceiling(L2Y1)) };
            //_gpath.AddCurve(pt2, 2.0f);
            //_gpath.CloseFigure();

            //_gpath.AddEllipse(L1X1, L1Y1, L2X1, L3Y1);
            //_gpath.CloseFigure();
            //_gpath.CloseFigure();
        }
    }
}
