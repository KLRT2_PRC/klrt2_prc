﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iSCADA.Design.Utilities.Electrical
{
    public partial class Switch03UC : ElectricalSymbol
    {
        private SymbolOrientation _orientation = SymbolOrientation.Horizontal;
        public Switch03UC()
        {
            InitializeComponent();
        }

        //[Category("Graphical Symbols for Single Line Diagrams"), Description("Get or set the element orientation of the Isolator")]
        [Category("Electrical Symbols"), Description("Get or set the Orientation"), DefaultValueAttribute(SymbolOrientation.Horizontal)]
        public SymbolOrientation Orientation
        {
            get { return this._orientation; }
            set
            {
                this._orientation = value;
                this.Refresh();
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            this.Paint(e);
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            ResizeNow(e);
        }

        private void ResizeNow(EventArgs ea)
        {
            this.Refresh();
        }

        private new void Paint(PaintEventArgs e)
        {
            System.Drawing.Drawing2D.PathGradientBrush _gBrush = null;
            System.Drawing.Drawing2D.GraphicsPath _gpath = new System.Drawing.Drawing2D.GraphicsPath();

            //定義圖形大小，可任意縮放
            Double defaultsize = (this.Orientation == SymbolOrientation.Horizontal ? (this.Width / 4.5) : (this.Height / 4.5));
            float switchsize = (int)Math.Ceiling(defaultsize);
            ///////////////////////////////////////////////////////
            float L1X1 = 0, L1Y1 = 0, L1X2 = 0, L1Y2 = 0;
            float L2X1 = 0, L2Y1 = 0, L2X2 = 0, L2Y2 = 0;
            float L3X1 = 0, L3Y1 = 0, L3X2 = 0, L3Y2 = 0;
            float L4X1 = 0, L4Y1 = 0, L4X2 = 0, L4Y2 = 0;
            float L5X1 = 0, L5Y1 = 0, L5X2 = 0, L5Y2 = 0;
            float L6X1 = 0, L6Y1 = 0, L6X2 = 0, L6Y2 = 0;
            float L7X1 = 0, L7Y1 = 0, L7X2 = 0, L7Y2 = 0;
            float L8X1 = 0, L8Y1 = 0, L8X2 = 0, L8Y2 = 0;
            float L9X1 = 0, L9Y1 = 0, L9X2 = 0, L9Y2 = 0;
            float L10X1 = 0, L10Y1 = 0, L10X2 = 0, L10Y2 = 0;
            float L11X1 = 0, L11Y1 = 0, L11X2 = 0, L11Y2 = 0;
            float L12X1 = 0, L12Y1 = 0, L12X2 = 0, L12Y2 = 0;
            float L13X1 = 0, L13Y1 = 0, L13X2 = 0, L13Y2 = 0;
            float L14X1 = 0, L14Y1 = 0, L14X2 = 0, L14Y2 = 0;
            float L15X1 = 0, L15Y1 = 0, L15X2 = 0, L15Y2 = 0;
            float L16X1 = 0, L16Y1 = 0, L16X2 = 0, L16Y2 = 0;
            ///////////////////////////////////////////////////////
            if (this.Orientation == SymbolOrientation.Horizontal)
            {
                L1X1 = 0;
                L1Y1 = switchsize;
                L1X2 =L1X1+ switchsize;
                L1Y2 = L1Y1;
                L2X1 = L1X2;
                L2Y1 = L1Y2;
                L2X2 = L2X1 + (switchsize / 5);
                L2Y2 = L2Y1 - (switchsize / 5);
                L3X1 = L1X2;
                L3Y1 = L1Y2;
                L3X2 = L2X2;
                L3Y2 = L2Y1 + (switchsize / 5);
                L6X1 = L1X2+(switchsize/5);
                L6Y1 = L1Y2;
                L6X2 = L6X1 + switchsize/2;
                L6Y2 = L6Y1;
                L4X1 = L6X1;
                L4Y1 = L6Y1;
                L4X2 = L4X1+(switchsize / 5);
                L4Y2 = L4Y1 - (switchsize / 5);
                L5X1 = L6X1;
                L5Y1 = L6Y1;
                L5X2 = L5X1 + (switchsize / 5);
                L5Y2 = L5Y1 + (switchsize / 5);

                L7X1 = L6X2 - (switchsize / 5);
                L7Y1 = L6Y2 - (switchsize / 5);
                L7X2 = L6X2 + (switchsize / 5);
                L7Y2 = L6Y2 + (switchsize / 5);

                L8X1 = L6X2 + (switchsize / 5);
                L8Y1 = L6Y2 - (switchsize / 5);
                L8X2 = L6X2 - (switchsize / 5);
                L8Y2 = L6Y2 + (switchsize / 5);

                if (this.State == SymbolState.On)
                {
                    L9X1 = L6X2;
                    L9Y1 = L6Y1;
                }
                else
                {
                    L9X1 = L6X2 + (switchsize/4);
                    L9Y1 = L6Y2 - (int)Math.Ceiling(switchsize * 0.8);
                }

                L9X2 = L6X2 + switchsize;
                L9Y2 = L6Y2 ;
                                                                                                    
                L10X1 = L9X2;
                L10Y1 = L9Y2;
                L10X2 = L10X1 + (switchsize / 3);
                L10Y2 = L10Y1;

                L11X1 = L10X2;
                L11Y1 = L10Y2;
                L11X2 = L10X2-(switchsize/5);
                L11Y2 = L10Y2 - (switchsize / 5);

                L12X1 = L10X2;
                L12Y1 = L10Y2;
                L12X2 = L10X2 - (switchsize / 5);
                L12Y2 = L10Y2 + (switchsize / 5);

                L15X1 = L10X2 + (switchsize / 5);
                L15Y1 = L10Y2;
                L15X2 = L15X1 + switchsize;
                L15Y2 = L15Y1;

                L13X1 = L15X1;
                L13Y1 = L15Y1;
                L13X2 = L13X1 - (switchsize / 5);
                L13Y2 = L13Y1 - (switchsize / 5);

                L14X1 = L15X1;
                L14Y1 = L15Y1;
                L14X2 = L14X1 - (switchsize / 5);
                L14Y2 = L14Y1 + (switchsize / 5);

                L16X1 = L15X2;
                L16Y1 = L15Y2 - (switchsize / 5);
                L16X2 = L15X2;
                L16Y2 = L15Y2 + (switchsize / 5);

                this.Width = (int)Math.Ceiling(switchsize *4.5);
                this.Height = (int)Math.Ceiling(switchsize) + (int)Math.Ceiling(switchsize/5)+this.ExtenderWidth;
            }
            else
            {
                L1X1 = switchsize/5;
                L1Y1 = 0;
                L1X2 = L1X1 ;
                L1Y2 = L1Y1 + switchsize;
                L2X1 = L1X2;
                L2Y1 = L1Y2;
                L2X2 = L2X1 + (switchsize / 5);
                L2Y2 = L2Y1 + (switchsize / 5);
                L3X1 = L1X2;
                L3Y1 = L1Y2;
                L3X2 = L3X1- (switchsize/5);
                L3Y2 = L2Y1 + (switchsize / 5);
                L6X1 = L1X2 ;
                L6Y1 = L1Y2 + (switchsize / 5);
                L6X2 = L6X1 ;
                L6Y2 = L6Y1 + switchsize / 2;
                L4X1 = L6X1;
                L4Y1 = L6Y1;
                L4X2 = L4X1 + (switchsize / 5);
                L4Y2 = L4Y1 + (switchsize / 5);
                L5X1 = L6X1;
                L5Y1 = L6Y1;
                L5X2 = L5X1 - (switchsize / 5);
                L5Y2 = L5Y1 + (switchsize / 5);

                L7X1 = L6X2 + (switchsize / 5);
                L7Y1 = L6Y2 - (switchsize / 5);
                L7X2 = L6X2 - (switchsize / 5);
                L7Y2 = L6Y2 + (switchsize / 5);

                L8X1 = L6X2 - (switchsize / 5);
                L8Y1 = L6Y2 - (switchsize / 5);
                L8X2 = L6X2 + (switchsize / 5);
                L8Y2 = L6Y2 + (switchsize / 5);

                if (this.State == SymbolState.On)
                {
                    L9X1 = L6X2;
                    L9Y1 = L6Y2;
                }
                else
                {
                    L9X1 = L6X2 + (int)Math.Ceiling(switchsize * 0.8) ;
                    L9Y1 = L6Y2 + (switchsize / 4);
                }

                L9X2 = L6X2 ;
                L9Y2 = L6Y2 + switchsize;

                L10X1 = L9X2;
                L10Y1 = L9Y2;
                L10X2 = L10X1 ;
                L10Y2 = L10Y1 + (switchsize / 3);

                L11X1 = L10X2;
                L11Y1 = L10Y2;
                L11X2 = L10X2 + (switchsize / 5);
                L11Y2 = L10Y2 - (switchsize / 5);

                L12X1 = L10X2;
                L12Y1 = L10Y2;
                L12X2 = L10X2 - (switchsize / 5);
                L12Y2 = L10Y2 - (switchsize / 5);

                L15X1 = L10X2 ;
                L15Y1 = L10Y2 + (switchsize / 5);
                L15X2 = L15X1 ;
                L15Y2 = L15Y1 + switchsize;

                L13X1 = L15X1;
                L13Y1 = L15Y1;
                L13X2 = L13X1 + (switchsize / 5);
                L13Y2 = L13Y1 - (switchsize / 5);

                L14X1 = L15X1;
                L14Y1 = L15Y1;
                L14X2 = L14X1 - (switchsize / 5);
                L14Y2 = L14Y1 - (switchsize / 5);

                L16X1 = L15X2 - (switchsize / 5);
                L16Y1 = L15Y2 ;
                L16X2 = L15X2 + (switchsize / 5);
                L16Y2 = L15Y2 ;

                this.Width = (int)Math.Ceiling(switchsize) + (int)Math.Ceiling(switchsize / 5) + this.ExtenderWidth;
                this.Height = (int)Math.Ceiling(switchsize * 4.5);
            }
            _gpath.AddLine(L1X1, L1Y1, L1X2, L1Y2);
            _gpath.CloseFigure();
            _gpath.AddLine(L2X1, L2Y1, L2X2, L2Y2);
            _gpath.AddLine(L3X1, L3Y1, L3X2, L3Y2);
            _gpath.CloseFigure();
            _gpath.AddLine(L6X1, L6Y1, L6X2, L6Y2);
            _gpath.CloseFigure();
            _gpath.AddLine(L4X1, L4Y1, L4X2, L4Y2);
            _gpath.AddLine(L5X1, L5Y1, L5X2, L5Y2);
            _gpath.CloseFigure();
            _gpath.AddLine(L7X1, L7Y1, L7X2, L7Y2);
            _gpath.CloseFigure();
            _gpath.AddLine(L8X1, L8Y1, L8X2, L8Y2);
            _gpath.CloseFigure();
            _gpath.AddLine(L9X1, L9Y1, L9X2, L9Y2);
            _gpath.CloseFigure();
            _gpath.AddLine(L10X1, L10Y1, L10X2, L10Y2);
            _gpath.CloseFigure();
            _gpath.AddLine(L11X1, L11Y1, L11X2, L11Y2);
            _gpath.CloseFigure();
            _gpath.AddLine(L12X1, L12Y1, L12X2, L12Y2);
            _gpath.CloseFigure();
            _gpath.AddLine(L15X1, L15Y1, L15X2, L15Y2);
            _gpath.CloseFigure();
            _gpath.AddLine(L13X1, L13Y1, L13X2, L13Y2);
            _gpath.CloseFigure();
            _gpath.AddLine(L14X1, L14Y1, L14X2, L14Y2);
            _gpath.CloseFigure();
            _gpath.AddLine(L16X1, L16Y1, L16X2, L16Y2);

            //e.Graphics.DrawPath(this.Pen, _gpath);
            using (Pen newpen = new Pen(this.Brush, this.ExtenderWidth))
            {
                e.Graphics.DrawPath(newpen, _gpath);
            }
        }
    }
}
