﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iSCADA.Design.Utilities.Electrical
{
    public class Switch08UC : ElectricalSymbol
    {

        private SymbolOrientation _orientation = SymbolOrientation.Horizontal;


        //[Category("Graphical Symbols for Single Line Diagrams"), Description("Get or set the element orientation of the Isolator")]
        [Category("Electrical Symbols"), Description("Get or set the Orientation"), DefaultValueAttribute(SymbolOrientation.Horizontal)]
        public SymbolOrientation Orientation
        {
            get { return this._orientation; }
            set
            {
                this._orientation = value;
                this.Refresh();
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            this.Paint(e);
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            this.ResizeNow(e);
        }

        private void ResizeNow(EventArgs e)
        {
            this.Refresh();
        }


        private new void Paint(PaintEventArgs e)
        {

            System.Drawing.Drawing2D.PathGradientBrush _gBrush = null;
            System.Drawing.Drawing2D.GraphicsPath _gpath = new System.Drawing.Drawing2D.GraphicsPath();

            //定義圖形大小，可任意縮放
            Double defaultsize = (this.Orientation == SymbolOrientation.Horizontal ? (this.Width / 3) : (this.Height / 3));
            float switchsize = (int)Math.Ceiling(defaultsize);


            //共有10條線，每條線有兩個點，起點終點(從左至右)

            //主線有2條
            float L1X1 = 0, L1Y1 = 0, L1X2 = 0, L1Y2 = 0;
            float L2X1 = 0, L2Y1 = 0, L2X2 = 0, L2Y2 = 0;

            //中間長方形
            float L3X1 = 0, L3Y1 = 0, L3X2 = 0, L3Y2 = 0;
            float L4X1 = 0, L4Y1 = 0, L4X2 = 0, L4Y2 = 0;
            float L5X1 = 0, L5Y1 = 0, L5X2 = 0, L5Y2 = 0;
            float L6X1 = 0, L6Y1 = 0, L6X2 = 0, L6Y2 = 0;

            //長方形內之直線
            float L7X1 = 0, L7Y1 = 0, L7X2 = 0, L7Y2 = 0;

            //長方形內之三角形
            float L8X1 = 0, L8Y1 = 0, L8X2 = 0, L8Y2 = 0;
            float L9X1 = 0, L9Y1 = 0, L9X2 = 0, L9Y2 = 0;
            float L10X1 = 0, L10Y1 = 0, L10X2 = 0, L10Y2 = 0;

            //水平的
            if (this.Orientation == SymbolOrientation.Horizontal)
            {
                //定義元件大小
                this.Height = (int)Math.Ceiling(switchsize * 1.1);
                this.Width = (int)Math.Ceiling((switchsize * 3) + 1);

                L1X1 = 0;
                L1Y1 = this.Height / 2;
                L1X2 = switchsize * 1.25f;
                L1Y2 = L1Y1;

                L2X1 = switchsize * 1.75f;
                L2Y1 = L1Y1;
                L2X2 = switchsize * 3;
                L2Y2 = L1Y1;

                L3X1 = switchsize;
                L3Y1 = L1Y1 + L1Y1 * 0.75f;
                L3X2 = L3X1;
                L3Y2 = L1Y1 - L1Y1 * 0.75f;

                L4X1 = switchsize;
                L4Y1 = L1Y1 + L1Y1 * 0.75f;
                L4X2 = switchsize * 2;
                L4Y2 = L4Y1;

                L5X1 = switchsize;
                L5Y1 = L1Y1 - L1Y1 * 0.75f;
                L5X2 = switchsize * 2;
                L5Y2 = L5Y1;

                L6X1 = switchsize * 2f;
                L6Y1 = L1Y1 + L1Y1 * 0.75f;
                L6X2 = L6X1;
                L6Y2 = L1Y1 - L1Y1 * 0.75f;

                L7X1 = switchsize * 1.25f;
                L7Y1 = L1Y1 + L1Y1 * 0.4f;
                L7X2 = L7X1;
                L7Y2 = L1Y1 - L1Y1 * 0.4f;

                L8X1 = switchsize * 1.25f;
                L8Y1 = L1Y1;
                L8X2 = switchsize * 1.75f;
                L8Y2 = L1Y1 - L1Y1 * 0.4f;

                L9X1 = switchsize * 1.25f;
                L9Y1 = L1Y1;
                L9X2 = switchsize * 1.75f;
                L9Y2 = L1Y1 + L1Y1 * 0.4f;

                L10X1 = switchsize * 1.75f;
                L10Y1 = L1Y1 + L1Y1 * 0.4f;
                L10X2 = L10X1;
                L10Y2 = L1Y1 - L1Y1 * 0.4f;
            }
            else
            {
                //定義元件大小
                this.Width = (int)Math.Ceiling(switchsize * 1.1);
                this.Height = (int)Math.Ceiling((switchsize * 3) + 1);

                L1X1 = this.Width / 2;
                L1Y1 = 0; ;
                L1X2 = L1X1;
                L1Y2 = switchsize * 1.25f;

                L2X1 = L1X1;
                L2Y1 = switchsize * 1.75f;
                L2X2 = L1X1;
                L2Y2 = switchsize * 3;



                L3X1 = L1X1 + L1X1 * 0.75f;
                L3Y1 = switchsize;
                L3X2 = L1X1 - L1X1 * 0.75f;
                L3Y2 = L3Y1;

                L4X1 = L1X1 + L1X1 * 0.75f;
                L4Y1 = switchsize;
                L4X2 = L4X1;
                L4Y2 = switchsize * 2;

                L5X1 = L1X1 - L1X1 * 0.75f;
                L5Y1 = switchsize;
                L5X2 = L5X1;
                L5Y2 = switchsize * 2;

                L6X1 = L1X1 + L1X1 * 0.75f;
                L6Y1 = switchsize * 2;
                L6X2 = L1X1 - L1X1 * 0.75f;
                L6Y2 = L6Y1;

                L7X1 = L1X1 + L1X1 * 0.4f; //;
                L7Y1 = switchsize * 1.25f;
                L7X2 = L1X1 - L1X1 * 0.4f;
                L7Y2 = L7Y1;

                L8X1 = L1X1 + L1X1 * 0.4f;
                L8Y1 = L1Y2;
                L8X2 = L2X1;
                L8Y2 = L2Y1;

                L9X1 = L1X1 - L1X1 * 0.4f;
                L9Y1 = L1Y2;
                L9X2 = L2X1;
                L9Y2 = L2Y1;

                L10X1 = L1X1 + L1X1 * 0.4f;
                L10Y1 = switchsize * 1.75f;
                L10X2 = L1X1 - L1X1 * 0.4f;
                L10Y2 = L10Y1;
            }


            if (this.Fill == false)
            {

            }
            else
            {
                _gpath.CloseAllFigures();
                _gBrush = new System.Drawing.Drawing2D.PathGradientBrush(_gpath);

                switch (this.State)
                {
                    case SymbolState.Disconnected:
                        _gBrush.CenterColor = Color.DeepSkyBlue;
                        _gBrush.SurroundColors = new Color[] { Color.DeepSkyBlue };
                        break;
                    case SymbolState.On:
                        _gBrush.CenterColor = Color.Lime;
                        _gBrush.SurroundColors = new Color[] { Color.Lime };
                        break;
                    case SymbolState.Off:
                        _gBrush.CenterColor = Color.Black;
                        _gBrush.SurroundColors = new Color[] { Color.Black };
                        break;
                    case SymbolState.Error:
                        _gBrush.CenterColor = Color.Red;
                        _gBrush.SurroundColors = new Color[] { Color.Red };
                        break;
                    case SymbolState.Level1_Alarm:
                        _gBrush.CenterColor = Color.Gold;
                        _gBrush.SurroundColors = new Color[] { Color.Gold };
                        break;
                    case SymbolState.Level2_Alarm:
                        _gBrush.CenterColor = Color.Orange;
                        _gBrush.SurroundColors = new Color[] { Color.Orange };
                        break;
                }


            }

            _gpath.CloseFigure();
            _gpath.AddLine(L1X1, L1Y1, L1X2, L1Y2);
            _gpath.CloseFigure();
            _gpath.AddLine(L2X1, L2Y1, L2X2, L2Y2);
            _gpath.CloseFigure();
            _gpath.AddLine(L3X1, L3Y1, L3X2, L3Y2);
            _gpath.CloseFigure();
            _gpath.AddLine(L4X1, L4Y1, L4X2, L4Y2);
            _gpath.CloseFigure();
            _gpath.AddLine(L5X1, L5Y1, L5X2, L5Y2);
            _gpath.CloseFigure();
            _gpath.AddLine(L6X1, L6Y1, L6X2, L6Y2);
            _gpath.CloseFigure();
            _gpath.AddLine(L7X1, L7Y1, L7X2, L7Y2);
            _gpath.CloseFigure();
            _gpath.AddLine(L8X1, L8Y1, L8X2, L8Y2);
            _gpath.CloseFigure();
            _gpath.AddLine(L9X1, L9Y1, L9X2, L9Y2);
            _gpath.CloseFigure();
            _gpath.AddLine(L10X1, L10Y1, L10X2, L10Y2);
            _gpath.CloseFigure();
            //e.Graphics.DrawPath(this.Pen, _gpath);
            using (Pen newpen = new Pen(this.Brush, this.ExtenderWidth))
            {
                e.Graphics.DrawPath(newpen, _gpath);
            }
        }



    }
}
