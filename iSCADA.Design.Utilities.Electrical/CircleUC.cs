﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iSCADA.Design.Utilities.Electrical
{
    public partial class CircleUC : ElectricalSymbol
    {
        public CircleUC()
        {
            InitializeComponent();
        }

        protected override void OnPaint(System.Windows.Forms.PaintEventArgs e)
        {
            base.OnPaint(e);
            this.Paint(e);
        }
        private new void Paint(System.Windows.Forms.PaintEventArgs e)
        {
            Rectangle rec = this.DisplayRectangle;
            Rectangle newrec = new Rectangle(rec.Left, rec.Top, rec.Width - 1, rec.Height - 1);
            //e.Graphics.DrawRectangle(this.Pen, newrec);
            if (this.Fill == true)
            {
                //e.Graphics.FillEllipse(this.Brush, newrec);
                using (Pen newpen = new Pen(this.Brush, this.ExtenderWidth))
                {
                    e.Graphics.FillEllipse(this.Brush, newrec);
                }
                //e.Graphics.FillRectangle(this.Brush, newrec);
                //e.Graphics.FillEllipse(this.Brush, rec.Left, rec.Top, rec.Width - 1, rec.Height - 1);
            }
            else
            {
                using (Pen newpen = new Pen(this.Brush, this.ExtenderWidth))
                {
                    e.Graphics.DrawEllipse(newpen, newrec);
                    //e.Graphics.DrawEllipse(this.Pen, newrec);
                }
                //e.Graphics.DrawEllipse(this.Pen, newrec);
            }
        }
        //protected override void ToolStripItem_Click(object sender, EventArgs e)
        //{
        //    base.ToolStripItem_Click(sender, e);
        //    MessageBox.Show(this.Name + " " + sender.ToString());
        //}
    }
}
