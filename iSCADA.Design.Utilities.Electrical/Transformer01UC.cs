﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iSCADA.Design.Utilities.Electrical
{
    public partial class Transformer01UC : ElectricalSymbol
    {
        private SymbolOrientation _orientation = SymbolOrientation.Horizontal;
        public Transformer01UC()
        {
            InitializeComponent();
        }

        [Category("Electrical Symbols"), Description("Get or set the Orientation"), DefaultValueAttribute(SymbolOrientation.Horizontal)]
        public SymbolOrientation Orientation
        {
            get { return this._orientation; }
            set
            {
                this._orientation = value;
                this.Refresh();
            }
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            this.Paint(e);
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            ResizeNow(e);
        }

        private void ResizeNow(EventArgs ea)
        {
            this.Refresh();
        }

        private new void Paint(PaintEventArgs e)
        {
            System.Drawing.Drawing2D.PathGradientBrush _gBrush = null;
            System.Drawing.Drawing2D.GraphicsPath _gpath = new System.Drawing.Drawing2D.GraphicsPath();

            //定義圖形大小，可任意縮放
            Double defaultsize = (this.Orientation == SymbolOrientation.Horizontal ? (this.Width/3)  : (this.Height/3 ));
            float switchsize = (int)Math.Ceiling(defaultsize);
            float circlesize = switchsize;// * .1f;
            //////////////////////////////////////////////////////
            //三個圓
            float C1X1 = 0, C1Y1 = 0;
            float C2X1 = 0, C2Y1 = 0;
            float C3X1 = 0, C3Y1 = 0;
            //兩條線
            float L1X1 = 0, L1Y1 = 0, L1X2 = 0, L1Y2 = 0;
            float L2X1 = 0, L2Y1 = 0, L2X2 = 0, L2Y2 = 0;
            //////////////////////////////////////////////////////
            if (this.Orientation == SymbolOrientation.Horizontal)
            {
                L1X1 = 0;
                L1Y1 = circlesize;
                L1X2 = switchsize;
                L1Y2 = circlesize;
                C1X1 = L1X2;
                C1Y1 = L1Y2 - (circlesize / 2);
                C2X1 = C1X1 + (circlesize / 2);
                C2Y1 = C1Y1 - (circlesize / 2);
                C3X1 = C2X1;
                C3Y1 = C1Y1 + (circlesize / 2);
                L2X1 = L1X2 + circlesize;
                L2Y1 = L1Y2;
                L2X2 = L2X1 + switchsize;
                L2Y2 = L2Y1;
                this.Width =(int)Math.Ceiling(switchsize*2+circlesize+1);
                this.Height = (int)Math.Ceiling(switchsize*2+1);
            }
            else
            {
                C1X1 = 0;
                //C1Y1 = 0;
                L1X1 = C1X1 + circlesize;
                L1Y1 = 0;
                L1X2 = L1X1;
                L1Y2 = L1Y1 + switchsize;

                C2X1 = C1X1 + (circlesize / 2);
                C2Y1 = L1Y2;
                C1Y1 = C2Y1 + (circlesize / 2);
                C3X1 = C1X1 + (circlesize);
                C3Y1 = C1Y1;

                L2X1 = L1X2;
                L2Y1 = L1X2 + circlesize;
                L2X2 = L2X1;
                L2Y2 = L2Y1 + switchsize;
                this.Width = (int)Math.Ceiling(switchsize * 2 + 1);
                this.Height = (int)Math.Ceiling(switchsize * 2 + circlesize + 1);
            }
            //////////////////////////////////////////////////////
            _gpath.AddArc(C1X1, C1Y1, circlesize, circlesize, 0, 360);
            _gpath.AddArc(C2X1, C2Y1, circlesize, circlesize, 0, 360);
            _gpath.AddArc(C3X1, C3Y1, circlesize, circlesize, 0, 360);
            _gpath.CloseFigure();
            _gpath.AddLine(L1X1, L1Y1, L1X2, L1Y2);
            _gpath.CloseFigure();
            _gpath.AddLine(L2X1, L2Y1, L2X2, L2Y2);
            _gpath.CloseFigure();
            //////////////////////////////////////////////////////            
            //e.Graphics.DrawPath(this.Pen, _gpath);
            using (Pen newpen = new Pen(this.Brush, this.ExtenderWidth))
            {
                e.Graphics.DrawPath(newpen, _gpath);
            }
        }
    }
}
