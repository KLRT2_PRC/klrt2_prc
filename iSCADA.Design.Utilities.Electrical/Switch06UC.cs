﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iSCADA.Design.Utilities.Electrical
{
    public class Switch06UC : ElectricalSymbol
    {
        private SymbolOrientation _orientation = SymbolOrientation.Horizontal;


        //[Category("Graphical Symbols for Single Line Diagrams"), Description("Get or set the element orientation of the Isolator")]
        [Category("Electrical Symbols"), Description("Get or set the Orientation"), DefaultValueAttribute(SymbolOrientation.Horizontal)]
        public SymbolOrientation Orientation
        {
            get { return this._orientation; }
            set
            {
                this._orientation = value;
                this.Refresh();
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            this.Paint(e);
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            this.ResizeNow(e);
        }


        private void ResizeNow(EventArgs e)
        {
            this.Refresh();
        }

        private new void Paint(PaintEventArgs e)
        {
            System.Drawing.Drawing2D.PathGradientBrush _gBrush = null;
            System.Drawing.Drawing2D.GraphicsPath _gpath = new System.Drawing.Drawing2D.GraphicsPath();

            //定義圖形大小，可任意縮放
            Double defaultsize = (this.Orientation == SymbolOrientation.Horizontal ? (this.Width / 3) : (this.Height / 3));
            float switchsize = (int)Math.Ceiling(defaultsize);
            //有兩個圓，大小為圖形的0.1
            float circlesize = switchsize * .3f;

            //共有7條線，每條線有兩個點，起點終點(從左至右)

            //主線有3條
            float L1X1 = 0, L1Y1 = 0, L1X2 = 0, L1Y2 = 0;
            float L2X1 = 0, L2Y1 = 0, L2X1_1 = 0, L2Y1_1 = 0, L2X2 = 0, L2Y2 = 0;
            float L3X1 = 0, L3Y1 = 0, L3X2 = 0, L3Y2 = 0;

            //分支有4條
            float L4X1 = 0, L4Y1 = 0, L4X2 = 0, L4Y2 = 0;
            float L5X1 = 0, L5Y1 = 0, L5X2 = 0, L5Y2 = 0;
            float L6X1 = 0, L6Y1 = 0, L6X2 = 0, L6Y2 = 0;
            float L7X1 = 0, L7Y1 = 0, L7X2 = 0, L7Y2 = 0;

            //2個圓的起點跟終點
            float C1X1 = 0, C1Y1 = 0;
            float C2X1 = 0, C2Y1 = 0;



            //水平的
            if (this.Orientation == SymbolOrientation.Horizontal)
            {
                //定義元件大小
                this.Height = (int)Math.Ceiling(switchsize * 1.1);
                this.Width = (int)Math.Ceiling((switchsize * 3) + 1);

                L1X1 = switchsize * 0.3f;
                L1Y1 = switchsize * 0.8f;
                L1X2 = switchsize * 1;
                L1Y2 = L1Y1;

                L2X1 = switchsize;
                L2Y1 = L1Y1;
                L2X2 = switchsize * 2;
                L2Y2 = L1Y1;
                L2X1_1 = (float)(L2X1 + (L2X2 * 0.12));
                L2Y1_1 = (float)(L2Y1 - (L2Y1 * 0.5));



                L3X1 = switchsize * 2 + circlesize;
                L3Y1 = L1Y1;
                L3X2 = switchsize * 4;
                L3Y2 = L1Y1;

                L4X1 = switchsize * 0.1f;
                L4Y1 = L1Y1 - L1Y1 * 0.1f;
                L4X2 = L4X1;
                L4Y2 = L1Y1 + L1Y1 * 0.1f;

                L5X1 = switchsize * 0.2f;
                L5Y1 = L1Y1 - L1Y1 * 0.2f;
                L5X2 = L5X1;
                L5Y2 = L1Y1 + L1Y1 * 0.2f;

                L6X1 = switchsize * 0.3f; ;
                L6Y1 = L1Y1 - L1Y1 * 0.3f;
                L6X2 = L6X1;
                L6Y2 = L1Y1 + L1Y1 * 0.3f;

                L7X1 = (L2X1_1 + L2X2) / 2;
                L7Y1 = (L2Y1_1 + L2Y2) / 2;
                L7X2 = L7X1;
                L7Y2 = L1Y1;

                C1X1 = L1X2;
                C1Y1 = L1Y1 - (circlesize / 2);

                C2X1 = L3X1 - circlesize;
                C2Y1 = L1Y1 - (circlesize / 2);

                //三角形斜邊的長度
                double hypotenuse = Math.Sqrt(Math.Pow(L2X2 - L2X1_1, 2) + Math.Pow(L2Y2 - L2Y1_1, 2)) / 2;
                //L7線的長度
                float L7Height = L7Y2 - L7Y1;
                //三角形第三邊的長度，與L7垂直
                double triangleThirdSide = Math.Sqrt(Math.Pow(hypotenuse, 2) - Math.Pow(L7Height, 2));


                // ON
                if (this.State == SymbolState.On)
                {


                    L2X1_1 = L2X1 + circlesize;
                    L2Y1_1 = L2Y1;

                    L7X1 = (L2X1_1 + L2X2) / 2;
                    L7Y1 = L2Y1;
                    L7X2 = (float)((Math.Pow(L7Height, 2) - Math.Pow(triangleThirdSide, 2) - Math.Pow(L7X1, 2) + Math.Pow(L2X2, 2)) / (-2 * (L7X1 - L2X2)));
                    L7Y2 = L7Y1 + (float)Math.Sqrt(Math.Pow(L7Height, 2) - Math.Pow(L7X2 - L7X1, 2));
                }



        



            }
            else
            {
                float rightGap = (switchsize * 0.8f);
                L1X1 = rightGap;
                L1Y1 = 0;
                L1X2 = L1X1;
                L1Y2 = switchsize - circlesize;

                L2X1 = L1X1;
                L2Y1 = switchsize;
                L2X2 = L1X1 - switchsize * 0.8f * 0.5f;
                L2Y2 = switchsize * 2 - (L2Y1 * 0.12f);
                L2X1_1 = L2X1 ;
                L2Y1_1 = L2Y1;

                L3X1 = L1X1;
                L3Y1 = switchsize * 2 + circlesize;
                L3X2 = L1X1;
                L3Y2 = switchsize * 3 - switchsize * 0.3f;

                L4X1 = L1X1 - rightGap * 0.1f;
                L4Y1 = L3Y2 + switchsize * 0.2f;
                L4X2 = L1X1 + rightGap * 0.1f;
                L4Y2 = L3Y2 + switchsize * 0.2f;

                L5X1 = L1X1 - rightGap * 0.3f;
                L5Y1 = L3Y2 + switchsize * 0.2f;
                L5X2 = L1X1 + rightGap * 0.3f;
                L5Y2 = L3Y2 + switchsize * 0.2f;
              
                L6X1 = L1X1 - rightGap * 0.5f;
                L6Y1 = L3Y2 ;
                L6X2 = L1X1 + rightGap * 0.5f;
                L6Y2 = L3Y2;


                L7X1 = (L2X1_1 + L2X2) / 2;
                L7Y1 = (L2Y1_1 + L2Y2) / 2;
                L7X2 = L1X1;
                L7Y2 = L7Y1;

                C1X1 = L1X1 - (circlesize / 2);
                C1Y1 = L1Y2;

                C2X1 = C1X1;
                C2Y1 = L3Y1 - circlesize;


                //三角形斜邊的長度
                double hypotenuse = Math.Sqrt(Math.Pow(L2X2 - L2X1, 2) + Math.Pow(L2Y2 - L2Y1, 2)) / 2;
                //L7線的長度
                float L7Width = L7X2 -  L7X1 ;
                //三角形第三邊的長度，與L7垂直
                double triangleThirdSide = Math.Sqrt(Math.Pow(hypotenuse, 2) - Math.Pow(L7Width, 2));

                // ON
                if (this.State == SymbolState.On)
                {


                    L2X2 = L2X1 ;
                    L2Y2 = L2Y1*2;

                    L7X1 = L2X1;
                    L7Y1 = (L2Y1 + L2Y2) / 2;
                    L7Y2 =  (float)((Math.Pow(L7Width, 2) - Math.Pow(triangleThirdSide, 2) - Math.Pow(L7Y1, 2) + Math.Pow(L2Y1, 2)) / (-2 * (L7Y1 - L2Y1)));
                    L7X2 = L2X1  + (float) Math.Sqrt(Math.Abs((Math.Pow(L7Width, 2) - Math.Pow((L7Y1 - L7Y2), 2))));
                }


                this.Width = (int)Math.Ceiling(switchsize * 1.2);
                this.Height = (int)Math.Ceiling((switchsize * 3) + 1);
            }

            if (this.Fill == false)
            {
                _gpath.AddArc(C1X1, C1Y1, circlesize, circlesize, 0, 360);
                _gpath.AddArc(C2X1, C2Y1, circlesize, circlesize, 0, 360);
                _gpath.CloseFigure();
            }
            else
            {
                _gpath.CloseAllFigures();
                _gBrush = new System.Drawing.Drawing2D.PathGradientBrush(_gpath);

                switch (this.State)
                {
                    case SymbolState.Disconnected:
                        _gBrush.CenterColor = Color.DeepSkyBlue;
                        _gBrush.SurroundColors = new Color[] { Color.DeepSkyBlue };
                        break;
                    case SymbolState.On:
                        _gBrush.CenterColor = Color.Lime;
                        _gBrush.SurroundColors = new Color[] { Color.Lime };
                        break;
                    case SymbolState.Off:
                        _gBrush.CenterColor = Color.Black;
                        _gBrush.SurroundColors = new Color[] { Color.Black };
                        break;
                    case SymbolState.Error:
                        _gBrush.CenterColor = Color.Red;
                        _gBrush.SurroundColors = new Color[] { Color.Red };
                        break;
                    case SymbolState.Level1_Alarm:
                        _gBrush.CenterColor = Color.Gold;
                        _gBrush.SurroundColors = new Color[] { Color.Gold };
                        break;
                    case SymbolState.Level2_Alarm:
                        _gBrush.CenterColor = Color.Orange;
                        _gBrush.SurroundColors = new Color[] { Color.Orange };
                        break;
                }

                _gpath.CloseFigure();
                _gpath.AddArc(C1X1, C1Y1, circlesize, circlesize, 0, 360);
                _gBrush = new System.Drawing.Drawing2D.PathGradientBrush(_gpath);
                _gBrush.WrapMode = WrapMode.TileFlipY;
                e.Graphics.FillPath(_gBrush, _gpath);
                _gpath.CloseFigure();



                _gpath.AddArc(C2X1, C2Y1, circlesize, circlesize, 0, 360);
                _gBrush = new System.Drawing.Drawing2D.PathGradientBrush(_gpath);
                _gBrush.WrapMode = WrapMode.TileFlipY;
                e.Graphics.FillPath(_gBrush, _gpath);



            }


            _gpath.CloseFigure();
            _gpath.AddLine(L1X1, L1Y1, L1X2, L1Y2);
            _gpath.CloseFigure();
            _gpath.AddLine(L2X1_1, L2Y1_1, L2X2, L2Y2);
            _gpath.CloseFigure();
            _gpath.AddLine(L3X1, L3Y1, L3X2, L3Y2);
            _gpath.CloseFigure();
            _gpath.AddLine(L4X1, L4Y1, L4X2, L4Y2);
            _gpath.CloseFigure();
            _gpath.AddLine(L5X1, L5Y1, L5X2, L5Y2);
            _gpath.CloseFigure();
            _gpath.AddLine(L6X1, L6Y1, L6X2, L6Y2);
            _gpath.CloseFigure();

            _gpath.AddLine(L7X1, L7Y1, L7X2, L7Y2);
            _gpath.CloseFigure();
            //e.Graphics.DrawPath(this.Pen, _gpath);
            using (Pen newpen = new Pen(this.Brush, this.ExtenderWidth))
            {
                e.Graphics.DrawPath(newpen, _gpath);
            }

        }


    }
}
