﻿using System;
using System.Data;
using System.Text;
using System.ComponentModel;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace iSCADA.Design.Utilities.Electrical
{
    public partial class RetangleUC : ElectricalSymbol
    {
        //private DashStyle _dashstyle = DashStyle.Solid;
        //private System.Drawing.Pen myPen = new System.Drawing.Pen(System.Drawing.Color.Lime, 3);
        //public RetangleUC()
        //{
        //    InitializeComponent();
        //}

        //[Category("Electrical Symbols"), Description("Get or set the DashStyle"), DefaultValueAttribute(DashStyle.Solid)]
        //[Category("Electrical Symbols"), Description("Get or set the DashStyle")]
        //public DashStyle dashstyle
        //{
        //    get { return this.dashstyle; }
        //    set
        //    {
        //        this._dashstyle = value;
        //        switch (value)
        //        {
        //            case DashStyle.Dash:
        //                this.Pen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash;
        //                break;
        //            case DashStyle.DashDot:
        //                this.Pen.DashStyle = System.Drawing.Drawing2D.DashStyle.DashDot;
        //                break;
        //            case DashStyle.DashDotDot:
        //                this.Pen.DashStyle = System.Drawing.Drawing2D.DashStyle.DashDotDot;
        //                break;
        //            case DashStyle.Dot:
        //                this.Pen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dot;
        //                break;
        //            case DashStyle.Solid:
        //                this.Pen.DashStyle = System.Drawing.Drawing2D.DashStyle.Solid;
        //                break;
        //        }
        //        this.Refresh();
        //    }
        //}

        protected override void OnPaint(System.Windows.Forms.PaintEventArgs e)
        {
            base.OnPaint(e);
            this.Paint(e);
        }
        private new void Paint(System.Windows.Forms.PaintEventArgs e)
        {
            Rectangle rec = this.DisplayRectangle;
            //Rectangle newrec = new Rectangle(rec.Left, rec.Top, rec.Width - 1, rec.Height - 1);
            Rectangle newrec = new Rectangle(rec.X, rec.Y, rec.Width - 1, rec.Height - 1);
            if (this.Fill == true)
            {
                e.Graphics.FillRectangle(this.Brush, newrec);
                //using (Pen newpen = new Pen(this.Brush, this.ExtenderWidth))
                //{
                //    e.Graphics.FillEllipse(this.Brush, newrec);
                //}
            }
            else
            {
                //e.Graphics.DrawRectangle(this.Pen, newrec);
                using (Pen newpen = new Pen(this.Brush, this.ExtenderWidth))
                {
                    //e.Graphics.DrawEllipse(newpen, newrec);
                    e.Graphics.DrawRectangle(newpen, newrec);
                    //e.Graphics.DrawEllipse(this.Pen, newrec);
                }
            }
            
        }


        //protected override void ToolStripItem_Click(object sender, EventArgs e)
        //{
        //    base.ToolStripItem_Click(sender, e);
        //    MessageBox.Show(this.Name+" "+sender.ToString());
        //}
        //protected override void OnResize(EventArgs e)
        //{
        //    base.OnResize(e);
        //    this.ResizeNow(e);
        //}

        //private void ResizeNow(EventArgs ea)
        //{
        //    this.Refresh();
        //}

    }
}
