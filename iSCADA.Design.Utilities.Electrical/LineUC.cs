﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace iSCADA.Design.Utilities.Electrical
{
    public partial class LineUC : ElectricalSymbol
    {
        private SymbolOrientation _orientation = SymbolOrientation.Horizontal;
        private SymbolState _state = SymbolState.Disconnected;
        private DashStyle _dashstyle = DashStyle.Solid;
        //private LineCap _startcap = LineCap.NoAnchor;
        //private LineCap _endcap = LineCap.NoAnchor;

        public LineUC()
        {
            InitializeComponent();
        }

        [Category("Electrical Symbols"), Description("Get or set the Orientation"), DefaultValueAttribute(SymbolOrientation.Horizontal)]
        public SymbolOrientation Orientation
        {
            get { return this._orientation; }
            set
            {
                this._orientation = value;
                this.Refresh();
            }
        }

        //[Category("Electrical Symbols"), Description("Get or set the StartCap"), DefaultValueAttribute(LineCap.NoAnchor)]
        //public LineCap StartCap
        //{
        //    get { return this._startcap; }
        //    set
        //    {
        //        this._startcap = value;
        //        switch (value)
        //        {
        //            case LineCap.ArrowAnchor:
        //                this.Pen.StartCap = System.Drawing.Drawing2D.LineCap.ArrowAnchor;
        //                break;
        //            case LineCap.DiamondAnchor:
        //                this.Pen.StartCap = System.Drawing.Drawing2D.LineCap.DiamondAnchor;
        //                break;
        //            case LineCap.Flat:
        //                this.Pen.StartCap = System.Drawing.Drawing2D.LineCap.Flat;
        //                break;
        //            case LineCap.Round:
        //                this.Pen.StartCap = System.Drawing.Drawing2D.LineCap.Round;
        //                break;
        //            case LineCap.RoundAnchor:
        //                this.Pen.StartCap = System.Drawing.Drawing2D.LineCap.RoundAnchor;
        //                break;
        //            case LineCap.Square:
        //                this.Pen.StartCap = System.Drawing.Drawing2D.LineCap.Square;
        //                break;
        //            case LineCap.SquareAnchor:
        //                this.Pen.StartCap = System.Drawing.Drawing2D.LineCap.SquareAnchor;
        //                break;
        //            case LineCap.Triangle:
        //                this.Pen.StartCap = System.Drawing.Drawing2D.LineCap.Triangle;
        //                break;
        //            default:
        //                this.Pen.StartCap = System.Drawing.Drawing2D.LineCap.NoAnchor;
        //                break;
        //        }
        //        this.Refresh();
        //    }
        //}

        //[Category("Electrical Symbols"), Description("Get or set the StartCap"), DefaultValueAttribute(LineCap.NoAnchor)]
        //public LineCap EndCap
        //{
        //    get { return this._endcap; }
        //    set
        //    {
        //        this._endcap = value;
        //        switch (value)
        //        {
        //            case LineCap.ArrowAnchor:
        //                this.Pen.EndCap = System.Drawing.Drawing2D.LineCap.ArrowAnchor;
        //                break;
        //            case LineCap.DiamondAnchor:
        //                this.Pen.EndCap = System.Drawing.Drawing2D.LineCap.DiamondAnchor;
        //                break;
        //            case LineCap.Flat:
        //                this.Pen.EndCap = System.Drawing.Drawing2D.LineCap.Flat;
        //                break;
        //            case LineCap.Round:
        //                this.Pen.EndCap = System.Drawing.Drawing2D.LineCap.Round;
        //                break;
        //            case LineCap.RoundAnchor:
        //                this.Pen.EndCap = System.Drawing.Drawing2D.LineCap.RoundAnchor;
        //                break;
        //            case LineCap.Square:
        //                this.Pen.EndCap = System.Drawing.Drawing2D.LineCap.Square;
        //                break;
        //            case LineCap.SquareAnchor:
        //                this.Pen.EndCap = System.Drawing.Drawing2D.LineCap.SquareAnchor;
        //                break;
        //            case LineCap.Triangle:
        //                this.Pen.EndCap = System.Drawing.Drawing2D.LineCap.Triangle;
        //                break;
        //            default:
        //                this.Pen.EndCap = System.Drawing.Drawing2D.LineCap.NoAnchor;
        //                break;
        //        }
        //        this.Refresh();
        //    }
        //}

        protected override void OnPaint(System.Windows.Forms.PaintEventArgs e)
        {
            base.OnPaint(e);
            this.Paint(e);
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            ResizeNow(e);
        }

        private void ResizeNow(EventArgs ea)
        {
            this.Refresh();
        }
        private new void Paint(System.Windows.Forms.PaintEventArgs e)
        {
            int start_x = 0, start_y = 0, end_x = 0, end_y = 0;
            int lheight = 0, lwidth = 0;

            //if (this._orientation == SymbolOrientation.Horizontal)
            //{
            //    start_x = 0;
            //    start_y = 0;
            //    end_y = 0;
            //    end_x = this.DisplayRectangle.Width;
            //    lheight = this.ExtenderWidth;
            //    lwidth = this.DisplayRectangle.Width;
            //    this.Width = lwidth;
            //    this.Height = lheight;
            //}
            //else
            //{
            //    start_x = 0;
            //    start_y = 0;
            //    end_x = 0;
            //    end_y = this.DisplayRectangle.Width;
            //    lheight =  this.DisplayRectangle.Height;
            //    lwidth = this.ExtenderWidth;
            //    this.Width = lwidth;
            //    this.Height = lheight;
            //}
            //System.Drawing.Drawing2D.GraphicsPath _gpath = new System.Drawing.Drawing2D.GraphicsPath();
            //if (this.ExtenderWidth <= 1)
            //{
            //    _gpath.AddLine(start_x, start_y, end_x, end_y);
            //}
            //else
            //{
            //    _gpath.AddRectangle(new Rectangle(start_x, start_y, lwidth-1, lheight-1));
            //}
            //e.Graphics.FillPath(this.Brush, _gpath);
            //e.Graphics.DrawPath(this.Pen, _gpath);
            /////////////////////////////////////////////////////////////////
            if (this._orientation == SymbolOrientation.Horizontal)
            {
                start_x = 0;
                start_y = this.DisplayRectangle.Height/2;
                end_y = this.DisplayRectangle.Height / 2;
                end_x = this.DisplayRectangle.Width;
                //lheight = this.ExtenderWidth;
                //lwidth = this.DisplayRectangle.Width;
                //this.Width = lwidth;
                //this.Height = lheight;
            }
            else
            {
                start_x = this.DisplayRectangle.Width/2;
                start_y = 0;
                end_x = this.DisplayRectangle.Width / 2;
                end_y = this.DisplayRectangle.Height;
                //lheight = this.DisplayRectangle.Height;
                //lwidth = this.ExtenderWidth;
                //this.Width = lwidth;
                //this.Height = lheight;
            }

            /////////////////////////////////////////////////////////////////////////////////////////////
            //switch (_state)
            //{
            //    case SymbolState.On:
            //        using (Pen newpen = new Pen(Color.Red, this.ExtenderWidth))
            //        {
            //            e.Graphics.DrawLine(newpen, start_x, start_y, end_x, end_y);
            //        }
            //        break;
            //    case SymbolState.Error:
            //        using (Pen newpen = new Pen(Color.Lime, this.ExtenderWidth))
            //        {
            //            e.Graphics.DrawLine(newpen, start_x, start_y, end_x, end_y);
            //        }
            //        break;
            //    case SymbolState.Off:
            //        using (Pen newpen = new Pen(Color.Black, this.ExtenderWidth))
            //        {
            //            e.Graphics.DrawLine(newpen, start_x, start_y, end_x, end_y);
            //        }
            //        break;
            //    case SymbolState.Level1_Alarm:
            //        using (Pen newpen = new Pen(Color.Gold, this.ExtenderWidth))
            //        {
            //            e.Graphics.DrawLine(newpen, start_x, start_y, end_x, end_y);
            //        }
            //        break;
            //    case SymbolState.Level2_Alarm:
            //        using (Pen newpen = new Pen(Color.Orange, this.ExtenderWidth))
            //        {
            //            e.Graphics.DrawLine(newpen, start_x, start_y, end_x, end_y);
            //        }
            //        break;
            //    case SymbolState.Disconnected:
            //        using (Pen newpen = new Pen(Color.Black, this.ExtenderWidth))
            //        {
            //            e.Graphics.DrawLine(newpen, start_x, start_y, end_x, end_y);
            //        }
            //        break;
            //    case SymbolState.Default:
            //        using (Pen newpen = new Pen(Color.Blue, this.ExtenderWidth))
            //        {
            //            e.Graphics.DrawLine(newpen, start_x, start_y, end_x, end_y);
            //        }
            //        break;
            //}
            /////////////////////////////////////////////////////////////////////////////////////////////
            //GraphicsPath graphPath = new GraphicsPath();
            //graphPath.AddLine(start_x, start_y, end_x, end_y);
            //e.Graphics.DrawPath(this.Pen, graphPath);
            Pen newpen = new Pen(this.Pen.Color, this.ExtenderWidth);
            e.Graphics.DrawLine(newpen, start_x, start_y, end_x, end_y);
            newpen.Dispose();
            //switch (_state)
            //{
            //    case SymbolState.On:
            //        Pen redpen = new Pen(Color.Red,this.ExtenderWidth);
            //        e.Graphics.DrawPath(redpen, graphPath);
            //        redpen.Dispose();
            //        break;
            //    case SymbolState.Error:
            //        Pen limepen = new Pen(Color.Lime, this.ExtenderWidth);
            //        e.Graphics.DrawPath(limepen, graphPath);
            //        limepen.Dispose();
            //        break;
            //    case SymbolState.Off:
            //        Pen blackpen = new Pen(Color.Black, this.ExtenderWidth);
            //        e.Graphics.DrawPath(blackpen, graphPath);
            //        blackpen.Dispose();
            //        break;
            //    case SymbolState.Level1_Alarm:
            //        Pen goldpen = new Pen(Color.Gold, this.ExtenderWidth);
            //        e.Graphics.DrawPath(goldpen, graphPath);
            //        goldpen.Dispose();
            //        break;
            //    case SymbolState.Level2_Alarm:
            //        Pen orangepen = new Pen(Color.Orange, this.ExtenderWidth);
            //        e.Graphics.DrawPath(orangepen, graphPath);
            //        orangepen.Dispose();
            //        break;
            //    case SymbolState.Disconnected:
            //        Pen penblack = new Pen(Color.Black, this.ExtenderWidth);
            //        e.Graphics.DrawPath(penblack, graphPath);
            //        penblack.Dispose();
            //        break;
            //    case SymbolState.Default:
            //        Pen bluepen = new Pen(Color.Blue, this.ExtenderWidth);
            //        e.Graphics.DrawPath(bluepen, graphPath);
            //        bluepen.Dispose();
            //        break;
            //}
            /////////////////////////////////////////////////////////////////////////////////////////////
            //e.Graphics.DrawLine(this.Pen, start_x, start_y, end_x, end_y);
            //using (Pen newpen = new Pen(this.Brush, this.ExtenderWidth))
            //{
            //    //pea.Graphics.DrawPath(this.Pen, _gpath);
            //    //pea.Graphics.DrawPath(newpen, _gpath);
            //    e.Graphics.DrawLine(newpen, start_x, start_y, end_x, end_y);
            //}
        }
    }
}
