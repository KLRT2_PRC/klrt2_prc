﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iSCADA.Design.Utilities.Electrical
{
    public partial class Switch02UC : ElectricalSymbol
    {
        private SymbolOrientation _orientation = SymbolOrientation.Horizontal;
        public Switch02UC()
        {
            InitializeComponent();
        }

        //[Category("Graphical Symbols for Single Line Diagrams"), Description("Get or set the element orientation of the Isolator")]
        [Category("Electrical Symbols"), Description("Get or set the Orientation"), DefaultValueAttribute(SymbolOrientation.Horizontal)]
        public SymbolOrientation Orientation
        {
            get { return this._orientation; }
            set
            {
                this._orientation = value;
                this.Refresh();
            }
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            this.Paint(e);
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            ResizeNow(e);
        }

        private void ResizeNow(EventArgs ea)
        {
            this.Refresh();
        }

        private new void Paint(PaintEventArgs e)
        {
            System.Drawing.Drawing2D.PathGradientBrush _gBrush = null;
            System.Drawing.Drawing2D.GraphicsPath _gpath = new System.Drawing.Drawing2D.GraphicsPath();

            //定義圖形大小，可任意縮放
            Double defaultsize =(this.Orientation==SymbolOrientation.Horizontal? (this.Width / 3):(this.Height / 3));
            float switchsize = (int)Math.Ceiling(defaultsize);
            //有兩個圓，大小為圖形的0.1
            float circlesize = switchsize * .1f;
            //共有四條線，每條線有兩個點，起點終點
            float L1X1 = 0, L1Y1 = 0, L1X2 = 0, L1Y2 = 0;
            float L2X1 = 0, L2Y1 = 0, L2X2 = 0, L2Y2 = 0, L2X2_1 = 0, L2Y2_1 = 0;
            float L3X1 = 0, L3Y1 = 0, L3X2 = 0, L3Y2 = 0;
            float L4X1 = 0, L4Y1 = 0, L4X2 = 0, L4Y2 = 0;
            //兩個圓的起點跟終點
            float C1X1 = 0, C1Y1 = 0, C2X1 = 0, C2Y1 = 0;
            ///////////////////////////////////////////////////////////////////////////
            float L5X1 = 0, L5Y1 = 0, L5X2 = 0, L5Y2 = 0;
            float L6X1 = 0, L6Y1 = 0, L6X2 = 0, L6Y2 = 0;
            float L7X1 = 0, L7Y1 = 0, L7X2 = 0, L7Y2 = 0;
            float L8X1 = 0, L8Y1 = 0, L8X2 = 0, L8Y2 = 0;
            float C3X1 = 0, C3Y1 = 0;
            ///////////////////////////////////////////////////////////////////////////
            if (this.Orientation == SymbolOrientation.Horizontal)
            {
                if (this.State == SymbolState.Error)
                {
                    L2X1 = switchsize;
                    L2Y1 = switchsize;
                    L2X2 = switchsize * 2;
                    L2Y2 = L2Y1;
                    L2X2_1 = L2X2 - circlesize;
                    L2Y2_1 = L2Y2;

                    L4X1 = (float)(switchsize * 1.5);
                    L4Y1 = L2Y1;
                    L4X2 = L4X1;
                    L4Y2 = (float)(L4Y1 - (L4Y1 * 0.5));
                }
                else
                {
                    L2X1 = switchsize;
                    L2Y1 = switchsize;
                    L2X2 = switchsize * 2;
                    L2Y2 = L2Y1;
                    L2X2_1 = (float)(L2X2 - (L2X2 * 0.12));
                    L2Y2_1 = (float)(L2Y2 - (L2Y2 * 0.5));

                    L4X1 = (float)(L2X1*1.25*1.15);
                    L4Y1 = (float)(L2Y1 - L2X1 * 0.25);
                    L4X2=(float)(L4X1*0.74);
                    L4Y2 = (float)(L4Y1 * 0.3);

                }

                L1X1 = 0;
                L1Y1 = L2Y1;
                L1X2 = L2X1;
                L1Y2 = L2Y1;

                L3X1 = L2X2;
                L3Y1 = L2Y1;
                L3X2 = switchsize * 3;
                L3Y2 = L2Y1;

                
                L5X1 = (float)(switchsize * 1.5);
                L5Y1 = L2Y1+ (float)(switchsize * 0.3);
                L5X2 = L5X1;
                L5Y2 = L2Y1 + (float)(switchsize*0.8);
                C3X1 = L5X1-(circlesize/2);
                C3Y1 = L5Y1+(circlesize/2);
                L5Y1 =C3Y1+(circlesize);

                C1X1 = L2X1;
                C1Y1 = L2Y1 - (circlesize / 2);
                L2X1 += circlesize;
                C2X1 = L2X2 - circlesize;
                C2Y1 = L2Y2 - (circlesize / 2);
                L2X2 -= circlesize;

                L6X1 = L5X2 - (float)(switchsize / 4);
                L6Y1 = L5Y2;
                L6X2 = L5X2 + (float)(switchsize/4);
                L6Y2 = L6Y1;
                L7X1 = L5X2 - (float)(switchsize / 8);
                L7Y1 = L5Y2+(float)(circlesize/2);
                L7X2 = L5X2 + (float)(switchsize / 8);
                L7Y2 = L7Y1;
                L8X1 = L5X2 - (float)(switchsize / 12);
                L8Y1 = L7Y1 + (float)(circlesize /2);
                L8X2 = L5X2 + (float)(switchsize / 12);
                L8Y2 = L8Y1;


                this.Height = (int)Math.Ceiling(L2Y2 * 2.2);
                this.Width = (int)Math.Ceiling((switchsize * 3) + 1);
            }
            else
            {
                if (this.State == SymbolState.Error)
                {
                    L2X1 = switchsize;
                    L2Y1 = switchsize;
                    L2X2 = L2X1;
                    L2Y2 = switchsize * 2;
                    L2X2_1 = L2X2;
                    L2Y2_1 = L2Y2 - circlesize;

                    L4X1 = L2X1;
                    L4Y1 = (float)(switchsize * 1.5);
                    L4X2 = (float)(L4X1 - (L4X1 * 0.5));
                    L4Y2 = L4Y1;
                }
                else
                {
                    L2X1 = switchsize;
                    L2Y1 = switchsize;
                    L2X2 = L2X1;
                    L2Y2 = switchsize * 2;
                    L2X2_1 = (float)(L2X2 - (L2X2 * 0.5));
                    L2Y2_1 = (float)(L2Y2 - (L2Y2 * 0.12));

                    L4X1 = (float)(L2X1 - L2X1 * 0.25); 
                    L4Y1 = (float)(L2Y1 * 1.25 * 1.15);
                    L4X2 = (float)(L4X1 *0.5); 
                    L4Y2 = (float)(L4Y1 * 0.8);
                }

                L1X1 = L2X1;
                L1Y1 = 0;
                L1X2 = L2X1;
                L1Y2 = L2Y1;

                L3X1 = L2X1;
                L3Y1 = L2Y2;
                L3X2 = L2X1;
                L3Y2 = switchsize * 3;

                L5X1 = L2X1 + (float)(switchsize * 0.3);
                L5Y1 = (float)(switchsize * 1.5); 
                L5X2 = L2X1 + (float)(switchsize * 0.8); 
                L5Y2 = L5Y1;
                C3X1 = L5X1 + (circlesize / 2); 
                C3Y1 = L5Y1 - (circlesize / 2);
                L5X1 = C3X1 + (circlesize);

                C1X1 = L2X1 - (circlesize / 2);
                C1Y1 = L2Y1;
                L2Y1 += circlesize;
                C2X1 = L2X2 - (circlesize / 2);
                C2Y1 = L2Y2 - circlesize;
                L2Y2 -= circlesize;

                L6X1 = L5X2; 
                L6Y1 = L5Y2 - (float)(switchsize / 4);
                L6X2 = L6X1; 
                L6Y2 = L5Y2 + (float)(switchsize / 4);
                L7X1 = L5X2 + (float)(circlesize / 2); 
                L7Y1 = L5Y2 - (float)(switchsize / 8);
                L7X2 = L7X1; 
                L7Y2 = L5Y2 + (float)(switchsize / 8);
                L8X1 = L7X1 + (float)(circlesize / 2); 
                L8Y1 = L5Y2 - (float)(switchsize / 12);
                L8X2 = L8X1; 
                L8Y2 = L5Y2 + (float)(switchsize / 12);

                this.Width = (int)Math.Ceiling(L2Y2 * 1.2);
                this.Height = (int)Math.Ceiling((switchsize * 3) + 1);
            }

            if (this.Fill == false)
            {
                _gpath.AddArc(C1X1, C1Y1, circlesize, circlesize, 0, 360);
                _gpath.AddArc(C2X1, C2Y1, circlesize, circlesize, 0, 360);
                _gpath.AddArc(C3X1, C3Y1, circlesize, circlesize, 0, 360);
                _gpath.CloseFigure();
            }
            else
            {
                _gpath.CloseFigure();
                _gpath.AddArc(C1X1, C1Y1, circlesize, circlesize, 0, 360);
                _gBrush = new System.Drawing.Drawing2D.PathGradientBrush(_gpath);
                //_gBrush.CenterColor = this.Parent.BackColor;

                switch (this.State)
                {
                    case SymbolState.Disconnected:
                        _gBrush.CenterColor = Color.DeepSkyBlue;
                        _gBrush.SurroundColors = new Color[] { Color.DeepSkyBlue };
                        break;
                    case SymbolState.On:
                        //_gBrush.CenterColor = Color.Lime;
                        //_gBrush.SurroundColors = new Color[] { Color.Lime };
                        _gBrush.CenterColor = Color.Red;
                        _gBrush.SurroundColors = new Color[] { Color.Red };
                        break;
                    case SymbolState.Off:
                        _gBrush.CenterColor = Color.Black;
                        _gBrush.SurroundColors = new Color[] { Color.Black };
                        break;
                    case SymbolState.Error:
                        //_gBrush.CenterColor = Color.Red;
                        //_gBrush.SurroundColors = new Color[] { Color.Red };
                        _gBrush.CenterColor = Color.Lime;
                        _gBrush.SurroundColors = new Color[] { Color.Lime };
                        break;
                    case SymbolState.Level1_Alarm:
                        _gBrush.CenterColor = Color.Gold;
                        _gBrush.SurroundColors = new Color[] { Color.Gold };
                        break;
                    case SymbolState.Level2_Alarm:
                        _gBrush.CenterColor = Color.Orange;
                        _gBrush.SurroundColors = new Color[] { Color.Orange };
                        break;
                }
                _gBrush.WrapMode = WrapMode.TileFlipY;
                e.Graphics.FillPath(_gBrush, _gpath);
                _gpath.CloseFigure();
                _gpath.AddArc(C2X1, C2Y1, circlesize, circlesize, 0, 360);
                _gBrush = new System.Drawing.Drawing2D.PathGradientBrush(_gpath);
                _gBrush.WrapMode = WrapMode.TileFlipY;
                e.Graphics.FillPath(_gBrush, _gpath);
                _gpath.CloseFigure();
                _gpath.AddArc(C3X1, C3Y1, circlesize, circlesize, 0, 360);
                _gBrush = new System.Drawing.Drawing2D.PathGradientBrush(_gpath);
                _gBrush.WrapMode = WrapMode.TileFlipY;
                e.Graphics.FillPath(_gBrush, _gpath);
            }
            _gpath.CloseFigure();
            _gpath.AddLine(L2X1, L2Y1, L2X2_1, L2Y2_1);
            _gpath.CloseFigure();
            _gpath.AddLine(L1X1, L1Y1, L1X2, L1Y2);
            _gpath.CloseFigure();
            _gpath.AddLine(L3X1, L3Y1, L3X2, L3Y2);
            _gpath.CloseFigure();
            _gpath.AddLine(L4X1, L4Y1, L4X2, L4Y2);
            _gpath.CloseFigure();
            _gpath.AddLine(L5X1, L5Y1, L5X2, L5Y2);
            _gpath.CloseFigure();
            _gpath.AddLine(L6X1, L6Y1, L6X2, L6Y2);
            _gpath.CloseFigure();
            _gpath.AddLine(L7X1, L7Y1, L7X2, L7Y2);
            _gpath.CloseFigure();
            _gpath.AddLine(L8X1, L8Y1, L8X2, L8Y2);

            //e.Graphics.DrawPath(this.Pen, _gpath);
            using (Pen newpen = new Pen(this.Brush, this.ExtenderWidth))
            {
                e.Graphics.DrawPath(newpen, _gpath);
            }
        }
    }
}
