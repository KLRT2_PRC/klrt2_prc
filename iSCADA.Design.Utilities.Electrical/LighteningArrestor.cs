﻿using System;
using System.Drawing.Drawing2D;
using System.Drawing;
using System.Windows.Forms;
using System.ComponentModel;

namespace iSCADA.Design.Utilities.Electrical
{
    public partial class LighteningArrestor : ElectricalSymbol
    {

        private Matrix matrix = null;

        private float _topHandRatio = 1f;
        private float _bottomHandRatio = 3f;
        private float _isolatorGapRatio = 1f;
        private float _isolatorHookRatio = .1f;
        private SymbolOrientation _orientation = SymbolOrientation.Verticle;

        public LighteningArrestor()
        {
            InitializeComponent();
        }

        [Category("Graphical Symbols for Single Line Diagrams"), Description("Get or set the Top Link Line ration of the top node of the Isolator")]
        public float TopHandRatio
        {
            get { return this._topHandRatio; }
            set
            {
                this._topHandRatio = value;
                this.Refresh();
            }
        }

        [Category("Graphical Symbols for Single Line Diagrams"), Description("Get or set the Bottom Link Line ration of the bottom node of the Isolator")]
        public float BottomHandRatio
        {
            get { return this._bottomHandRatio; }
            set
            {
                this._bottomHandRatio = value;
                this.Refresh();
            }
        }

        [Category("Graphical Symbols for Single Line Diagrams"), Description("Get or set the ratio of isolator bar hucks of the Isolator")]
        public float IsolatorHookRatio
        {
            get { return this._isolatorHookRatio; }
            set
            {
                this._isolatorHookRatio = value;
                this.Refresh();
            }
        }

        [Category("Graphical Symbols for Single Line Diagrams"), Description("Get or set the Gap ration between isolator bar and nodes of the Isolator")]
        public float IsolatorGapRatio
        {
            get { return this._isolatorGapRatio; }
            set
            {
                this._isolatorGapRatio = value;
                this.Refresh();
            }
        }

        [Category("Graphical Symbols for Single Line Diagrams"), Description("Get or set the element orientation of the Isolator")]
        public SymbolOrientation Orientation
        {
            get { return this._orientation; }
            set
            {
                this._orientation = value;
                this.Refresh();
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            this.Paint(e);
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            ResizeNow(e);
        }

        private void ResizeNow(EventArgs ea)
        {
            this.Refresh();
        }

        private new void Paint(PaintEventArgs pea)
        {
            System.Drawing.Drawing2D.PathGradientBrush _gBrush = null;
            System.Drawing.Drawing2D.GraphicsPath _gpath = new System.Drawing.Drawing2D.GraphicsPath();

            
            //圓點size
            Double defaultsize = (this.Orientation == SymbolOrientation.Horizontal ? (this.Width/6) : (this.Height/6));
            int circlesize =(int) Math.Ceiling(defaultsize * .1f); // (int)Math.Ceiling(defaultsize); 
            //float circlesize = this.DisplayRectangle.Height * .1f;
            ///////////////////////////////////////////////////////////
            //啟始跟結束的圓點
            //float C1X1 = 0, C1Y1 = 0;
            //float C2X1 = 0, C2Y1 = 0;
            float L1X1 = 0, L1Y1 = 0;
            float L1X2 = 0, L1Y2 = 0;
            ///////////////////////////////////////////////////////////
            int C1X1 = 0, C1Y1 = 0;
            int C2X1 = C1X1, C2Y1 = this.DisplayRectangle.Height;
            int C3X1 = (int)Math.Ceiling(C1X1 + defaultsize), C3Y1 = this.DisplayRectangle.Height / 2;

            //L1X1 = C1X1;
            //L1Y1 = C1Y1;// - (circlesize / 2);
            //C1X1 += (circlesize / 2);

            L1X2 = C1X1;
            L1Y2 = C2Y1;// - (circlesize / 2);
            //C2X1 = C1X1;
            Point[] pt = { new Point(C1X1, C1Y1), new Point(C3X1, C3Y1), new Point(C2X1, C2Y1) };
            this.Width =C3X1+this.ExtenderWidth;
            this.Height = this.DisplayRectangle.Height;
            _gpath.AddCurve(pt);
            _gpath.CloseFigure();
            //_gpath.AddArc(L1X1, L1Y1, circlesize, circlesize, 0, 360);
            //_gpath.AddArc(L1X2, L1Y2, circlesize, circlesize, 0, 360);
            
            using (Pen newpen = new Pen(this.Brush, this.ExtenderWidth))
            {
                //pea.Graphics.DrawPath(this.Pen, _gpath);
                pea.Graphics.DrawPath(newpen, _gpath);
            }
            //pea.Graphics.DrawCurve(this.Pen, pt);
        }
    }
}
