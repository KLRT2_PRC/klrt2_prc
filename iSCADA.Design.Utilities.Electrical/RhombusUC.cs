﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iSCADA.Design.Utilities.Electrical
{
    public partial class RhombusUC : ElectricalSymbol
    {
        public RhombusUC()
        {
            InitializeComponent();
        }

        protected override void OnPaint(System.Windows.Forms.PaintEventArgs e)
        {
            base.OnPaint(e);
            this.Paint(e);
        }
        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            ResizeNow(e);
        }

        private void ResizeNow(EventArgs ea)
        {
            this.Refresh();
        }
        private new void Paint(System.Windows.Forms.PaintEventArgs e)
        {

            //int x = 8;
            //int y = 8;
            //Point point1 = new Point(x - 7, y);
            //Point point2 = new Point(x, y + 7);
            //Point point3 = new Point(x + 7, y);
            //Point point4 = new Point(x, y - 7);
            //this.Width = x+8;
            //this.Height = y+8;
            //Point[] curvePoints =
            //         {
            //     point1,
            //     point2,
            //     point3,
            //     point4

            // };
            //e.Graphics.DrawPolygon(this.Pen, curvePoints);

            //using (GraphicsPath myPath = new GraphicsPath())
            //{
            //    myPath.AddLines(new[]
            //        {
            //    new Point(0, Height / 2),
            //    new Point(Width / 2, 0),
            //    new Point(Width, Height / 2),
            //    new Point(Width / 2, Height)
            //});
            //Region = new Region(myPath);
            if (this.Fill == true)
            {
                e.Graphics.FillPolygon(this.Brush, new Point[] { new Point(0, Height / 2), new Point(Width / 2, 0), new Point(Width, Height / 2), new Point(Width / 2, Height) });
            }
            else
            {
                using (Pen newpen = new Pen(this.Brush, this.ExtenderWidth))
                {
                    e.Graphics.DrawPolygon(newpen, new Point[] { new Point(0, Height / 2), new Point(Width / 2, 0), new Point(Width, Height / 2), new Point(Width / 2, Height) });
                }
                //e.Graphics.DrawPolygon(this.Pen, new Point[] { new Point(0, Height / 2), new Point(Width / 2, 0), new Point(Width, Height / 2), new Point(Width / 2, Height) });
            }
        }
    }
}
//}
