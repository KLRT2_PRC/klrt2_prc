﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICSC.SysUtil.Log
{
    public class LogHandler
    {
        public static void Append(StringBuilder logSB, string msg)
        {
            if (!string.Equals(msg, string.Empty))
            {
                logSB.Append("<" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss, fff") + ">: " + msg);
            }
        }

        public static void AppendLine(StringBuilder logSB, string msg)
        {
            if (!string.Equals(msg, string.Empty))
            {
                logSB.AppendLine("<" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss, fff") + ">: " + msg);
            }
        }

        public static void EnqueueByLine(string msg, SafeQueue<string> qName)
        {
            string[] array = msg.Split(new string[]
            {
                Environment.NewLine
            }, StringSplitOptions.None);
            for (int i = 0; i < array.Length; i++)
            {
                if (i != array.Length - 1 || !(array[i] == string.Empty))
                {
                    qName.Enqueue(array[i]);
                }
            }
        }
    }
}
